<?php
class Bootstrap extends Zend_Application_Bootstrap_Bootstrap
{

    protected $shop_id = 0;

    protected Shop $shop;

    protected function _initAutoload ()
    {
        $autoloader = Zend_Loader_Autoloader::getInstance();
        $autoloader->registerNamespace( 'TP_' );
        $autoloader->registerNamespace( 'XMPPHP_' );
        $autoloader->registerNamespace( 'Imind_' );
        $autoloader->registerNamespace( 'Apache_' );
        $autoloader->registerNamespace( 'ZFDebug' );
        $autoloader->registerNamespace( 'Pheanstalk' );
        $autoloader->registerNamespace( 'Payment_' );
        $autoloader->registerNamespace( 'Twig' );
        $autoloader->registerNamespace( 'Elastica_' );
        $autoloader->registerNamespace( 'EasyBib_' );
        require_once ('Doctrine.php');
        $autoloader->pushAutoloader( array (
        'Doctrine', 
        'autoload'), 'Doctrine' );
        $autoloaderModule = new Zend_Application_Module_Autoloader( array (
        'namespace' => 'Admin', 
        'basePath' => APPLICATION_PATH . '/modules/admin') );
        $autoloader->pushAutoloader( $autoloaderModule );

        

    }
    protected function _initDb ()
    {

        try
        {
            $config = new Zend_Config_Ini( APPLICATION_PATH . '/configs/database.ini', APPLICATION_ENV );
            /**
             * @see Zend_Db
             */
            $servers = array (
            'host' => 'localhost', 
            'port' => 11211, 
            'persistent' => true);
            /*$cacheDriver = new Doctrine_Cache_Memcache(array(
                    'servers' => $servers,
                    'compression' => false
                )
            );*/
            $cacheDriver = new Doctrine_Cache_Array( );
            $dsn = 'mysql:dbname=psc;host=mysql';
            $user = 'root';
            $password = 'Wichtig1';

            if ( (isset( $_REQUEST['ARTID'] ) || isset( $_REQUEST['artid'] ) || isset( $_POST['ARTID'] )) && (strpos( $_SERVER['REQUEST_URI'], 'upload' ) !== FALSE || strpos( $_SERVER['REQUEST_URI'], 'template' ) !== FALSE || strpos( $_SERVER['REQUEST_URI'], 'basket' ) !== FALSE || strpos( $_SERVER['REQUEST_URI'], 'article' ) !== FALSE) )
            {
                if(strlen($_REQUEST['ARTID']) > 100) {
                    $values = TP_Crypt::decrypt($_REQUEST['ARTID']);
                    $_SERVER["SERVER_NAME"] = str_replace(array("http://", "https://"), array("", ""), $values['SERVER']);

                }
            }

            $dbh = new PDO($dsn, $user, $password);

            $dbh -> exec("set names utf8");
            Doctrine_Manager::connection( $dbh )->setAttribute( Doctrine::ATTR_QUERY_CACHE, $cacheDriver )->setAttribute( Doctrine::ATTR_RESULT_CACHE, $cacheDriver )->setAttribute( Doctrine::ATTR_QUERY_CACHE_LIFESPAN, 3600 )->setAttribute( Doctrine::ATTR_RESULT_CACHE_LIFESPAN, 3600 );
            Doctrine_Manager::getInstance()->setCharset( 'utf8' );
            Doctrine_Manager::getInstance()->setCollate( 'utf8_general_ci' );
            //Doctrine_Manager::getInstance()->getCurrentConnection()->setAttribute(PDO::MYSQL_ATTR_INIT_COMMAND, "SET NAMES 'utf8';");
            Doctrine_Manager::getInstance()->registerExtension( 'Taggable' );
            Doctrine::loadModels( dirname( __FILE__ ) . '/data/models/generated' );
            Doctrine::loadModels( dirname( __FILE__ ) . '/data/models' );
            /*
             * Configure Doctrine
             */
            Zend_Registry::set( 'doctrine_config', array (
            'data_fixtures_path' => dirname( __FILE__ ) . '/doctrine/data/fixtures', 
            'models_path' => dirname( __FILE__ ) . '/models', 
            'migrations_path' => dirname( __FILE__ ) . '/doctrine/migrations', 
            'sql_path' => dirname( __FILE__ ) . '/doctrine/data/sql', 
            'yaml_schema_path' => dirname( __FILE__ ) . '/doctrine/schema') );
        }
        catch ( Exception $e )
        {
            die( $e->getMessage() );
        }
    }
    protected function _initLog ()
    {
        $writer = new Zend_Log_Writer_Stream( APPLICATION_PATH . '/../logs/app.log' );
        //$writer = new Zend_Log_Writer_Null();
        $logger = new Zend_Log( $writer );
        $logger->addPriority( 'TABLE', 8 );
        Zend_Registry::set( 'log', $logger );
        ini_set( 'error_log', APPLICATION_PATH . '/../logs/php.error.log' );
        if ( (isset( $_REQUEST['ARTID'] ) || isset( $_REQUEST['artid'] ) || isset( $_POST['ARTID'] )) && (strpos( $_SERVER['REQUEST_URI'], 'upload' ) !== FALSE || strpos( $_SERVER['REQUEST_URI'], 'template' ) !== FALSE || strpos( $_SERVER['REQUEST_URI'], 'basket' ) !== FALSE || strpos( $_SERVER['REQUEST_URI'], 'overview' ) !== FALSE || strpos( $_SERVER['REQUEST_URI'], 'steplayouter' ) !== FALSE  || strpos( $_SERVER['REQUEST_URI'], 'article' ) !== FALSE) )
        {
            if(strlen($_REQUEST['ARTID']) > 100) {
                $values = TP_Crypt::decrypt($_REQUEST['ARTID']);
                Zend_Session::setId( $values['ARTID'] );
                
            }else{
                if ( isset( $_POST['ARTID'] ) )
                {
                    Zend_Registry::get( 'log' )->debug( $_POST['ARTID'] );
                    try
                    {
                        Zend_Session::setId( $_POST['ARTID'] );
                    }
                    catch ( Exception $e )
                    {
                        Zend_Registry::get( 'log' )->debug( $e->getMessage() );
                    }
                }
                else
                {
                    Zend_Session::setId( $_REQUEST['ARTID'] );
                }
            }
        }
        /*$hostname = explode('.', $_SERVER["SERVER_NAME"]);
        if(count($hostname) == 2) {
            Zend_Session::start(array('cookie_domain' => '.'.$hostname[0].'.'.$hostname[1]));
        }elseif(count($hostname) == 3) {
            Zend_Session::start(array('cookie_domain' => '.'.$hostname[1].'.'.$hostname[2]));
        }*/
    }
    protected function _initCli ()
    {
        if ( strrpos( strtolower( PHP_SAPI ), 'cli' ) !== false && APPLICATION_ENV != 'testing' )
        {
            $this->bootstrap( 'frontController' );
            $this->frontController->setRouter( new TP_Controller_Router_Cli( ) )->setRequest( new TP_Controller_Request_Cli( ) )->setResponse( new TP_Controller_Response_Cli( ) );
        }
    }
    protected function _initLayout ()
    {
        try
        {
            if($_SERVER["SERVER_NAME"] == "" && isset($_SERVER["HTTP_HOST"]) && $_SERVER["HTTP_HOST"] != "") {
                $_SERVER["SERVER_NAME"] =  $_SERVER["HTTP_HOST"];
            }
            $config = new Zend_Config_Ini( APPLICATION_PATH . '/configs/layout.ini', APPLICATION_ENV, true );
            $row = Doctrine_Query::create()->from( 'Domain m' )->where( 'm.name = ?', $_SERVER["SERVER_NAME"] )->fetchOne();
            if ( $row === false )
            {
                $shop = Doctrine_Query::create()->from( 'Shop s' )->fetchOne();
            }else{
                $shop = $row->Shop;
            }
            $this->shop = $shop;
            if ( $row->redirect != "" && $_SERVER["SERVER_NAME"] != "web") {
                $path = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);
                header('Location: '.$row->redirect. $path, true, 301);
                die();
            }

            if($shop->deleted == true) {
                die('<html>
<head><title>' . $shop->name . ' ist geschlossen</title></head>
<body>
	<center>
	<h1 style="font-family:arial">Dieser Shop ist zur Zeit geschlossen</h1>
	</center>
</body>
</html>');
            }

            $config->layoutPath = str_replace( '%SHOPID%', $shop->uid, $config->layoutPath );
            $config->shopPath = APPLICATION_PATH . '/' . str_replace( '%SHOPID%', $shop->uid, $config->shopPath );

            $this->shop_id = $shop->uid;
            if ( $shop->customtemplates == 1 )
            {
                $config->layoutPath = APPLICATION_PATH . '/design/vorlagen/' . $shop->layout;
            }elseif($shop->template_switch && Zend_Auth::getInstance()->hasIdentity()) {
                $user = Zend_Auth::getInstance()->getIdentity();
                $accountPath = $this->getAccountTemplatePath($user['account_id']);
                if($accountPath != "") {
                    $config->layoutPath = APPLICATION_PATH . '/' . str_replace( '%LAYOUT%', $accountPath, $config->layoutPath );
                }else{
                    $config->layoutPath = APPLICATION_PATH . '/' . str_replace( '%LAYOUT%', $shop->layout, $config->layoutPath );
                }
            } else {
                if(!file_exists(APPLICATION_PATH . str_replace( '%LAYOUT%', "", $config->layoutPath))) mkdir(APPLICATION_PATH . str_replace( '%LAYOUT%', "", $config->layoutPath));
                if(strpos('_'.$shop->layout, "/")) {
                    $config->layoutPath = APPLICATION_PATH . str_replace( '%LAYOUT%', "", $config->layoutPath) . $shop->layout;
                }else{
                    $config->layoutPath = APPLICATION_PATH . '/' . str_replace( '%LAYOUT%', $shop->layout, $config->layoutPath );
                }
            }

            
            Zend_Registry::set( 'install', $shop->Install->toArray() );
            Zend_Registry::set( 'shop', $shop->toArray() );
            Zend_Registry::set( 'shop_path', $config->shopPath );
            Zend_Registry::set( 'layout_path', $config->layoutPath );
            $config->layoutPath = $config->layoutPath . '/layout';
            require_once 'Zend/Layout.php';
            Zend_Layout::startMvc( $config );

            if ( isset( $_GET['no_cache'] ) )
            {
                TP_Util::clearCache(TP_Util::CLEAR_ALL);
            }
        }
        catch ( Exception $e )
        {
            var_dump($e->getTraceAsString());
            die( var_dump( $e ) );
        }
    }
    protected function _initLanguage ()
    {
        try
        {
            $config = new Zend_Config_Ini( APPLICATION_PATH . '/configs/printshopcreator.ini', APPLICATION_ENV, true );
            Zend_Registry::set( 'marketid', $config->marketid );
            if ( isset( $_REQUEST['ticketid'] ) && strpos( $_SERVER['REQUEST_URI'], 'service/upload' ) !== FALSE )
                Zend_Session::setId( $_REQUEST['ticketid'] );
            if ( isset( $_REQUEST['chunkid'] ) && strpos( $_SERVER['REQUEST_URI'], 'service/upload' ) !== FALSE )
                Zend_Session::setId( $_REQUEST['chunkid'] );
            require_once ('Zend/Locale.php');
            if ( isset( $_REQUEST['speak'] ) )
            {
                $langsess = new Zend_Session_Namespace( 'lang' );
                $langsess->code = $_REQUEST['speak'];
            }
            if ( Zend_Auth::getInstance()->hasIdentity() && ! Zend_Session::namespaceIsset( 'lang' ) )
            {
                $user = Zend_Auth::getInstance()->getIdentity();
                if($user['language']) {
                	$locale = new Zend_Locale( $user['language'] );
                }else{
                	$locale = new Zend_Locale( 'de_DE' );
                }
            }
            elseif ( Zend_Session::namespaceIsset( 'lang' ) )
            {
                $lang = new Zend_Session_Namespace( 'lang' );
                $locale = new Zend_Locale( $lang->code );
            }
            else
            {
                $locale = new Zend_Locale( $this->shop->getDefaultLocale() );
            }
            
            if(!file_exists(APPLICATION_PATH . '/../cache/'.$this->shop_id.'/')) mkdir(APPLICATION_PATH . '/../cache/'.$this->shop_id.'/');

            $frontendOptions = array (
            'lifetime' => 14000,  // Lebensdauer des Caches 2 Stunden
            'automatic_serialization' => true);
            $backendOptions = array (
            'cache_dir' => APPLICATION_PATH . '/../cache/'.$this->shop_id.'/')// Verzeichnis, in welches die Cache Dateien kommen
;
            /*$backendOptions = array('host' => 'localhost',
                       'port' => 11211,
                       'persistent' => true,
                       'weight' => 1,
                       'timeout' => 5,
                       'retry_interval' => 15,
                       'status' => true,
                       'failure_callback' => '');*/

            $cache = Zend_Cache::factory( 'Core', 'File', $frontendOptions, array () );
            $fcache = Zend_Cache::factory( 'Core', 'File', $frontendOptions, $backendOptions );
            $locale->setCache( $cache );
            Zend_Registry::set( 'locale', $locale );
            Zend_Translate::setCache( $fcache );
            
            $translate = new Zend_Translate( 'TP_Translate_Adapter_Frontend_Db', APPLICATION_PATH . '/locale', $locale, array (
            'scan' => Zend_Translate::LOCALE_DIRECTORY) );
            
            $translateDb = new Zend_Translate('TP_Translate_Adapter_Db', null ,$locale->getLanguage());
            $translateDb->addTranslation('admin_translation', Zend_Registry::get('locale')->getLanguage());
            
            Zend_Registry::set( 'translate', $translateDb );
            
            Zend_Registry::set( 'Zend_Translate', $translate );
            Zend_Registry::set( 'cache', $cache );
            Zend_Registry::set( 'filecache', $fcache );
            Zend_Form::setDefaultTranslator( $translate );
        }
        catch ( Exception $e )
        {
            die( $e->getMessage() );
        }
    }
    protected function _initArticleQueues ()
    {
        $articles = array ();
        $_message = new Zend_Wildfire_Plugin_FirePhp_TableMessage( 'Articles / Queues' );
        $_message->setBuffered( true );
        $_message->setHeader( array (
        'Typ', 
        'Name', 
        'Typeid', 
        'Filename') );
        foreach ( new DirectoryIterator( APPLICATION_PATH . '/articles' ) as $file )
        {
            if ( $file == '.' || $file == '..' || $file == '.svn' || $file == '.DS_Store' )
                continue;
            require_once (APPLICATION_PATH . '/articles/' . $file->getFilename() . '/Article.php');
            eval( '$article = new ' . $file->getFilename() . '_Article();' );
            $articles[$article->id] = $file->getFilename() . '_Article';
            $_message->addRow( array (
            'Article', 
            $article->name, 
            $article->id, 
            $file->getFilename()) );
        }
        Zend_Registry::set( 'articles', $articles );
        $queues = array ();
        foreach ( new DirectoryIterator( APPLICATION_PATH . '/queues' ) as $file )
        {
            if ( $file == '.' || $file == '..' || $file == '.svn' || $file == '.DS_Store' )
                continue;
            require_once (APPLICATION_PATH . '/queues/' . $file->getFilename() . '/Queues.php');
            eval( '$queue = new ' . $file->getFilename() . '_Queues();' );
            $queues[$queue->id] = $file->getFilename() . '_Queues';
            $_message->addRow( array (
            'Queue', 
            $queue->name, 
            $queue->id, 
            $file->getFilename()) );
        }
        Zend_Registry::set( 'queues', $queues );
        
    }
    protected function _initPlugins ()
    {
        $this->bootstrap( 'FrontController' );
        $front = $this->getResource( 'FrontController' );
        $auth = Zend_Auth::getInstance();
        $cache = Zend_Registry::get( 'cache' );
        if ( ! ($acl = $cache->load( 'acl' )) )
        {
            $acl = new TP_Plugin_Acl( $auth );
            $cache->save( $acl );
        }
$acl = new TP_Plugin_Acl( $auth );
            $cache->save( $acl );
        $front->registerPlugin( new Zend_Controller_Plugin_ErrorHandler( array (
        'module' => 'default', 
        'controller' => 'error', 
        'action' => 'error') ) );
        $front->registerPlugin( new TP_Plugin_Auth( $auth, $acl ) );
        $front = Zend_Controller_Front::getInstance();
    }

    protected function getAccountTemplatePath($account_id) {
        $account = Doctrine_Query::create()->from('Account c')->where('c.id = ?', array($account_id))->fetchOne();
        if($account['template_switch'] != "") {
            return $account['template_switch'];
        }
        if($account['filiale_id'] != "" && $account['filiale_id'] != 0) {
            return $this->getAccountTemplatePath($account['filiale_id']);
        }
        return "";
    }

    protected function _initCustomRouter ()
    {
        $this->bootstrap('router');
        $router = $this->getResource( 'router' );

        $shop = Zend_Registry::get('shop');

        $dbMongo = TP_Mongo::getInstance();
        $objs = $dbMongo->Route->find(array('shop' => (string)$shop['uid']));

        foreach($objs as $obj) {
            if($obj->controller == 2) {
                $route = new Zend_Controller_Router_Route(
                    $obj->url,
                        array_merge([
                            'module' => 'default',
                            'controller' => 'cms',
                            'action' => 'index',
                        ],
                        $obj->parameter->getArrayCopy())
                    );
                $router->addRoute((string)$obj->_id, $route);
            }

            if($obj->controller == 1) {
                $route = new Zend_Controller_Router_Route(
                    $obj->url,
                        array_merge([
                            'module' => 'default',
                            'controller' => 'article',
                            'action' => 'show',
                        ],
                        $obj->parameter->getArrayCopy())
                    );
                $router->addRoute((string)$obj->_id, $route);
            }

            if($obj->controller == 3) {
                $route = new Zend_Controller_Router_Route(
                    $obj->url,
                    array_merge([
                        'module' => 'default',
                        'controller' => 'overview',
                        'action' => 'index',
                    ],
                    $obj->parameter->getArrayCopy())
                );
                $router->addRoute((string)$obj->_id, $route);
            }

            if($obj->controller == 4) {
                if(strpos($obj->url, 'basket') !== false) {

                    $basketUrl = explode("/", $obj->url);

                    $route = new Zend_Controller_Router_Route(
                        $obj->parameter->getArrayCopy()['target'],
                        array_merge([
                            'module' => 'default',
                            'controller' => 'basket',
                            'action' => $basketUrl[2]?$basketUrl[2]:'index',
                        ])
                    );
                    $router->addRoute((string)$obj->_id, $route);
                }elseif(strpos($obj->url, 'user') !== false) {
                    $basketUrl = explode("/", $obj->url);
                    $route = new Zend_Controller_Router_Route(
                        $obj->parameter->getArrayCopy()['target'],
                        array_merge([
                            'module' => 'default',
                            'controller' => 'user',
                            'action' => $basketUrl[2]?$basketUrl[2]:'index',
                        ])
                    );
                    $router->addRoute((string)$obj->_id, $route);
                }elseif(strpos($obj->url, 'impress') !== false) {
                    $route = new Zend_Controller_Router_Route(
                        $obj->parameter->getArrayCopy()['target'],
                        array_merge([
                            'module' => 'default',
                            'controller' => 'index',
                            'action' => 'impress'
                        ])
                    );
                    $router->addRoute((string)$obj->_id, $route);
                }elseif(strpos($obj->url, 'privacy') !== false) {
                    $route = new Zend_Controller_Router_Route(
                        $obj->parameter->getArrayCopy()['target'],
                        array_merge([
                            'module' => 'default',
                            'controller' => 'index',
                            'action' => 'privacy',
                        ])
                    );
                    $router->addRoute((string)$obj->_id, $route);
                }elseif(strpos($obj->url, 'agb') !== false) {
                    $route = new Zend_Controller_Router_Route(
                        $obj->parameter->getArrayCopy()['target'],
                        array_merge([
                            'module' => 'default',
                            'controller' => 'index',
                            'action' => 'agb',
                        ])
                    );
                    $router->addRoute((string)$obj->_id, $route);
                }else{
                    $route = new Zend_Controller_Router_Route(
                        $obj->url,
                        array_merge([
                            'module' => 'default',
                            'controller' => 'index',
                            'action' => 'redirect',
                        ],
                            $obj->parameter->getArrayCopy())
                    );
                    $router->addRoute((string)$obj->_id, $route);
                }
            }

        }
    }
}
