<?php

class csvcalc_article
{

    public $id = "11";
    public $articlegroups;
    public $private;
    public $versandwert;
    public $versand;
    public $netto = 0;
    public $name = "Csvcalc Product";
    public $template = "csvcalc_article";
    public $information = "Bietet Csvcalc Produkte an";
    public $backend = 'Csvcalc/config/backend.ini';
    public $frontend = 'Csvcalc/config/frontend.xml';
    public $path = 'Csvcalc';
    public $title;
    public $text;
    public $file;
    public $file1;
    public $xmltext;

    public $versandspalten,$broschspalten,$lettershopspalten,$kuvertdruckspalten,$anschreibkostenspalten,$versandlander;

    public function getNetto() {
        return $this->netto;
    }

    public function setNetto($value) {
        $this->netto = $value;
    }

    public function toArray() {
        return array('thomas' => $this->thomas, 'pdf' => $this->pdf, 'template' => $this->template, 'a1_xml' => str_replace('\\', '', $this->a1_xml), 'versand' => $this->versand, 'versandwert' => $this->versandwert, 'private' => $this->private, 'articlegroups' => $this->articlegroups, 'mwert' => $this->mwert, 'title' => $this->title, 'price' => $this->price, 'text' => $this->text, 'file' => $this->file, 'file1' => $this->file1);
    }

    public function createFrontend($view, $translate, Article $article, $ajax = false) {
        $view->view->delivery_count = 0;

        $defaultData = array();

        $product = array();
        $umfang = array();
        $farbigkeiten = array();
        $papier_select = array();
        $papier_data = array();

        $verarbeitung = array();
        $auflage = array("min" => 1000000, "max" => 0);

        $papiersorten = $this->getPapiersorten($view);
        $papierpreise = $this->getPapierpreise($view);
        $rotationen = $this->getRotationen($view);
        $papierzuschuss = $this->getPapierzuschuss($view);
        $maschinenpreise = $this->getMaschinenpreise($view);
        $sortenwechselpreise = $this->getSortenwechselpreise($view);
        $rotationslaufleistungen = $this->getRotationslaufleistungen($view);
        $farbpreise = $this->getFarbpreise($view);
        $versandpreise = $this->getVersandpreise($view);
        $broschuerenpreise = $this->getBroschuerenpreise($view);

        $selectedRow = false;

        $paletierung_value = $view->getRequest()->getParam("palettierung");
        $verarbeitung_value = $view->getRequest()->getParam("verarbeitung");
        $verpackung_value = $view->getRequest()->getParam("verpackung");
        $papier_value = $view->getRequest()->getParam("papier");
        $farben_value = $view->getRequest()->getParam("farben");
        $auflage_value = $view->getRequest()->getParam("auflage");
        $umfang_value = $view->getRequest()->getParam("umfang");
        $versand_value = $view->getRequest()->getParam("versand");

        $sortwechsel1art_value = $view->getRequest()->getParam("sortwechsel1art");
        $sortwechsel1anzahl_value = $view->getRequest()->getParam("sortwechsel1anzahl");
        $sortwechsel2art_value = $view->getRequest()->getParam("sortwechsel2art");
        $sortwechsel2anzahl_value = $view->getRequest()->getParam("sortwechsel2anzahl");
        $sortwechsel3art_value = $view->getRequest()->getParam("sortwechsel3art");
        $sortwechsel3anzahl_value = $view->getRequest()->getParam("sortwechsel3anzahl");
        $sortwechsel4art_value = $view->getRequest()->getParam("sortwechsel4art");
        $sortwechsel4anzahl_value = $view->getRequest()->getParam("sortwechsel4anzahl");

        if(!$ajax) {
            $view->view->form->addElement('hidden', 'skonto_check', array('value' => 0));
            $view->view->form->addElement('hidden', 'eroeffnungsrabatt_check', array('value' => 0));
            $view->view->form->addElement('hidden', 'fruehbucherrabatt_check', array('value' => 0));
            $view->view->form->addElement('hidden', 'zahl30_check', array('value' => 0));
            $view->view->form->addElement('hidden', 'zahl60_check', array('value' => 0));
            $view->view->form->addElement('hidden', 'zahl90_check', array('value' => 0));
        }


        $row = 0;

        $calculation = array();

        if (($handle = fopen(APPLICATION_PATH . "/design/clients/" . $view->view->shop->uid . "/artikelstamm.csv", "r")) !== FALSE) {
            while (($data = fgetcsv($handle, 0, ",", '"')) !== FALSE) {
                if($row == 0) {
                    $row++;
                    continue;
                };

                if($data['1'] == $article->id) {
                    $product[$data['3'].'_'.$data[4].'_'.$data[6]] = $data;

                    $selectedRow = $data;
                    //Zend_Debug::dump($data);
                    $umfang[$data[3]] = $data[3];
                    $farbigkeiten[$data[4]] = $data[4];

                    $papierexplode = explode(",", $data[5]);

                    foreach($papierexplode as $val) {
                        if(isset($papiersorten[trim($val)])) {
                            $papier_select[trim($val)] = $papiersorten[trim($val)][2] . ' g ' . $papiersorten[trim($val)][1];
                            $papier_data[trim($val)] = $papiersorten[trim($val)];

                            if(!isset($defaultData['papier'])) {
                                $defaultData['papier'] = trim($val);
                            }
                        }
                    }

                    $verarbeitung[$data[6]] = str_replace("_", " ", $data[6]);

                }

                if($data['1'] == $article->id && $selectedRow && $view->getRequest()->isPost()
                    && $view->getRequest()->getParam('auflage') > $data[7] && $view->getRequest()->getParam('auflage') < $data[8]
                ) {

                    $product[$data['3'].'_'.$data[4].'_'.$data[6]] = $data;

                    $selectedRow = $data;

                    $umfang[$data[3]] = $data[3];

                    $farbigkeiten[$data[4]] = $data[4];

                    $papierexplode = explode(",", $data[5]);

                    foreach($papierexplode as $val) {
                        if(isset($papiersorten[trim($val)])) {
                            $papier_select[trim($val)] = $papiersorten[trim($val)][2] . ' g ' . $papiersorten[trim($val)][1];
                            $papier_data[trim($val)] = $papiersorten[trim($val)];

                            if(!isset($defaultData['papier'])) {
                                $defaultData['papier'] = trim($val);
                            }
                        }
                    }

                    $verarbeitung[$data[6]] = str_replace("_", " ", $data[6]);

                }
            }
        }

        if(!$ajax) {
            $view->view->form->addElement('text', 'projectname');
            $view->view->form->projectname->setLabel("Projektname");


            if($selectedRow[24] == "Broschure") {

                $view->view->form->addElement('text', 'auflage');
                $view->view->form->auflage->setLabel("Auflage")
                    ->setValue(10000);

                $view->view->form->addElement('select', 'umfang');
                $view->view->form->umfang->setLabel("Umfang")
                    ->setMultiOptions(array(16 => '16 Seiten', 20 => '20 Seiten'
                    , 24 => '24 Seiten', 28 => '28 Seiten', 32 => '32 Seiten', 36 => '36 Seiten', 40 => '40 Seiten'
                    , 44 => '44 Seiten', 48 => '48 Seiten'));


                $view->view->form->addElement('select', 'farben');
                $view->view->form->farben->setLabel("Druck")
                    ->setMultiOptions(array("4/4-farbig" => "4/4-farbig"));

                $view->view->form->addElement('select', 'papier');
                $view->view->form->papier->setLabel("Papier")
                    ->setMultiOptions(array(
                        "1000_Brosch_Inhalt_matt_90" => "90.0g/m² matt",
                        "1000_Brosch_Inhalt_glanz_90" => "90.0g/m² glanz",
                        "1001_Brosch_Inhalt_matt_115" => "115.0g/m² matt",
                        "1001_Brosch_Inhalt_glanz_115" => "115.0g/m² glanz"));

                $view->view->form->addElement('select', 'papier_umschlag');
                $view->view->form->papier_umschlag->setLabel("Papier Umschlag")
                    ->setMultiOptions(array(
                        "NA" => "ohne Umschlag",
                        "1100_Brosch_Umschl_matt_170" => "170.0g/m² matt",
                        "1100_Brosch_Umschl_glanz_170" => "170.0g/m² glanz",
                        "1101_Brosch_Umschl_matt_200" => "200.0g/m² matt",
                        "1101_Brosch_Umschl_glanz_200" => "200.0g/m² glanz"));

                $view->view->form->addElement('multiCheckbox', 'umschlag_sonder');
                $view->view->form->umschlag_sonder->setLabel("")
                    ->setMultiOptions(array(
                        '1' => 'Dispersionslack auf Umschlag'

                    ));

                $view->view->form->addElement('select', 'verarbeitung');
                $view->view->form->verarbeitung->setLabel("Verarbeitung")
                    ->setMultiOptions(array("geheftet" => "geheftet"));
            }else{

                asort($umfang);

                $view->view->form->addElement('select', 'umfang');
                $view->view->form->umfang->setLabel("Umfang")
                    ->setMultiOptions($umfang);

                $view->view->form->addElement('select', 'farben');
                $view->view->form->farben->setLabel("Druck")
                    ->setMultiOptions($farbigkeiten);

                $view->view->form->addElement('select', 'papier');
                $view->view->form->papier->setLabel("Papier")
                    ->setMultiOptions($papier_select);

                $view->view->form->addElement('select', 'verarbeitung');
                $view->view->form->verarbeitung->setLabel("Verarbeitung")
                    ->setMultiOptions($verarbeitung);

                $view->view->form->addElement('select', 'sortwechsel1art');
                $view->view->form->sortwechsel1art->setLabel("Sortenwechsel 1 Art");

                $view->view->form->addElement('text', 'sortwechsel1anzahl');
                $view->view->form->sortwechsel1anzahl->setLabel("Sortenwechsel 1 Anzahl");

                $view->view->form->addElement('select', 'sortwechsel2art');
                $view->view->form->sortwechsel2art->setLabel("Sortenwechsel 2 Art");

                $view->view->form->addElement('text', 'sortwechsel2anzahl');
                $view->view->form->sortwechsel2anzahl->setLabel("Sortenwechsel 2 Anzahl");

                $view->view->form->addElement('select', 'sortwechsel3art');
                $view->view->form->sortwechsel3art->setLabel("Sortenwechsel 3 Art");

                $view->view->form->addElement('text', 'sortwechsel3anzahl');
                $view->view->form->sortwechsel3anzahl->setLabel("Sortenwechsel 3 Anzahl");

                $view->view->form->addElement('select', 'sortwechsel4art');
                $view->view->form->sortwechsel4art->setLabel("Sortenwechsel 4 Art");

                $view->view->form->addElement('text', 'sortwechsel4anzahl');
                $view->view->form->sortwechsel4anzahl->setLabel("Sortenwechsel 4 Anzahl");

                $view->view->form->addElement('text', 'auflage');
                $view->view->form->auflage->setLabel("Auflage")
                    ->setValue($auflage['min']);

            }



            $view->view->form->addElement('select', 'versand');
            $view->view->form->versand->setLabel("Versand");

            if(Zend_Auth::getInstance()->hasIdentity()) {

                if($data[18] != "NA" || $data[19] != "NA") {
                    if($selectedRow[24] == "Broschure") {
                        $view->view->form->versand->setMultiOptions(array(
                            '0' => 'Bitte wählen',
                            '1' => 'an eine Adresse',
                            '3' => 'per Lettershop',
                        ));
                    }else{
                        $view->view->form->versand->setMultiOptions(array(
                            '0' => 'Bitte wählen',
                            '1' => 'an eine Adresse',
                            '2' => 'an viele Adressen',
                            '3' => 'per Lettershop',
                        ));
                    }
                }else{
                    $view->view->form->versand->setMultiOptions(array(
                        '0' => 'Bitte wählen',
                        '1' => 'an eine Adresse',
                        '2' => 'an viele Adressen'
                    ));
                }
            }else{
                $view->view->form->versand->setMultiOptions(array(
                    '0' => 'Bitte melden Sie sich an'
                ));
            }

            $view->view->form->addElement('submit', 'send');
            $view->view->form->send->setLabel("Aktualisieren")->setAttrib("class", "btn btn btn-large btn-success");

        }


        fclose($handle);


        if($selectedRow[24] == "Broschure") {
            $data = $selectedRow;

            $temp = array();

            if(!$view->getRequest()->isPost() && !$view->getRequest()->getParam("load", false)) {

                $temp['papiergewicht'] = $papiersorten['1000_Brosch_Inhalt_matt_90'][2];
                $temp['papiergewicht_umschlag'] = 0;
                $temp['umfang_umschlag'] = 0;

                $view->view->form->umfang->setValue(16);
                $view->view->form->papier->setValue("1000_Brosch_Inhalt_matt_90");
                $view->view->form->papier_umschlag->setValue("NA");

                $temp['umfang'] = 16;
                $temp['papier'] = "1000_Brosch_Inhalt_matt_90";
                $temp['papier_umschlag'] = "NA";
                $temp['auflage'] = $view->view->form->auflage->getValue();
                $temp['farben'] = $view->view->form->farben->getValue();
            }else{

                $formData = $view->getRequest()->getPost();
                if (!$ajax && $view->view->form->isValid($formData)) {
                    $temp['papiergewicht'] = $papiersorten[$view->view->form->papier->getValue()][2];
                    $temp['papiergewicht_umschlag'] = $papiersorten[$view->view->form->papier_umschlag->getValue()][2];

                    if($view->view->form->papier_umschlag->getValue() != "NA") {
                        $temp['umfang_umschlag'] = 4;
                    }
                }else{
                    $temp['papiergewicht'] = $papiersorten[$formData['papier']][2];
                    $temp['papiergewicht_umschlag'] = $papiersorten[$formData['papier_umschlag']][2];

                    if($formData['papier_umschlag'] != "NA") {
                        $temp['umfang_umschlag'] = 4;
                    }
                }
                $temp['umfang'] = $formData['umfang'];
                $temp['papier'] = $formData['papier'];
                $temp['farben'] = $formData['farben'];
                $temp['versand'] = $formData['versand'];
                $temp['verarbeitung'] = $formData['verarbeitung'];
                $temp['projectname'] = $formData['projectname'];
                $temp['auflage'] = $formData['auflage'];
                $temp['papier_umschlag'] = $formData['papier_umschlag'];
                $versand_value = $formData['versand'];
            }

            $temp['flache_exemplar_beschnitten'] = $data[13]*($temp['umfang']+$temp['umfang_umschlag']);
            $temp['gewicht_exemplar_beschnitten'] = (
                (($data[13] * ($temp['umfang']  / 2)  / 10000) * $temp['papiergewicht']) +
                (($data[13] * ($temp['umfang_umschlag'] / 2) / 10000) * $temp['papiergewicht_umschlag'])
            );


            $temp['broschpreisRow'] = $broschuerenpreise[$data[0].'_'.$temp['umfang'].'_'.
            str_replace(array("_matt", "_glanz"), array("",""), $temp['papier']).'_'.
            str_replace(array("_matt", "_glanz"), array("",""), $temp['papier_umschlag'])];

            $temp['broschpreisSpalten'] = $this->broschspalten;

            foreach($this->broschspalten as $key => $row) {

                if($temp['auflage'] >= ($row) || $key == 39) {

                    $cd = array_reverse($temp['broschpreisRow']);

                    $temp['broschuere_preis'] = $cd[$key];
                    break;
                }
            }

            $temp['produktionsPreis'] = ($temp['broschuere_preis'] / 1000) * $temp['auflage'];

            $temp['formular'] = array(
                    'projectname' => $temp['projectname'],
                    'verarbeitung' => $temp['verarbeitung'],
                    'papier' => $temp['papier'],
                    'papier_umschlag' => $temp['papier_umschlag'],
                    'farben' => $temp['farben'],
                    'auflage' => $temp['auflage'],
                    'umfang' => $temp['umfang'],
                    'versand' => $temp['versand'],
            );
            $temp['vorgang'] = rand(100000, 999999);
            $currentAuflage = $temp['auflage'];
            $currentPapier = $papier_data[$temp['papier']];


        }else{

            if(!$view->getRequest()->isPost() && !$view->getRequest()->getParam("load", false)) {

                $minProduct = array();

                foreach($product as $key => $minData) {
                    if(!isset($minProduct[7]) || $minProduct[7] > $minData[7]) {
                        $minProduct = $minData;
                    }
                }

                $data = $minProduct;

                $view->view->form->auflage->setvalue($data[7]);
                $view->view->form->umfang->setvalue($data[3]);
                $view->view->form->farben->setvalue($data[4]);
                if(trim($data[17]) != "") {
                    $view->view->form->papier->setvalue(trim($data[17]));
                }else{
                    $view->view->form->papier->setvalue($defaultData['papier']);
                }

                $view->view->form->verarbeitung->setvalue($data[6]);
                $view->view->form->sortwechsel1anzahl->setvalue(0);
                $view->view->form->sortwechsel2anzahl->setvalue(0);
                $view->view->form->sortwechsel3anzahl->setvalue(0);
                $view->view->form->sortwechsel4anzahl->setvalue(0);
                $view->view->form->versand->setvalue(0);

                switch($data[4]) {
                    case "2/2":
                        $sortenwechselart = array(
                            "1/0" => "1/0-farbig",
                            "1/1" => "1/1-farbig",
                            "2/0" => "2/0-farbig",
                            "2/2" => "2/2-farbig"
                        );
                        break;
                    case "4/4":
                        $sortenwechselart = array(
                            "1/0" => "1/0-farbig",
                            "1/1" => "1/1-farbig",
                            "2/0" => "2/0-farbig",
                            "2/2" => "2/2-farbig",
                            "4/0" => "4/0-farbig",
                            "4/4" => "4/4-farbig"
                        );
                        break;
                    case "5/5":
                        $sortenwechselart = array(
                            "1/0" => "1/0-farbig",
                            "1/1" => "1/1-farbig",
                            "2/0" => "2/0-farbig",
                            "2/2" => "2/2-farbig",
                            "4/0" => "4/0-farbig",
                            "4/4" => "4/4-farbig",
                            "5/0" => "5/0-farbig",
                            "5/5" => "5/5-farbig"
                        );
                        break;
                }

                $view->view->form->sortwechsel1art->setMultiOptions($sortenwechselart);
                $view->view->form->sortwechsel1art->setValue("1/0");
                $view->view->form->sortwechsel2art->setMultiOptions($sortenwechselart);
                $view->view->form->sortwechsel2art->setValue("2/2");
                $view->view->form->sortwechsel3art->setMultiOptions($sortenwechselart);
                $view->view->form->sortwechsel3art->setValue("4/0");
                $view->view->form->sortwechsel4art->setMultiOptions($sortenwechselart);
                $view->view->form->sortwechsel4art->setValue("4/4");

                if(isset($view->view->form->palettierung)) {
                    $paletierung_value = $view->view->form->palettierung->getValue();
                }
                if(isset($view->view->form->verpackung)) {
                    $verpackung_value = $view->view->form->verpackung->getValue();
                }
                if(isset($view->view->form->verarbeitung)) {
                    $verarbeitung_value = $view->view->form->verarbeitung->getValue();
                }



                $papier_value = $view->view->form->papier->getValue();
                $projectname_value = $view->view->form->projectname->getValue();
                $farben_value = $view->view->form->farben->getValue();
                $auflage_value = $view->view->form->auflage->getValue();
                $umfang_value = $view->view->form->umfang->getValue();
                $versand_value = $view->view->form->versand->getValue();
                $sortwechsel1art_value = $view->view->form->sortwechsel1art->getValue();
                $sortwechsel1anzahl_value = $view->view->form->sortwechsel1anzahl->getValue();
                $sortwechsel2art_value = $view->view->form->sortwechsel2art->getValue();
                $sortwechsel2anzahl_value = $view->view->form->sortwechsel2anzahl->getValue();
                $sortwechsel3art_value = $view->view->form->sortwechsel3art->getValue();
                $sortwechsel3anzahl_value = $view->view->form->sortwechsel3anzahl->getValue();
                $sortwechsel4art_value = $view->view->form->sortwechsel4art->getValue();
                $sortwechsel4anzahl_value = $view->view->form->sortwechsel4anzahl->getValue();

                $data = $product[$umfang_value.'_'.$farben_value.'_'.$verarbeitung_value];

            }else{
                $formData = $view->getRequest()->getPost();

                if($view->getRequest()->getParam("load", false)) {
                    $formData = $view->getRequest()->getParams();
                }

                if(!$ajax) {
                    switch($view->getRequest()->getParam("farben")) {
                        case "2/2":
                            $sortenwechselart = array(
                                "1/0" => "1/0-farbig",
                                "1/1" => "1/1-farbig",
                                "2/0" => "2/0-farbig",
                                "2/2" => "2/2-farbig"
                            );
                            break;
                        case "4/4":
                            $sortenwechselart = array(
                                "1/0" => "1/0-farbig",
                                "1/1" => "1/1-farbig",
                                "2/0" => "2/0-farbig",
                                "2/2" => "2/2-farbig",
                                "4/0" => "4/0-farbig",
                                "4/4" => "4/4-farbig"
                            );
                            break;
                        case "5/5":
                        default:
                            $sortenwechselart = array(
                                "1/0" => "1/0-farbig",
                                "1/1" => "1/1-farbig",
                                "2/0" => "2/0-farbig",
                                "2/2" => "2/2-farbig",
                                "4/0" => "4/0-farbig",
                                "4/4" => "4/4-farbig",
                                "5/0" => "5/0-farbig",
                                "5/5" => "5/5-farbig"
                            );
                            break;
                    }

                    $view->view->form->sortwechsel1art->setMultiOptions($sortenwechselart);
                    $view->view->form->sortwechsel2art->setMultiOptions($sortenwechselart);
                    $view->view->form->sortwechsel3art->setMultiOptions($sortenwechselart);
                    $view->view->form->sortwechsel4art->setMultiOptions($sortenwechselart);

                    if ($view->getRequest()->getParam("versand") == 1 || $view->getRequest()->getParam("versand") == 2) {
                        $view->view->form->addElement('multiCheckbox', 'verpackung');
                        $view->view->form->verpackung->setLabel("Verpackung")
                            ->setMultiOptions(array(
                                '1' => 'In Karton'

                            ));

                        $view->view->form->addElement('select', 'palettierung');
                        $view->view->form->palettierung->setLabel("Palettierung")
                            ->setMultiOptions(array(
                                '1' => 'Euro-Tauschpaletten',
                                '2' => 'Einwegpaletten',
                            ));
                    }

                    if ($view->view->form->isValid($formData)) {

                        if(isset($view->view->form->palettierung)) {
                            $paletierung_value = $view->view->form->palettierung->getValue();
                        }
                        if(isset($view->view->form->verpackung)) {
                            $verpackung_value = $view->view->form->verpackung->getValue();
                        }
                        if(isset($view->view->form->verarbeitung)) {
                            $verarbeitung_value = $view->view->form->verarbeitung->getValue();
                        }
                        $papier_value = $view->view->form->papier->getValue();
                        $farben_value = $view->view->form->farben->getValue();
                        $auflage_value = $view->view->form->auflage->getValue();
                        $umfang_value = $view->view->form->umfang->getValue();
                        $versand_value = $view->view->form->versand->getValue();
                        $sortwechsel1art_value = $view->view->form->sortwechsel1art->getValue();
                        $sortwechsel1anzahl_value = $view->view->form->sortwechsel1anzahl->getValue();
                        $sortwechsel2art_value = $view->view->form->sortwechsel2art->getValue();
                        $sortwechsel2anzahl_value = $view->view->form->sortwechsel2anzahl->getValue();
                        $sortwechsel3art_value = $view->view->form->sortwechsel3art->getValue();
                        $sortwechsel3anzahl_value = $view->view->form->sortwechsel3anzahl->getValue();
                        $sortwechsel4art_value = $view->view->form->sortwechsel4art->getValue();
                        $sortwechsel4anzahl_value = $view->view->form->sortwechsel4anzahl->getValue();
                        $projectname_value = $view->view->form->projectname->getValue();

                        $data = $product[$umfang_value.'_'.$farben_value.'_'.$verarbeitung_value];

                        if(!isset($product[$umfang_value.'_'.$farben_value.'_'.$verarbeitung_value])) {
                            Zend_Debug::dump("Kein Datensatz im Artikelstamm gefunden: ".$article->id . ":" .$umfang_value.'_'.$farben_value.'_'.$verarbeitung_value);
                        }

                        $view->view->form->auflage
                            ->addValidator('Between', false, array('min' =>  trim($data[7]), 'max' => trim($data[8]) ));

                    }else{
                        Zend_Debug::dump($view->view->form->getErrors());
                    }
                }else{
                    $paletierung_value = $view->getRequest()->getParam("palettierung");
                    $verarbeitung_value = $view->getRequest()->getParam("verarbeitung");
                    $verpackung_value = $view->getRequest()->getParam("verpackung");
                    $papier_value = $view->getRequest()->getParam("papier");
                    $farben_value = $view->getRequest()->getParam("farben");
                    $auflage_value = $view->getRequest()->getParam("auflage");
                    $umfang_value = $view->getRequest()->getParam("umfang");
                    $versand_value = $view->getRequest()->getParam("versand");
                    $sortwechsel1art_value = $view->getRequest()->getParam("sortwechsel1art");
                    $sortwechsel1anzahl_value = $view->getRequest()->getParam("sortwechsel1anzahl");
                    $sortwechsel2art_value = $view->getRequest()->getParam("sortwechsel2art");
                    $sortwechsel2anzahl_value = $view->getRequest()->getParam("sortwechsel2anzahl");
                    $sortwechsel3art_value = $view->getRequest()->getParam("sortwechsel3art");
                    $sortwechsel3anzahl_value = $view->getRequest()->getParam("sortwechsel3anzahl");
                    $sortwechsel4art_value = $view->getRequest()->getParam("sortwechsel4art");
                    $sortwechsel4anzahl_value = $view->getRequest()->getParam("sortwechsel4anzahl");
                    $projectname_value = $view->getRequest()->getParam("projectname");

                    $data = $product[$umfang_value.'_'.$farben_value.'_'.$verarbeitung_value];
                }
            }

            /* Calculation */


            $currentPapier = $papier_data[$papier_value];
            $currentAuflage = $auflage_value;

            $papierzusch = 0;
            foreach($papierzuschuss as $row) {
                if($currentAuflage >= intval($row[0]) && $currentAuflage <= intval($row[1])) {
                    $papierzusch = $row[2];
                }
            }



            $temp = array(
                'formular' => array(
                    'projectname' => $projectname_value,
                    'palettierung' => $paletierung_value,
                    'palettierung' => $paletierung_value,
                    'verarbeitung' => $verarbeitung_value,
                    'verpackung' => $verpackung_value,
                    'papier' => $papier_value,
                    'farben' => $farben_value,
                    'auflage' => $auflage_value,
                    'umfang' => $umfang_value,
                    'versand' => $versand_value,
                    'sortwechsel1art' => $sortwechsel1art_value,
                    'sortwechsel1anzahl' => $sortwechsel1anzahl_value,
                    'sortwechsel2art' => $sortwechsel2art_value,
                    'sortwechsel2anzahl' => $sortwechsel2anzahl_value,
                    'sortwechsel3art' => $sortwechsel3art_value,
                    'sortwechsel3anzahl' => $sortwechsel3anzahl_value,
                    'sortwechsel4art' => $sortwechsel4art_value,
                    'sortwechsel4anzahl' => $sortwechsel4anzahl_value,
                ),
                'vorgang' => rand(100000, 999999),
                'auflage' => $currentAuflage,
                'zylinderumfang' => $rotationen[$data[10]][1],
                'bahnbreite' =>  $data[12],
                'abriss' => ( $data[12]*$rotationen[$data[10]][1]),
                'nutzen' => $data[11],
                'fl_ex_unb' => ( $data[12]*$rotationen[$data[10]][1]/$data[11]),
                'pg_ex_unb' => ( $data[12]*$rotationen[$data[10]][1]/$data[11])/10000*$currentPapier[2],
                'papierzuschuss' => $papierzusch,
                'papiergewicht_unb' => (((( $data[12]*$rotationen[$data[10]][1]/$data[11])/10000*$currentPapier[2]) / 1000000) * $currentAuflage * (1.0 + $papierzusch/100)),
                'papierkosten_ges' => (((( $data[12]*$rotationen[$data[10]][1]/$data[11])/10000*$currentPapier[2]) / 1000000) * $currentAuflage * (1.0 + $papierzusch/100))*$papierpreise[$currentPapier[0].'_'.$data[9]][2]

            );

            if(!isset($papierpreise[$currentPapier[0].'_'.$data[9]])) {
                Zend_Debug::dump("Papier in der Datenbank nicht gefunden: ".$currentPapier[0].'_'.$data[9]);
            }
            $temp['rotations_kosten_h'] = $maschinenpreise[$data[10].'_'.$data[9].'_'.$data[14]][3];
            $temp['einrichtungs_kosten'] = $maschinenpreise[$data[10].'_'.$data[9].'_'.$data[14]][4];
            $temp['zylinder_umdrehungen'] = $currentAuflage/$temp['nutzen'];


            $currentLaufleistung = 0;
            foreach($rotationslaufleistungen as $row) {
                if($row[0] == $data[10] && intval($row[1]) <= $temp['zylinder_umdrehungen'] && intval($row[2]) >= $temp['zylinder_umdrehungen'] ) {
                    $currentLaufleistung = $row[3];
                }
            }

            $temp['laufleistung'] = $currentLaufleistung;
            $temp['fortdruckzeit'] = $temp['zylinder_umdrehungen']/$temp['laufleistung'];
            $temp['fortdruckkosten'] = $temp['fortdruckzeit']*$temp['rotations_kosten_h'];

            $temp['papiersorten_art'] = $currentPapier[3];
            $temp['farbpreis'] = $farbpreise[$data[4].'_'.$temp['papiersorten_art']][2];
            $temp['farbkosten'] = $temp['fl_ex_unb']*$temp['farbpreis']*$currentAuflage/1000;

            $temp['verarbeitung'] = 0;
            if($data[6] != 'lose') {
                $temp['verarbeitung'] = 0.30*($currentAuflage/1000);
            }

            $temp['verpackungkosten'] = 0;
            if($verpackung_value && is_array($verpackung_value) && in_array(1, $verpackung_value)) {
                $temp['verpackungkosten'] = $currentAuflage/300;
            }


            $temp['sammelhefterlaufleistung'] = 4000;
            $temp['sammelheftereinrichtung'] = 0;
            $temp['sammelhefterstundensatz'] = 0;
            $temp['sammelhefterkosten'] = 0;

            if($data[15] != "n") {

                $sammelHefter = $maschinenpreise['Sammelhefter_'.$data[15]];

                $temp['sammelheftereinrichtung'] = $sammelHefter[4];
                $temp['sammelhefterstundensatz'] = $sammelHefter[3];
                $temp['sammelhefterkosten'] = ($currentAuflage/$temp['sammelhefterlaufleistung']*$temp['sammelhefterstundensatz'])+$temp['sammelheftereinrichtung'];
            }

            $temp['sortewechselkosten1'] = 0;

            if(intval($sortwechsel1anzahl_value) > 0) {
                $temp['sortewechselkosten1'] = $sortenwechselpreise[$sortwechsel1art_value.'_'.$data[9].'_'.$data[10]][3] * intval($sortwechsel1anzahl_value);
                if(!isset($sortenwechselpreise[$sortwechsel1art_value.'_'.$data[9].'_'.$data[10]])) {
                    Zend_Debug::dump("Sortenwechsel nicht in DB: ".$sortwechsel1art_value.'_'.$data[9].'_'.$data[10]);
                }
            }

            $temp['sortewechselkosten2'] = 0;

            if(intval($sortwechsel2anzahl_value) > 0) {
                $temp['sortewechselkosten2'] = $sortenwechselpreise[$sortwechsel2art_value.'_'.$data[9].'_'.$data[10]][3] * intval($sortwechsel2anzahl_value);
                if(!isset($sortenwechselpreise[$sortwechsel2art_value.'_'.$data[9].'_'.$data[10]])) {
                    Zend_Debug::dump("Sortenwechsel nicht in DB: ".$sortwechsel2art_value.'_'.$data[9].'_'.$data[10]);
                }
            }

            $temp['sortewechselkosten3'] = 0;

            if(intval($sortwechsel3anzahl_value) > 0) {
                $temp['sortewechselkosten3'] = $sortenwechselpreise[$sortwechsel3art_value.'_'.$data[9].'_'.$data[10]][3] * intval($sortwechsel3anzahl_value);
                if(!isset($sortenwechselpreise[$sortwechsel3art_value.'_'.$data[9].'_'.$data[10]])) {
                    Zend_Debug::dump("Sortenwechsel nicht in DB: ".$sortwechsel3art_value.'_'.$data[9].'_'.$data[10]);
                }
            }

            $temp['sortewechselkosten4'] = 0;

            if(intval($sortwechsel4anzahl_value) > 0) {
                $temp['sortewechselkosten4'] = $sortenwechselpreise[$sortwechsel4art_value.'_'.$data[9].'_'.$data[10]][3] * intval($sortwechsel4anzahl_value);
                if(!isset($sortenwechselpreise[$sortwechsel4art_value.'_'.$data[9].'_'.$data[10]])) {
                    Zend_Debug::dump("Sortenwechsel nicht in DB: ".$sortwechsel4art_value.'_'.$data[9].'_'.$data[10]);
                }
            }

            $temp['produktionsPreis'] = $temp['sammelhefterkosten'] + $temp['sortewechselkosten1'] + $temp['sortewechselkosten2'] + $temp['sortewechselkosten4'] + $temp['sortewechselkosten3'] + $temp['einrichtungs_kosten'] + $temp['papierkosten_ges'] + $temp['fortdruckkosten'] + $temp['farbkosten'] + $temp['verarbeitung'] + $temp['verpackungkosten'];

            if (!$ajax) {

            }

            $temp['gewicht_exemplar_beschnitten'] = ($data[13]/10000*$currentPapier[2]);
        }



        $view->view->gewicht_flaeche = $temp['gewicht_exemplar_beschnitten'];

        $view->view->article_stamm = $data;

        $view->view->anzahlSorten = 1 + intval($sortwechsel1anzahl_value) + intval($sortwechsel2anzahl_value) + intval($sortwechsel3anzahl_value) + intval($sortwechsel4anzahl_value);

        $basket = new TP_Basket();
        $tempProdukt = $basket->getTempProduct($article->uuid);
        $delivers = $tempProdukt->getDeliverys();

        $first_delivery_date = false;

        if($versand_value == 3) {

            $view->view->versand_teile_1 = false;
            $view->view->versand_teile_2 = false;
            $view->view->versand_teile_3 = false;

            if($data[18] != "NA") {
                $view->view->versand_teile_1 = true;
                $view->view->versand_teile_2 = true;
            }
            if($data[19] != "NA") {
                $view->view->versand_teile_3 = true;
            }

            $view->view->mailing_values = $delivers;

            $first_delivery_date = new Datetime($delivers['versand_date']);

            $mailingStamm = $this->getMailingstamm($view);
            $farbinfo = $this->getFarbinfo($view);
            $mailing = array();
            $mailing['anschreiben_seiten'] = 0;
            if($delivers['versand_teile'] == 2 || $delivers['versand_teile'] == 3) {
                $mailing['anschreiben_seiten'] = 2;
            }
            if($delivers['versand_teile'] == 1) {
                $mailing['kuvert_art'] = 'ohne_fenster';
            }
            if($delivers['versand_teile'] == 2) {
                $mailing['kuvert_art'] = 'mit_fenster';
            }
            if($delivers['versand_teile'] == 3) {
                $mailing['kuvert_art'] = 'folie';
            }

            $mailing['kuvert_format'] = $data[19];
            if($delivers['versand_teile'] != 3) {
                $mailing['kuvert_format'] = $data[18];
            }
            $mailing['lettershop_id'] = $mailingStamm[$mailing['kuvert_art'].'_'.$mailing['anschreiben_seiten'].'_'.$mailing['kuvert_format']][0];
            $mailing['gewicht_kuvert'] = $mailingStamm[$mailing['kuvert_art'].'_'.$mailing['anschreiben_seiten'].'_'.$mailing['kuvert_format']][5];
            $mailing['flaeche_seite'] = $mailingStamm[$mailing['kuvert_art'].'_'.$mailing['anschreiben_seiten'].'_'.$mailing['kuvert_format']][6];



            $kuvert_formate = array();
            $anschreiben_formate = array();

            foreach($farbinfo as $row) {
                if(strpos(' '.$row[8].' ', $mailingStamm[$mailing['kuvert_art'].'_'.$mailing['anschreiben_seiten'].'_'.$mailing['kuvert_format']][3])) {
                    $kuvert_formate[] = array('value' => $row[0], 'label' => $row[1]);
                }
                if(strpos(' '.$row[7].' ', 'A4')) {
                    $anschreiben_formate[] = array('value' => $row[0], 'label' => $row[1]);
                }
            }

            $view->view->versand_huelle_druck_data = $kuvert_formate;
            $view->view->versand_anschreib_druck_data = $anschreiben_formate;

            if($delivers['versand_anschreib'] != 1) {
                $mailing['papier_gewicht_anschreiben'] = $papiersorten[$farbinfo[$delivers['versand_anschreib_druck']][6]][2];
            }else{
                $mailing['papier_gewicht_anschreiben'] = $papiersorten[$farbinfo["unbedruckt"][6]][2];
            }

            $mailing['gewicht_anschreiben'] = $mailing['papier_gewicht_anschreiben']*($mailing['flaeche_seite']/10000)*$mailing['anschreiben_seiten'];
            $mailing['gewicht_exemplar'] = $temp['gewicht_exemplar_beschnitten'] + $mailing['gewicht_anschreiben'] + $mailingStamm[$mailing['kuvert_art'].'_'.$mailing['anschreiben_seiten'].'_'.$mailing['kuvert_format']][5];

            $temp['gewicht_exemplar'] = $mailing['gewicht_exemplar'];

            $lettershopPreise = $this->getLettershoppreise($view);

            foreach($lettershopPreise as $row) {
                if($row[0] == $mailing['lettershop_id'] && $row[1] <= $mailing['gewicht_exemplar'] && $row[2] >= $mailing['gewicht_exemplar']) {

                    foreach($this->lettershopspalten as $key => $srow) {

                        if($currentAuflage >= $srow || $srow == 5000) {

                            $row = array_reverse($row);

                            $mailing['lettershop_preise'] = $row[$key];

                            break;
                        }
                    }

                }
            }

            $mailing['lettershop_kosten'] = $mailing['lettershop_preise'] / 1000 * $currentAuflage;

            $kuvertdruckPreise = $this->getKuvertdruckpreise($view);

            $row = $kuvertdruckPreise[$mailingStamm[$mailing['kuvert_art'].'_'.$mailing['anschreiben_seiten'].'_'.$mailing['kuvert_format']][3].'_'.$delivers['versand_huelle_druck']];

            foreach($this->kuvertdruckspalten as $key => $srow) {

                if($currentAuflage >= $srow || $srow == 5000) {

                    $row = array_reverse($row);

                    $mailing['kuvert_druck_preise'] = $row[$key];

                    break;
                }
            }


            $mailing['kuvert_druck_kosten'] = $mailing['kuvert_druck_preise'] / 1000 * $currentAuflage;

            $anschreibdruckPreise = $this->getAnschreibpreise($view);
            if($delivers['versand_anschreib'] != 1) {
                $row = $anschreibdruckPreise['A4_'.$delivers['versand_anschreib_druck']];
            }else{
                $row = $anschreibdruckPreise['A4_unbedruckt'];
            }
            foreach($this->anschreibkostenspalten as $key => $srow) {

                if($currentAuflage >= $srow || $srow == 5000) {

                    $row = array_reverse($row);

                    $mailing['AnschreibenDruckKostenBlatt'] = $row[$key];

                    break;
                }
            }

            $mailing['anschreib_druck_preis'] = $mailingStamm[$mailing['kuvert_art'].'_'.$mailing['anschreiben_seiten'].'_'.$mailing['kuvert_format']][4] / 2 *$mailing['AnschreibenDruckKostenBlatt'];

            $mailing['anschreib_druck_kosten'] = $mailing['anschreib_druck_preis'] / 1000 * $currentAuflage;

            $delivery_preis = 0;

            $view->view->gewicht_site = $temp['gewicht_exemplar_beschnitten'];
            $view->view->gewicht_kuvert = $mailing['gewicht_kuvert'];
            $view->view->gewicht_anschreiben = $mailing['gewicht_anschreiben'];
            $view->view->gewicht_exemplar = $mailing['gewicht_exemplar'];

            if(!isset($delivers['versand_national'])) {
                $delivers['versand_national'] = $currentAuflage;
                $delivers['versand_innational'] = 0;

                $date = new DateTime();
                $date->modify("+2day");

                $delivers['versand_date'] = $date->format("d.m.Y");

            }

            $view->view->versand_national = $delivers['versand_national'];
            $view->view->versand_innational = $delivers['versand_innational'];
            $view->view->versand_date = $delivers['versand_date'];

            $mailing['porto_nat'] = 0;
            $mailing['porto_int'] = 0;

            if($mailing['kuvert_format'] == "C4") {
                if($mailing['gewicht_exemplar'] < 20) {
                    $mailing['porto_nat'] = 0.36 * $delivers['versand_national'];
                }elseif($mailing['gewicht_exemplar'] < 100) {
                    $mailing['porto_nat'] = (($mailing['gewicht_exemplar'] - 20) * 0.00352 + 0.36) * $delivers['versand_national'];
                }else{
                    $mailing['porto_nat'] = (($mailing['gewicht_exemplar'] - 20) * 0.00046 + 0.64) * $delivers['versand_national'];
                }

                $mailing['porto_int'] = (0.43*$delivers['versand_innational'] + (($mailing['gewicht_exemplar']*$delivers['versand_innational']*8.0)/1000.0));

            }

            if($mailing['kuvert_format'] == "C5") {
                if($mailing['gewicht_exemplar'] < 20) {
                    $mailing['porto_nat'] = 0.36 * $delivers['versand_national'];
                }elseif($mailing['gewicht_exemplar'] < 100) {
                    $mailing['porto_nat'] = (($mailing['gewicht_exemplar'] - 20) * 0.00352 + 0.36) * $delivers['versand_national'];
                }else{
                    $mailing['porto_nat'] = (($mailing['gewicht_exemplar'] - 20) * 0.00046 + 0.64) * $delivers['versand_national'];
                }

                $mailing['porto_int'] = (0.43*$delivers['versand_innational'] + (($mailing['gewicht_exemplar']*$delivers['versand_innational']*8.0)/1000.0));

            }

            if($mailing['kuvert_format'] == "A4") {
                if($mailing['gewicht_exemplar'] < 20) {
                    $mailing['porto_nat'] = 0.36 * $delivers['versand_national'];
                }elseif($mailing['gewicht_exemplar'] < 100) {
                    $mailing['porto_nat'] = (($mailing['gewicht_exemplar'] - 20) * 0.00352 + 0.36) * $delivers['versand_national'];
                }else{
                    $mailing['porto_nat'] = (($mailing['gewicht_exemplar'] - 20) * 0.00046 + 0.64) * $delivers['versand_national'];
                }

                $mailing['porto_int'] = (0.43*$delivers['versand_innational'] + (($mailing['gewicht_exemplar']*$delivers['versand_innational']*8.0)/1000.0));

            }

            if($mailing['kuvert_format'] == "A5") {
                if($mailing['gewicht_exemplar'] < 20) {
                    $mailing['porto_nat'] = 0.36 * $delivers['versand_national'];
                }elseif($mailing['gewicht_exemplar'] < 100) {
                    $mailing['porto_nat'] = (($mailing['gewicht_exemplar'] - 20) * 0.00352 + 0.36) * $delivers['versand_national'];
                }else{
                    $mailing['porto_nat'] = (($mailing['gewicht_exemplar'] - 20) * 0.00046 + 0.64) * $delivers['versand_national'];
                }

                $mailing['porto_int'] = (0.43*$delivers['versand_innational'] + (($mailing['gewicht_exemplar']*$delivers['versand_innational']*8.0)/1000.0));

            }

            if($mailing['kuvert_format'] == "DINLang") {
                if($mailing['gewicht_exemplar'] < 20) {
                    $mailing['porto_nat'] = 0.36 * $delivers['versand_national'];
                }elseif($mailing['gewicht_exemplar'] < 100) {
                    $mailing['porto_nat'] = (($mailing['gewicht_exemplar'] - 20) * 0.00352 + 0.36) * $delivers['versand_national'];
                }else{
                    $mailing['porto_nat'] = (($mailing['gewicht_exemplar'] - 20) * 0.00046 + 0.64) * $delivers['versand_national'];
                }

                $mailing['porto_int'] = (0.43*$delivers['versand_innational'] + (($mailing['gewicht_exemplar']*$delivers['versand_innational']*8.0)/1000.0));

            }

            $mailing['mailing_kosten'] = $mailing['porto_int'] + $mailing['porto_nat'] + $mailing['lettershop_kosten'] + $mailing['kuvert_druck_kosten'] + $mailing['anschreib_druck_kosten'];


        }

        $temp['address_national'] = false;
        $temp['address_international'] = false;

        if($versand_value == 1 || $versand_value == 2) {

            if(!isset($delivers['address']) && $view->getUser()) {
                $delivers['address'] = array(
                    'company' => $view->getUser()->getInvoiceAddress()->getCompany(),
                    'name' => $view->getUser()->getInvoiceAddress()->getFirstname() . ' ' . $view->getUser()->getInvoiceAddress()->getLastname(),
                    'street' => $view->getUser()->getInvoiceAddress()->getStreet() . ' ' . $view->getUser()->getInvoiceAddress()->getHouseNumber(),
                    'city' => $view->getUser()->getInvoiceAddress()->getCity(),
                    'zip' => $view->getUser()->getInvoiceAddress()->getZip(),
                    'country' => $view->getUser()->getInvoiceAddress()->getCountry()? str_replace(array("DE","AT","FR","Ö"), array("D","A","F","A"), $view->getUser()->getInvoiceAddress()->getCountry()):'D'
                );

                $tempProdukt->setDeliverys($delivers);
            }


            if($delivers['uuid'] != $article->uuid || !isset($delivers['uuid'])) {
                $delivers['uuid'] = $article->uuid;
                $delivers['data'] = array();
                $tempProdukt->setDeliverys($delivers);
            }

            $view->view->delivery_address = $delivers['address'];

            $view->view->versandlaender = $this->versandlander;

            $temp_delivers = array();
            $delivery_preis = 0;
            $auflage = 0;



            if(count($delivers['data']) == 0 && ($versand_value == 1)) {


                for($i=1; $i<=$view->view->anzahlSorten;$i++) {

                    $temp = array();

                    $temp[0] = $i;
                    $temp[1] = $i;
                    if($i==1) {
                        $temp[2] = $currentAuflage;
                    }else{
                        $temp[2] = 0;
                    }
                    $temp[3] = $delivers['address']['name'];
                    $temp[4] = $delivers['address']['street'];
                    $temp[5] = $delivers['address']['country'];
                    $temp[6] = $delivers['address']['zip'];
                    $temp[7] = $delivers['address']['city'];
                    $temp[8] = "";
                    $temp[9] = "";
                    $temp[10] = "";

                    $delivers['data'][TP_Util::uuid()] = $temp;

                }
                $tempProdukt->setDeliverys($delivers);
            }

            if(count($delivers['data']) != $view->view->anzahlSorten && ($versand_value == 1)) {

                if(count($delivers['data']) > $view->view->anzahlSorten) {
                    $delivers['data'] = array_slice($delivers['data'], 0 , $view->view->anzahlSorten);
                }

                if(count($delivers['data']) < $view->view->anzahlSorten) {
                    for($i=count($delivers['data'])+1; $i<=$view->view->anzahlSorten;$i++) {

                        $temp = array();

                        $temp[0] = $i;
                        $temp[1] = $i;
                        $temp[2] = 0;
                        $temp[3] = $delivers['address']['name'];
                        $temp[4] = $delivers['address']['street'];
                        $temp[5] = $delivers['address']['country'];
                        $temp[6] = $delivers['address']['zip'];
                        $temp[7] = $delivers['address']['city'];
                        $temp[8] = "";
                        $temp[9] = "";
                        $temp[10] = "";

                        $delivers['data'][TP_Util::uuid()] = $temp;

                    }
                }
                $tempProdukt->setDeliverys($delivers);
            }

            $delivery_converted = false;
            foreach($delivers['data'] as $key => $delivery) {
                if($delivery[9] != "") {
                    $delivery_converted = new DateTime($delivery[9]);
                }
                if(!$first_delivery_date && $delivery_converted != "" && $first_delivery_date > $delivery_converted) {
                    $first_delivery_date = $delivery_converted;
                }

                if(!isset( $temp_delivers[$delivery[1].'_'.$delivery[4].'_'.$delivery[6].'_'.$delivery[9].'_'.$delivery[5]])) {
                    $temp_delivers[$delivery[1].'_'.$delivery[4].'_'.$delivery[6].'_'.$delivery[9].'_'.$delivery[5]] = array('version' => $delivery[1], 'street' => $delivery[4], 'land' => $delivery[5], 'plz' => $delivery[6], 'auflage' => 0);
                }
                $temp_delivers[$delivery[1].'_'.$delivery[4].'_'.$delivery[6].'_'.$delivery[9].'_'.$delivery[5]]['auflage'] += $delivery[2];
                $auflage += $delivery[2];
            }

            $view->view->delivery_count = $auflage;

            $temp_delivers_g1 = array();

            foreach($temp_delivers as $key => $delivery) {

                $tempDelivery = array();
                $tempDelivery['groupByValue'] = $key;
                if($selectedRow[24] == "Broschure") {
                    $tempDelivery['gewicht_exemplar_beschnitten'] = $temp['gewicht_exemplar_beschnitten'];
                }else{
                    $tempDelivery['gewicht_exemplar_beschnitten'] = ($data[13]/10000*$currentPapier[2]);
                }
                $tempDelivery['teilauflage_gewicht'] = ($tempDelivery['gewicht_exemplar_beschnitten']*$delivery['auflage'])/1000;
                if($verpackung_value && is_array($verpackung_value) && in_array(1, $verpackung_value)) {
                    $tempDelivery['karton_anzahl'] = ceil(($delivery['auflage'] + 299)/300);
                    $tempDelivery['karton_gewicht'] = $tempDelivery['karton_anzahl']*0.2;
                    $tempDelivery['karton_kosten'] = $tempDelivery['karton_anzahl']*1;
                }else{
                    $tempDelivery['karton_anzahl'] = 0;
                    $tempDelivery['karton_gewicht'] = 0;
                    $tempDelivery['karton_kosten'] = 0;
                }
                $tempDelivery['paletten_anzahl'] = ceil(($tempDelivery['teilauflage_gewicht'] + $tempDelivery['karton_gewicht'])/700);
                $tempDelivery['paletten_gewicht'] = $tempDelivery['paletten_anzahl'] * 20;
                $tempDelivery['auflage'] = $delivery['auflage'];
                $tempDelivery['paletten_kosten'] = 0;
                if(isset($paletierung_value) && $paletierung_value == 2) {
                    $tempDelivery['paletten_kosten'] = $tempDelivery['paletten_anzahl'] * 8;
                }
                $tempDelivery['sorten_gewicht'] = $tempDelivery['teilauflage_gewicht'] + $tempDelivery['karton_gewicht'] + $tempDelivery['paletten_gewicht'];
                $tempDelivery['sorten_prod'] = $data[9];
                $tempDelivery['sorten_land'] = $delivery['land'];
                $tempDelivery['sorten_plz'] = $delivery['plz'];
                $tempDelivery['sorten_version'] = $delivery['plz'];
                $tempDelivery['sorten_street'] = $delivery['street'];



                $temp_delivers_g1[] = $tempDelivery;

                $delivery_preis += $tempDelivery['karton_kosten'];
            }

            $temp_delivers_g2 = array();

            foreach($temp_delivers_g1 as $key => $delivery) {
                if(!isset($temp_delivers_g2[$delivery['sorten_land'].'_'.$delivery['sorten_plz'].'_'.$delivery['sorten_street']])) {
                    $temp_delivers_g2[$delivery['sorten_land'].'_'.$delivery['sorten_plz'].'_'.$delivery['sorten_street']] = array('gewicht' => 0, 'street' => $delivery['sorten_street'], 'land' => $delivery['sorten_land'], 'plz' => $delivery['sorten_plz']);
                }

                $temp_delivers_g2[$delivery['sorten_land'].'_'.$delivery['sorten_plz'].'_'.$delivery['sorten_street']]['gewicht'] += $delivery['sorten_gewicht'];


            }



            foreach($temp_delivers_g2 as $key => $delivery) {
                $calc_delivery = $versandpreise[$delivery['land'].'_'.$data[9]];
                if(!isset($versandpreise[$delivery['land'].'_'.$data[9]])) {
                    if(!$ajax) {
                        Zend_Debug::dump("Keine Versandpreise für ".$delivery['land'].'_'.$data[9]." gefunden.");
                    }
                }

                if($delivery['land'] == "D" && !$temp['address_national']) {
                   $temp['address_national'] = true;
                }

                if($delivery['land'] != "D" && !$temp['address_international']) {
                    $temp['address_international'] = true;
                }



                if ($delivery['gewicht'] > 23999) {

                    foreach($calc_delivery as $cd) {

                        if(23999 < $cd[3]) {

                            foreach($this->versandspalten as $key => $row) {


                                if((strlen($delivery['plz']) == 5 && $delivery['plz'] >= ($row*10)) ||
                                    (strlen($delivery['plz']) == 4 && $delivery['plz'] >= ($row))) {

                                    $cd = array_reverse($cd);

                                    $delivery_preis += $cd[$key]*(floor($delivery['gewicht']/23999));
                                    $delivery['sorten_preis'] = $cd[$key]*(floor($delivery['gewicht']/23999));
                                    $delivery['sorten_spalte'] = $key;
                                    $delivery['sorten_row'] =  ($row*10);
                                    $delivery['sorten_row_raw'] =  ($row);

                                    break;
                                }
                            }

                            break;

                        }
                    }

                    $restGramm = $delivery['gewicht'] - (23999*(floor($delivery['gewicht']/23999)));

                    foreach($calc_delivery as $cd) {

                        if($restGramm < $cd[3]) {

                            foreach($this->versandspalten as $key => $row) {


                                if((strlen($delivery['plz']) == 5 && $delivery['plz'] >= ($row*10)) ||
                                    (strlen($delivery['plz']) == 4 && $delivery['plz'] >= ($row))) {

                                    $cd = array_reverse($cd);

                                    $delivery_preis += $cd[$key];
                                    $delivery['sorten_preis'] = $cd[$key];
                                    $delivery['sorten_spalte'] = $key;
                                    $delivery['sorten_row'] =  ($row*10);
                                    $delivery['sorten_row_raw'] =  ($row);

                                    break;
                                }
                            }

                            break;

                        }
                    }


                }else{

                    foreach($calc_delivery as $cd) {

                        if($delivery['gewicht'] < $cd[3]) {

                            foreach($this->versandspalten as $key => $row) {


                                if((strlen($delivery['plz']) == 5 && $delivery['plz'] >= ($row*10)) ||
                                   (strlen($delivery['plz']) == 4 && $delivery['plz'] >= ($row))) {

                                    $cd = array_reverse($cd);

                                    $delivery_preis += $cd[$key];
                                    $delivery['sorten_preis'] = $cd[$key];
                                    $delivery['sorten_spalte'] = $key;
                                    $delivery['sorten_row'] =  ($row*10);
                                    $delivery['sorten_row_raw'] =  ($row);

                                    break;
                                }
                            }

                            break;

                        }
                    }
                }
            }



        }

        $view->view->production_price = $temp['produktionsPreis'];
        $view->view->production_price_text = $view->view->currency->toCurrency($view->view->production_price );


        $view->view->netto_product = $view->view->production_price * 1;
        $view->view->netto_product_text = $view->view->currency->toCurrency($view->view->production_price * 1);

        $view->view->anschreib_kosten_raw = 0;
        $view->view->kuvert_kosten_raw = 0;
        $view->view->letter_kosten_raw = 0;
        $view->view->porto_kosten_raw = 0;

        if($mailing) {
            $view->view->anschreib_kosten_raw = $mailing['anschreib_druck_kosten'];
            $view->view->anschreib_kosten_text = $view->view->currency->toCurrency($mailing['anschreib_druck_kosten']);
            $view->view->kuvert_kosten_raw = $mailing['kuvert_druck_kosten'];
            $view->view->kuvert_kosten_text = $view->view->currency->toCurrency($mailing['kuvert_druck_kosten']);
            $view->view->letter_kosten_raw = $mailing['lettershop_kosten'];
            $view->view->letter_kosten_raw_text = $view->view->currency->toCurrency($mailing['lettershop_kosten']);

            $view->view->porto_kosten_raw = $mailing['porto_nat'] + $mailing['porto_int'];
            $view->view->porto_kosten_raw_text = $view->view->currency->toCurrency($mailing['porto_nat'] + $mailing['porto_int']);

	    $view->view->production_price = $view->view->production_price + $mailing['porto_nat'] + $mailing['porto_int'] + $mailing['lettershop_kosten'] + $mailing['kuvert_druck_kosten'] + $mailing['anschreib_druck_kosten'];
   
        }


        $view->view->delivery_price   = $delivery_preis;
        $view->view->delivery_price_text = $view->view->currency->toCurrency($delivery_preis);



        $view->view->netto_product_delivery = $view->view->production_price  + $delivery_preis * 1;
        $view->view->netto_product_delivery_text = $view->view->currency->toCurrency($view->view->production_price  + $delivery_preis * 1);

        $view->view->skonto_raw = 0;
        $view->view->eroeffnungsrabatt_raw = 0;
        $view->view->fruehbucherrabatt_raw = 0;
        $view->view->zahl30_raw = 0;
        $view->view->zahl60_raw = 0;
        $view->view->zahl90_raw = 0;



        $temp['Dauer_Zahlungssicherung'] = 0;
        $temp['Dauer_Zahlungsziel'] = 0;

        if($view->getRequest()->getParam("eroeffnungsrabatt_check")) {
            $view->view->eroeffnungsrabatt_raw = (($view->view->netto_product+$view->view->kuvert_kosten_raw+$view->view->anschreib_kosten_raw)/100*3)*-1;
            $view->view->production_price = $view->view->production_price-(($view->view->netto_product+$view->view->kuvert_kosten_raw+$view->view->anschreib_kosten_raw)/100*3);
            $temp['eroeffnungsrabatt'] = array(
                'raw' => $view->view->eroeffnungsrabatt_raw
            );
        }
        if($view->getRequest()->getParam("fruehbucherrabatt_check")) {
            $view->view->fruehbucherrabatt_raw = (($view->view->netto_product+$view->view->kuvert_kosten_raw+$view->view->anschreib_kosten_raw)/100*2)*-1;
            $view->view->production_price = $view->view->production_price-(($view->view->netto_product+$view->view->kuvert_kosten_raw+$view->view->anschreib_kosten_raw)/100*2);
            $temp['fruehbucherrabatt'] = array(
                'raw' => $view->view->fruehbucherrabatt_raw
            );
        }
        if($view->getRequest()->getParam("skonto_check")) {
            $view->view->skonto_raw = (($view->view->production_price-$mailing['porto_nat']-$mailing['porto_int'])/100*2)*-1;
            $view->view->production_price = $view->view->production_price-(($view->view->production_price-$mailing['porto_nat']-$mailing['porto_int'])/100*2);
            $temp['skonto'] = array(
                'raw' => $view->view->skonto_raw
            );
        }
        if($view->getRequest()->getParam("zahl30_check")) {
            $temp['zahl30'] = array(
                'raw' => 0
            );
            $temp['Dauer_Zahlungssicherung'] = 2;
            $temp['Dauer_Zahlungsziel'] = 30;
        }
        if($view->getRequest()->getParam("zahl60_check")) {
            $view->view->zahl60_raw = ($view->view->production_price/100*1);
            $view->view->production_price = $view->view->production_price+($view->view->production_price/100*1);
            $temp['zahl60'] = array(
                'raw' => $view->view->zahl60_raw
            );
            $temp['Dauer_Zahlungssicherung'] = 2;
            $temp['Dauer_Zahlungsziel'] = 60;
        }
        if($view->getRequest()->getParam("zahl90_check")) {
            $view->view->zahl90_raw= ($view->view->production_price/100*2);
            $view->view->production_price = $view->view->production_price+($view->view->production_price/100*2);
            $temp['zahl90'] = array(
                'raw' => $view->view->zahl90_raw
            );
            $temp['Dauer_Zahlungssicherung'] = 2;
            $temp['Dauer_Zahlungsziel'] = 90;
        }

        $view->view->skonto_text = $view->view->currency->toCurrency($view->view->skonto_raw);
        $view->view->eroeffnungsrabatt_text = $view->view->currency->toCurrency($view->view->eroeffnungsrabatt_raw);
        $view->view->fruehbucherrabatt_text = $view->view->currency->toCurrency($view->view->fruehbucherrabatt_raw);
        $view->view->zahl30_text = $view->view->currency->toCurrency($view->view->zahl30_raw);
        $view->view->zahl60_text = $view->view->currency->toCurrency($view->view->zahl60_raw);
        $view->view->zahl90_text = $view->view->currency->toCurrency($view->view->zahl90_raw);

        $temp['Dauer_Papiereinkauf'] = 5;
        if($view->view->article_stamm[17] == $currentPapier[0] || ($view->view->article_stamm[17] == "" && $currentPapier[0] == "1000_Brosch_Inhalt_matt_90")) {
            $temp['Dauer_Papiereinkauf'] = 0;
        }

        $temp['Dauer_Druck'] = 3;

        if(($versand_value == 1 || $versand_value == 2) && $temp['address_national']) {
            $temp['Dauer_Versand_National'] = 2;
        }

        if(($versand_value == 1 || $versand_value == 2) && $temp['address_international']) {
            $temp['Dauer_Versand_International'] = 3;
        }

        $temp['Dauer_Versand_Lettershop'] = 0;
        if($versand_value == 3) {
            $temp['Dauer_Versand_Lettershop'] = 3;
        }
        $temp['Dauer_Versand_Auslieferung_Post'] = 0;
        if($versand_value == 3) {
            $temp['Dauer_Versand_Auslieferung_Post'] = 1;
        }
        $temp['Dauer_Versand_Mailing'] = $temp['Dauer_Versand_Lettershop'] + $temp['Dauer_Versand_Auslieferung_Post'];


        $temp['Dauer_Versand'] = $temp['Dauer_Versand_National'];

        if($temp['Dauer_Versand_International'] > $temp['Dauer_Versand_National']) {
            $temp['Dauer_Versand'] = $temp['Dauer_Versand_International'];
        }

        if($temp['Dauer_Versand_Mailing'] > $temp['Dauer_Versand_International'] && $temp['Dauer_Versand_Mailing'] > $temp['Dauer_Versand_National']) {
            $temp['Dauer_Versand'] = $temp['Dauer_Versand_Mailing'];
        }

        $temp['Dauer_Gesamt'] = $temp['Dauer_Druck'] + $temp['Dauer_Papiereinkauf'] + $temp['Dauer_Zahlungssicherung'] + $temp['Dauer_Versand'];

        if($view->getRequest()->getParam("fruehbucherrabatt_check")) {
            $temp['Termin_frueheste_Lieferung'] = $this->getFruehsterTermin($view, 30);
        }else{
            $temp['Termin_frueheste_Lieferung'] = $this->getFruehsterTermin($view, $temp['Dauer_Gesamt']);
        }

        $temp['Termin_Zahlungsziel'] = $this->getFruehsterTermin($view, $temp['Dauer_Zahlungsziel']);

        $temp['Termin_Datenlieferung'] = $this->getDatenTermin($view, $first_delivery_date, $temp['Dauer_Versand'] + $temp['Dauer_Druck']);

        $temp['Termin_Bestellung'] = new Datetime();

        $temp['article_stamm'] = $view->view->article_stamm;

        $tempProdukt->setCalcValues($temp);

        if(Zend_Auth::getInstance()->hasIdentity() && !$ajax) {
            $user = Zend_Auth::getInstance()->getIdentity();
            if($user['show_calc'] == 1) {
                Zend_Debug::dump($temp);
                Zend_Debug::dump($temp_delivers);
            }
        }

        return $view->view->production_price  + $delivery_preis * 1;

    }


    private function getPapiersorten($view) {
        $papiersorten = array();

        $row = 0;

        if (($handle = fopen(APPLICATION_PATH . "/design/clients/" . $view->view->shop->uid . "/papiersorten.csv", "r")) !== FALSE) {
            while (($data = fgetcsv($handle, 0, ",", '"')) !== FALSE) {

                if($row == 0) {
                    $row++;
                    continue;
                };

                $papiersorten[$data[0]] = $data;
            }
        }

        return $papiersorten;
    }

    private function getRotationen($view) {
        $rotationen = array();

        $row = 0;

        if (($handle = fopen(APPLICATION_PATH . "/design/clients/" . $view->view->shop->uid . "/rotationen.csv", "r")) !== FALSE) {
            while (($data = fgetcsv($handle, 0, ",", '"')) !== FALSE) {

                if($row == 0) {
                    $row++;
                    continue;
                };

                $rotationen[$data[0]] = $data;
            }
        }

        return $rotationen;
    }

    private function getPapierzuschuss($view)
    {
        $papierzuschuss = array();

        $row = 0;

        if (($handle = fopen(APPLICATION_PATH . "/design/clients/" . $view->view->shop->uid . "/papierzuschuss.csv", "r")) !== FALSE) {
            while (($data = fgetcsv($handle, 0, ",", '"')) !== FALSE) {

                if($row == 0) {
                    $row++;
                    continue;
                };

                $papierzuschuss[] = $data;
            }
        }

        return $papierzuschuss;
    }

    private function getPapierpreise($view)
    {
        $papierpreise = array();

        $row = 0;

        if (($handle = fopen(APPLICATION_PATH . "/design/clients/" . $view->view->shop->uid . "/papierpreise.csv", "r")) !== FALSE) {
            while (($data = fgetcsv($handle, 0, ",", '"')) !== FALSE) {

                if($row == 0) {
                    $row++;
                    continue;
                };

                $papierpreise[$data[0].'_'.$data[1]] = $data;
            }
        }

        return $papierpreise;
    }

    private function getMaschinenpreise($view)
    {
        $maschinenpreise = array();

        $row = 0;

        if (($handle = fopen(APPLICATION_PATH . "/design/clients/" . $view->view->shop->uid . "/maschinenpreise.csv", "r")) !== FALSE) {
            while (($data = fgetcsv($handle, 0, ",", '"')) !== FALSE) {

                if($row == 0) {
                    $row++;
                    continue;
                };

                $maschinenpreise[$data[0].'_'.$data[1]] = $data;
                $maschinenpreise[$data[0].'_'.$data[1].'_'.$data[2]] = $data;
                $maschinenpreise[$data[0].'_'.$data[2]] = $data;
            }
        }

        return $maschinenpreise;
    }

    private function getSortenwechselpreise($view)
    {
        $swp = array();

        $row = 0;

        if (($handle = fopen(APPLICATION_PATH . "/design/clients/" . $view->view->shop->uid . "/sortenwechselpreise.csv", "r")) !== FALSE) {
            while (($data = fgetcsv($handle, 0, ",", '"')) !== FALSE) {

                if($row == 0) {
                    $row++;
                    continue;
                };

                $swp[$data[0].'_'.$data[2].'_'.$data[1]] = $data;
            }
        }

        return $swp;
    }

    private function getRotationslaufleistungen($view)
    {
        $rll = array();

        $row = 0;

        if (($handle = fopen(APPLICATION_PATH . "/design/clients/" . $view->view->shop->uid . "/rotationlaufleistungen.csv", "r")) !== FALSE) {
            while (($data = fgetcsv($handle, 0, ",", '"')) !== FALSE) {

                if($row == 0) {
                    $row++;
                    continue;
                };

                $rll[] = $data;
            }
        }

        return $rll;
    }

    private function getFarbpreise($view)
    {
        $rll = array();

        $row = 0;

        if (($handle = fopen(APPLICATION_PATH . "/design/clients/" . $view->view->shop->uid . "/farbpreise.csv", "r")) !== FALSE) {
            while (($data = fgetcsv($handle, 0, ",", '"')) !== FALSE) {

                if($row == 0) {
                    $row++;
                    continue;
                };

                $rll[$data[0].'_'.$data[1]] = $data;
            }
        }

        return $rll;
    }

    private function getFarbinfo($view)
    {
        $rll = array();

        $row = 0;

        if (($handle = fopen(APPLICATION_PATH . "/design/clients/" . $view->view->shop->uid . "/farbinfo.csv", "r")) !== FALSE) {
            while (($data = fgetcsv($handle, 0, ",", '"')) !== FALSE) {

                if($row == 0) {
                    $row++;
                    continue;
                };

                $rll[$data[0]] = $data;
            }
        }

        return $rll;
    }

    private function getVersandpreise($view)
    {
        $rll = array();
        $this->versandlander = array();
        $row = 0;

        if (($handle = fopen(APPLICATION_PATH . "/design/clients/" . $view->view->shop->uid . "/versandpreise.csv", "r")) !== FALSE) {
            while (($data = fgetcsv($handle, 0, ",", '"')) !== FALSE) {

                if($row == 0) {
                    $this->versandspalten = array_reverse($data);
                    $row++;
                    continue;
                };

                if(!isset($rll[$data[1].'_'.$data[2]])) {
                    $rll[$data[1].'_'.$data[2]] = array();
                }
                $this->versandlander[$data[1]] = $data[1];
                $rll[$data[1].'_'.$data[2]][] = $data;
            }
        }

        return $rll;
    }

    private function getMailingstamm($view)
    {
        $rll = array();

        $row = 0;

        if (($handle = fopen(APPLICATION_PATH . "/design/clients/" . $view->view->shop->uid . "/mailingstamm.csv", "r")) !== FALSE) {
            while (($data = fgetcsv($handle, 0, ",", '"')) !== FALSE) {

                if($row == 0) {
                    $row++;
                    continue;
                };

                $rll[$data[2].'_'.$data[4].'_'.$data[3]] = $data;
            }
        }

        return $rll;
    }

    private function getLettershoppreise($view)
    {
        $rll = array();

        $row = 0;

        if (($handle = fopen(APPLICATION_PATH . "/design/clients/" . $view->view->shop->uid . "/lettershoppreise.csv", "r")) !== FALSE) {
            while (($data = fgetcsv($handle, 0, ",", '"')) !== FALSE) {

                if($row == 0) {
                    $this->lettershopspalten = array_reverse($data);
                    $row++;
                    continue;
                };

                $rll[] = $data;
            }
        }

        return $rll;
    }

    private function getKuvertdruckpreise($view)
    {
        $rll = array();

        $row = 0;

        if (($handle = fopen(APPLICATION_PATH . "/design/clients/" . $view->view->shop->uid . "/kuvertdruckpreise.csv", "r")) !== FALSE) {
            while (($data = fgetcsv($handle, 0, ",", '"')) !== FALSE) {

                if($row == 0) {
                    $this->kuvertdruckspalten = array_reverse($data);
                    $row++;
                    continue;
                };

                $rll[$data[0].'_'.$data[1]] = $data;
            }
        }

        return $rll;
    }

    private function getAnschreibpreise($view)
    {
        $rll = array();

        $row = 0;

        if (($handle = fopen(APPLICATION_PATH . "/design/clients/" . $view->view->shop->uid . "/anschreibendruckpreise.csv", "r")) !== FALSE) {
            while (($data = fgetcsv($handle, 0, ",", '"')) !== FALSE) {

                if($row == 0) {
                    $this->anschreibkostenspalten = array_reverse($data);
                    $row++;
                    continue;
                };

                $rll[$data[0].'_'.$data[1]] = $data;
            }
        }

        return $rll;
    }

    private function getBroschuerenpreise($view)
    {
        $rll = array();

        $row = 0;

        if (($handle = fopen(APPLICATION_PATH . "/design/clients/" . $view->view->shop->uid . "/broschurepreise.csv", "r")) !== FALSE) {
            while (($data = fgetcsv($handle, 0, ",", '"')) !== FALSE) {
                if($row == 0) {
                    $this->broschspalten = array_reverse($data);
                    $row++;
                    continue;
                };

                $rll[$data[0].'_'.$data[2].'_'.$data[3].'_'.$data[4]] = $data;
            }
        }

        return $rll;
    }

    private function checkIfRichText($value) {

        if($value instanceof PHPExcel_RichText) {
            return $value->getPlainText();
        }

        return $value;
    }

    private function getFruehsterTermin($view, $int)
    {
        $obj = new TP_Delivery(date('Y-m-d'));
        return $obj->getDeliveryDate($int);
    }

    private function getDatenTermin($view, $first_delivery_date, $param)
    {
        if(!$first_delivery_date || $first_delivery_date == "") {
            return new DateTime();
        }
        $obj = new TP_Delivery($first_delivery_date->format("Y-m-d"));
        return $obj->getDeliveryDateBack($param);
    }

}
