<?php

class freedescription_article {
	
	public $id = "7";

	public $netto = 0;
	
	public $name = "Freie Produktbeschreibung";
	
	public $template = "freedescription_article";
		
	public $information = "Bietet normale Produkte an";
		
	public $backend = 'Freedescription/config/backend.ini';
	
	public $frontend = 'Freedescription/config/frontend.xml';
	
	public $path = 'Freedescription';
	
	public function getNetto() {
		return $this->netto;
	}
	
	public function setNetto($value) {
		$this->netto = $value;
	}
	
	public function createFrontend($view, $translate, Article $article, $ajax = false) {

            if($article->a1_xml != '') {
              $calc = new TP_Calc(null, str_replace('\\', '', $article->a1_xml), $ajax);
            }else{
              $calc = new TP_Calc(APPLICATION_PATH . '/articles/' . $this->frontend, null, $ajax);
            }
		
            $this->netto = $calc->parse($view);
            if(!$view->getRequest()->getParam('special')) {
                $view->view->form->addElement('submit', 'send', array('label' => $translate->translate('Calc')));
            }
            if($article->special == true) {
                $view->view->form->addElement('submit', 'special', array('label' => $translate->translate('Special')));
            }
            return $this->netto;
       }
}
