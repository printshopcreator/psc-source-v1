<?php

class market_article
{

    public $id = "6";
    public $articlegroups;
    public $private;
    public $versandwert;
    public $versand;
    public $netto = 0;
    public $name = "Main Product";
    public $template = "market_article";
    public $information = "Bietet Markt Produkte an";
    public $backend = 'Market/config/backend.ini';
    public $backend_market = 'Market/config/backend_market.ini';
    public $frontend = 'Market/config/frontend.xml';
    public $path = 'Market';
    public $title;
    public $text;
    public $file;
    public $file1;
    public $xmltext;

    public function getNetto()
    {
        return $this->netto;
    }

    public function setNetto($value)
    {
        $this->netto = $value;
    }

    public function toArray()
    {
        return array('thomas' => $this->thomas, 'pdf' => $this->pdf, 'template' => $this->template, 'a1_xml' => str_replace('\\', '', $this->a1_xml), 'versand' => $this->versand, 'versandwert' => $this->versandwert, 'private' => $this->private, 'articlegroups' => $this->articlegroups, 'mwert' => $this->mwert, 'title' => $this->title, 'price' => $this->price, 'text' => $this->text, 'file' => $this->file, 'file1' => $this->file1);
    }

    public function createFrontend($view, $translate, Article $article, $ajax = false)
    {

        $view->view->upload_possible = array();

        $upload = array();

        if ($article->a6_org_article != 0) {

            if ($article->OrgArticle->upload_article) {
                $upload['article'] = 'Jetzt Hochladen';
                $view->view->upload_possible[] = 'article';
            }
            if ($article->OrgArticle->upload_center) {
                $upload['center'] = 'Upload im Uploadcenter';
                $view->view->upload_possible[] = 'center';
            }
            if ($article->OrgArticle->upload_weblayouter) {
                $upload['weblayouter'] = 'jetzt Online Gestalten';
                $view->view->upload_possible[] = 'online';
            }
            if ($article->OrgArticle->upload_post) {
                $upload['post'] = 'Per Post';
                $view->view->upload_possible[] = 'post';
            }
            if ($article->OrgArticle->upload_email) {
                $upload['email'] = 'Per E-Mail';
                $view->view->upload_possible[] = 'mail';
            }
            if ($article->OrgArticle->upload_templateprint) {
                $upload['templateprint'] = 'Per Templateprint';
                $view->view->upload_possible[] = 'templateprint';
            }

            if ($article->OrgArticle->upload_steplayouter) {
                $upload['steplayouter'] = 'Per Steplayouter';
                $view->view->upload_possible[] = 'steplayouter';
            }

            if ($article->OrgArticle->upload_steplayouter2) {
                $upload['steplayouter2'] = 'Per Steplayouter';
                $view->view->upload_possible[] = 'steplayouter2';
            }

            if ($article->OrgArticle->upload_collecting_orders) {
                $upload['collecting_orders'] = 'Sammelbestellung';
                $view->view->upload_possible[] = 'collecting_orders';
            }

            if ($article->OrgArticle->upload_multi) {
                $upload['multi'] = 'Einfacher Multiupload';
                $view->view->upload_possible[] = 'multi';
            }
        } else {

            if ($article->upload_article) {
                $upload['article'] = 'Jetzt Hochladen';
                $view->view->upload_possible[] = 'article';
            }
            if ($article->upload_center) {
                $upload['center'] = 'Upload im Uploadcenter';
                $view->view->upload_possible[] = 'center';
            }
            if ($article->upload_weblayouter) {
                $upload['weblayouter'] = 'jetzt Online Gestalten';
                $view->view->upload_possible[] = 'online';
            }
            if ($article->upload_post) {
                $upload['post'] = 'Per Post';
                $view->view->upload_possible[] = 'post';
            }
            if ($article->upload_email) {
                $upload['email'] = 'Per E-Mail';
                $view->view->upload_possible[] = 'mail';
            }
            if ($article->upload_templateprint) {
                $upload['templateprint'] = 'Per Templateprint';
                $view->view->upload_possible[] = 'templateprint';
            }
            if ($article->upload_steplayouter) {
                $upload['steplayouter'] = 'Per Steplayouter';
                $view->view->upload_possible[] = 'steplayouter';
            }
            if ($article->upload_steplayouter2) {
                $upload['steplayouter2'] = 'Per Steplayouter';
                $view->view->upload_possible[] = 'steplayouter2';
            }
            if ($article->upload_collecting_orders) {
                $upload['collecting_orders'] = 'Sammelbestellung';
                $view->view->upload_possible[] = 'collecting_orders';
            }
            if ($article->upload_multi) {
                $upload['multi'] = 'Einfacher Multiupload';
                $view->view->upload_possible[] = 'multi';
            }
            if ($article->isUploadProvided()) {
                $upload['provided'] = 'Provided';
                $view->view->upload_possible[] = 'provided';
            }
        }
        if ($view->getRequest()->getParam('auflage', false)) {
            $view->getRequest()->setParam('auflage', str_replace(".", "", $view->getRequest()->getParam('auflage', false)));
            $view->getRequest()->setPost('auflage', str_replace(".", "", $view->getRequest()->getParam('auflage', false)));
        }
        if (!$view->getRequest()->getParam('upload_mode', false)) {
            $keys = array_keys($upload);
            if (isset($keys[0])) {

                if ($article->not_edit == 1) {
                    $_POST['upload_mode'] = 'article';
                    $view->getRequest()->setParam('upload_mode', 'article');
                } elseif(count($upload) == 1 && ($view->view->upload_possible[0] == 'provided' || $view->view->upload_possible[0] == 'center' || $view->view->upload_possible[0] == 'mail' || $view->view->upload_possible[0] == 'post')) {
                    $_POST['upload_mode'] = $view->view->upload_possible[0];
                    $view->getRequest()->setParam('upload_mode', $view->view->upload_possible[0]);
                }else {
                    $_POST['upload_mode'] = 'none';
                    $view->getRequest()->setParam('upload_mode', 'none');
                }

                if ($view->getRequest()->getParam('layouter', false) || $view->getRequest()->getParam('openlayouter', false)) {
                    $view->getRequest()->setParam('upload_mode', 'steplayouter2');
                    $_POST['upload_mode'] = 'steplayouter2';
                }
                if ($view->getRequest()->getParam('openlayouter', false)) {
                    $view->view->openlayouter = true;
                }
            }
        }

        if (!$ajax) {
            $view->view->upload_mode_form = new Zend_Form();
            $view->view->upload_mode_form->addElement('radio', 'upload_mode_select');
            $view->view->upload_mode_form->upload_mode_select->setLabel('Datenübertragung')
                ->setMultiOptions($upload)
                ->removeDecorator('label')->removeDecorator('HtmlTag')
                ->setAttrib('onChange', 'this.form.submit()');

            $view->view->upload_mode_form->valid();

            $view->view->form->addElement('hidden', 'upload_mode');
            $view->view->form->upload_mode
                ->removeDecorator('label')->removeDecorator('HtmlTag');

            if ($view->getRequest()->getParam('upload_mode')) {
                $view->view->upload_mode = $view->getRequest()->getParam('upload_mode');

                $basket = new TP_Basket();
                $articleBasket = $basket->getTempProduct($article->uuid);
                $articleBasket->setUploadMode($view->view->upload_mode);
                $view->view->upload_mode_form->upload_mode_select->setValue($view->view->upload_mode);
            } else {
                $keys = array_keys($upload);
                if (isset($keys[0])) {
                    $view->view->upload_mode = $keys[0];

                    $basket = new TP_Basket();
                    $articleBasket = $basket->getTempProduct($article->uuid);
                    $articleBasket->setUploadMode($keys[0]);
                    $view->view->form->upload_mode->setValue($keys[0]);
                    $view->view->upload_mode_form->upload_mode_select->setValue($keys[0]);
                } else {
                    $view->view->upload_mode = null;

                    $basket = new TP_Basket();
                    $articleBasket = $basket->getTempProduct($article->uuid);
                    $articleBasket->setUploadMode(null);
                    $view->view->form->upload_mode->setValue(null);
                    $view->view->upload_mode_form->upload_mode_select->setValue(null);
                }
            }
        } else {

            if ($view->getRequest()->getParam('upload_mode')) {
                $view->view->upload_mode = $view->getRequest()->getParam('upload_mode');

                $basket = new TP_Basket();
                $articleBasket = $basket->getTempProduct($article->uuid);
                $articleBasket->setUploadMode($view->view->upload_mode);

                $view->view->form[] = array('type' => 'radio', 'options' => $upload, 'value' => $view->view->upload_mode, 'label' => $translate->translate('Datenübertragung'), 'name' => 'upload_mode');
            } else {
                $keys = array_keys($upload);
                if (isset($keys[0])) {
                    $view->view->upload_mode = $keys[0];

                    $basket = new TP_Basket();
                    $articleBasket = $basket->getTempProduct($article->uuid);
                    $articleBasket->setUploadMode($keys[0]);

                    $view->view->form[] = array('type' => 'radio', 'options' => $upload, 'value' => $keys[0], 'label' => $translate->translate('Datenübertragung'), 'name' => 'upload_mode');
                } else {
                    $view->view->form[] = array('type' => 'radio', 'options' => $upload, 'value' => null, 'label' => $translate->translate('Datenübertragung'), 'name' => 'upload_mode');
                }
            }
        }

        if ($view->getRequest()->getParam('openlayouter', false)) {
            $view->view->openlayouter = true;
        }

        if ($article->a1_xml != '') {
            if ($article->a6_org_article != '0' || $article->a6_org_article > 0) {
                $org = Doctrine_Core::getTable('Article')->find($article->a6_org_article);
                $motiv_group = $org->motiv_group;
                $calc = new TP_Calc(null, str_replace('\\', '', $org->a1_xml), $ajax);
            } else {
                $motiv_group = $article->motiv_group;
                $calc = new TP_Calc(null, str_replace('\\', '', $article->a1_xml), $ajax);
            }
        } else {
            if ($article->a6_org_article != '0' || $article->a6_org_article > 0) {
                $org = Doctrine_Core::getTable('Article')->find($article->a6_org_article);
                $motiv_group = $org->motiv_group;
                $calc = new TP_Calc(null, str_replace('\\', '', $org->a1_xml), $ajax);
            } else {
                $motiv_group = $article->motiv_group;
                $calc = new TP_Calc(APPLICATION_PATH . '/articles/' . $this->frontend, null, $ajax);
            }
        }

        if ($motiv_group == 0) {
            $motiv_group = 1;
        }

        $user = false;
        if (Zend_Auth::getInstance()->hasIdentity()) {
            $user = Zend_Auth::getInstance()->getIdentity();
        }

        $basket = new TP_Basket();

        $articleBasket = $basket->getTempProduct($article->uuid);
        $articleBasket->setResalePrice($article->a6_resale_price);
        $articleBasket->clearMotive();

        if ($view->getRequest()->getParam('layouter', false)) {
            $articleBasket->setLayouterId($view->getRequest()->getParam('layouter', false));
        }

        $files = $articleBasket->getFiles();
        $max = 0;
        if (count($files) > 0) {
            $max = 0;
            foreach ($files as $file) {
                $finfo = new finfo(FILEINFO_MIME_TYPE);
                $finfo = $finfo->file('uploads/' . $view->view->shop->uid . '/article/' . $file['value']);
                if ($finfo == "application/pdf") {
                    $p = new pdflib();
                    $indoc = $p->open_pdi_document('uploads/' . $view->view->shop->uid . '/article/' . $file['value'], "");
                    $max = $max + $p->pcos_get_number($indoc, "length:pages");
                } else {
                    $max += 1;
                }
            }

            $view->getRequest()->setPost('layouter_seiten', $max);
            $view->getRequest()->setParam('layouter_seiten', $max);
            $param['layouter_seiten'] = $max;
            $articleBasket->setUploadSeiten($max);
        } else {

            $dom = new DOMDocument();
            if (isset($view->view->layouterPreviewId) && $view->view->layouterPreviewId != "") {
                $articleSession = new TP_Layoutersession();

                $articleSess = $articleSession->getLayouterArticle($view->view->layouterPreviewId);

                $dom->loadXML($articleSess->getPageObjects());
            } else {
                if (!file_exists(APPLICATION_PATH . '/../market/products/' . $article->a6_directory . '/pageobjects.xml')) {
                    $this->createPreDispatch($article);
                }

                $dom->load(APPLICATION_PATH . '/../market/products/' . $article->a6_directory . '/pageobjects.xml');
            }

            $xpath = new DOMXPath($dom);
            $seiten = $xpath->query("//root/settings/netto_pages");
            $param = array();
            if ($seiten->length > 0) {
                $seiten = (int) $seiten->item(0)->nodeValue;
                $view->getRequest()->setPost('layouter_seiten', $seiten);
                $view->getRequest()->setParam('layouter_seiten', $seiten);
                $param['layouter_seiten'] = $seiten;
            }
            $motive = $xpath->query('//root/item[contains(objType,"Image")]')->length;

            $view->getRequest()->setPost('motive', $motive);
            $view->getRequest()->setParam('motive', $motive);

            $motiveCalc = $xpath->query('//root/item[contains(objType,"Image")]/image_uuid');

            $motive_calc = array();
            $motive_calc_link = array();

            $motive_price = 0;

            $install = Zend_Registry::get('install');
            $motiv1 = $install['motiv_1'];
            $motiv2 = $install['motiv_2'];
            $motiv3 = $install['motiv_3'];

            foreach ($motiveCalc as $motivCalc) {

                $row = Doctrine_Query::create()
                    ->from('Motiv c')
                    ->where('c.uuid = ?')
                    ->fetchOne(array($motivCalc->nodeValue));
                $a = $xpath->query('//root/item[contains(image_uuid,"' . $motivCalc->nodeValue . '")]/a')->nodeValue / 2.8;
                $b = $xpath->query('//root/item[contains(image_uuid,"' . $motivCalc->nodeValue . '")]/b')->nodeValue / 2.8;

                $mm = (($a * $b) / 100);

                if ($mm <= $motiv1) {
                    $motiv_group = 1;
                } elseif ($mm > $motiv1 && $mm <= $motiv2) {
                    $motiv_group = 2;
                } elseif ($mm > $motiv2 && $mm <= $motiv3) {
                    $motiv_group = 3;
                } else {
                    $motiv_group = 3;
                }

                $motive_calc_link[] = array('uuid' => $row->uuid);

                if ($row && ($row['price' . $motiv_group] > 0) && (!Zend_Auth::getInstance()->hasIdentity() || $row->contact_id != $user['id'])) {
                    $articleBasket->addMotiv($row, $motiv_group);

                    $motive_calc[] = $row->uuid;
                    $motive_price += $row['price' . $motiv_group];
                }
            }

            if (implode(',', $motive_calc) != $article->motive_calc || $article->motive_price != $motive_price) {
                $article->motive_calc = implode(',', $motive_calc);
                $article->motive_price = $motive_price;
                $article->save();
            }

            $view->view->motive = $articleBasket->getMotive();

            $view->getRequest()->setPost('motivepaid', count($articleBasket->getMotive()));
            $view->getRequest()->setParam('motivepaid', count($articleBasket->getMotive()));
            $articleBasket->setDesignerSeiten($seiten);
        }

        $this->netto = $calc->parse($view, null, $param, $article->a6_resale_price + $articleBasket->getMotivPrice());

        if (isset($view->view->layouterPreviewId) && $view->view->layouterPreviewId != "") {
            $articleSession = new TP_Layoutersession();

            $articleSess = $articleSession->getLayouterArticle($view->view->layouterPreviewId);
            if ($articleSess->getLayouterModus() == 5) {
                $this->netto = $articleBasket->getNetto();
            }
            if ($articleSess->getLayouterModus() == 6) {
                $this->netto = $articleBasket->getNetto();
            }
        }

        if (!$ajax) {

            if (!$view->getRequest()->getParam('special', false)) {
                $view->view->form->addElement('submit', 'send', array('label' => $translate->translate('Calc')));
            }
        } else {

            if (!$view->getRequest()->getParam('special', false)) {
                $view->view->form[] = array('type' => 'submit', 'label' => $translate->translate('Calc'), 'name' => 'send');
            }
        }

        if (!$ajax) {
            if ($view->getRequest()->getParam('upload_mode') == 'weblayouter') {
                if (isset($view->view->form->seiten)) {
                    if ($view->getRequest()->getParam('layouter_seiten', false)) {
                        $view->getRequest()->setPost('seiten', $view->getRequest()->getParam('layouter_seiten'));
                        $view->getRequest()->setParam('seiten', $view->getRequest()->getParam('layouter_seiten'));
                        $seiten = new TP_Form_Element_Text('seiten');
                        $seiten->setLabel('Seiten');
                        $seiten->setValue($view->getRequest()->getParam('layouter_seiten'));
                        $view->view->form->seiten = $seiten;
                        $view->view->form->seiten->setValue($view->getRequest()->getParam('layouter_seiten'));
                    }
                }
            }
        }
        $auflage = $view->getRequest()->getParam('auflage', false);
        if ($auflage && $auflage > 1) {
            return $this->netto + ($view->getRequest()->getParam('auflage', 1) * $article->a6_resale_price) + ($view->getRequest()->getParam('auflage', 1) * $articleBasket->getMotivPrice());
        }
        return $this->netto + $article->a6_resale_price + $articleBasket->getMotivPrice();
    }

    public function createPreDispatch($data)
    {
        $path = str_split($data->id);

        $data->a6_directory = implode('/', $path) . '/';

        $data->save();
    }

    public function clearPreDispatch($data)
    {
    }

    public function updatePreDispatch($data)
    {
    }

    public function copyPreDispatch($data, $orguuid = false, $art = false)
    {
        $path = str_split($data->id);


        $orginal = $data->a6_directory;
        $data->a6_directory = implode('/', $path) . '/';

        if (file_exists(APPLICATION_PATH . '/../market/templateprint/orginal/' . $orginal . '/product.zip')) {
            mkdir(APPLICATION_PATH . '/../market/templateprint/orginal/' . implode('/', $path), 0777, true);
            copy(APPLICATION_PATH . '/../market/templateprint/orginal/' . $orginal . '/product.zip', APPLICATION_PATH . '/../market/templateprint/orginal/' . implode('/', $path) . '/product.zip');
        }

        if (file_exists(APPLICATION_PATH . '/../market/templateprint/orginal/' . $orginal . '/final.pdf')) {
            mkdir(APPLICATION_PATH . '/../market/templateprint/orginal/' . implode('/', $path), 0777, true);
            copy(APPLICATION_PATH . '/../market/templateprint/orginal/' . $orginal . '/final.pdf', APPLICATION_PATH . '/../market/templateprint/orginal/' . implode('/', $path) . '/final.pdf');
        }

        if (file_exists(APPLICATION_PATH . '/../market/templateprint/orginal/' . $orginal . '/preview.pdf')) {
            mkdir(APPLICATION_PATH . '/../market/templateprint/orginal/' . implode('/', $path), 0777, true);
            copy(APPLICATION_PATH . '/../market/templateprint/orginal/' . $orginal . '/preview.pdf', APPLICATION_PATH . '/../market/templateprint/orginal/' . implode('/', $path) . '/preview.pdf');
        }


        $data->save();

        if ($orguuid != false) {

            $articleSession = new TP_Layoutersession();

            $articleSess = $articleSession->getLayouterArticle($orguuid);

            if ($articleSess->getLayouterModus() == 2) {

                $data->upload_steplayouter_data = $articleSess->getDesignerXML();
                if ($articleSess->getTitle() != "") {
                    $data->title = $articleSess->getTitle();
                }
                $data->save();
            } elseif ($articleSess->getLayouterModus() == 4) {

                $design = Doctrine_Query::create()->from('LayouterDesignData c')->where('c.uuid = ?', array(
                    $articleSess->getArticleId()
                ))->fetchOne();
                $data->upload_steplayouter_data = $design->design;
                $articleSession->deleteLayouterArticle($orguuid);
            } else {
            }
            $articleSession->deleteLayouterArticle($orguuid);
        }
    }

    public function copyAdminPreDispatch($orgArticle, $copyArticle)
    {

        if ($orgArticle->install_id == $copyArticle->install_id) {
            return;
        }
        $xml = new SimpleXMLElement(str_replace('\\', '', $orgArticle->a1_xml), null, false);

        $orgInstall = $orgArticle->Shop->Install;

        eval(str_replace('\\', '', $orgArticle->Shop->parameter));
        eval(str_replace('\\', '', $orgArticle->Shop->formel));

        $copyInstall = $copyArticle->Shop->Install;

        $papierdborg = new SimpleXMLElement($orgInstall->containerdb, null, false);
        $papierdb = new SimpleXMLElement($copyInstall->containerdb, null, false);

        foreach ($xml as $node) {
            foreach ($node->option as $nod) {

                foreach ($nod->children() as $key => $nextNode) {

                    if ($key != 'opt') {
                        foreach ($nextNode->children() as $keys => $nextNodee) {
                            preg_match_all('/\$P\w*\$P/', $nextNodee['formel'], $founds);
                            if (!empty($founds[0])) {

                                foreach ($founds[0] as $key => $found) {

                                    $foundvalue = str_replace('$P', '', $found);
                                    $nextNodee['formel'] = str_replace($found, $$foundvalue, $nextNodee['formel']);
                                }
                            }
                        }
                    }
                    if ($key == 'opt') {
                        foreach ($nextNode->children() as $keys => $nextNodee) {
                            foreach ($nextNodee->children() as $keyss => $nextNodeee) {
                                preg_match_all('/\$P\w*\$P/', $nextNodeee['formel'], $founds);
                                if (!empty($founds[0])) {

                                    foreach ($founds[0] as $key => $found) {

                                        $foundvalue = str_replace('$P', '', $found);
                                        $nextNodeee['formel'] = str_replace($found, $$foundvalue, $nextNodeee['formel']);
                                    }
                                }
                            }
                        }
                    }
                }

                if ((string) $nod['mode'] == 'papierdb' && (string) $nod['container'] != "") {
                    $inhalt = $papierdborg->xpath('//papiercontainer[@id="' . (string) $nod['container'] . '"]');
                    $inhalt = $inhalt[0];

                    $papierdborgset = $papierdb->addChild('papiercontainer');
                    $papierdborgset->addAttribute('id', $inhalt['id'] . '_' . $copyArticle->id);
                    if ($inhalt['value'] != "") {
                        $papierdborgset->addAttribute('value', $inhalt['value']);
                    }

                    foreach ($inhalt->papier as $papier) {

                        $p = Doctrine_Query::create()->select()->from('Papier p')->where('p.art_nr = ? AND p.install_id = ?', array($papier['id'], $orgInstall->id))->fetchOne();

                        $newPapier = new Papier();
                        $newPapier->install_id = $copyInstall->id;
                        $newPapier->art_nr = $p->art_nr . '_' . $copyArticle->id;
                        $newPapier->description_1 = $p->description_1;
                        $newPapier->description_2 = $p->description_2;
                        $newPapier->preis = $p->preis;
                        $newPapier->save();

                        $papierdbp = $papierdborgset->addChild('papier');
                        $papierdbp->addAttribute('id', $newPapier->art_nr);
                        if ($papier['value'] != "") {
                            $papierdbp->addAttribute('value', $papier['value']);
                        }
                    }

                    $nod['container'] = $inhalt['id'] . '_' . $copyArticle->id;
                    $nod['default'] = $nod['default'] . '_' . $copyArticle->id;
                }
            }
        }

        $copyInstall->containerdb = $papierdb->asXML();
        $copyInstall->save();

        $copyArticle->a1_xml = $xml->asXML();
        $copyArticle->template_admin = false;
        $copyArticle->template_system_operator = false;
        $copyArticle->save();
    }

    public function generatePreviewLink($articleId, $layouterPreviewId = false)
    {
        if ($layouterPreviewId) {
            $article = Doctrine_Query::create()
                ->from('Article c')
                ->where('c.id = ?', array(intval($articleId)))
                ->fetchOne();

            $articleSession = new TP_Layoutersession();
            $articleSess = $articleSession->getLayouterArticle($layouterPreviewId);

            if ($articleSess->getTemplatePrintId() != "") {
                return TP_Templateprint::generatePreviewLink($article, $layouterPreviewId);
            }
        }
    }

    public function generatePreview($articleId, $layouterPreviewId = false, $mode = false, $class = "", $onlyPath = false, $order_id = false, $pos = false, $site = 1)
    {

        if ($order_id && $pos) {

            $article = Doctrine_Query::create()
                ->from('Article c')
                ->where('c.id = ?', array(intval($articleId)))
                ->fetchOne();

            return TP_Image::generateWidthImgTag(TP_Image::LAYOUTERPREVIEW, TP_Templateprint::generatePreview($article, $layouterPreviewId, $order_id, $pos), $mode, $class, $onlyPath);
        }

        if ($layouterPreviewId) {
            $article = Doctrine_Query::create()
                ->from('Article c')
                ->where('c.id = ?', array(intval($articleId)))
                ->fetchOne();

            $articleSession = new TP_Layoutersession();
            $articleSess = $articleSession->getLayouterArticle($layouterPreviewId);

            if ($articleSess->getLayouterModus() == 2) {

                if (!file_exists(PUBLIC_PATH . '/steplayouter/temp/' . $layouterPreviewId . ".png")) {

                    Zend_Registry::set("steplayout_preview", 1);

                    $data = json_decode($articleSess->getDesignerXML(), true);
                    $steplayouter = new TP_Steplayouter($data['jsonSaveData'], $article, $data['jsonLayerData'], $data['jsonCreateData']);
                    $steplayouter->buildRenderStack($data['width'], $data['height']);
                    $file = $steplayouter->exportPNG(PUBLIC_PATH . '/steplayouter/temp/' . $layouterPreviewId, 400, 400, 1);
                }
                return TP_Image::generateWidthImgTag(TP_Image::LAYOUTERPREVIEW, PUBLIC_PATH . '/steplayouter/temp/' . $layouterPreviewId . ".png", $mode, $class, $onlyPath);
            } elseif ($articleSess->getLayouterModus() == 4) {
                return '<img src="/apps/component/steplayouter/pdf/imagepreview/' . $layouterPreviewId . '/500" />';
            } elseif ($articleSess->getLayouterModus() == 5) {
                require_once(APPLICATION_PATH . '/helpers/Image.php');
                $imageHelper = new TP_View_Helper_Image();
                $file = $imageHelper->thumbnailImage($article->title, 'articlesinglegreater', $article->file, true);
                return TP_Image::generateWidthImgTag(TP_Image::LAYOUTERPREVIEW, $file, $mode, $class, $onlyPath);
            } elseif ($articleSess->getLayouterModus() == 6) {
                require_once(APPLICATION_PATH . '/helpers/Image.php');
                $imageHelper = new TP_View_Helper_Image();
                $file = $imageHelper->thumbnailImage($article->title, 'articlesinglegreater', $article->file, true);
                return TP_Image::generateWidthImgTag(TP_Image::LAYOUTERPREVIEW, $file, $mode, $class, $onlyPath);
            } elseif ($articleSess->getLayouterModus() == 8) {
                return '<img src="https://store.hpwallart.com/huelswitt/projects/' . $articleSess->getTemplatePrintId() . '/preview_medium.png" />';
            } elseif ($articleSess->getLayouterModus() == 9) {
                return '<img src="/apps/plugin/chilipublish/designer/preview/' . $article->Shop->getApiKey() . '/' . $articleSess->getTemplatePrintId() . '/' . $site . '" />';
            } elseif ($articleSess->getLayouterModus() == 10) {
                return '<img src="/apps/api/formlayouter/preview/layouter/' . $layouterPreviewId . '/' . $site . '/500" />';
            } elseif ($articleSess->getLayouterModus() == 11) {
                $layoutData = Doctrine_Query::create()
                ->from('LayouterDesignData c')
                ->where('c.uuid = ?', array((string)$layouterPreviewId))
                ->fetchOne();
                $design = json_decode($layoutData->design, true);
                return '<img src="' . $design['previews'][0] .'" class="layouter_10" />';

            } elseif ($articleSess->getTemplatePrintId() != "") {

                if (!TP_Templateprint::siteExists($article, $layouterPreviewId, false, false, $site)) {
                    return '';
                }
                return TP_Image::generateWidthImgTag(TP_Image::LAYOUTERPREVIEW, TP_Templateprint::generatePreview($article, $layouterPreviewId, false, false, $site), $mode, $class, $onlyPath, true);
            }
        } else {
            $article = Doctrine_Query::create()
                ->from('Article c')
                ->where('c.id = ?', array(intval($articleId)))
                ->fetchOne();
            if ($article->upload_steplayouter_data != "") {
                return '<img src="/apps/component/steplayouter/pdf/imagepreview/' . $article->uuid . '/500" />';
            } elseif (TP_Templateprint::isTemplateExists($article)) {
                return TP_Image::generateWidthImgTag(TP_Image::LAYOUTERPREVIEW, TP_Templateprint::generatePreview($article), $mode, $class, $onlyPath);
            }
        }

        if ($layouterPreviewId != false && $layouterPreviewId != "") {
            return TP_Image::generateWidthImgTag(TP_Image::LAYOUTERPREVIEW, TP_FOP::generatePreview($articleId, $layouterPreviewId), $mode, $class, $onlyPath);
        }
        return TP_Image::generateWidthImgTag(TP_Image::LAYOUTERPREVIEW, TP_FOP::generatePreview($articleId), $mode, $class, $onlyPath);
    }

    public function generatePreviewBig($articleId, $layouterPreviewId = false)
    {

        if ($layouterPreviewId) {
            $article = Doctrine_Query::create()
                ->from('Article c')
                ->where('c.id = ?', array(intval($articleId)))
                ->fetchOne();

            $articleSession = new TP_Layoutersession();
            $articleSess = $articleSession->getLayouterArticle($layouterPreviewId);

            if ($articleSess->getLayouterModus() == 2) {

                $steplayouter = new TP_Steplayouter(json_decode($articleSess->getDesignerXML(), true), $article);
                $steplayouter->buildRenderStack();
                $file = $steplayouter->exportPNG(PUBLIC_PATH . '/steplayouter/temp/' . $layouterPreviewId, 400, 400, 1);
                return TP_Image::generateWidthImgTag(TP_Image::LAYOUTERPREVIEW, $file);
            } elseif ($articleSess->getLayouterModus() == 8) {
                return '<img src="https://store.hpwallart.com/huelswitt/projects/' . $articleSess->getTemplatePrintId() . '/preview_large.png" />';
            } elseif ($articleSess->getTemplatePrintId() != "") {
                return TP_Image::generateWidthImgTag(TP_Image::LAYOUTERPREVIEW, TP_Templateprint::generatePreview($article, $layouterPreviewId));
            }
        } else {
            $article = Doctrine_Query::create()
                ->from('Article c')
                ->where('c.id = ?', array(intval($articleId)))
                ->fetchOne();

            if ($article->upload_templateprint && $article->render_new_preview_image == false) {
                return TP_Image::generateWidthImgTag(TP_Image::LAYOUTERPREVIEW, TP_Templateprint::generatePreview($article));
            }
        }

        if ($layouterPreviewId != false && $layouterPreviewId != "") {
            return TP_Image::generateWidthImgTag(TP_Image::LAYOUTERPREVIEW, TP_FOP::generatePreview($articleId, $layouterPreviewId));
        }
        return TP_Image::generateWidthImgTag(TP_Image::LAYOUTERPREVIEW, TP_FOP::generatePreview($articleId));
    }
}
