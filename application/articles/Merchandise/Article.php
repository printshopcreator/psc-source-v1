<?php

class merchandise_article {
	
    public $id = "9";
    public $articlegroups;
    public $private;
    public $versandwert;
    public $versand;

    public $netto = 0;

    public $name = "Merchandise Product";

    public $template = "merchandise_article";

    public $information = "Bietet Merchandise Produkte an";

    public $backend = 'Merchandise/config/backend.ini';
    
    public $backend_market = 'Merchandise/config/backend_market.ini';

    public $frontend = 'Merchandise/config/frontend.xml';

    public $path = 'Merchandise';

    public $title;
    public $text;
    public $file;
    public $file1;

    public $xmltext;

    public function getNetto() {
            return $this->netto;
    }

    public function setNetto($value) {
            $this->netto = $value;
    }

    public function toArray() {
            return array('thomas' => $this->thomas, 'pdf' => $this->pdf, 'template' => $this->template, 'a1_xml' => str_replace('\\', '', $this->a1_xml), 'versand' => $this->versand,'versandwert' => $this->versandwert,'private' => $this->private, 'articlegroups' => $this->articlegroups, 'mwert' => $this->mwert, 'title' => $this->title, 'price' => $this->price, 'text' => $this->text, 'file' => $this->file, 'file1' => $this->file1);
    }

    public function createFrontend($view, $translate, Article $article, $ajax = false) {

        if($article->a6_org_article > 0) {
            $orgArticle = $article->OrgArticle;
        }else{
            $orgArticle = $article;
        }
        
        $xml = simplexml_load_string($orgArticle->a9_sizes);
        $basket = new TP_Basket ( );
        $netto = $article->a6_resale_price;
        $count = 0;

        $prices = array();
        $install = Zend_Registry::get('install');
        $pt = new SimpleXMLElement($install['a9_pt'], null, false);
        $params = array();
        
        $print_sizes = array();
        
        foreach($xml as $el) {
            $value = intval($view->getRequest()->getParam('s'.$el['id'], 0));
            
            $print_sizes[] = array(
                'width'  => (string)$el['wth'],
                'height' => (string)$el['hth'],
                'value'  => $value,
                'id'     => $el['id']
            );
            
            $prices[] = array('id' => 's'.(string)$el['id'], 'name' => (string)$el['size'], 'value' => $value);
            if(isset($el['added_price'])) {
                $netto = $netto + ($value * (float)$el['added_price']);
            }
            $netto = $netto + ($orgArticle->preis * $value);
            $count += $value;
            $params['s'.(string)$el['id']] = array('value' => $value, 'name' => (string)$el['size'] );
        }
        $view->view->has_price = $count;
        $tempPro = $basket->getTempProduct($article->uuid);
        $tempPro = $tempPro->getDesignerData();
        $tempProOrg = $tempPro;
        
        $tempProOrg = $tempPro;
        
        if(!isset($tempPro['design_config']) || $tempPro['design_config'] == "") {
            $tempPro = Zend_Json::decode($article->mercendise_design);
        }
        
        if(isset($tempProOrg['colors']) || $tempProOrg['colors'] == "") {
            $tempPro['colors'] = $tempProOrg['colors'];
        }
     

        if(isset($tempPro['design_config']) && $tempPro['design_config'] != "") {
            
            $install = Zend_Registry::get('install');
            $motiv1 = $install['motiv_1'];
            $motiv2 = $install['motiv_2'];
            $motiv3 = $install['motiv_3'];
            
            
            $price = 0;
    	    $xmldes = simplexml_load_string('<?xml version="1.0"?><root>' . str_replace("\n","#", str_replace("&","+",$tempPro['design_config'])) . '</root>');

            foreach ($xmldes->clip as $el) {

                if((string)$el['colname'] != "undefined") {
                    $colname = strstr((string)$el['colname'], ":", true);
                	if((string)$colname == 'Flex Print') {
                        $colname = "Flex Folie";
                    }
                }else{
                    if ((string)$el['type'] == 'txt') {
                        $colname = "Flex Folie";
                    }else{
                        $colname = "Digitaldruck";
                    }
                }
                
                $itm = $pt->xpath('//printing/itm[@technology_name="' . $colname . '"]');
                if(!$orgArticle->a9_price_print_disable) {
                    $price = $price + (float)$itm[0]['technology_price'];
                }
                
                if ((string)$el['type'] == 'img') {
					
                    if (strpos($el['path_str'], "readusermotivebig")) {
                        
                        $match = str_replace("/service/designer/readusermotivebig?id=", "", str_replace(",undefined,undefined", "", $el['path_str']));
                                
                        $motiv = Doctrine_Query::create()
                                    ->from('Motiv a')
                                    ->where('a.uuid = ?', array($match))->fetchOne();
                        foreach($print_sizes as $ps) {
                            
                            $area = 'a9_'.(string)$el['side'].'_area';
                            
                            $screen = explode(',', $orgArticle->$area);
                            
                            $fw = ($ps['width'] / ($screen[2] / (float)$el['width']));
                            $fh = ($ps['height'] / ($screen[3] / (float)$el['height']));
                            
                            $mm = $fw * $fh;
                            
                            if($mm <= $motiv1) {
                                $motiv_group = 1;
                            }elseif($mm > $motiv1 && $mm <= $motiv2) {
                                $motiv_group = 2;
                            }elseif($mm > $motiv2 && $mm <= $motiv3) {
                                $motiv_group = 3;
                            }else{
                                $motiv_group = 3;
                            }
                            
                            if(!$orgArticle->a9_price_print_disable) {
                                $netto = $netto + ($ps['value'] * ($orgArticle->a9_price_motiv + $motiv['price'.$motiv_group]));
                            }else{
                                $netto = $netto + ($ps['value'] * $motiv['price'.$motiv_group]);
                            }
                        }
                        reset($print_sizes);
                    }else{
                        if(!$orgArticle->a9_price_print_disable) {
                            $netto = $netto + ($count *  $orgArticle->a9_price_icon);
                        }
                    }
                }
                if ((string)$el['type'] == 'txt') {
                    $netto = $netto + ($count * $orgArticle->a9_price_text);
                    $anzahl = substr_count((string)$el['path_str'], '#');
                    $anzahl = $anzahl;
                    if($anzahl < 0) {
                    	$anzahl = 0;
                    }
                    if(!$orgArticle->a9_price_print_disable) {
                        $netto = $netto + ($count * ($anzahl * $orgArticle->a9_price_text_line1));
                    }
                }
            }
           
            $netto = $netto + ($count * $price);
        }
 
        if(isset($tempPro['colors']) && $tempPro['colors'] != "") {
                $color = str_replace(',','',  strstr($tempPro['colors'], ','));
                $xml = simplexml_load_string($orgArticle->a9_colors);
                $element = $xml->xpath('//itm[@colorvalue="' . $color . '"]');
   
                if(isset($element[0]) && isset($element[0]['added_price'])) {
                    $netto = $netto + ($count * (float)$element[0]['added_price']);
                }
        }
        
        $view->view->prices = $prices;
        
        $sessart = $basket->getTempProduct($article->uuid);
        $sessart->setCount($count);
        $sessart->setDesignerCost($netto);
        $sessart->setDesignerParam($params);
       
        return $netto;
    	
    }

    public function generatePreview($articleId, $layouterPreviewId = false) {
        $articleSession = new TP_Layoutersession();
        $articleSess = $articleSession->getLayouterArticle($layouterPreviewId);
            
        return $articleSess->getPreviewPath();
    }
    
    public function copyPreDispatch($data, $orguuid = false, $art = false) {
        
        if ($orguuid != false) {

            $articleSession = new TP_Layoutersession();

            $articleSess = $articleSession->getLayouterArticle($orguuid);
            
            $data->mercendise_design = $articleSess->getDesignerXML();
            
            $shop = Zend_Registry::get('target_shop');
            
            if(!file_exists('uploads/' . $shop . '/designer/')) {
                mkdir('uploads/' . $shop . '/designer/', 0777, true);
            }
            
            copy($articleSess->getPreviewPath(), 'uploads/' . $shop . '/designer/' . $data->id . '.jpg');
            $data->mercendise_thumbnail = 'uploads/' . $shop . '/designer/' . $data->id . '.jpg';
            
            
            $data->save();
        }
        
    }
}
