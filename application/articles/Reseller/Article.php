<?php

require_once(__DIR__ . '/../Market/Article.php');

class reseller_article extends market_article
{

    public $id = "106";
    public $name = "Reseller Api Product";
    public $template = "market_article";
    public $information = "Bietet Reseller Produkte an";
    public $backend = 'Reseller/config/backend.ini';
    public $backend_market = 'Reseller/config/backend_market.ini';
    public $frontend = 'Reseller/config/frontend.xml';
    public $path = 'Reseller';


}
