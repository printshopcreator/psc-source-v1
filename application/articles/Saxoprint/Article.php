<?php

require_once(__DIR__ . '/../Market/Article.php');

class saxoprint_article extends market_article
{

    public $id = "100";
    public $name = "Saxoprint Api Product";
    public $template = "market_article";
    public $information = "Bietet Saxoprint Produkte an";
    public $backend = 'Saxoprint/config/backend.ini';
    public $backend_market = 'Saxoprint/config/backend_market.ini';
    public $frontend = 'Saxoprint/config/frontend.xml';
    public $path = 'Saxoprint';


}
