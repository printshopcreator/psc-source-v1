<?php

class simple_article {

    public $id = "1";
    public $netto = 0;
    public $name = "Kalkulations Produkt";
    public $template = "simple_article";
    public $information = "Bietet normale Produkte an";
    public $backend = 'Simple/config/backend.ini';
    public $frontend = 'Simple/config/frontend.xml';
    public $path = 'Simple';

    public function getNetto() {
        return $this->netto;
    }

    public function setNetto($value) {
        $this->netto = $value;
    }

    public function createFrontend($view, $translate, Article $article, $ajax = false) {

        if ($article->a1_xml != '') {
            $calc = new TP_Calc(null, str_replace('\\', '', $article->a1_xml), $ajax);
        } else {
            $calc = new TP_Calc(APPLICATION_PATH . '/articles/' . $this->frontend, null, $ajax);
        }

        $this->netto = $calc->parse($view, $article->a1_special);
        if (!$view->getRequest()->getParam('special') && $article->a1_special == true) {
            $view->view->form->addElement('submit', 'send', array('label' => $translate->translate('Calc')));
            $view->view->form->addElement('submit', 'special', array('label' => $translate->translate('individuelle Anfrage erstellen')));
        } else {
            $view->view->form->addElement('submit', 'send', array('label' => $translate->translate('Calc')));
        }
        if ($view->view->special == true) {
            $view->view->form->addElement('textarea', 'info', array('label' => 'Info', 'rows' => 11));
            $view->view->form->addElement('submit', 'special', array('label' => $translate->translate('Anfrage abschicken')));
        }
        return $this->netto * 1;
    }

    public function copyAdminPreDispatch($orgArticle, $copyArticle) {

        if ($orgArticle->install_id == $copyArticle->install_id) {
            return;
        }
        $xml = new SimpleXMLElement(str_replace('\\', '', $orgArticle->a1_xml), null, false);

        $orgInstall = $orgArticle->Shop->Install;

        eval(str_replace('\\', '', $orgArticle->Shop->parameter));
        eval(str_replace('\\', '', $orgArticle->Shop->formel));

        $copyInstall = $copyArticle->Shop->Install;

        $papierdborg = new SimpleXMLElement($orgInstall->containerdb, null, false);
        $papierdb = new SimpleXMLElement($copyInstall->containerdb, null, false);

        foreach ($xml as $node) {
            foreach ($node->option as $nod) {

                foreach ($nod->children() as $key => $nextNode) {

                    if ($key != 'opt') {
                        foreach ($nextNode->children() as $keys => $nextNodee) {
                            preg_match_all('/\$P\w*\$P/', $nextNodee['formel'], $founds);
                            if (!empty($founds[0])) {

                                foreach ($founds[0] as $key => $found) {

                                    $foundvalue = str_replace('$P', '', $found);
                                    $nextNodee['formel'] = str_replace($found, $$foundvalue, $nextNodee['formel']);
                                }
                            }
                        }
                    }
                    if ($key == 'opt') {
                        foreach ($nextNode->children() as $keys => $nextNodee) {
                            foreach ($nextNodee->children() as $keyss => $nextNodeee) {
                                preg_match_all('/\$P\w*\$P/', $nextNodeee['formel'], $founds);
                                if (!empty($founds[0])) {

                                    foreach ($founds[0] as $key => $found) {

                                        $foundvalue = str_replace('$P', '', $found);
                                        $nextNodeee['formel'] = str_replace($found, $$foundvalue, $nextNodeee['formel']);
                                    }
                                }
                            }
                        }
                    }
                }


                if ((string) $nod['mode'] == 'papierdb' && (string) $nod['container'] != "") {
                    $inhalt = $papierdborg->xpath('//papiercontainer[@id="' . (string) $nod['container'] . '"]');
                    $inhalt = $inhalt[0];

                    $papierdborgset = $papierdb->addChild('papiercontainer');
                    $papierdborgset->addAttribute('id', $inhalt['id'] . '_' . $copyArticle->id);
                    if ($inhalt['value'] != "") {
                        $papierdborgset->addAttribute('value', $inhalt['value']);
                    }

                    foreach ($inhalt->papier as $papier) {

                        $p = Doctrine_Query::create()->select()->from('Papier p')->where('p.art_nr = ? AND p.install_id = ?', array($papier['id'], $orgInstall->id))->fetchOne();

                        $newPapier = new Papier();
                        $newPapier->install_id = $copyInstall->id;
                        $newPapier->art_nr = $p->art_nr . '_' . $copyArticle->id;
                        $newPapier->description_1 = $p->description_1;
                        $newPapier->description_2 = $p->description_2;
                        $newPapier->preis = $p->preis;
                        $newPapier->save();

                        $papierdbp = $papierdborgset->addChild('papier');
                        $papierdbp->addAttribute('id', $newPapier->art_nr);
                        if ($papier['value'] != "") {
                            $papierdbp->addAttribute('value', $papier['value']);
                        }
                    }


                    $nod['container'] = $inhalt['id'] . '_' . $copyArticle->id;
                    $nod['default'] = $nod['default'] . '_' . $copyArticle->id;
                }
            }
        }

        $copyInstall->containerdb = $papierdb->asXML();
        $copyInstall->save();

        $copyArticle->a1_xml = $xml->asXML();
        $copyArticle->template_admin = false;
        $copyArticle->template_system_operator = false;
        $copyArticle->save();
    }

}
