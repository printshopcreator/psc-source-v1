<?php

class waren_article
{

    public $id = "2";

    public $netto = 0;

    public $name = "Warenhaus Produkt";

    public $template = "edition_article";

    public $information = "Bietet normale Produkte an";

    public $backend = 'Waren/config/backend.ini';

    public $frontend = 'Waren/config/frontend.xml';

    public $path = 'Waren';

    public function getNetto()
    {
        return $this->netto;
    }

    public function setNetto($value)
    {
        $this->netto = $value;
    }

    public function fixBasketItem($article)
    {
        $first = false;
        $options = array();
        foreach (explode('<br />', nl2br($article['a4_auflagen'])) as $row) {
            $row = str_replace('\r\n', '', $row);
            $row = str_replace('\n', '', $row);
            $row = str_replace('\r', '', $row);
            if (stripos($row, '|') != false) {
                $rowar = explode('|', $row);
                $rowar[0] = trim($rowar[0]);
                $auflagen[$rowar[0]] = $rowar[1];
                if (!isset($params['auflage']) && $first == false) {
                    $options['auflage'] = $rowar[0];
                    $options['weight'] = $rowar[3];

                    $first = true;
                }
            }
        }

        return $options;
    }

    public function createFrontend($view, $translate, Article $article, $ajax = false)
    {

        $params = Zend_Registry::get('params');

        $shop = Zend_Registry::get('shop');

        $basket = new TP_Basket();
        $tempProd = $basket->getTempProduct($article->uuid);
        $sessart = $tempProd->getOptions();

        if (($article->preis != "" && $article->preis != 0) || $article->a4_auflagen == '') {
            $this->netto = $article->preis * 1;
            return $this->netto;
        }

        $auflagen = array();
        $first = false;
        foreach (explode('<br />', nl2br($article->a4_auflagen)) as $row) {
            $row = str_replace('\r\n', '', $row);
            $row = str_replace('\n', '', $row);
            $row = str_replace('\r', '', $row);
            if (stripos($row, '|') != false) {
                $rowar = explode('|', $row);
                $rowar[0] = trim($rowar[0]);
                $auflagen[$rowar[0]] = $rowar[1];
                if (isset($params['auflage']) && $params['auflage'] == $rowar[0]) {
                    $Preis = str_replace(',', '.', $rowar[2]);
                    $Gewicht = $rowar[3];
                } elseif (!isset($params['auflage']) && $first == false) {
                    $Preis = str_replace(',', '.', $rowar[2]);
                    $Gewicht = $rowar[3];
                    $first = true;
                }
            }
        }



        if (!isset($sessart['auflage']) && !empty($auflagen)) {
            $sessart['auflage'] = key($auflagen);
            $tempProd->setOptions($sessart);
            $view->getRequest()->setParam('auflage', $sessart['auflage']);
        } elseif (!in_array($sessart['auflage'], $auflagen)) {
            $sessart['auflage'] = key($auflagen);
            $tempProd->setOptions($sessart);
            $view->getRequest()->setParam('auflage', $sessart['auflage']);
        } elseif (isset($sessart['auflage']) && isset($params['auflage']) && $params['auflage'] != $sessart['auflage']) {
            $sessart['auflage'] = $params['auflage'];
            $tempProd->setOptions($sessart);
            $view->getRequest()->setParam('auflage', $sessart['auflage']);
        }

        $tempProd->setWeight($Gewicht);
        $Preis = ($article->a6_resale_price * 1) + $Preis;

        $basket->setTempProductClass($tempProd);

        $view->view->preis = $Preis;

        $auflage = $view->view->form->createElement('select', 'auflage')
            ->setLabel('Auflage')
            ->setMultiOptions($auflagen)
            ->setAttrib('onchange', 'javascript:this.form.submit();')
            ->setAttrib('style', 'width: 220px;');

        $view->view->form->addElements(array($auflage));

        $this->netto = $Preis;

        return $this->netto;
    }

    public function defaultSettingsPreDispatch()
    {

        return array('a4_auflagen' => '1|1 Stück|1.00');
    }

    public function buyPreDispatch(TP_Basket_Item $sessart, $article)
    {
        $sessart->setWeight($sessart->getCount() * $article->getWeight());
        $sessart->setWeightSingle($article->getWeight());
    }
}
