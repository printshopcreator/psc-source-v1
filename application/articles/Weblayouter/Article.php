<?php

require_once dirname(__FILE__) . "/../Market/Article.php";

class weblayouter_article extends market_article {

    public $id = "8";
    public $name = "Upload oder Webbased Layouter Produkt";
    public $template = "weblayouter_article";
    public $information = "Bietet Weblayouter Produkte an";
    public $backend = 'Weblayouter/config/backend.ini';

    public function createFrontend($view, $translate, Article $article, $ajax = false) {

        $var = parent::createFrontend($view, $translate, $article, $ajax);
       
        return $var;
    }

}
