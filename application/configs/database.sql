SET NAMES utf8;
SET FOREIGN_KEY_CHECKS = 0;
CREATE DATABASE psc;
USE psc;
-- ----------------------------
--  Table structure for `account`
-- ----------------------------
DROP TABLE IF EXISTS `account`;
CREATE TABLE `account` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `install_id` bigint(20) DEFAULT NULL,
  `created` date DEFAULT NULL,
  `updated` date DEFAULT NULL,
  `company` varchar(255) DEFAULT NULL,
  `appendix` varchar(255) DEFAULT NULL,
  `street` varchar(255) DEFAULT NULL,
  `destrict` varchar(5) DEFAULT NULL,
  `zip` varchar(255) DEFAULT NULL,
  `city` varchar(255) DEFAULT NULL,
  `state` varchar(255) DEFAULT NULL,
  `country` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `mobile` varchar(255) DEFAULT NULL,
  `fax` varchar(255) DEFAULT NULL,
  `homepage` varchar(255) DEFAULT NULL,
  `bank_name` varchar(255) DEFAULT NULL,
  `bank_blz` varchar(255) DEFAULT NULL,
  `bank_kto` varchar(255) DEFAULT NULL,
  `bank_code` varchar(255) DEFAULT NULL,
  `bank_iban` varchar(255) DEFAULT NULL,
  `bank_bic` varchar(255) DEFAULT NULL,
  `usid` varchar(255) DEFAULT NULL,
  `mwert` tinyint(1) DEFAULT '1',
  `house_number` varbinary(255) DEFAULT NULL,
  `logo1` varchar(255) DEFAULT NULL,
  `locked` int(1) NOT NULL,
  `locked_master` int(1) NOT NULL,
  `filiale_id` int(8) NOT NULL,
  `typ` int(8) NOT NULL,
  `phone_vorwahl` varchar(255) NOT NULL,
  `phone_durchwahl` varchar(255) NOT NULL,
  `mobile_vorwahl` varchar(255) NOT NULL,
  `mobile_durchwahl` varchar(255) NOT NULL,
  `fax_vorwahl` varchar(255) NOT NULL,
  `fax_durchwahl` varchar(255) NOT NULL,
  `alternativ_type` varchar(255) NOT NULL,
  `alternativ` varchar(255) NOT NULL,
  `alternativ_durchwahl` varchar(255) NOT NULL,
  `informations` text NOT NULL,
  `bank_bank_name` varchar(255) NOT NULL,
  `use_account_as_invoice` int(1) NOT NULL,
  `template_switch` varchar(100) NOT NULL DEFAULT '',
  `mega_code` varchar(255) DEFAULT NULL,
  `hotel_name` varchar(255) DEFAULT NULL,
  `zimmer` varchar(255) DEFAULT NULL,
  `gz` varchar(255) DEFAULT NULL,
  `gf` varchar(255) DEFAULT NULL,
  `gsitz` varchar(255) DEFAULT NULL,
  `handelsregister` varchar(255) DEFAULT NULL,
  `hrb` varchar(255) DEFAULT NULL,
  `vorsitz` varchar(255) DEFAULT NULL,
  `anrede1` int(1) DEFAULT NULL,
  `firstname1` varchar(255) DEFAULT NULL,
  `lastname1` varchar(255) DEFAULT NULL,
  `anrede2` int(1) DEFAULT NULL,
  `firstname2` varchar(255) DEFAULT NULL,
  `lastname2` varchar(255) DEFAULT NULL,
  `phone_lv` varchar(255) DEFAULT NULL,
  `mobile_lv` varchar(255) DEFAULT NULL,
  `fax_lv` varchar(255) DEFAULT NULL,
  `alternativ_lv` varchar(255) DEFAULT NULL,
  `sub_hotel_name` varchar(255) DEFAULT NULL,
  `email_gm` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `install_id_idx` (`install_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
--  Records of `account`
-- ----------------------------
BEGIN;
INSERT INTO `account` VALUES ('1', '1', null, null, 'No Account', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '1', null, null, '0', '0', '0', '0', '', '', '', '', '', '', '', '', '', '', '', '0', '', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
COMMIT;

-- ----------------------------
--  Table structure for `account_article`
-- ----------------------------
DROP TABLE IF EXISTS `account_article`;
CREATE TABLE `account_article` (
  `account_id` bigint(20) NOT NULL DEFAULT '0',
  `article_id` bigint(20) NOT NULL DEFAULT '0',
  PRIMARY KEY (`account_id`,`article_id`),
  KEY `article_id` (`article_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
--  Table structure for `account_paymenttype`
-- ----------------------------
DROP TABLE IF EXISTS `account_paymenttype`;
CREATE TABLE `account_paymenttype` (
  `account_id` bigint(20) NOT NULL DEFAULT '0',
  `paymenttype_id` bigint(20) NOT NULL DEFAULT '0',
  PRIMARY KEY (`account_id`,`paymenttype_id`),
  KEY `paymenttype_id` (`paymenttype_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
--  Table structure for `account_shippingtype`
-- ----------------------------
DROP TABLE IF EXISTS `account_shippingtype`;
CREATE TABLE `account_shippingtype` (
  `account_id` int(8) NOT NULL,
  `shippingtype_id` int(8) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
--  Table structure for `admin_news`
-- ----------------------------
DROP TABLE IF EXISTS `admin_news`;
CREATE TABLE `admin_news` (
  `id` int(8) NOT NULL AUTO_INCREMENT,
  `message` text NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `status` int(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
--  Table structure for `admin_translation`
-- ----------------------------
DROP TABLE IF EXISTS `admin_translation`;
CREATE TABLE `admin_translation` (
  `id` varchar(255) NOT NULL,
  `en` varchar(255) NOT NULL,
  `de` varchar(255) NOT NULL,
  `fr` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
--  Records of `admin_translation`
-- ----------------------------
BEGIN;
INSERT INTO `admin_translation` VALUES ('!!!Finish!!!', '!!!Fertig!!!', '!!!Fertig!!!', ''), ('!!!Upload!!!', '!!!Hochladen!!!', '!!!Hochladen!!!', ''), ('0%', '', '0%', ''), ('1', '1', '1', ''), ('10', 'neue Bestellung', 'neue Bestellung', ''), ('100', 'wartet auf Freigabe Alle', 'wartet auf Freigabe Alle', ''), ('110', 'Freigabe ist erfolgt', 'Freigabe ist erfolgt', ''), ('120', 'Freigabe abgelehnt', 'Freigabe abgelehnt', ''), ('130', 'Angebot abgegeben', 'Angebot abgegeben', ''), ('140', 'wartet auf Zahlungseingang', 'wartet auf Zahlungseingang', ''), ('145', 'payment received', 'Zahlung erhalten', ''), ('146', 'Mahnung', 'Mahnung', ''), ('150', 'Download steht bereit', 'Download steht bereit', ''), ('160', 'in Bearbeitung', 'in Bearbeitung', ''), ('170', 'abgebrochen oder storniert', 'abgebrochen oder storniert', ''), ('173', 'produziert', 'produziert', ''), ('175', 'Zur Abholung bereit', 'Zur Abholung bereit', ''), ('180', 'in Bearbeitung', 'Export ins Verzeichniss', ''), ('19%', '', '19%', ''), ('190', 'wartet auf Versand', 'wartet auf Versand', ''), ('20', 'neue Anfrage', 'neue Anfrage', ''), ('200', 'versendet', 'versendet', ''), ('210', 'abgeschlossen', 'abgeschlossen', ''), ('30', 'Upload offen', 'Upload offen', ''), ('40', 'Dateien hochgeladen', 'Dateien hochgeladen', ''), ('5', '', '5', ''), ('50', 'Upload abgeschlossen', 'Upload abgeschlossen', ''), ('60', 'Uploads werden Ã¼berprÃ¼ft', 'Uploads werden Ã¼berprÃ¼ft', ''), ('7%', '', '7%', ''), ('70', 'Upload Fehler', 'Upload Fehler', ''), ('78', 'Preflight wird ausgeführt', 'Preflight wird ausgeführt', ''), ('80', 'Preflightcheck Fehler', 'Preflightcheck Fehler', ''), ('84', 'Preflight Ok', 'Preflight Ok', ''), ('90', 'wartet auf Freigabe Einer', 'wartet auf Freigabe Einer', ''), ('Account', 'Account', 'Firmen', ''), ('accountregister', 'Account Register', 'accountregister', ''), ('account_edit_js_Close', 'Close', 'Schlie&szlig;en', ''), ('account_edit_js_Edit Account', 'Editing Account', 'Firma bearbeiten', ''), ('account_edit_js_Save', 'Save', 'Speichern', ''), ('account_edit_js_Save and Close', 'Save And Close', 'Speichern und schlie&szlig;en', ''), ('account_edit_js_Saving', 'Save', 'Speichern', ''), ('account_gridaccount_js_Add Account', 'Add Account', 'Firma hinzuf&uuml;gen', ''), ('account_gridaccount_js_City', 'Town/City', 'Wohnort', ''), ('account_gridaccount_js_Company', 'Account', 'Firma', ''), ('account_gridaccount_js_Delete', 'Delete', 'L&ouml;schen', ''), ('account_gridaccount_js_Edit', 'Edit', 'Bearbeiten', ''), ('account_gridaccount_js_Email', 'Email', 'Email', ''), ('account_gridaccount_js_Filter', 'Apply Filter', 'Filter anwenden', ''), ('account_gridaccount_js_Phone', 'Phone Number', 'Telefonnummer', ''), ('account_gridaccount_js_title_Accounts', 'Accounts', 'Firmen', ''), ('account_gridaccount_js_Zip', 'Zip/Postal Code', 'PLZ', ''), ('account_settings_fieldLabel_4_Shippingtype', 'Versandart', 'Versandart', ''), ('account_settings_fieldLabel_Appendix', 'Appendix', 'Zusatz', ''), ('account_settings_fieldLabel_bank_account', 'Account Number', 'Konto Nr.', ''), ('account_settings_fieldLabel_bank_code', 'Bank Code', 'BLZ', ''), ('account_settings_fieldLabel_bank_owner', 'Owner', 'Eigent&uuml;mer', ''), ('account_settings_fieldLabel_Company', 'Company', 'Firma', ''), ('account_settings_fieldLabel_Destrict', 'Destrict', 'Bundesland', ''), ('account_settings_fieldLabel_Filiale', 'Filiale von', 'Filiale von', ''), ('account_settings_fieldLabel_Locked', 'Gesperrt', 'Gesperrt', ''), ('account_settings_fieldLabel_Products', 'Products', 'Produkte', ''), ('account_settings_fieldLabel_Street', 'Adress', 'Strasse', ''), ('account_settings_fieldLabel_Tax', 'VAT', 'MwSt.', ''), ('account_settings_fieldLabel_Typ', 'Typ', 'Typ', ''), ('account_settings_fieldLabel_usid', 'VAT Code', 'USID', ''), ('account_settings_Title', 'Account', 'Firma', ''), ('account_settings_title_Bank', 'Bank Account', 'Bankdaten', ''), ('account_settings_title_finance', 'Finanz', 'Finanz', ''), ('account_settings_title_Information', 'Basic Setting', 'Grundeinstellungen', ''), ('account_settings_title_Products', 'Products', 'Produkte', ''), ('account_settings_title_Shippingtype', 'Versandart', 'Versandart', ''), ('active', 'Active', 'Aktiv', ''), ('Add Articlegroup', 'Add Articlegroup', 'Produktgruppe hinzuf&uuml;gen', ''), ('Add Contact', 'Add Adress', 'Adresse hinzuf&uuml;gen', ''), ('Add Payment', 'Add Method Of Payment', 'Zahlart hinzuf&uuml;gen', ''), ('Add Product', 'Add Product', 'Produkt hinzuf&uuml;gen', ''), ('Add Queue', 'Add Queue', 'Queue hinzuf&uuml;gen', ''), ('Add Shipping', 'Add Type Of Dispatch', 'Versandart hinzuf&uuml;gen', ''), ('Add Site', 'Add Site', 'Seite hinzuf&uuml;gen', ''), ('Additional Information', 'Additional Information', 'Zus&auml;tzliche Informationen', ''), ('Admin', 'Admin', 'Admin', ''), ('Adminpanel', 'Admin Panel', 'Adminpanel', ''), ('Adminpanellogo', 'Admin Panel Logo', 'Adminpanellogo', ''), ('admin_tree_booking', 'Buchungen', 'Buchungen', ''), ('admin_tree_cliparts', 'Cliparts', 'Cliparts', ''), ('admin_tree_cms', 'Content Managegement', 'Content Managegement', ''), ('admin_tree_creditsystem', 'Coupon', 'Gutscheine', ''), ('admin_tree_displayshop', 'Display Shop', 'Shop anzeigen', ''), ('admin_tree_filemanager', 'File Manager', 'Dateimanager', ''), ('admin_tree_motiv', 'Motives', 'Motive', ''), ('admin_tree_mymotiv', 'Motives', 'Motive', ''), ('admin_tree_myorders', 'My Orders', 'Meine Auftr&auml;ge', ''), ('admin_tree_mysettings', 'My Settings', 'Meine Einstellungen', ''), ('admin_tree_newsletter', 'Newsletter', 'Newsletter', ''), ('admin_tree_orders-customers', 'Orders & Customers', 'Auftr&auml;ge & Adressen', ''), ('admin_tree_paperdb', 'Papierdatenbank', 'Papierdatenbank', ''), ('admin_tree_paymentmethods', 'Method Of Payment', 'Zahlarten', ''), ('admin_tree_preflight', 'Preflightcheck Rules', 'Preflightcheck Regeln', ''), ('admin_tree_productgroups', 'Product Groups', 'Produktgruppen', ''), ('admin_tree_products', 'Products', 'Produkte', ''), ('admin_tree_product_templates', 'Producttemplates', 'Produktvorlagen', ''), ('admin_tree_queues', 'Queues', 'Queues', ''), ('admin_tree_shippingmethods', 'Versandarten', 'Versandarten', ''), ('admin_tree_shops', 'Shoplist', 'Shopliste', ''), ('admin_tree_shopsettings', 'Shop Setting', 'Shopeinstellungen', ''), ('admin_tree_stat', 'Statistiken', 'Statistiken', ''), ('admin_tree_system', 'Configuration', 'Systemeinstellung', ''), ('admin_tree_themes', 'Themes Groups', 'Themengruppen', ''), ('admin_tree_translation', 'Translation', '&Uuml;bersetzung', ''), ('all', 'All', 'Alles', ''), ('Ansprechpartner', 'Contact Person', 'Ansprechpartner', ''), ('appcontacts_js_Logout', 'Logout', 'Abmelden', ''), ('appcontacts_js_Menu', 'Menu', 'Men&uuml;', ''), ('appcontacts_js_MyOrders', 'My Orders', 'Meine Auftr&auml;ge', ''), ('appcontacts_js_Overview', 'Overview', '&Uuml;bersicht', ''), ('approval accepted', 'Approval Accepted', 'Freigegeben', ''), ('approval non-accepted', 'Approval Non-Accepted', 'Nicht freigegeben', ''), ('app_js_Logout', 'Logout', 'Abmelden', ''), ('app_js_Menu', 'Menu', 'Men&uuml;', ''), ('app_js_Overview', 'Overview', '&Uuml;bersicht', ''), ('app_js_User', 'User', 'Benutzer', ''), ('article', 'Product', 'Produkt', ''), ('Articlegroups', 'Product Groups', 'Produktgruppen', ''), ('articlegroup_all_js_Add Productgroup', 'Add Product Group', 'Produktgruppe hinzuf&uuml;gen', ''), ('articlegroup_all_js_Delete Productgroup', 'Delete Product Group', 'Produktgruppe l&ouml;schen', ''), ('articlegroup_all_js_Edit Productgroup', 'Edit Product Group', 'Produktgruppe bearbeiten', ''), ('articlegroup_all_js_Enable', 'Enabled', 'Aktiv', ''), ('articlegroup_all_js_Parent', 'Parent', 'Vorg&auml;nger', ''), ('articlegroup_all_js_Productgroupname', 'Name of Product Group', 'Produktgruppenname', ''), ('articlegroup_all_js_Productgroups', 'Product Group', 'Produktgruppe', ''), ('articlegroup_all_root', 'First Level', 'Oberste Ebene', ''), ('articlegroup_edit_js_Close', 'Close', 'Schlie&szlig;en', ''), ('articlegroup_edit_js_Edit Productgroup', 'Edit Product Group', 'Produktgruppe bearbeiten', ''), ('articlegroup_edit_js_Save', 'Save', 'Speichern', ''), ('articlegroup_edit_js_Save and Close', 'Speichern und schlieÃŸen', 'Speichern und schlieÃŸen', ''), ('articlegroup_edit_js_Saving', 'Save', 'Speichern', ''), ('articlegroup_settings_Articlegroup', 'Product Group', 'Produktgruppe', ''), ('articlegroup_settings_fieldLabel_Enable', 'Enabled', 'Aktiv', ''), ('articlegroup_settings_fieldLabel_Image', 'Thumbnail', 'Vorschaubild', ''), ('articlegroup_settings_fieldLabel_Language', 'Language', 'Sprache', ''), ('articlegroup_settings_fieldLabel_NotinMenu', 'Blank in Navigation ', 'In Navigation ausblenden', ''), ('articlegroup_settings_fieldLabel_Parent', 'Superior Group', '&Uuml;bergeordnete Gruppe', ''), ('articlegroup_settings_fieldLabel_Pos', 'Position', 'Position', ''), ('articlegroup_settings_fieldLabel_Text', 'Infos', 'Infos', ''), ('articlegroup_settings_fieldLabel_Title', 'Name', 'Name', ''), ('articlegroup_settings_title_Information', 'Base Setting', 'Grundeinstellungen', ''), ('Articlename', 'Product Name', 'Produktname', ''), ('articletheme_settings_fieldLabel_Parent', 'Parent', 'Vorg&auml;nger', ''), ('articletheme_settings_fieldLabel_Title', 'Titele', 'Titel', ''), ('articletheme_settings_title', 'Product Theme', 'Produktthema', ''), ('articletheme_settings_title_Theme', 'General', 'Allgemein', ''), ('article_backend_fieldLabel_a9_1', 'Farbe 1', 'Farbe 1', ''), ('article_backend_fieldLabel_a9_2', 'Farbe 2', 'Farbe 2', ''), ('article_backend_fieldLabel_a9_3', 'Farbe 3', 'Farbe 3', ''), ('article_backend_fieldLabel_a9_4', 'Farbe 4', 'Farbe 4', ''), ('article_backend_fieldLabel_a9_5', 'Farbe 5', 'Farbe 5', ''), ('article_backend_fieldLabel_Abprice', 'Price at Just', 'Ab Preis', ''), ('article_backend_fieldLabel_Articlegroups', 'Product Groups', 'Produktgruppen', ''), ('article_backend_fieldLabel_ArticlegroupsMarket', 'Productgroups (Market)', 'Produktgruppen (Market)', ''), ('article_backend_fieldLabel_Confirm', 'Approval Needed', 'Freigabebed&uuml;rftig', ''), ('article_backend_fieldLabel_Confirm One', 'Only One Contact Has to be Approved', 'Nur ein Kontakt mu&szlig; freigeben', ''), ('article_backend_fieldLabel_confirmAccount', 'Account', 'Firma', ''), ('article_backend_fieldLabel_confirmContact', 'Contact', 'Kontakt', ''), ('article_backend_fieldLabel_Customtemplates', 'use Designpresets', 'Designvorlagen benutzen', ''), ('article_backend_fieldLabel_Custom_Text', '', 'Custom Tabtext', ''), ('article_backend_fieldLabel_Custom_Title', '', 'Custom Tabname', ''), ('article_backend_fieldLabel_Display_Market', 'Show in Marketplace', 'Im Marktplatz anzeigen', ''), ('article_backend_fieldLabel_Enable', 'Enabled', 'Aktiv', ''), ('article_backend_fieldLabel_File1', 'Thumbnail 1', 'Vorschaubild 1', ''), ('article_backend_fieldLabel_File2', 'Thumbnail 2', 'Vorschaubild 2', ''), ('article_backend_fieldLabel_File3', 'Image 3', 'Bild 3', ''), ('article_backend_fieldLabel_File4', 'Image 4', 'Bild 4', ''), ('article_backend_fieldLabel_File5', 'Image 5', 'Bild 5', ''), ('article_backend_fieldLabel_File6', 'Image 6', 'Bild 6', ''), ('article_backend_fieldLabel_File7', 'Image 7', 'Bild 7', ''), ('article_backend_fieldLabel_File8', 'Bild 8', 'Bild 8', ''), ('article_backend_fieldLabel_Html', 'HTML Codeblock', 'HTML Codeblock', ''), ('article_backend_fieldLabel_Informations', 'Infotext', 'Infotext', ''), ('article_backend_fieldLabel_init_status', 'Initial Status', 'Initialstatus', ''), ('article_backend_fieldLabel_kostenstelle', 'Cost Unit', 'Kostenstelle', ''), ('article_backend_fieldLabel_Lager_File', 'Artwork File', 'Druckvorlagendatei', ''), ('article_backend_fieldLabel_Lager_File_Preview', 'Preview File', 'Vorschaudatei', ''), ('article_backend_fieldLabel_Link', 'HTML Link', 'HTML Link', ''), ('article_backend_fieldLabel_Motiv_Staffel', 'Motiv Gruppe', 'Motiv Gruppe', ''), ('article_backend_fieldLabel_Not_edit', 'No Edit', 'Als Produkt verkaufen', ''), ('article_backend_fieldLabel_Noverify', 'Without Approval', 'Ohne Freischaltung', ''), ('article_backend_fieldLabel_Pdf', 'Template Background File (PDF, PNG)', 'Template Hintergrund Datei (PDF, PNG)', ''), ('article_backend_fieldLabel_Pos', 'Position', 'Position', ''), ('article_backend_fieldLabel_Private', 'Private', 'Privat', ''), ('article_backend_fieldLabel_Private_Product', 'Shop Privat', 'Shopprodukte Login erforderlich', ''), ('article_backend_fieldLabel_Producttype', 'Product Typ', 'Produkttyp', ''), ('article_backend_fieldLabel_Question', 'Query', 'Anfrage', ''), ('article_backend_fieldLabel_Questiontheme', 'Query Themes Marketplace', 'Anfrage Themenmarktplatz', ''), ('article_backend_fieldLabel_Registration', 'Registration Possible', 'Registrierung m&ouml;glich', ''), ('article_backend_fieldLabel_releatedarticle', 'Crossselling', 'Kreuzverkauf', ''), ('article_backend_fieldLabel_releatedthememarket', 'Market Themes', 'Themen (Market)', ''), ('article_backend_fieldLabel_releatedthememarkt', 'Themes', 'Themen', ''), ('article_backend_fieldLabel_releatedthemes', 'Themes', 'Themen', ''), ('article_backend_fieldLabel_Reorder', 'Reorder', 'Nachbestellung', ''), ('article_backend_fieldLabel_Reordertheme', 'Reorder Themes Marketplace', 'Nachbestellung Themenmarktplatz', ''), ('article_backend_fieldLabel_Resale', 'Retail', 'Weiterverkaufen zulassen', ''), ('article_backend_fieldLabel_Resale_Design', 'Als Designvorlage', 'Als Designvorlage', ''), ('article_backend_fieldLabel_simple_preflight', 'Simple Preflight', 'Einfacher Preflightcheck', ''), ('article_backend_fieldLabel_Special', 'Allow Query', 'Anfrage zulassen', ''), ('article_backend_fieldLabel_Tax', 'VAT', 'MwSt.', ''), ('article_backend_fieldLabel_Template Admin', 'Admin Approval', 'Adminfreigabe', ''), ('article_backend_fieldLabel_Template System Operator', 'System Operator Approval', 'System Betreiberfreigabe', ''), ('article_backend_fieldLabel_template_navi_display_all', 'Show All Product Groups', 'Alle Produktgruppen anzeigen', ''), ('article_backend_fieldLabel_text_art', 'Art', 'Art', ''), ('article_backend_fieldLabel_text_format', 'Format', 'Format', ''), ('article_backend_fieldLabel_Title', 'Product Name', 'Produktname', ''), ('article_backend_fieldLabel_Upload', 'Approve Upload', 'Upload zulassen', ''), ('article_backend_fieldLabel_upload_article', 'Upload in Product', 'Upload im Produkt', ''), ('article_backend_fieldLabel_upload_article_status', '', 'Upload im Produktstatus', ''), ('article_backend_fieldLabel_upload_article_sum', 'Upload Article', 'Jetzt Hochladen', ''), ('article_backend_fieldLabel_upload_center', 'Upload in Center', 'Upload im Uploadcenter', ''), ('article_backend_fieldLabel_upload_center_status', 'Upload in Center Status', 'Upload im Centerstatus', ''), ('article_backend_fieldLabel_upload_center_sum', 'Uploadcenter', 'Upload in der Auftragsverwaltung', ''), ('article_backend_fieldLabel_upload_email', 'Upload by Email', 'per Email', ''), ('article_backend_fieldLabel_upload_email_status', 'Upload by Email Status', 'Upload per Emailstatus', ''), ('article_backend_fieldLabel_upload_email_sum', 'Upload EMail', 'Daten&uuml;bergabe per Email', ''), ('article_backend_fieldLabel_upload_post', 'Upload by Mail', 'per Post', ''), ('article_backend_fieldLabel_upload_post_status', 'Upload by Mail Status', 'Upload per Poststatus', ''), ('article_backend_fieldLabel_upload_post_sum', 'Upload Post', 'Daten&uuml;bergabe per Post', ''), ('article_backend_fieldLabel_upload_templateprint_sum', 'Templateprint', 'Templateprint', ''), ('article_backend_fieldLabel_upload_weblayouter', 'Upload by Weblayouter', 'jetzt Online Gestalten', ''), ('article_backend_fieldLabel_upload_weblayouter_status', 'Upload by Weblayouter Status', 'Upload per Weblayouterstatus', ''), ('article_backend_fieldLabel_upload_weblayouter_sum', 'Upload Weblayouter', 'Online Gestalten', ''), ('article_backend_fieldLabel_Used', 'Sale', 'AbverkÃ¤ufe', ''), ('article_backend_fieldLabel_Useemailaslogin', 'Email as Login', 'Email als Login', ''), ('article_backend_fieldLabel_Uselanguage', 'Use Language', 'Sprachen benutzen?', ''), ('article_backend_fieldLabel_Versandart', 'Mailing  Expenses  Calculation Type', 'Versandkosten Berechnungsart', ''), ('article_backend_fieldLabel_Vorlage', 'Download Datei', 'Download Datei', ''), ('article_backend_fieldLabel_Vorlage_File', '', 'Vorlagendatei', ''), ('article_backend_fieldLabel_Vorlage_Info', '', 'Vorlageninfo', ''), ('article_backend_fieldLabel_Wert', 'Versandkosten Betrag', 'Versandkosten Betrag', ''), ('article_backend_Simple Product', '', 'article_backend_Simple Product', ''), ('article_backend_title_Admin', 'Admin', 'Admin', ''), ('article_backend_title_Articlegroups', 'Gruppen', 'Gruppen', ''), ('article_backend_title_basic Images', 'Images', 'Bilder', ''), ('article_backend_title_basic Settings', 'Basics', 'Basics', ''), ('article_backend_title_Freigabe', 'Freigaben', 'Freigaben', ''), ('article_backend_title_Master', 'Master', 'Master', ''), ('article_backend_title_Pdf', 'Template Hintergrund', 'Template Hintergrund', ''), ('article_backend_title_releatedarticle', 'Marketing', 'Marketing', ''), ('article_backend_title_releatedmarkettheme', 'Marketthemen', 'Marketthemen', ''), ('article_backend_title_releatedthemes', 'Themen', 'Themen', ''), ('article_backend_title_texte', 'Beschreibung', 'Besch.', ''), ('article_backend_title_Versandkosten', 'Versandkosten', 'Versandkosten', ''), ('article_edit_js_Close', 'SchlieÃŸen', 'SchlieÃŸen', ''), ('article_edit_js_Edit Product', 'Produkt bearbeiten', 'Produkt bearbeiten', ''), ('article_edit_js_Save', 'Speichern', 'Speichern', ''), ('article_edit_js_Save and Close', 'Speichern und schlieÃŸen', 'Speichern und schlieÃŸen', ''), ('article_edit_js_Saving', 'Speichern', 'Speichern', ''), ('article_export', 'Produkt Export', 'Produkt Export', ''), ('article_index_js_add Product', 'Produkt hinzuf&uuml;gen', 'Produkt hinzuf&uuml;gen', ''), ('article_index_js_Add Shop', 'Shop hinzufÃ¼gen', 'Shop hinzufÃ¼gen', ''), ('article_index_js_Admin', 'Admin', 'Admin', ''), ('article_index_js_Copy Products', 'Produkt kopieren', 'Produkt kopieren', ''), ('article_index_js_Created', 'Erstellt am', 'Erstellt am', ''), ('article_index_js_Delete Products', 'Produkt l&ouml;schen', 'Produkt l&ouml;schen', ''), ('article_index_js_Edit Product', 'Produkt bearbeiten', 'Produkt bearbeiten', ''), ('article_index_js_Enable', 'Aktiv', 'Aktiv', ''), ('article_index_js_Productgroup', 'Produktgruppen', 'Produktgruppen', ''), ('article_index_js_Productlink', 'Shoplink', 'Shoplink', ''), ('article_index_js_Productname', 'Produktname', 'Produktname', ''), ('article_index_js_Products', 'Produkte', 'Produkte', ''), ('article_index_js_Producttyp', 'Produkttyp', 'Produkttyp', ''), ('article_index_js_Select Producttype', 'Produkt hinzufÃ¼gen', 'Produkt hinzufÃ¼gen', ''), ('article_index_js_Updated', 'GeÃ¤ndert am', 'GeÃ¤ndert am', ''), ('article_index_js_Used', 'AbverkÃ¤ufe', 'AbverkÃ¤ufe', ''), ('Artikel 1', '', 'Artikel 1', ''), ('Artikel 2', '', 'Artikel 2', ''), ('Artikel 3', '', 'Artikel 3', ''), ('Artikel 4', '', 'Artikel 4', ''), ('Artikel 5', '', 'Artikel 5', ''), ('Attachments', '', 'Attachments', ''), ('Auf Lager?', '', 'Auf Lager?', ''), ('Auflagen', '', 'Auflagen', ''), ('Auftragsnummer', '', 'Auftragsnummer', ''), ('Auftragsnummer Datum', '', 'Auftragsnummer Datum', ''), ('Author', '', 'Author', ''), ('Ãœbersicht', '', 'Ãœbersicht', ''), ('Ã–sterreich', '', 'Ã–sterreich', ''), ('Bad Words', '', 'Bad Words', ''), ('booking_edit_js_Booking', 'Buchung', 'Buchung', ''), ('booking_gridshops_js_booking_text', 'Buchungstext', 'Buchungstext', ''), ('booking_gridshops_js_booking_typ', 'Buchungstyp', 'Buchungstyp', ''), ('booking_gridshops_js_Buyer', 'K&auml;ufer', 'K&auml;ufer', ''), ('booking_gridshops_js_Creator', 'Ersteller', 'Ersteller', ''), ('booking_gridshops_js_Name', 'Shop', 'Shop', ''), ('booking_gridshops_js_payment_value', 'Wert', 'Wert', ''), ('booking_gridshops_js_product_name', 'Produktname', 'Produktname', ''), ('booking_gridshops_js_product_resale', 'Wiederverkaufspreis', 'Wiederverkaufspreis', ''), ('booking_gridshops_js_type', 'Typ', 'Typ', ''), ('booking_grid_js_Add Booking', 'Buchung hinzuf&uuml;gen', 'Buchung hinzuf&uuml;gen', ''), ('booking_grid_js_DTAUS Export', 'DTAUS Export', 'DTAUS Export', ''), ('booking_grid_js_Edit_Booking', 'Buchung bearbeiten', 'Buchung bearbeiten', ''), ('booking_grid_js_title', 'Buchungen', 'Buchungen', ''), ('booking_settings_fieldLabel_Booking Buyer City', 'K&auml;ufer Ort', 'K&auml;ufer Ort', ''), ('booking_settings_fieldLabel_Booking Buyer Mail', 'K&auml;ufer Mail', 'K&auml;ufer Mail', ''), ('booking_settings_fieldLabel_Booking Buyer Name', 'K&auml;ufer Name', 'K&auml;ufer Name', ''), ('booking_settings_fieldLabel_Booking Buyer Street', 'K&auml;ufer Strasse', 'K&auml;ufer Strasse', ''), ('booking_settings_fieldLabel_Booking Buyer Tel', 'K&auml;ufer Tel', 'K&auml;ufer Tel', ''), ('booking_settings_fieldLabel_Booking Creator City', 'Ersteller Ort', 'Ersteller Ort', ''), ('booking_settings_fieldLabel_Booking Creator Mail', 'Ersteller Mail', 'Ersteller Mail', ''), ('booking_settings_fieldLabel_Booking Creator Name', 'Ersteller Name', 'Ersteller Name', ''), ('booking_settings_fieldLabel_Booking Creator Street', 'Ersteller Strasse', 'Ersteller Strasse', ''), ('booking_settings_fieldLabel_Booking Creator Tel', 'Ersteller Tel', 'Ersteller Tel', ''), ('booking_settings_fieldLabel_Booking Text', 'Buchungstext', 'Buchungstext', ''), ('booking_settings_fieldLabel_Booking Type', 'Buchungstyp', 'Buchungstyp', ''), ('booking_settings_fieldLabel_Payment Value', 'Wert', 'Wert', ''), ('booking_settings_fieldLabel_Shop', 'Shop', 'Shop', ''), ('booking_settings_fieldLabel_Type', 'Typ', 'Typ', ''), ('booking_settings_title_Address', 'Adresse', 'Adresse', ''), ('booking_settings_title_Booking', 'Allgemeines', 'Allgemeines', ''), ('Browse Images', 'Bilder', 'Bilder', ''), ('Cancel', 'Abbrechen', 'Abbrechen', ''), ('City', 'Ort', 'Ort', ''), ('ClipartThemes_js_addShopThemes', 'Theme hinzuf&uuml;gen', 'Theme hinzuf&uuml;gen', ''), ('ClipartThemes_js_Name', 'Name', 'Name', ''), ('cliparttheme_settings_fieldLabel_Parent', 'Vorg&auml;nger', 'Vorg&auml;nger', ''), ('cliparttheme_settings_fieldLabel_Title', 'Title', 'Titel', ''), ('cliparttheme_settings_title_Theme', 'Allgemeines', 'Allgemeines', ''), ('clipart_backend_fieldLabel_images', 'Bilder', 'Bilder', ''), ('Clipart_js_addClipart', 'Clipart hinzuf&uuml;gen', 'Clipart hinzuf&uuml;gen', ''), ('Clipart_js_Clipart', 'Cliparts', 'Cliparts', ''), ('Clipart_js_Name', 'Name', 'Name', ''), ('clipart_settings_fieldLabel_Colors', 'Colors', 'Farben', ''), ('clipart_settings_fieldLabel_Parent', 'Vorg&auml;nger', 'Vorg&auml;nger', ''), ('clipart_settings_fieldLabel_Preview', 'Preview', 'Vorschau', ''), ('clipart_settings_fieldLabel_Price', 'Price', 'Preis', ''), ('clipart_settings_fieldLabel_Shops', 'Shop ausw&auml;hlen', 'Shop ausw&auml;hlen', ''), ('clipart_settings_fieldLabel_Slice', 'Slice', 'Abst&auml;nde', ''), ('clipart_settings_fieldLabel_Title', 'Title', 'Titel', ''), ('clipart_settings_title_Theme', 'Allgemeines', 'Allgemeines', ''), ('Clipart_Themes', 'Clipartthemes', 'Clipartthemes', ''), ('Close', 'SchlieÃŸen', 'SchlieÃŸen', ''), ('CMS 1', '', 'CMS 1', ''), ('CMS 2', '', 'CMS 2', ''), ('CMS 3', '', 'CMS 3', ''), ('CMS 4', '', 'CMS 4', ''), ('CMS 5', '', 'CMS 5', ''), ('cms_edit_js_Close', 'SchlieÃŸen', 'SchlieÃŸen', ''), ('cms_edit_js_CMS', 'CMS', 'CMS', ''), ('cms_edit_js_Save', 'Speichern', 'Speichern', ''), ('cms_edit_js_Save and Close', 'Speichern und schlieÃŸen', 'Speichern und schlieÃŸen', ''), ('cms_edit_js_Saving', 'Speichern', 'Speichern', ''), ('cms_gridcms_js_Add Site', 'Seite hinzuf&uuml;gen', 'Seite hinzuf&uuml;gen', ''), ('cms_gridcms_js_CMS', 'CMS', 'CMS', ''), ('cms_gridcms_js_Delete Page', 'l&ouml;schen', 'l&ouml;schen', ''), ('cms_gridcms_js_Edit Page', 'Bearbeiten', 'Bearbeiten', ''), ('cms_gridcms_js_Menu', 'Link Text', 'Link Text', ''), ('cms_gridcms_js_Name', 'Name', 'Name', ''), ('cms_gridcms_js_URL', 'URL', 'URL', ''), ('cms_settings_fieldLabel_Author', 'Author', 'Author', ''), ('cms_settings_fieldLabel_CopyMarket', 'FÃ¼r TM kopieren', 'FÃ¼r TM kopieren', ''), ('cms_settings_fieldLabel_CustomTitle', 'Custom Title', 'Seiten Titel', ''), ('cms_settings_fieldLabel_Description', 'Beschreibung', 'Besch.', ''), ('cms_settings_fieldLabel_Keywords', 'Keywords', 'Keywords', ''), ('cms_settings_fieldLabel_Language', 'Sprache', 'Sprache', ''), ('cms_settings_fieldLabel_Menu', 'Link Text', 'Link Text', ''), ('cms_settings_fieldLabel_Modul', 'Auf ext. Modul verweisen', 'Auf ext. Modul verweisen', ''), ('cms_settings_fieldLabel_NotinMenu', 'In Navigation ausblenden', 'In Navigation ausblenden', ''), ('cms_settings_fieldLabel_Parameter', 'Parameter', 'Parameter', ''), ('cms_settings_fieldLabel_Parent', 'Vorg&auml;nger', 'Vorg&auml;nger', ''), ('cms_settings_fieldLabel_Pos', 'Navigationsbar', 'Navigationsbar', ''), ('cms_settings_fieldLabel_Sort', 'Sortierung', 'Sortierung', ''), ('cms_settings_fieldLabel_Text', 'Inhalt', 'Inhalt', ''), ('cms_settings_fieldLabel_Title', 'Seitenname', 'Seitenname', ''), ('cms_settings_fieldLabel_URL', 'Seitendateiname', 'Seitendateiname', ''), ('cms_settings_title', 'CMS', 'CMS', ''), ('cms_settings_title_CMS', 'Content Management', 'Content Management', ''), ('cms_settings_title_CMS_Inhalt', 'Text', 'Text', ''), ('Config', '', 'Config', ''), ('Constrain Proportions', 'Abh&auml;ngig', 'Abh&auml;ngig', ''), ('Contact', 'Contact', 'Kunde', ''), ('contactlogin', '', 'contactlogin', ''), ('contactoverviewgrid_js_firstname', 'Vorname', 'Vorname', ''), ('contactoverviewgrid_js_lastname', 'Nachname', 'Nachname', ''), ('contactregister', '', 'contactregister', ''), ('Contacts', 'Adressen', 'Adressen', ''), ('contactverify', '', 'contactverify', ''), ('contact_edit_js_Close', 'SchlieÃŸen', 'SchlieÃŸen', ''), ('contact_edit_js_Edit Contact', 'Contact bearbeiten', 'Kunde bearbeiten', ''), ('contact_edit_js_Save', 'Speichern', 'Speichern', ''), ('contact_edit_js_Save and Close', 'Speichern und schlieÃŸen', 'Speichern und schlieÃŸen', ''), ('contact_edit_js_Saving', 'Speichern', 'Speichern', ''), ('contact_gridcontact_js_Add Contact', 'Contact hinzuf&uuml;gen', 'Kunde hinzuf&uuml;gen', ''), ('contact_gridcontact_js_City', 'Ort', 'Ort', ''), ('contact_gridcontact_js_Company', 'Firma', 'Firma', ''), ('contact_gridcontact_js_Contacts', 'Contacts', 'Kunden', ''), ('contact_gridcontact_js_Delete', 'L&ouml;schen', 'L&ouml;schen', ''), ('contact_gridcontact_js_Edit', 'Bearbeiten', 'Bearbeiten', ''), ('contact_gridcontact_js_Email', 'Email', 'Email', ''), ('contact_gridcontact_js_Export Contact', 'Export CSV', 'Export CSV', ''), ('contact_gridcontact_js_Filter', 'Filter anwenden', 'Filter anwenden', ''), ('contact_gridcontact_js_Firstname', 'Vorname', 'Vorname', ''), ('contact_gridcontact_js_Lastname', 'Nachname', 'Nachname', ''), ('contact_gridcontact_js_Phone', 'Telefon', 'Telefon', ''), ('contact_gridcontact_js_Shop', 'Shop', 'Shop', ''), ('contact_gridcontact_js_Zip', 'PLZ', 'PLZ', ''), ('contact_settings_fieldLabel_2_City', 'Stadt', 'Stadt', ''), ('contact_settings_fieldLabel_2_Firstname', 'Vorname', 'Vorname', ''), ('contact_settings_fieldLabel_2_Housenumber', 'Hausnummer', 'Hausnummer', ''), ('contact_settings_fieldLabel_2_Lastname', 'Nachname', 'Nachname', ''), ('contact_settings_fieldLabel_2_Mobil', 'Mobil', 'Mobil', ''), ('contact_settings_fieldLabel_2_Salutation', 'Anrede', 'Anrede', ''), ('contact_settings_fieldLabel_2_Street', 'Strasse', 'Strasse', ''), ('contact_settings_fieldLabel_2_Tel', 'Telefon 1', 'Telefon 1', ''), ('contact_settings_fieldLabel_2_Typ', 'Telefontyp 2', 'Telefontyp 2', ''), ('contact_settings_fieldLabel_2_Wert', 'Telefon 2', 'Telefon 2', ''), ('contact_settings_fieldLabel_2_Zip', 'PLZ', 'PLZ', ''), ('contact_settings_fieldLabel_3_Products', 'Produkte', 'Produkte', ''), ('contact_settings_fieldLabel_4_Paymenttype', 'Zahlarten', 'Zahlarten', ''), ('contact_settings_fieldLabel_4_Shops', 'Shops', 'Shops', ''), ('contact_settings_fieldLabel_8_newsletter', 'Newsletter', 'Newsletter', ''), ('contact_settings_fieldLabel_8_vonwo', 'Vonwo', 'Vonwo', ''), ('contact_settings_fieldLabel_Abteilung', 'Abteilung', 'Abteilung', ''), ('contact_settings_fieldLabel_Account', 'Account', 'Firma', ''), ('contact_settings_fieldLabel_Admin', 'Admin', 'Admin', ''), ('contact_settings_fieldLabel_Alternativ', 'Alternative Nummer', 'Alternative Nummer', ''), ('contact_settings_fieldLabel_Alternativ_bank_bank_name', 'Bankname', 'Bank Name', ''), ('contact_settings_fieldLabel_Alternativ_bank_bic', 'Bankbic', 'Bank BIC', ''), ('contact_settings_fieldLabel_Alternativ_bank_blz', 'Bankblz', 'Bank BLZ', ''), ('contact_settings_fieldLabel_Alternativ_bank_iban', 'Bankiban', 'Bank IBAN', ''), ('contact_settings_fieldLabel_Alternativ_bank_kto', 'Bankkto', 'Bank Kontonummer', ''), ('contact_settings_fieldLabel_Alternativ_bank_name', 'Bankkto Inhaber', 'Bank Kontoinhaber', ''), ('contact_settings_fieldLabel_Alternativ_Durchwahl', 'Alternative Durchwahl', 'Alternative Durchwahl', ''), ('contact_settings_fieldLabel_Alternativ_Type', 'Alternative Typ', 'Alternative Typ', ''), ('contact_settings_fieldLabel_Appendix', 'Zusatz', 'Zusatz', ''), ('contact_settings_fieldLabel_bank_iban', 'Iban', 'Iban', ''), ('contact_settings_fieldLabel_bank_swift', 'Swift', 'Swift', ''), ('contact_settings_fieldLabel_City', 'Stadt', 'Stadt', ''), ('contact_settings_fieldLabel_city_sum', 'LKZ/PLZ/Ort', 'LKZ/PLZ/Ort', ''), ('contact_settings_fieldLabel_Company', 'Firma', 'Firma', ''), ('contact_settings_fieldLabel_Department', 'Firma', 'Firma', ''), ('contact_settings_fieldLabel_Durchwahl', 'Durchwahl', 'Durchwahl', ''), ('contact_settings_fieldLabel_Email', 'Email', 'Email', ''), ('contact_settings_fieldLabel_Enable', 'Aktiv', 'Aktiv', ''), ('contact_settings_fieldLabel_Fax', 'Fax', 'Fax', ''), ('contact_settings_fieldLabel_Fax_Durchwahl', 'Fax Durchwahl', 'Fax Durchwahl', ''), ('contact_settings_fieldLabel_fax_sum', 'Vorwahl/Fax/Durchwahl', 'Vorwahl/Fax/Durchwahl', ''), ('contact_settings_fieldLabel_Fax_Vorwahl', 'Fax Vorwahl', 'Fax Vorwahl', ''), ('contact_settings_fieldLabel_Firstname', 'Vorname', 'Vorname', ''), ('contact_settings_fieldLabel_Function', 'Funktion', 'Funktion', ''), ('contact_settings_fieldLabel_Group', 'Gruppen', 'Gruppen', ''), ('contact_settings_fieldLabel_Homepage', 'Homepage', 'Homepage', ''), ('contact_settings_fieldLabel_Housenumber', 'Hausnummer', 'Hausnummer', ''), ('contact_settings_fieldLabel_Information', 'Infos', 'Infos', ''), ('contact_settings_fieldLabel_KundenNr', 'Kunden Nr', 'Kunden Nr.', ''), ('contact_settings_fieldLabel_Language', 'Sprache', 'Sprache', ''), ('contact_settings_fieldLabel_Lastname', 'Address', 'Anr./Nachname/Vorname', ''), ('contact_settings_fieldLabel_Lastname_Name', 'Nachname', 'Nachname', ''), ('contact_settings_fieldLabel_Liefer', 'Lieferadresse', 'Lieferadresse', ''), ('contact_settings_fieldLabel_Lkz', 'LKZ', 'LKZ', ''), ('contact_settings_fieldLabel_Locked', 'Gesperrt', 'Gesperrt', ''), ('contact_settings_fieldLabel_Mobil', 'Mobil', 'Mobil', ''), ('contact_settings_fieldLabel_Mobil_Durchwahl', 'Mobil Durchwahl', 'Mobil Durchwahl', ''), ('contact_settings_fieldLabel_mobil_sum', 'Vorwahl/Mobil/Durchwahl', 'Vorwahl/Mobil/Durchwahl', ''), ('contact_settings_fieldLabel_Mobil_Vorwahl', 'Mobil Vorwahl', 'Mobil Vorwahl', ''), ('contact_settings_fieldLabel_Name', 'Benutzername', 'Benutzername', ''), ('contact_settings_fieldLabel_name_sum', 'Salutaion/Lastname/Firstname', 'Anrede/Nachname/Vorname', ''), ('contact_settings_fieldLabel_Order_Number', 'Auftragsnummer', 'Auftragsnummer', ''), ('contact_settings_fieldLabel_Password', 'Passwort', 'Passwort', ''), ('contact_settings_fieldLabel_phone_alt_sum', 'Typ/Wert1/Wert2', 'Typ/Wert1/Wert2', ''), ('contact_settings_fieldLabel_phone_sum', 'Vorwahl/Telefon/Durchwahl', 'Vorwahl/Telefon/Durchwahl', ''), ('contact_settings_fieldLabel_position', 'Position', 'Position', ''), ('contact_settings_fieldLabel_Salutation', 'Anrede', 'Anrede', ''), ('contact_settings_fieldLabel_Self_department', 'Firma', 'Firma', ''), ('contact_settings_fieldLabel_Street', 'Strasse', 'Strasse', ''), ('contact_settings_fieldLabel_street_sum', 'Street/Housenumber', 'Strasse/Hausnummer', ''), ('contact_settings_fieldLabel_Tax', 'MwSt. anzeigen', 'MwSt. anzeigen', ''), ('contact_settings_fieldLabel_Tel', 'Telefon 1', 'Telefon 1', ''), ('contact_settings_fieldLabel_Telefon', 'Telefon Nummer', 'Telefon Nummer', ''), ('contact_settings_fieldLabel_Telefon_Durchwahl', 'Telefon Durchwahl', 'Telefon Durchwahl', ''), ('contact_settings_fieldLabel_Telefon_Vorwahl', 'Telefon Vorwahl', 'Telefon Vorwahl', ''), ('contact_settings_fieldLabel_title', 'Titel', 'Titel ( z.b.: Dipl. )', ''), ('contact_settings_fieldLabel_Typ', 'Telefontyp 2', 'Telefontyp 2', ''), ('contact_settings_fieldLabel_Username', 'Username', 'Username', ''), ('contact_settings_fieldLabel_USID', 'USID', 'USID', ''), ('contact_settings_fieldLabel_Wert', 'Telefon 2', 'Telefon 2', ''), ('contact_settings_fieldLabel_Zip', 'PLZ', 'PLZ', ''), ('contact_settings_title', 'Contact', 'Kunde', ''), ('contact_settings_title_Additional Information', 'Zusatz', 'Zusatz', ''), ('contact_settings_title_Groups', 'Rechte', 'Rechte', ''), ('contact_settings_title_Information', 'Grundeinstellungen', 'Grundeinstellungen', ''), ('contact_settings_title_Liefer', 'Lieferadresse', 'Lieferadresse', ''), ('contact_settings_title_Marketing', 'Marketing', 'Marketing', ''), ('contact_settings_title_Paymenttype', 'Zahlarten', 'Zahlarten', ''), ('contact_settings_title_Products', 'Produkte', 'Produkte', ''), ('contact_settings_title_Shops', 'Shopzuordnung', 'Shopzuordnung', ''), ('Copy Article', 'Produkt kopieren', 'Produkt kopieren', ''), ('Copyright', '', 'Copyright', ''), ('Count', 'Anzahl', 'Anzahl', ''), ('Create an Shop', 'Einen neuen Shop erstellen', 'Einen neuen Shop erstellen', ''), ('Creditsystem_gridCreditsystem_js_addCreditsystem', 'Gutscheinkontigent hinzufÃ¼gen', 'Gutscheinkontigent hinzufÃ¼gen', ''), ('Creditsystem_gridCreditsystem_js_Count', 'Anzahl', 'Anzahl', ''), ('Creditsystem_gridCreditsystem_js_Creditsystem', 'Gutscheine', 'Gutscheine', ''), ('Creditsystem_gridCreditsystem_js_Delete Page', 'LÃ¶schen', 'LÃ¶schen', ''), ('Creditsystem_gridCreditsystem_js_Edit Page', 'Bearbeiten', 'Bearbeiten', ''), ('Creditsystem_gridCreditsystem_js_From', 'Von', 'Von', ''), ('Creditsystem_gridCreditsystem_js_Generated', 'Generiert', 'Generiert', ''), ('Creditsystem_gridCreditsystem_js_Name', 'Name', 'Name', ''), ('Creditsystem_gridCreditsystem_js_To', 'Bis', 'Bis', ''), ('Creditsystem_gridCreditsystem_js_Used', 'Verbraucht', 'Verbraucht', ''), ('credit_backend_fieldLabel_More', 'More', 'Mehrmals benutzen', ''), ('credit_backend_fieldLabel_Percent', 'Prozent', 'Prozent', ''), ('credit_settings_fieldLabel_Articlegroup', 'Articlegroup', 'Artikelgruppe', ''), ('credit_settings_fieldLabel_Count', 'Anzahl', 'Anzahl', ''), ('credit_settings_fieldLabel_From', 'Von', 'Von', ''), ('credit_settings_fieldLabel_Pre Code', 'Gutscheincode', 'Gutscheincode ( wenn Mehrmals benutzen angehackt und Anzahl 1)', ''), ('credit_settings_fieldLabel_Product', 'Productid', 'Produkt ID', ''), ('credit_settings_fieldLabel_Title', 'Titel', 'Titel', ''), ('credit_settings_fieldLabel_To', 'Bis', 'Bis', ''), ('credit_settings_fieldLabel_Value', 'Wert', 'Wert', ''), ('credit_settings_title', 'Gutscheine', 'Gutscheine', ''), ('credit_settings_title_Credit', 'Infos', 'Infos', ''), ('Date', 'Datum', 'Datum', ''), ('deactive', 'Deactive', 'Deaktiviert', ''), ('Delete', 'L&ouml;schen', 'L&ouml;schen', ''), ('Delete Contact', 'Adresse l&ouml;schen', 'Adresse l&ouml;schen', ''), ('Delete Product', 'Produkt lï¿½schen', 'Produkt lï¿½schen', ''), ('Description', 'Beschreibung', 'Besch.', ''), ('Deutschland', '', 'Deutschland', ''), ('de_DE', '', 'de_DE', ''), ('Dimensions', 'Abmessungen', 'Abmessungen', ''), ('Directory', '', 'Directory', ''), ('Displaying {0} - {1} of {2}', 'Anzeigen {0} - {1} von {2}', 'Anzeigen {0} - {1} von {2}', ''), ('display_article_count', 'Anzahl Artikel in Liste', 'Anzahl Artikel in Liste', ''), ('display_market_count', 'Anzahl Shops in Liste', 'Anzahl Shops in Liste', ''), ('display_motiv_count', 'Anzahl Motive in Liste', 'Anzahl Motive in Liste', ''), ('display_top_modul_title', 'Navipanel', 'Navipanel', ''), ('domains_edit_js_DOMAINS', 'Domains', 'Domains', ''), ('domains_edit_js_Name', 'Name', 'Name', ''), ('domains_griddomain_js_Delete', 'L&ouml;schen', 'L&ouml;schen', ''), ('downloadable', '', 'downloadable', ''), ('Edit Articlegroup', 'Produktgruppe bearbeiten', 'Produktgruppe bearbeiten', ''), ('Edit Contact', 'Adresse bearbeiten', 'Adresse bearbeiten', ''), ('Edit Order', 'Auftrag bearbeiten', 'Auftrag bearbeiten', ''), ('Edit Payment', 'Zahlart bearbeiten', 'Zahlart bearbeiten', ''), ('Edit Product', 'Produkt bearbeiten', 'Produkt bearbeiten', ''), ('Edit Queue', 'Queue bearbeiten', 'Queue bearbeiten', ''), ('Edit Shipping', 'Versandart bearbeiten', 'Versandart bearbeiten', ''), ('Einleitung', '', 'Einleitung', ''), ('EMail', '', 'EMail', ''), ('Enable', 'Verf&uuml;gbar', 'Verf&uuml;gbar', ''), ('Execute', 'AusfÃ¼hren', 'AusfÃ¼hren', ''), ('Fax', 'Fax', 'Fax', ''), ('File', 'Datei', 'Datei', ''), ('fileExcludeExtensionFalse', 'Die Datei \'%value%\' hat das falsche Format', 'Die Datei \'%value%\' hat das falsche Format', ''), ('filemanager_index_js_Are_delete_file', 'Datei lÃ¶schen', 'Datei lÃ¶schen', ''), ('filemanager_index_js_Are_remove_folder_contents', 'Wollen Sie wirklich den Ordner und den kompletten Inhalt lÃ¶schen?', 'Wollen Sie wirklich den Ordner und den kompletten Inhalt lÃ¶schen?', ''), ('filemanager_index_js_Cannot_rename', 'Fehler beim umbenennen', 'Fehler beim umbenennen', ''), ('filemanager_index_js_Confirm_file_delete', 'Wollen Sie die Datei wirklich lÃ¶schen', 'Wollen Sie die Datei wirklich lÃ¶schen', ''), ('filemanager_index_js_Confirm_folder_delete', 'Wollen Sie den Ordner wirklich lÃ¶schen', 'Wollen Sie den Ordner wirklich lÃ¶schen', ''), ('filemanager_index_js_Confirm_multiple_file_delete', 'Mehrere Dateien lÃ¶schen', 'Mehrere Dateien lÃ¶schen', ''), ('filemanager_index_js_Date_Modified', 'GeÃ¤ndert am', 'GeÃ¤ndert am', ''), ('filemanager_index_js_Delete', 'LÃ¶schen', 'LÃ¶schen', ''), ('filemanager_index_js_Details', 'Details', 'Details', ''), ('filemanager_index_js_Download', 'Runterladen', 'Runterladen', ''), ('filemanager_index_js_Error_changing_extension', 'Fehler beim umbenennen', 'Fehler beim umbenennen', ''), ('filemanager_index_js_Files', 'Dateien', 'Dateien', ''), ('filemanager_index_js_Folders', 'Ordner', 'Ordner', ''), ('filemanager_index_js_Name', 'Name', 'Name', ''), ('filemanager_index_js_New', 'Neu', 'Neu', ''), ('filemanager_index_js_New_Folder', 'Neuer Ordner', 'Neuer Ordner', ''), ('filemanager_index_js_No_files_to_display', 'Keine Dateien zum anzeigen', 'Keine Dateien zum anzeigen', ''), ('filemanager_index_js_Rename', 'Umbenennen', 'Umbenennen', ''), ('filemanager_index_js_Size', 'GrÃ¶ÃŸe', 'GrÃ¶ÃŸe', ''), ('filemanager_index_js_Thumbnails', 'Thumbnails', 'Thumbnails', ''), ('filemanager_index_js_Type', 'Typ', 'Typ', ''), ('filemanager_index_js_Upload', 'Hochladen', 'Hochladen', ''), ('filemanager_index_js_Views', 'Ansicht', 'Ansicht', ''), ('files uploaded', '', 'files uploaded', ''), ('Filter', 'Ausw&auml;hlen', 'Ausw&auml;hlen', ''), ('Firma', '', 'Firma', ''), ('First Page', 'Erste Seite', 'Erste Seite', ''), ('Firstname', 'Vorname', 'Vorname', ''), ('Fix', '', 'Fix', ''), ('FreigabebedÃ¼rftige Bestellungen', '', 'FreigabebedÃ¼rftige Bestellungen', ''), ('From Mail', '', 'From Mail', ''), ('From Name', '', 'From Name', ''), ('General', 'Information', 'Information', ''), ('global', '', 'global', ''), ('Guest', 'Gast', 'Gast', ''), ('Hello World', 'Hallo Welt', 'Hallo Welt', ''), ('Housenumber', 'Hausnummer', 'Hausnummer', ''), ('Id', '', 'Id', ''), ('In Process', 'In Bearbeitung', 'In Bearbeitung', ''), ('Information', 'Informationen', 'Informationen', ''), ('Insert/Edit Image', 'Bild einf&uuml;gen/bearbeiten', 'Bild einf&uuml;gen/bearbeiten', ''), ('install_settings_fieldLabel_Agb', 'Agb', 'Agb', ''), ('install_settings_title_Agb', 'Agb', 'Agb', ''), ('IReport Hilfe', '', 'IReport Hilfe', ''), ('IReport Mac', '', 'IReport Mac', ''), ('IReport Windows', '', 'IReport Windows', ''), ('items', 'Positionen', 'Positionen', ''), ('keine', '', 'keine', ''), ('Keycode', '', 'Keycode', ''), ('Keywords', '', 'Keywords', ''), ('Kurzbeschreibung', '', 'Kurzbeschreibung', ''), ('Lager', '', 'Lager', ''), ('Lagerartikel?', '', 'Lagerartikel?', ''), ('Last Page', 'Letzte Seite', 'Letzte Seite', ''), ('Lastname', 'Nachname', 'Nachname', ''), ('Layouter Daten lÃ¶schen', '', 'Layouter Daten lÃ¶schen', ''), ('Lieferanschrift', '', 'Lieferanschrift', ''), ('Logout', '', 'Logout', ''), ('Mail', '', 'Mail', ''), ('Mailqueue', '', 'Mailqueue', ''), ('Meine AuftrÃ¤ge', '', 'Meine AuftrÃ¤ge', ''), ('Meine Einstellungen', '', 'Meine Einstellungen', ''), ('Mind. Anzahl Lager?', '', 'Mind. Anzahl Lager?', ''), ('Mobil', 'Mobil', 'Mobil', ''), ('Mode', 'Modus', 'Modus', ''), ('motivetheme_settings_fieldLabel_Parent', 'VorgÃ¤nger', 'VorgÃ¤nger', ''), ('motivetheme_settings_fieldLabel_Title', 'Titel', 'Titel', ''), ('motivetheme_settings_title_Theme', 'Allgemein', 'Allgemein', ''), ('Motivfav added', 'Motiv zu meinen Favoriten hinzugefÃ¼gt', 'Motiv zu meinen Favoriten hinzugefÃ¼gt', ''), ('Motivfav deleted', 'Motiv aus meinen Favoriten gelÃ¶scht', 'Motiv aus meinen Favoriten gelÃ¶scht', ''), ('MotivThemes_gridMotivThemes_js_addMotivThemes', 'Theme hinzufÃ¼gen', 'Theme hinzufÃ¼gen', ''), ('MotivThemes_gridMotivThemes_js_Delete Page', 'LÃ¶schen', 'LÃ¶schen', ''), ('MotivThemes_gridMotivThemes_js_Edit Page', 'Bearbeiten', 'Bearbeiten', ''), ('MotivThemes_gridMotivThemes_js_ExportCsv', 'Export nach CSV', 'Export nach CSV', ''), ('MotivThemes_gridMotivThemes_js_Generate', 'Generieren', 'Generieren', ''), ('MotivThemes_gridMotivThemes_js_MotivThemes', 'Motivthemen', 'Motivthemen', ''), ('MotivThemes_gridMotivThemes_js_Name', 'Name', 'Name', ''), ('motivtheme_settings_fieldLabel_Parent', 'VorgÃ¤nger', 'VorgÃ¤nger', ''), ('motivtheme_settings_title', 'Titel', 'Titel', ''), ('motiv_backend_fieldLabel_Copyright', 'Copyright', 'Copyright', ''), ('motiv_backend_fieldLabel_Download_Price', 'Download Price', 'Download Preis', ''), ('motiv_backend_fieldLabel_Informations', 'Informationen', 'Informationen', ''), ('motiv_backend_fieldLabel_Keywords', 'Keywords', 'Keywords', ''), ('motiv_backend_fieldLabel_Price', 'Preis', 'Preis', ''), ('motiv_backend_fieldLabel_Price_1', 'Staffel 1', 'Staffel 1', ''), ('motiv_backend_fieldLabel_Price_2', 'Staffel 2', 'Staffel 2', ''), ('motiv_backend_fieldLabel_Price_3', 'Staffel 3', 'Staffel 3', ''), ('motiv_backend_fieldLabel_Private', 'Nicht Verkaufen?', 'Nicht Verkaufen?', ''), ('motiv_backend_fieldLabel_releatedthememarket', 'Marktthemen', 'Themen (Market)', ''), ('motiv_backend_fieldLabel_releatedthemes', 'Themen', 'Themen', ''), ('motiv_backend_fieldLabel_Resale_Download', 'Als Download verkaufen', 'Als Download verkaufen', ''), ('motiv_backend_fieldLabel_Resale_Market', 'Im Marktplatz anzeigen', 'Im Marktplatz anzeigen', ''), ('motiv_backend_fieldLabel_Resale_Shop', 'Im Shop anzeigen', 'Im Shop anzeigen', ''), ('motiv_backend_fieldLabel_Status Text', 'Status Text', 'Status Text', ''), ('motiv_backend_fieldLabel_Tags', 'Tags', 'Tags (Tag1, Tag2, ...)', ''), ('motiv_backend_fieldLabel_Texte', 'Texte', 'Texte', ''), ('motiv_backend_fieldLabel_Title', 'Titel', 'Titel', ''), ('motiv_backend_Motiv', '', 'motiv_backend_Motiv', ''), ('motiv_backend_title_basic Settings', 'Grundeinstellungen', 'Grundeinstellungen', ''), ('motiv_backend_title_releatedthemes', 'Themen', 'Themen', ''), ('motiv_edit_js_MotivTheme', 'Motiv', 'Motiv', ''), ('Motiv_gridMotiv_js_addMotiv', 'Motiv hinzufÃ¼gen', 'Motiv hinzufÃ¼gen', ''), ('Motiv_gridMotiv_js_Delete Motiv', 'Motiv lÃ¶schen', 'Motiv lÃ¶schen', ''), ('Motiv_gridMotiv_js_Edit Motiv', 'Motiv bearbeiten', 'Motiv bearbeiten', ''), ('Motiv_gridMotiv_js_Image', 'Bild', 'Bild', ''), ('Motiv_gridMotiv_js_Motiv', 'Motive', 'Motive', ''), ('Motiv_gridMotiv_js_Name', 'Name', 'Name', ''), ('Motiv_gridMotiv_js_Private', 'Private', 'Private', ''), ('Mr', 'Herr', 'Herr', ''), ('Ms', 'Frau', 'Frau', ''), ('msg_clear_layouter', 'Layouter', 'Layouter', ''), ('msg_clear_layouter_confirm_beforetext', 'Layouterdateien lÃ¶schen', 'Layouterdateien lÃ¶schen', ''), ('msg_copy', 'Kopieren', 'Kopieren', ''), ('msg_copy_confirm_aftertext', 'Es existiert keine R&uuml;ckg&auml;ngig Funktion!', 'Es existiert keine R&uuml;ckg&auml;ngig Funktion!', ''), ('msg_copy_confirm_beforetext', 'Wirklich kopieren?', 'Wirklich kopieren?', ''), ('msg_delete', 'L&ouml;schen?', 'L&ouml;schen?', ''), ('msg_delete_confirm_aftertext', 'Es existiert keine R&uuml;ckg&auml;ngig Funktion!', 'Es existiert keine R&uuml;ckg&auml;ngig Funktion!', ''), ('msg_delete_confirm_beforetext', 'Wirklich l&ouml;schen', 'Wirklich l&ouml;schen', ''), ('msg_delete_generate_aftertext', 'generiert werden?', 'generiert werden?', ''), ('msg_generate', 'Generieren', 'Generieren', ''), ('msg_generate_confirm_beforetext', 'Soll', 'Soll', ''), ('msg_newsletter_send_aftertext', 'senden?', 'senden?', ''), ('msg_newsletter_send_beforetext', 'Wollen Sie wirklich', 'Wollen Sie wirklich', ''), ('msg_newsletter_send_test_title', '!!!TESTEN!!!', '!!!TESTEN!!!', ''), ('msg_newsletter_send_title', '!!!SENDEN!!!', '!!!SENDEN!!!', ''), ('My Shop', 'Mein Shop', 'Mein Shop', ''), ('MyAdministration', '', 'MyAdministration', ''), ('myorders_edit_js_Contact', 'Contact', 'Kunde', ''), ('myorders_edit_js_Count', 'Anzahl', 'Anzahl', ''), ('myorders_edit_js_Edit Order', 'Detailansicht', 'Detailansicht', ''), ('myorders_edit_js_Options', 'Optionen', 'Optionen', ''), ('myorders_edit_js_Overview', '&Uuml;bersicht', '&Uuml;bersicht', ''), ('myorders_edit_js_Price all', 'Gesamtpreis', 'Gesamtpreis', ''), ('myorders_edit_js_Price one', 'Einzelpreis', 'Einzelpreis', ''), ('myorders_edit_js_Productname', 'Produktname', 'Produktname', ''), ('myorders_edit_js_Products', 'Positionen', 'Positionen', ''), ('myorders_gridorder_js_Account', 'Account', 'Firma', ''), ('myorders_gridorder_js_Alias', 'Alias', 'Alias', ''), ('myorders_gridorder_js_Contact', 'Contact', 'Kunde', ''), ('myorders_gridorder_js_Date', 'Datum', 'Datum', ''), ('myorders_gridorder_js_Delete', 'L&ouml;schen', 'L&ouml;schen', ''), ('myorders_gridorder_js_Edit', 'Detailansicht', 'Detailansicht', ''), ('myorders_gridorder_js_Orders', 'Auftr&auml;ge', 'Auftr&auml;ge', ''), ('myorders_gridorder_js_Status', 'Auftragsstatus', 'Auftragsstatus', ''), ('myorders_gridorder_js_Time', 'Uhrzeit', 'Uhrzeit', ''), ('myorders_gridorder_js_Uploadcenter', 'Uploadcenter', 'Uploadcenter', ''), ('MyPrintscout Portal', 'MyPrintscout Portal', 'MyPrintscout Portal', ''), ('MyShops', '', 'MyShops', ''), ('MyWeb2Print', '', 'MyWeb2Print', ''), ('Name', '', 'Name', ''), ('New', 'Neu', 'Neu', ''), ('new order', '', 'new order', ''), ('new request', '', 'new request', ''), ('News', '', 'News', ''), ('Newsletter_gridNewsletter_js_addNewsletter', 'Newsletter hinzufÃ¼gen', 'Newsletter hinzufÃ¼gen', ''), ('Newsletter_gridNewsletter_js_Delete Page', 'LÃ¶schen', 'LÃ¶schen', ''), ('Newsletter_gridNewsletter_js_Edit Page', 'Bearbeiten', 'Bearbeiten', ''), ('Newsletter_gridNewsletter_js_Name', 'Name', 'Name', ''), ('Newsletter_gridNewsletter_js_Newsletter', 'Newsletter', 'Newsletter', ''), ('Newsletter_gridNewsletter_js_Send', 'Senden', '!!!SENDEN!!!', ''), ('Newsletter_gridNewsletter_js_Subject', 'Subject', 'Betreff', ''), ('Newsletter_gridNewsletter_js_Test', 'Testen', '!!!TESTEN!!!', ''), ('Newsletter_gridNewsletter_js_Title', 'Title', 'Titel', ''), ('newsletter_settings_fieldLabel_From_Mail', 'Von Mail', 'Von Mail', ''), ('newsletter_settings_fieldLabel_From_Name', 'Von Name', 'Von Name', ''), ('newsletter_settings_fieldLabel_Subject', 'Subject', 'Betreff', ''), ('newsletter_settings_fieldLabel_Title', 'Title', 'Titel', ''), ('newsletter_settings_tab1_title', 'Allgemeines', 'Allgemeines', ''), ('newsletter_settings_tab2_title', 'Plaintext', 'Plaintext', ''), ('newsletter_settings_tab3_title', 'Htmltext', 'Htmltext', ''), ('news_edit_js_Close', 'SchlieÃŸen', 'SchlieÃŸen', ''), ('news_edit_js_NEWS', 'News', 'News', ''), ('news_edit_js_Save', 'Speichern', 'Speichern', ''), ('news_edit_js_Save and Close', 'Speichern und schlieÃŸen', 'Speichern und schlieÃŸen', ''), ('news_edit_js_Saving', 'Speichern', 'Speichern', ''), ('news_gridnews_js_addnews', 'News hinzuf&uuml;gen', 'News hinzuf&uuml;gen', ''), ('news_gridnews_js_Delete Page', 'l&ouml;schen', 'l&ouml;schen', ''), ('news_gridnews_js_Edit Page', 'Bearbeiten', 'Bearbeiten', ''), ('news_gridnews_js_Name', 'Name', 'Name', ''), ('news_gridnews_js_NEWS', 'News', 'News', ''), ('news_settings_fieldLabel_Active', 'Active', 'Aktiv', ''), ('news_settings_fieldLabel_Einleitung', 'Einleitung', 'Einleitung', ''), ('news_settings_fieldLabel_Language', 'Sprache', 'Sprache', ''), ('news_settings_fieldLabel_Sort_Date', 'Datum', 'Datum f&uuml;r Sortierung', ''), ('news_settings_fieldLabel_Text', 'Inhalt', 'Inhalt', ''), ('news_settings_fieldLabel_Title', 'Headline', 'Headline', ''), ('news_settings_title', 'News', 'News', ''), ('news_settings_title_news', 'News', 'News', ''), ('Next Page', 'Nï¿½chste Seite', 'Nï¿½chste Seite', ''), ('No data to display', 'Kein Daten zum anzeigen', 'Kein Daten zum anzeigen', ''), ('notBetween', '\'%value%\' muÃŸ im Bereich von \'%min%\' bis \'%max%\' liegen', '\'%value%\' muÃŸ im Bereich von \'%min%\' bis \'%max%\' liegen', ''), ('notBetweenStrict', '\'%value%\' muÃŸ im Bereich von \'%min%\' bis \'%max%\' liegen', '\'%value%\' muÃŸ im Bereich von \'%min%\' bis \'%max%\' liegen', ''), ('of {0}', 'von {0}', 'von {0}', ''), ('Open Layouter', '', 'Open Layouter', ''), ('open offer', '', 'open offer', ''), ('Options', 'Optionen', 'Optionen', ''), ('ordercreated', '', 'ordercreated', ''), ('orderpackagekeychange', '', 'orderpackagekeychange', ''), ('orderposstatuschange', '', 'orderposstatuschange', ''), ('orderposuploadcomplete', '', 'orderposuploadcomplete', ''), ('Orders', 'Auftr&auml;ge', 'Auftr&auml;ge', ''), ('ordersoverviewgrid_js_account', 'Account', 'Firma', ''), ('ordersoverviewgrid_js_alias', 'Alias', 'Alias', ''), ('ordersoverviewgrid_js_status', 'Auftragsstatus', 'Auftragsstatus', ''), ('orderstatuschange', '', 'orderstatuschange', ''), ('orders_contact_fieldLabel_2_Firstname', 'Vorname', 'Vorname', ''), ('orders_contact_fieldLabel_2_Information', 'Infos', 'Infos', ''), ('orders_contact_fieldLabel_2_Lastname', 'Nachname', 'Nachname', ''), ('orders_contact_fieldLabel_2_Mobil', 'Mobil', 'Mobil', ''), ('orders_contact_fieldLabel_2_Salutation', 'Anrede', 'Anrede', ''), ('orders_contact_fieldLabel_2_Tel', 'Telefon 1', 'Telefon 1', ''), ('orders_contact_fieldLabel_2_Typ', 'Telefontyp 2', 'Telefontyp 2', ''), ('orders_contact_fieldLabel_2_Wert', 'Telefon 2', 'Telefon 2', ''), ('orders_contact_fieldLabel_Firstname', 'Vorname', 'Vorname', ''), ('orders_contact_fieldLabel_Information', 'Infos', 'Infos', ''), ('orders_contact_fieldLabel_Lastname', 'Nachname', 'Nachname', ''), ('orders_contact_fieldLabel_Mobil', 'Mobil', 'Mobil', ''), ('orders_contact_fieldLabel_Password', 'Passwort', 'Passwort', ''), ('orders_contact_fieldLabel_Salutation', 'Anrede', 'Anrede', ''), ('orders_contact_fieldLabel_Tel', 'Telefon 1', 'Telefon 1', ''), ('orders_contact_fieldLabel_Typ', 'Telefontyp 2', 'Telefontyp 2', ''), ('orders_contact_fieldLabel_Username', 'Username', 'Username', ''), ('orders_contact_fieldLabel_Wert', 'Telefon 2', 'Telefon 2', ''), ('orders_contact_Settings', 'Order', 'Auftrag', ''), ('orders_contact_title_2_Additional Information', 'Appendix', 'Zusatz', ''), ('orders_contact_title_Information', 'Contact Details', 'Kontakdaten', ''), ('orders_edit_js_Close', 'Close', 'Schlie&szlig;en', ''), ('orders_edit_js_Contact', 'Contact', 'Kunde', ''), ('orders_edit_js_Count', 'Quantity', 'Anzahl', ''), ('orders_edit_js_delivery ticket', 'Delivery Note', 'Lieferschein', ''), ('orders_edit_js_Edit Order', 'Process Order', 'Auftrag bearbeiten', ''), ('orders_edit_js_Invoice', 'Invoice', 'Rechnung', ''), ('orders_edit_js_Jobticket', 'Job Ticket', 'Jobticket', ''), ('orders_edit_js_Offer', 'Offer', 'Angebot', ''), ('orders_edit_js_Options', 'Optionen', 'Optionen', ''), ('orders_edit_js_Order', 'Order', 'Auftrag', ''), ('orders_edit_js_Overview', 'Overview', '&Uuml;bersicht', ''), ('orders_edit_js_Price all', 'Total Price', 'Gesamtpreis', ''), ('orders_edit_js_Price one', 'Unit Price', 'Einzelpreis', ''), ('orders_edit_js_Print delivery ticket', 'Print Delivery Note', 'Lieferschein drucken', ''), ('orders_edit_js_Print Invoice', 'Print Invoice', 'Rechnnung drucken', ''), ('orders_edit_js_Print Jobticket', 'Print Jobticket', 'Jobticket drucken', ''), ('orders_edit_js_Print Offer', 'Print Offer', 'Angebot drucken', ''), ('orders_edit_js_Print Order', 'Print Order', 'Auftrag drucken', ''), ('orders_edit_js_Productame', 'Product Name', 'Produktname', ''), ('orders_edit_js_Products', 'Positions', 'Positionen', ''), ('orders_edit_js_product_details', 'Product Details', 'Produktdetails', ''), ('orders_edit_js_product_options', 'Options', 'Optionen', ''), ('orders_edit_js_Save', 'Save', 'Speichern', ''), ('orders_edit_js_Save and Close', 'Save And Close', 'Speichern und schlieÃŸen', ''), ('orders_edit_js_Saving', 'Save', 'Speichern', ''), ('orders_edit_js_Special', 'Special Request', 'Spezial Anfrage', ''), ('orders_gridorder_js_Account', 'Account', 'Firma', ''), ('orders_gridorder_js_Alias', 'Alias', 'Alias', ''), ('orders_gridorder_js_Contact', 'Contact', 'Kunde', ''), ('orders_gridorder_js_Date', 'Datum', 'Datum', ''), ('orders_gridorder_js_Delete', 'L&ouml;schen', 'L&ouml;schen', ''), ('orders_gridorder_js_Download_Package', 'Download Package', 'Paket downloaden', ''), ('orders_gridorder_js_Edit', 'Bearbeiten', 'Bearbeiten', ''), ('orders_gridorder_js_Filter', 'Filter anwenden', 'Filter anwenden', ''), ('orders_gridorder_js_Orders', 'Auftr&auml;ge', 'Auftr&auml;ge', ''), ('orders_gridorder_js_OrgArticle', 'Source Produkt', 'Source Produkt', ''), ('orders_gridorder_js_Product', 'Preis', 'Preis', ''), ('orders_gridorder_js_ResalePrice', 'Mein Aufschlag', 'Mein Aufschlag', ''), ('orders_gridorder_js_Shop', 'Shop', 'Shop', ''), ('orders_gridorder_js_Status', 'Auftragsstatus', 'Auftragsstatus', ''), ('orders_gridorder_js_Time', 'Uhrzeit', 'Uhrzeit', ''), ('orders_gridorder_js_Uploadcenter', 'Uploadcenter', 'Uploadcenter', ''), ('orders_order_fieldLabel_2_Info', 'Info', 'Info', ''), ('orders_order_fieldLabel_2_Package', 'Auftrag', 'Paketnummer', ''), ('orders_order_fieldLabel_2_Status', 'Status', 'Status', ''), ('orders_order_fieldLabel_Alias', 'Alias', 'Alias', ''), ('orders_order_fieldLabel_Basketfield1', 'Warenkorb Info 1', 'Warenkorb Info 1', ''), ('orders_order_fieldLabel_Basketfield2', 'Warenkorb Info 2', 'Warenkorb Info 2', ''), ('orders_order_fieldLabel_Brutto', 'Brutto', 'Brutto', ''), ('orders_order_fieldLabel_Netto', 'Netto', 'Netto', ''), ('orders_order_fieldLabel_Package', 'Auftrag', 'Auftrag', ''), ('orders_order_fieldLabel_Paymenttype', 'Zahlart', 'Zahlart', ''), ('orders_order_fieldLabel_Status', 'Status', 'Status', ''), ('orders_order_Settings', 'Auftrag', 'Auftrag', ''), ('orders_order_title_Edit', 'Bearbeiten', 'Bearbeiten', ''), ('orders_order_title_Overview', '&Uuml;bersicht', '&Uuml;bersicht', ''), ('Ort', '', 'Ort', ''), ('OtherShop', 'Anderer Shop', 'Anderer Shop', ''), ('overview_js_newcontacts', 'neue Contacts', 'neue Kunden', ''), ('overview_js_neworders', 'neue Auftr&auml;ge', 'neue Auftr&auml;ge', ''), ('overview_js_overview', '&Uuml;berblick', '&Uuml;berblick', ''), ('Page', 'Seite', 'Seite', ''), ('Pageobjects', '', 'Pageobjects', ''), ('Pages', '', 'Pages', ''), ('Pagetemplates', '', 'Pagetemplates', ''), ('paperdb_settings_fieldLabel_Paper', 'Papiere', 'Papiere', ''), ('paperdb_settings_fieldLabel_Papercontainer', 'Papiercontainer', 'Papiercontainer', ''), ('paperdb_settings_Settings', 'Papierdatenbank', 'Papierdatenbank', ''), ('paperdb_settings_title_Paper', 'Papierdatenbank', 'Papierdatenbank', ''), ('paperdb_settings_title_Papercontainer', 'Papiercontainer', 'Papiercontainer', ''), ('PapierDB_gridPapierDB_js_PapierDB', 'Papiere', 'Papiere', ''), ('PapierDB_grid_js_add', 'Papier hinzuf&uuml;gen', 'Papier hinzuf&uuml;gen', ''), ('papier_all_js_artnr', 'Nr', 'Nr', ''), ('papier_all_js_description_1', 'Name 1', 'Name 1', ''), ('papier_all_js_description_2', 'Name 2', 'Name 2', ''), ('papier_all_js_grammatur', 'Grammatur', 'Grammatur', ''), ('papier_all_js_preis', 'Preis', 'Preis', ''), ('papier_edit_js_Papier', 'Papier', 'Papier', ''), ('papier_settings_fieldLabel_ArtNr', 'Nr', 'Nr', ''), ('papier_settings_fieldLabel_Description_1', 'Name 1', 'Name 1', ''), ('papier_settings_fieldLabel_Description_2', 'Name 2', 'Name 2', ''), ('papier_settings_fieldLabel_Grammatur', 'Grammatur', 'Grammatur', ''), ('papier_settings_fieldLabel_Preis', 'Preis', 'Preis', ''), ('papier_settings_title_General', 'Allgemein', 'Allgemein', ''), ('Params', '', 'Params', ''), ('Parent', 'Vorg&auml;nger', 'Vorg&auml;nger', ''), ('Password', 'Kennwort', 'Kennwort', ''), ('Payments', 'Zahlarten', 'Zahlarten', ''), ('Paymenttype', 'Zahlarten', 'Zahlarten', ''), ('payment_all_js_Add Payment', 'Zahlart hinzuf&uuml;gen', 'Zahlart hinzuf&uuml;gen', ''), ('payment_all_js_Delete', 'L&ouml;schen', 'L&ouml;schen', ''), ('payment_all_js_Edit', 'Bearbeiten', 'Bearbeiten', ''), ('payment_all_js_Paymentmethods', 'Zahlarten', 'Zahlarten', ''), ('payment_all_js_prozent', 'Prozentual', 'Prozentual', ''), ('payment_all_js_title', 'Bezeichnung', 'Bezeichnung', ''), ('payment_all_js_wert', 'Wert', 'Wert', ''), ('payment_edit_js_Close', 'SchlieÃŸen', 'SchlieÃŸen', ''), ('payment_edit_js_Edit Payment', 'Zahlart bearbeiten', 'Zahlart bearbeiten', ''), ('payment_edit_js_Save', 'Speichern', 'Speichern', ''), ('payment_edit_js_Save and Close', 'Speichern und schlieÃŸen', 'Speichern und schlieÃŸen', ''), ('payment_edit_js_Saving', 'Speichern', 'Speichern', ''), ('payment_settings_fieldLabel_Private', 'Privat', 'Privat', ''), ('payment_settings_fieldLabel_Prozentual', 'Prozentual', 'Prozentual', ''), ('payment_settings_fieldLabel_title', 'Bezeichnung', 'Bezeichnung', ''), ('payment_settings_fieldLabel_Wert', 'Wert', 'Wert', ''), ('payment_settings_Payment', 'Zahlart', 'Zahlart', ''), ('payment_settings_title_Information', '&Uuml;bersicht', '&Uuml;bersicht', ''), ('Phone', 'Telefon', 'Telefon', ''), ('please wait .....', 'Bitte warten .....', 'Bitte warten .....', ''), ('Pos', '', 'Pos', ''), ('Position', '', 'Position', ''), ('preflightcheck error', '', 'preflightcheck error', ''), ('preflight_gridshops_js_Name', 'Name', 'Name', ''), ('preflight_grid_js_Add Preflight', 'Regel hinzuf&uuml;gen', 'Preflightcheck Regeln', ''), ('preflight_grid_js_title', 'Preflightcheck Regeln', 'Preflightcheck Regeln', ''), ('Preis', '', 'Preis', ''), ('Preise & Kalkulation', '', 'Preise & Kalkulation', ''), ('Previous Page', 'Vorherige Seite', 'Vorherige Seite', ''), ('Price all', 'Gesamtpreis', 'Gesamtpreis', ''), ('Price one', 'Einzelpreis', 'Einzelpreis', ''), ('Printdate', '', 'Printdate', ''), ('printdate_edit_js_Close', 'SchlieÃŸen', 'SchlieÃŸen', ''), ('printdate_edit_js_PRINTDATE', 'Printdates', 'Printdates', ''), ('printdate_edit_js_Save', 'Speichern', 'Speichern', ''), ('printdate_edit_js_Save and Close', 'Speichern und schlieÃŸen', 'Speichern und schlieÃŸen', ''), ('printdate_edit_js_Saving', 'Speichern', 'Speichern', ''), ('printdate_gridprintdate_js_add', 'Printdate hinzufÃ¼gen', 'Printdate hinzufÃ¼gen', ''), ('printdate_gridprintdate_js_Delete Printdate', 'Printdate lÃ¶schen', 'Printdate lÃ¶schen', ''), ('printdate_gridprintdate_js_Edit Printdate', 'Printdate bearbeiten', 'Printdate bearbeiten', ''), ('printdate_gridprintdate_js_Printdate', 'Printdates', 'Printdates', ''), ('printdate_gridprintdate_js_startdate', 'Startdate', 'Startdate', ''), ('printdate_settings_fieldLabel_Startdate', 'Startdate', 'Startdate', ''), ('printdate_settings_title', 'Printdate', 'Printdate', ''), ('printdate_settings_title_printdate', '&Uuml;bersicht', '&Uuml;bersicht', ''), ('PrintshopCreator', 'PrintshopCreator', 'PrintshopCreator', ''), ('Products', 'Produkte', 'Produkte', ''), ('ProductThemes_gridProductThemes_js_addProductThemes', 'Thema hinzufÃ¼gen', 'Thema hinzufÃ¼gen', ''), ('ProductThemes_gridProductThemes_js_Delete Page', 'LÃ¶schen', 'LÃ¶schen', ''), ('ProductThemes_gridProductThemes_js_Edit Page', 'Bearbeiten', 'Bearbeiten', ''), ('ProductThemes_gridProductThemes_js_Name', 'Name', 'Name', ''), ('ProductThemes_gridProductThemes_js_ProductThemes', 'Produktthemen', 'Produktthemen', ''), ('Queues', 'Queues', 'Queues', ''), ('queue_all_js_add Queue', 'Queue hinzuf&uuml;gen', 'Queue hinzuf&uuml;gen', ''), ('queue_all_js_Delete', 'L&ouml;schen', 'L&ouml;schen', ''), ('queue_all_js_Edit', 'Bearbeiten', 'Bearbeiten', ''), ('queue_all_js_Enable', 'Aktiv', 'Aktiv', ''), ('queue_all_js_Name', 'Bezeichnung', 'Bezeichnung', ''), ('queue_all_js_Queues', 'Queues', 'Queues', ''), ('queue_all_js_Select Queuetyp', 'Queuetyp selektieren', 'Queuetyp selektieren', ''), ('queue_all_js_Typ', 'Typ', 'Typ', ''), ('queue_edit_js_Close', 'SchlieÃŸen', 'SchlieÃŸen', ''), ('queue_edit_js_Edit', 'Queue bearbeiten', 'Queue bearbeiten', ''), ('queue_edit_js_Save', 'Speichern', 'Speichern', ''), ('queue_edit_js_Save and Close', 'Speichern und schlieÃŸen', 'Speichern und schlieÃŸen', ''), ('queue_edit_js_Saving', 'Speichern', 'Speichern', ''), ('Refresh', 'Neu laden', 'Neu laden', ''), ('Remote Options', '', 'Remote Options', ''), ('Salutation', 'Anrede', 'Anrede', ''), ('Save', 'Speichern', 'Speichern', ''), ('Save and Close', 'Speichern und SchlieÃŸen', 'Speichern und SchlieÃŸen', ''), ('Schweiz', '', 'Schweiz', ''), ('Select', 'Ausw&auml;hlen', 'Ausw&auml;hlen', ''), ('select pos', 'Position ausw&auml;hlen', 'Position ausw&auml;hlen', ''), ('Select Product Type', 'Produkttyp wï¿½hlen', 'Produkttyp wï¿½hlen', ''), ('Select Queue Type', 'Queuevariante ausw&auml;hlen', 'Queuevariante ausw&auml;hlen', ''), ('Send Delivery with Mail', '', 'Send Delivery with Mail', ''), ('Send Invoice with Mail', '', 'Send Invoice with Mail', ''), ('Send Jobtiket with Mail', '', 'Send Jobtiket with Mail', ''), ('Send Offer with Mail', '', 'Send Offer with Mail', ''), ('Send Order with Mail', '', 'Send Order with Mail', ''), ('Settings', 'Benutzereinstellungen', 'Benutzereinstellungen', ''), ('Settings find', 'Einstellungen finden', 'Einstellungen finden', ''), ('Settings for Other Shop', 'Einstellungen fÃ¼r andere Shops', 'Einstellungen fÃ¼r andere Shops', ''), ('Shippingtype', 'Versandarten & Kosten', 'Versandarten & Kosten', ''), ('shipping_all_js_Add Shipping', 'Versandart hinzuf&uuml;gen', 'Versandart hinzuf&uuml;gen', ''), ('shipping_all_js_Art', 'Versandart', 'Versandart', ''), ('shipping_all_js_Delete', 'L&ouml;schen', 'L&ouml;schen', ''), ('shipping_all_js_Edit', 'Bearbeiten', 'Bearbeiten', ''), ('shipping_all_js_Kosten A', 'Kosten A', 'Kosten A', ''), ('shipping_all_js_Kosten B', 'Kosten B', 'Kosten B', ''), ('shipping_all_js_Kosten Fix', 'Kosten Fix', 'Kosten Fix', ''), ('shipping_all_js_Land', 'Land', 'Land', ''), ('shipping_all_js_Shippings', 'Versandarten', 'Versandarten', ''), ('shipping_all_js_Title', 'Bezeichnung', 'Bezeichnung', ''), ('shipping_edit_js_Close', 'SchlieÃŸen', 'SchlieÃŸen', ''), ('shipping_edit_js_Edit Shipping', 'Versandart bearbeiten', 'Versandart bearbeiten', ''), ('shipping_edit_js_Save', 'Speichern', 'Speichern', ''), ('shipping_edit_js_Save and Close', 'Speichern und schlieÃŸen', 'Speichern und schlieÃŸen', ''), ('shipping_edit_js_Saving', 'Speichern', 'Speichern', ''), ('shipping_settings_fieldLabel_Art', 'Versandart', 'Versandart', ''), ('shipping_settings_fieldLabel_Kosten A', 'Kosten A', 'Kosten A', ''), ('shipping_settings_fieldLabel_Kosten B', 'Kosten B', 'Kosten B', ''), ('shipping_settings_fieldLabel_Kosten C', 'Kosten C', 'Kosten C', ''), ('shipping_settings_fieldLabel_Kosten D', 'Kosten D', 'Kosten D', ''), ('shipping_settings_fieldLabel_Kosten E', 'Kosten E', 'Kosten E', ''), ('shipping_settings_fieldLabel_Kosten F', 'Kosten F', 'Kosten F', ''), ('shipping_settings_fieldLabel_Kosten Fix', 'Kosten Fix', 'Kosten Fix', ''), ('shipping_settings_fieldLabel_Land', 'Land', 'Land', ''), ('shipping_settings_fieldLabel_Tax', 'MwSt.', 'MwSt.', ''), ('shipping_settings_fieldLabel_Title', 'Bezeichnung', 'Bezeichnung', ''), ('shipping_settings_fieldLabel_Weight From', 'Weight From(gr.)', 'Gewicht von (gramm)', ''), ('shipping_settings_fieldLabel_Weight To', 'Weight To (gramm)', 'Gewicht bis (gramm)', ''), ('shipping_settings_Shipping', 'Versandart', 'Versandart', ''), ('shipping_settings_title_Information', '&Uuml;bersicht', '&Uuml;bersicht', ''), ('shipping_settings_title_Matrix', 'Matrix', 'Matrix', ''), ('Shop Operator', 'Shopbetreiber', 'Shopbetreiber', ''), ('Shops', 'Shops', 'Shops', ''), ('shops_gridshops_js_Copy Shop', 'Shop kopieren', 'Shop kopieren', ''), ('shops_gridshops_js_Edit Domains', 'Domains bearbeiten', 'Domains bearbeiten', ''), ('shops_gridshops_js_Name', 'Name', 'Name', '');
INSERT INTO `admin_translation` VALUES ('shops_gridshops_js_Shopcontact', 'Ersteller', 'Ersteller', ''), ('shops_gridshops_js_Shoplink', 'Shoplink', 'Shoplink', ''), ('shops_gridshops_js_SHOPS', 'Shops', 'Shops', ''), ('shops_gridshops_js_Shoptyp', 'Typ', 'Typ', ''), ('ShopThemes_gridShopThemes_js_addShopThemes', 'Shoptheme hinzufÃ¼gen', 'Shoptheme hinzufÃ¼gen', ''), ('ShopThemes_gridShopThemes_js_Delete Page', 'Shoptheme lÃ¶schen', 'Shoptheme lÃ¶schen', ''), ('ShopThemes_gridShopThemes_js_Edit Page', 'Shoptheme bearbeiten', 'Shoptheme bearbeiten', ''), ('ShopThemes_gridShopThemes_js_Name', 'Name', 'Name', ''), ('ShopThemes_gridShopThemes_js_ShopThemes', 'Shopthemen', 'Shopthemen', ''), ('shoptheme_settings_fieldLabel_Parent', 'VorgÃ¤nger', 'VorgÃ¤nger', ''), ('shoptheme_settings_fieldLabel_Title', 'Titel', 'Titel', ''), ('shoptheme_settings_title', 'Shoptheme', 'Shoptheme', ''), ('shoptheme_settings_title_Theme', 'Shoptheme', 'Shoptheme', ''), ('shop_settings_fieldLabel_Address', 'Adresse', 'Adresse', ''), ('shop_settings_fieldLabel_Basket_1', 'Warenkorb Feld 1', 'Warenkorb Feld 1', ''), ('shop_settings_fieldLabel_Basket_2', 'Warenkorb Feld 2', 'Warenkorb Feld 2', ''), ('shop_settings_fieldLabel_BrowserIcon', 'BrowserIcon', 'BrowserIcon', ''), ('shop_settings_fieldLabel_Company', 'Firma', 'Firma', ''), ('shop_settings_fieldLabel_Css', 'CSS', 'CSS', ''), ('shop_settings_fieldLabel_Default Mode', 'Startseite', 'Startseite', ''), ('shop_settings_fieldLabel_Default Parameter', 'Startparameter', 'Startparameter', ''), ('shop_settings_fieldLabel_default_account', 'Default Account', 'Default Firma', ''), ('shop_settings_fieldLabel_Display_Lead_Box_Modul', 'Aufmacher', 'Aufmacher anzeigen', ''), ('shop_settings_fieldLabel_Display_Resale_Modul', '', 'Slider anzeigen', ''), ('shop_settings_fieldLabel_Display_Top_Modul', '', 'Top Modul anzeigen', ''), ('shop_settings_fieldLabel_Email', 'EMail', 'EMail', ''), ('shop_settings_fieldLabel_Formel', 'Formel', 'Formel', ''), ('shop_settings_fieldLabel_Frommail', 'Absenderemail', 'Absenderemail', ''), ('shop_settings_fieldLabel_Fromname', 'Absendername', 'Absendername', ''), ('shop_settings_fieldLabel_Ftp Password', 'Passwort', 'Passwort', ''), ('shop_settings_fieldLabel_Ftp Username', 'Benutzername', 'Benutzername', ''), ('shop_settings_fieldLabel_google_Analytics', 'G. Analytics Code', 'G. Analytics Code', ''), ('shop_settings_fieldLabel_Hostname', 'Server', 'Server', ''), ('shop_settings_fieldLabel_Land', 'Land', 'Land', ''), ('shop_settings_fieldLabel_Layout', 'Shoplayout', 'Shoplayout', ''), ('shop_settings_fieldLabel_Layouter Presets', 'Voreinstellungen', 'Voreinstellungen', ''), ('shop_settings_fieldLabel_Logo 1', 'Topbanner', 'Topbanner', ''), ('shop_settings_fieldLabel_Logo 2', 'Bild 2', 'Bild 2', ''), ('shop_settings_fieldLabel_market', 'Als Themenmarktplatz verÃ¶ffentlichen', 'Als Themenmarktplatz verÃ¶ffentlichen', ''), ('shop_settings_fieldLabel_Name', 'Name', 'Name', ''), ('shop_settings_fieldLabel_Parameter', 'Parameter', 'Parameter', ''), ('shop_settings_fieldLabel_Password', 'Passwort', 'Passwort', ''), ('shop_settings_fieldLabel_preview', 'Preview Image (170x130)', 'Preview Image (170x130)', ''), ('shop_settings_fieldLabel_releatedthemeshop', 'Theme', 'Theme', ''), ('shop_settings_fieldLabel_report Site1', 'Report Seite 1 Background PDF/PNG/JPG', 'Report Seite 1 Background PDF/PNG/JPG', ''), ('shop_settings_fieldLabel_report Site2', 'Report Seite 2 Background PDF/PNG/JPG', 'Report Seite 2 Background PDF/PNG/JPG', ''), ('shop_settings_fieldLabel_Sliderlogo', 'Slider Logo', 'Slider Logo', ''), ('shop_settings_fieldLabel_Street', 'StraÃŸe', 'StraÃŸe', ''), ('shop_settings_fieldLabel_Subtitle', 'Untertitel', 'Untertitel', ''), ('shop_settings_fieldLabel_Title', 'Titel', 'Titel', ''), ('shop_settings_fieldLabel_Top_Modul_Settings', '', 'Top Modul Einstellungen', ''), ('shop_settings_fieldLabel_UID', 'Shop UID', 'Shop UID', ''), ('shop_settings_fieldLabel_Username', 'Username', 'Username', ''), ('shop_settings_fieldLabel_Usethis', 'Eigener Server?', 'Eigener Server?', ''), ('shop_settings_fieldLabel_Web_title', 'Title', 'Titel', ''), ('shop_settings_Settings', 'Shopsettings', 'Shopsettings', ''), ('shop_settings_title_Information', 'Shopsettings', 'Shopsettings', ''), ('shop_settings_title_Services', 'Services', 'Services', ''), ('size', 'Gr&ouml;ÃŸe (bytes)', 'Gr&ouml;ÃŸe (bytes)', ''), ('Source', 'Quelle', 'Quelle', ''), ('Strasse', '', 'Strasse', ''), ('Street', 'StraÃŸe', 'StraÃŸe', ''), ('Subject', '', 'Subject', ''), ('System Operator', 'Systembetreiber', 'Systembetreiber', ''), ('Systemoperatorcontrolcenter', 'Systemoperatorcontrolcenter', 'Systemoperatorcontrolcenter', ''), ('Tags (Tag1,Tag2)', '', 'Tags (Tag1,Tag2)', ''), ('Tel', 'Telefon', 'Telefon', ''), ('template_display_help', 'Hilfebereich anzeigen', 'Hilfebereich anzeigen', ''), ('template_display_help_cms', 'Hilfecmsseite', 'Hilfecmsseite', ''), ('template_display_lead_box', 'Aufmacher CMS Seite', 'Aufmacher CMS Seite', ''), ('template_display_lead_box_products', 'Leadbox Products', 'Aufmacher Produkte', ''), ('template_display_lesezeichen', 'Lesezeichen anzeigen', 'Lesezeichen anzeigen', ''), ('template_display_motive', 'TopNavi Motive', 'TopNavi Motive', ''), ('template_display_motive_all', 'ProdMot Alle', 'ProdMot Alle', ''), ('template_display_motive_fav', 'ProdMot Favoriten', 'ProdMot Favoriten', ''), ('template_display_motive_my', 'ProdMot eigene Motive', 'ProdMot eigene Motive', ''), ('template_display_news', 'News anzeigen', 'News anzeigen', ''), ('template_display_newsletter', 'Newsletter anzeigen', 'Newsletter anzeigen', ''), ('template_display_orders', 'AuftragsÃ¼bersicht Text', 'AuftragsÃ¼bersicht Text', ''), ('template_display_products', 'TopNavi Produkte', 'TopNavi Produkte', ''), ('template_display_products_all', 'ProdNavi Alle', 'ProdNavi Alle', ''), ('template_display_products_calc', 'ProdNavi Kalkulation & Upload', 'ProdNavi Kalkulation & Upload', ''), ('template_display_products_crossselling', 'Cross Selling anzeigen', 'Cross Selling anzeigen', ''), ('template_display_products_customer_related', 'Kundenempfehlungen anzeigen', 'Kundenempfehlungen anzeigen', ''), ('template_display_products_custom_layouter', 'ProdNavi Neu individuell gestallten', 'ProdNavi Neu individuell gestallten', ''), ('template_display_products_depot', 'ProdNavi Lagerware', 'ProdNavi Lagerware', ''), ('template_display_products_my_products', 'ProdNavi persÃ¶nliche Produkte', 'ProdNavi persÃ¶nliche Produkte', ''), ('template_display_products_navi', 'Produktgruppennavigation anzeigen', 'Produktgruppennavigation anzeigen', ''), ('template_display_products_own_products', 'ProdNavi eigene Produkte', 'ProdNavi eigene Produkte', ''), ('template_display_products_products', 'ProdNavi Produkte', 'ProdNavi Produkte', ''), ('template_display_products_product_templates', 'ProdNavi Produkte & Vorlagen', 'ProdNavi Produkte & Vorlagen', ''), ('template_display_products_related_carusel', 'Kreuzverkaufkarusell anzeigen', 'Kreuzverkaufkarusell anzeigen', ''), ('template_display_products_summary', 'display', 'Anzeige', ''), ('template_display_products_templates', 'ProdNavi Vorlagen', 'ProdNavi Vorlagen', ''), ('template_display_rate', 'Voting anzeigen?', 'Voting anzeigen?', ''), ('template_display_themenshops', 'TopNavi Themenmarktplatz', 'TopNavi Themenmarktplatz', ''), ('template_display_theme_navi', 'Themennavigation anzeigen', 'Themennavigation anzeigen', ''), ('template_display_uploadcenter', 'Uploadcenter Text', 'Uploadcenter Text', ''), ('template_display_violation', 'VerstoÃŸ melden anzeigen?', 'VerstoÃŸ melden anzeigen?', ''), ('test', 'test', 'test', ''), ('Text', '', 'Text', ''), ('theme_edit_js_ArticleTheme', 'Produkttheme', 'Produkttheme', ''), ('theme_edit_js_Clipart', 'Clipart', 'Clipart', ''), ('theme_edit_js_ClipartTheme', 'Cliparttheme', 'Cliparttheme', ''), ('theme_edit_js_Credit', 'Gutscheine', 'Gutscheine', ''), ('theme_edit_js_MotivTheme', 'Motivtheme', 'Motivtheme', ''), ('theme_edit_js_Newsletter', 'Newsletter', 'Newsletter', ''), ('theme_edit_js_ShopTheme', 'Shoptheme', 'Shoptheme', ''), ('ThisShop', 'Dieser Shop', 'Dieser Shop', ''), ('Time', 'Zeit', 'Zeit', ''), ('Title', 'Titel', 'Titel', ''), ('To', '', 'To', ''), ('Typ', 'Variante', 'Variante', ''), ('Up. o. W. Layouter', '', 'Up. o. W. Layouter', ''), ('Upload', 'Hochladen', 'Hochladen', ''), ('upload error', '', 'upload error', ''), ('upload finshed', '', 'upload finshed', ''), ('upload in prove', '', 'upload in prove', ''), ('Upload oder Webbased Layouter Produkt', '', 'Upload oder Webbased Layouter Produkt', ''), ('uploadmanager_index_js_Abort', 'Abbrechen', 'Abbrechen', ''), ('uploadmanager_index_js_Add', 'HinzufÃ¼gen', 'HinzufÃ¼gen', ''), ('uploadmanager_index_js_Add_file_queue', 'Dateien der Warteschlange hinzufÃ¼gen', 'Dateien der Warteschlange hinzufÃ¼gen', ''), ('uploadmanager_index_js_Close', 'SchlieÃŸen', 'SchlieÃŸen', ''), ('uploadmanager_index_js_Close_dialog', 'Dialog schlieÃŸen', 'Dialog schlieÃŸen', ''), ('uploadmanager_index_js_Error', 'Fehler', 'Fehler', ''), ('uploadmanager_index_js_Filename', 'Dateiname', 'Dateiname', ''), ('uploadmanager_index_js_File_upload_dialog', 'Dateien hochladen', 'Dateien hochladen', ''), ('uploadmanager_index_js_Note', 'Notiz', 'Notiz', ''), ('uploadmanager_index_js_Remove', 'Entfernen', 'Entfernen', ''), ('uploadmanager_index_js_Remove_file_queue', 'Datei aus der Warteschlange entfernen', 'Datei aus der Warteschlange entfernen', ''), ('uploadmanager_index_js_Reset', 'Reset', 'Reset', ''), ('uploadmanager_index_js_Reset_queue', 'Warteschlange zurÃ¼cksetzen', 'Warteschlange zurÃ¼cksetzen', ''), ('uploadmanager_index_js_State', 'Status', 'Status', ''), ('uploadmanager_index_js_Stop_upload', 'Hochladen stoppen', 'Hochladen stoppen', ''), ('uploadmanager_index_js_Type', 'Typ', 'Typ', ''), ('uploadmanager_index_js_Upload', 'Hochladen', 'Hochladen', ''), ('uploadmanager_index_js_Upload queued files to the server', 'Hochladen der Dateien', 'Hochladen der Dateien', ''), ('uploadmanager_index_js_Uploading: {0} of {1} files complete', 'Hochgeladen {0} von {1} der Dateien', 'Hochgeladen {0} von {1} der Dateien', ''), ('uploadmanager_index_js_Waiting', 'Warten', 'Warten', ''), ('upload_all_js_Count', 'Anzahl', 'Anzahl', ''), ('upload_all_js_Created', 'Erstellt', 'Erstellt', ''), ('upload_all_js_Delete', 'L&ouml;schen', 'L&ouml;schen', ''), ('upload_all_js_download', 'Download', 'Download', ''), ('upload_all_js_file', 'Datei', 'Datei', ''), ('upload_all_js_Finish', 'Beenden', 'Beenden', ''), ('upload_all_js_id', 'ID', 'ID', ''), ('upload_all_js_items', 'Positionen', 'Positionen', ''), ('upload_all_js_Name', 'Name', 'Name', ''), ('upload_all_js_please wait .....', 'bitte warten .....', 'bitte warten .....', ''), ('upload_all_js_Position', 'Position', 'Position', ''), ('upload_all_js_select pos', 'Position selektieren', 'Position selektieren', ''), ('upload_all_js_size', 'Gr&ouml;ÃŸe (bytes)', 'Gr&ouml;ÃŸe (bytes)', ''), ('upload_all_js_Upload now', 'Jetzt hochladen', 'Jetzt hochladen', ''), ('upload_all_js_Uploadcenter', 'Uploadcenter', 'Uploadcenter', ''), ('Url1', '', 'Url1', ''), ('Url2', '', 'Url2', ''), ('Url3', '', 'Url3', ''), ('Url4', '', 'Url4', ''), ('Url5', '', 'Url5', ''), ('Username', 'Username', 'Username', ''), ('user_settings_fieldLabel_2_City', 'Stadt', 'Stadt', ''), ('user_settings_fieldLabel_2_Firstname', 'Vorname', 'Vorname', ''), ('user_settings_fieldLabel_2_Housenumber', 'Hausnummer', 'Hausnummer', ''), ('user_settings_fieldLabel_2_Information', 'Info', 'Info', ''), ('user_settings_fieldLabel_2_Lastname', 'Nachname', 'Nachname', ''), ('user_settings_fieldLabel_2_Mobil', 'Mobile', 'Handy', ''), ('user_settings_fieldLabel_2_Products', 'Products', 'Produkte', ''), ('user_settings_fieldLabel_2_Salutation', 'Anrede', 'Anrede', ''), ('user_settings_fieldLabel_2_Street', 'Strasse', 'Strasse', ''), ('user_settings_fieldLabel_2_Tel', 'Phone Number 1', 'Telefonnummer 1', ''), ('user_settings_fieldLabel_2_Typ', 'Telefontyp 2', 'Telefontyp 2', ''), ('user_settings_fieldLabel_2_Wert', 'Telefon 2', 'Telefon 2', ''), ('user_settings_fieldLabel_2_Zip', ' Zip/Postal Code', 'PLZ', ''), ('user_settings_fieldLabel_City', 'Tomn/City', 'Stadt', ''), ('user_settings_fieldLabel_Firstname', 'Vorname', 'Vorname', ''), ('user_settings_fieldLabel_Housenumber', 'Hausnummer', 'Hausnummer', ''), ('user_settings_fieldLabel_Information', 'Info', 'Info', ''), ('user_settings_fieldLabel_Lastname', 'Nachname', 'Nachname', ''), ('user_settings_fieldLabel_Mobil', 'Mobil', 'Mobil', ''), ('user_settings_fieldLabel_Password', 'Passwort', 'Passwort', ''), ('user_settings_fieldLabel_Salutation', 'Anrede', 'Anrede', ''), ('user_settings_fieldLabel_Street', 'Strasse', 'Strasse', ''), ('user_settings_fieldLabel_Tax', 'MwSt.', 'MwSt.', ''), ('user_settings_fieldLabel_Tel', 'Telefon 1', 'Telefon 1', ''), ('user_settings_fieldLabel_Typ', 'Telefontyp 2', 'Telefontyp 2', ''), ('user_settings_fieldLabel_Wert', 'Telefon 2', 'Telefon 2', ''), ('user_settings_fieldLabel_Zip', 'PLZ', 'PLZ', ''), ('user_settings_js_Save', 'Speichern', 'Speichern', ''), ('user_settings_js_Saving', 'Speichern', 'Speichern', ''), ('user_settings_Settings', 'User', 'User', ''), ('user_settings_title_Additional Information', 'Zusatz', 'Zusatz', ''), ('user_settings_title_Information', 'Kontakdaten', 'Kontakdaten', ''), ('UUID', '', 'UUID', ''), ('Validation_Failed', 'Achtung', 'Achtung', ''), ('Validation_Failed_Message', 'Bitte Ã¼berprÃ¼fen Sie ob alle Felder korrekt ausgefÃ¼llt sind.', 'Bitte Ã¼berprÃ¼fen Sie ob alle Felder korrekt ausgefÃ¼llt sind.', ''), ('Vorlagen', 'templates', 'Vorlagen', ''), ('wait', 'Wait', 'Wartend', ''), ('wait for approval all', '', 'wait for approval all', ''), ('wait for approval one', '', 'wait for approval one', ''), ('wait for payment', '', 'wait for payment', ''), ('wait for upload', '', 'wait for upload', ''), ('Warenhaus Produkt', '', 'Warenhaus Produkt', ''), ('Web', '', 'Web', ''), ('Webbased Layouter Source Product', '', 'Webbased Layouter Source Product', ''), ('When', '', 'When', ''), ('Xml', 'Xml', 'Xml', ''), ('Xml Files from Import', '', 'Xml Files from Import', ''), ('XslFo', 'XslFo', 'XslFo', ''), ('Zip', ' Zip/Postal Code', 'PLZ', ''), ('Zipfile', '', 'Zipfile', '');
COMMIT;

-- ----------------------------
--  Table structure for `admin_user_settings`
-- ----------------------------
DROP TABLE IF EXISTS `admin_user_settings`;
CREATE TABLE `admin_user_settings` (
  `id` int(8) NOT NULL AUTO_INCREMENT,
  `user_id` int(8) NOT NULL,
  `name` varchar(255) NOT NULL,
  `data` mediumtext NOT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id_name` (`user_id`,`name`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
--  Table structure for `api`
-- ----------------------------
DROP TABLE IF EXISTS `api`;
CREATE TABLE `api` (
  `id` int(8) NOT NULL AUTO_INCREMENT,
  `uuid` varchar(40) NOT NULL,
  `created` datetime NOT NULL,
  `updated` datetime NOT NULL,
  `contact_id` int(8) NOT NULL,
  `shop_id` int(8) NOT NULL,
  `install_id` int(8) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
--  Table structure for `approval`
-- ----------------------------
DROP TABLE IF EXISTS `approval`;
CREATE TABLE `approval` (
  `id` int(8) NOT NULL AUTO_INCREMENT,
  `uuid` varchar(50) NOT NULL,
  `shop_id` int(8) NOT NULL,
  `install_id` int(8) NOT NULL,
  `created` datetime NOT NULL,
  `updated` datetime NOT NULL,
  `orderpos_id` int(8) NOT NULL,
  `contact_id` int(8) NOT NULL,
  `text` text NOT NULL,
  `type` int(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
--  Table structure for `article`
-- ----------------------------
DROP TABLE IF EXISTS `article`;
CREATE TABLE `article` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `created` datetime DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `install_id` bigint(20) DEFAULT NULL,
  `shop_id` bigint(20) DEFAULT NULL,
  `account_id` bigint(20) DEFAULT NULL,
  `contact_id` bigint(20) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `url` varchar(255) NOT NULL,
  `typ` varchar(255) DEFAULT NULL,
  `data` text,
  `enable` tinyint(1) DEFAULT NULL,
  `private` tinyint(1) DEFAULT NULL,
  `mwert` bigint(20) DEFAULT NULL,
  `versand` varchar(29) DEFAULT NULL,
  `versandwert` double DEFAULT NULL,
  `info` text,
  `einleitung` varchar(500) DEFAULT NULL,
  `pos` float DEFAULT NULL,
  `upload` tinyint(1) DEFAULT NULL,
  `special` tinyint(1) DEFAULT NULL,
  `file` varchar(255) DEFAULT NULL,
  `file1` varchar(255) DEFAULT NULL,
  `a6_xmlconfigfile` varchar(255) DEFAULT NULL,
  `a6_xmlpagesfile` varchar(255) NOT NULL,
  `a6_xmlpagetemplatesfile` varchar(255) NOT NULL,
  `a6_xmlpageobjectsfile` varchar(255) NOT NULL,
  `a6_xmlxslfofile` varchar(255) DEFAULT NULL,
  `a6_directory` varchar(255) DEFAULT NULL,
  `a6_isroot` int(1) DEFAULT NULL,
  `price` double DEFAULT NULL,
  `a1_xml` longtext,
  `a1_special` int(1) DEFAULT NULL,
  `a2_count` int(11) DEFAULT NULL,
  `a3_xml` text,
  `a3_special` int(1) DEFAULT NULL,
  `a3_pdf` varchar(255) DEFAULT NULL,
  `a3_maske` varchar(255) DEFAULT NULL,
  `a4_auflagen` text,
  `a4_sponsoring` int(1) DEFAULT NULL,
  `a4_prodzeit` varchar(255) DEFAULT NULL,
  `a4_abpreis` float(20,2) DEFAULT NULL,
  `a5_buy` int(1) DEFAULT NULL,
  `a5_xml` text,
  `layouterid` varchar(255) NOT NULL,
  `uuid` varchar(255) NOT NULL,
  `used` int(8) DEFAULT NULL,
  `kostenstelle` varchar(255) DEFAULT NULL,
  `reorder` int(1) DEFAULT NULL,
  `reordertheme` int(1) DEFAULT NULL,
  `question` int(1) DEFAULT NULL,
  `questiontheme` int(1) DEFAULT NULL,
  `a7_xml` text,
  `abprice` varchar(100) DEFAULT NULL,
  `a6_xmlpreviewxslfofile` varchar(255) DEFAULT NULL,
  `resale` int(1) NOT NULL,
  `a6_resale_price` float(20,2) NOT NULL,
  `a6_org_article` int(8) NOT NULL,
  `render_new_preview_image` int(1) NOT NULL,
  `render_new_preview_pdf` int(1) NOT NULL,
  `render_new_preview_gallery` int(1) NOT NULL,
  `a6_display_market` int(1) NOT NULL DEFAULT '0',
  `visits` int(8) NOT NULL,
  `display_market` int(1) NOT NULL DEFAULT '0',
  `stock` int(1) NOT NULL,
  `stock_count` int(8) NOT NULL,
  `stock_count_min` int(8) NOT NULL,
  `a6_xmlextendpreviewxslfofile` varchar(255) NOT NULL,
  `rate` int(8) NOT NULL,
  `rate_count` int(8) NOT NULL,
  `confirm` int(1) NOT NULL,
  `confirmone` int(1) NOT NULL,
  `confirmaccount_id` int(8) NOT NULL,
  `template_admin` int(1) NOT NULL,
  `template_system_operator` int(1) NOT NULL,
  `init_status` int(8) NOT NULL,
  `lager_file_file` varchar(255) NOT NULL,
  `lager_file_preview` varchar(255) NOT NULL,
  `preis` float NOT NULL,
  `upload_article` int(1) NOT NULL,
  `upload_article_status` int(4) NOT NULL,
  `upload_center` int(1) NOT NULL,
  `upload_center_status` int(4) NOT NULL,
  `upload_post` int(1) NOT NULL,
  `upload_post_status` int(4) NOT NULL,
  `upload_email` int(1) NOT NULL,
  `upload_email_status` int(4) NOT NULL,
  `vorlage_file` varchar(50) NOT NULL,
  `vorlage_info` text NOT NULL,
  `upload_weblayouter` int(1) NOT NULL,
  `upload_weblayouter_status` int(4) NOT NULL,
  `not_edit` int(1) NOT NULL,
  `custom_1_title` varchar(255) NOT NULL,
  `custom_1_text` text NOT NULL,
  `resale_design` int(1) NOT NULL,
  `motive_calc` mediumtext NOT NULL,
  `motive_price` float NOT NULL,
  `a9_back1` varchar(255) NOT NULL,
  `a9_front1` varchar(255) NOT NULL,
  `a9_left1` varchar(255) NOT NULL,
  `a9_right1` varchar(255) NOT NULL,
  `a9_back2` varchar(255) NOT NULL,
  `a9_front2` varchar(255) NOT NULL,
  `a9_left2` varchar(255) NOT NULL,
  `a9_right2` varchar(255) NOT NULL,
  `a9_back3` varchar(255) NOT NULL,
  `a9_front3` varchar(255) NOT NULL,
  `a9_left3` varchar(255) NOT NULL,
  `a9_right3` varchar(255) NOT NULL,
  `a9_back4` varchar(255) NOT NULL,
  `a9_front4` varchar(255) NOT NULL,
  `a9_left4` varchar(255) NOT NULL,
  `a9_right4` varchar(255) NOT NULL,
  `a9_back5` varchar(255) NOT NULL,
  `a9_front5` varchar(255) NOT NULL,
  `a9_left5` varchar(255) NOT NULL,
  `a9_right5` varchar(255) NOT NULL,
  `a9_sizes` mediumtext NOT NULL,
  `a9_colors` mediumtext NOT NULL,
  `a9_pt` mediumtext NOT NULL,
  `file2` varchar(255) NOT NULL,
  `file3` varchar(255) NOT NULL,
  `file4` varchar(255) NOT NULL,
  `file5` varchar(255) NOT NULL,
  `file6` varchar(255) NOT NULL,
  `file7` varchar(255) NOT NULL,
  `a9_back_area` varchar(255) NOT NULL,
  `a9_front_area` varchar(255) NOT NULL,
  `a9_left_area` varchar(255) NOT NULL,
  `a9_right_area` varchar(255) NOT NULL,
  `can_buy_with_no_price` int(1) NOT NULL,
  `motiv_group` int(1) NOT NULL,
  `simple_preflight` int(1) NOT NULL,
  `upload_templateprint` int(1) NOT NULL,
  `upload_templateprint_status` int(8) NOT NULL,
  `meta_keywords` text NOT NULL,
  `meta_description` text NOT NULL,
  `meta_author` varchar(255) NOT NULL,
  `meta_custom_title` varchar(255) NOT NULL,
  `a9_price_motiv` float NOT NULL,
  `a9_price_icon` float NOT NULL,
  `a9_price_text` float NOT NULL,
  `a9_price_text_line1` float NOT NULL,
  `a9_price_print_disable` float NOT NULL,
  `not_buy` int(1) NOT NULL,
  `a4_abpreis_calc` int(1) NOT NULL DEFAULT '0',
  `a4_abpreis_must_recalc` int(1) NOT NULL DEFAULT '1',
  `meta_og_title` varchar(255) NOT NULL,
  `meta_og_type` varchar(255) NOT NULL,
  `meta_og_url` varchar(255) NOT NULL,
  `meta_og_image` varchar(255) NOT NULL,
  `meta_og_description` text NOT NULL,
  `text_art` varchar(255) NOT NULL,
  `text_format` varchar(255) NOT NULL,
  `display_no_price` int(1) NOT NULL,
  `vorlage_file1` varchar(255) DEFAULT NULL,
  `vorlage_file2` varchar(255) DEFAULT NULL,
  `vorlage_file3` varchar(255) DEFAULT NULL,
  `upload_steplayouter` int(1) DEFAULT '0',
  `upload_steplayouter_status` int(8) DEFAULT NULL,
  `upload_steplayouter_xml` longtext,
  `upload_steplayouter_data` longtext,
  `lang_data` longtext,
  `article_nr_intern` varchar(255) DEFAULT NULL,
  `article_nr_extern` varchar(255) DEFAULT NULL,
  `to_export` int(1) NOT NULL DEFAULT '0',
  `changes` longtext NOT NULL,
  `params` longtext,
  `upload_collecting_orders` int(1) NOT NULL DEFAULT '0',
  `upload_collecting_orders_status` int(8) NOT NULL DEFAULT '10',
  `upload_multi` int(1) NOT NULL DEFAULT '0',
  `upload_multi_status` int(8) NOT NULL DEFAULT '10',
  `verpackungseinheit` varchar(255) DEFAULT NULL,
  `papierfaktor` varchar(255) DEFAULT NULL,
  `display_not_in_overview` int(1) DEFAULT '0',
  `stock_place` varchar(100) DEFAULT NULL,
  `stock_max_buy` int(4) DEFAULT NULL,
  `set_config` mediumtext,
  `as_offer` int(1) DEFAULT NULL,
  `upload_steplayouter2` int(1) DEFAULT NULL,
  `upload_steplayouter2_status` int(8) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `install_id_idx` (`install_id`),
  KEY `shop_id_idx` (`shop_id`),
  KEY `account_id_idx` (`account_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 AVG_ROW_LENGTH=2891 ROW_FORMAT=COMPACT;

-- ----------------------------
--  Records of `article`
-- ----------------------------
BEGIN;
INSERT INTO `article` VALUES ('1', '2012-05-24 16:49:58', '2012-08-27 13:33:20', '1', '1', null, '9225', 'Trauerkarte ABC', 'trauerkarte-abc', '6', null, '1', '0', '19', '', '0', '<div>\n            <h3 class=\"product_title\"><ul style=\"margin: 0px 0px 25px; padding: 0px; border: 0px; outline: 0px; font-size: 12px; background-color: rgb(255, 255, 255); list-style: none; font-family: Arial, Helvetica, sans-serif; font-weight: normal; line-height: 12px; \"><li class=\"artikel_bezeichnung\" style=\"margin: 0px; padding: 0px; border: 0px; outline: 0px; font-size: 16px; background-color: transparent; list-style-type: none; line-height: 19px; color: rgb(152, 16, 8); font-weight: bold; background-position: initial initial; background-repeat: initial initial; \">Trauerkarten</li><li class=\"vorlage_bezeichnung\" style=\"margin: 0px; padding: 0px; border: 0px; outline: 0px; font-size: 16px; background-color: transparent; list-style-type: none; line-height: 19px; font-weight: bold; \">Fotoklappkarte</li></ul><ul style=\"margin: 0px 0px 25px; padding: 0px; border: 0px; outline: 0px; font-size: 12px; background-color: rgb(255, 255, 255); list-style: none; font-family: Arial, Helvetica, sans-serif; font-weight: normal; line-height: 12px; \"><li class=\"produkteigenschaften\" style=\"margin: 0px; padding: 0px; border: 0px; outline: 0px; background-color: transparent; list-style-type: none; line-height: 19px; font-weight: bold; color: rgb(112, 120, 126); \"><p style=\"margin: 0px; padding: 0px; border: 0px; outline: 0px; background-color: transparent; line-height: 16px; \">Foto - Trauerklappkarte mit Schleifenband</p></li></ul><ul class=\"artikel_preis\" style=\"margin: 0px 0px 25px; padding: 0px; border: 0px; outline: 0px; font-size: 12px; background-color: rgb(255, 255, 255); list-style: none; font-family: Arial, Helvetica, sans-serif; font-weight: normal; line-height: 12px; \"><li class=\"preisbeispiel\" style=\"margin: 0px; padding: 0px; border: 0px; outline: 0px; background-color: transparent; list-style-type: none; line-height: 19px; \"><p style=\"margin: 0px; padding: 0px; border: 0px; outline: 0px; background-color: transparent; line-height: 16px; color: rgb(112, 120, 126); \">Die Karten drucken wir individuell fÃ¼r Sie auf hochwertigem 250-300 g/mÂ² Kartenmaterial. Die passenden BriefhÃ¼llen werden kostenlos mitgeliefert.</p></li></ul></h3></div><div class=\"pers_text product_desc\">\n         </div>', '', '200', null, null, '430fba6fba15ffdb39cd07d12dc84e14fce13483', '16075d04b0994c5334a8564de9514142509b8916', 'config.xml', 'pages.xml', 'pagetemplates.xml', 'pageobjects.xml', 'xslfo.fop', '5/4/3/8/', '1', '0', '<?xml version=\"1.0\" encoding=\"utf-8\"?>\n<kalkulation>\n  <artikel>\n   <name>Trauerkarte ABC</name>\n    <kommentar>Trauerkarte ABC</kommentar>\n\n<uploads>\n       <upload id=\"karte\" name=\"Karte\" description=\"Bitte laden sie eine PDF, PNG oder JPG Datei fÃ¼r die Karte hoch\">\n       </upload>\n</uploads>\n\n   <option id=\"auflage\" name=\"Auflage\" type=\"Select\" default=\"10\" require=\"true\" helplink=\"/cms/trauerkarten-info-material\" help=\"Geben Sie hier bitte Ihre Auflage ein\">\n                                <opt id=\"10\" name=\"10 St.\"/>\n                                <opt id=\"25\" name=\"25 St.\"/>\n                                <opt id=\"50\" name=\"50 St.\"/>\n                                <opt id=\"100\" name=\"100 St.\"/>\n                                <opt id=\"200\" name=\"200 St.\"/>\n                                <opt id=\"500\" name=\"500 St.\"/>\n                                <opt id=\"1000\" name=\"1000 St.\"/>\n                                <opt id=\"2000\" name=\"2000 St.\"/>\n    </option>\n\n\n   <option id=\"material\" name=\"Material\" type=\"Select\" default=\"1\" require=\"true\" help=\"Papierwunsch - Standard ist: weiÃŸ, matt (Kartenmaterial)\" helplink=\"/cms/trauerkarten-info-material\">\n     <opt id=\"1\" name=\"weiÃŸ, matt (Kartenmaterial)\">\n        <auflage>\n         <grenze formel=\"25+($Vauflage$V*0.20)\">10-24</grenze>\n         <grenze formel=\"25+($Vauflage$V*0.10)\">25-99</grenze>\n         <grenze formel=\"25+($Vauflage$V*0.07)\">100-</grenze>\n        </auflage>\n      </opt>\n      <opt id=\"2\" name=\"Creme (Kartenmaterial)\">\n        <auflage>\n         <grenze formel=\"28+($Vauflage$V*0.20)\">10-24</grenze>\n         <grenze formel=\"28+($Vauflage$V*0.10)\">25-99</grenze>\n         <grenze formel=\"28+($Vauflage$V*0.07)\">100-</grenze>\n        </auflage>\n      </opt>\n      <opt id=\"3\" name=\"Leinen (Kartenmaterial)\">\n       <auflage>\n         <grenze formel=\"30+($Vauflage$V*0.20)\">10-24</grenze>\n         <grenze formel=\"30+($Vauflage$V*0.10)\">25-99</grenze>\n         <grenze formel=\"30+($Vauflage$V*0.07)\">100-</grenze>\n        </auflage>\n      </opt>\n    </option>\n\n\n   <option id=\"umschlaege\" name=\"UmschlÃ¤ge\" type=\"Select\" default=\"1\" require=\"true\" help=\"UmschlÃ¤ge - Standard ist: UmschlÃ¤ge unbedruckt\">\n     <opt id=\"1\" name=\"UmschlÃ¤ge unbedruckt\">\n       <auflage>\n         <grenze formel=\"($Vauflage$V*0.10)\">10-</grenze>\n        </auflage>\n      </opt>\n      <opt id=\"2\" name=\"UmschlÃ¤ge mit einfachem Rand\">\n       <auflage>\n         <grenze formel=\"($Vauflage$V*0.20)\">10-</grenze>\n        </auflage>\n      </opt>\n      <opt id=\"3\" name=\"UmschlÃ¤ge mit Rand und Kreuz\">\n       <auflage>\n         <grenze formel=\"($Vauflage$V*0.30)\">10-</grenze>\n        </auflage>\n      </opt>\n    </option>\n\n\n\n     <option id=\"produktion\" name=\"Produktionszeit\" type=\"Select\" default=\"std\" help=\"Entscheiden Sie, wieviel Tage wir fÃ¼r Ihren Auftrag einplanen dÃ¼rfen\">\n       <opt id=\"std\" name=\"Standard: 1 Arbeitstag\">\n          <auflage>\n           <grenze formel=\"($Vauflage$V*0.00)\">10-</grenze>\n          </auflage>\n        </opt>\n        <opt id=\"std2\" name=\"innerhalb eines Arbeitstages\">\n         <auflage>\n           <grenze formel=\"($Vauflage$V*0.00)\">10-</grenze>\n          </auflage>\n        </opt>\n\n      </option>\n \n  </artikel>\n\n</kalkulation>', '0', null, null, null, null, null, null, null, null, '33.32', null, null, '', '0001-54bb32cc-4fbe4a96-65b7-aa441f0f', '1', '', null, null, null, null, null, null, 'previewxslfo.fop', '0', '0.00', '0', '0', '0', '0', '0', '378', '0', '0', '0', '0', 'extendpreviewxslfo.fop', '0', '0', '0', '0', '0', '0', '0', '10', '', '', '0', '1', '140', '0', '0', '1', '30', '0', '30', '', 'Das sind meine Datenhinweise.<br><br>', '0', '0', '0', '', '', '0', '', '0', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '662cd72c5279b5cc145a22c5c679fa7d2026701e', '', '', '', '', '', '', '', '', '', '0', '0', '0', '0', '0', '', '', '', '', '0', '0', '0', '0', '0', '0', '1', '0', '', '', '', '', '', '', '', '0', '', '', '', '1', '140', '<?xml version=\"1.0\" encoding=\"utf-8\"?>\n<settings>\n    <version>1.1</version>\n    <includes>\n        <include type=\"css\">trauerkarten.css</include>\n        <include type=\"javascript\">trauerkarten.js</include>\n    </includes>\n    <general>\n        <size width=\"900\" height=\"700\"/>\n        <headline>Gestalten Sie Ihre Karte nach Wunsch</headline>\n        <buttons>\n            <finish label=\"Fertigstellen\"/>\n            <preview label=\"Vorschau\"/>\n            <cancel label=\"Abbrechen\"/>\n            <next label=\"Weiter\"/>\n            <back label=\"ZurÃ¼ck\"/>\n        </buttons>\n    </general>\n    <rendering>\n        <sites>\n            <site id=\"1\" trim-left=\"8.5\" trim-top=\"8.5\" trim-bottom=\"8.5\" trim-right=\"8.5\" fold-mark-x=\"320\" width=\"640.63\" height=\"498.9\" render-preview-trim=\"1\" render-preview-fold-mark=\"1\">\n                <elements>\n                    <motiv render-mode=\"motiv\" render-pos-x=\"0\" render-pos-y=\"0\" render-pos-layer=\"20\" render-pos-width=\"640.63\" value=\"0001-d95c8e64-5032958a-f18d-ba3c620b\" render-pos-height=\"498.9\" />\n                    <motiv render-mode=\"motiv\" render-pos-x=\"450\" render-pos-y=\"-47\" render-pos-layer=\"30\" render-pos-width=\"250\" value=\"0001-d95c8e64-50329f02-f99a-f61ebbae\" render-rotate=\"315\" render-pos-height=\"60\" />\n                </elements>\n            </site>\n            <site id=\"2\" trim-left=\"8.5\" trim-top=\"8.5\" trim-bottom=\"8.5\" trim-right=\"8.5\" fold-mark-x=\"320\" width=\"640.63\" height=\"498.9\" render-preview-trim=\"1\" render-preview-fold-mark=\"1\">\n                <elements>\n                </elements>\n            </site>\n        </sites>\n    </rendering>\n    <steps>\n        <step id=\"1\" label=\"AuÃŸenseite\" layout=\"column\">\n            <headline>AuÃŸenseite</headline>\n            <description>bitte eingeben</description>\n            <elements>\n                <formdesigntemplates id=\"formdesigntemplates\" label=\"Designvorlagen:\">\n                </formdesigntemplates>\n                \n\n                <formtext color-select=\"1\" render-bold=\"1\" render-text-style=\"bold\" render-pos-x=\"490\" render-size=\"15\" render-pos-y=\"20\" render-site=\"1\" render-pos-width=\"170\" render-mode=\"text\" render-align=\"center\" render-font-color=\"#ffffff\" render-pos-layer=\"50\" render-rotate=\"315\" data-binding=\"text-vorne\" id=\"text-vorne\" label=\"Text vorne:\" value=\"WIR NEHMEN ABSCHIED\" width=\"100\" column=\"1\"/>\n\n                <formupload render-pos-x=\"340\" render-pos-y=\"50\" render-site=\"1\" render-mode=\"formupload\" render-layer=\"10\" data-binding=\"foto-vorne\" id=\"foto-vorne\" label=\"Foto vorne:\" render-pos-width=\"290\" render-pos-height=\"400\" column=\"1\" />\n\n                <preview id=\"preview\" label=\"Vorschau\" render-site=\"1\" width=\"400\" height=\"370\" pos=\"right\" column=\"2\" />\n            </elements>\n        </step>\n        <step id=\"2\" label=\"Innenseite\" layout=\"column\">\n            <headline>Innenseite</headline>\n            <description>bitte eingeben</description>\n            <elements>\n                <formimageselect render-mode=\"formimageselect\" render-site=\"2\" render-pos-width=\"200\" render-pos-height=\"300\" value=\"kreuz\" render-pos-x=\"70\" render-pos-y=\"70\" id=\"ziergrafik\" label=\"Grafik\" column=\"1\" render-layer=\"15\" base-dir=\"/styles/vorlagen/bootstrap/steplayouter/img/examples/\">\n                    <options>\n                        <option id=\"kreuz\" thumbnail=\"item_kreuz_.png\" file=\"item_kreuz_.png\" style=\"width: 50px\">Kreuz</option>\n                        <option id=\"taube\" thumbnail=\"item_taube_.png\" file=\"item_taube_.png\" style=\"width: 50px\">Taube</option>\n                    </options>\n                </formimageselect>\n\n                <formtextarea render-pos-x=\"20\" render-size=\"12\" render-pos-width=\"290\" render-pos-height=\"100\" render-pos-y=\"440\" render-site=\"2\" render-mode=\"textarea\" render-layer=\"15\" color-select=\"0\" data-binding=\"text-links\" id=\"text-links\" label=\"Text links\" value=\"Die Beerdigung findet am Mittwoch,dem 12. Januar 2000 um 15.30 Uhr auf dem Friedhof in Musterdorf statt.\nAnschlieÃŸend laden wir zum Kaffee in die GaststÃ¤tte\nMeier, in Musterdorf.\" width=\"100\" column=\"1\" />\n\n                <formtextarea render-pos-x=\"450\" render-size=\"12\" render-pos-width=\"180\" render-pos-height=\"100\" render-pos-y=\"30\" render-site=\"2\" render-mode=\"textarea\" render-layer=\"15\" color-select=\"1\" data-binding=\"spruch\" id=\"spruch\" label=\"Spruch\" value=\"Wer so gelebt wie Du im Leben,\nwer so erfÃ¼llte seine Pflicht,\nder hat das HÃ¶chste hingegeben,\nder stirbt auch selbst im Tode nicht.\" width=\"100\" column=\"1\" />\n                \n                <formtextarea render-pos-x=\"350\" render-size=\"12\" render-pos-width=\"270\" render-pos-height=\"100\" render-pos-y=\"160\" render-site=\"2\" render-mode=\"textarea\" render-layer=\"12\" color-select=\"1\" data-binding=\"spruch2\" id=\"spruch22\" label=\"Spruch 2\" value=\"In Liebe und Dankbarkeit nehmen wir Abschied von meiner lieben Frau, unserer guten Mutter, Schwiegermutter, Oma und Tante.\" width=\"100\" column=\"1\" />\n    \n                <formtext color-select=\"1\" render-bold=\"1\" render-pos-x=\"350\" render-align=\"center\" render-pos-width=\"250\" render-size=\"16\" render-pos-y=\"250\" render-site=\"2\" render-text-style=\"bold\" render-mode=\"text\" render-layer=\"10\" data-binding=\"text-name\" id=\"text-name\" label=\"Name\" value=\"Maria Muster\" width=\"200\" column=\"1\"/>\n\n                <formtext color-select=\"1\" render-bold=\"1\" render-pos-x=\"350\" render-align=\"center\" render-pos-width=\"250\" render-size=\"12\" render-pos-y=\"290\" render-site=\"2\" render-mode=\"text\" render-layer=\"10\" data-binding=\"text-geburtsname\" id=\"text-geburtsname\" label=\"Geburtsname\" value=\"geb. Meier\" width=\"100\" column=\"1\"/>\n\n                <formtext color-select=\"1\" render-bold=\"1\" render-pos-x=\"350\" render-align=\"center\" render-pos-width=\"250\" render-size=\"12\" render-pos-y=\"320\" render-site=\"2\" render-mode=\"text\" render-layer=\"10\" data-binding=\"text-daten\" id=\"text-daten\" label=\"Daten\" value=\"* 01.01.1900             â€  01.01.2000\" width=\"100\" column=\"1\"/>\n\n                <formtextarea render-pos-x=\"380\" render-size=\"12\" render-pos-width=\"200\" render-pos-height=\"100\" render-pos-y=\"410\" render-site=\"2\" render-mode=\"textarea\" render-layer=\"15\" color-select=\"1\" data-binding=\"text-angehoerige\" id=\"text-angehoerige\" label=\"AngehÃ¶rige\" value=\"In stiller Trauer\n\nKarl und Luise\nHelga und Theo\nRainer und Verena mit Michelle\nund alle Verwandten\" width=\"100\" column=\"1\" />\n                \n                <formtext color-select=\"1\" render-bold=\"1\" render-align=\"center\" render-pos-width=\"250\" render-pos-x=\"360\" render-size=\"12\" render-pos-y=\"470\" render-site=\"2\" render-mode=\"text\" render-layer=\"10\" data-binding=\"text-ort\" id=\"text-ort\" label=\"Ort u. Datum\" value=\"Musterhausen, im Januar 2011\" width=\"100\" column=\"1\"/>\n\n                <preview id=\"preview\" label=\"Vorschau\" width=\"400\" height=\"370\" render-site=\"2\" column=\"2\" />\n            </elements>\n        </step>\n        <step id=\"3\" label=\"Vorschau\">\n            <headline>Vorschau</headline>\n            <description>Schauen Sie es sich gut an</description>\n            <elements>\n                <preview id=\"preview\" label=\"Vorschau\" width=\"400\" height=\"370\" render-site=\"all\" />\n            </elements>\n        </step>\n    </steps>\n</settings>', null, '{\"en\":{\"uuid\":\"0001-d95c8e64-502e2816-3b77-5d0298ed\",\"title\":\"en title\",\"info\":\"en info\",\"einleitung\":\"en einleitung\"}}', null, null, '0', '', null, '0', '10', '0', '10', null, null, '0', null, null, null, null, null, null), ('2', '2012-07-06 08:36:20', '2012-08-20 17:48:25', '1', '1', null, '9225', 'Karte DIN A6', 'karte-din-a6-6', '6', null, '1', '0', '19', 'keine', '0', 'Hoch- oder Querformat (10,5 x 14,8 cm)<br>Viele Papiervarianten zur Auswahl, u. a.:<br>\n<ul><li>300 g/mÂ² Bilderdruckpapier: beidseitig matte Dispersionslackierung</li><li>400 g/mÂ² Bilderdruckpapier: beidseitig matte Dispersionslackierung</li><li>280 g/mÂ² Chromokarton: Vorderseite glÃ¤nzend dispersionslackiert, \nRÃ¼ckseite beschreibbar</li><li>250 g/mÂ² Offsetpapier: beidseitig unbehandelt, beschreibbar</li><li>250 g/mÂ² CHROMOLUX Pearl (Pearleffekt): Perlmutt-WeiÃŸe \nVorderseite. Mit UV-Farben bedruckt</li><li>250 g/mÂ² CHROMOLUX magic chrome (Spiegeleffekt): Die silbern \nschimmernde Vorderseite sorgt fÃ¼r ein tolles Erscheinungsbild. Mit \nUV-Farben bedruckt</li><li>250 g/mÂ² CHROMOLUX 700 weiÃŸ: 1-seitig extrem hochglÃ¤nzend, \ngussgestrichen, die RÃ¼ckseite ist weiÃŸ und beschreibbar. Mit UV-Farben \nbedruckt</li><li>250g/mÂ² CHROMOLUX Magic chrome, Pearl oder 700 weiÃŸ mit UV-Farben \nbedruckt und mit flÃ¤chigem UV-Lack veredelt</li></ul>\n<br>', 'Hoch- oder Querformat (10,5 x 14,8 cm)', '100', '0', '1', 'de25ae47b139f2513a559ce64a8878931f4ae76b', '', 'config.xml', 'pages.xml', 'pagetemplates.xml', 'pageobjects.xml', 'xslfo.fop', '5/5/2/9/', null, '0', '<?xml version=\"1.0\" encoding=\"utf-8\"?>\n<kalkulation>\n  <artikel>\n   <name>Digitaldruck</name>\n   <kommentar>Flyer A6 Digitaldruck</kommentar>\n\n\n      <option id=\"auflage\" name=\"Auflage\" type=\"Input\" width=\"3\" default=\"500\" require=\"true\" help=\"Geben Sie hier bitte Ihre Auflage ein\">\n       <auflage>\n         <grenze formel=\"((($Vauflage$V*1/8)*$Vpapier_value$V+12))\">1-</grenze>\n        </auflage>\n      </option>\n\n\n     <option id=\"druck\" name=\"Druck\" type=\"Select\" default=\"40f\" help=\"Wie soll Ihr Produkt bedruckt werden?\">\n       <opt id=\"44f\" name=\"4/4-farbig (beidseitig farbig bedruckt)\">\n         <auflage>\n           <grenze formel=\"(($Vauflage$V*1/8)*0.17)\">1-</grenze>\n         </auflage>\n        </opt>\n        <opt id=\"40f\" name=\"4/0-farbig (einseitig farbig, RÃ¼ckseite unbedruckt)\">\n          <auflage>\n           <grenze formel=\"(($Vauflage$V*1/8)*0.10)\">1-</grenze>\n         </auflage>\n        </opt>\n        <opt id=\"41fs\" name=\"4/1-farbig (eine Seite farbig+eine Seite schwarz bedruckt)\">\n         <auflage>\n           <grenze formel=\"(($Vauflage$V*1/8)*0.13)\">1-</grenze>\n         </auflage>\n        </opt>\n        <opt id=\"11s\" name=\"1/1-fabig (beidseitig schwarz bedruckt)\">\n         <auflage>\n           <grenze formel=\"(($Vauflage$V*1/8)*0.06)\" overwrite=\"papier|container|PK-FL2\">1-</grenze>\n         </auflage>\n        </opt>\n        <opt id=\"10s\" name=\"1/0-farbig (einseitig schwarz, RÃ¼ckseite unbedruckt)\">\n         <auflage>\n           <grenze formel=\"(($Vauflage$V*1/8)*0.03)\" overwrite=\"papier|container|PK-FL2\">1-</grenze>\n         </auflage>\n        </opt>\n      </option>\n\n\n     <option id=\"papier\" name=\"Papier\" type=\"Select\" mode=\"papierdb\" container=\"PK-FL1_774\" default=\"bg135_774\" help=\"Aus welchem Papier soll Ihr Produkt bestehen?\">\n      </option>\n\n\n     <option id=\"veredelung\" name=\"Veredelung\" help=\"Soll Ihr Produkt mit Folie kaschiert werden? Dies ergibt entweder eine hochglÃ¤nzende oder seidenmatt, edle OberflÃ¤che\" type=\"Select\" default=\"keine\">\n       <opt id=\"keine\" name=\"Keine Veredelung\">\n          <auflage>\n           <grenze pauschale=\"0\">1-</grenze>\n         </auflage>\n        </opt>\n        <opt id=\"1sfm\" name=\"1-seitig Folienkaschiert matt\">\n          <auflage>\n           <grenze formel=\"(($Vauflage$V*1/8)*0.32)+15\">1-</grenze>\n          </auflage>\n        </opt>\n        <opt id=\"2sfm\" name=\"2-seitig Folienkaschiert matt\">\n          <auflage>\n           <grenze formel=\"(($Vauflage$V*1/8)*0.64)+15\">1-</grenze>\n          </auflage>\n        </opt>\n        <opt id=\"1sfg\" name=\"1-seitig Folienkaschiert glÃ¤nzend\">\n         <auflage>\n           <grenze formel=\"(($Vauflage$V*1/8)*0.26)+15\">1-</grenze>\n          </auflage>\n        </opt>\n        <opt id=\"2sfg\" name=\"2-seitig Folienkaschiert glÃ¤nzend\">\n         <auflage>\n           <grenze formel=\"(($Vauflage$V*1/8)*0.52)+15\">1-</grenze>\n          </auflage>\n        </opt>\n      </option>\n\n\n     <option id=\"produktion\" name=\"Produktionszeit\" type=\"Select\" default=\"std\" help=\"Entscheiden Sie, wieviel Tage wir fÃ¼r Ihren Auftrag einplanen dÃ¼rfen - Dateneingang bis 10 Uhr\">\n       <opt id=\"std\" name=\"Standard: 3 Arbeitstage\">\n         <auflage>\n           <grenze pauschale=\"0\">1-</grenze>\n         </auflage>\n        </opt>\n        <opt id=\"exp\" name=\"Express: 1 Arbeitstag\">\n         <auflage>\n           <grenze formel=\"(9)\">1-</grenze>\n          </auflage>\n        </opt>\n        <opt id=\"expp\" name=\"Express-Plus: am gleichen Tag verschickt, Daten bis 10 Uhr\">\n         <auflage>\n           <grenze formel=\"(14)\">1-</grenze>\n         </auflage>\n        </opt>\n      </option>\n\n\n     \n      <option id=\"datenformat\" name=\"Datenformat\" type=\"Select\" default=\"pdf\" help=\"In welchem Dateiformat liefern Sie uns Ihre druckfertigen Daten?\">\n        <opt id=\"pdf\" name=\"PDF\">\n         <auflage>\n           <grenze pauschale=\"0\">1-</grenze>\n         </auflage>\n        </opt>\n        <opt id=\"jpg\" name=\"JPG\">\n         <auflage>\n           <grenze formel=\"(3)\">1-</grenze>\n          </auflage>\n        </opt>\n        <opt id=\"andere\" name=\"Andere\">\n         <auflage>\n           <grenze formel=\"(5)\">1-</grenze>\n          </auflage>\n        </opt>\n      </option>\n\n     \n    <option id=\"datenuebertragung\" name=\"DatenÃ¼bertragung\" type=\"Select\" default=\"upload\" help=\"Wie wollen Sie uns Ihre Daten Ã¼bertragen?\">\n       <opt id=\"upload\" name=\"Per Upload am Bestellende\">\n          <auflage>\n           <grenze pauschale=\"0\">1-</grenze>\n         </auflage>\n        </opt>\n        <opt id=\"mail\" name=\"Per E-Mail\">\n         <auflage>\n           <grenze formel=\"(0)\">1-</grenze>\n          </auflage>\n        </opt>\n        <opt id=\"post\" name=\"Per Post\">\n         <auflage>\n           <grenze formel=\"(4)\">1-</grenze>\n          </auflage>\n        </opt>\n      </option>\n\n\n     <option id=\"musterdruck\" name=\"Musterdruck\" type=\"Select\" default=\"keinmuster\" help=\"Sie haben hier die MÃ¶glichkeit zur Endkontrolle von Ihrem Produkt ein Muster vorab per Post zu bekommen\">\n       <opt id=\"keinmuster\" name=\"Ohne Vorabmuster\">\n         <auflage>\n           <grenze pauschale=\"0\">1-</grenze>\n         </auflage>\n        </opt>\n        <opt id=\"muster\" name=\"Musterdruck vorab per Post schicken\">\n          <auflage>\n           <grenze formel=\"(9)\">1-</grenze>\n          </auflage>\n        </opt>\n      </option>\n\n\n\n\n </artikel>\n\n\n\n\n\n  <artikel>\n   <name>Offsetdruck</name>\n    <kommentar>Preise von F</kommentar>\n     <option id=\"auflage\" name=\"Auflage\" type=\"Select\" default=\"1000\" require=\"true\" help=\"WÃ¤hlen Sie hier bitte Ihre Auflage\">\n<opt id=\"1000\" name=\"1.000 St.\"/>\n<opt id=\"2500\" name=\"2.500 St.\"/>\n<opt id=\"5000\" name=\"5.000 St.\"/>\n<opt id=\"10000\" name=\"10.000 St.\"/>\n<opt id=\"15000\" name=\"15.000 St.\"/>\n<opt id=\"20000\" name=\"20.000 St.\"/>\n<opt id=\"25000\" name=\"25.000 St.\"/>\n<opt id=\"30000\" name=\"30.000 St.\"/>\n\n</option>\n        \n      <option id=\"druck\" name=\"Druck\" type=\"Select\" default=\"44f\" help=\"Wie soll Ihr Produkt bedruckt werden?\">\n       <opt id=\"40f\" name=\"4/0-farbig (einseitig farbig, RÃ¼ckseite unbedruckt)\">\n        </opt>\n        <opt id=\"44f\" name=\"4/4-farbig (beidseitig farbig bedruckt)\">\n       </opt>\n      </option>\n\n\n     <option id=\"papier\" name=\"Papier\" type=\"Select\" help=\"Aus welchem Papier soll Ihr Produkt bestehen?\" default=\"bdg135\">\n        <opt id=\"bdg135\" name=\"135 g/mÂ² Bilderdruck glÃ¤nzend\">\n          <auflage>\n           <grenze formel=\"((24+10)*1.10)\">1000-1000</grenze>\n            <grenze formel=\"((28+10)*1.10)\">2500-2500</grenze>\n            <grenze formel=\"((29+10)*1.10)\">5000-5000</grenze>\n            <grenze formel=\"((55+10)*1.10)\">10000-10000</grenze>\n            <grenze formel=\"((79+10)*1.10)\">15000-15000</grenze>\n            <grenze formel=\"((95+10)*1.10)\">20000-20000</grenze>\n            <grenze formel=\"((119+10)*1.10)\">25000-25000</grenze>\n           <grenze formel=\"((142+10)*1.10)\">30000-30000</grenze>\n         </auflage>\n        </opt>\n        <opt id=\"bdm135\" name=\"135 g/mÂ² Bilderdruck matt\">\n         <auflage>\n           <grenze formel=\"((24+10)*1.10)\">1000-1000</grenze>\n            <grenze formel=\"((27+10)*1.10)\">2500-2500</grenze>\n            <grenze formel=\"((29+10)*1.10)\">5000-5000</grenze>\n            <grenze formel=\"((55+10)*1.10)\">10000-10000</grenze>\n            <grenze formel=\"((76+10)*1.10)\">15000-15000</grenze>\n            <grenze formel=\"((98+10)*1.10)\">20000-20000</grenze>\n            <grenze formel=\"((115+10)*1.10)\">25000-25000</grenze>\n           <grenze formel=\"((137+10)*1.10)\">30000-30000</grenze>\n         </auflage>\n        </opt>\n        <opt id=\"bdm170\" name=\"170 g/mÂ² Bilderdruck matt\">\n         <auflage>\n           <grenze formel=\"((27+10)*1.10)\">1000-1000</grenze>\n            <grenze formel=\"((33+10)*1.10)\">2500-2500</grenze>\n            <grenze formel=\"((43+10)*1.10)\">5000-5000</grenze>\n            <grenze formel=\"((63+10)*1.10)\">10000-10000</grenze>\n            <grenze formel=\"((89+10)*1.10)\">15000-15000</grenze>\n            <grenze formel=\"((115+10)*1.10)\">20000-20000</grenze>\n           <grenze formel=\"((147+10)*1.10)\">25000-25000</grenze>\n           <grenze formel=\"((167+10)*1.10)\">30000-30000</grenze>\n         </auflage>\n        </opt>\n        <opt id=\"bdg250\" name=\"250 g/mÂ² Bilderdruck glÃ¤nzend\">\n          <auflage>\n           <grenze formel=\"((24+10)*1.10)\">1000-1000</grenze>\n            <grenze formel=\"((29+10)*1.10)\">2500-2500</grenze>\n            <grenze formel=\"((32+10)*1.10)\">5000-5000</grenze>\n            <grenze formel=\"((65+10)*1.10)\">10000-10000</grenze>\n            <grenze formel=\"((129+10)*1.10)\">15000-15000</grenze>\n           <grenze formel=\"((129+10)*1.10)\">20000-20000</grenze>\n           <grenze formel=\"((216+10)*1.10)\">25000-25000</grenze>\n           <grenze formel=\"((216+10)*1.10)\">30000-30000</grenze>\n         </auflage>\n        </opt>\n        <opt id=\"bdm300\" name=\"300 g/mÂ² Bilderdruck matt mit beidseitig Lack\">\n         <auflage>\n           <grenze formel=\"((39+10)*1.10)\">1000-1000</grenze>\n            <grenze formel=\"((55+10)*1.10)\">2500-2500</grenze>\n            <grenze formel=\"((69+10)*1.10)\">5000-5000</grenze>\n            <grenze formel=\"((114+10)*1.10)\">10000-10000</grenze>\n           <grenze formel=\"((171+10)*1.10)\">15000-15000</grenze>\n           <grenze formel=\"((216+10)*1.10)\">20000-20000</grenze>\n           <grenze formel=\"((268+10)*1.10)\">25000-25000</grenze>\n           <grenze formel=\"((315+10)*1.10)\">30000-30000</grenze>\n         </auflage>\n        </opt>\n\n      </option>\n\n     <option id=\"produktion\" name=\"Produktionszeit\" type=\"Select\" default=\"std\" help=\"Entscheiden Sie, wieviel Tage wir fÃ¼r Ihren Auftrag einplanen dÃ¼rfen - Dateneingang bis 10 Uhr\">\n       <opt id=\"std\" name=\"Standard: 4 Arbeitstage\">\n         <auflage>\n           <grenze pauschale=\"0\">1-</grenze>\n         </auflage>\n        </opt>\n        <opt id=\"exp\" name=\"Express: 2 Arbeitstage\">\n          <auflage>\n           <grenze pauschale=\"11\">1-2500</grenze>\n            <grenze pauschale=\"14\">2501-5000</grenze>\n           <grenze pauschale=\"17\">5001-10000</grenze>\n            <grenze pauschale=\"26\">10001-25000</grenze>\n           <grenze pauschale=\"32\">25001-</grenze>\n          </auflage>\n        </opt>\n      </option>\n\n\n     <option id=\"datenformat\" name=\"Datenformat\" type=\"Select\" default=\"pdf\" help=\"In welchem Dateiformat liefern Sie uns Ihre druckfertigen Daten?\">\n        <opt id=\"pdf\" name=\"PDF\">\n         <auflage>\n           <grenze pauschale=\"0\">1-</grenze>\n         </auflage>\n        </opt>\n        <opt id=\"jpg\" name=\"JPG\">\n         <auflage>\n           <grenze formel=\"(3)\">1-</grenze>\n          </auflage>\n        </opt>\n        <opt id=\"andere\" name=\"Andere\">\n         <auflage>\n           <grenze formel=\"(5)\">1-</grenze>\n          </auflage>\n        </opt>\n      </option>\n\n     \n    <option id=\"datenuebertragung\" name=\"DatenÃ¼bertragung\" type=\"Select\" default=\"upload\" help=\"Wie wollen Sie uns Ihre Daten Ã¼bertragen?\">\n       <opt id=\"upload\" name=\"Per Upload am Bestellende\">\n          <auflage>\n           <grenze pauschale=\"0\">1-</grenze>\n         </auflage>\n        </opt>\n        <opt id=\"mail\" name=\"Per E-Mail\">\n         <auflage>\n           <grenze formel=\"(0)\">1-</grenze>\n          </auflage>\n        </opt>\n        <opt id=\"post\" name=\"Per Post\">\n         <auflage>\n           <grenze formel=\"(4)\">1-</grenze>\n          </auflage>\n        </opt>\n      </option>\n\n\n </artikel>\n\n\n\n</kalkulation>\n', '0', null, null, null, null, null, null, null, null, '23.81', null, null, '', '0001-578b5c52-4ff68764-fbf5-f3dafcdb', '0', '', null, null, null, null, null, '', 'previewxslfo.fop', '0', '0.00', '0', '0', '0', '1', '0', '292', '1', '0', '0', '0', 'extendpreviewxslfo.fop', '3', '1', '0', '0', '0', '0', '0', '0', '', '', '0', '1', '50', '1', '30', '0', '0', '0', '0', '', '', '1', '140', '0', '', '', '0', '', '0', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0', '0', '0', '0', '0', '', '', '', '', '0', '0', '0', '0', '0', '0', '0', '1', '', '', '', '', '', '', '', '0', null, null, null, '0', null, null, null, null, null, null, '0', '', null, '0', '10', '0', '10', null, null, '0', null, null, null, null, null, null), ('3', '2012-08-07 23:38:41', '2012-08-27 10:31:14', '1', '1', null, '9225', 'Trauerkarte ABC Variante 1', 'trauerkarte-abc-variante-1', '6', null, '1', '0', '19', '', '0', '<div>\r\n            <h3 class=\"product_title\">Trauerkarte Blumen am Abend</h3>\r\n            <div class=\"pers_text\">\r\n             <div class=\"product_desc\" style=\"padding-top: 5px;\">Klappkarte: Querformat, 170 x 114 mm (geschlossen)\r\n</div>\r\n            </div>\r\n          </div>\r\n          <br>\r\n          <div class=\"pers_text product_desc\">\r\n          <ul class=\"pers_circle\"><li><h4 style=\"font: bold 1em/1.25 Arial,Helvetica,sans-serif;\">Druck auf hochwertigem, matten 300g Papier</h4></li><li><h4 style=\"font: bold 1em/1.25 Arial,Helvetica,sans-serif;\">inkl. Trauer-Umschlag</h4></li></ul>\r\n          </div>', '', '300', null, null, '6661246764f97286cc4df4ac8cf0892ae421301a', '37a3e4d9b1cb548457c2a859ce25d2f26afdb64f', 'config.xml', 'pages.xml', 'pagetemplates.xml', 'pageobjects.xml', 'xslfo.fop', '5/6/0/2/', '1', '0', '<?xml version=\"1.0\" encoding=\"utf-8\"?>\n<kalkulation>\n  <artikel>\n   <name>Offsetdruck</name>\n    <kommentar>Plakate DIN A1</kommentar>\n\n\n     <option id=\"auflage\" name=\"Auflage\" type=\"Select\" default=\"100\" require=\"true\" help=\"WÃ¤hlen Sie hier bitte Ihre Auflage\">\n<opt id=\"100\" name=\"100 St.\"/>\n<opt id=\"250\" name=\"250 St.\"/>\n<opt id=\"500\" name=\"500 St.\"/>\n<opt id=\"1000\" name=\"1.000 St.\"/>\n<opt id=\"2000\" name=\"2.000 St.\"/>\n</option>\n       \n\n\n\n      <option id=\"produktion\" name=\"Produktionszeit\" type=\"Select\" default=\"std\" help=\"Entscheiden Sie, wieviel Tage wir fÃ¼r Ihren Auftrag einplanen dÃ¼rfen - Dateneingang bis 10 Uhr\">\n       <opt id=\"std\" name=\"Standard: 4 Arbeitstage\">\n         <auflage>\n           <grenze pauschale=\"15\">1-</grenze>\n          </auflage>\n        </opt>\n        <opt id=\"exp\" name=\"Express: 2 Arbeitstage\">\n          <auflage>\n           <grenze pauschale=\"11\">1-250</grenze>\n           <grenze pauschale=\"14\">500-500</grenze>\n           <grenze pauschale=\"17\">1000-1000</grenze>\n           <grenze pauschale=\"23\">2000-2000</grenze>\n         </auflage>\n        </opt>\n      </option>\n\n\n     <option id=\"datenformat\" name=\"Datenformat\" type=\"Select\" default=\"pdf\" help=\"In welchem Dateiformat liefern Sie uns Ihre druckfertigen Daten?\">\n        <opt id=\"pdf\" name=\"PDF\">\n         <auflage>\n           <grenze pauschale=\"0\">1-</grenze>\n         </auflage>\n        </opt>\n        <opt id=\"jpg\" name=\"JPG\">\n         <auflage>\n           <grenze pauschale=\"0\">1-</grenze>\n         </auflage>\n        </opt>\n        <opt id=\"andere\" name=\"Andere\">\n         <auflage>\n           <grenze pauschale=\"5\">1-</grenze>\n         </auflage>\n        </opt>\n      </option>\n\n\n     \n    <option id=\"datenuebertragung\" name=\"DatenÃ¼bertragung\" type=\"Select\" default=\"upload\" help=\"Wie wollen Sie uns Ihre Daten Ã¼bertragen?\">\n       <opt id=\"upload\" name=\"Per Upload am Bestellende\">\n          <auflage>\n           <grenze pauschale=\"0\">1-</grenze>\n         </auflage>\n        </opt>\n        <opt id=\"mail\" name=\"Per E-Mail\">\n         <auflage>\n           <grenze pauschale=\"0\">1-</grenze>\n         </auflage>\n        </opt>\n        <opt id=\"post\" name=\"Per Post (+3,00 EUR)\">\n         <auflage>\n           <grenze pauschale=\"3\">1-</grenze>\n         </auflage>\n        </opt>\n      </option>\n\n\n </artikel>\n\n\n  <artikel>\n   <name>Digitaldruck</name>\n   <kommentar>Plakate DIN A1</kommentar>\n\n\n     <option id=\"auflage\" name=\"Auflage\" type=\"Select\" default=\"2\" require=\"true\" help=\"WÃ¤hlen Sie hier bitte Ihre Auflage\">\n<opt id=\"1\" name=\"1 St.\"/>\n<opt id=\"2\" name=\"2 St.\"/>\n<opt id=\"3\" name=\"3 St.\"/>\n<opt id=\"4\" name=\"4 St.\"/>\n<opt id=\"5\" name=\"5 St.\"/>\n<opt id=\"6\" name=\"6 St.\"/>\n<opt id=\"7\" name=\"7 St.\"/>\n<opt id=\"8\" name=\"8 St.\"/>\n<opt id=\"9\" name=\"9 St.\"/>\n<opt id=\"10\" name=\"10 St.\"/>\n<opt id=\"11\" name=\"11 St.\"/>\n<opt id=\"12\" name=\"12 St.\"/>\n<opt id=\"13\" name=\"13 St.\"/>\n<opt id=\"14\" name=\"14 St.\"/>\n<opt id=\"15\" name=\"15 St.\"/>\n<opt id=\"20\" name=\"20 St.\"/>\n<opt id=\"25\" name=\"25 St.\"/>\n</option>\n       \n\n      <option id=\"papier\" name=\"Papier\" type=\"Select\" default=\"indoor140\" help=\"Aus welchem Papier soll Ihr Produkt bestehen?\">\n       <opt id=\"indoor140\" name=\"145 g/mÂ² Indoor Papier\">\n         <auflage>\n     <grenze formel=\"((16+6)*1.10)\">1</grenze>\n     <grenze formel=\"((19+6)*1.10)\">2</grenze>\n     <grenze formel=\"((23+6)*1.10)\">3</grenze>\n     <grenze formel=\"((26+6)*1.10)\">4</grenze>\n     <grenze formel=\"((31+6)*1.10)\">5</grenze>\n     <grenze formel=\"((34+6)*1.10)\">6</grenze>\n     <grenze formel=\"((37+6)*1.10)\">7</grenze>\n     <grenze formel=\"((39+6)*1.10)\">8</grenze>\n     <grenze formel=\"((42+6)*1.10)\">9</grenze>\n     <grenze formel=\"((44+6)*1.10)\">10</grenze>\n      <grenze formel=\"((47+6)*1.10)\">11</grenze>\n      <grenze formel=\"((49+6)*1.10)\">12</grenze>\n      <grenze formel=\"((52+6)*1.10)\">13</grenze>\n      <grenze formel=\"((54+6)*1.10)\">14</grenze>\n      <grenze formel=\"((57+6)*1.10)\">15</grenze>\n      <grenze formel=\"((78+6)*1.10)\">20</grenze>\n      <grenze formel=\"((92+6)*1.10)\">25</grenze>\n          </auflage>\n        </opt>\n        <opt id=\"affiche120\" name=\"120 g/mÂ² Affichenpapier (AuÃŸenbereich)\">\n         <auflage>\n     <grenze formel=\"((16+6)*1.10)\">1</grenze>\n     <grenze formel=\"((19+6)*1.10)\">2</grenze>\n     <grenze formel=\"((24+6)*1.10)\">3</grenze>\n     <grenze formel=\"((27+6)*1.10)\">4</grenze>\n     <grenze formel=\"((32+6)*1.10)\">5</grenze>\n     <grenze formel=\"((35+6)*1.10)\">6</grenze>\n     <grenze formel=\"((39+6)*1.10)\">7</grenze>\n     <grenze formel=\"((41+6)*1.10)\">8</grenze>\n     <grenze formel=\"((44+6)*1.10)\">9</grenze>\n     <grenze formel=\"((46+6)*1.10)\">10</grenze>\n      <grenze formel=\"((50+6)*1.10)\">11</grenze>\n      <grenze formel=\"((52+6)*1.10)\">12</grenze>\n      <grenze formel=\"((55+6)*1.10)\">13</grenze>\n      <grenze formel=\"((57+6)*1.10)\">14</grenze>\n      <grenze formel=\"((61+6)*1.10)\">15</grenze>\n      <grenze formel=\"((82+6)*1.10)\">20</grenze>\n      <grenze formel=\"((98+6)*1.10)\">25</grenze>\n          </auflage>\n        </opt>\n        <opt id=\"foto190\" name=\"185 g/mÂ² Fotopapier (fÃ¼r Fotos, Kunstdrucke)\">\n          <auflage>\n     <grenze formel=\"((17+6)*1.10)\">1</grenze>\n     <grenze formel=\"((22+6)*1.10)\">2</grenze>\n     <grenze formel=\"((28+6)*1.10)\">3</grenze>\n     <grenze formel=\"((33+6)*1.10)\">4</grenze>\n     <grenze formel=\"((39+6)*1.10)\">5</grenze>\n     <grenze formel=\"((43+6)*1.10)\">6</grenze>\n     <grenze formel=\"((48+6)*1.10)\">7</grenze>\n     <grenze formel=\"((51+6)*1.10)\">8</grenze>\n     <grenze formel=\"((56+6)*1.10)\">9</grenze>\n     <grenze formel=\"((60+6)*1.10)\">10</grenze>\n      <grenze formel=\"((64+6)*1.10)\">11</grenze>\n      <grenze formel=\"((68+6)*1.10)\">12</grenze>\n      <grenze formel=\"((73+6)*1.10)\">13</grenze>\n      <grenze formel=\"((76+6)*1.10)\">14</grenze>\n      <grenze formel=\"((81+6)*1.10)\">15</grenze>\n      <grenze formel=\"((109+6)*1.10)\">20</grenze>\n     <grenze formel=\"((131+6)*1.10)\">25</grenze>\n         </auflage>\n        </opt>\n      </option>\n\n\n\n     <option id=\"produktion\" name=\"Produktionszeit\" type=\"Select\" default=\"std\" help=\"Entscheiden Sie, wieviel Tage wir fÃ¼r Ihren Auftrag einplanen dÃ¼rfen - Dateneingang bis 10 Uhr\">\n       <opt id=\"std\" name=\"Standard: 4 Arbeitstage\">\n         <auflage>\n           <grenze pauschale=\"0\">1-</grenze>\n         </auflage>\n        </opt>\n        <opt id=\"exp\" name=\"Express: 2 Arbeitstage\">\n          <auflage>\n           <grenze pauschale=\"11\">1-15</grenze>\n            <grenze pauschale=\"14\">16-50</grenze>\n         </auflage>\n        </opt>\n      </option>\n\n\n     <option id=\"datenformat\" name=\"Datenformat\" type=\"Select\" default=\"pdf\" help=\"In welchem Dateiformat liefern Sie uns Ihre druckfertigen Daten?\">\n        <opt id=\"pdf\" name=\"PDF\">\n         <auflage>\n           <grenze pauschale=\"0\">1-</grenze>\n         </auflage>\n        </opt>\n        <opt id=\"jpg\" name=\"JPG\">\n         <auflage>\n           <grenze pauschale=\"0\">1-</grenze>\n         </auflage>\n        </opt>\n        <opt id=\"andere\" name=\"Andere\">\n         <auflage>\n           <grenze pauschale=\"5\">1-</grenze>\n         </auflage>\n        </opt>\n      </option>\n\n\n     \n    <option id=\"datenuebertragung\" name=\"DatenÃ¼bertragung\" type=\"Select\" default=\"upload\" help=\"Wie wollen Sie uns Ihre Daten Ã¼bertragen?\">\n       <opt id=\"upload\" name=\"Per Upload am Bestellende\">\n          <auflage>\n           <grenze pauschale=\"0\">1-</grenze>\n         </auflage>\n        </opt>\n        <opt id=\"mail\" name=\"Per E-Mail\">\n         <auflage>\n           <grenze pauschale=\"0\">1-</grenze>\n         </auflage>\n        </opt>\n        <opt id=\"post\" name=\"Per Post (+3,00 EUR)\">\n         <auflage>\n           <grenze pauschale=\"3\">1-</grenze>\n         </auflage>\n        </opt>\n      </option>\n\n </artikel>\n\n</kalkulation>\n', '0', null, null, null, null, null, null, null, null, '33.32', null, null, '', '0001-4fc31fe2-50218ae1-c791-8c9c413e', '2', '', null, null, null, null, null, null, 'previewxslfo.fop', '0', '0.00', '5438', '1', '1', '1', '0', '210', '1', '0', '0', '0', 'extendpreviewxslfo.fop', '0', '0', '0', '0', '0', '0', '0', '0', '', '', '0', '1', '0', '0', '0', '1', '0', '0', '0', '', 'Das sind meine Datenhinweise.<br><br>', '0', '0', '0', '', '', '1', '', '0', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '22700ac9cd407af6430aedbd3c569e03668bb13f', '', '', '', '', '', '', '', '', '', '0', '0', '0', '0', '0', '', '', '', '', '0', '0', '0', '0', '0', '0', '0', '0', '', '', '', '', '', '', '', '0', '', '', '', '1', '140', '<?xml version=\"1.0\" encoding=\"utf-8\"?>\n<settings>\n  <version>1.1</version>\n  <includes>\n    <include type=\"css\">trauerkarten.css</include>\n    <include type=\"javascript\">trauerkarten.js</include>\n  </includes>\n <general>\n   <size width=\"900\" height=\"500\"/>\n    <headline>Gestalten Sie Ihren Artikel nach Wunsch</headline>\n    <buttons>\n     <finish/>\n     <preview/>\n      <cancel label=\"Abbrechen\"/>\n     <next/>\n     <back/>   \n    </buttons>\n  </general>\n  <rendering>\n   <sites>\n     <site id=\"1\" crop-left=\"10\" width=\"470\" height=\"334\">\n       <elements>\n          <motiv render-mode=\"image\" render-pos-x=\"0\" render-pos-y=\"0\" render-layer=\"0\" render-pos-width=\"470\" value=\"0001-4fc30a60-5001c6b7-ef86-ab17a5c8\" render-pos-height=\"334\" />\n          <text render-mode=\"text\" render-size=\"15\" render-pos-x=\"100\" render-pos-y=\"320\" render-layer=\"10\" id=\"copyright\" value=\"@PrintshopCreator GmbH\" />\n        </elements>\n     </site>\n     <site id=\"2\" crop-right=\"10\" width=\"470\" height=\"334\">\n        <elements>\n          <motiv render-mode=\"image\" render-pos-x=\"0\" render-pos-y=\"0\" render-layer=\"0\" render-pos-width=\"470\" value=\"0001-4fc30a60-5001c6b7-ef86-ab17a5c8\" render-pos-height=\"334\" />\n          <text render-mode=\"text\" render-size=\"15\" render-pos-x=\"100\" render-pos-y=\"320\" render-layer=\"10\" id=\"copyright\" value=\"@PrintshopCreator GmbH\" />\n        </elements>\n     </site>\n   </sites>\n  </rendering>\n  <steps>\n   <step id=\"1\" label=\"Vorderseite\" layout=\"column\">\n     <headline>Vorderseite</headline>\n      <description>bitte eingeben</description>\n     <elements>\n        <formtext color-select=\"1\" render-pos-x=\"100\" render-size=\"18\" render-pos-y=\"100\" render-site=\"1\" render-mode=\"text\" render-layer=\"10\" data-binding=\"firstname\" id=\"vorname\" label=\"Vorname\" value=\"Ihr Vorname\" width=\"100\" column=\"1\"/>\n       <formtext render-pos-x=\"100\" render-size=\"18\" render-pos-y=\"50\" render-site=\"1\" render-mode=\"text\" render-layer=\"15\" data-binding=\"lastname\" id=\"nachname\" label=\"Nachname\" value=\"Ihr Nachname\" width=\"100\" column=\"1\" />\n        <formupload render-pos-x=\"100\" render-pos-y=\"50\" render-site=\"1\" render-mode=\"upload\" render-layer=\"15\" data-binding=\"logo\" id=\"logo\" label=\"Logo\" value=\"Ihr Logo\" render-pos-width=\"100\" render-pos-height=\"100\" column=\"1\" />\n        <preview id=\"preview\" label=\"Vorschau\" render-site=\"1\" width=\"400\" height=\"370\" pos=\"right\" column=\"2\" />\n     </elements>\n   </step>\n   <step id=\"2\" label=\"RÃ¼ckseite\" layout=\"column\">\n      <headline>RÃ¼ckseite</headline>\n     <description>bitte eingeben</description>\n     <elements>\n        <formtextarea render-pos-x=\"100\" render-size=\"18\" render-pos-width=\"200\" render-pos-height=\"100\" render-pos-y=\"100\" render-site=\"2\" render-mode=\"textarea\" render-layer=\"15\" color-select=\"1\" data-binding=\"slogan\" id=\"slogan\" label=\"Slogan\" value=\"Ihr Slogan\" width=\"100\" column=\"1\"  />\n        <preview id=\"preview\" label=\"Vorschau\" width=\"400\" height=\"370\" render-site=\"2\" column=\"2\" />\n     </elements>\n   </step>\n   <step id=\"3\" label=\"Vorschau\">\n      <headline>Vorschau</headline>\n     <description>Schauen Sie es sich gut an</description>\n     <elements>\n        <preview id=\"preview\" label=\"Vorschau\" width=\"400\" height=\"370\" render-site=\"all\" />\n      </elements>\n   </step>\n </steps>\n</settings>', null, null, null, null, '0', '', null, '0', '10', '0', '10', null, null, '0', null, null, null, null, null, null), ('4', '2012-08-13 23:45:34', '2012-08-22 15:22:03', '1', '1', null, '9225', 'Trauerkarte ABC Variante 2', 'trauerkarte-abc-variante-2', '6', null, '1', '0', '19', '', '0', 'Mit Slogan', '', '400', null, null, 'b0729c6c886e18935f4d7fe1014992f59ca3e366', 'f828a093a997723debffe9625e15428d1df776fa', 'config.xml', 'pages.xml', 'pagetemplates.xml', 'pageobjects.xml', 'xslfo.fop', '5/7/3/0/', '1', '0', '<?xml version=\"1.0\" encoding=\"utf-8\"?>\n<kalkulation>\n <artikel>\n   <name>Offsetdruck</name>\n    <kommentar>Plakate DIN A1</kommentar>\n\n\n     <option id=\"auflage\" name=\"Auflage\" type=\"Select\" default=\"100\" require=\"true\" help=\"WÃ¤hlen Sie hier bitte Ihre Auflage\">\n<opt id=\"100\" name=\"100 St.\"/>\n<opt id=\"250\" name=\"250 St.\"/>\n<opt id=\"500\" name=\"500 St.\"/>\n<opt id=\"1000\" name=\"1.000 St.\"/>\n<opt id=\"2000\" name=\"2.000 St.\"/>\n</option>\n       \n\n\n\n      <option id=\"produktion\" name=\"Produktionszeit\" type=\"Select\" default=\"std\" help=\"Entscheiden Sie, wieviel Tage wir fÃ¼r Ihren Auftrag einplanen dÃ¼rfen - Dateneingang bis 10 Uhr\">\n       <opt id=\"std\" name=\"Standard: 4 Arbeitstage\">\n         <auflage>\n           <grenze pauschale=\"15\">1-</grenze>\n          </auflage>\n        </opt>\n        <opt id=\"exp\" name=\"Express: 2 Arbeitstage\">\n          <auflage>\n           <grenze pauschale=\"11\">1-250</grenze>\n           <grenze pauschale=\"14\">500-500</grenze>\n           <grenze pauschale=\"17\">1000-1000</grenze>\n           <grenze pauschale=\"23\">2000-2000</grenze>\n         </auflage>\n        </opt>\n      </option>\n\n\n     <option id=\"datenformat\" name=\"Datenformat\" type=\"Select\" default=\"pdf\" help=\"In welchem Dateiformat liefern Sie uns Ihre druckfertigen Daten?\">\n        <opt id=\"pdf\" name=\"PDF\">\n         <auflage>\n           <grenze pauschale=\"0\">1-</grenze>\n         </auflage>\n        </opt>\n        <opt id=\"jpg\" name=\"JPG\">\n         <auflage>\n           <grenze pauschale=\"0\">1-</grenze>\n         </auflage>\n        </opt>\n        <opt id=\"andere\" name=\"Andere\">\n         <auflage>\n           <grenze pauschale=\"5\">1-</grenze>\n         </auflage>\n        </opt>\n      </option>\n\n\n     \n    <option id=\"datenuebertragung\" name=\"DatenÃ¼bertragung\" type=\"Select\" default=\"upload\" help=\"Wie wollen Sie uns Ihre Daten Ã¼bertragen?\">\n       <opt id=\"upload\" name=\"Per Upload am Bestellende\">\n          <auflage>\n           <grenze pauschale=\"0\">1-</grenze>\n         </auflage>\n        </opt>\n        <opt id=\"mail\" name=\"Per E-Mail\">\n         <auflage>\n           <grenze pauschale=\"0\">1-</grenze>\n         </auflage>\n        </opt>\n        <opt id=\"post\" name=\"Per Post (+3,00 EUR)\">\n         <auflage>\n           <grenze pauschale=\"3\">1-</grenze>\n         </auflage>\n        </opt>\n      </option>\n\n\n </artikel>\n\n\n  <artikel>\n   <name>Digitaldruck</name>\n   <kommentar>Plakate DIN A1</kommentar>\n\n\n     <option id=\"auflage\" name=\"Auflage\" type=\"Select\" default=\"2\" require=\"true\" help=\"WÃ¤hlen Sie hier bitte Ihre Auflage\">\n<opt id=\"1\" name=\"1 St.\"/>\n<opt id=\"2\" name=\"2 St.\"/>\n<opt id=\"3\" name=\"3 St.\"/>\n<opt id=\"4\" name=\"4 St.\"/>\n<opt id=\"5\" name=\"5 St.\"/>\n<opt id=\"6\" name=\"6 St.\"/>\n<opt id=\"7\" name=\"7 St.\"/>\n<opt id=\"8\" name=\"8 St.\"/>\n<opt id=\"9\" name=\"9 St.\"/>\n<opt id=\"10\" name=\"10 St.\"/>\n<opt id=\"11\" name=\"11 St.\"/>\n<opt id=\"12\" name=\"12 St.\"/>\n<opt id=\"13\" name=\"13 St.\"/>\n<opt id=\"14\" name=\"14 St.\"/>\n<opt id=\"15\" name=\"15 St.\"/>\n<opt id=\"20\" name=\"20 St.\"/>\n<opt id=\"25\" name=\"25 St.\"/>\n</option>\n       \n\n      <option id=\"papier\" name=\"Papier\" type=\"Select\" default=\"indoor140\" help=\"Aus welchem Papier soll Ihr Produkt bestehen?\">\n       <opt id=\"indoor140\" name=\"145 g/mÂ² Indoor Papier\">\n         <auflage>\n     <grenze formel=\"((16+6)*1.10)\">1</grenze>\n     <grenze formel=\"((19+6)*1.10)\">2</grenze>\n     <grenze formel=\"((23+6)*1.10)\">3</grenze>\n     <grenze formel=\"((26+6)*1.10)\">4</grenze>\n     <grenze formel=\"((31+6)*1.10)\">5</grenze>\n     <grenze formel=\"((34+6)*1.10)\">6</grenze>\n     <grenze formel=\"((37+6)*1.10)\">7</grenze>\n     <grenze formel=\"((39+6)*1.10)\">8</grenze>\n     <grenze formel=\"((42+6)*1.10)\">9</grenze>\n     <grenze formel=\"((44+6)*1.10)\">10</grenze>\n      <grenze formel=\"((47+6)*1.10)\">11</grenze>\n      <grenze formel=\"((49+6)*1.10)\">12</grenze>\n      <grenze formel=\"((52+6)*1.10)\">13</grenze>\n      <grenze formel=\"((54+6)*1.10)\">14</grenze>\n      <grenze formel=\"((57+6)*1.10)\">15</grenze>\n      <grenze formel=\"((78+6)*1.10)\">20</grenze>\n      <grenze formel=\"((92+6)*1.10)\">25</grenze>\n          </auflage>\n        </opt>\n        <opt id=\"affiche120\" name=\"120 g/mÂ² Affichenpapier (AuÃŸenbereich)\">\n         <auflage>\n     <grenze formel=\"((16+6)*1.10)\">1</grenze>\n     <grenze formel=\"((19+6)*1.10)\">2</grenze>\n     <grenze formel=\"((24+6)*1.10)\">3</grenze>\n     <grenze formel=\"((27+6)*1.10)\">4</grenze>\n     <grenze formel=\"((32+6)*1.10)\">5</grenze>\n     <grenze formel=\"((35+6)*1.10)\">6</grenze>\n     <grenze formel=\"((39+6)*1.10)\">7</grenze>\n     <grenze formel=\"((41+6)*1.10)\">8</grenze>\n     <grenze formel=\"((44+6)*1.10)\">9</grenze>\n     <grenze formel=\"((46+6)*1.10)\">10</grenze>\n      <grenze formel=\"((50+6)*1.10)\">11</grenze>\n      <grenze formel=\"((52+6)*1.10)\">12</grenze>\n      <grenze formel=\"((55+6)*1.10)\">13</grenze>\n      <grenze formel=\"((57+6)*1.10)\">14</grenze>\n      <grenze formel=\"((61+6)*1.10)\">15</grenze>\n      <grenze formel=\"((82+6)*1.10)\">20</grenze>\n      <grenze formel=\"((98+6)*1.10)\">25</grenze>\n          </auflage>\n        </opt>\n        <opt id=\"foto190\" name=\"185 g/mÂ² Fotopapier (fÃ¼r Fotos, Kunstdrucke)\">\n          <auflage>\n     <grenze formel=\"((17+6)*1.10)\">1</grenze>\n     <grenze formel=\"((22+6)*1.10)\">2</grenze>\n     <grenze formel=\"((28+6)*1.10)\">3</grenze>\n     <grenze formel=\"((33+6)*1.10)\">4</grenze>\n     <grenze formel=\"((39+6)*1.10)\">5</grenze>\n     <grenze formel=\"((43+6)*1.10)\">6</grenze>\n     <grenze formel=\"((48+6)*1.10)\">7</grenze>\n     <grenze formel=\"((51+6)*1.10)\">8</grenze>\n     <grenze formel=\"((56+6)*1.10)\">9</grenze>\n     <grenze formel=\"((60+6)*1.10)\">10</grenze>\n      <grenze formel=\"((64+6)*1.10)\">11</grenze>\n      <grenze formel=\"((68+6)*1.10)\">12</grenze>\n      <grenze formel=\"((73+6)*1.10)\">13</grenze>\n      <grenze formel=\"((76+6)*1.10)\">14</grenze>\n      <grenze formel=\"((81+6)*1.10)\">15</grenze>\n      <grenze formel=\"((109+6)*1.10)\">20</grenze>\n     <grenze formel=\"((131+6)*1.10)\">25</grenze>\n         </auflage>\n        </opt>\n      </option>\n\n\n\n     <option id=\"produktion\" name=\"Produktionszeit\" type=\"Select\" default=\"std\" help=\"Entscheiden Sie, wieviel Tage wir fÃ¼r Ihren Auftrag einplanen dÃ¼rfen - Dateneingang bis 10 Uhr\">\n       <opt id=\"std\" name=\"Standard: 4 Arbeitstage\">\n         <auflage>\n           <grenze pauschale=\"0\">1-</grenze>\n         </auflage>\n        </opt>\n        <opt id=\"exp\" name=\"Express: 2 Arbeitstage\">\n          <auflage>\n           <grenze pauschale=\"11\">1-15</grenze>\n            <grenze pauschale=\"14\">16-50</grenze>\n         </auflage>\n        </opt>\n      </option>\n\n\n     <option id=\"datenformat\" name=\"Datenformat\" type=\"Select\" default=\"pdf\" help=\"In welchem Dateiformat liefern Sie uns Ihre druckfertigen Daten?\">\n        <opt id=\"pdf\" name=\"PDF\">\n         <auflage>\n           <grenze pauschale=\"0\">1-</grenze>\n         </auflage>\n        </opt>\n        <opt id=\"jpg\" name=\"JPG\">\n         <auflage>\n           <grenze pauschale=\"0\">1-</grenze>\n         </auflage>\n        </opt>\n        <opt id=\"andere\" name=\"Andere\">\n         <auflage>\n           <grenze pauschale=\"5\">1-</grenze>\n         </auflage>\n        </opt>\n      </option>\n\n\n     \n    <option id=\"datenuebertragung\" name=\"DatenÃ¼bertragung\" type=\"Select\" default=\"upload\" help=\"Wie wollen Sie uns Ihre Daten Ã¼bertragen?\">\n       <opt id=\"upload\" name=\"Per Upload am Bestellende\">\n          <auflage>\n           <grenze pauschale=\"0\">1-</grenze>\n         </auflage>\n        </opt>\n        <opt id=\"mail\" name=\"Per E-Mail\">\n         <auflage>\n           <grenze pauschale=\"0\">1-</grenze>\n         </auflage>\n        </opt>\n        <opt id=\"post\" name=\"Per Post (+3,00 EUR)\">\n         <auflage>\n           <grenze pauschale=\"3\">1-</grenze>\n         </auflage>\n        </opt>\n      </option>\n\n </artikel>\n\n</kalkulation>\n', '0', null, null, null, null, null, null, null, null, '33.32', null, null, '', '0001-d95c8e64-5029757e-9ebc-f41c4fb7', '0', '', null, null, null, null, null, null, 'previewxslfo.fop', '0', '0.00', '5438', '1', '1', '1', '0', '31', '1', '0', '0', '0', 'extendpreviewxslfo.fop', '0', '0', '0', '0', '0', '0', '0', '0', '', '', '0', '1', '0', '0', '0', '1', '0', '0', '0', '', 'Das sind meine Datenhinweise.<br><br>', '0', '0', '0', '', '', '1', '', '0', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '9a75e30bc695d57e90d6f6729eddbbd922d13ddf', '', '', '', '', '', '', '', '', '', '0', '0', '0', '0', '0', '', '', '', '', '0', '0', '0', '0', '0', '0', '1', '0', '', '', '', '', '', '', '', '0', '', '', '', '1', '140', '<?xml version=\"1.0\" encoding=\"utf-8\"?>\n<settings>\n    <version>1.1</version>\n    <includes>\n        <include type=\"css\">trauerkarten.css</include>\n        <include type=\"javascript\">trauerkarten.js</include>\n    </includes>\n    <general>\n        <size width=\"900\" height=\"500\"/>\n        <headline>Gestalten Sie Ihren Artikel nach Wunsch</headline>\n        <buttons>\n            <finish/>\n            <preview/>\n            <cancel label=\"Abbrechen\"/>\n            <next/>\n            <back/>\n        </buttons>\n    </general>\n    <rendering>\n        <sites>\n            <site id=\"1\" crop-left=\"10\" width=\"470\" height=\"334\">\n                <elements>\n                    <motiv render-mode=\"motiv\" render-pos-x=\"0\" render-pos-y=\"0\" render-layer=\"0\" render-pos-width=\"470\" value=\"0001-4fc30a60-5001c6b7-ef86-ab17a5c8\" render-pos-height=\"334\" />\n                    <text render-mode=\"text\" render-size=\"15\" render-pos-x=\"100\" render-pos-y=\"320\" render-layer=\"10\" id=\"copyright\" value=\"@PrintshopCreator GmbH\" />\n                </elements>\n            </site>\n            <site id=\"2\" crop-right=\"10\" width=\"470\" height=\"334\">\n                <elements>\n                    <motiv render-mode=\"motiv\" render-pos-x=\"0\" render-pos-y=\"0\" render-layer=\"0\" render-pos-width=\"470\" value=\"0001-4fc30a60-5001c6b7-ef86-ab17a5c8\" render-pos-height=\"334\" />\n                    <text render-mode=\"text\" render-size=\"15\" render-pos-x=\"100\" render-pos-y=\"320\" render-layer=\"10\" id=\"copyright\" value=\"@PrintshopCreator GmbH\" />\n                </elements>\n            </site>\n        </sites>\n    </rendering>\n    <steps>\n        <step id=\"1\" label=\"Vorderseite\" layout=\"column\">\n            <headline>Vorderseite</headline>\n            <description>bitte eingeben</description>\n            <elements>\n                <formdesigntemplates id=\"formdesigntemplates\" label=\"Templates\">\n                </formdesigntemplates>\n                <formimageselect render-mode=\"formimageselect\" render-site=\"1\" render-pos-width=\"200\" render-pos-height=\"100\" value=\"leinen\" render-pos-x=\"100\" render-pos-y=\"100\" id=\"falten\" label=\"Falten\" column=\"1\" render-layer=\"15\" base-dir=\"/styles/vorlagen/bootstrap/steplayouter/img/examples/\">\n                    <options>\n                        <option id=\"leinen\" thumbnail=\"buchtyp_leinen2040.png\" file=\"leinen-1085.png\" style=\"width: 50px\">Leinen</option>\n                        <option id=\"megara\" thumbnail=\"buchtyp_megara.png\" file=\"megara-24080.png\" style=\"width: 50px\">Megara</option>\n                    </options>\n                </formimageselect>\n\n                <formtext color-select=\"1\" render-bold=\"1\" render-text-style=\"bold\" render-pos-x=\"100\" render-size=\"18\" render-pos-y=\"100\" render-site=\"1\" render-mode=\"text\" render-layer=\"10\" data-binding=\"firstname\" id=\"vorname\" label=\"Vorname\" value=\"Ihr Vorname\" width=\"100\" column=\"1\"/>\n                <formtext render-pos-x=\"100\" render-size=\"18\" render-text-style=\"bold\" render-pos-y=\"50\" render-site=\"1\" render-mode=\"text\" render-layer=\"15\" data-binding=\"lastname\" id=\"nachname\" label=\"Nachname\" value=\"Ihr Nachname\" width=\"100\" column=\"1\" />\n                <formupload render-pos-x=\"100\" render-pos-y=\"50\" render-site=\"1\" render-mode=\"formupload\" render-layer=\"15\" data-binding=\"logo\" id=\"logo\" label=\"Logo\" value=\"Ihr Logo\" render-pos-width=\"100\" render-pos-height=\"100\" column=\"1\" />\n                <preview id=\"preview\" label=\"Vorschau\" render-site=\"1\" width=\"400\" height=\"370\" pos=\"right\" column=\"2\" />\n            </elements>\n        </step>\n        <step id=\"2\" label=\"RÃ¼ckseite\" layout=\"column\">\n            <headline>RÃ¼ckseite</headline>\n            <description>bitte eingeben</description>\n            <elements>\n                <formtextarea render-pos-x=\"100\" render-size=\"18\" render-text-style=\"bold\" render-pos-width=\"200\" render-pos-height=\"100\" render-pos-y=\"100\" render-site=\"2\" render-mode=\"textarea\" render-layer=\"15\" color-select=\"1\" data-binding=\"slogan\" id=\"slogan\" label=\"Slogan\" value=\"Ihr Slogan\" width=\"100\" column=\"1\"  />\n                <preview id=\"preview\" label=\"Vorschau\" width=\"400\" height=\"370\" render-site=\"2\" column=\"2\" />\n            </elements>\n        </step>\n        <step id=\"3\" label=\"Vorschau\">\n            <headline>Vorschau</headline>\n            <description>Schauen Sie es sich gut an</description>\n            <elements>\n                <preview id=\"preview\" label=\"Vorschau\" width=\"400\" height=\"370\" render-site=\"all\" />\n            </elements>\n        </step>\n    </steps>\n</settings>', '{\"falten\":\"leinen\",\"vorname\":\"Ihr Vorname\",\"vorname_color\":\"#000000\",\"nachname\":\"Ihr Nachname\",\"slogan\":\"Ihr Slogan\",\"slogan_color\":\"#000000\"}', null, null, null, '0', '', null, '0', '10', '0', '10', null, null, '0', null, null, null, null, null, null);
COMMIT;

-- ----------------------------
--  Table structure for `article_buy`
-- ----------------------------
DROP TABLE IF EXISTS `article_buy`;
CREATE TABLE `article_buy` (
  `contact_id` int(8) NOT NULL,
  `article_id` int(8) NOT NULL,
  `id` int(8) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`),
  KEY `contact_id` (`contact_id`),
  KEY `article_id` (`article_id`),
  KEY `contact_article` (`contact_id`,`article_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
--  Table structure for `article_confirm_contact`
-- ----------------------------
DROP TABLE IF EXISTS `article_confirm_contact`;
CREATE TABLE `article_confirm_contact` (
  `contact_id` int(8) NOT NULL,
  `article_id` int(8) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
--  Table structure for `article_counter`
-- ----------------------------
DROP TABLE IF EXISTS `article_counter`;
CREATE TABLE `article_counter` (
  `counter_id` int(11) NOT NULL,
  `max_doc_id` int(11) NOT NULL,
  PRIMARY KEY (`counter_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
--  Table structure for `article_group`
-- ----------------------------
DROP TABLE IF EXISTS `article_group`;
CREATE TABLE `article_group` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `pid` bigint(20) DEFAULT NULL,
  `createdd` date DEFAULT NULL,
  `updatedd` date DEFAULT NULL,
  `createdt` time DEFAULT NULL,
  `updatedt` time DEFAULT NULL,
  `install_id` bigint(20) DEFAULT NULL,
  `shop_id` bigint(20) DEFAULT NULL,
  `account_id` bigint(20) DEFAULT NULL,
  `parent` bigint(20) DEFAULT NULL,
  `pos` int(3) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `url` varchar(255) NOT NULL,
  `text` text,
  `language` varchar(255) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `enable` tinyint(1) DEFAULT '1',
  `notinmenu` tinyint(1) DEFAULT '0',
  `parameter` varchar(255) NOT NULL,
  `meta_keywords` text NOT NULL,
  `meta_description` text NOT NULL,
  `meta_author` varchar(255) NOT NULL,
  `meta_custom_title` varchar(255) NOT NULL,
  `meta_og_title` varchar(255) NOT NULL,
  `meta_og_type` varchar(255) NOT NULL,
  `meta_og_url` varchar(255) NOT NULL,
  `meta_og_image` varchar(255) NOT NULL,
  `meta_og_description` text NOT NULL,
  `image_big` varchar(40) NOT NULL,
  `display_title` int(1) NOT NULL DEFAULT '1',
  `lang_data` longtext,
  PRIMARY KEY (`id`),
  KEY `install_id_idx` (`install_id`),
  KEY `shop_id_idx` (`shop_id`),
  KEY `account_id_idx` (`account_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 AVG_ROW_LENGTH=682 ROW_FORMAT=COMPACT;

-- ----------------------------
--  Records of `article_group`
-- ----------------------------
BEGIN;
INSERT INTO `article_group` VALUES ('1', null, null, null, null, null, '1', '1', null, '0', '0', 'Trauerkarten', 'trauerkarten', null, 'all', '', '1', '0', '', '', '', '', '', '', '', '', '', '', '', '1', null), ('2', null, null, null, null, null, '1', '1', null, '0', '0', 'Sterbebilder', 'sterbebilder', null, 'all', '', '1', '0', '', '', '', '', '', '', '', '', '', '', '', '1', null), ('3', null, null, null, null, null, '1', '1', null, '0', '800', 'Trauerbanner', 'trauerbanner', '<h1 align=\"center\"><font size=\"2\">â€‹Trauerbanner sind eine besondere Bereicherung fÃ¼r jede Beisetzung.</font></h1>\n<h1 align=\"center\"><font size=\"2\">Dezent sorgen Sie fÃ¼r Stimmung und <i><span class=\"st\"><em>AtmosphÃ¤re und zeigen auf Wunsch den Verstorbenen - so wie Sie Ihn in Erninnerung halten wollen.</em></span></i></font></h1>\n', 'all', '', '1', '0', '', '', '', '', '', '', '', '', '', '', '', '1', null);
COMMIT;

-- ----------------------------
--  Table structure for `article_group_article`
-- ----------------------------
DROP TABLE IF EXISTS `article_group_article`;
CREATE TABLE `article_group_article` (
  `articlegroup_id` bigint(20) NOT NULL DEFAULT '0',
  `article_id` bigint(20) NOT NULL DEFAULT '0',
  PRIMARY KEY (`articlegroup_id`,`article_id`),
  KEY `article_id` (`article_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AVG_ROW_LENGTH=1260 ROW_FORMAT=COMPACT;

-- ----------------------------
--  Records of `article_group_article`
-- ----------------------------
BEGIN;
INSERT INTO `article_group_article` VALUES ('1', '1'), ('2', '1'), ('1', '2'), ('1', '3');
COMMIT;

-- ----------------------------
--  Table structure for `article_group_market_article`
-- ----------------------------
DROP TABLE IF EXISTS `article_group_market_article`;
CREATE TABLE `article_group_market_article` (
  `articlegroup_id` bigint(20) NOT NULL DEFAULT '0',
  `article_id` bigint(20) NOT NULL DEFAULT '0',
  PRIMARY KEY (`articlegroup_id`,`article_id`),
  KEY `article_id` (`article_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
--  Table structure for `article_offers`
-- ----------------------------
DROP TABLE IF EXISTS `article_offers`;
CREATE TABLE `article_offers` (
  `id` int(8) NOT NULL AUTO_INCREMENT,
  `article_id` int(8) DEFAULT NULL,
  `data` longblob,
  `created` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `article_taggable_tag`
-- ----------------------------
DROP TABLE IF EXISTS `article_taggable_tag`;
CREATE TABLE `article_taggable_tag` (
  `tag_id` int(8) DEFAULT NULL,
  `id` int(8) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
--  Table structure for `article_theme`
-- ----------------------------
DROP TABLE IF EXISTS `article_theme`;
CREATE TABLE `article_theme` (
  `id` int(8) NOT NULL AUTO_INCREMENT,
  `created` datetime DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `uuid` varchar(100) DEFAULT NULL,
  `install_id` int(8) DEFAULT NULL,
  `shop_id` int(8) DEFAULT NULL,
  `parent_id` int(8) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
--  Table structure for `article_theme_article`
-- ----------------------------
DROP TABLE IF EXISTS `article_theme_article`;
CREATE TABLE `article_theme_article` (
  `theme_id` int(8) DEFAULT NULL,
  `article_id` int(8) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
--  Table structure for `article_theme_market_article`
-- ----------------------------
DROP TABLE IF EXISTS `article_theme_market_article`;
CREATE TABLE `article_theme_market_article` (
  `theme_id` int(8) DEFAULT NULL,
  `article_id` int(8) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
--  Table structure for `base_article_releated`
-- ----------------------------
DROP TABLE IF EXISTS `base_article_releated`;
CREATE TABLE `base_article_releated` (
  `article1` bigint(20) NOT NULL DEFAULT '0',
  `article2` bigint(20) NOT NULL DEFAULT '0',
  PRIMARY KEY (`article1`,`article2`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
--  Table structure for `clipart`
-- ----------------------------
DROP TABLE IF EXISTS `clipart`;
CREATE TABLE `clipart` (
  `id` int(8) NOT NULL AUTO_INCREMENT,
  `created` datetime NOT NULL,
  `updated` datetime NOT NULL,
  `title` varchar(255) NOT NULL,
  `url` varchar(255) NOT NULL,
  `install_id` int(8) NOT NULL,
  `uuid` varchar(40) NOT NULL,
  `clip_preview` varchar(255) NOT NULL,
  `parent_id` int(8) NOT NULL,
  `clip_price` float(10,0) NOT NULL,
  `clip_colors` varchar(255) NOT NULL,
  `clip_slice` varchar(255) NOT NULL,
  `clip_file1` varchar(255) NOT NULL,
  `clip_file2` varchar(255) NOT NULL,
  `webbasedlayouter` int(1) NOT NULL,
  `editable` int(1) NOT NULL,
  `xml` varchar(255) NOT NULL,
  `plotter` int(1) NOT NULL,
  `preview` longblob NOT NULL,
  `options` longblob NOT NULL,
  `tags` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
--  Table structure for `clipart_theme`
-- ----------------------------
DROP TABLE IF EXISTS `clipart_theme`;
CREATE TABLE `clipart_theme` (
  `id` int(8) NOT NULL AUTO_INCREMENT,
  `created` datetime NOT NULL,
  `updated` datetime NOT NULL,
  `install_id` int(8) NOT NULL,
  `uuid` varchar(40) NOT NULL,
  `title` varchar(255) NOT NULL,
  `parent_id` int(8) NOT NULL,
  `url` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
--  Table structure for `cms`
-- ----------------------------
DROP TABLE IF EXISTS `cms`;
CREATE TABLE `cms` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `install_id` bigint(20) DEFAULT NULL,
  `shop_id` bigint(20) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `pos` varchar(100) NOT NULL,
  `sor` varchar(10) NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `menu` varchar(255) NOT NULL,
  `url` varchar(255) NOT NULL,
  `language` varchar(5) DEFAULT NULL,
  `text1` longtext,
  `notinmenu` tinyint(1) NOT NULL,
  `modul` varchar(255) NOT NULL,
  `parent` int(8) NOT NULL,
  `parameter` text NOT NULL,
  `copy_market` int(1) NOT NULL,
  `meta_keywords` text NOT NULL,
  `meta_description` text NOT NULL,
  `meta_author` varchar(255) NOT NULL,
  `meta_custom_title` varchar(255) NOT NULL,
  `meta_og_title` varchar(255) NOT NULL,
  `meta_og_type` varchar(255) NOT NULL,
  `meta_og_url` varchar(255) NOT NULL,
  `meta_og_image` varchar(255) NOT NULL,
  `meta_og_description` text NOT NULL,
  `display_title` int(1) NOT NULL DEFAULT '1',
  `display_only_logged_in` int(1) NOT NULL DEFAULT '0',
  `template` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `install_id_idx` (`install_id`),
  KEY `shop_id_idx` (`shop_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AVG_ROW_LENGTH=2520 ROW_FORMAT=COMPACT;

-- ----------------------------
--  Table structure for `contact`
-- ----------------------------
DROP TABLE IF EXISTS `contact`;
CREATE TABLE `contact` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `install_id` bigint(20) DEFAULT NULL,
  `account_id` bigint(20) DEFAULT NULL,
  `created` date DEFAULT NULL,
  `updated` date DEFAULT NULL,
  `enable` tinyint(1) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `hash` varchar(255) DEFAULT NULL,
  `password` varchar(40) DEFAULT NULL,
  `language` varchar(5) DEFAULT NULL,
  `self_anrede` int(11) DEFAULT NULL,
  `self_firstname` varchar(255) DEFAULT NULL,
  `self_lastname` varchar(255) DEFAULT NULL,
  `self_function` varchar(255) DEFAULT NULL,
  `self_department` varchar(255) DEFAULT NULL,
  `self_street` varchar(255) DEFAULT NULL,
  `self_house_number` varchar(255) DEFAULT NULL,
  `self_destrict` varchar(5) DEFAULT NULL,
  `self_zip` varchar(255) DEFAULT NULL,
  `self_city` varchar(255) DEFAULT NULL,
  `self_state` varchar(255) DEFAULT NULL,
  `self_country` varchar(255) DEFAULT NULL,
  `self_email` varchar(255) DEFAULT NULL,
  `self_web` varchar(255) DEFAULT NULL,
  `self_foto` varchar(255) DEFAULT NULL,
  `self_phone` varchar(255) DEFAULT NULL,
  `self_phone_alternative_type` varchar(255) DEFAULT NULL,
  `self_phone_alternative` varchar(255) DEFAULT NULL,
  `self_phone_mobile` varchar(255) DEFAULT NULL,
  `self_newsletter` tinyint(1) DEFAULT NULL,
  `self_information` text,
  `anrede` int(11) DEFAULT NULL,
  `firstname` varchar(255) DEFAULT NULL,
  `lastname` varchar(255) DEFAULT NULL,
  `function` varchar(255) DEFAULT NULL,
  `department` varchar(255) DEFAULT NULL,
  `street` varchar(255) DEFAULT NULL,
  `house_number` varchar(255) DEFAULT NULL,
  `destrict` varchar(5) DEFAULT NULL,
  `zip` varchar(255) DEFAULT NULL,
  `city` varchar(255) DEFAULT NULL,
  `state` varchar(255) DEFAULT NULL,
  `country` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `web` varchar(255) DEFAULT NULL,
  `foto` varchar(255) DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `phone_alternative_type` int(11) DEFAULT NULL,
  `phone_alternative` varchar(255) DEFAULT NULL,
  `phone_mobile` varchar(255) DEFAULT NULL,
  `newsletter` tinyint(1) DEFAULT NULL,
  `information` text,
  `contact_correspondence_languages` int(11) DEFAULT NULL,
  `mwert` tinyint(1) DEFAULT '1',
  `liefer_firstname` varchar(255) NOT NULL,
  `liefer_lastname` varchar(255) NOT NULL,
  `liefer_street` varchar(255) NOT NULL,
  `liefer_house_number` varchar(255) NOT NULL,
  `liefer_zip` varchar(255) NOT NULL,
  `liefer_city` varchar(255) NOT NULL,
  `liefer_phone` varchar(255) NOT NULL,
  `vonwo` varchar(255) NOT NULL,
  `liefer` tinyint(1) NOT NULL,
  `passwordresetid` varchar(255) NOT NULL,
  `passwordresetdate` datetime NOT NULL,
  `logo1` varchar(255) DEFAULT NULL,
  `uuid` varchar(40) DEFAULT NULL,
  `article_count` int(8) NOT NULL DEFAULT '0',
  `motiv_count` int(8) NOT NULL DEFAULT '0',
  `locked_master` int(1) NOT NULL,
  `locked` int(1) NOT NULL,
  `shop_kto` varchar(100) NOT NULL,
  `shop_blz` varchar(100) NOT NULL,
  `shop_iban` varchar(100) NOT NULL,
  `shop_bic` varchar(100) NOT NULL,
  `shop_bank_name` varchar(255) NOT NULL,
  `self_durchwahl` varchar(255) NOT NULL,
  `self_abteilung` varchar(255) NOT NULL,
  `is_sek` int(1) NOT NULL,
  `self_phone_vorwahl` varchar(255) NOT NULL,
  `self_phone_durchwahl` varchar(255) NOT NULL,
  `self_mobile_vorwahl` varchar(255) NOT NULL,
  `self_mobile_durchwahl` varchar(255) NOT NULL,
  `self_fax_vorwahl` varchar(255) NOT NULL,
  `self_fax_phone` varchar(255) NOT NULL,
  `self_fax_durchwahl` varchar(255) NOT NULL,
  `self_phone_alternative_durchwahl` varchar(255) NOT NULL,
  `shop_name` varchar(255) NOT NULL,
  `bank_name` varchar(255) NOT NULL,
  `bank_kto` varchar(255) NOT NULL,
  `bank_blz` varchar(255) NOT NULL,
  `bank_iban` varchar(255) NOT NULL,
  `bank_bic` varchar(255) NOT NULL,
  `bank_bank_name` varchar(255) NOT NULL,
  `ustid` varchar(255) NOT NULL,
  `self_title` varchar(255) NOT NULL,
  `self_position` varchar(255) NOT NULL,
  `standart_delivery` int(8) NOT NULL,
  `standart_sender` int(8) NOT NULL,
  `standart_invoice` int(8) NOT NULL,
  `show_calc` int(1) NOT NULL DEFAULT '0',
  `test_calc` int(1) NOT NULL,
  `production_status` int(8) DEFAULT NULL,
  `collecting_orders` int(1) NOT NULL DEFAULT '0',
  `self_phone_lv` varchar(255) DEFAULT NULL,
  `self_mobile_lv` varchar(255) DEFAULT NULL,
  `self_fax_lv` varchar(255) DEFAULT NULL,
  `self_phone_alternative_lv` varchar(255) DEFAULT NULL,
  `self_birthday` varchar(255) DEFAULT NULL,
  `self_kostenstellung` varchar(255) DEFAULT NULL,
  `self_department2` varchar(255) DEFAULT NULL,
  `budget` int(8) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `install_id_idx` (`install_id`),
  KEY `account_id_idx` (`account_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 AVG_ROW_LENGTH=1170 ROW_FORMAT=COMPACT;

-- ----------------------------
--  Records of `contact`
-- ----------------------------
BEGIN;
INSERT INTO `contact` VALUES ('1', '1', '1', '2012-05-24', '2012-08-13', '1', 'admin@shop.de', null, 'shop2014', 'de_DE', null, null, null, null, null, null, null, null, null, null, null, null, 'admin@shop.de', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '1', '', '', '', '', '', '', '', '', '0', '', '0000-00-00 00:00:00', null, '0001-54bb32cc-4fbe5196-4f49-954c11fc', '1', '0', '0', '0', '', '', '', '', '', '', '', '0', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '13182', '13186', '13183', '0', '0', null, '0', null, null, null, null, null, null, null, null);
COMMIT;

-- ----------------------------
--  Table structure for `contact_account`
-- ----------------------------
DROP TABLE IF EXISTS `contact_account`;
CREATE TABLE `contact_account` (
  `contact_id` int(8) NOT NULL,
  `account_id` int(8) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
--  Table structure for `contact_address`
-- ----------------------------
DROP TABLE IF EXISTS `contact_address`;
CREATE TABLE `contact_address` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created` datetime NOT NULL,
  `updated` datetime NOT NULL,
  `uuid` varchar(40) NOT NULL,
  `display` int(1) NOT NULL,
  `install_id` int(8) NOT NULL,
  `contact_id` int(8) NOT NULL,
  `firstname` varchar(100) NOT NULL,
  `lastname` varchar(100) NOT NULL,
  `street` varchar(255) NOT NULL,
  `house_number` varchar(20) NOT NULL,
  `zip` varchar(10) NOT NULL,
  `city` varchar(255) NOT NULL,
  `phone` varchar(100) NOT NULL,
  `mobil_phone` varchar(100) NOT NULL,
  `email` varchar(255) NOT NULL,
  `type` int(1) NOT NULL,
  `company` varchar(100) NOT NULL,
  `anrede` varchar(100) NOT NULL,
  `country` varchar(100) NOT NULL,
  `fax` varchar(255) DEFAULT NULL,
  `company2` varchar(255) DEFAULT NULL,
  `kostenstellung` varchar(255) DEFAULT NULL,
  `abteilung` varchar(255) DEFAULT NULL,
  `position` varchar(255) DEFAULT NULL,
  `ustid` varchar(255) DEFAULT NULL,
  `zusatz1` varchar(255) DEFAULT NULL,
  `zusatz2` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
--  Table structure for `contact_article`
-- ----------------------------
DROP TABLE IF EXISTS `contact_article`;
CREATE TABLE `contact_article` (
  `contact_id` bigint(20) NOT NULL DEFAULT '0',
  `article_id` bigint(20) NOT NULL DEFAULT '0',
  PRIMARY KEY (`contact_id`,`article_id`),
  KEY `article_id` (`article_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AVG_ROW_LENGTH=8192 ROW_FORMAT=COMPACT;

-- ----------------------------
--  Table structure for `contact_fav_article`
-- ----------------------------
DROP TABLE IF EXISTS `contact_fav_article`;
CREATE TABLE `contact_fav_article` (
  `uuid` varchar(40) NOT NULL,
  `article_uuid` varchar(40) NOT NULL,
  `contact_uuid` int(8) NOT NULL,
  `fav_group_uuid` varchar(40) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
--  Table structure for `contact_motiv`
-- ----------------------------
DROP TABLE IF EXISTS `contact_motiv`;
CREATE TABLE `contact_motiv` (
  `contact_id` int(8) NOT NULL,
  `motiv_id` int(8) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
--  Table structure for `contact_newsletter`
-- ----------------------------
DROP TABLE IF EXISTS `contact_newsletter`;
CREATE TABLE `contact_newsletter` (
  `id` int(8) NOT NULL AUTO_INCREMENT,
  `install_id` int(8) NOT NULL,
  `shop_id` int(8) NOT NULL,
  `verifyed` int(1) NOT NULL,
  `email` varchar(255) NOT NULL,
  `uuid` varchar(40) NOT NULL,
  `created` datetime NOT NULL,
  `updated` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
--  Table structure for `contact_paymenttype`
-- ----------------------------
DROP TABLE IF EXISTS `contact_paymenttype`;
CREATE TABLE `contact_paymenttype` (
  `contact_id` bigint(20) NOT NULL DEFAULT '0',
  `paymenttype_id` bigint(20) NOT NULL DEFAULT '0',
  PRIMARY KEY (`contact_id`,`paymenttype_id`),
  KEY `paymenttype_id` (`paymenttype_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AVG_ROW_LENGTH=16384 ROW_FORMAT=COMPACT;

-- ----------------------------
--  Table structure for `contact_role`
-- ----------------------------
DROP TABLE IF EXISTS `contact_role`;
CREATE TABLE `contact_role` (
  `created` datetime DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `role_id` bigint(20) NOT NULL DEFAULT '0',
  `contact_id` bigint(20) NOT NULL DEFAULT '0',
  PRIMARY KEY (`role_id`,`contact_id`),
  KEY `contact_id` (`contact_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AVG_ROW_LENGTH=496 ROW_FORMAT=COMPACT;

-- ----------------------------
--  Records of `contact_role`
-- ----------------------------
BEGIN;
INSERT INTO `contact_role` VALUES (null, null, '1', '1'), (null, null, '3', '1'), (null, null, '4', '1'), (null, null, '5', '1'), (null, null, '6', '1'), (null, null, '7', '1');
COMMIT;

-- ----------------------------
--  Table structure for `credit_system`
-- ----------------------------
DROP TABLE IF EXISTS `credit_system`;
CREATE TABLE `credit_system` (
  `id` int(8) NOT NULL AUTO_INCREMENT,
  `shop_id` int(8) DEFAULT NULL,
  `install_id` int(8) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `count` int(8) DEFAULT NULL,
  `enable` int(1) DEFAULT NULL,
  `f` date DEFAULT NULL,
  `t` date DEFAULT NULL,
  `percent` int(1) DEFAULT NULL,
  `wert` float DEFAULT NULL,
  `more` int(1) NOT NULL,
  `product_id` int(8) NOT NULL,
  `pre_code` varchar(255) NOT NULL,
  `articlegroup_id` int(8) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
--  Table structure for `credit_system_detail`
-- ----------------------------
DROP TABLE IF EXISTS `credit_system_detail`;
CREATE TABLE `credit_system_detail` (
  `id` int(8) NOT NULL AUTO_INCREMENT,
  `install_id` int(8) DEFAULT NULL,
  `shop_id` int(8) DEFAULT NULL,
  `creditsystem_id` int(8) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `uuid` varchar(255) DEFAULT NULL,
  `used` int(1) DEFAULT NULL,
  `contact_id` int(8) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
--  Table structure for `domain`
-- ----------------------------
DROP TABLE IF EXISTS `domain`;
CREATE TABLE `domain` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `shop_id` bigint(20) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `redirect` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `shop_id_idx` (`shop_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
--  Records of `domain`
-- ----------------------------
BEGIN;
INSERT INTO `domain` VALUES ('1', '1', '2012-07-19 14:54:22', '2012-07-19 14:54:22', 'baseline.shopsrvdev.de', '');
COMMIT;

-- ----------------------------
--  Table structure for `frontend_translation`
-- ----------------------------
DROP TABLE IF EXISTS `frontend_translation`;
CREATE TABLE `frontend_translation` (
  `id` int(8) NOT NULL AUTO_INCREMENT,
  `message_id` varchar(255) NOT NULL,
  `locale` varchar(255) NOT NULL,
  `text` text NOT NULL,
  `shop_id` int(8) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
--  Table structure for `image`
-- ----------------------------
DROP TABLE IF EXISTS `image`;
CREATE TABLE `image` (
  `id` varchar(40) NOT NULL DEFAULT '',
  `path` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `imagetype` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AVG_ROW_LENGTH=4096 ROW_FORMAT=COMPACT;

-- ----------------------------
--  Table structure for `install`
-- ----------------------------
DROP TABLE IF EXISTS `install`;
CREATE TABLE `install` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `created` datetime DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `paperdb` longtext,
  `containerdb` longtext,
  `defaultmarket` int(11) NOT NULL,
  `marketdomain` varchar(255) NOT NULL,
  `mall_logo` varchar(255) NOT NULL,
  `order_number` int(8) NOT NULL,
  `order_number_date` int(1) NOT NULL,
  `agb` text NOT NULL,
  `admincontact` int(8) NOT NULL,
  `check_agb` text NOT NULL,
  `a9_pt` mediumtext NOT NULL,
  `designer_debug` int(1) NOT NULL,
  `user_passwordreset_mode` int(1) NOT NULL DEFAULT '0',
  `redirect_ssl` int(1) NOT NULL,
  `service_value` int(3) NOT NULL DEFAULT '30',
  `check_agb_motiv` text NOT NULL,
  `check_agb_article` text NOT NULL,
  `calc_template` longtext NOT NULL,
  `calc_template_test` longtext NOT NULL,
  `motiv_1` int(8) NOT NULL,
  `motiv_2` int(8) NOT NULL,
  `motiv_3` int(8) NOT NULL,
  `export_template_contact` varchar(255) NOT NULL,
  `export_template_product` varchar(255) NOT NULL,
  `use_opengraph` int(1) NOT NULL DEFAULT '0',
  `offline_custom1` longtext NOT NULL,
  `offline_custom2` longtext NOT NULL,
  `offline_custom3` longtext NOT NULL,
  `motiv_agb` text,
  `only_de` int(1) DEFAULT '0',
  `default_status_pos_status_change` int(3) DEFAULT '0',
  `production_status` varchar(255) DEFAULT NULL,
  `layouter_agb` text,
  `impress` mediumtext,
  `impress_themenshop` mediumtext,
  `layouter_version` varchar(20) DEFAULT NULL,
  `revocation` mediumtext,
  `vacation` mediumtext,
  `colordb` longblob,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
--  Records of `install`
-- ----------------------------
BEGIN;
INSERT INTO `install` VALUES ('1', null, null, 'dummyshop', null, '<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<container>\n <papiercontainer id=\"visitenkarten\">\n    <papier id=\"mg150\"/>\n    <papier id=\"mg300\" value=\"0.0192\" />\n  </papiercontainer>\n        <papiercontainer id=\"flyer\">\n    <papier id=\"mg300\"/>\n  </papiercontainer>\n</container>', '1', '.shopsrv.de', '', '2', '0', 'Meine AGB<br>', '1', 'Meine AGB', '', '0', '0', '0', '30', 'Meine AGB', 'Meine AGB', '', '', '0', '0', '0', '', '', '0', '', '', '', '', '0', '0', null, '', 'Mein Impressum<br>werwerwer<br><span style=\"font-weight: bold;\">werwerwer</span><br><br>', '', '', null, null, null);
COMMIT;

-- ----------------------------
--  Table structure for `layouter_session`
-- ----------------------------
DROP TABLE IF EXISTS `layouter_session`;
CREATE TABLE `layouter_session` (
  `uuid` varchar(50) NOT NULL,
  `xmlconfig` mediumtext NOT NULL,
  `xmlpages` mediumtext NOT NULL,
  `xmlpagetemplates` mediumtext NOT NULL,
  `xmlpageobjects` mediumtext NOT NULL,
  `xmlxslfo` mediumtext NOT NULL,
  `xmlpreviewxslfo` mediumtext NOT NULL,
  `xmlextendpreviewxslfo` mediumtext NOT NULL,
  `created` datetime NOT NULL,
  `updated` datetime NOT NULL,
  `contact_id` int(8) NOT NULL,
  `org_article_id` varchar(50) NOT NULL,
  `id` int(8) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `preview_path` varchar(255) NOT NULL,
  `designer_xml` longtext NOT NULL,
  `template_print_id` varchar(255) NOT NULL,
  `layouter_modus` int(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
--  Table structure for `layouterupload`
-- ----------------------------
DROP TABLE IF EXISTS `layouterupload`;
CREATE TABLE `layouterupload` (
  `id` int(8) NOT NULL AUTO_INCREMENT,
  `session_id` varchar(255) NOT NULL,
  `uploadticket_id` varchar(255) NOT NULL,
  `article_id` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AVG_ROW_LENGTH=496 PACK_KEYS=0 ROW_FORMAT=COMPACT;

-- ----------------------------
--  Table structure for `migration_version`
-- ----------------------------
DROP TABLE IF EXISTS `migration_version`;
CREATE TABLE `migration_version` (
  `version` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
--  Table structure for `motiv`
-- ----------------------------
DROP TABLE IF EXISTS `motiv`;
CREATE TABLE `motiv` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `uuid` varchar(40) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `install_id` bigint(20) DEFAULT NULL,
  `shop_id` bigint(20) DEFAULT NULL,
  `account_id` bigint(20) DEFAULT NULL,
  `contact_id` bigint(20) DEFAULT NULL,
  `file_orginal` varchar(255) DEFAULT NULL,
  `file_work` varchar(255) DEFAULT NULL,
  `file_thumb` varchar(255) DEFAULT NULL,
  `file_mid` varchar(255) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `info` text,
  `keywords` text,
  `width` bigint(20) DEFAULT NULL,
  `height` bigint(20) DEFAULT NULL,
  `copyright` varchar(255) DEFAULT NULL,
  `typ` varchar(3) DEFAULT NULL,
  `resale_shop` tinyint(1) DEFAULT '0',
  `mid_width` int(8) DEFAULT NULL,
  `mid_height` int(8) DEFAULT NULL,
  `deleted` int(1) NOT NULL,
  `price1` float NOT NULL,
  `dpi_org` varchar(10) NOT NULL,
  `dpi_con` varchar(10) NOT NULL,
  `visits` int(8) NOT NULL DEFAULT '0',
  `used` int(8) NOT NULL DEFAULT '0',
  `orgfilename` varchar(255) NOT NULL,
  `rate` int(8) NOT NULL,
  `rate_count` int(8) NOT NULL,
  `resale_market` tinyint(1) NOT NULL DEFAULT '0',
  `resale_download` tinyint(1) NOT NULL DEFAULT '0',
  `status` int(2) NOT NULL DEFAULT '10',
  `status_text` mediumtext NOT NULL,
  `download_price` float NOT NULL DEFAULT '0',
  `price2` float NOT NULL,
  `price3` float NOT NULL,
  `exif` longtext NOT NULL,
  `delete` int(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `sluggable_idx` (`url`),
  KEY `install_id_idx` (`install_id`),
  KEY `shop_id_idx` (`shop_id`),
  KEY `account_id_idx` (`account_id`),
  KEY `contact_id_idx` (`contact_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
--  Table structure for `motiv_counter`
-- ----------------------------
DROP TABLE IF EXISTS `motiv_counter`;
CREATE TABLE `motiv_counter` (
  `counter_id` int(11) NOT NULL,
  `max_doc_id` int(11) NOT NULL,
  PRIMARY KEY (`counter_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
--  Table structure for `motiv_taggable_tag`
-- ----------------------------
DROP TABLE IF EXISTS `motiv_taggable_tag`;
CREATE TABLE `motiv_taggable_tag` (
  `id` bigint(20) NOT NULL DEFAULT '0',
  `tag_id` bigint(20) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`,`tag_id`),
  KEY `motiv_taggable_tag_tag_id_taggable_tag_id` (`tag_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
--  Table structure for `motiv_theme`
-- ----------------------------
DROP TABLE IF EXISTS `motiv_theme`;
CREATE TABLE `motiv_theme` (
  `id` int(8) NOT NULL AUTO_INCREMENT,
  `created` datetime DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `uuid` varchar(100) DEFAULT NULL,
  `install_id` int(8) DEFAULT NULL,
  `shop_id` int(8) DEFAULT NULL,
  `parent_id` int(8) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
--  Table structure for `motiv_theme_market_motiv`
-- ----------------------------
DROP TABLE IF EXISTS `motiv_theme_market_motiv`;
CREATE TABLE `motiv_theme_market_motiv` (
  `theme_id` int(8) DEFAULT NULL,
  `motiv_id` int(8) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
--  Table structure for `motiv_theme_motiv`
-- ----------------------------
DROP TABLE IF EXISTS `motiv_theme_motiv`;
CREATE TABLE `motiv_theme_motiv` (
  `theme_id` int(8) DEFAULT NULL,
  `motiv_id` int(8) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
--  Table structure for `news`
-- ----------------------------
DROP TABLE IF EXISTS `news`;
CREATE TABLE `news` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `install_id` bigint(20) DEFAULT NULL,
  `shop_id` bigint(20) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `pos` varchar(100) DEFAULT NULL,
  `sor` varchar(10) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `menu` varchar(255) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `language` varchar(5) DEFAULT NULL,
  `text` text,
  `notinmenu` tinyint(1) DEFAULT NULL,
  `einleitung` text NOT NULL,
  `sort_date` date NOT NULL,
  `active` int(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `install_id_idx` (`install_id`),
  KEY `shop_id_idx` (`shop_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AVG_ROW_LENGTH=8192 ROW_FORMAT=COMPACT;

-- ----------------------------
--  Table structure for `newsletter`
-- ----------------------------
DROP TABLE IF EXISTS `newsletter`;
CREATE TABLE `newsletter` (
  `id` int(8) NOT NULL AUTO_INCREMENT,
  `created` datetime NOT NULL,
  `updated` datetime NOT NULL,
  `install_id` int(8) NOT NULL,
  `shop_id` int(8) NOT NULL,
  `title` varchar(255) NOT NULL,
  `subject` varchar(255) NOT NULL,
  `from_mail` varchar(255) NOT NULL,
  `from_name` varchar(255) NOT NULL,
  `text` mediumtext NOT NULL,
  `text_html` mediumtext NOT NULL,
  `uuid` varchar(40) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
--  Table structure for `newsletter_queue`
-- ----------------------------
DROP TABLE IF EXISTS `newsletter_queue`;
CREATE TABLE `newsletter_queue` (
  `id` int(8) NOT NULL AUTO_INCREMENT,
  `shop_id` int(8) NOT NULL,
  `install_id` int(8) NOT NULL,
  `newsletter_id` int(8) NOT NULL,
  `contact_id` int(8) NOT NULL,
  `status` int(1) NOT NULL,
  `send_count` int(8) NOT NULL,
  `created` datetime NOT NULL,
  `updated` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
--  Table structure for `orders`
-- ----------------------------
DROP TABLE IF EXISTS `orders`;
CREATE TABLE `orders` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `created` datetime DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `install_id` bigint(20) DEFAULT NULL,
  `account_id` bigint(20) DEFAULT NULL,
  `shop_id` bigint(20) DEFAULT NULL,
  `alias` varchar(255) DEFAULT NULL,
  `contact_id` bigint(20) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `paymenttype_id` bigint(20) DEFAULT NULL,
  `shippingtype_id` bigint(20) DEFAULT NULL,
  `preis` float NOT NULL,
  `preissteuer` float DEFAULT NULL,
  `preisbrutto` float DEFAULT NULL,
  `enable` tinyint(1) NOT NULL DEFAULT '1',
  `package` varchar(255) NOT NULL,
  `info` text NOT NULL,
  `basketfield1` text,
  `basketfield2` text,
  `gutschein` varchar(40) DEFAULT NULL,
  `gutscheinabzug` float DEFAULT NULL,
  `delivery_same` int(1) NOT NULL,
  `delivery_address` int(8) NOT NULL,
  `uuid` varchar(50) NOT NULL,
  `versandkosten` float NOT NULL,
  `zahlkosten` float NOT NULL,
  `gutscheinabzugtyp` int(1) NOT NULL,
  `use_account_as_invoice` int(1) NOT NULL,
  `mwertalle` mediumtext NOT NULL,
  `sender_same` int(1) NOT NULL,
  `invoice_same` int(1) NOT NULL,
  `sender_address` int(8) NOT NULL,
  `invoice_address` int(8) NOT NULL,
  `delivery_date` date NOT NULL,
  `version` int(5) NOT NULL DEFAULT '1',
  `shippingtype_extra_label` varchar(255) DEFAULT NULL,
  `file1` varchar(255) DEFAULT NULL,
  `lang` varchar(2) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `contact_id_idx` (`contact_id`),
  KEY `install_id_idx` (`install_id`),
  KEY `account_id_idx` (`account_id`),
  KEY `shop_id_idx` (`shop_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AVG_ROW_LENGTH=682 ROW_FORMAT=COMPACT;

-- ----------------------------
--  Table structure for `orderspos`
-- ----------------------------
DROP TABLE IF EXISTS `orderspos`;
CREATE TABLE `orderspos` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `createdd` date DEFAULT NULL,
  `updatedd` date DEFAULT NULL,
  `createdt` time DEFAULT NULL,
  `updatedt` time DEFAULT NULL,
  `install_id` bigint(20) DEFAULT NULL,
  `shop_id` bigint(20) DEFAULT NULL,
  `account_id` bigint(20) DEFAULT NULL,
  `orders_id` bigint(20) NOT NULL DEFAULT '0',
  `count` int(11) DEFAULT NULL,
  `data` longblob,
  `priceone` float DEFAULT NULL,
  `priceonesteuer` float DEFAULT NULL,
  `priceonebrutto` float DEFAULT NULL,
  `priceall` float DEFAULT NULL,
  `priceallsteuer` float DEFAULT NULL,
  `priceallbrutto` float DEFAULT NULL,
  `uploadfinish` tinyint(1) DEFAULT NULL,
  `article_id` int(11) DEFAULT NULL,
  `question` text,
  `resale_price` float NOT NULL,
  `typ` int(8) NOT NULL,
  `status` int(3) NOT NULL,
  `uuid` varchar(50) NOT NULL,
  `layouter_mode` int(1) NOT NULL DEFAULT '0',
  `render_print` int(1) NOT NULL DEFAULT '0',
  `xmlconfigfile` mediumtext NOT NULL,
  `xmlpagesfile` mediumtext NOT NULL,
  `xmlpagetemplatesfile` mediumtext NOT NULL,
  `xmlpageobjectsfile` mediumtext NOT NULL,
  `xmlxslfofile` mediumtext NOT NULL,
  `xmlpreviewxslfofile` mediumtext NOT NULL,
  `xmlextendpreviewxslfofile` mediumtext NOT NULL,
  `calc_xml` mediumtext NOT NULL,
  `a9_user_cost` float(8,0) NOT NULL,
  `a9_designer_data` mediumtext NOT NULL,
  `hasgutschein` int(1) NOT NULL,
  `weight` int(8) NOT NULL,
  `calc_values` longtext NOT NULL,
  `delivery_date` date NOT NULL,
  `maschine` int(8) NOT NULL,
  `papier` int(1) NOT NULL,
  `proddate` date NOT NULL,
  `version` int(5) NOT NULL DEFAULT '1',
  `pos` int(2) NOT NULL,
  `shipping_type` int(8) NOT NULL,
  `shipping_price` float NOT NULL,
  `shipping_price_mwert` float NOT NULL,
  `ref` varchar(255) NOT NULL,
  `kst` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`,`orders_id`),
  KEY `install_id_idx` (`install_id`),
  KEY `shop_id_idx` (`shop_id`),
  KEY `account_id_idx` (`account_id`),
  KEY `orders_id` (`orders_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AVG_ROW_LENGTH=5242 ROW_FORMAT=COMPACT;

-- ----------------------------
--  Table structure for `orderspos_confirm_contact`
-- ----------------------------
DROP TABLE IF EXISTS `orderspos_confirm_contact`;
CREATE TABLE `orderspos_confirm_contact` (
  `orderspos_id` int(8) NOT NULL,
  `contact_id` int(8) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
--  Table structure for `papierdb`
-- ----------------------------
DROP TABLE IF EXISTS `papierdb`;
CREATE TABLE `papierdb` (
  `id` int(8) NOT NULL AUTO_INCREMENT,
  `install_id` int(8) NOT NULL,
  `art_nr` varchar(25) NOT NULL,
  `auslauf` varchar(1) NOT NULL,
  `description_1` varchar(255) NOT NULL,
  `description_2` varchar(255) NOT NULL,
  `grammatur` varchar(6) NOT NULL,
  `staerke` varchar(60) NOT NULL,
  `breite` varchar(60) NOT NULL,
  `hoehe` varchar(60) NOT NULL,
  `laenge` varchar(60) NOT NULL,
  `rollenlaenge` varchar(60) NOT NULL,
  `gewicht` int(10) NOT NULL,
  `papierausruestung` varchar(60) NOT NULL,
  `farbnummer` varchar(60) NOT NULL,
  `farbbezeichnung` varchar(60) NOT NULL,
  `huelsendurchmesser` varchar(60) NOT NULL,
  `kleber` varchar(60) NOT NULL,
  `abdeckpapier` varchar(60) NOT NULL,
  `laufrichtung` varchar(2) NOT NULL,
  `mengenangabe` int(10) NOT NULL,
  `mengenangabe_palette` int(10) NOT NULL,
  `inhalt` varchar(60) NOT NULL,
  `etiketten_je_blatt` varchar(60) NOT NULL,
  `mengeneinheit` varchar(30) NOT NULL,
  `staffelmenge_1` int(10) NOT NULL,
  `staffelpreis_1` float(10,0) NOT NULL,
  `staffelmenge_2` int(10) NOT NULL,
  `staffelpreis_2` float(10,0) NOT NULL,
  `staffelmenge_3` int(10) NOT NULL,
  `staffelpreis_3` float(10,0) NOT NULL,
  `staffelmenge_4` int(10) NOT NULL,
  `staffelpreis_4` float(10,0) NOT NULL,
  `staffelmenge_5` int(10) NOT NULL,
  `staffelpreis_5` float(10,0) NOT NULL,
  `lagerort` varchar(7) NOT NULL,
  `verkaufshinweise` text NOT NULL,
  `abnahmeinfo` varchar(60) NOT NULL,
  `produkt_beschreibung` text NOT NULL,
  `produkt_eigenschaften` text NOT NULL,
  `produkt_vorteile` text NOT NULL,
  `produkt_nutzen` text NOT NULL,
  `produkt_anwendungen` text NOT NULL,
  `produkt_besonderheit` text NOT NULL,
  `musterbuch` varchar(40) NOT NULL,
  `zur_aufnahme_von` varchar(60) NOT NULL,
  `eigenschaften` text NOT NULL,
  `preis` float NOT NULL,
  `uuid` varchar(40) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
--  Table structure for `paymenttype`
-- ----------------------------
DROP TABLE IF EXISTS `paymenttype`;
CREATE TABLE `paymenttype` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `created` datetime DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `install_id` bigint(20) DEFAULT NULL,
  `shop_id` bigint(20) DEFAULT NULL,
  `enable` tinyint(1) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `prozent` tinyint(1) DEFAULT NULL,
  `wert` double DEFAULT NULL,
  `private` tinyint(1) DEFAULT NULL,
  `pos` int(8) NOT NULL,
  `description` text NOT NULL,
  `trustedshop_name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `install_id_idx` (`install_id`),
  KEY `shop_id_idx` (`shop_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 AVG_ROW_LENGTH=8192 ROW_FORMAT=COMPACT;

-- ----------------------------
--  Records of `paymenttype`
-- ----------------------------
BEGIN;
INSERT INTO `paymenttype` VALUES ('1', '2012-05-24 17:18:25', '2012-05-24 17:18:25', '1', '1', null, 'Vorkasse', '0', '10', '0', '0', 'Erst bezahlen dann Ware', null);
COMMIT;

-- ----------------------------
--  Table structure for `preflight`
-- ----------------------------
DROP TABLE IF EXISTS `preflight`;
CREATE TABLE `preflight` (
  `id` int(8) NOT NULL AUTO_INCREMENT,
  `install_id` int(8) NOT NULL,
  `created` datetime NOT NULL,
  `updated` datetime NOT NULL,
  `uuid` varchar(40) DEFAULT NULL,
  `nr` varchar(255) DEFAULT NULL,
  `description` mediumtext,
  `check_filetype_pdf` int(1) DEFAULT NULL,
  `check_filetype_image` int(1) DEFAULT NULL,
  `check_filetype_error_message` varchar(255) DEFAULT NULL,
  `check_site_count_rel` varchar(255) DEFAULT NULL,
  `check_site_count` int(8) DEFAULT NULL,
  `check_site_count_color` int(8) DEFAULT NULL,
  `check_site_count_bw` int(8) DEFAULT NULL,
  `check_site_count_error_message` varchar(255) DEFAULT NULL,
  `check_format_a1` int(1) DEFAULT NULL,
  `check_format_a2` int(1) DEFAULT NULL,
  `check_format_a3` int(1) DEFAULT NULL,
  `check_format_a4` int(1) DEFAULT NULL,
  `check_format_a5` int(1) DEFAULT NULL,
  `check_format_a6` int(1) DEFAULT NULL,
  `check_format_custom_width_from` int(8) DEFAULT NULL,
  `check_format_custom_width_to` int(8) DEFAULT NULL,
  `check_format_custom_height_from` int(8) DEFAULT NULL,
  `check_format_custom_height_to` int(8) DEFAULT NULL,
  `check_format_error_message` varchar(255) DEFAULT NULL,
  `check_format_box_media` int(1) DEFAULT NULL,
  `check_format_box_trim` int(1) DEFAULT NULL,
  `check_format_box_bleed` int(1) DEFAULT NULL,
  `check_format_box_crop` int(1) DEFAULT NULL,
  `check_format_box_art` int(1) DEFAULT NULL,
  `check_format_box_error_message` varchar(255) DEFAULT NULL,
  `check_format_a0` int(1) DEFAULT NULL,
  `check_format_a7` int(1) DEFAULT NULL,
  `check_format_a8` int(1) DEFAULT NULL,
  `check_format_a9` int(1) DEFAULT NULL,
  `check_format_a10` int(1) DEFAULT NULL,
  `check_format_rel` varchar(255) DEFAULT NULL,
  `check_format_orientation_rel` varchar(255) DEFAULT NULL,
  `check_format_orientation_quer` int(1) DEFAULT NULL,
  `check_format_orientation_hoch` int(1) DEFAULT NULL,
  `check_fonts_embedded` int(1) DEFAULT NULL,
  `check_fonts_embedded_error_message` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
--  Table structure for `printdate`
-- ----------------------------
DROP TABLE IF EXISTS `printdate`;
CREATE TABLE `printdate` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `install_id` bigint(20) DEFAULT NULL,
  `shop_id` bigint(20) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `startdate` date DEFAULT NULL,
  `data` text,
  PRIMARY KEY (`id`),
  KEY `install_id_idx` (`install_id`),
  KEY `shop_id_idx` (`shop_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AVG_ROW_LENGTH=8192 ROW_FORMAT=COMPACT;

-- ----------------------------
--  Table structure for `privilege`
-- ----------------------------
DROP TABLE IF EXISTS `privilege`;
CREATE TABLE `privilege` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `created` datetime DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
--  Records of `privilege`
-- ----------------------------
BEGIN;
INSERT INTO `privilege` VALUES ('1', null, null, 'index'), ('2', null, null, 'login'), ('3', null, null, 'tree'), ('4', null, null, 'overview'), ('5', null, null, 'settings'), ('6', null, null, 'register'), ('7', null, null, 'show'), ('8', null, null, 'buy'), ('9', null, null, 'finish'), ('10', null, null, 'review'), ('11', null, null, 'edit'), ('12', null, null, 'registeraccount'), ('13', null, null, 'all'), ('14', null, null, 'upload'), ('15', null, null, 'shops'), ('16', null, null, 'domains'), ('17', null, null, 'portal'), ('18', null, null, 'createthemeshop'), ('19', null, null, 'createshop'), ('20', null, null, 'mymotiv'), ('21', null, null, 'templates'), ('22', null, null, 'translation'), ('23', null, null, 'anfrage'), ('24', null, null, 'extjssettings');
COMMIT;

-- ----------------------------
--  Table structure for `queues`
-- ----------------------------
DROP TABLE IF EXISTS `queues`;
CREATE TABLE `queues` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `createdd` date DEFAULT NULL,
  `updatedd` date DEFAULT NULL,
  `createdt` time DEFAULT NULL,
  `updatedt` time DEFAULT NULL,
  `install_id` bigint(20) DEFAULT NULL,
  `shop_id` bigint(20) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `typ` varchar(255) DEFAULT NULL,
  `data` text,
  `enable` tinyint(1) DEFAULT NULL,
  `action` varchar(255) DEFAULT NULL,
  `pos` int(8) NOT NULL,
  `shop` int(8) NOT NULL DEFAULT '0',
  `xml` longtext NOT NULL,
  PRIMARY KEY (`id`),
  KEY `install_id_idx` (`install_id`),
  KEY `shop_id_idx` (`shop_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AVG_ROW_LENGTH=3276 ROW_FORMAT=COMPACT;

-- ----------------------------
--  Table structure for `resource`
-- ----------------------------
DROP TABLE IF EXISTS `resource`;
CREATE TABLE `resource` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `created` datetime DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `modul` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=63 DEFAULT CHARSET=utf8 AVG_ROW_LENGTH=606 ROW_FORMAT=COMPACT;

-- ----------------------------
--  Records of `resource`
-- ----------------------------
BEGIN;
INSERT INTO `resource` VALUES ('1', null, null, 'index', 'default'), ('2', null, null, 'index', 'admin'), ('3', null, null, 'login', 'admin'), ('4', null, null, 'app', 'admin'), ('5', null, null, 'user', 'admin'), ('6', null, null, 'account', 'admin'), ('7', null, null, 'shop', 'admin'), ('8', null, null, 'cms', 'admin'), ('9', null, null, 'index', 'service'), ('10', null, null, 'cms', 'default'), ('11', null, null, 'user', 'default'), ('12', null, null, 'article', 'admin'), ('13', null, null, 'article', 'default'), ('14', null, null, 'basket', 'default'), ('15', null, null, 'what', 'admin'), ('16', null, null, 'queue', 'admin'), ('17', null, null, 'orders', 'admin'), ('18', null, null, 'contact', 'admin'), ('19', null, null, 'myorders', 'admin'), ('20', null, null, 'articlegroup', 'admin'), ('21', null, null, 'upload', 'admin'), ('22', null, null, 'overview', 'default'), ('23', null, null, 'payment', 'admin'), ('24', null, null, 'shipping', 'admin'), ('25', null, null, 'webdav', 'service'), ('26', null, null, 'news', 'admin'), ('27', null, null, 'printdate', 'admin'), ('28', null, null, 'system', 'admin'), ('29', null, null, 'displayshop', 'admin'), ('30', null, null, 'error', 'default'), ('31', null, null, 'upload', 'service'), ('32', null, null, 'amf', 'service'), ('33', null, null, 'uploadcenter', 'default'), ('34', null, null, 'news', 'default'), ('35', null, null, 'layouter', 'default'), ('36', null, null, 'file', 'admin'), ('37', null, null, 'install', 'admin'), ('38', null, null, 'motiv', 'admin'), ('39', null, null, 'themes', 'admin'), ('40', null, null, 'newsletter', 'admin'), ('41', null, null, 'creditsystem', 'admin'), ('42', null, null, 'api', 'service'), ('43', null, null, 'themes', 'default'), ('44', null, null, 'soap', 'service'), ('45', null, null, 'motiv', 'default'), ('46', null, null, 'search', 'default'), ('47', null, null, 'assets', 'service'), ('48', null, null, 'resale', 'default'), ('49', null, null, 'market', 'default'), ('50', null, null, 'papierdb', 'admin'), ('51', null, null, 'designer', 'service'), ('52', null, null, 'designer', 'admin'), ('53', null, null, 'booking', 'admin'), ('54', null, null, 'preflight', 'admin'), ('55', null, null, 'stats', 'admin'), ('56', null, null, 'template', 'service'), ('57', null, null, 'preflight', 'default'), ('58', null, null, 'json', 'service'), ('59', null, null, 'index', 'production'), ('60', null, null, 'myshop', 'default'), ('61', null, null, 'marketoverview', 'default'), ('62', null, null, 'steplayouter', 'service');
COMMIT;

-- ----------------------------
--  Table structure for `resource_privilege`
-- ----------------------------
DROP TABLE IF EXISTS `resource_privilege`;
CREATE TABLE `resource_privilege` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `resource_id` bigint(20) DEFAULT NULL,
  `privilege_id` bigint(20) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `tree` tinyint(1) DEFAULT NULL,
  `treename` varchar(255) DEFAULT NULL,
  `treeicon` varchar(255) DEFAULT NULL,
  `treeparent` varchar(255) DEFAULT NULL,
  `sort` int(3) DEFAULT NULL,
  `market` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `resource_id_idx` (`resource_id`),
  KEY `privilege_id_idx` (`privilege_id`)
) ENGINE=InnoDB AUTO_INCREMENT=82 DEFAULT CHARSET=utf8 AVG_ROW_LENGTH=468 ROW_FORMAT=COMPACT;

-- ----------------------------
--  Records of `resource_privilege`
-- ----------------------------
BEGIN;
INSERT INTO `resource_privilege` VALUES ('1', '1', '13', null, null, null, null, null, null, null, null), ('2', '2', '1', null, null, null, null, null, null, null, null), ('3', '3', '2', null, null, null, null, null, null, null, null), ('4', '4', '1', null, null, null, null, null, null, null, null), ('5', '4', '3', null, null, null, null, null, null, null, null), ('6', '4', '4', null, null, null, null, null, null, null, null), ('7', '5', '5', null, null, '0', 'admin_tree_mysettings', 'mysettings.png', '10', null, null), ('8', '6', '13', null, null, '1', 'admin_tree_orders-customers', 'orders-customers.png', '11,12', '1', '1'), ('9', '7', '5', null, null, '1', 'admin_tree_shopsettings', 'shopsettings.png', '', '2', '1'), ('10', '8', '13', null, null, '1', 'admin_tree_cms', 'cms.png', '', '3', '1'), ('11', '9', '13', null, null, null, null, null, null, null, null), ('12', '10', '1', null, null, null, null, null, null, null, null), ('13', '12', '13', null, null, '1', 'admin_tree_products', 'products.png', '', null, '1'), ('14', '13', '13', null, null, null, null, null, null, null, null), ('15', '13', '7', null, null, null, null, null, null, null, null), ('16', '13', '8', null, null, null, null, null, null, null, null), ('17', '14', '13', null, null, null, null, null, null, null, null), ('18', '14', '9', null, null, null, null, null, null, null, null), ('19', '14', '10', null, null, null, null, null, null, null, null), ('20', '12', '11', null, null, null, null, null, null, null, null), ('21', '15', '11', null, null, null, null, null, null, null, null), ('22', '11', '13', null, null, null, null, null, null, null, null), ('23', '16', '13', null, null, '1', 'admin_tree_queues', 'queues.png', '', null, null), ('24', '17', '13', null, null, null, null, null, null, null, null), ('25', '18', '13', null, null, null, null, null, null, null, null), ('26', '55', '13', null, null, '1', 'admin_tree_stat', 'myorders.png', '10', null, null), ('27', '13', '14', null, null, null, null, null, null, null, null), ('28', '20', '13', null, null, '1', 'admin_tree_productgroups', 'productgroups.png', '', null, '1'), ('29', '21', '13', null, null, null, null, null, null, null, null), ('30', '22', '1', null, null, null, null, null, null, null, null), ('31', '23', '13', null, null, '1', 'admin_tree_paymentmethods', 'paymentmethods.png', '', '4', null), ('32', '24', '13', null, null, '1', 'admin_tree_shippingmethods', 'shippingmethods.png', '', '5', null), ('33', '25', '13', null, null, null, null, null, null, null, null), ('34', '26', '13', null, null, '1', 'News', 'news.png', '', null, '1'), ('35', '27', '13', null, null, '0', 'Printdate', 'printdate.png', '', null, null), ('36', '28', '15', null, null, '1', 'admin_tree_shops', 'shopverwaltung1.png', '20', null, null), ('37', '28', '16', null, null, null, null, null, null, null, null), ('38', '29', '13', null, null, '1', 'admin_tree_displayshop', 'kuickshow.png', '', null, '1'), ('39', '30', '13', null, null, null, null, null, null, null, null), ('40', '31', '13', null, null, null, null, null, null, null, null), ('41', '32', '13', null, null, null, null, null, null, null, null), ('42', '33', '13', null, null, null, null, null, null, null, null), ('43', '34', '13', null, null, null, null, null, null, null, null), ('44', '35', '13', null, null, null, null, null, null, null, null), ('45', '4', '17', null, null, null, null, null, null, null, null), ('46', '36', '13', null, null, '1', 'admin_tree_filemanager', 'file.png', '', null, null), ('47', '37', '5', null, null, '1', 'admin_tree_system', 'shopsettings.png', '12', null, null), ('48', '38', '13', null, null, '1', 'admin_tree_motiv', 'contactgalerieimage2.png', '', null, '1'), ('49', '39', '13', null, null, '1', 'admin_tree_themes', 'operatorpaperdb.png', '', null, '1'), ('50', '40', '13', null, null, '1', 'admin_tree_newsletter', 'operatornewsletter.png', '', null, '1'), ('51', '41', '13', null, null, '1', 'admin_tree_creditsystem', 'shopvorlagen1.png', '', null, '0'), ('52', '38', '20', null, null, '0', 'admin_tree_mymotiv', 'contactgalerieimage2.png', '10', null, '1'), ('53', '7', '18', null, null, null, null, null, null, null, null), ('54', '7', '19', null, null, null, null, null, null, null, null), ('55', '42', '13', null, null, null, null, null, null, null, null), ('56', '43', '13', null, null, null, null, null, null, null, null), ('57', '44', '13', null, null, null, null, null, null, null, null), ('58', '45', '13', null, null, null, null, null, null, null, null), ('59', '46', '13', null, null, null, null, null, null, null, null), ('60', '47', '13', null, null, null, null, null, null, null, null), ('61', '48', '13', null, null, null, null, null, null, null, null), ('62', '49', '13', null, null, null, null, null, null, null, null), ('63', '49', '13', null, null, null, null, null, null, null, null), ('64', '12', '21', null, null, '1', 'admin_tree_product_templates', 'producttemplate.png', '20', '1', null), ('65', '12', '13', null, null, null, null, null, null, null, null), ('66', '28', '22', null, null, '1', 'admin_tree_translation', 'keyboard_layout.png', '30', null, null), ('67', '28', '13', null, null, null, null, null, null, null, null), ('68', '50', '13', null, null, '1', 'admin_tree_paperdb', 'paperdb.png', '12', null, null), ('69', '14', '23', null, null, null, null, null, null, null, null), ('70', '5', '24', null, null, null, null, null, null, null, null), ('71', '51', '13', null, null, null, null, null, null, null, null), ('72', '52', '13', null, null, '1', 'admin_tree_cliparts', 'krita.png', '12', null, null), ('73', '53', '13', null, null, '1', 'admin_tree_booking', 'booking.png', '20', null, null), ('74', '54', '13', null, null, '1', 'admin_tree_preflight', 'preflight.png', '20', null, null), ('75', '56', '13', null, null, null, null, null, null, null, null), ('76', '57', '13', null, null, null, null, null, null, null, null), ('77', '58', '13', null, null, null, null, null, null, null, null), ('78', '59', '13', null, null, null, null, null, null, null, null), ('79', '60', '13', null, null, null, null, null, null, null, null), ('80', '61', '13', null, null, null, null, null, null, null, null), ('81', '62', '13', null, null, null, null, null, null, null, null);
COMMIT;

-- ----------------------------
--  Table structure for `resourceprivilegeshop`
-- ----------------------------
DROP TABLE IF EXISTS `resourceprivilegeshop`;
CREATE TABLE `resourceprivilegeshop` (
  `shop_id` int(8) NOT NULL,
  `resourceprivilege_id` int(8) NOT NULL,
  `id` int(8) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
--  Records of `resourceprivilegeshop`
-- ----------------------------
BEGIN;
INSERT INTO `resourceprivilegeshop` VALUES ('1', '8', '1'), ('1', '9', '2'), ('1', '10', '3'), ('1', '13', '4'), ('1', '23', '5'), ('1', '28', '6'), ('1', '32', '7'), ('1', '33', '8'), ('1', '35', '9'), ('1', '38', '10'), ('1', '46', '11'), ('1', '48', '12'), ('1', '49', '13'), ('1', '50', '14'), ('1', '51', '15'), ('1', '31', '16'), ('1', '34', '17');
COMMIT;

-- ----------------------------
--  Table structure for `role`
-- ----------------------------
DROP TABLE IF EXISTS `role`;
CREATE TABLE `role` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `created` datetime DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `level` mediumint(9) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
--  Records of `role`
-- ----------------------------
BEGIN;
INSERT INTO `role` VALUES ('1', null, null, 'guest', '0'), ('2', null, null, 'Admin', '50'), ('3', null, null, 'System Operator', '40'), ('4', null, null, 'Shop Operator', '30'), ('5', null, null, 'Contact', '10'), ('6', null, null, 'Account', '20'), ('7', null, null, 'Production', '35');
COMMIT;

-- ----------------------------
--  Table structure for `role_privilege`
-- ----------------------------
DROP TABLE IF EXISTS `role_privilege`;
CREATE TABLE `role_privilege` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `role_id` bigint(20) DEFAULT NULL,
  `resource_privilege_id` bigint(20) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `role_id_idx` (`role_id`),
  KEY `resource_privilege_id_idx` (`resource_privilege_id`)
) ENGINE=InnoDB AUTO_INCREMENT=88 DEFAULT CHARSET=utf8 AVG_ROW_LENGTH=455 ROW_FORMAT=COMPACT;

-- ----------------------------
--  Records of `role_privilege`
-- ----------------------------
BEGIN;
INSERT INTO `role_privilege` VALUES ('1', '1', '1', null, null), ('2', '1', '2', null, null), ('3', '5', '4', null, null), ('4', '5', '5', null, null), ('5', '5', '6', null, null), ('6', '5', '7', null, null), ('7', '3', '8', null, null), ('8', '3', '9', null, null), ('9', '4', '9', null, null), ('10', '3', '10', null, null), ('11', '4', '10', null, null), ('12', '1', '11', null, null), ('13', '1', '12', null, null), ('14', '4', '13', null, null), ('15', '1', '14', null, null), ('16', '1', '15', null, null), ('17', '1', '16', null, null), ('18', '1', '17', null, null), ('19', '1', '18', null, null), ('20', '1', '19', null, null), ('21', '4', '20', null, null), ('22', '4', '21', null, null), ('23', '1', '22', null, null), ('24', '4', '23', null, null), ('25', '4', '24', null, null), ('26', '4', '25', null, null), ('27', '4', '26', null, null), ('28', '1', '27', null, null), ('29', '5', '29', null, null), ('30', '4', '28', null, null), ('31', '1', '30', null, null), ('32', '4', '31', null, null), ('33', '4', '32', null, null), ('34', '1', '33', null, null), ('35', '4', '34', null, null), ('36', '4', '35', null, null), ('37', '3', '36', null, null), ('38', '3', '37', null, null), ('39', '4', '38', null, null), ('40', '1', '39', null, null), ('41', '1', '40', null, null), ('42', '1', '41', null, null), ('43', '5', '42', null, null), ('44', '1', '43', null, null), ('45', '1', '44', null, null), ('46', '4', '45', null, null), ('47', '4', '46', null, null), ('48', '4', '47', null, null), ('49', '4', '48', null, null), ('50', '4', '49', null, null), ('51', '3', '50', null, null), ('52', '4', '51', null, null), ('53', '5', '53', null, null), ('54', '4', '54', null, null), ('55', '4', '52', null, null), ('56', '1', '55', null, null), ('57', '1', '56', null, null), ('58', '1', '57', null, null), ('59', '1', '58', null, null), ('60', '1', '59', null, null), ('61', '1', '60', null, null), ('62', '1', '61', null, null), ('63', '4', '8', null, null), ('64', '1', '63', null, null), ('65', '3', '64', null, null), ('66', '2', '66', null, null), ('67', '3', '67', null, null), ('68', '4', '68', null, null), ('69', '1', '69', null, null), ('70', '5', '70', null, null), ('71', '1', '71', null, null), ('72', '3', '72', null, null), ('73', '3', '73', null, null), ('74', '3', '74', null, null), ('75', '1', '75', null, null), ('76', '1', '76', null, null), ('77', '1', '77', null, null), ('78', '7', '8', null, null), ('79', '7', '45', null, null), ('81', '7', '25', null, null), ('82', '7', '24', null, null), ('83', '7', '11', null, null), ('84', '1', '78', null, null), ('85', '1', '79', null, null), ('86', '1', '80', null, null), ('87', '1', '81', null, null);
COMMIT;

-- ----------------------------
--  Table structure for `service_booking`
-- ----------------------------
DROP TABLE IF EXISTS `service_booking`;
CREATE TABLE `service_booking` (
  `id` int(8) NOT NULL AUTO_INCREMENT,
  `install_id` int(8) NOT NULL,
  `shop_id` int(8) NOT NULL,
  `shop_name` varchar(255) NOT NULL,
  `product_count` int(8) NOT NULL,
  `product_id` int(8) NOT NULL,
  `product_name` varchar(255) NOT NULL,
  `booking_typ` int(8) NOT NULL,
  `booking_text` varchar(255) NOT NULL,
  `order_id` int(8) NOT NULL,
  `orderpos_id` int(8) NOT NULL,
  `orderpos_netto` float NOT NULL,
  `orderpos_mwert` float NOT NULL,
  `orderpos_brutto` float NOT NULL,
  `payment_value` float NOT NULL,
  `service_value` int(3) NOT NULL,
  `uuid` varchar(40) NOT NULL,
  `created` datetime NOT NULL,
  `updated` datetime NOT NULL,
  `product_resale` float NOT NULL,
  `product_contact_creator` int(8) NOT NULL,
  `product_contact_buyer` int(8) NOT NULL,
  `type` int(8) NOT NULL,
  `product_contact_creator_name` varchar(255) NOT NULL,
  `product_contact_creator_street` varchar(255) NOT NULL,
  `product_contact_creator_city` varchar(255) NOT NULL,
  `product_contact_creator_mail` varchar(255) NOT NULL,
  `product_contact_creator_tel` varchar(255) NOT NULL,
  `product_contact_buyer_name` varchar(255) NOT NULL,
  `product_contact_buyer_street` varchar(255) NOT NULL,
  `product_contact_buyer_city` varchar(255) NOT NULL,
  `product_contact_buyer_mail` varchar(255) NOT NULL,
  `product_contact_buyer_tel` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
--  Table structure for `shippingtype`
-- ----------------------------
DROP TABLE IF EXISTS `shippingtype`;
CREATE TABLE `shippingtype` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `created` datetime DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `install_id` bigint(20) DEFAULT NULL,
  `shop_id` bigint(20) DEFAULT NULL,
  `enable` tinyint(1) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `art` varchar(255) DEFAULT NULL,
  `land` varchar(255) DEFAULT NULL,
  `mwert` bigint(20) DEFAULT NULL,
  `kosten_a` varchar(255) DEFAULT NULL,
  `kosten_b` varchar(255) DEFAULT NULL,
  `kosten_c` varchar(255) DEFAULT NULL,
  `kosten_d` varchar(255) DEFAULT NULL,
  `kosten_e` varchar(255) DEFAULT NULL,
  `kosten_f` varchar(255) DEFAULT NULL,
  `kosten_fix` varchar(255) DEFAULT NULL,
  `pos` int(8) NOT NULL,
  `no_payment` int(1) NOT NULL,
  `description` text NOT NULL,
  `private` int(1) NOT NULL,
  `weight_from` int(11) NOT NULL,
  `weight_to` int(11) NOT NULL,
  `xml` longtext NOT NULL,
  `kosten_percent` float DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `install_id_idx` (`install_id`),
  KEY `shop_id_idx` (`shop_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 AVG_ROW_LENGTH=5461 ROW_FORMAT=COMPACT;

-- ----------------------------
--  Records of `shippingtype`
-- ----------------------------
BEGIN;
INSERT INTO `shippingtype` VALUES ('1', '2012-05-24 17:17:01', '2012-05-24 17:17:01', '1', '1', null, 'Abholung vor Ort', 'Abholung vor Ort', null, '19', null, null, null, null, null, null, '10', '100', '0', 'Holen Sie die Ware vor Ort ab!', '0', '0', '0', '', '0'), ('2', '2012-06-27 12:21:44', '2012-06-27 12:24:03', '1', '1', null, 'Normalversand', 'Normalversand per Paketdienst', null, '19', null, null, null, null, null, null, '3.90', '90', '0', 'Auf den sicheren Versand legen wir besonders groÃŸen Wert. Daher versenden wir unsere Pakete innerhalb Deutschlands werden Ã¼blicherweise mit DPD verschickt. Die Zustellung der Ware erfolgt durch den Dienstleister in der Regel binnen 1 - 3 Werktagen. DPD liefert montags bis freitag in der Zeit von 08.00 Uhr bis 18.00 Uhr aus. Samstagszustellungen sind gegen Aufpreis mÃ¶glich.', '0', '0', '0', '', '0'), ('3', '2012-06-27 12:23:23', '2012-06-27 12:24:14', '1', '1', null, 'Expressversand ', 'Expressversand innerhalb Deutschlands', null, '19', null, null, null, null, null, null, '19.00', '110', '0', 'Expressversand innerhalb Deutschlands: Besonders eilige Lieferungen versenden wir auf Wunsch per Express Versandkosten. Sie erhalten die Ware am nÃ¤chsten Tag (Mo - Fr) vor 12:00 Uhr zugestellt. Versand ins Ausland auf Anfrage. ', '0', '0', '0', '', '0');
COMMIT;

-- ----------------------------
--  Table structure for `shop`
-- ----------------------------
DROP TABLE IF EXISTS `shop`;
CREATE TABLE `shop` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `install_id` bigint(20) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `layout` varchar(255) DEFAULT NULL,
  `uid` varchar(255) DEFAULT NULL,
  `defaultfunc` varchar(255) DEFAULT NULL,
  `defaultparam` varchar(255) DEFAULT NULL,
  `mwert` varchar(2) DEFAULT NULL,
  `logo1` varchar(255) DEFAULT NULL,
  `logo2` varchar(255) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `subtitle` varchar(500) DEFAULT NULL,
  `parameter` text,
  `formel` text,
  `land` bigint(20) DEFAULT NULL,
  `googleanalyticscode` varchar(50) NOT NULL,
  `private` tinyint(1) NOT NULL DEFAULT '1',
  `pdname` varchar(255) DEFAULT NULL,
  `pdoptions` text,
  `pdremoteoptions` text,
  `pdkeycode` varchar(255) DEFAULT NULL,
  `pdurl1` varchar(255) DEFAULT NULL,
  `pdurl2` varchar(255) DEFAULT NULL,
  `pdurl3` varchar(255) DEFAULT NULL,
  `pdurl4` varchar(255) DEFAULT NULL,
  `pdurl5` varchar(255) DEFAULT NULL,
  `pdcompanydestrict` varchar(255) DEFAULT NULL,
  `pdcompanyname` varchar(255) DEFAULT NULL,
  `pdcompanystreet` varchar(255) DEFAULT NULL,
  `pdcompanycity` varchar(255) DEFAULT NULL,
  `pdcompanytel` varchar(255) DEFAULT NULL,
  `pdcompanyfax` varchar(255) DEFAULT NULL,
  `pdcompanyemail` varchar(255) DEFAULT NULL,
  `pdcompanyweb` varchar(255) DEFAULT NULL,
  `pdcompanyid` varchar(255) DEFAULT NULL,
  `smtpfromname` varchar(255) NOT NULL,
  `smtpfrommail` varchar(255) NOT NULL,
  `smtphostname` varchar(255) NOT NULL,
  `smtpusername` varchar(255) NOT NULL,
  `smtppassword` varchar(255) NOT NULL,
  `smtpusethis` int(1) NOT NULL,
  `useemailaslogin` int(1) NOT NULL,
  `noverify` int(1) NOT NULL,
  `keywords` longtext,
  `description` varchar(1000) DEFAULT NULL,
  `author` varchar(255) DEFAULT NULL,
  `copyright` varchar(255) DEFAULT NULL,
  `basketfield1` varchar(255) DEFAULT NULL,
  `basketfield2` varchar(255) DEFAULT NULL,
  `css` text,
  `market` tinyint(1) DEFAULT NULL,
  `markettemplate` tinyint(1) DEFAULT NULL,
  `reportsite1` varchar(255) DEFAULT NULL,
  `reportsite2` varchar(255) DEFAULT NULL,
  `customtemplates` int(1) DEFAULT '0',
  `templatecopyright` text,
  `uploadfrontend` int(1) DEFAULT '1',
  `uselanguage` int(1) DEFAULT '0',
  `default_account` int(8) NOT NULL,
  `template_navi_display_all` int(1) NOT NULL DEFAULT '1',
  `template_display_products` int(1) NOT NULL,
  `template_display_motive` int(1) NOT NULL,
  `template_display_themenshops` int(1) NOT NULL,
  `template_display_products_all` int(1) NOT NULL,
  `template_display_products_product_templates` int(1) NOT NULL,
  `template_display_products_products` int(1) NOT NULL,
  `template_display_products_templates` int(1) NOT NULL,
  `template_display_products_own_products` int(1) NOT NULL,
  `template_display_products_my_products` int(1) NOT NULL,
  `template_display_products_custom_layouter` int(1) NOT NULL,
  `template_display_products_calc` int(1) NOT NULL,
  `template_display_products_depot` int(1) NOT NULL,
  `template_display_motive_all` int(1) NOT NULL,
  `template_display_motive_fav` int(1) NOT NULL,
  `template_display_motive_my` int(1) NOT NULL,
  `browsericon` varchar(255) NOT NULL,
  `template_display_products_navi` int(1) NOT NULL,
  `template_display_theme_navi` int(1) NOT NULL,
  `template_display_products_crossselling` int(1) NOT NULL,
  `template_display_help` int(1) NOT NULL,
  `template_display_help_cms` int(8) NOT NULL,
  `template_display_lesezeichen` int(1) NOT NULL,
  `template_display_news` int(1) NOT NULL,
  `template_display_newsletter` int(1) NOT NULL,
  `pmb` int(1) NOT NULL DEFAULT '0',
  `views` int(8) NOT NULL,
  `preview_logo` varchar(255) NOT NULL,
  `template_display_uploadcenter` int(8) NOT NULL,
  `template_display_orders` int(8) NOT NULL,
  `registration` int(1) NOT NULL DEFAULT '1',
  `slider_cms` int(1) NOT NULL,
  `slider_a_1` int(8) NOT NULL,
  `slider_a_2` int(8) NOT NULL,
  `slider_a_3` int(8) NOT NULL,
  `slider_a_4` int(8) NOT NULL,
  `slider_a_5` int(8) NOT NULL,
  `slider_cms_1` int(8) NOT NULL,
  `slider_cms_2` int(8) NOT NULL,
  `slider_cms_3` int(8) NOT NULL,
  `slider_cms_4` int(8) NOT NULL,
  `slider_cms_5` int(8) NOT NULL,
  `slider_logo` varchar(50) NOT NULL,
  `template_display_lead_box` int(8) NOT NULL,
  `rate` int(8) NOT NULL,
  `rate_count` int(8) NOT NULL,
  `template_display_rate` int(1) NOT NULL DEFAULT '1',
  `template_display_violation` int(1) NOT NULL DEFAULT '1',
  `layouter_presets` text NOT NULL,
  `ftp_username` varchar(255) NOT NULL,
  `ftp_password` varchar(255) NOT NULL,
  `ftp_uid` varchar(255) NOT NULL,
  `ftp_gid` varchar(255) NOT NULL,
  `ftp_dir` varchar(255) NOT NULL,
  `ftp_quotafiles` varchar(255) NOT NULL,
  `ftp_quotasize` varchar(255) NOT NULL,
  `ftp_ulbandwidth` varchar(255) NOT NULL,
  `ftp_dlbandwidth` varchar(255) NOT NULL,
  `betreiber_company` varchar(100) NOT NULL,
  `betreiber_name` varchar(100) NOT NULL,
  `betreiber_street` varchar(100) NOT NULL,
  `betreiber_address` varchar(100) NOT NULL,
  `display_top_modul` int(1) NOT NULL,
  `top_modul_settings` text NOT NULL,
  `display_resale_modul` int(1) NOT NULL,
  `betreiber_email` varchar(50) NOT NULL,
  `template_lead_product_1` int(8) NOT NULL,
  `template_lead_product_2` int(8) NOT NULL,
  `template_lead_product_3` int(8) NOT NULL,
  `template_lead_product_4` int(8) NOT NULL,
  `display_lead_box_modul` int(1) NOT NULL,
  `template_lead_product_5` int(8) NOT NULL,
  `display_market` int(1) NOT NULL DEFAULT '1',
  `creator_id` int(8) NOT NULL,
  `web_title` varchar(250) NOT NULL,
  `default_motiv_status` int(3) NOT NULL DEFAULT '30',
  `display_motiv_count` int(2) NOT NULL DEFAULT '32',
  `display_market_count` int(2) NOT NULL DEFAULT '32',
  `display_article_count` int(2) NOT NULL DEFAULT '32',
  `service_value` int(3) NOT NULL DEFAULT '30',
  `private_product` int(1) NOT NULL,
  `sitemap` varchar(40) NOT NULL,
  `report_agb` varchar(40) NOT NULL,
  `test_parameter` text NOT NULL,
  `test_formel` text NOT NULL,
  `report_background_invoice` int(1) NOT NULL DEFAULT '1',
  `report_background_delivery` int(1) NOT NULL DEFAULT '1',
  `report_background_label` int(1) NOT NULL,
  `report_background_job` int(1) NOT NULL,
  `report_background_offer` int(1) NOT NULL DEFAULT '1',
  `report_background_order` int(1) NOT NULL DEFAULT '1',
  `redirect_login` varchar(255) NOT NULL,
  `template_display_products_customer_related` int(1) NOT NULL,
  `template_display_products_related_carusel` int(1) NOT NULL,
  `template_display_user_approval` int(1) NOT NULL,
  `robots` varchar(40) NOT NULL,
  `custom_agb` longtext NOT NULL,
  `report_background_offer_blank` int(1) NOT NULL DEFAULT '1',
  `report_background_offer_contact` int(1) NOT NULL DEFAULT '1',
  `shipping_mode` int(1) NOT NULL DEFAULT '1',
  `product_sort` varchar(8) NOT NULL,
  `product_sort_dir` varchar(3) NOT NULL,
  `xmlvideositemap` varchar(40) NOT NULL,
  `betreiber_tel` varchar(255) DEFAULT NULL,
  `betreiber_uid` varchar(255) DEFAULT NULL,
  `betreiber_hid` varchar(255) DEFAULT NULL,
  `betreiber_sid` varchar(255) DEFAULT NULL,
  `betreiber_register` varchar(255) DEFAULT NULL,
  `betreiber_vb` varchar(255) DEFAULT NULL,
  `layout_settings` longtext,
  `betreiber_description` longtext,
  `custom_impress` longtext,
  `betreiber_rechtsform` varchar(255) DEFAULT NULL,
  `betreiber_web` varchar(255) DEFAULT NULL,
  `display_sender` int(1) DEFAULT '1',
  `display_delivery` int(1) DEFAULT '1',
  `basket_offer_number` int(8) DEFAULT '0',
  `deleted` int(1) DEFAULT '0',
  `layouter_version` varchar(20) DEFAULT NULL,
  `basketposfield1` varchar(255) NOT NULL DEFAULT '',
  `basketposfield2` varchar(255) DEFAULT NULL,
  `redirect_to_uploadcenter_after_basket_finish` int(1) NOT NULL DEFAULT '0',
  `template_switch` int(1) NOT NULL DEFAULT '0',
  `apikey` varchar(40) DEFAULT NULL,
  `fullbackground` varchar(40) DEFAULT NULL,
  `redirect_logout` varchar(255) DEFAULT NULL,
  `redirect_register` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `install_id_idx` (`install_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
--  Records of `shop`
-- ----------------------------
BEGIN;
INSERT INTO `shop` VALUES ('1', '1', '2012-05-21 22:14:27', '2014-03-20 12:01:24', 'testsystem', 'bootstrap', '1', 'Index', 'all', null, '', '', null, 'Testsystem', '', '', '1', '', '0', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '', '', '', '', '', '0', '1', '1', 'TEST SYSTEM', 'test', 'PSC', 'PSC', '', '', '', '0', null, '', '', '0', '', '1', '0', '204', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '0', '336', '', '0', '0', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '', '0', '0', '0', '1', '1', '', 'inmemorium', '', '', '', '', '', '', '', '', 'PSC Gmbh', 'PSC Gmbh', 'Musterstraße 14', 'Teststadt', '0', '', '0', '', '0', '0', '0', '0', '0', '0', '1', '0', 'Inital DB', '30', '32', '32', '32', '30', '0', '', '', '', '', '1', '1', '0', '0', '1', '1', '', '0', '0', '0', '', 'asd', '1', '1', '1', '', '', '', '12345', '12345', '12345', '12345', 'HRB', 'Inital Commit', '{\"bootstrap\":{\"custom_css\":null,\"preset\":\"7\",\"display_motive\":\"0\",\"display_mainnav_top\":\"0\",\"mainnav_top_stretch\":\"1\",\"motiv_modal_big\":\"0\",\"motiv_copyright\":\"0\",\"custom_css_delete\":\"0\",\"slider_interval\":\"6000\",\"mc_start_products\":[\"5730\",\"5438\",\"5602\",\"5529\"]}}', null, '', 'GmbH', '', '1', '1', '0', '0', '', '', null, '0', '0', null, null, null, null);
COMMIT;

-- ----------------------------
--  Table structure for `shop_account`
-- ----------------------------
DROP TABLE IF EXISTS `shop_account`;
CREATE TABLE `shop_account` (
  `shop_id` bigint(20) NOT NULL DEFAULT '0',
  `account_id` bigint(20) NOT NULL DEFAULT '0',
  PRIMARY KEY (`shop_id`,`account_id`),
  KEY `account_id` (`account_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AVG_ROW_LENGTH=16384 ROW_FORMAT=COMPACT;

-- ----------------------------
--  Records of `shop_account`
-- ----------------------------
BEGIN;
INSERT INTO `shop_account` VALUES ('1', '1');
COMMIT;

-- ----------------------------
--  Table structure for `shop_clipart`
-- ----------------------------
DROP TABLE IF EXISTS `shop_clipart`;
CREATE TABLE `shop_clipart` (
  `shop_id` int(8) NOT NULL,
  `clipart_id` int(8) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
--  Table structure for `shop_contact`
-- ----------------------------
DROP TABLE IF EXISTS `shop_contact`;
CREATE TABLE `shop_contact` (
  `shop_id` bigint(20) NOT NULL DEFAULT '0',
  `contact_id` bigint(20) NOT NULL DEFAULT '0',
  `login` int(11) NOT NULL,
  `admin` int(11) NOT NULL,
  `uuid` varchar(255) NOT NULL,
  `created` datetime NOT NULL,
  `updated` datetime NOT NULL,
  PRIMARY KEY (`shop_id`,`contact_id`),
  KEY `contact_id` (`contact_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AVG_ROW_LENGTH=780 ROW_FORMAT=COMPACT;

-- ----------------------------
--  Records of `shop_contact`
-- ----------------------------
BEGIN;
INSERT INTO `shop_contact` VALUES ('1', '1', '0', '1', '43crw3t34rc3t5t', '0000-00-00 00:00:00', '2012-08-13 23:45:35');
COMMIT;

-- ----------------------------
--  Table structure for `shop_counter`
-- ----------------------------
DROP TABLE IF EXISTS `shop_counter`;
CREATE TABLE `shop_counter` (
  `counter_id` int(11) NOT NULL,
  `max_doc_id` int(11) NOT NULL,
  PRIMARY KEY (`counter_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
--  Table structure for `shop_setting`
-- ----------------------------
DROP TABLE IF EXISTS `shop_setting`;
CREATE TABLE `shop_setting` (
  `id` int(8) NOT NULL AUTO_INCREMENT,
  `shop_id` int(8) NOT NULL,
  `keyid` varchar(255) NOT NULL,
  `value` longtext NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
--  Table structure for `shop_shippingtype`
-- ----------------------------
DROP TABLE IF EXISTS `shop_shippingtype`;
CREATE TABLE `shop_shippingtype` (
  `shippingtype_id` int(8) DEFAULT NULL,
  `shop_id` int(8) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
--  Table structure for `shop_theme`
-- ----------------------------
DROP TABLE IF EXISTS `shop_theme`;
CREATE TABLE `shop_theme` (
  `id` int(8) NOT NULL AUTO_INCREMENT,
  `created` datetime DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `uuid` varchar(100) DEFAULT NULL,
  `install_id` int(8) DEFAULT NULL,
  `shop_id` int(8) DEFAULT NULL,
  `parent_id` int(8) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
--  Table structure for `shop_theme_shop`
-- ----------------------------
DROP TABLE IF EXISTS `shop_theme_shop`;
CREATE TABLE `shop_theme_shop` (
  `theme_id` int(8) DEFAULT NULL,
  `shop_id` int(8) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
--  Table structure for `steplayouterdata`
-- ----------------------------
DROP TABLE IF EXISTS `steplayouterdata`;
CREATE TABLE `steplayouterdata` (
  `uuid` varchar(40) NOT NULL DEFAULT '',
  `data` longtext,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uuid` (`uuid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
--  Table structure for `stockbooking`
-- ----------------------------
DROP TABLE IF EXISTS `stockbooking`;
CREATE TABLE `stockbooking` (
  `uid` varchar(40) DEFAULT NULL,
  `product_id` int(8) DEFAULT NULL,
  `description` text,
  `amount` int(8) DEFAULT NULL,
  `delivery` varchar(255) DEFAULT NULL,
  `contact_id` int(8) DEFAULT NULL,
  `created` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
--  Table structure for `taggable_tag`
-- ----------------------------
DROP TABLE IF EXISTS `taggable_tag`;
CREATE TABLE `taggable_tag` (
  `id` int(8) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
--  Table structure for `upload`
-- ----------------------------
DROP TABLE IF EXISTS `upload`;
CREATE TABLE `upload` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `orderpos_id` int(11) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `path` varchar(255) DEFAULT NULL,
  `typ` varchar(255) DEFAULT NULL,
  `pathpreview` varchar(255) DEFAULT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `chunktitle` varchar(255) DEFAULT NULL,
  `uuid` varchar(40) NOT NULL,
  `preflight_status` int(3) DEFAULT NULL,
  `preflight_errors` longtext,
  PRIMARY KEY (`id`),
  KEY `orderpos_id` (`orderpos_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AVG_ROW_LENGTH=712 PACK_KEYS=0 ROW_FORMAT=COMPACT;

-- ----------------------------
--  Table structure for `upload_article`
-- ----------------------------
DROP TABLE IF EXISTS `upload_article`;
CREATE TABLE `upload_article` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `article_id` int(11) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `path` varchar(255) DEFAULT NULL,
  `typ` varchar(255) DEFAULT NULL,
  `pathpreview` varchar(255) DEFAULT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `chunktitle` varchar(255) DEFAULT NULL,
  `uuid` varchar(40) NOT NULL,
  `preflight_status` int(3) DEFAULT NULL,
  `preflight_errors` longtext,
  PRIMARY KEY (`id`),
  KEY `orderpos_id` (`article_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AVG_ROW_LENGTH=712 PACK_KEYS=0 ROW_FORMAT=COMPACT;

SET FOREIGN_KEY_CHECKS = 1;
