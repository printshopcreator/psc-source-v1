<?php

/**
 * Account Modelclass
 *
 * Account Modelsuperclass
 *
 * PHP versions 4 and 5
 *
 * @category   Data
 * @package    Model
 * @subpackage Supermodel
 * @author     Thomas Peterson <tpeterson@printshopcreator.de>
 * @copyright  1997-2009 Thomas Peterson
 * @license    Not Public
 * @version    SVN: $Id: Account.php 7151 2012-01-16 14:00:19Z boonkerz $
 * @link       http://www.printshopcreator.de
 */

/**
 * Account Modelclass
 *
 * Account Modelsuperclass
 *
 * PHP versions 4 and 5
 *
 * @category   Data
 * @package    Model
 * @subpackage Supermodel
 * @author     Thomas Peterson <tpeterson@printshopcreator.de>
 * @copyright  1997-2009 Thomas Peterson
 * @license    Not Public
 * @version    Release: $Id: Account.php 7151 2012-01-16 14:00:19Z boonkerz $
 * @link       http://www.printshopcreator.de
 */
class Account extends BaseAccount
{

    protected $calcValue1 = '';
    protected $calcValue2 = '';
    protected $extraSettings;

    protected $priceFactor = 1.0;

    protected $mongoLoaded = false;

    public function saveMongo()
    {
        $dbMongo = TP_Mongo::getInstance();
        $obj = $dbMongo->Account->findOne(array('uid' => $this->id));
        if($obj) {
            $dbMongo->Account->updateOne(array('uid' => $this->id), [ '$set' =>  $this->getArray()]);
        }else{
            $dbMongo->Account->insertOne($this->getArray());
        }
    }

    public function loadData() {
        if($this->mongoLoaded) {
            return true;
        }
        $dbMongo = TP_Mongo::getInstance();
        $obj = $dbMongo->Account->findOne(array('uid' => $this->id));
        if($obj) {
            $this->calcValue1 = $obj['calcValue1'];
            $this->calcValue2 = $obj['calcValue2'];
            $this->extraSettings = $obj['extraSettings'];
            $this->priceFactor = $obj['priceFactor'];

            if(!$this->priceFactor) {
                $this->priceFactor = 1.0;
            }
            $this->mongoLoaded = true;
        }
    }

    public function getArray() {
        return array(
            'uid' => $this->id,
            'calcValue1' => $this->calcValue1,
            'calcValue2' => $this->calcValue2,
            'extraSettings' => $this->extraSettings
        );
    }

    /**
     * Methode vor dem Speichern
     * Setzt Created und Updated Datum
     *
     * @param object $event
     * @return null
     */
    public function preSave($event) {

        if ($this->identifier()) {
            $this->updated = date('Y-m-d H:i:s');
            if ($this->created == '') {
                $this->created = date('Y-m-d H:i:s');
            }
        } else {
            $this->created = date('Y-m-d H:i:s');
            $this->updated = date('Y-m-d H:i:s');
        }
    }

    public function getAnrede1() {

        if ($this->anrede1 == 1) {
            return 'Herr';
        } elseif ($this->anrede1 == 2) {
            return 'Frau';
        } elseif ($this->anrede1 == 3) {
            return 'Firma';
        }
    }

    public function getAnrede2() {

        if ($this->anrede2 == 1) {
            return 'Herr';
        } elseif ($this->anrede2 == 2) {
            return 'Frau';
        } elseif ($this->anrede2 == 3) {
            return 'Firma';
        }
    }

    /**
     * Gibt den Bezeichner zurück
     *
     * @return string
     */
    public function __toString() {
        return 'test';
    }

    /**
     * Gibt die Function zurück
     *
     * @return string
     */
    public function getFunction() {
        return false;
    }

    /**
     * Gibt die Firma zurück
     *
     * @return string
     */
    public function getDepartment() {
        return $this->company;
    }

    /**
     * Gibt die Firma zurück
     *
     * @return string
     */
    public function getCompany() {
        return $this->company;
    }

    /**
     * Gibt die Straße zurück
     *
     * @return string
     */
    public function getStreet() {
        return $this->street;
    }

    /**
     * Gibt die Hausnummer zurück
     *
     * @return string
     */
    public function getHouseNumber() {
        return $this->house_number;
    }

    /**
     * Gibt die PLZ zurück
     *
     * @return string
     */
    public function getZip() {
        return $this->zip;
    }

    /**
     * Gibt die Stadt zurück
     *
     * @return string
     */
    public function getCity() {
        return $this->city;
    }

    public function getFirstname() {
        return "";
    }

    public function getLastname() {
        return "";
    }

    /**
     * @return string
     */
    public function getCalcValue2()
    {
        $this->loadData();
        return $this->calcValue2;
    }

    /**
     * @param string $calcValue2
     */
    public function setCalcValue2($calcValue2)
    {
        $this->calcValue2 = $calcValue2;
    }

    /**
     * @return string
     */
    public function getCalcValue1()
    {
        $this->loadData();
        return $this->calcValue1;
    }

    /**
     * @param string $calcValue1
     */
    public function setCalcValue1($calcValue1)
    {
        $this->calcValue1 = $calcValue1;
    }

    /**
     * @return mixed
     */
    public function getExtraSettings()
    {
        $this->loadData();
        return $this->extraSettings;
    }

    /**
     * @param mixed $extraSettings
     */
    public function setExtraSettings($extraSettings)
    {
        $this->extraSettings = $extraSettings;
    }

    /**
     * @return float
     */
    public function getPriceFactor(): float
    {
        $this->loadData();
        return $this->priceFactor;
    }

    /**
     * @param float $priceFactor
     */
    public function setPriceFactor(float $priceFactor): void
    {
        $this->priceFactor = $priceFactor;
    }

}