<?php

/**
 * Adminnews Modelclass
 *
 * Adminnews Modelsuperclass
 *
 * PHP versions 4 and 5
 *
 * @category   Data
 * @package    Model
 * @subpackage Supermodel
 * @author     Thomas Peterson <tpeterson@printshopcreator.de>
 * @copyright  1997-2009 Thomas Peterson
 * @license    Not Public
 * @version    SVN: $Id: Adminnews.php 5832 2011-02-26 00:15:16Z boonkerz $
 * @link       http://www.printshopcreator.de
 */

/**
 * Adminnews Modelclass
 *
 * Adminnews Modelsuperclass
 *
 * PHP versions 4 and 5
 *
 * @category   Data
 * @package    Model
 * @subpackage Supermodel
 * @author     Thomas Peterson <tpeterson@printshopcreator.de>
 * @copyright  1997-2009 Thomas Peterson
 * @license    Not Public
 * @version    Release: $Id: Adminnews.php 5832 2011-02-26 00:15:16Z boonkerz $
 * @link       http://www.printshopcreator.de
 */
class Adminnews extends BaseAdminnews
{

}