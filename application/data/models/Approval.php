<?php

/**
 * Cms Modelclass
 *
 * Cms Modelsuperclass
 *
 * PHP versions 4 and 5
 *
 * @category   Data
 * @package    Model
 * @subpackage Supermodel
 * @author     Thomas petersomn <tpeterson@printshopcreator.de>
 * @copyright  1997-2009 Thomas Peterson
 * @license    Not Public
 * @version    SVN: $Id: Approval.php 5162 2010-11-06 01:42:44Z boonkerz $
 * @link       http://www.printshopcreator.de
 */

/**
 * Cms Modelclass
 *
 * Cms Modelsuperclass
 *
 * PHP versions 4 and 5
 *
 * @category   Data
 * @package    Model
 * @subpackage Supermodel
 * @author     Thomas petersomn <tpeterson@printshopcreator.de>
 * @copyright  1997-2009 Thomas Peterson
 * @license    Not Public
 * @version    Release: $Id: Approval.php 5162 2010-11-06 01:42:44Z boonkerz $
 * @link       http://www.printshopcreator.de
 */

class Approval extends BaseApproval
{

    /**
     * PreSave fired before save or update
     *
     * @return void
     */
    public function preSave($obj) {
        if ($this->identifier()) {
            $this->updated = date('Y-m-d H:i:s');
            if ($this->created == '') {
                $this->created = date('Y-m-d H:i:s');
            }
        } else {
            $this->created = date('Y-m-d H:i:s');
            $this->updated = date('Y-m-d H:i:s');
        }

        if ($this->uuid == '') {
            $this->uuid = TP_Util::uuid();
        }
    }
}