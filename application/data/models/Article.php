<?php

/**
 * Article Modelclass
 *
 * Article Modelsuperclass
 *
 * PHP versions 4 and 5
 *
 * @category   Data
 * @package    Model
 * @subpackage Supermodel
 * @author     Thomas petersomn <tpeterson@printshopcreator.de>
 * @copyright  1997-2009 Thomas Peterson
 * @license    Not Public
 * @version    SVN: $Id: Article.php 7035 2011-12-06 10:31:05Z boonkerz $
 * @link       http://www.printshopcreator.de
 */

/**
 * Article Modelclass
 *
 * Article Modelsuperclass
 *
 * PHP versions 4 and 5
 *
 * @category   Data
 * @package    Model
 * @subpackage Supermodel
 * @author     Thomas Peterson <tpeterson@printshopcreator.de>
 * @copyright  1997-2009 Thomas Peterson
 * @license    Not Public
 * @version    Release: $Id: Article.php 7035 2011-12-06 10:31:05Z boonkerz $
 * @link       http://www.printshopcreator.de
 */
class Article extends BaseArticle
{

    protected $zusatzAbmessung = '';
    protected $zusatzDesigner = '';
    protected $zusatzShipping = '';
    /** @var  \DateTime */
    protected $ablaufDatum;
    protected $ansprechPartner;
    protected $printPartnerEmail;
    protected $mongoLoaded = false;
    protected $custom1;
    protected $custom2;
    protected $custom3;
    protected $custom4;
    protected $custom5;
    protected $custom6;
    protected $custom7;
    protected $custom8;
    protected $custom9;
    protected $custom10;
    protected $custom11;
    protected $custom12;
    protected $text;
    protected $collectingOrdersAccount;
    protected $collectingOrdersAccountFilter = 1;
    protected $collectingOrdersChangePicture = false;
    protected $collectingOrdersCopy = false;
    protected $collectingOrdersNewContact = false;
    protected $collectingOrdersInviteContact = false;
    protected $collectingOrdersIntegrated = false;
    protected $uploadFromLatestOrder = false;
    protected $uploadFromLatestOrderInitalStatus = 10;
    protected $uploadProvided = false;
    protected $uploadProvidedFile = "";
    protected $uploadProvidedInitalStatus = 10;
    protected $extraSettings;
    protected $weight;
    protected $asRequest = false;

    protected $pluginSettings = [];

    public function loadData()
    {
        if ($this->mongoLoaded) {
            return true;
        }
        $dbMongo = TP_Mongo::getInstance();
        $obj = $dbMongo->Product->findOne(array('uid' => $this->id));
        if ($obj) {
            $this->zusatzShipping = $obj['zusatzShipping'];
            $this->zusatzAbmessung = $obj['zusatzAbmessung'];
            $this->zusatzDesigner = $obj['zusatzDesigner'];
            $this->custom1 = $obj['custom1'];
            $this->custom2 = $obj['custom2'];
            $this->custom3 = $obj['custom3'];
            $this->custom4 = $obj['custom4'];
            $this->custom5 = $obj['custom5'];
            $this->custom6 = $obj['custom6'];
            $this->custom7 = $obj['custom7'];
            $this->custom8 = $obj['custom8'];
            $this->custom9 = $obj['custom9'];
            $this->custom10 = $obj['custom10'];
            $this->custom11 = $obj['custom11'];
            $this->custom12 = $obj['custom12'];
            $this->text = $obj['text'];
            $this->extraSettings = $obj['extraSettings'];
            $this->weight = $obj['weight'];
            $this->collectingOrdersAccount = $obj['collectingOrdersAccount'];
            $this->collectingOrdersChangePicture = boolval($obj['collectingOrdersChangePicture']);
            $this->collectingOrdersAccountFilter = $obj['collectingOrdersAccountFilter'];
            $this->collectingOrdersCopy = boolval($obj['collectingOrdersCopy']);
            $this->collectingOrdersNewContact = boolval($obj['collectingOrdersNewContact']);
            $this->collectingOrdersInviteContact = boolval($obj['collectingOrdersInviteContact']);
            $this->collectingOrdersIntegrated = boolval($obj['collectingOrdersIntegrated']);
            $this->uploadFromLatestOrder = boolval($obj['uploadFromLatestOrder']);
            $this->uploadProvided = boolval($obj['uploadProvided']);
            $this->uploadProvidedFile = ($obj['uploadProvidedFile']);
            $this->uploadProvidedInitalStatus = ($obj['uploadProvidedInitalStatus']);
            $this->uploadFromLatestOrderInitalStatus = ($obj['uploadFromLatestOrderInitalStatus']);
            $this->pluginSettings = $obj['pluginSettings'];
            $this->printPartnerEmail = $obj['printPartnerEmail'];
            $this->ansprechPartner = $obj['ansprechPartner'];
            $this->ablaufDatum = $obj['ablaufDatum'];
            $this->asRequest = boolval($obj['asRequest']);
            $this->mongoLoaded = true;
        }
    }

    public function setWeight($weight)
    {
        $this->weight = $weight;
    }

    public function getWeight()
    {
        $this->loadData();
        return $this->weight;
    }

    public function preSave($event)
    {

        if ($this->identifier()) {
            $this->updated = date('Y-m-d H:i:s');
            if ($this->created == '') {
                $this->created = date('Y-m-d H:i:s');
            }
        } else {
            $this->created = date('Y-m-d H:i:s');
            $this->updated = date('Y-m-d H:i:s');
        }

        if ($this->uuid == '') {
            $this->uuid = TP_Util::uuid();
        }
        if ($this->title == "" || $this->title == " ") {
            $this->title = TP_Util::uuid();
        }

        $this->price = str_replace(',', '.', $this->price);
        $this->a6_resale_price = str_replace(',', '.', $this->a6_resale_price);

        $this->a9_price_motiv = str_replace(',', '.', $this->a9_price_motiv);
        $this->a9_price_icon = str_replace(',', '.', $this->a9_price_icon);
        $this->a9_price_text = str_replace(',', '.', $this->a9_price_text);
        $this->a9_price_text_line1 = str_replace(',', '.', $this->a9_price_text_line1);

        $this->a4_abpreis = str_replace(',', '.', $this->a4_abpreis);
        $this->preis = str_replace(',', '.', $this->preis);
        $this->versandwert = str_replace(',', '.', $this->versandwert);

        TP_Util::clearCache();
    }

    public function getYoutubeId()
    {
        return $this->a9_back1;
    }

    public function getPrintpartnerEmail()
    {
        $this->loadData();
        return $this->printPartnerEmail;
    }

    public function setPrintpartnerEmail(string $printPartnerEmail)
    {
        $this->printPartnerEmail = $printPartnerEmail;
    }

    public function getAblaufDatum()
    {
        $this->loadData();
        if($this->ablaufDatum === null) return null;
        $val = $this->ablaufDatum->toDateTime();
        $val->setTimezone(new DateTimeZone('Europe/Berlin'));
        return $val;
    }

    public function setAblaufDatum($datum)
    {
        $this->ablaufDatum = $ablaufDatum;
    }

    public function getAnsprechPartner()
    {
        $this->loadData();
        return $this->ansprechPartner;
    }

    public function setAnsprechPartner($ansprechPartner)
    {
        $this->ansprechPartner = $ansprechPartner;
    }

    public function getMarketFile()
    {
        if ($this->typ == 6 || $this->typ == 8) {
            return APPLICATION_PATH . '/../market/preview/' . $this->a6_directory . 'preview.png';
        } else {
            return $this->file;
        }
    }

    public function getOldChangesValues()
    {
        $changes = json_decode($this->changes, true);
        return array_values($changes);
    }

    public function getChangesValues()
    {
        $changes = json_decode($this->changes, true);
        $tmp = array();
        if (isset($changes['title'])) {
            $tmp[] = $this->title;
        }
        if (isset($changes['einleitung'])) {
            $tmp[] = $this->einleitung;
        }
        if (isset($changes['info'])) {
            $tmp[] = $this->info;
        }
        if (isset($changes['text_format'])) {
            $tmp[] = $this->text_format;
        }
        if (isset($changes['a2_count'])) {
            $tmp[] = $this->a2_count;
        }
        if (isset($changes['text_art'])) {
            $tmp[] = $this->text_art;
        }


        return $tmp;
    }

    public function getChangesKeysReadable()
    {
        $changes = json_decode($this->changes, true);
        $tmp = array();
        if (isset($changes['title'])) {
            $tmp[] = 'Titel';
        }
        if (isset($changes['einleitung'])) {
            $tmp[] = 'Einleitung';
        }
        if (isset($changes['info'])) {
            $tmp[] = 'Infos';
        }
        if (isset($changes['text_format'])) {
            $tmp[] = 'Format';
        }
        if (isset($changes['a2_count'])) {
            $tmp[] = 'Seitenanzahl';
        }
        if (isset($changes['text_art'])) {
            $tmp[] = 'Produktart';
        }

        return $tmp;
    }

    public function setLangData($var)
    {
        $this->lang_data = json_encode($var);
    }

    public function getLangData()
    {
        if ($this->lang_data == "") {
            return array();
        }
        return json_decode($this->lang_data, true);
    }

    public function getTitle($fallback = true, $overwriteLang = false)
    {

        if ($overwriteLang) {

            if (strtolower($overwriteLang) == "de") {
                return $this->title;
            }

            $language = $this->getLangData();
            if (
                isset($language[$overwriteLang]) &&
                isset($language[$overwriteLang]['title']) &&
                $language[$overwriteLang]['title'] != ""
            ) {
                return $language[$overwriteLang]['title'];
            } else {
                if (!$fallback) return "";
            }
        }

        if (strtolower(Zend_Registry::get('locale')->getLanguage()) == "de") {
            return $this->title;
        }

        $language = $this->getLangData();
        if (
            isset($language[Zend_Registry::get('locale')->getLanguage()]) &&
            isset($language[Zend_Registry::get('locale')->getLanguage()]['title']) &&
            $language[Zend_Registry::get('locale')->getLanguage()]['title'] != ""
        ) {
            return $language[Zend_Registry::get('locale')->getLanguage()]['title'];
        } else {
            if (!$fallback) return "";
        }

        return $this->title;
    }

    public function getInfo($fallback = true, $overwriteLang = false)
    {


        if ($overwriteLang) {

            if (strtolower($overwriteLang) == "de") {
                return $this->info;
            }

            $language = $this->getLangData();
            if (
                isset($language[$overwriteLang]) &&
                isset($language[$overwriteLang]['info']) &&
                $language[$overwriteLang]['info'] != ""
            ) {
                return $language[$overwriteLang]['info'];
            } else {
                if (!$fallback) return "";
            }
        }


        if (strtolower(Zend_Registry::get('locale')->getLanguage()) == "de") {
            return $this->info;
        }


        $language = $this->getLangData();
        if (
            isset($language[Zend_Registry::get('locale')->getLanguage()]) &&
            isset($language[Zend_Registry::get('locale')->getLanguage()]['info']) &&
            $language[Zend_Registry::get('locale')->getLanguage()]['info'] != ""
        ) {
            return nl2br($language[Zend_Registry::get('locale')->getLanguage()]['info']);
        } else {
            if (!$fallback) return "";
        }

        return $this->info;
    }

    public function getTextArt($fallback = true, $overwriteLang = false)
    {

        if ($overwriteLang) {

            if (strtolower($overwriteLang) == "de") {
                return $this->text_art;
            }

            $language = $this->getLangData();
            if (
                isset($language[$overwriteLang]) &&
                isset($language[$overwriteLang]['text_art']) &&
                $language[$overwriteLang]['text_art'] != ""
            ) {
                return $language[$overwriteLang]['text_art'];
            } else {
                if (!$fallback) return "";
            }
        }


        if (strtolower(Zend_Registry::get('locale')->getLanguage()) == "de") {
            return $this->text_art;
        }

        $language = $this->getLangData();
        if (
            isset($language[Zend_Registry::get('locale')->getLanguage()]) &&
            isset($language[Zend_Registry::get('locale')->getLanguage()]['text_art']) &&
            $language[Zend_Registry::get('locale')->getLanguage()]['text_art'] != ""
        ) {
            return nl2br($language[Zend_Registry::get('locale')->getLanguage()]['text_art']);
        } else {
            if (!$fallback) return "";
        }

        return $this->text_art;
    }

    public function getEinleitung($fallback = true, $overwriteLang = false)
    {

        if ($overwriteLang) {

            if (strtolower($overwriteLang) == "de") {
                return $this->einleitung;
            }

            $language = $this->getLangData();
            if (
                isset($language[$overwriteLang]) &&
                isset($language[$overwriteLang]['einleitung']) &&
                $language[$overwriteLang]['einleitung'] != ""
            ) {
                return $language[$overwriteLang]['einleitung'];
            } else {
                if (!$fallback) return "";
            }
        }


        if (strtolower(Zend_Registry::get('locale')->getLanguage()) == "de") {
            return $this->einleitung;
        }

        $language = $this->getLangData();
        if (
            isset($language[Zend_Registry::get('locale')->getLanguage()]) &&
            isset($language[Zend_Registry::get('locale')->getLanguage()]['einleitung']) &&
            $language[Zend_Registry::get('locale')->getLanguage()]['einleitung'] != ""
        ) {
            return nl2br($language[Zend_Registry::get('locale')->getLanguage()]['einleitung']);
        } else {
            if (!$fallback) return "";
        }

        return $this->einleitung;
    }

    public function getRiakData()
    {
        return array(
            'title' => strtolower($this->title),
            'shop_id' => $this->shop_id,
            'type' => $this->typ,
            'meta_keywords' => strtolower($this->meta_keywords),
            'description' => strtolower(strip_tags($this->info)),
            'einleitung' => strtolower(strip_tags($this->einleitung)),
            'kostenstelle' => strtolower(strip_tags($this->kostenstelle)),
            'article_nr_intern' => strtolower(strip_tags($this->article_nr_intern)),
            'article_nr_extern' => strtolower(strip_tags($this->article_nr_extern)),
            'install_id' => $this->install_id,
            'lang_data' => $this->lang_data,
            'url' => $this->url,
            'private' => (bool) $this->private,
            'enable' => (bool) $this->enable
        );
    }

    public function getRate()
    {
        if ($this->rate_count == 0) {
            return 0;
        }
        return round($this->rate / $this->rate_count, 0);
    }

    public function getRawOptArray($params, $ser = true)
    {
        $options = array();
        if ($ser) {
            $params = unserialize($params);
        }
        $params = $params->getOptions();
        return $params;
    }

    public function getMarketRawOptArray($params, $ser = true)
    {
        $options = array();
        if ($ser) {
            $params = unserialize($params);
        }


        $params = $params->getOptions();
        return $params;
    }

    public function getOptionArray($params, $ser = true)
    {
        $options = array();

        if ($ser) {
            $params = unserialize($params);
        }
        $special = false;

        if ($params instanceof TP_Basket_Item) {
            $special = $params->getSpecial();
            $params = $params->getOptions();
        }


        $install = Zend_Registry::get('install');

        $xml = $this->a1_xml;
        if ($this->a6_org_article != 0) {
            $articleOrd = Doctrine_Query::create()
                ->from('Article c')
                ->where('c.id = ?', array($this->a6_org_article))
                ->fetchOne();
            $xml = $articleOrd->a1_xml;
        };


        if ($xml != "" || $this->a3_xml != "") {
            $str = str_replace('\\', '', $xml . $this->a3_xml);
            $str = simplexml_load_string($str);

            $template = simplexml_load_string(str_replace('\\"', '"', '<root>' . $install['calc_template'] . '</root>'));

            if (is_array($params)) {
                $opt = $params;
                foreach ($params as $key => $value) {
                    if ($special != true) {

                        if (count($str->artikel) == 1) {
                            $opt['kalk_artikel'] = (string) $str->artikel->name;
                        }

                        $keys = $str->xpath("//artikel[name='" . $opt['kalk_artikel'] . "']/option[@id='" . $key . "']");
                        if (isset($keys[0])) {
                            if (($keys[0]['type'] == 'Radio' || $keys[0]['type'] == 'Select') && $keys[0]['mode'] != 'papierdb') {
                                $value = $str->xpath("//artikel[name='" . $opt['kalk_artikel'] . "']/option[@id='" . $key . "']/opt[@id='" . $value . "']");
                                if (isset($value[0])) {
                                    $options[] = [
                                        'id' => (string) $keys[0]['id'],
                                        'label' => (string) $keys[0]['name'],
                                        'value' => (string) $value[0]['name']
                                    ];
                                }
                            } elseif ($keys[0]['mode'] == 'papierdb') {

                                $papierSingle = Doctrine_Query::create()->select()->from('Papier p')->andWhere('p.art_nr = ? AND p.install_id = ?', array($value, $this->Install->id))->fetchOne();
                                $options[] = [
                                    'id' => (string) $keys[0]['id'],
                                    'label' => (string) $keys[0]['name'],
                                    'value' => $papierSingle->description_1
                                ];
                            } elseif ($keys[0]['type'] == 'Checkbox') {
                                $strr = array();
                                foreach ($value as $keyys) {
                                    $val = $str->xpath("//artikel[name='" . $opt['kalk_artikel'] . "']/option[@id='" . $key . "']/opt[@id='" . $keyys . "']");
                                    $strr[] = $val[0]['name'];
                                }
                                $options[] = [
                                    'id' => (string) $keys[0]['id'],
                                    'label' => (string) $keys[0]['name'],
                                    'value' => implode(', ', $strr)
                                ];
                            } elseif (strtolower($keys[0]['type']) == 'template') {
                                $keys = $template->xpath('//option[@id="' . $keys[0]['select'] . '"]');

                                if ($keys[0]['type'] == 'Select' && $keys[0]['mode'] != 'papierdb') {
                                    $value = $template->xpath("//option[@id='" . $key . "']/opt[@id='" . $value . "']");
                                    if (isset($value[0])) {
                                        array_push($options, (string) $keys[0]['name'] . ': ' . (string) $value[0]['name']);
                                        $optionsAr[(string) $keys[0]['name']] = (string) $value[0]['name'];
                                        $optionsArId[(string) $keys[0]['id']] = (string) $value[0]['name'];
                                    }
                                } elseif ($keys[0]['mode'] == 'papierdb') {
                                    $papierSingle = Doctrine_Query::create()->select()->from('Papier p')->andWhere('p.art_nr = ? AND p.install_id = ?', array($value, $this->Install->id))->fetchOne();
                                    array_push($options, (string) $keys[0]['name'] . ': ' . $papierSingle->description_1);
                                    $optionsAr[(string) $keys[0]['name']] = $papierSingle->description_1;
                                    $optionsArId[(string) $keys[0]['id']] = $papierSingle->description_1;
                                } elseif ($keys[0]['type'] == 'Checkbox') {
                                    $strr = array();
                                    foreach ($value as $keyys) {
                                        $val = $template->xpath("//option[@id='" . $key . "']/opt[@id='" . $keyys . "']");
                                        $strr[] = $val[0]['name'];
                                    }
                                    $optionsAr[$keys[0]['name']] = implode(', ', $strr);
                                    $optionsArId[(string) $keys[0]['id']] = implode(', ', $strr);
                                    array_push($options, (string) $keys[0]['name'] . ': ' . implode(', ', $strr));
                                } elseif (strtolower($keys[0]['type']) != 'hidden') {
                                    array_push($options, (string) $keys[0]['name'] . ': ' . $value);
                                    $optionsAr[$keys[0]['name']] = $value;
                                    $optionsArId[(string) $keys[0]['id']] = $value;
                                }
                            } elseif (strtolower($keys[0]['type']) != 'hidden') {
                                $options[] = [
                                    'id' => (string) $keys[0]['id'],
                                    'label' => (string) $keys[0]['name'],
                                    'value' => $value
                                ];
                            } elseif (strtolower($keys[0]['type']) == 'hidden' && $key == 'papier') {
                                $papierSingle = Doctrine_Query::create()->select()->from('Papier p')->andWhere('p.art_nr = ? AND p.install_id = ?', array($value, $this->Install->id))->fetchOne();
                                $options[] = [
                                    'id' => (string) $keys[0]['id'],
                                    'label' => (string) $keys[0]['name'],
                                    'value' => $papierSingle->description_1
                                ];
                            }
                        } elseif ($key == 'kalk_artikel') {
                        } elseif ($key == 'upload_mode') {
                        }
                    } else {
                        $options[] = [
                            'id' => (string) $key,
                            'label' => (string) $key,
                            'value' => $value
                        ];
                    }
                }
            }
        }

        return $options;
    }


    public function getMarketOptArray($params, $ser = true, $dsgData)
    {
        $options = array();
        if ($dsgData != "") {
            $dsgData = Zend_Json::decode($dsgData);
            if ($ser) {
                $params = unserialize($params);
            }
            $params = $params->getOptions();
            if (isset($dsgData['colors'])) {
                array_push($options, 'Farbe: ' . strstr($dsgData['colors'], ",", true));
            } elseif ($this->not_edit) {
                $dsgFData = Zend_Json::decode($this->mercendise_design);
                array_push($options, 'Farbe: ' . strstr($dsgFData['colors'], ",", true));
            }

            $xmldes = simplexml_load_string('<?xml version="1.0"?>' . $this->a9_sizes);
            foreach ($xmldes->itm as $item) {
                if ((int) $params['s' . $item['id']]) {
                    array_push($options, 'Größe ' . $item['size'] . ': ' . (int) $params['s' . $item['id']]);
                }
            }
        }
        return $options;
    }

    public function getOptArray($params, $ser = true, $asArray = false, $widtIds = false)
    {
        $options = array();
        $optionsAr = array();
        $optionsArId = array();
        if ($ser) {
            $params = unserialize($params);
        }
        $special = false;

        if ($params instanceof TP_Basket_Item) {
            $special = $params->getSpecial();
            $params = $params->getOptions();
        }

        $install = Zend_Registry::get('install');

        $xml = $this->a1_xml;
        if ($this->a6_org_article != 0) {
            $articleOrd = Doctrine_Query::create()
                ->from('Article c')
                ->where('c.id = ?', array($this->a6_org_article))
                ->fetchOne();
            $xml = $articleOrd->a1_xml;
        };


        if ($xml != "" || $this->a3_xml != "") {
            $str = str_replace('\\', '', $xml . $this->a3_xml);
            $str = simplexml_load_string($str);

            $template = simplexml_load_string(str_replace('\\"', '"', '<root>' . $install['calc_template'] . '</root>'));

            if ($str && is_array($params)) {
                $opt = $params;
                foreach ($params as $key => $value) {
                    if ($special != true) {
                        if (count($str->artikel) == 1) {
                            $opt['kalk_artikel'] = (string) $str->artikel->name;
                        }
                        if (count($str->artikel) > 1 ) {
                            $opt['kalk_artikel'] = (string) $str->artikel[0]->name;
                        }

                        $keys = $str->xpath("//artikel[name='" . $opt['kalk_artikel'] . "']/option[@id='" . $key . "']");
                        if (isset($keys[0])) {
                            if (($keys[0]['type'] == 'Radio' || $keys[0]['type'] == 'Select') && $keys[0]['mode'] != 'papierdb') {
                                $value = $str->xpath("//artikel[name='" . $opt['kalk_artikel'] . "']/option[@id='" . $key . "']/opt[@id='" . $value . "']");
                                if (isset($value[0])) {
                                    $optionsAr[(string) $keys[0]['name']] = (string) $value[0]['name'];
                                    $optionsArId[(string) $keys[0]['id']] = (string) $value[0]['name'];
                                    array_push($options, (string) $keys[0]['name'] . ': ' . (string) $value[0]['name']);
                                }
                            } elseif ($keys[0]['mode'] == 'papierdb') {

                                $papierSingle = Doctrine_Query::create()->select()->from('Papier p')->andWhere('p.art_nr = ? AND p.install_id = ?', array($value, $this->Install->id))->fetchOne();
                                array_push($options, (string) $keys[0]['name'] . ': ' . $papierSingle->description_1);
                                $optionsAr[(string) $keys[0]['name']] = $papierSingle->description_1;
                                $optionsArId[(string) $keys[0]['id']] = $papierSingle->description_1;
                            } elseif ($keys[0]['type'] == 'Checkbox') {
                                $strr = array();
                                foreach ($value as $keyys) {
                                    $val = $str->xpath("//artikel[name='" . $opt['kalk_artikel'] . "']/option[@id='" . $key . "']/opt[@id='" . $keyys . "']");
                                    $strr[] = $val[0]['name'];
                                }
                                array_push($options, (string) $keys[0]['name'] . ': ' . implode(', ', $strr));
                                $optionsAr[(string) $keys[0]['name']] = implode(', ', $strr);
                                $optionsArId[(string) $keys[0]['id']] = implode(', ', $strr);
                            } elseif (strtolower($keys[0]['type']) == 'template') {
                                $keys = $template->xpath('//option[@id="' . $keys[0]['select'] . '"]');

                                if ($keys[0]['type'] == 'Select' && $keys[0]['mode'] != 'papierdb') {
                                    $value = $template->xpath("//option[@id='" . $key . "']/opt[@id='" . $value . "']");
                                    if (isset($value[0])) {
                                        array_push($options, (string) $keys[0]['name'] . ': ' . (string) $value[0]['name']);
                                        $optionsAr[(string) $keys[0]['name']] = (string) $value[0]['name'];
                                        $optionsArId[(string) $keys[0]['id']] = (string) $value[0]['name'];
                                    }
                                } elseif ($keys[0]['mode'] == 'papierdb') {
                                    $papierSingle = Doctrine_Query::create()->select()->from('Papier p')->andWhere('p.art_nr = ? AND p.install_id = ?', array($value, $this->Install->id))->fetchOne();
                                    array_push($options, (string) $keys[0]['name'] . ': ' . $papierSingle->description_1);
                                    $optionsAr[(string) $keys[0]['name']] = $papierSingle->description_1;
                                    $optionsArId[(string) $keys[0]['id']] = $papierSingle->description_1;
                                } elseif ($keys[0]['type'] == 'Checkbox') {
                                    $strr = array();
                                    foreach ($value as $keyys) {
                                        $val = $template->xpath("//option[@id='" . $key . "']/opt[@id='" . $keyys . "']");
                                        $strr[] = $val[0]['name'];
                                    }
                                    $optionsAr[$keys[0]['name']] = implode(', ', $strr);
                                    $optionsArId[(string) $keys[0]['id']] = implode(', ', $strr);
                                    array_push($options, (string) $keys[0]['name'] . ': ' . implode(', ', $strr));
                                } elseif (strtolower($keys[0]['type']) != 'hidden') {
                                    array_push($options, (string) $keys[0]['name'] . ': ' . $value);
                                    $optionsAr[$keys[0]['name']] = $value;
                                    $optionsArId[(string) $keys[0]['id']] = $value;
                                }
                            } elseif (strtolower($keys[0]['type']) != 'hidden') {

                                $optionsAr[(string) $keys[0]['name']] = $value;
                                $optionsArId[(string) $keys[0]['id']] = $value;
                                array_push($options, (string) $keys[0]['name'] . ': ' . $value);
                            } elseif (strtolower($keys[0]['type']) == 'hidden' && $key == 'papier') {
                                $papierSingle = Doctrine_Query::create()->select()->from('Papier p')->andWhere('p.art_nr = ? AND p.install_id = ?', array($value, $this->Install->id))->fetchOne();
                                array_push($options, (string) $keys[0]['name'] . ': ' . $papierSingle->description_1);
                                $optionsAr[(string) $keys[0]['name']] = $papierSingle->description_1;
                                $optionsArId[(string) $keys[0]['id']] = $papierSingle->description_1;
                            }
                        } elseif ($key == 'kalk_artikel') {
                            $optionsAr['Typ'] = $value;
                            $optionsArId['typ'] = $value;
                            array_unshift($options, 'Typ: ' . $value);
                        } elseif ($key == 'upload_mode') {
                            //$optionsAr[$key] = $value;
                            $optionsArId[$key] = $value;
                            $upload = array();
                            $upload['article'] = 'Jetzt Hochladen';
                            $upload['center'] = 'Upload im Uploadcenter';
                            $upload['weblayouter'] = 'jetzt Online Gestalten';
                            $upload['post'] = 'per Post';
                            $upload['email'] = 'per EMail';

                            //array_push($options, (string) 'Upload: ' . $upload[$value]);
                        }
                    } else {
                        array_push($options, (string) $key . ': ' . $value);
                    }
                }
            }
        } elseif ($this->typ == '7') {
            $str = simplexml_load_file(APPLICATION_PATH . '/articles/Freedescription/config/frontend.xml');
            if (is_array($params)) {
                foreach ($params as $key => $value) {
                    if ($special != true) {
                        $keys = $str->xpath("//option[@id='" . $key . "']");
                        if (isset($keys[0])) {
                            if ($keys[0]['type'] == 'Select' && $keys[0]['mode'] != 'papierdb') {
                                $value = $str->xpath("//option[@id='" . $key . "']/opt[@id='" . $value . "']");
                                array_push($options, (string) $keys[0]['name'] . ': ' . (string) $value[0]['name']);
                            } elseif ($keys[0]['type'] == 'Select' && $keys[0]['mode'] == 'papierdb') {
                                $papierSingle = Doctrine_Query::create()->select()->from('Papier p')->andWhere('p.art_nr = ? AND p.install_id = ?', array($value, $this->Install->id))->fetchOne();
                                array_push($options, (string) $keys[0]['name'] . ': ' . $papierSingle->description_1);
                            } else {
                                array_push($options, (string) $keys[0]['name'] . ': ' . $value);
                            }
                        }
                    } else {
                        array_push($options, (string) $key . ': ' . $value);
                    }
                }
            }
        }
        if ($widtIds) {
            return $optionsArId;
        }
        if ($asArray) {
            return $optionsAr;
        }

        return $options;
    }

    public function getOptArrayName2($params, $ser = true, $asArray = false)
    {
        $options = array();
        $optionsAr = array();
        if ($ser) {
            $params = unserialize($params);
        }

        $special = $params->getSpecial();
        $params = $params->getOptions();

        $install = Zend_Registry::get('install');

        if ($this->a1_xml != "" || $this->a3_xml != "") {
            $str = str_replace('\\', '', $this->a1_xml . $this->a3_xml);
            $str = simplexml_load_string($str);

            $template = simplexml_load_string(str_replace('\\"', '"', '<root>' . $install['calc_template'] . '</root>'));

            if (is_array($params)) {
                $opt = $params;
                foreach ($params as $key => $value) {
                    if ($special != true) {

                        if (count($str->artikel) == 1) {
                            $opt['kalk_artikel'] = (string) $str->artikel->name;
                        }

                        if (isset($keys[0])) {
                            if (($keys[0]['type'] == 'Radio' || $keys[0]['type'] == 'Select') && $keys[0]['mode'] != 'papierdb') {
                                $optionsAr[$key] = $value;
                                $value = $str->xpath("//artikel[name='" . $opt['kalk_artikel'] . "']/option[@id='" . $key . "']/opt[@id='" . $value . "']");
                                if (isset($value[0])) {
                                    array_push($options, (string) $keys[0]['name'] . ': ' . (string) $value[0]['name']);
                                }
                            } elseif ($keys[0]['mode'] == 'papierdb') {
                                $optionsAr[$key] = $value;
                                $papierSingle = Doctrine_Query::create()->select()->from('Papier p')->andWhere('p.art_nr = ? AND p.install_id = ?', array($value, $this->Install->id))->fetchOne();
                                array_push($options, (string) $keys[0]['name'] . ': ' . $papierSingle->description_2);
                            } elseif ($keys[0]['type'] == 'Checkbox') {
                                $optionsAr[$key] = $value;
                                $strr = array();
                                foreach ($value as $keyys) {
                                    $val = $str->xpath("//artikel[name='" . $opt['kalk_artikel'] . "']/option[@id='" . $key . "']/opt[@id='" . $keyys . "']");
                                    $strr[] = $val[0]['name'];
                                }
                                array_push($options, (string) $keys[0]['name'] . ': ' . implode(', ', $strr));
                            } elseif (strtolower($keys[0]['type']) == 'template') {
                                $optionsAr[$key] = $value;
                                $keys = $template->xpath('//option[@id="' . $keys[0]['select'] . '"]');

                                if ($keys[0]['type'] == 'Select' && $keys[0]['mode'] != 'papierdb') {
                                    $value = $template->xpath("//option[@id='" . $key . "']/opt[@id='" . $value . "']");
                                    if (isset($value[0])) {
                                        array_push($options, (string) $keys[0]['name'] . ': ' . (string) $value[0]['name']);
                                    }
                                } elseif ($keys[0]['mode'] == 'papierdb') {
                                    $papierSingle = Doctrine_Query::create()->select()->from('Papier p')->andWhere('p.art_nr = ? AND p.install_id = ?', array($value, $this->Install->id))->fetchOne();
                                    array_push($options, (string) $keys[0]['name'] . ': ' . $papierSingle->description_1);
                                } elseif ($keys[0]['type'] == 'Checkbox') {
                                    $strr = array();
                                    foreach ($value as $keyys) {
                                        $val = $template->xpath("//option[@id='" . $key . "']/opt[@id='" . $keyys . "']");
                                        $strr[] = $val[0]['name'];
                                    }
                                    array_push($options, (string) $keys[0]['name'] . ': ' . implode(', ', $strr));
                                } elseif (strtolower($keys[0]['type']) != 'hidden') {
                                    array_push($options, (string) $keys[0]['name'] . ': ' . $value);
                                }
                            } elseif (strtolower($keys[0]['type']) != 'hidden') {
                                $optionsAr[$key] = $value;
                                array_push($options, (string) $keys[0]['name'] . ': ' . $value);
                            } elseif (strtolower($keys[0]['type']) == 'hidden' && $key == 'papier') {
                                $optionsAr[$key] = $value;
                                $papierSingle = Doctrine_Query::create()->select()->from('Papier p')->andWhere('p.art_nr = ? AND p.install_id = ?', array($value, $this->Install->id))->fetchOne();
                                array_push($options, (string) $keys[0]['name'] . ': ' . $papierSingle->description_2);
                            }
                        } elseif ($key == 'kalk_artikel') {
                            $optionsAr[$key] = $value;
                            array_push($options, 'Typ: ' . $value);
                        } elseif ($key == 'upload_mode') {
                            //$optionsAr[$key] = $value;
                            $upload = array();
                            $upload['article'] = 'Jetzt Hochladen';
                            $upload['center'] = 'Upload im Uploadcenter';
                            $upload['weblayouter'] = 'jetzt Online Gestalten';
                            $upload['post'] = 'per Post';
                            $upload['email'] = 'per EMail';

                            //array_push($options, (string) 'Upload: ' . $upload[$value]);
                        }
                    } else {
                        array_push($options, (string) $key . ': ' . $value);
                    }
                }
            }
        } elseif ($this->typ == '7') {
            $str = simplexml_load_file(APPLICATION_PATH . '/articles/Freedescription/config/frontend.xml');
            if (is_array($params)) {
                foreach ($params as $key => $value) {
                    if ($special != true) {
                        $keys = $str->xpath("//option[@id='" . $key . "']");
                        if (isset($keys[0])) {
                            if ($keys[0]['type'] == 'Select' && $keys[0]['mode'] != 'papierdb') {
                                $value = $str->xpath("//option[@id='" . $key . "']/opt[@id='" . $value . "']");
                                array_push($options, (string) $keys[0]['name'] . ': ' . (string) $value[0]['name']);
                            } elseif ($keys[0]['type'] == 'Select' && $keys[0]['mode'] == 'papierdb') {
                                $papierSingle = Doctrine_Query::create()->select()->from('Papier p')->andWhere('p.art_nr = ? AND p.install_id = ?', array($value, $this->Install->id))->fetchOne();
                                array_push($options, (string) $keys[0]['name'] . ': ' . $papierSingle->description_1);
                            } else {
                                array_push($options, (string) $keys[0]['name'] . ': ' . $value);
                            }
                        }
                    } else {
                        array_push($options, (string) $key . ': ' . $value);
                    }
                }
            }
        }
        if ($asArray) {
            return $optionsAr;
        }
        return $options;
    }

    public function getLiveedit($field, $type = 'INPUT')
    {
        $mode = new Zend_Session_Namespace('adminmode');

        if ($mode->liveedit && $type == 'INPUT') {
            return '<span class="liveedit" mode="text"table="article" field="' . $field . '" id="' . $this->id . '">' . $this->$field . '</div>';
        } elseif ($mode->liveedit && $type == 'RTE') {
            return '<span class="liveedit" mode="wysiwyg" table="article" field="' . $field . '" id="' . $this->id . '">' . $this->$field . '</div>';
        }

        return $this->$field;
    }

    public function getRelated($limit = 100)
    {

        $rows = Doctrine_Query::create()->select('a.*, (a.rate/a.rate_count) as ratesum, l.*, (l.a4_abpreis+a.a6_resale_price) as sum')->from('Article a')
            ->leftJoin('a.OrgArticle l')
            ->leftJoin('a.ArticleReleated c')->where('c.article1 = ? AND a.enable = 1 AND a.private = 0', array($this->id))->limit($limit)->execute();

        if (count($rows) == 0) {
            return array();
        }
        return $rows;
    }

    public function getBuyRelated($limit = 5)
    {

        $install = Zend_Registry::get('install');
        $shop = Zend_Registry::get('shop');

        $rows = Doctrine_Manager::connection()->execute('SELECT DISTINCT b2.article_id FROM article_buy b1 JOIN article_buy b2 ON (b1.contact_id = b2.contact_id and b1.article_id != b2.article_id)
                    WHERE b1.article_id = ? AND b1.contact_id != ?', array($this->id, $install['admincontact']));

        $articles = array();
        foreach ($rows->fetchAll(Doctrine::FETCH_ASSOC) as $row) {
            $articles[] = $row['article_id'];
        }
        if (empty($articles)) {
            return array();
        }

        if ($shop['pmb']) {
            return Doctrine_Query::create()->select('a.*, (a.rate/a.rate_count) as ratesum, l.*, (l.a4_abpreis+a.a6_resale_price) as sum')->from('Article a')->leftJoin('a.OrgArticle l')
                ->whereIn('a.id', $articles)->addWhere('a.enable = 1 AND a.private = 0 AND a.display_market = 1')->limit(intval($limit))->execute();
        }


        return Doctrine_Query::create()->select('a.*, (a.rate/a.rate_count) as ratesum, l.*, (l.a4_abpreis+a.a6_resale_price) as sum')->from('Article a')->leftJoin('a.OrgArticle l')
            ->whereIn('a.id', $articles)->addWhere('a.enable = 1 AND a.private = 0 AND a.shop_id = ?', array($shop['id']))->limit(intval($limit))->execute();
    }

    public function getArticlegroupAsRootline()
    {
        $str = '';
        if (isset($this->ArticleGroup[0]->ArticleGroup)) {
            $str = $this->ArticleGroup[0]->ArticleGroup->title . ' ' . $str;
        }
        if (isset($this->ArticleGroup[0]->ArticleGroup->ArticleGroup)) {
            $str = $this->ArticleGroup[0]->ArticleGroup->ArticleGroup->title . ' ' . $str;
        }
        return $str;
    }

    public function getSetProducts()
    {

        if ($this->set_config != "") {

            $setConfig = json_decode($this->set_config);

            if (is_array($setConfig) && count($setConfig) > 0) {

                $products = array();
                foreach ($setConfig as $conf) {
                    $product = Doctrine_Query::create()->select()->from('Article p')->andWhere('p.id = ? AND p.install_id = ?', array($conf->article_id, $this->install_id))->fetchOne();

                    $products[] = $product;
                }

                return $products;
            }
        }

        return array();
    }

    public function getUploadsFromXML($params)
    {

        $xml = str_replace('\\', '', $this->a1_xml);
        $params = unserialize($params);
        $params = $params->getOptions();
        try {
            $xml = new SimpleXMLElement($xml, null, false);
        } catch (Exception $e) {
            $xml = array();
        }
        $uploads = array();

        foreach ($xml as $article_xml) {

            $up = array();
            foreach ($article_xml->uploads->children() as $upload) {
                if (isset($upload['select']) && $params[(string) $upload['select']] != (string) $upload['value']) {
                    continue;
                }
                $up[] = array(
                    'id' => (string) $upload['id'],
                    'name' => (string) $upload['name'],
                    'description' => (string) $upload['description']
                );
            }

            $uploads[(string) $article_xml->name] = $up;
        }



        return $uploads[$params['kalk_artikel']];
    }

    /**
     * @return string
     */
    public function getZusatzShipping()
    {
        $this->loadData();
        return $this->zusatzShipping;
    }

    /**
     * @return string
     */
    public function getZusatzAbmessung()
    {
        $this->loadData();
        return $this->zusatzAbmessung;
    }

    /**
     * @return string
     */
    public function getZusatzDesigner()
    {
        $this->loadData();
        return $this->zusatzDesigner;
    }

    /**
     * @return mixed
     */
    public function getCustom1()
    {
        $this->loadData();
        return $this->custom1;
    }

    /**
     * @param mixed $custom1
     */
    public function setCustom1($custom1)
    {
        $this->custom1 = $custom1;
    }

    /**
     * @return mixed
     */
    public function getCustom2()
    {
        $this->loadData();
        return $this->custom2;
    }

    /**
     * @param mixed $custom2
     */
    public function setCustom2($custom2)
    {
        $this->custom2 = $custom2;
    }

    /**
     * @return mixed
     */
    public function getCustom3()
    {
        $this->loadData();
        return $this->custom3;
    }

    /**
     * @param mixed $custom3
     */
    public function setCustom3($custom3)
    {
        $this->custom3 = $custom3;
    }

    /**
     * @return mixed
     */
    public function getCustom4()
    {
        $this->loadData();
        return $this->custom4;
    }

    /**
     * @param mixed $custom4
     */
    public function setCustom4($custom4)
    {
        $this->custom4 = $custom4;
    }

    /**
     * @return mixed
     */
    public function getCustom5()
    {
        $this->loadData();
        return $this->custom5;
    }

    /**
     * @param mixed $custom5
     */
    public function setCustom5($custom5)
    {
        $this->custom5 = $custom5;
    }

    /**
     * @return mixed
     */
    public function getCustom6()
    {
        $this->loadData();
        return $this->custom6;
    }

    /**
     * @param mixed $custom6
     */
    public function setCustom6($custom6)
    {
        $this->custom6 = $custom6;
    }

    /**
     * @return mixed
     */
    public function getCustom7()
    {
        $this->loadData();
        return $this->custom7;
    }

    /**
     * @param mixed $custom7
     */
    public function setCustom7($custom7)
    {
        $this->custom7 = $custom7;
    }

    /**
     * @return mixed
     */
    public function getCustom8()
    {
        $this->loadData();
        return $this->custom8;
    }

    /**
     * @param mixed $custom8
     */
    public function setCustom8($custom8)
    {
        $this->custom8 = $custom8;
    }

    /**
     * @return mixed
     */
    public function getCustom9()
    {
        $this->loadData();
        return $this->custom9;
    }

    /**
     * @param mixed $custom9
     */
    public function setCustom9($custom9)
    {
        $this->custom9 = $custom9;
    }

    /**
     * @return mixed
     */
    public function getCustom10()
    {
        $this->loadData();
        return $this->custom10;
    }

    /**
     * @param mixed $custom10
     */
    public function setCustom10($custom10)
    {
        $this->custom10 = $custom10;
    }

    /**
     * @return mixed
     */
    public function getCustom11()
    {
        $this->loadData();
        return $this->custom11;
    }

    /**
     * @param mixed $custom11
     */
    public function setCustom11($custom11)
    {
        $this->custom11 = $custom11;
    }

    /**
     * @return mixed
     */
    public function getCustom12()
    {
        $this->loadData();
        return $this->custom12;
    }

    /**
     * @param mixed $custom12
     */
    public function setCustom12($custom12)
    {
        $this->custom12 = $custom12;
    }

    /**
     * @return mixed
     */
    public function getCollectingOrdersAccount()
    {
        $this->loadData();
        return $this->collectingOrdersAccount;
    }

    /**
     * @param mixed $collectingOrdersAccount
     */
    public function setCollectingOrdersAccount($collectingOrdersAccount)
    {
        $this->collectingOrdersAccount = $collectingOrdersAccount;
    }

    /**
     * @return bool
     */
    public function isCollectingOrdersChangePicture()
    {
        $this->loadData();
        return $this->collectingOrdersChangePicture;
    }

    /**
     * @param bool $collectingOrdersChangePicture
     */
    public function setCollectingOrdersChangePicture($collectingOrdersChangePicture)
    {
        $this->collectingOrdersChangePicture = $collectingOrdersChangePicture;
    }

    /**
     * @return bool
     */
    public function isCollectingOrdersCopy()
    {
        $this->loadData();
        return $this->collectingOrdersCopy;
    }

    /**
     * @param bool $collectingOrdersCopy
     */
    public function setCollectingOrdersCopy($collectingOrdersCopy)
    {
        $this->collectingOrdersCopy = $collectingOrdersCopy;
    }

    /**
     * @return bool
     */
    public function isCollectingOrdersNewContact()
    {
        $this->loadData();
        return $this->collectingOrdersNewContact;
    }

    /**
     * @param bool $collectingOrdersNewContact
     */
    public function setCollectingOrdersNewContact($collectingOrdersNewContact)
    {
        $this->collectingOrdersNewContact = $collectingOrdersNewContact;
    }

    /**
     * @return bool
     */
    public function isCollectingOrdersInviteContact()
    {
        $this->loadData();
        return $this->collectingOrdersInviteContact;
    }

    /**
     * @param bool $collectingOrdersInviteContact
     */
    public function setCollectingOrdersInviteContact($collectingOrdersInviteContact)
    {
        $this->collectingOrdersInviteContact = $collectingOrdersInviteContact;
    }

    /**
     * @return int
     */
    public function getCollectingOrdersAccountFilter()
    {
        $this->loadData();
        return $this->collectingOrdersAccountFilter;
    }

    /**
     * @param int $collectingOrdersAccountFilter
     */
    public function setCollectingOrdersAccountFilter($collectingOrdersAccountFilter)
    {
        $this->collectingOrdersAccountFilter = $collectingOrdersAccountFilter;
    }

    /**
     * @return array
     */
    public function getPluginSettings($module, $name)
    {
        $this->loadData();
        if (!isset($this->pluginSettings[$module]) || !isset($this->pluginSettings[$module][$name])) {
            return null;
        }

        return $this->pluginSettings[$module][$name];
    }

    /**
     * @return bool
     */
    public function isCollectingOrdersIntegrated()
    {
        $this->loadData();
        return $this->collectingOrdersIntegrated;
    }

    /**
     * @param bool $collectingOrdersIntegrated
     */
    public function setCollectingOrdersIntegrated($collectingOrdersIntegrated)
    {
        $this->collectingOrdersIntegrated = $collectingOrdersIntegrated;
    }

    /**
     * @return mixed
     */
    public function getText()
    {
        $this->loadData();
        return $this->text;
    }

    /**
     * @param mixed $text
     */
    public function setText($text)
    {
        $this->text = $text;
    }

    /**
     * @return mixed
     */
    public function getExtraSettings()
    {
        $this->loadData();
        return $this->extraSettings;
    }

    /**
     * @param mixed $extraSettings
     */
    public function setExtraSettings($extraSettings)
    {
        $this->extraSettings = $extraSettings;
    }

    /**
     * @return int
     */
    public function getUploadFromLatestOrderInitalStatus(): int
    {
        $this->loadData();
        return (int)$this->uploadFromLatestOrderInitalStatus;
    }

    /**
     * @param int $uploadFromLatestOrderInitalStatus
     */
    public function setUploadFromLatestOrderInitalStatus(int $uploadFromLatestOrderInitalStatus): void
    {
        $this->uploadFromLatestOrderInitalStatus = $uploadFromLatestOrderInitalStatus;
    }

    /**
     * @return bool
     */
    public function isUploadFromLatestOrder(): bool
    {
        $this->loadData();
        return (bool)$this->uploadFromLatestOrder;
    }

    /**
     * @param bool $uploadFromLatestOrder
     */
    public function setUploadFromLatestOrder(bool $uploadFromLatestOrder): void
    {
        $this->uploadFromLatestOrder = $uploadFromLatestOrder;
    }

    /**
     * @return bool
     */
    public function isUploadProvided(): bool
    {
        $this->loadData();
        return (bool)$this->uploadProvided;
    }

    /**
     * @param bool $uploadProvided
     */
    public function setUploadProvided(bool $uploadProvided): void
    {
        $this->uploadProvided = $uploadProvided;
    }

    /**
     * @return string
     */
    public function getUploadProvidedFile(): string
    {
        $this->loadData();
        return (string)$this->uploadProvidedFile;
    }

    /**
     * @param string $uploadProvidedFile
     */
    public function setUploadProvidedFile(string $uploadProvidedFile): void
    {
        $this->uploadProvidedFile = $uploadProvidedFile;
    }

    /**
     * @return int
     */
    public function getUploadProvidedInitalStatus(): int
    {
        $this->loadData();
        return (int)$this->uploadProvidedInitalStatus;
    }

    /**
     * @param int $uploadProvidedInitalStatus
     */
    public function setUploadProvidedInitalStatus(int $uploadProvidedInitalStatus): void
    {
        $this->uploadProvidedInitalStatus = $uploadProvidedInitalStatus;
    }

    public function copyProvidedFileToBasket(Orderspos $pos) {

        $dbMongo = TP_Mongo::getInstance();
        $obj = $dbMongo->Media->findOne(array('_id' => new \MongoDB\BSON\ObjectId($this->getUploadProvidedFile())));
        if($obj && file_exists('/data/www/new/web/'.$obj['url'])) {
            if(!file_exists('/data/www/old/market/basket/' . $pos->orders_id)) {
                mkdir('/data/www/old/market/basket/' . $pos->orders_id);
            }
            if(!file_exists('/data/www/old/market/basket/' . $pos->orders_id. '/' . $pos->pos)) {
                mkdir('/data/www/old/market/basket/' . $pos->orders_id . '/' . $pos->pos);
            }

            copy('/data/www/new/web/'.$obj['url'], '/data/www/old/market/basket/' . $pos->orders_id . '/' . $pos->pos . '/' . pathinfo('/data/www/new/web/'.$obj['url'], PATHINFO_BASENAME));
        }
    }


    public function getAsRequest(): bool
    {
        $this->loadData();
        return (bool)$this->asRequest;
    }
}
