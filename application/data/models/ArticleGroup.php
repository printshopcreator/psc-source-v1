<?php
/**
 * ArticleGroup Modelclass
 *
 * ArticleGroup Modelsuperclass
 *
 * PHP versions 4 and 5
 *
 * @category   Data
 * @package    Model
 * @subpackage Supermodel
 * @author     Thomas petersomn <tpeterson@printshopcreator.de>
 * @copyright  1997-2009 Thomas Peterson
 * @license    Not Public
 * @version    SVN: $Id: ArticleGroup.php 5180 2010-11-06 03:26:56Z boonkerz $
 * @link       http://www.printshopcreator.de
 */
/**
 * ArticleGroup Modelclass
 *
 * ArticleGroup Modelsuperclass
 *
 * PHP versions 4 and 5
 *
 * @category   Data
 * @package    Model
 * @subpackage Supermodel
 * @author     Thomas petersomn <tpeterson@printshopcreator.de>
 * @copyright  1997-2009 Thomas Peterson
 * @license    Not Public
 * @version    Release: $Id: ArticleGroup.php 5180 2010-11-06 03:26:56Z boonkerz $
 * @link       http://www.printshopcreator.de
 */
class ArticleGroup extends BaseArticleGroup
{

    protected $textArt;
    protected $textFormat;
    protected $einleitung;
    protected $description;
    protected $extraSettings;
    protected $mongoLoaded = false;

    /**
     * (non-PHPdoc)
     * @see Doctrine_Record::preSave()
     */
    public function preSave($obj) {
        TP_Util::clearCache();

        if(Zend_Registry::isRegistered('filecache')) {
            Zend_Registry::get('filecache')->clean(
                Zend_Cache::CLEANING_MODE_MATCHING_TAG,
                array('articlegroups_' . $this->shop_id)
            );
        }
    }

    public function setLangData($var) {
        $this->lang_data = json_encode($var);
    }

    public function getLangData() {
        if ($this->lang_data == "") {
            return array();
        }
        return json_decode($this->lang_data, true);
    }

    public function getTitle() {

        $language = $this->getLangData();
        if (isset($language[Zend_Registry::get('locale')->getLanguage()]) &&
            isset($language[Zend_Registry::get('locale')->getLanguage()]['title']) &&
            $language[Zend_Registry::get('locale')->getLanguage()]['title'] != ""
        ) {
            return $language[Zend_Registry::get('locale')->getLanguage()]['title'];
        }
        return $this->title;
    }

    public function loadData() {
        if($this->mongoLoaded) {
            return true;
        }
        $dbMongo = TP_Mongo::getInstance();
        $obj = $dbMongo->Productgroup->findOne(array('uid' => $this->id));
        if($obj) {
            $this->einleitung = $obj['einleitung'];
            $this->textArt = $obj['textArt'];
            $this->textFormat = $obj['textFormat'];
            $this->description = $obj['description'];
            $this->extraSettings = $obj['extraSettings'];
            $this->mongoLoaded = true;
        }
    }

    /**
     * @return mixed
     */
    public function getTextArt()
    {
        $this->loadData();
        return $this->textArt;
    }

    /**
     * @param mixed $textArt
     */
    public function setTextArt($textArt)
    {
        $this->textArt = $textArt;
    }

    /**
     * @return mixed
     */
    public function getTextFormat()
    {
        $this->loadData();
        return $this->textFormat;
    }

    /**
     * @param mixed $textFormat
     */
    public function setTextFormat($textFormat)
    {
        $this->textFormat = $textFormat;
    }

    /**
     * @return mixed
     */
    public function getEinleitung()
    {
        $this->loadData();
        return $this->einleitung;
    }

    /**
     * @param mixed $einleitung
     */
    public function setEinleitung($einleitung)
    {
        $this->einleitung = $einleitung;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        $this->loadData();
        return $this->description;
    }

    /**
     * @param mixed $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return mixed
     */
    public function getExtraSettings()
    {
        $this->loadData();
        return $this->extraSettings;
    }

    /**
     * @param mixed $extraSettings
     */
    public function setExtraSettings($extraSettings)
    {
        $this->extraSettings = $extraSettings;
    }


}