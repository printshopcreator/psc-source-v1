<?php
/**
 * ArticleGroupArticle Modelclass
 *
 * ArticleGroupArticle Modelsuperclass
 *
 * PHP versions 4 and 5
 *
 * @category   Data
 * @package    Model
 * @subpackage Basemodel
 * @author     Thomas petersomn <tpeterson@printshopcreator.de>
 * @copyright  1997-2009 Thomas Peterson
 * @license    Not Public
 * @version    SVN: $Id: ArticleGroupArticle.php 5162 2010-11-06 01:42:44Z boonkerz $
 * @link       http://www.printshopcreator.de
 */
class ArticleGroupArticle extends BaseArticleGroupArticle
{

    /**
     * Execute before saveing
     * @param Event $event
     * @return boolean
     */
    public function preSave($event) {
        TP_Util::clearCache();
        return true;
    }
}