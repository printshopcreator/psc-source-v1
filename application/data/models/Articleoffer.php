<?php

/**
 * Image Modelclass
 *
 * Image Modelsuperclass
 *
 * PHP versions 4 and 5
 *
 * @category   Data
 * @package    Model
 * @subpackage Supermodel
 * @author     Thomas petersomn <tpeterson@printshopcreator.de>
 * @copyright  1997-2009 Thomas Peterson
 * @license    Not Public
 * @version    SVN: $Id: Api.php 5896 2011-03-09 22:07:32Z boonkerz $
 * @link       http://www.printshopcreator.de
 */

/**
 * Image Modelclass
 *
 * Image Modelsuperclass
 *
 * PHP versions 4 and 5
 *
 * @category   Data
 * @package    Model
 * @subpackage Supermodel
 * @author     Thomas Peterson <tpeterson@printshopcreator.de>
 * @copyright  1997-2009 Thomas Peterson
 * @license    Not Public
 * @version    Release: $Id: Api.php 5896 2011-03-09 22:07:32Z boonkerz $
 * @link       http://www.printshopcreator.de
 */
class Articleoffer extends BaseArticleoffer
{

    /**
     * Empty template method to provide concrete Record classes with the possibility
     * to hook into the saving procedure.
     *
     * @param object $event
     */
    public function preSave($event) {
        if ($this->identifier()) {
            if ($this->created == '') {
                $this->created = date('Y-m-d H:i:s');
            }
        } else {
            $this->created = date('Y-m-d H:i:s');
        }
    }



}
