<?php

/**
 * MotivTheme Modelclass
 *
 * MotivTheme Modelsuperclass
 *
 * PHP versions 4 and 5
 *
 * @category   Data
 * @package    Model
 * @subpackage Supermodel
 * @author     Thomas petersomn <tpeterson@printshopcreator.de>
 * @copyright  1997-2009 Thomas Peterson
 * @license    Not Public
 * @version    SVN: $Id: Clipart.php 5162 2010-11-06 01:42:44Z boonkerz $
 * @link       http://www.printshopcreator.de
 */

/**
 * MotivTheme Modelclass
 *
 * MotivTheme Modelsuperclass
 *
 * PHP versions 4 and 5
 *
 * @category   Data
 * @package    Model
 * @subpackage Supermodel
 * @author     Thomas petersomn <tpeterson@printshopcreator.de>
 * @copyright  1997-2009 Thomas Peterson
 * @license    Not Public
 * @version    Release: $Id: Clipart.php 5162 2010-11-06 01:42:44Z boonkerz $
 * @link       http://www.printshopcreator.de
 */
class Clipart extends BaseClipart
{

    /**
     * Empty template method to provide concrete Record classes with the possibility
     * to hook into the saving procedure.
     */
    public function preSave($event) {

        if ($this->identifier()) {
            $this->updated = date('Y-m-d H:i:s');
            if ($this->created == '') {
                $this->created = date('Y-m-d H:i:s');
            }
        } else {
            $this->created = date('Y-m-d H:i:s');
            $this->updated = date('Y-m-d H:i:s');
        }

        if ($this->uuid == '') {
            $this->uuid = TP_Util::uuid();
        }
        $this->clip_price = str_replace(',', '.', $this->clip_price);
    }

}