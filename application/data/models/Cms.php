<?php

/**
 * Cms Modelclass
 *
 * Cms Modelsuperclass
 *
 * PHP versions 4 and 5
 *
 * @category   Data
 * @package    Model
 * @subpackage Supermodel
 * @author     Thomas petersomn <tpeterson@printshopcreator.de>
 * @copyright  1997-2009 Thomas Peterson
 * @license    Not Public
 * @version    SVN: $Id: Cms.php 6932 2011-10-31 19:37:31Z boonkerz $
 * @link       http://www.printshopcreator.de
 */

/**
 * Cms Modelclass
 *
 * Cms Modelsuperclass
 *
 * PHP versions 4 and 5
 *
 * @category   Data
 * @package    Model
 * @subpackage Supermodel
 * @author     Thomas petersomn <tpeterson@printshopcreator.de>
 * @copyright  1997-2009 Thomas Peterson
 * @license    Not Public
 * @version    Release: $Id: Cms.php 6932 2011-10-31 19:37:31Z boonkerz $
 * @link       http://www.printshopcreator.de
 */
class Cms extends BaseCms
{
    protected $mongoLoaded = false;

    protected $extraSettings;

    public function setUp() {
        parent::setUp();

        $this->hasOne('Cms', array(
            'local' => 'parent',
            'foreign' => 'id'));
    }

    public function loadData()
    {
        if ($this->mongoLoaded) {
            return true;
        }

        $m = TP_Mongo::getInstance();
        $obj = $m->Instance->findOne(array('appId' => "1"));
        if ($obj) {
            $this->extraSettings = $obj['extraSettings'];
            $this->mongoLoaded = true;
        }
    }

    public function postDelete($obj) {
        TP_Util::clearCache();

        Zend_Registry::get('filecache')->clean(
            Zend_Cache::CLEANING_MODE_MATCHING_TAG,
            array('cms_' . $this->shop_id)
        );
    }

    public function getRiakData() {
        return array(
            'title' => $this->title,
            'shop_id' => $this->shop_id,
            'install_id' => $this->install_id,
            'text1' => $this->text1,
            'url' => $this->url
        );
    }

    /**
     * PreSave fired before save or update
     *
     * @return void
     */
    public function preSave($obj) {

        if ($this->menu == "") {
            $this->menu = $this->title;
        }

        Zend_Registry::get('filecache')->clean(
            Zend_Cache::CLEANING_MODE_MATCHING_TAG,
            array('cms_' . $this->shop_id)
        );
    }

    /**
     * PreSave fired before save or update
     *
     * @return void
     */
    public function postSave($obj) {
        TP_Util::clearShop($this->Shop);
    }

    public function getLiveedit($field, $type = 'INPUT') {
        $mode = new Zend_Session_Namespace('adminmode');
        if ($mode->liveedit && $type == 'NO') {
            return $this->$field;
        } elseif ($mode->liveedit && $type == 'INPUT') {
            return '<div class="liveedit" mode="text"table="cms" field="' . $field . '" id="' . $this->id . '">' . $this->$field . '</div>';
        } elseif ($mode->liveedit && $type == 'RTE') {
            return '<div class="liveedit" mode="wysiwyg" table="cms" field="' . $field . '" id="' . $this->id . '">' . $this->$field . '</div>';
        }
        return $this->$field;
    }

    public function getText() {

        $templates = array([]);
        $twig = new \Twig\Environment(new \Twig\Loader\ArrayLoader($templates));
        $template = $twig->createTemplate($this->text1);

        $shop = Zend_Registry::get('shop');
        $shop = Doctrine_Query::create()->select()->from('Shop s')->where('s.id = ?', array($shop['id']))->fetchOne();
        $install = Zend_Registry::get('install');
        if (Zend_Auth::getInstance()->hasIdentity()) {
            $user = Zend_Auth::getInstance()->getIdentity();
        } else {
            $user = false;
        }

        $captcha = $element = new Zend_Form_Element_Captcha('cp', array(
            'label' => "",
            'captcha' => array(
                'captcha' => 'Image',
                'font' => APPLICATION_PATH . '/fonts/verdana.ttf',
                'imgDir' => PUBLIC_PATH . '/temp/thumb/',
                'imgUrl' => '/temp/thumb/',
                'wordLen' => '4'
            ),
        ));

        return $template->render(array(
            'contact' => $user
        , 'install' => $install
        , 'langCode' => Zend_Registry::get('locale')->getLanguage()
        , 'captcha' => $captcha
        , 'shop' => $shop
        , 'url' => 'http://' . $shop->Domain[0]->name . $_SERVER['REQUEST_URI']));

    }

    /**
     * @return mixed
     */
    public function getExtraSettings()
    {
        $this->loadData();
        return $this->extraSettings;
    }

    /**
     * @param mixed $extraSettings
     */
    public function setExtraSettings($extraSettings)
    {
        $this->extraSettings = $extraSettings;
    }

}