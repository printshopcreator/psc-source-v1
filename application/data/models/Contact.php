<?php

/**
 * Contact Modelclass
 *
 * Contact Modelsuperclass
 *
 * PHP versions 4 and 5
 *
 * @category   Data
 * @package    Model
 * @subpackage Supermodel
 * @author     Thomas petersomn <tpeterson@printshopcreator.de>
 * @copyright  1997-2009 Thomas Peterson
 * @license    Not Public
 * @version    SVN: $Id: Contact.php 6611 2011-09-04 12:51:25Z boonkerz $
 * @link       http://www.printshopcreator.de
 */

/**
 * Contact Modelclass
 *
 * Contact Modelsuperclass
 *
 * PHP versions 4 and 5
 *
 * @category   Data
 * @package    Model
 * @subpackage Supermodel
 * @author     Thomas petersomn <tpeterson@printshopcreator.de>
 * @copyright  1997-2009 Thomas Peterson
 * @license    Not Public
 * @version    Release: $Id: Contact.php 6611 2011-09-04 12:51:25Z boonkerz $
 * @link       http://www.printshopcreator.de
 */
class Contact extends BaseContact
{

    protected $kundenNr = '';
    protected $calcValue1 = '';
    protected $calcValue2 = '';
    protected $custom1;
    protected $custom2;
    protected $custom3;
    protected $custom4;
    protected $custom5;
    protected $custom6;
    protected $custom7;
    protected $custom8;
    protected $custom9;
    protected $custom10;
    protected $custom11;
    protected $custom12;
    protected $custom13;
    protected $custom14;
    protected $custom15;
    protected $custom16;
    protected $custom17;
    protected $custom18;
    protected $custom19;
    protected $custom20;
    protected $custom21;
    protected $custom22;
    protected $custom23;
    protected $custom24;
    protected $showOtherOrders = false;
    protected $showOtherOrdersAccountFilter;
    protected $showOtherOrdersAccount;
    protected $extraSettings;
    protected $defaultPayment;
    protected $defaultShipment;

    protected $priceFactor = 1.0;

    protected $mongoLoaded = false;
    /**
     * Empty template method to provide concrete Record classes with the possibility
     * to hook into the saving procedure.
     */
    public function preSave($event) {

        if ($this->identifier()) {
            $this->updated = date('Y-m-d H:i:s');
            if ($this->created == '') {
                $this->created = date('Y-m-d H:i:s');
            }
        } else {
            $this->created = date('Y-m-d H:i:s');
            $this->updated = date('Y-m-d H:i:s');
        }

        if ($this->uuid == '') {
            $this->uuid = TP_Util::uuid();
        }
        TP_Util::clearCache();
    }

    public function loadData() {
        if($this->mongoLoaded) {
            return true;
        }
        $dbMongo = TP_Mongo::getInstance();
        $obj = $dbMongo->Contact->findOne(array('uid' => $this->id));
        if($obj) {
            $this->kundenNr = $obj['kundenNr'];
            $this->calcValue1 = $obj['calcValue1'];
            $this->calcValue2 = $obj['calcValue2'];
            $this->custom1 = $obj['custom1'];
            $this->custom2 = $obj['custom2'];
            $this->custom3 = $obj['custom3'];
            $this->custom4 = $obj['custom4'];
            $this->custom5 = $obj['custom5'];
            $this->custom6 = $obj['custom6'];
            $this->custom7 = $obj['custom7'];
            $this->custom8 = $obj['custom8'];
            $this->custom9 = $obj['custom9'];
            $this->custom10 = $obj['custom10'];
            $this->custom11 = $obj['custom11'];
            $this->custom12 = $obj['custom12'];
            $this->custom13 = $obj['custom13'];
            $this->custom14 = $obj['custom14'];
            $this->custom15 = $obj['custom15'];
            $this->custom16 = $obj['custom16'];
            $this->custom17 = $obj['custom17'];
            $this->custom18 = $obj['custom18'];
            $this->custom19 = $obj['custom19'];
            $this->custom20 = $obj['custom20'];
            $this->custom21 = $obj['custom21'];
            $this->custom22 = $obj['custom22'];
            $this->custom23 = $obj['custom23'];
            $this->custom24 = $obj['custom24'];
            $this->showOtherOrders = $obj['showOtherOrders'];
            $this->showOtherOrdersAccountFilter = $obj['showOtherOrdersAccountFilter'];
            $this->showOtherOrdersAccount = $obj['showOtherOrdersAccount'];
            $this->extraSettings = $obj['extraSettings'];
            $this->priceFactor = $obj['priceFactor'];
            $this->defaultPayment = $obj['defaultPayment'];
            $this->defaultShipment = $obj['defaultShipment'];

            if(!$this->priceFactor) {
                $this->priceFactor = 1.0;
            }
            $this->mongoLoaded = true;
        }
    }

    public function getArray() {
        return array(
            'uid' => $this->id,
            'kundenNr' => $this->kundenNr,
            'calcValue1' => $this->calcValue1,
            'calcValue2' => $this->calcValue2,
            'custom1' => $this->custom1,
            'custom2' => $this->custom2,
            'custom3' => $this->custom3,
            'custom4' => $this->custom4,
            'custom5' => $this->custom5,
            'custom6' => $this->custom6,
            'custom7' => $this->custom7,
            'custom8' => $this->custom8,
            'custom9' => $this->custom9,
            'custom10' => $this->custom10,
            'custom11' => $this->custom11,
            'custom12' => $this->custom12,
            'custom13' => $this->custom13,
            'custom14' => $this->custom14,
            'custom15' => $this->custom15,
            'custom16' => $this->custom16,
            'custom17' => $this->custom17,
            'custom18' => $this->custom18,
            'custom19' => $this->custom19,
            'custom20' => $this->custom20,
            'custom21' => $this->custom21,
            'custom22' => $this->custom22,
            'custom23' => $this->custom23,
            'custom24' => $this->custom24,
            'showOtherOrders' => $this->showOtherOrders,
            'showOtherOrdersAccountFilter' => $this->showOtherOrdersAccountFilter,
            'showOtherOrdersAccount' => $this->showOtherOrdersAccount,
            'extraSettings' => $this->extraSettings,
            'defaultPayment' => $this->defaultPayment,
            'defaultShipment' => $this->defaultShipment
        );
    }

    public function getState() {

        $rows = $this->Adminusersettings->toArray();
        $temp = array();
        foreach ($rows as $row) {
            $temp[] = array('name' => $row['name'], 'value' => $row['data']);
        }

        return Zend_Json::encode($temp);
    }

    public function getAnrede() {

        if ($this->self_anrede == 1) {
            return 'Herr';
        } elseif ($this->self_anrede == 2) {
            return 'Frau';
        } elseif ($this->self_anrede == 3) {
            return 'Firma';
        } elseif ($this->self_anrede == 4) {
            return 'Herr Dr.';
        } elseif ($this->self_anrede == 5) {
            return 'Frau Dr.';
        } elseif ($this->self_anrede == 6) {
            return 'Herr Prof.';
        } elseif ($this->self_anrede == 7) {
            return 'Frau Prof.';
        } elseif ($this->self_anrede == 8) {
            return 'Herr Prof. Dr.';
        } elseif ($this->self_anrede == 9) {
            return 'Frau Prof. Dr.';
        } elseif ($this->self_anrede == 10) {
            return 'Schwester';
        }
    }

    public function getInvoiceAddress() {

        if ($this->Account->use_account_as_invoice) {
            return $this->Account;
        }

        if ($this->standart_invoice == 0) {
            return $this;
        }

        return Doctrine_Query::create()->select('o.firstname, o.lastname, o.street, o.house_number, o.zip, o.city, o.phone, o.email')
            ->from('ContactAddress o')->where('o.id = ?', array($this->standart_invoice))->fetchOne();
    }

    public function getDeliveryAddress() {

        if ($this->standart_delivery == 0) {
            return $this;
        }

        return Doctrine_Query::create()->select('o.firstname, o.lastname, o.street, o.house_number, o.zip, o.city, o.phone, o.email')
            ->from('ContactAddress o')->where('o.id = ?', array($this->standart_delivery))->fetchOne();
    }

    public function getSenderAddress() {

        if ($this->standart_sender == 0) {
            return $this;
        }

        return Doctrine_Query::create()->select('o.firstname, o.lastname, o.street, o.house_number, o.zip, o.city, o.phone, o.email')
            ->from('ContactAddress o')->where('o.id = ?', array($this->standart_sender))->fetchOne();
    }

    public function getFirstname() {
        return $this->self_firstname;
    }

    public function getCompany() {
        return ($this->self_department);
    }

    public function getLastname() {
        return $this->self_lastname;
    }

    public function getFunction() {
        return $this->self_function;
    }

    public function getDepartment() {
        return htmlspecialchars($this->self_department);
    }

    public function getStreet() {
        return $this->self_street;
    }

    public function getHouseNumber() {
        return $this->self_house_number;
    }

    public function getZip() {
        return $this->self_zip;
    }

    public function getCity() {
        return $this->self_city;
    }

    public function getPhone() {
        return $this->self_phone;
    }

    public function getMail() {
        return $this->self_email;
    }

    public function getCountry() {
        return $this->self_country;
    }

    /**
     * @return string
     */
    public function getKundenNr()
    {
        $this->loadData();
        return $this->kundenNr;
    }

    /**
     * @param string $kundenNr
     */
    public function setKundenNr($kundenNr)
    {
        $this->kundenNr = $kundenNr;
    }

    public function setDefaultPayment($payment)
    {
        $this->defaultPayment = $payment;
    }

    public function getDefaultPayment()
    {
        $this->loadData();
        return $this->defaultPayment;
    }

    public function setDefaultShipment($shipment)
    {
        $this->defaultShipment = $shipment;
    }

    public function getDefaultShipment()
    {
        $this->loadData();
        return $this->defaultShipment;
    }

    public function saveMongo()
    {
        $dbMongo = TP_Mongo::getInstance();
        $obj = $dbMongo->Contact->findOne(array('uid' => $this->id));
        if($obj) {
            $dbMongo->Contact->updateOne(array('uid' => $this->id), [ '$set' =>  $this->getArray()]);
        }else{
            $dbMongo->Contact->insertOne($this->getArray());
        }
    }

    /**
     * @return string
     */
    public function getCalcValue1()
    {
        $this->loadData();
        return $this->calcValue1;
    }

    /**
     * @param string $calcValue1
     */
    public function setCalcValue1($calcValue1)
    {
        $this->calcValue1 = $calcValue1;
    }

    /**
     * @return string
     */
    public function getCalcValue2()
    {
        $this->loadData();
        return $this->calcValue2;
    }

    /**
     * @param string $calcValue2
     */
    public function setCalcValue2($calcValue2)
    {
        $this->calcValue2 = $calcValue2;
    }

    /**
     * @return mixed
     */
    public function getCustom1()
    {
        $this->loadData();
        return $this->custom1;
    }

    /**
     * @param mixed $custom1
     */
    public function setCustom1($custom1)
    {
        $this->custom1 = $custom1;
    }

    /**
     * @return mixed
     */
    public function getCustom2()
    {
        $this->loadData();
        return $this->custom2;
    }

    /**
     * @param mixed $custom2
     */
    public function setCustom2($custom2)
    {
        $this->custom2 = $custom2;
    }

    /**
     * @return mixed
     */
    public function getCustom3()
    {
        $this->loadData();
        return $this->custom3;
    }

    /**
     * @param mixed $custom3
     */
    public function setCustom3($custom3)
    {
        $this->custom3 = $custom3;
    }

    /**
     * @return mixed
     */
    public function getCustom4()
    {
        $this->loadData();
        return $this->custom4;
    }

    /**
     * @param mixed $custom4
     */
    public function setCustom4($custom4)
    {
        $this->custom4 = $custom4;
    }

    /**
     * @return mixed
     */
    public function getCustom5()
    {
        $this->loadData();
        return $this->custom5;
    }

    /**
     * @param mixed $custom5
     */
    public function setCustom5($custom5)
    {
        $this->custom5 = $custom5;
    }

    /**
     * @return mixed
     */
    public function getCustom6()
    {
        $this->loadData();
        return $this->custom6;
    }

    /**
     * @param mixed $custom6
     */
    public function setCustom6($custom6)
    {
        $this->custom6 = $custom6;
    }

    /**
     * @return mixed
     */
    public function getCustom7()
    {
        $this->loadData();
        return $this->custom7;
    }

    /**
     * @param mixed $custom7
     */
    public function setCustom7($custom7)
    {
        $this->custom7 = $custom7;
    }

    /**
     * @return mixed
     */
    public function getCustom8()
    {
        $this->loadData();
        return $this->custom8;
    }

    /**
     * @param mixed $custom8
     */
    public function setCustom8($custom8)
    {
        $this->custom8 = $custom8;
    }

    /**
     * @return mixed
     */
    public function getCustom9()
    {
        $this->loadData();
        return $this->custom9;
    }

    /**
     * @param mixed $custom9
     */
    public function setCustom9($custom9)
    {
        $this->custom9 = $custom9;
    }

    /**
     * @return mixed
     */
    public function getCustom10()
    {
        $this->loadData();
        return $this->custom10;
    }

    /**
     * @param mixed $custom10
     */
    public function setCustom10($custom10)
    {
        $this->custom10 = $custom10;
    }

    /**
     * @return mixed
     */
    public function getCustom11()
    {
        $this->loadData();
        return $this->custom11;
    }

    /**
     * @param mixed $custom11
     */
    public function setCustom11($custom11)
    {
        $this->custom11 = $custom11;
    }

    /**
     * @return mixed
     */
    public function getCustom12()
    {
        $this->loadData();
        return $this->custom12;
    }

    /**
     * @param mixed $custom12
     */
    public function setCustom12($custom12)
    {
        $this->custom12 = $custom12;
    }

    /**
     * @return mixed
     */
    public function getCustom13()
    {
        $this->loadData();
        return $this->custom13;
    }

    /**
     * @param mixed $custom13
     */
    public function setCustom13($custom13)
    {
        $this->custom13 = $custom13;
    }

    /**
     * @return mixed
     */
    public function getCustom14()
    {
        $this->loadData();
        return $this->custom14;
    }

    /**
     * @param mixed $custom14
     */
    public function setCustom14($custom14)
    {
        $this->custom14 = $custom14;
    }

    /**
     * @return mixed
     */
    public function getCustom15()
    {
        $this->loadData();
        return $this->custom15;
    }

    /**
     * @param mixed $custom15
     */
    public function setCustom15($custom15)
    {
        $this->custom15 = $custom15;
    }

    /**
     * @return mixed
     */
    public function getCustom16()
    {
        $this->loadData();
        return $this->custom16;
    }

    /**
     * @param mixed $custom16
     */
    public function setCustom16($custom16)
    {
        $this->custom16 = $custom16;
    }

    /**
     * @return mixed
     */
    public function getCustom17()
    {
        $this->loadData();
        return $this->custom17;
    }

    /**
     * @param mixed $custom17
     */
    public function setCustom17($custom17)
    {
        $this->custom17 = $custom17;
    }

    /**
     * @return mixed
     */
    public function getCustom18()
    {
        $this->loadData();
        return $this->custom18;
    }

    /**
     * @param mixed $custom18
     */
    public function setCustom18($custom18)
    {
        $this->custom18 = $custom18;
    }

    /**
     * @return mixed
     */
    public function getCustom19()
    {
        $this->loadData();
        return $this->custom19;
    }

    /**
     * @param mixed $custom19
     */
    public function setCustom19($custom19)
    {
        $this->custom19 = $custom19;
    }

    /**
     * @return mixed
     */
    public function getCustom20()
    {
        $this->loadData();
        return $this->custom20;
    }

    /**
     * @param mixed $custom20
     */
    public function setCustom20($custom20)
    {
        $this->custom20 = $custom20;
    }

    /**
     * @return mixed
     */
    public function getCustom21()
    {
        $this->loadData();
        return $this->custom21;
    }

    /**
     * @param mixed $custom21
     */
    public function setCustom21($custom21)
    {
        $this->custom21 = $custom21;
    }

    /**
     * @return mixed
     */
    public function getCustom22()
    {
        $this->loadData();
        return $this->custom22;
    }

    /**
     * @param mixed $custom22
     */
    public function setCustom22($custom22)
    {
        $this->custom22 = $custom22;
    }

    /**
     * @return mixed
     */
    public function getCustom23()
    {
        $this->loadData();
        return $this->custom23;
    }

    /**
     * @param mixed $custom23
     */
    public function setCustom23($custom23)
    {
        $this->custom23 = $custom23;
    }

    /**
     * @return mixed
     */
    public function getCustom24()
    {
        $this->loadData();
        return $this->custom24;
    }

    /**
     * @param mixed $custom24
     */
    public function setCustom24($custom24)
    {
        $this->custom24 = $custom24;
    }

    /**
     * @return bool
     */
    public function isShowOtherOrders()
    {
        $this->loadData();
        return $this->showOtherOrders;
    }

    /**
     * @param bool $showOtherOrders
     */
    public function setShowOtherOrders($showOtherOrders)
    {
        $this->showOtherOrders = $showOtherOrders;
    }

    /**
     * @return mixed
     */
    public function getShowOtherOrdersAccountFilter()
    {
        $this->loadData();
        return $this->showOtherOrdersAccountFilter;
    }

    /**
     * @param mixed $showOtherOrdersAccountFilter
     */
    public function setShowOtherOrdersAccountFilter($showOtherOrdersAccountFilter)
    {
        $this->showOtherOrdersAccountFilter = $showOtherOrdersAccountFilter;
    }

    /**
     * @return mixed
     */
    public function getShowOtherOrdersAccount()
    {
        $this->loadData();
        return $this->showOtherOrdersAccount;
    }

    /**
     * @param mixed $showOtherOrdersAccount
     */
    public function setShowOtherOrdersAccount($showOtherOrdersAccount)
    {
        $this->showOtherOrdersAccount = $showOtherOrdersAccount;
    }

    /**
     * @return mixed
     */
    public function getExtraSettings()
    {
        $this->loadData();
        return $this->extraSettings;
    }

    /**
     * @param mixed $extraSettings
     */
    public function setExtraSettings($extraSettings)
    {
        $this->extraSettings = $extraSettings;
    }

    /**
     * @return float
     */
    public function getPriceFactor(): float
    {
        $this->loadData();
        return $this->priceFactor;
    }

    /**
     * @param float $priceFactor
     */
    public function setPriceFactor(float $priceFactor): void
    {
        $this->priceFactor = $priceFactor;
    }


}
