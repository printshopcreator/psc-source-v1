<?php

/**
 * Contact Modelclass
 *
 * Contact Modelsuperclass
 *
 * PHP versions 4 and 5
 *
 * @category   Data
 * @package    Model
 * @subpackage Supermodel
 * @author     Thomas petersomn <tpeterson@printshopcreator.de>
 * @copyright  1997-2009 Thomas Peterson
 * @license    Not Public
 * @version    SVN: $Id: ContactAddress.php 6607 2011-09-04 12:29:11Z boonkerz $
 * @link       http://www.printshopcreator.de
 */

/**
 * Contact Modelclass
 *
 * Contact Modelsuperclass
 *
 * PHP versions 4 and 5
 *
 * @category   Data
 * @package    Model
 * @subpackage Supermodel
 * @author     Thomas petersomn <tpeterson@printshopcreator.de>
 * @copyright  1997-2009 Thomas Peterson
 * @license    Not Public
 * @version    Release: $Id: ContactAddress.php 6607 2011-09-04 12:29:11Z boonkerz $
 * @link       http://www.printshopcreator.de
 */

class ContactAddress extends BaseContactAddress
{

    protected $kundenNr = '';
    protected $homepage = '';
    protected $mongoLoaded = false;

    /**
     * Empty template method to provide concrete Record classes with the possibility
     * to hook into the saving procedure.
     */
    public function preSave($event) {

        if ($this->identifier()) {
            $this->updated = date('Y-m-d H:i:s');
            if ($this->created == '') {
                $this->created = date('Y-m-d H:i:s');
            }
        } else {
            $this->created = date('Y-m-d H:i:s');
            $this->updated = date('Y-m-d H:i:s');
        }

        if ($this->uuid == '') {
            $this->uuid = TP_Util::uuid();
        }

        TP_Util::clearCache();
    }

    public function getAnrede() {

        if ($this->anrede == 1) {
            return 'Herr';
        } elseif ($this->anrede == 2) {
            return 'Frau';
        } elseif ($this->anrede == 3) {
            return 'Firma';
        } elseif ($this->anrede == 4) {
            return 'Herr Dr.';
        } elseif ($this->anrede == 5) {
            return 'Frau Dr.';
        } elseif ($this->anrede == 6) {
            return 'Herr Prof.';
        } elseif ($this->anrede == 7) {
            return 'Frau Prof.';
        } elseif ($this->anrede == 8) {
            return 'Herr Prof. Dr.';
        } elseif ($this->anrede == 9) {
            return 'Frau Prof. Dr.';
        } elseif ($this->anrede == 10) {
            return 'Schwester';
        }


    }

    public function getAddressString() {

        $str = '';
        if ($this->company != "") {
            $str .= $this->company . ',';
        }
        if ($this->anrede != "") {
            $str .= ' ' . $this->getAnrede();
        }
        if ($this->firstname != "") {
            $str .= ' ' . $this->firstname;
        }
        if ($this->lastname != "") {
            $str .= ' ' . $this->lastname;
        }
        if ($this->street != "") {
            $str .= ', ' . $this->street;
        }
        if ($this->house_number != "") {
            $str .= ' ' . $this->house_number;
        }
        if ($this->zip != "") {
            $str .= ', ' . $this->zip;
        }
        if ($this->city != "") {
            $str .= ' ' . $this->city;
        }
        if ($this->country != "") {
            $str .= ', ' . $this->country;
        }
        return $str;
    }

    public function getAddressStringWithBreak() {

        $str = '';
        if ($this->company != "") {
            $str .= $this->company . '<br/>';
        }
        if ($this->anrede != "") {
            $str .= ' ' . $this->getAnrede();
        }
        if ($this->firstname != "") {
            $str .= ' ' . $this->firstname;
        }
        if ($this->lastname != "") {
            $str .= ' ' . $this->lastname . '<br/>';
        }
        if ($this->street != "") {
            $str .= $this->street;
        }
        if ($this->house_number != "") {
            $str .= ' ' . $this->house_number . '<br/>';
        }
        if ($this->zip != "") {
            $str .= $this->zip;
        }
        if ($this->city != "") {
            $str .= ' ' . $this->city;
        }

        return $str;
    }

    public function getFirstname() {
        return $this->firstname;
    }

    public function getCompany() {
        return ($this->company);
    }

    public function getCompany2() {
        return ($this->company2);
    }

    public function getCountry() {
        return $this->country;
    }

    public function getLastname() {
        return $this->lastname;
    }

    public function getStreet() {
        return $this->street;
    }

    public function getHouseNumber() {
        return $this->house_number;
    }

    public function getZip() {
        return $this->zip;
    }

    public function getCity() {
        return $this->city;
    }

    public function getPhone() {
        return $this->phone;
    }

    public function getMail() {
        return $this->email;
    }

    public function getKostenstellung() {
        return $this->kostenstellung;
    }

    public function getAbteilung() {
        return $this->abteilung;
    }

    public function getPosition() {
        return $this->position;
    }

    public function getUstId() {
        return $this->ustid;
    }

    public function getFax() {
        return $this->fax;
    }

    public function getZusatz1() {
        return $this->zusatz1;
    }

    public function getZusatz2() {
        return $this->zusatz2;
    }

    public function loadData() {
        if($this->mongoLoaded) {
            return true;
        }
        $dbMongo = TP_Mongo::getInstance();
        $obj = $dbMongo->ContactAddress->findOne(array('uid' => $this->id));
        if($obj) {
            $this->kundenNr = $obj['kundenNr'];
            $this->homepage = $obj['homepage'];
            $this->mongoLoaded = true;
        }
    }

    public function getArray() {
        return array(
            'uid' => $this->id,
            'kundenNr' => $this->kundenNr,
            'homepage' => $this->homepage
        );
    }

    public function getOrderSaveArray() {

        $arr = $this->toArray();
        $arr['kundenNr'] = $this->getKundenNr();
        $arr['homepage'] = $this->getHomepage();
        $arr['houseNumber'] = $arr['house_number'];
        $arr['_doctrine_class_name']= "PSC\\Shop\\EntityBundle\\Document\\Embed\\ContactAddress";

        return $arr;
    }

    /**
     * @return string
     */
    public function getKundenNr()
    {
        $this->loadData();
        return $this->kundenNr;
    }

    /**
     * @param string $kundenNr
     */
    public function setKundenNr($kundenNr)
    {
        $this->kundenNr = $kundenNr;
    }

    public function getHomepage()
    {
        $this->loadData();
        return $this->homepage;
    }

    public function setHomepage($value)
    {
        $this->homepage = $value;
    }

    public function saveMongo() {
        $dbMongo = TP_Mongo::getInstance();
        $obj = $dbMongo->ContactAddress->findOne(array('uid' => $this->id));
        if($obj) {
            $dbMongo->ContactAddress->updateOne(array('uid' => $this->id), [ '$set' =>  $this->getArray()]);
        }else{
            $dbMongo->ContactAddress->insertOne($this->getArray());
        }

    }
}
