<?php

/**
 * Install Modelclass
 *
 * Install Modelsuperclass
 *
 * PHP versions 4 and 5
 *
 * @category   Data
 * @package    Model
 * @subpackage Supermodel
 * @author     Thomas petersomn <tpeterson@printshopcreator.de>
 * @copyright  1997-2009 Thomas Peterson
 * @license    Not Public
 * @version    SVN: $Id: Install.php 7278 2012-02-09 16:02:00Z boonkerz $
 * @link       http://www.printshopcreator.de
 */

/**
 * Install Modelclass
 *
 * Install Modelsuperclass
 *
 * PHP versions 4 and 5
 *
 * @category   Data
 * @package    Model
 * @subpackage Supermodel
 * @author     Thomas petersomn <tpeterson@printshopcreator.de>
 * @copyright  1997-2009 Thomas Peterson
 * @license    Not Public
 * @version    Release: $Id: Install.php 7278 2012-02-09 16:02:00Z boonkerz $
 * @link       http://www.printshopcreator.de
 */

class Install extends BaseInstall
{
    protected $mongoLoaded = false;

    protected $extraSettings;
    protected $maintenanceMode = false;
    protected $maintenanceTitle;
    protected $maintenanceText;

    public function loadData()
    {
        if ($this->mongoLoaded) {
            return true;
        }

        $m = TP_Mongo::getInstance();
        $obj = $m->Instance->findOne(array('appId' => "1"));
        if ($obj) {
            $this->extraSettings = $obj['extraSettings'];
            $this->maintenanceMode = $obj['maintenanceMode'];
            $this->maintenanceText = $obj['maintenanceText'];
            $this->maintenanceTitle = $obj['maintenanceTitle'];
            $this->mongoLoaded = true;
        }
    }
    public function getMotivAgb() {

        $loader = new Twig_Loader_String();
        $twig = new Twig_Environment($loader);
        $template = $twig->loadTemplate($this->motiv_agb);

        $shop = Zend_Registry::get('shop');
        $shop = Doctrine_Query::create()->select()->from('Shop s')->where('s.id = ?', array($shop['id']))->fetchOne();
        $install = Zend_Registry::get('install');
        if (Zend_Auth::getInstance()->hasIdentity()) {
            $user = Zend_Auth::getInstance()->getIdentity();
        } else {
            $user = false;
        }

        return $template->render(array(
            'contact' => $user
        , 'install' => $install
        , 'shop' => $shop
        , 'url' => 'http://' . $shop->Domain[0]->name . $_SERVER['REQUEST_URI']));

    }

    public function getAgb() {

        $loader = new Twig_Loader_String();
        $twig = new Twig_Environment($loader);
        $template = $twig->loadTemplate($this->agb);

        $shop = Zend_Registry::get('shop');
        $shop = Doctrine_Query::create()->select()->from('Shop s')->where('s.id = ?', array($shop['id']))->fetchOne();
        $install = Zend_Registry::get('install');
        if (Zend_Auth::getInstance()->hasIdentity()) {
            $user = Zend_Auth::getInstance()->getIdentity();
        } else {
            $user = false;
        }

        return $template->render(array(
            'contact' => $user
        , 'install' => $install
        , 'captcha' => $captcha
        , 'shop' => $shop
        , 'url' => 'http://' . $shop->Domain[0]->name . $_SERVER['REQUEST_URI']));

    }

    public function getImpress() {

        $loader = new Twig_Loader_String();
        $twig = new Twig_Environment($loader);
        $template = $twig->loadTemplate($this->impress_themenshop);

        $shop = Zend_Registry::get('shop');
        $shop = Doctrine_Query::create()->select()->from('Shop s')->where('s.id = ?', array($shop['id']))->fetchOne();
        $install = Zend_Registry::get('install');
        if (Zend_Auth::getInstance()->hasIdentity()) {
            $user = Zend_Auth::getInstance()->getIdentity();
        } else {
            $user = false;
        }

        return $template->render(array(
            'contact' => $user
        , 'install' => $install
        , 'captcha' => $captcha
        , 'shop' => $shop
        , 'url' => 'http://' . $shop->Domain[0]->name . $_SERVER['REQUEST_URI']));

    }

    /**
     * @return mixed
     */
    public function getExtraSettings()
    {
        $this->loadData();
        return $this->extraSettings;
    }

    /**
     * @param mixed $extraSettings
     */
    public function setExtraSettings($extraSettings)
    {
        $this->extraSettings = $extraSettings;
    }

    /**
     * @return mixed
     */
    public function getMaintenanceText()
    {
        $this->loadData();
        return $this->maintenanceText;
    }

    /**
     * @param mixed $maintenanceText
     */
    public function setMaintenanceText($maintenanceText): void
    {
        $this->maintenanceText = $maintenanceText;
    }

    /**
     * @return mixed
     */
    public function getMaintenanceTitle()
    {
        $this->loadData();
        return $this->maintenanceTitle;
    }

    /**
     * @param mixed $maintenanceTitle
     */
    public function setMaintenanceTitle($maintenanceTitle): void
    {
        $this->maintenanceTitle = $maintenanceTitle;
    }

    /**
     * @return bool
     */
    public function isMaintenanceMode(): bool
    {
        $this->loadData();
        return (bool)$this->maintenanceMode;
    }

    /**
     * @param bool $maintenanceMode
     */
    public function setMaintenanceMode(bool $maintenanceMode): void
    {
        $this->maintenanceMode = $maintenanceMode;
    }
}