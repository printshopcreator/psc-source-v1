<?php

/**
 * Article Modelclass
 *
 * Article Modelsuperclass
 *
 * PHP versions 4 and 5
 *
 * @category   Data
 * @package    Model
 * @subpackage Supermodel
 * @author     Thomas petersomn <tpeterson@printshopcreator.de>
 * @copyright  1997-2009 Thomas Peterson
 * @license    Not Public
 * @version    SVN: $Id: LayouterSession.php 6981 2011-11-15 18:33:31Z boonkerz $
 * @link       http://www.printshopcreator.de
 */

/**
 * Article Modelclass
 *
 * Article Modelsuperclass
 *
 * PHP versions 4 and 5
 *
 * @category   Data
 * @package    Model
 * @subpackage Supermodel
 * @author     Thomas Peterson <tpeterson@printshopcreator.de>
 * @copyright  1997-2009 Thomas Peterson
 * @license    Not Public
 * @version    Release: $Id: LayouterSession.php 6981 2011-11-15 18:33:31Z boonkerz $
 * @link       http://www.printshopcreator.de
 */
class LayouterDesignData extends BaseLayouterDesignData
{



}