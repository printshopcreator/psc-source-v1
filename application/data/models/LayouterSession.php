<?php

/**
 * Article Modelclass
 *
 * Article Modelsuperclass
 *
 * PHP versions 4 and 5
 *
 * @category   Data
 * @package    Model
 * @subpackage Supermodel
 * @author     Thomas petersomn <tpeterson@printshopcreator.de>
 * @copyright  1997-2009 Thomas Peterson
 * @license    Not Public
 * @version    SVN: $Id: LayouterSession.php 6981 2011-11-15 18:33:31Z boonkerz $
 * @link       http://www.printshopcreator.de
 */

/**
 * Article Modelclass
 *
 * Article Modelsuperclass
 *
 * PHP versions 4 and 5
 *
 * @category   Data
 * @package    Model
 * @subpackage Supermodel
 * @author     Thomas Peterson <tpeterson@printshopcreator.de>
 * @copyright  1997-2009 Thomas Peterson
 * @license    Not Public
 * @version    Release: $Id: LayouterSession.php 6981 2011-11-15 18:33:31Z boonkerz $
 * @link       http://www.printshopcreator.de
 */
class LayouterSession extends BaseLayouterSession
{

    protected $ref = '';
    protected $kst = '';
    protected $templatePrintContactId = 0;
    protected $mongoLoaded = false;

    /**
     * Empty template method to provide concrete Record classes with the possibility
     * to hook into the saving procedure.
     *
     * @param object $event
     */
    public function preSave($event) {

        if ($this->identifier()) {
            $this->updated = date('Y-m-d H:i:s');
            if ($this->created == '') {
                $this->created = date('Y-m-d H:i:s');
            }
        } else {
            $this->created = date('Y-m-d H:i:s');
            $this->updated = date('Y-m-d H:i:s');
        }

        if ($this->uuid == '') {
            $this->uuid = TP_Util::uuid();
        }
    }

    public function copy($deep = false) {

        $item = parent::copy($deep);

        $item->setArticleId(TP_Util::uuid());
        $item->save();
        return $item;

    }

    function getUpdated() {
        return $this->updated;
    }

    public function copyFromGuest($id) {

        if (!file_exists(APPLICATION_PATH . '/../market/templateprint/user/' . $id)) {
            mkdir(APPLICATION_PATH . '/../market/templateprint/user/' . $id);
        }
        if (file_exists(APPLICATION_PATH . '/../market/templateprint/guest/' . $id . '/preview.pdf')) {
            copy(APPLICATION_PATH . '/../market/templateprint/guest/' . $id . '/preview.pdf', APPLICATION_PATH . '/../market/templateprint/user/' . $id . '/preview.pdf');
        }
        if (file_exists(APPLICATION_PATH . '/../market/templateprint/guest/' . $id . '/product.zip')) {
            copy(APPLICATION_PATH . '/../market/templateprint/guest/' . $id . '/product.zip', APPLICATION_PATH . '/../market/templateprint/user/' . $id . '/product.zip');
        }
        if (file_exists(APPLICATION_PATH . '/../market/templateprint/guest/' . $id . '/final.pdf')) {
            copy(APPLICATION_PATH . '/../market/templateprint/guest/' . $id . '/final.pdf', APPLICATION_PATH . '/../market/templateprint/user/' . $id . '/final.pdf');
        }

        $this->setTemplatePrintId($id);
    }

    public function copyToBasket(Orders $order, Orderspos $pos, $id) {

        if (!file_exists(APPLICATION_PATH . '/../market/templateprint/basket/' . $order->id . '/' . $pos->pos)) {
            mkdir(APPLICATION_PATH . '/../market/templateprint/basket/' . $order->id . '/' . $pos->pos, 0777, true);
        }
        if (file_exists(APPLICATION_PATH . '/../market/templateprint/user/' . $id . '/preview.pdf')) {
            copy(APPLICATION_PATH . '/../market/templateprint/user/' . $id . '/preview.pdf', APPLICATION_PATH . '/../market/templateprint/basket/' . $order->id . '/' . $pos->pos . '/preview.pdf');
        }
        if (file_exists(APPLICATION_PATH . '/../market/templateprint/user/' . $id . '/product.zip')) {
            copy(APPLICATION_PATH . '/../market/templateprint/user/' . $id . '/product.zip', APPLICATION_PATH . '/../market/templateprint/basket/' . $order->id . '/' . $pos->pos . '/product.zip');
        }
        if (file_exists(APPLICATION_PATH . '/../market/templateprint/user/' . $id . '/result.zip')) {
            copy(APPLICATION_PATH . '/../market/templateprint/user/' . $id . '/product.zip', APPLICATION_PATH . '/../market/templateprint/basket/' . $order->id . '/' . $pos->pos . '/product.zip');
        }
        if (file_exists(APPLICATION_PATH . '/../market/templateprint/user/' . $id . '/final.pdf')) {
            copy(APPLICATION_PATH . '/../market/templateprint/user/' . $id . '/final.pdf', APPLICATION_PATH . '/../market/templateprint/basket/' . $order->id . '/' . $pos->pos . '/final.pdf');
        }

        $this->setTemplatePrintId($id);
    }

    public function copyFromBasket(Orders $order, Orderspos $pos, $id) {

        if (!file_exists(APPLICATION_PATH . '/../market/templateprint/user/' . $id)) {
            mkdir(APPLICATION_PATH . '/../market/templateprint/user/' . $id, 0777, true);
        }
        copy(APPLICATION_PATH . '/../market/templateprint/basket/' . $order->id . '/' . $pos->pos . '/preview.pdf', APPLICATION_PATH . '/../market/templateprint/user/' . $id . '/preview.pdf');
        copy(APPLICATION_PATH . '/../market/templateprint/basket/' . $order->id . '/' . $pos->pos . '/product.zip', APPLICATION_PATH . '/../market/templateprint/user/' . $id . '/product.zip');
        copy(APPLICATION_PATH . '/../market/templateprint/basket/' . $order->id . '/' . $pos->pos . '/final.pdf', APPLICATION_PATH . '/../market/templateprint/user/' . $id . '/final.pdf');

        $this->setTemplatePrintId($id);
        $this->save();
    }

    public function copyToBasketStepLayouter(Orders $order, Orderspos $pos, $id) {
        if (!file_exists(APPLICATION_PATH . '/../market/steplayouter/basket/' . $order->id . '/' . $pos->pos)) {
            mkdir(APPLICATION_PATH . '/../market/steplayouter/basket/' . $order->id . '/' . $pos->pos, 0777, true);
        }
        if (file_exists(PUBLIC_PATH . '/steplayouter/temp/' . $id . '.pdf')) {
            copy(PUBLIC_PATH . '/steplayouter/temp/' . $id . '.pdf', APPLICATION_PATH . '/../market/steplayouter/basket/' . $order->id . '/' . $pos->pos . '/print.pdf');
        }
    }

    public function setIsEdit() {

    }

    public function getIsEdit() {

    }

    public function getArticleId() {
        return $this->uuid;
    }

    public function setArticleId($id) {
        $this->uuid = $id;
    }

    public function getOrgArticleId() {
        return $this->org_article_id;
    }

    public function setTemplatePrintId($var) {
        $this->template_print_id = $var;
        $this->save();
    }

    public function getTemplatePrintId() {
        return $this->template_print_id;
    }

    public function setOrgArticleId($id) {
        $this->org_article_id = $id;
        $this->save();
    }

    public function getPageObjects() {
        return $this->xmlpageobjects;
    }

    public function setPageObjects($xml) {
        $this->xmlpageobjects = $xml;
        $this->save();
    }

    public function getPageTemplates() {
        return $this->xmlpagetemplates;
    }

    public function setPageTemplates($xml) {
        $this->xmlpagetemplates = $xml;
        $this->save();
    }

    public function getConfigXml() {
        return $this->xmlconfig;
    }

    public function setConfigXml($xml) {
        $this->xmlconfig = $xml;
        $this->save();
    }

    public function getPagesXml() {
        return $this->xmlpages;
    }

    public function setPagesXml($xml) {
        $this->xmlpages = $xml;
        $this->save();
    }

    public function getTitle() {
        return $this->title;
    }

    public function setTitle($xml) {
        $this->title = $xml;
        $this->save();
    }

    public function getXslFo() {
        return $this->xmlxslfo;
    }

    public function setXslFo($xml) {
        $this->xmlxslfo = $xml;
        $this->save();
    }

    public function getPreviewXslFo() {
        return $this->xmlpreviewxslfo;
    }

    public function setPreviewXslFo($xml) {
        $this->xmlpreviewxslfo = $xml;
        $this->save();
    }

    public function getExtendPreviewXslFo() {
        return $this->xmlextendpreviewxslfo;
    }

    public function setExtendPreviewXslFo($xml) {
        $this->xmlextendpreviewxslfo = $xml;
        $this->save();
    }

    public function setPreviewPath($xml) {
        $this->preview_path = $xml;
        $this->save();
    }

    public function setLayouterModus($var) {
        $this->layouter_modus = $var;
        $this->save();
    }

    public function getPreviewPath() {
        return $this->preview_path;
    }

    public function getLayouterModus() {
        return $this->layouter_modus;
    }

    public function setDesignerXML($xml) {
        $this->designer_xml = $xml;
        $this->save();
    }

    public function getDesignerXML() {
        return $this->designer_xml;
    }

    /**
     * @return int
     */
    public function getTemplatePrintContactId()
    {
        $this->loadMongo();
        return $this->templatePrintContactId;
    }

    /**
     * @param int $templatePrintContactId
     */
    public function setTemplatePrintContactId($templatePrintContactId)
    {
        $this->loadMongo();
        $this->templatePrintContactId = $templatePrintContactId;
    }

    /**
     * @return string
     */
    public function getRef()
    {
        $this->loadMongo();
        return $this->ref;
    }

    /**
     * @param string $ref
     */
    public function setRef($ref)
    {
        $this->loadMongo();
        $this->ref = $ref;
    }

    public function loadMongo() {
        if($this->mongoLoaded) {
            return true;
        }
        $dbMongo = TP_Mongo::getInstance();
        $obj = $dbMongo->LayouterSession->findOne(array('uid' => $this->id));
        if($obj) {
            $this->ref = $obj['ref'];
            $this->kst = $obj['kst'];
            $this->templatePrintContactId = $obj['templatePrintContactId'];
            $this->mongoLoaded = true;
        }
    }

    public function saveMongo() {
        $this->loadMongo();
        $dbMongo = TP_Mongo::getInstance();
        $obj = $dbMongo->LayouterSession->findOne(array('uid' => $this->id));
        if($obj) {
            $dbMongo->LayouterSession->updateOne(array('uid' => $this->id), [ '$set' =>  $this->getArray()]);
        }else{
            $dbMongo->LayouterSession->insertOne($this->getArray());
        }   $this->mongoLoaded = true;

    }

    public function getArray() {
        return array(
            'uid' => $this->id,
            'ref' => $this->ref,
            'kst' => $this->kst,
            'templatePrintContactId' => $this->templatePrintContactId
        );
    }

    /**
     * @return string
     */
    public function getKst()
    {
        $this->loadMongo();
        return $this->kst;
    }

    /**
     * @param string $kst
     */
    public function setKst($kst)
    {
        $this->loadMongo();
        $this->kst = $kst;
    }

}