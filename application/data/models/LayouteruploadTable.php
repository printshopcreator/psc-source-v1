<?php

/**
 * LayouteruploadTable Modelclass
 *
 * LayouteruploadTable Modelsuperclass
 *
 * PHP versions 4 and 5
 *
 * @category   Data
 * @package    Model
 * @subpackage SuperTable
 * @author     Thomas petersomn <tpeterson@printshopcreator.de>
 * @copyright  1997-2009 Thomas Peterson
 * @license    Not Public
 * @version    SVN: $Id: LayouteruploadTable.php 5162 2010-11-06 01:42:44Z boonkerz $
 * @link       http://www.printshopcreator.de
 */

/**
 * LayouteruploadTable Modelclass
 *
 * LayouteruploadTable Modelsuperclass
 *
 * PHP versions 4 and 5
 *
 * @category   Data
 * @package    Model
 * @subpackage SuperTable
 * @author     Thomas petersomn <tpeterson@printshopcreator.de>
 * @copyright  1997-2009 Thomas Peterson
 * @license    Not Public
 * @version    Release: $Id: LayouteruploadTable.php 5162 2010-11-06 01:42:44Z boonkerz $
 * @link       http://www.printshopcreator.de
 */

class LayouteruploadTable extends Doctrine_Table
{

}