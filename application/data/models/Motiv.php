<?php

/**
 * Motiv Modelclass
 *
 * Motiv Modelsuperclass
 *
 * PHP versions 4 and 5
 *
 * @category   Data
 * @package    Model
 * @subpackage Supermodel
 * @author     Thomas petersomn <tpeterson@printshopcreator.de>
 * @copyright  1997-2009 Thomas Peterson
 * @license    Not Public
 * @version    SVN: $Id: Motiv.php 7314 2012-02-13 21:37:58Z boonkerz $
 * @link       http://www.printshopcreator.de
 */

/**
 * Motiv Modelclass
 *
 * Motiv Modelsuperclass
 *
 * PHP versions 4 and 5
 *
 * @category   Data
 * @package    Model
 * @subpackage Supermodel
 * @author     Thomas petersomn <tpeterson@printshopcreator.de>
 * @copyright  1997-2009 Thomas Peterson
 * @license    Not Public
 * @version    Release: $Id: Motiv.php 7314 2012-02-13 21:37:58Z boonkerz $
 * @link       http://www.printshopcreator.de
 */
class Motiv extends BaseMotiv
{

    /**
     * Empty template method to provide concrete Record classes with the possibility
     * to hook into the saving procedure.
     */
    public function preSave($event) {

        if ($this->identifier()) {
            $this->updated = date('Y-m-d H:i:s');
            if ($this->created == '') {
                $this->created = date('Y-m-d H:i:s');
            }
        } else {
            $this->created = date('Y-m-d H:i:s');
            $this->updated = date('Y-m-d H:i:s');
        }

        if ($this->uuid == '') {
            $this->uuid = TP_Util::uuid();
        }
        $this->price1 = str_replace(',', '.', $this->price1);
        $this->price2 = str_replace(',', '.', $this->price2);
        $this->price3 = str_replace(',', '.', $this->price3);
        $this->download_price = str_replace(',', '.', $this->download_price);

    }

    public function postSave($event) {
        $m = TP_Mongo::getInstance();
        $obj = $m->motive->findOne(array('_id' => $this->uuid));
        $temp5 = array();
        if ($obj) {
            foreach ($obj['tags'] as $row) {
                $temp5[] = $row['name'];
            }
        }

        TP_Util::clearCache(TP_Util::CLEAR_ALL);
    }

    public function postDelete($event) {
        TP_Util::clearCache(TP_Util::CLEAR_ALL);
    }

    public function postUpdate($event) {
        TP_Util::clearCache(TP_Util::CLEAR_ALL);
    }

    public function getRiakData() {
        return array(
            'title' => $this->title,
            'shop_id' => $this->shop_id,
            'install_id' => $this->install_id,
            'url' => $this->url,
            'resale_shop' => (bool)$this->resale_shop,
            'resale_market' => (bool)$this->resale_market,
            'status' => $this->status
        );
    }

    public function getRate() {
        if ($this->rate_count == 0) {
            return 0;
        }
        return round($this->rate / $this->rate_count, 0);
    }

}