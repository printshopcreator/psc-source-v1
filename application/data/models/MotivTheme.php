<?php

/**
 * MotivTheme Modelclass
 *
 * MotivTheme Modelsuperclass
 *
 * PHP versions 4 and 5
 *
 * @category   Data
 * @package    Model
 * @subpackage Supermodel
 * @author     Thomas petersomn <tpeterson@printshopcreator.de>
 * @copyright  1997-2009 Thomas Peterson
 * @license    Not Public
 * @version    SVN: $Id: MotivTheme.php 7314 2012-02-13 21:37:58Z boonkerz $
 * @link       http://www.printshopcreator.de
 */

/**
 * MotivTheme Modelclass
 *
 * MotivTheme Modelsuperclass
 *
 * PHP versions 4 and 5
 *
 * @category   Data
 * @package    Model
 * @subpackage Supermodel
 * @author     Thomas petersomn <tpeterson@printshopcreator.de>
 * @copyright  1997-2009 Thomas Peterson
 * @license    Not Public
 * @version    Release: $Id: MotivTheme.php 7314 2012-02-13 21:37:58Z boonkerz $
 * @link       http://www.printshopcreator.de
 */

class MotivTheme extends BaseMotivTheme
{
    /**
     * Empty template method to provide concrete Record classes with the possibility
     * to hook into the saving procedure.
     */
    public function preSave($event) {

        if ($this->identifier()) {
            $this->updated = date('Y-m-d H:i:s');
            if ($this->created == '') {
                $this->created = date('Y-m-d H:i:s');
            }
        } else {
            $this->created = date('Y-m-d H:i:s');
            $this->updated = date('Y-m-d H:i:s');
        }

        if ($this->uuid == '') {
            $this->uuid = TP_Util::uuid();
        }

        TP_Util::clearCache();

        Zend_Registry::get('filecache')->clean(
            Zend_Cache::CLEANING_MODE_MATCHING_TAG,
            array('motivtheme_' . $this->shop_id)
        );
    }

    public function preDelete($event) {
        TP_Util::clearCache();

        Zend_Registry::get('filecache')->clean(
            Zend_Cache::CLEANING_MODE_MATCHING_TAG,
            array('motivtheme_' . $this->shop_id)
        );
    }

    public function preUpdate($event) {
        TP_Util::clearCache();

        Zend_Registry::get('filecache')->clean(
            Zend_Cache::CLEANING_MODE_MATCHING_TAG,
            array('motivtheme_' . $this->shop_id)
        );
    }
}