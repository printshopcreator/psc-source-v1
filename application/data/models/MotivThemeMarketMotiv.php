<?php

/**
 * MotivThemeMarketMotiv Modelclass
 *
 * MotivThemeMarketMotiv Modelsuperclass
 *
 * PHP versions 4 and 5
 *
 * @category   Data
 * @package    Model
 * @subpackage Supermodel
 * @author     Thomas petersomn <tpeterson@printshopcreator.de>
 * @copyright  1997-2009 Thomas Peterson
 * @license    Not Public
 * @version    SVN: $Id: MotivThemeMarketMotiv.php 6492 2011-07-31 23:58:21Z boonkerz $
 * @link       http://www.printshopcreator.de
 */

/**
 * MotivThemeMarketMotiv Modelclass
 *
 * MotivThemeMarketMotiv Modelsuperclass
 *
 * PHP versions 4 and 5
 *
 * @category   Data
 * @package    Model
 * @subpackage Supermodel
 * @author     Thomas petersomn <tpeterson@printshopcreator.de>
 * @copyright  1997-2009 Thomas Peterson
 * @license    Not Public
 * @version    Release: $Id: MotivThemeMarketMotiv.php 6492 2011-07-31 23:58:21Z boonkerz $
 * @link       http://www.printshopcreator.de
 */

class MotivThemeMarketMotiv extends BaseMotivThemeMarketMotiv
{
    /**
     * Empty template method to provide concrete Record classes with the possibility
     * to hook into the saving procedure.
     */
    public function preSave($event) {

        //TP_Util::clearCache();
    }

}
