<?php

/**
 * ArticleGroupArticle Modelclass
 *
 * ArticleGroupArticle Modelsuperclass
 *
 * PHP versions 4 and 5
 *
 * @category   Data
 * @package    Model
 * @subpackage Supermodel
 * @author     Thomas petersomn <tpeterson@printshopcreator.de>
 * @copyright  1997-2009 Thomas Peterson
 * @license    Not Public
 * @version    SVN: $Id: MotivThemeMotiv.php 6492 2011-07-31 23:58:21Z boonkerz $
 * @link       http://www.printshopcreator.de
 */

/**
 * ArticleGroupArticle Modelclass
 *
 * ArticleGroupArticle Modelsuperclass
 *
 * PHP versions 4 and 5
 *
 * @category   Data
 * @package    Model
 * @subpackage Supermodel
 * @author     Thomas petersomn <tpeterson@printshopcreator.de>
 * @copyright  1997-2009 Thomas Peterson
 * @license    Not Public
 * @version    Release: $Id: MotivThemeMotiv.php 6492 2011-07-31 23:58:21Z boonkerz $
 * @link       http://www.printshopcreator.de
 */

class MotivThemeMotiv extends BaseMotivThemeMotiv
{
    /**
     * Empty template method to provide concrete Record classes with the possibility
     * to hook into the saving procedure.
     */
    public function preSave($event) {

        //TP_Util::clearCache();
    }

}
