<?php

/**
 * News Modelclass
 *
 * News Modelsuperclass
 *
 * PHP versions 4 and 5
 *
 * @category   Data
 * @package    Model
 * @subpackage Supermodel
 * @author     Thomas petersomn <tpeterson@printshopcreator.de>
 * @copyright  1997-2009 Thomas Peterson
 * @license    Not Public
 * @version    SVN: $Id: News.php 5162 2010-11-06 01:42:44Z boonkerz $
 * @link       http://www.printshopcreator.de
 */

/**
 * News Modelclass
 *
 * News Modelsuperclass
 *
 * PHP versions 4 and 5
 *
 * @category   Data
 * @package    Model
 * @subpackage Supermodel
 * @author     Thomas petersomn <tpeterson@printshopcreator.de>
 * @copyright  1997-2009 Thomas Peterson
 * @license    Not Public
 * @version    Release: $Id: News.php 5162 2010-11-06 01:42:44Z boonkerz $
 * @link       http://www.printshopcreator.de
 */

class News extends BaseNews
{

    public function getLiveedit($field, $type = 'INPUT') {
        $mode = new Zend_Session_Namespace ('adminmode');
        if ($mode->liveedit && $type == 'INPUT') {
            return '<span class="liveeditinput" table="news" field="' . $field . '" id="' . $this->id . '">' . $this->$field . '</span>';
        } elseif ($mode->liveedit && $type == 'RTE') {
            return '<span class="liveedit" table="news" field="' . $field . '" id="' . $this->id . '">' . $this->$field . '</span>';
        }

        return $this->$field;
    }
}