<?php

/**
 * Orders Modelclass
 *
 * Orders Modelsuperclass
 *
 * PHP versions 4 and 5
 *
 * @category   Data
 * @package    Model
 * @subpackage Supermodel
 * @author     Thomas petersomn <tpeterson@printshopcreator.de>
 * @copyright  1997-2009 Thomas Peterson
 * @license    Not Public
 * @version    SVN: $Id: Orders.php 6674 2011-09-16 12:15:05Z boonkerz $
 * @link       http://www.printshopcreator.de
 */

/**
 * Orders Modelclass
 *
 * Orders Modelsuperclass
 *
 * PHP versions 4 and 5
 *
 * @category   Data
 * @package    Model
 * @subpackage Supermodel
 * @author     Thomas petersomn <tpeterson@printshopcreator.de>
 * @copyright  1997-2009 Thomas Peterson
 * @license    Not Public
 * @version    Release: $Id: Orders.php 6674 2011-09-16 12:15:05Z boonkerz $
 * @link       http://www.printshopcreator.de
 */
class Orders extends BaseOrders
{

    protected $packageExported = false;
    public $mongoLoaded = false;
    protected $paymentRef = '';
    protected $paymentToken = '';
    protected $invoiceAddressSaved = [];
    protected $deliveryAddressSaved = [];
    protected $senderAddressSaved = [];
    protected $withTax = true;
    protected $gutscheinAbzugNetto = 0;
    protected $sendDataToShipping = false;

    public function preSave($event) {

        if ($this->uuid == '') {
            $this->uuid = TP_Util::uuid();
        }

        if ($this->identifier()) {
            $this->updated = date('Y-m-d H:i:s');
            if ($this->created == '') {
                $this->created = date('Y-m-d H:i:s');
            }
        } else {
            $this->created = date('Y-m-d H:i:s');
            $this->updated = date('Y-m-d H:i:s');
        }
        if ($this->version == "") {
            $this->version = 0;
        }
        if ($this->type == "") {
            $this->type = 1;
        }
        $this->version = $this->version + 1;
    }

    public function getPayment() {

        $paymenttype = Doctrine_Query::create()
            ->from('Paymenttype c')
            ->where('c.id = ?', array(intval($this->paymenttype_id)))
            ->fetchOne();

        if ($paymenttype != false) {
            return $paymenttype;
        }
        return new Paymenttype();
    }

    public function getShipping() {
        $shippingtype = Doctrine_Query::create()
            ->from('Shippingtype c')
            ->where('c.id = ?', array(intval($this->shippingtype_id)))
            ->fetchOne();

        if ($shippingtype != false) {
            return $shippingtype;
        }
        return new Shippingtype();
    }

    public function getInvoiceAddress() {
        if ($this->invoice_address != 0) {

            $address = Doctrine_Query::create()
                ->from('ContactAddress c')
                ->where('c.id = ?', array(intval($this->invoice_address)))
                ->fetchOne();

        } else {

            $address = Doctrine_Query::create()
                ->from('ContactAddress c')
                ->where('c.id = ?', array(intval($this->delivery_address)))
                ->fetchOne();

        }

        return $address;
    }

    public function getDeliveryAddress() {

        $address = Doctrine_Query::create()
            ->from('ContactAddress c')
            ->where('c.id = ?', array(intval($this->delivery_address)))
            ->fetchOne();

        if(!$address) {
            return new ContactAddress();
        }

        return $address;
    }

    public function getSenderAddress() {
        if ($this->sender_address != 0) {

            $address = Doctrine_Query::create()
                ->from('ContactAddress c')
                ->where('c.id = ?', array(intval($this->sender_address)))
                ->fetchOne();

        } else {

            $address = Doctrine_Query::create()
                ->from('ContactAddress c')
                ->where('c.id = ?', array(intval($this->invoice_address)))
                ->fetchOne();

        }
        if(!$address) {
            $address = Doctrine_Query::create()
                ->from('ContactAddress c')
                ->where('c.id = ?', array(intval($this->delivery_address)))
                ->fetchOne();
        }

        return $address;
    }

    public function getInfo() {
        if($this->info == "") {
            return array();
        }

        return json_decode($this->info, true);
    }

    /**
     * @return boolean
     */
    public function isPackageExported()
    {
        $this->loadMongo();
        return $this->packageExported;
    }

    /**
     * @param boolean $packageExported
     */
    public function setPackageExported($packageExported)
    {
        $this->packageExported = $packageExported;
    }

    public function loadMongo() {
        if($this->mongoLoaded) {
            return true;
        }
        $dbMongo = TP_Mongo::getInstance();
        $obj = $dbMongo->Order->findOne(array('uid' => $this->id));
        if($obj) {
            $this->packageExported = $obj['packageExported'];
            $this->paymentRef = $obj['paymentRef'];
            $this->mongoLoaded = true;
            $this->paymentToken = $obj['paymentToken'];
            $this->invoiceAddressSaved = $obj['invoiceAddressSaved'];
            $this->deliveryAddressSaved = $obj['deliveryAddressSaved'];
            $this->senderAddressSaved = $obj['senderAddressSaved'];
            $this->gutscheinAbzugNetto = $obj['gutscheinAbzugNetto'];
            $this->sendDataToShipping = $obj['sendDataToShipping'];
            $this->withTax = $obj['withTax'];
        }
    }

    public function saveMongo() {
        $this->loadMongo();
        $dbMongo = TP_Mongo::getInstance();
        $obj = $dbMongo->Order->findOne(array('uid' => $this->id));
        if($obj) {
            $dbMongo->Order->updateOne(array('uid' => $this->id), [ '$set' =>  $this->getArray()]);
        }else{
            $dbMongo->Order->insertOne($this->getArray());
        }   $this->mongoLoaded = true;

    }

    public function getArray() {
        return array(
            'uid' => $this->id,
            'packageExported' => $this->packageExported,
            'paymentRef' => $this->paymentRef,
            'paymentToken' => $this->paymentToken,
            'invoiceAddressSaved' => $this->invoiceAddressSaved,
            'deliveryAddressSaved' => $this->deliveryAddressSaved,
            'senderAddressSaved' => $this->senderAddressSaved,
            'gutscheinAbzugNetto' => $this->gutscheinAbzugNetto,
            'sendDataToShipping' => $this->sendDataToShipping,
            'withTax' => $this->withTax,
        );
    }

    /**
     * @return string
     */
    public function getPaymentRef()
    {
        $this->loadMongo();
        return $this->paymentRef;
    }

    /**
     * @param string $paymentRef
     */
    public function setPaymentRef($paymentRef)
    {
        $this->paymentRef = $paymentRef;
    }

    /**
     * @return string
     */
    public function getPaymentToken()
    {
        return $this->paymentToken;
    }

    /**
     * @param string $paymentToken
     */
    public function setPaymentToken($paymentToken)
    {
        $this->paymentToken = $paymentToken;
    }

    /**
     * @return array
     */
    public function getInvoiceAddressSaved()
    {
        $this->loadMongo();
        return $this->invoiceAddressSaved;
    }

    /**
     * @param array $invoiceAddressSaved
     */
    public function setInvoiceAddressSaved($invoiceAddressSaved)
    {
        $this->invoiceAddressSaved = $invoiceAddressSaved;
    }

    /**
     * @return array
     */
    public function getDeliveryAddressSaved()
    {
        $this->loadMongo();
        return $this->deliveryAddressSaved;
    }

    /**
     * @param array $deliveryAddressSaved
     */
    public function setDeliveryAddressSaved($deliveryAddressSaved)
    {
        $this->deliveryAddressSaved = $deliveryAddressSaved;
    }

    /**
     * @return array
     */
    public function getSenderAddressSaved()
    {
        $this->loadMongo();
        return $this->senderAddressSaved;
    }

    /**
     * @param array $senderAddressSaved
     */
    public function setSenderAddressSaved($senderAddressSaved)
    {
        $this->senderAddressSaved = $senderAddressSaved;
    }

    /**
     * @return bool
     */
    public function isWithTax()
    {
        $this->loadMongo();
        return $this->withTax;
    }

    /**
     * @param bool $withTax
     */
    public function setWithTax($withTax)
    {
        $this->withTax = $withTax;
    }

    public function isSendDataToShipping()
    {
        $this->loadMongo();
        return $this->sendDataToShipping;
    }

    public function setSendDataToShipping($sendDataToShipping)
    {
        $this->sendDataToShipping = $sendDataToShipping;
    }

    public function getGutscheinAbzugNetto()
    {
        $this->loadMongo();
        return $this->gutscheinAbzugNetto;
    }

    public function setGutscheinAbzugNetto($gutscheinAbzugNetto)
    {
        $this->gutscheinAbzugNetto = $gutscheinAbzugNetto;
    }


}