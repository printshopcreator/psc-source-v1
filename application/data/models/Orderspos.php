<?php

/**
 * Orderspos Modelclass
 *
 * Orderspos Modelsuperclass
 *
 * PHP versions 4 and 5
 *
 * @category   Data
 * @package    Model
 * @subpackage Supermodel
 * @author     Thomas petersomn <tpeterson@printshopcreator.de>
 * @copyright  1997-2009 Thomas Peterson
 * @license    Not Public
 * @version    SVN: $Id: Orderspos.php 6826 2011-10-17 23:22:06Z boonkerz $
 * @link       http://www.printshopcreator.de
 */

/**
 * Orderspos Modelclass
 *
 * Orderspos Modelsuperclass
 *
 * PHP versions 4 and 5
 *
 * @category   Data
 * @package    Model
 * @subpackage Supermodel
 * @author     Thomas petersomn <tpeterson@printshopcreator.de>
 * @copyright  1997-2009 Thomas Peterson
 * @license    Not Public
 * @version    Release: $Id: Orderspos.php 6826 2011-10-17 23:22:06Z boonkerz $
 * @link       http://www.printshopcreator.de
 */

class Orderspos extends BaseOrderspos
{

    protected $layouterId = false;
    protected $templatePrintId = false;
    protected $templatePrintContactId = 0;
    protected $deliveryData = false;
    protected $calcReferences = [];
    protected $additionalInfos = [];
    protected $customerInfo = "";
    protected $uploadMode = "";
    protected $mongoLoaded = false;

    public function preSave($event) {

        if ($this->uuid == '') {
            $this->uuid = TP_Util::uuid();
        }
        if ($this->version == "") {
            $this->version = 0;
        }
        $this->version = $this->version + 1;
    }

    public function getPostionData($key) {


        $data = $this->getAllPostionData();

        if($this->count != 1 && $key == "auflage") {
            return $this->count;
        }
        if (isset($data[$key])) {
            return $data[$key];
        }
        return false;
    }

    public function getAllPostionData() {
        $data = unserialize($this->data);
        return $data->getOptions();
    }

    public function getRef() {
        if($this->ref == "") {
            return $this->Article->getTitle();
        }

        return $this->ref;
    }

    public function loadMongo() {
        if($this->mongoLoaded) {
            return true;
        }
        $dbMongo = TP_Mongo::getInstance();
        $obj = $dbMongo->Position->findOne(array('uid' => $this->id));
        if($obj) {
            $this->templatePrintId = $obj['templatePrintId'];
            $this->templatePrintContactId = $obj['templatePrintContactId'];
            $this->layouterId = $obj['layouterId'];
            $this->deliveryData = $obj['deliveryData'];
            $this->calcReferences = $obj['calcReferences'];
            $this->additionalInfos = $obj['additionalInfos'];
            $this->uploadMode = $obj['uploadMode'];
            $this->customerInfo = $obj['customerInfo'];
            $this->mongoLoaded = true;
        }
    }

    public function saveMongo() {
        $dbMongo = TP_Mongo::getInstance();
        $obj = $dbMongo->Position->findOne(array('uid' => $this->id));
        if($obj) {
            $dbMongo->Position->updateOne(array('uid' => $this->id), [ '$set' =>  $this->getArray()]);
        }else{
            $dbMongo->Position->insertOne($this->getArray());
        }   $this->mongoLoaded = true;

    }

    public function getArray() {
        return array(
            'uid' => $this->id,
            'templatePrintId' => $this->templatePrintId,
            'templatePrintContactId' => $this->templatePrintContactId,
            'layouterId' => $this->layouterId,
            'deliveryData' => $this->deliveryData,
            'calcReferences' => $this->calcReferences,
            'additionalInfos' => $this->additionalInfos,
            'uploadMode' => $this->uploadMode,
            'customerInfo' => $this->customerInfo,
        );
    }

    public function getUploadMode()
    {
        $this->loadMongo();
        return $this->uploadMode;
    }

    public function setUploadMode($uploadMode)
    {
        $this->uploadMode = $uploadMode;
    }

    public function getCustomerInfo()
    {
        $this->loadMongo();
        return $this->customerInfo;
    }

    public function setCustomerInfo($customerInfo)
    {
        $this->customerInfo = $customerInfo;
    }

    /**
     * @return boolean
     */
    public function isTemplatePrintId()
    {
        $this->loadMongo();
        return $this->templatePrintId;
    }

    /**
     * @param boolean $templatePrintId
     */
    public function setTemplatePrintId($templatePrintId)
    {
        $this->templatePrintId = $templatePrintId;
    }

    /**
     * @return boolean
     */
    public function isLayouterId()
    {
        $this->loadMongo();
        return $this->layouterId;
    }

    /**
     * @param boolean $layouterId
     */
    public function setLayouterId($layouterId)
    {
        $this->layouterId = $layouterId;
    }

    /**
     * @return array
     */
    public function getDeliveryData()
    {
        $this->loadMongo();
        return $this->deliveryData;
    }

    /**
     * @param array $data
     */
    public function setDeliveryData($data)
    {
        $this->deliveryData = $data;
    }

    /**
     * @return int
     */
    public function getTemplatePrintContactId()
    {
        $this->loadMongo();
        return $this->templatePrintContactId;
    }

    /**
     * @param int $templatePrintContactId
     */
    public function setTemplatePrintContactId($templatePrintContactId)
    {
        $this->templatePrintContactId = $templatePrintContactId;
    }

    /**
     * @return array
     */
    public function getCalcReferences()
    {
        $this->loadMongo();
        return $this->calcReferences;
    }

    /**
     * @param array $calcReferences
     */
    public function setCalcReferences($calcReferences)
    {
        $this->calcReferences = $calcReferences;
    }

    /**
     * @return array
     */
    public function getAdditionalInfos()
    {
        $this->loadMongo();
        return $this->additionalInfos;
    }

    /**
     * @param array $additionalInfos
     */
    public function setAdditionalInfos($additionalInfos)
    {
        $this->additionalInfos = $additionalInfos;
    }
}
