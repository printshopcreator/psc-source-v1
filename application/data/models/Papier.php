<?php

/**
 * Papier Modelclass
 *
 * Papier Modelsuperclass
 *
 * PHP versions 4 and 5
 *
 * @category   Data
 * @package    Model
 * @subpackage Supermodel
 * @author     Thomas petersomn <tpeterson@printshopcreator.de>
 * @copyright  1997-2009 Thomas Peterson
 * @license    Not Public
 * @version    SVN: $Id: Papier.php 5162 2010-11-06 01:42:44Z boonkerz $
 * @link       http://www.printshopcreator.de
 */

/**
 * Papier Modelclass
 *
 * Papier Modelsuperclass
 *
 * PHP versions 4 and 5
 *
 * @category   Data
 * @package    Model
 * @subpackage Supermodel
 * @author     Thomas petersomn <tpeterson@printshopcreator.de>
 * @copyright  1997-2009 Thomas Peterson
 * @license    Not Public
 * @version    Release: $Id: Papier.php 5162 2010-11-06 01:42:44Z boonkerz $
 * @link       http://www.printshopcreator.de
 */

class Papier extends BasePapier
{

    protected $papierTyp1 = 1;
    protected $papierTyp2 = 1;
    protected $papierTyp3 = 1;
    protected $papierTyp4 = 1;
    protected $papierTyp5 = 1;
    protected $papierTyp6 = 1;
    protected $papierTyp7 = 1;
    protected $papierTyp8 = 1;
    protected $papierTyp9 = 1;
    protected $papierTyp10 = 1;
    protected $papierTyp11 = 1;
    protected $papierTyp12 = 1;
    protected $papierTyp13 = 1;
    protected $papierTyp14 = 1;
    protected $umschlagen;
    protected $happy;
    protected $eq;
    protected $sense;
    protected $sky;
    protected $glam;
    protected $post;
    protected $volume;
    protected $mongoLoaded = false;

    public function preSave($event) {

        if ($this->uuid == '') {
            $this->uuid = TP_Util::uuid();
        }

    }

    public function asArray()
    {
        $tmp = $this->toArray();
        $tmp['volume'] = $this->getVolume();

        return $tmp;
    }

    /**
     * @return int
     */
    public function getPapierTyp1()
    {
        $this->loadData();
        return $this->papierTyp1;
    }

    /**
     * @param int $papierTyp1
     */
    public function setPapierTyp1($papierTyp1)
    {
        $this->papierTyp1 = $papierTyp1;
    }

    /**
     * @return int
     */
    public function getPapierTyp2()
    {
        $this->loadData();
        return $this->papierTyp2;
    }

    /**
     * @param int $papierTyp2
     */
    public function setPapierTyp2($papierTyp2)
    {
        $this->papierTyp2 = $papierTyp2;
    }

    /**
     * @return int
     */
    public function getPapierTyp3()
    {
        $this->loadData();
        return $this->papierTyp3;
    }

    /**
     * @param int $papierTyp3
     */
    public function setPapierTyp3($papierTyp3)
    {
        $this->papierTyp3 = $papierTyp3;
    }

    /**
     * @return int
     */
    public function getPapierTyp4()
    {
        $this->loadData();
        return $this->papierTyp4;
    }

    /**
     * @param int $papierTyp4
     */
    public function setPapierTyp4($papierTyp4)
    {
        $this->papierTyp4 = $papierTyp4;
    }

    /**
     * @return int
     */
    public function getPapierTyp5()
    {
        $this->loadData();
        return $this->papierTyp5;
    }

    /**
     * @param int $papierTyp5
     */
    public function setPapierTyp5($papierTyp5)
    {
        $this->papierTyp5 = $papierTyp5;
    }

    /**
     * @return int
     */
    public function getPapierTyp6()
    {
        $this->loadData();
        return $this->papierTyp6;
    }

    /**
     * @param int $papierTyp6
     */
    public function setPapierTyp6($papierTyp6)
    {
        $this->papierTyp6 = $papierTyp6;
    }

    /**
     * @return int
     */
    public function getPapierTyp7()
    {
        $this->loadData();
        return $this->papierTyp7;
    }

    /**
     * @param int $papierTyp7
     */
    public function setPapierTyp7($papierTyp7)
    {
        $this->papierTyp7 = $papierTyp7;
    }

    /**
     * @return int
     */
    public function getPapierTyp8()
    {
        $this->loadData();
        return $this->papierTyp8;
    }

    /**
     * @param int $papierTyp8
     */
    public function setPapierTyp8($papierTyp8)
    {
        $this->papierTyp8 = $papierTyp8;
    }

    /**
     * @return mixed
     */
    public function getPapierTyp9()
    {
        $this->loadData();
        return $this->papierTyp9;
    }

    /**
     * @param mixed $papierTyp9
     */
    public function setPapierTyp9($papierTyp9)
    {
        $this->papierTyp9 = $papierTyp9;
    }

    /**
     * @return mixed
     */
    public function getPapierTyp10()
    {
        $this->loadData();
        return $this->papierTyp10;
    }

    /**
     * @param mixed $papierTyp10
     */
    public function setPapierTyp10($papierTyp10)
    {
        $this->papierTyp10 = $papierTyp10;
    }

    /**
     * @return mixed
     */
    public function getPapierTyp11()
    {
        $this->loadData();
        return $this->papierTyp11;
    }

    /**
     * @param mixed $papierTyp11
     */
    public function setPapierTyp11($papierTyp11)
    {
        $this->papierTyp11 = $papierTyp11;
    }

    /**
     * @return mixed
     */
    public function getPapierTyp12()
    {
        $this->loadData();
        return $this->papierTyp12;
    }

    /**
     * @param mixed $papierTyp12
     */
    public function setPapierTyp12($papierTyp12)
    {
        $this->papierTyp12 = $papierTyp12;
    }

    /**
     * @return mixed
     */
    public function getPapierTyp13()
    {
        $this->loadData();
        return $this->papierTyp13;
    }

    /**
     * @param mixed $papierTyp13
     */
    public function setPapierTyp13($papierTyp13)
    {
        $this->papierTyp13 = $papierTyp13;
    }

    /**
     * @return mixed
     */
    public function getUmschlagen()
    {
        $this->loadData();
        return $this->umschlagen;
    }

    /**
     * @param mixed $umschlagen
     */
    public function setUmschlagen($umschlagen)
    {
        $this->umschlagen = $umschlagen;
    }

    /**
     * @return mixed
     */
    public function getHappy()
    {
        $this->loadData();
        return $this->happy;
    }

    /**
     * @param mixed $happy
     */
    public function setHappy($happy)
    {
        $this->happy = $happy;
    }

    /**
     * @return mixed
     */
    public function getEq()
    {
        $this->loadData();
        return $this->eq;
    }

    /**
     * @param mixed $eq
     */
    public function setEq($eq)
    {
        $this->eq = $eq;
    }

    /**
     * @return mixed
     */
    public function getSense()
    {
        $this->loadData();
        return $this->sense;
    }

    /**
     * @param mixed $sense
     */
    public function setSense($sense)
    {
        $this->sense = $sense;
    }

    /**
     * @return mixed
     */
    public function getSky()
    {
        $this->loadData();
        return $this->sky;
    }

    /**
     * @param mixed $sky
     */
    public function setSky($sky)
    {
        $this->sky = $sky;
    }

    /**
     * @return mixed
     */
    public function getGlam()
    {
        $this->loadData();
        return $this->glam;
    }

    /**
     * @param mixed $glam
     */
    public function setGlam($glam)
    {
        $this->glam = $glam;
    }

    /**
     * @return mixed
     */
    public function getPost()
    {
        $this->loadData();
        return $this->post;
    }

    /**
     * @param mixed $post
     */
    public function setPost($post)
    {
        $this->post = $post;
    }

    public function getPaperTypes() {

        $temp = array();
        if($this->getPapierTyp1()) {
            $temp[] = 'papierTyp1';
        }
        if($this->getPapierTyp2()) {
            $temp[] = 'papierTyp2';
        }
        if($this->getPapierTyp3()) {
            $temp[] = 'papierTyp3';
        }
        if($this->getPapierTyp4()) {
            $temp[] = 'papierTyp4';
        }
        if($this->getPapierTyp5()) {
            $temp[] = 'papierTyp5';
        }
        if($this->getPapierTyp6()) {
            $temp[] = 'papierTyp6';
        }
        if($this->getPapierTyp7()) {
            $temp[] = 'papierTyp7';
        }
        if($this->getPapierTyp8()) {
            $temp[] = 'papierTyp8';
        }
        if($this->getPapierTyp9()) {
            $temp[] = 'papierTyp9';
        }
        if($this->getPapierTyp10()) {
            $temp[] = 'papierTyp10';
        }
        if($this->getPapierTyp11()) {
            $temp[] = 'papierTyp11';
        }
        if($this->getPapierTyp12()) {
            $temp[] = 'papierTyp12';
        }
        if($this->getPapierTyp13()) {
            $temp[] = 'papierTyp13';
        }
        if($this->getPapierTyp14()) {
            $temp[] = 'papierTyp14';
        }
    }

    public function getSammelform() {
        if($this->getHappy()) {
            return 2;
        }
        if($this->getEq()) {
            return 3;
        }
        if($this->getSense()) {
            return 4;
        }
        if($this->getSky()) {
            return 5;
        }
        if($this->getGlam()) {
            return 6;
        }
        if($this->getPost()) {
            return 7;
        }
        return 1;
    }

    public function loadData() {
        if($this->mongoLoaded) {
            return true;
        }
        $dbMongo = TP_Mongo::getInstance();
        $obj = $dbMongo->Paper->findOne(array('uid' => $this->id));

        if($obj) {
            $this->papierTyp1 = $obj['papierTyp1'];
            $this->papierTyp2 = $obj['papierTyp2'];
            $this->papierTyp3 = $obj['papierTyp3'];
            $this->papierTyp4 = $obj['papierTyp4'];
            $this->papierTyp5 = $obj['papierTyp5'];
            $this->papierTyp6 = $obj['papierTyp6'];
            $this->papierTyp7 = $obj['papierTyp7'];
            $this->papierTyp8 = $obj['papierTyp8'];
            $this->papierTyp9 = $obj['papierTyp9'];
            $this->papierTyp10 = $obj['papierTyp10'];
            $this->papierTyp11 = $obj['papierTyp11'];
            $this->papierTyp12 = $obj['papierTyp12'];
            $this->papierTyp13 = $obj['papierTyp13'];
            $this->papierTyp14 = $obj['papierTyp14'];
            $this->umschlagen = $obj['umschlagen'];
            $this->happy = $obj['happy'];
            $this->eq = $obj['eq'];
            $this->sense = $obj['sense'];
            $this->sky = $obj['sky'];
            $this->glam = $obj['glam'];
            $this->post = $obj['post'];
            $this->volume = $obj['volume'];
            $this->mongoLoaded = true;
        }
    }

    /**
     * @return int
     */
    public function getPapierTyp14()
    {
        return $this->papierTyp14;
    }

    /**
     * @param int $papierTyp14
     */
    public function setPapierTyp14($papierTyp14)
    {
        $this->loadData();
        $this->papierTyp14 = $papierTyp14;
    }

    /**
     * @return mixed
     */
    public function getVolume()
    {
        $this->loadData();
        return $this->volume;
    }

    /**
     * @param mixed $volume
     */
    public function setVolume($volume)
    {
        $this->volume = $volume;
    }
}