<?php

/**
 * Paymenttype Modelclass
 *
 * Paymenttype Modelsuperclass
 *
 * PHP versions 4 and 5
 *
 * @category   Data
 * @package    Model
 * @subpackage Supermodel
 * @author     Thomas petersomn <tpeterson@printshopcreator.de>
 * @copyright  1997-2009 Thomas Peterson
 * @license    Not Public
 * @version    SVN: $Id: Paymenttype.php 5162 2010-11-06 01:42:44Z boonkerz $
 * @link       http://www.printshopcreator.de
 */

/**
 * Paymenttype Modelclass
 *
 * Paymenttype Modelsuperclass
 *
 * PHP versions 4 and 5
 *
 * @category   Data
 * @package    Model
 * @subpackage Supermodel
 * @author     Thomas petersomn <tpeterson@printshopcreator.de>
 * @copyright  1997-2009 Thomas Peterson
 * @license    Not Public
 * @version    Release: $Id: Paymenttype.php 5162 2010-11-06 01:42:44Z boonkerz $
 * @link       http://www.printshopcreator.de
 */

class Paymenttype extends BasePaymenttype
{
    protected $pluginSettings = [];

    protected $shippings = [];

    protected $paymentGateway = '';

    protected $mongoLoaded = false;

    public function loadData() {
        if($this->mongoLoaded) {
            return true;
        }
        $dbMongo = TP_Mongo::getInstance();
        $obj = $dbMongo->Payment->findOne(array('uid' => $this->id));
        if($obj) {
            $this->pluginSettings = $obj['pluginSettings'];
            $this->shippings = $obj['shippings'];
            $this->paymentGateway = $obj['paymentGateway'];
            $this->mongoLoaded = true;
        }
    }

    /**
     * Empty template method to provide concrete Record classes with the possibility
     * to hook into the saving procedure.
     */
    public function preSave($event) {

        if ($this->identifier()) {
            $this->updated = date('Y-m-d H:i:s');
            if ($this->created == '') {
                $this->created = date('Y-m-d H:i:s');
            }
        } else {
            $this->created = date('Y-m-d H:i:s');
            $this->updated = date('Y-m-d H:i:s');
        }

        $this->wert = str_replace(',', '.', $this->wert);
        $this->prozent = str_replace(',', '.', $this->prozent);

    }

    /**
     * @return array
     */
    public function getPluginSettings($module, $name)
    {
        $this->loadData();
        if(!isset($this->pluginSettings[$module]) || !isset($this->pluginSettings[$module][$name])) {
            return null;
        }

        return $this->pluginSettings[$module][$name];
    }

    /**
     * @return array
     */
    public function getShippings()
    {
        $this->loadData();
        return $this->shippings;
    }

    /**
     * @return string
     */
    public function getPaymentGateway()
    {
        $this->loadData();
        return $this->paymentGateway;
    }

    /**
     * @param string $paymentGateway
     */
    public function setPaymentGateway($paymentGateway)
    {
        $this->paymentGateway = $paymentGateway;
    }
}