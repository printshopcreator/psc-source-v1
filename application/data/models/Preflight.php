<?php

/**
 * MotivTheme Modelclass
 *
 * MotivTheme Modelsuperclass
 *
 * PHP versions 4 and 5
 *
 * @category   Data
 * @package    Model
 * @subpackage Supermodel
 * @author     Thomas petersomn <tpeterson@printshopcreator.de>
 * @copyright  1997-2009 Thomas Peterson
 * @license    Not Public
 * @version    SVN: $Id: Preflight.php 6551 2011-09-01 08:30:38Z boonkerz $
 * @link       http://www.printshopcreator.de
 */

/**
 * MotivTheme Modelclass
 *
 * MotivTheme Modelsuperclass
 *
 * PHP versions 4 and 5
 *
 * @category   Data
 * @package    Model
 * @subpackage Supermodel
 * @author     Thomas petersomn <tpeterson@printshopcreator.de>
 * @copyright  1997-2009 Thomas Peterson
 * @license    Not Public
 * @version    Release: $Id: Preflight.php 6551 2011-09-01 08:30:38Z boonkerz $
 * @link       http://www.printshopcreator.de
 */
class Preflight extends BasePreflight
{

    /**
     * Empty template method to provide concrete Record classes with the possibility
     * to hook into the saving procedure.
     */
    public function preSave($event) {

        if ($this->identifier()) {
            $this->updated = date('Y-m-d H:i:s');
            if ($this->created == '') {
                $this->created = date('Y-m-d H:i:s');
            }
        } else {
            $this->created = date('Y-m-d H:i:s');
            $this->updated = date('Y-m-d H:i:s');
        }

        if ($this->uuid == '') {
            $this->uuid = TP_Util::uuid();
        }
    }

}