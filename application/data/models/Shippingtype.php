<?php

/**
 * Shippingtype Modelclass
 *
 * Shippingtype Modelsuperclass
 *
 * PHP versions 4 and 5
 *
 * @category   Data
 * @package    Model
 * @subpackage Supermodel
 * @author     Thomas petersomn <tpeterson@printshopcreator.de>
 * @copyright  1997-2009 Thomas Peterson
 * @license    Not Public
 * @version    SVN: $Id: Shippingtype.php 7055 2011-12-11 22:43:37Z boonkerz $
 * @link       http://www.printshopcreator.de
 */

/**
 * Shippingtype Modelclass
 *
 * Shippingtype Modelsuperclass
 *
 * PHP versions 4 and 5
 *
 * @category   Data
 * @package    Model
 * @subpackage Supermodel
 * @author     Thomas petersomn <tpeterson@printshopcreator.de>
 * @copyright  1997-2009 Thomas Peterson
 * @license    Not Public
 * @version    Release: $Id: Shippingtype.php 7055 2011-12-11 22:43:37Z boonkerz $
 * @link       http://www.printshopcreator.de
 */

class Shippingtype extends BaseShippingtype
{
    protected $pluginSettings = [];

    protected $mongoLoaded = false;

    public function loadData() {
        if($this->mongoLoaded) {
            return true;
        }
        $dbMongo = TP_Mongo::getInstance();
        $obj = $dbMongo->Shipping->findOne(array('uid' => $this->id));
        if($obj) {
            $this->pluginSettings = $obj['pluginSettings'];
            $this->mongoLoaded = true;
        }
    }

    /**
     * Empty template method to provide concrete Record classes with the possibility
     * to hook into the saving procedure.
     */
    public function preSave($event) {

        if ($this->identifier()) {
            $this->updated = date('Y-m-d H:i:s');
            if ($this->created == '') {
                $this->created = date('Y-m-d H:i:s');
            }
        } else {
            $this->created = date('Y-m-d H:i:s');
            $this->updated = date('Y-m-d H:i:s');
        }

        $this->kosten_fix = str_replace(',', '.', $this->kosten_fix);

    }

    public function calcVersand($netto, $gewicht = 0, $country = 'de', $user = 0, $plz = false) {

        $basket = new TP_Basket();

        if ($this->xml != "" && ($this->Shop->uid == '4ewlieu' || $user || $basket->getPLZ() != "" || $basket->getDeliveryPLZ() != "" || $plz)) {
            if ($plz) {
                if(intval($plz) >= 100) {
                    $plz = substr(preg_replace("/[^0-9.]/", "", $plz), 0, 2);
                }
            } elseif ($basket->getPLZ() != "") {
                $plz = substr(preg_replace("/[^0-9.]/", "", $basket->getPLZ()), 0, 2);
            } elseif ($basket->getDeliveryPLZ() != "") {
                $plz = substr(preg_replace("/[^0-9.]/", "", $basket->getDeliveryPLZ()), 0, 2);
            } else {
                $plz = substr(preg_replace("/[^0-9.]/", "", $user->self_zip), 0, 2);
            }

            if($gewicht == 0) {
                $gewicht = $gewicht + 1;
            }
            $xml = simplexml_load_string($this->xml);
            $plz = intval($plz);

            Zend_Registry::get('log')->debug("ZIP:" . $plz);
            $country = strtolower($country);
            if($country == "" && isset($xml['default'])) {
                $country = (string)$xml['default'];
            }
            if($country == "" && !isset($xml['default'])) {
                $country = "de";
            }

            if(!isset($xml->$country) && isset($xml['default'])) {
                $country = strtolower((string)$xml['default']);
            }

            foreach ($xml->$country->children() as $limit) {

                if ((preg_match("/^([0-9a-zA-Z]+)$/", trim($limit ['value']), $regs) && $regs [1] == $gewicht) ||
                    (preg_match("/^([0-9]+)-([0-9]+)$/", trim($limit ['value']), $regs) && ($gewicht >= $regs [1] && $gewicht <= $regs [2])) ||
                    (preg_match("/^([0-9]+)-$/", trim($limit ['value']), $regs) && $gewicht >= $regs [1])
                ) {
                    foreach ($limit->opt as $opt) {

                        if ((preg_match("/^([0-9a-zA-Z]+)$/", trim($opt), $regs) && $regs [1] == $plz) ||
                            (preg_match("/^([0-9]+)-([0-9]+)$/", trim($opt), $regs) && ($plz >= $regs [1] && $plz <= $regs [2])) ||
                            (preg_match("/^([0-9]+)-$/", trim($opt), $regs) && $plz >= $regs [1])
                        ) {
                            if (isset($opt['label'])) {
                                $basket->setShippingtypeExtraLabel((string)$opt['label']);
                            } else {
                                $basket->setShippingtypeExtraLabel("");
                            }
                            if(isset($opt['per-unit'])) {
                                return (((float)$opt['value'] + ($gewicht/1000*(float)$opt['per-unit'])) + ((((float)$opt['value'] + ($gewicht/1000*(float)$opt['per-unit'])) + $netto) / 100 * $this->kosten_percent));
                            }
                            return ((float)$opt['value'] + (((float)$opt['value'] + $netto) / 100 * $this->kosten_percent));
                        } elseif (strpos('u' . trim($opt), ',') >= 1) {
                            $matches = explode(',', trim($opt));

                            foreach ($matches as $row_calc) {
                                if ((preg_match("/^([0-9a-zA-Z]+)$/", trim($row_calc), $regs) && $regs[1] == $plz) ||
                                    (preg_match("/^([0-9]+)-([0-9]+)$/", trim($row_calc), $regs) && ($plz >= $regs[1] && $plz <= $regs[2])) ||
                                    (preg_match("/^([0-9]+)-$/", trim($row_calc), $regs) && $plz >= $regs[1])
                                ) {

                                    if (isset($opt['label'])) {
                                        $basket->setShippingtypeExtraLabel((string)$opt['label']);
                                    } else {
                                        $basket->setShippingtypeExtraLabel("");
                                    }
                                    if(isset($opt['per-unit'])) {
                                        return (((float)$opt['value'] + ($gewicht/1000*(float)$opt['per-unit'])) + ((((float)$opt['value'] + ($gewicht/1000*(float)$opt['per-unit'])) + $netto) / 100 * $this->kosten_percent));
                                    }
                                    return ((float)$opt['value'] + (((float)$opt['value'] + $netto) / 100 * $this->kosten_percent));
                                }
                            }
                        }
                    }

                }
            }

        } else {
            $basket->setShippingtypeExtraLabel("");
            return ($this->kosten_fix + (($this->kosten_fix + $netto) / 100 * $this->kosten_percent));
        }

    }

    /**
     * @return array
     */
    public function getPluginSettings($module, $name)
    {
        $this->loadData();
        if(!isset($this->pluginSettings[$module]) || !isset($this->pluginSettings[$module][$name])) {
            return null;
        }

        return $this->pluginSettings[$module][$name];
    }
}