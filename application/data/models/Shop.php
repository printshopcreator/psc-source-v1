<?php

/**
 * Shop Modelclass
 *
 * Shop Modelsuperclass
 *
 * PHP versions 4 and 5
 *
 * @category   Data
 * @package    Model
 * @subpackage Supermodel
 * @author     Thomas petersomn <tpeterson@printshopcreator.de>
 * @copyright  1997-2009 Thomas Peterson
 * @license    Not Public
 * @version    SVN: $Id: Shop.php 7313 2012-02-13 21:26:34Z boonkerz $
 * @link       http://www.printshopcreator.de
 */

/**
 * Shop Modelclass
 *
 * Shop Modelsuperclass
 *
 * PHP versions 4 and 5
 *
 * @category   Data
 * @package    Model
 * @subpackage Supermodel
 * @author     Thomas petersomn <tpeterson@printshopcreator.de>
 * @copyright  1997-2009 Thomas Peterson
 * @license    Not Public
 * @version    Release: $Id: Shop.php 7313 2012-02-13 21:26:34Z boonkerz $
 * @link       http://www.printshopcreator.de
 */
class Shop extends BaseShop
{

    protected $mongoLoaded = false;
    protected $defaultLocale = "de_DE";
    protected $defaultCurrency = "EUR";
    protected $contactOwnNumber = false;
    protected $contactNumberPattern;
    protected $contactNumberStart;

    protected $offerOwnNumber = false;
    protected $offerNumberPattern;
    protected $offerNumberStart;

    protected $ownNumber = false;
    protected $numberPattern;
    protected $numberStart;

    protected $apiKey;

    protected $docInvoiceNotInPackage = false;
    protected $docJobticketNotInPackage = false;
    protected $docOfferNotInPackage = false;
    protected $docOrderNotInPackage = false;
    protected $docStornoNotInPackage = false;
    protected $docLabelNotInPackage = false;
    protected $docDeliveryNotInPackage = false;
    protected $tpSaveUserData = false;
    protected $guestEnable = false;


    protected $redirectAfterBuy;
    protected $productFieldName1;
    protected $productFieldName2;
    protected $productFieldName3;
    protected $productFieldName4;
    protected $productFieldName5;
    protected $productFieldName6;
    protected $productFieldName7;
    protected $productFieldName8;
    protected $productFieldName9;
    protected $productFieldName10;
    protected $productFieldName11;
    protected $productFieldName12;

    protected $betreiberFax;

    protected $senderCompany;
    protected $senderFirstname;
    protected $senderLastname;
    protected $senderStreet;
    protected $senderHouseNumber;
    protected $senderZip;
    protected $senderCity;
    protected $senderCountry;
    protected $senderTel;
    protected $senderFax;
    protected $senderMobile;
    protected $senderEmail;

    protected $customerFieldName1;
    protected $customerFieldName2;
    protected $customerFieldName3;
    protected $customerFieldName4;
    protected $customerFieldName5;
    protected $customerFieldName6;
    protected $customerFieldName7;
    protected $customerFieldName8;
    protected $customerFieldName9;
    protected $customerFieldName10;
    protected $customerFieldName11;
    protected $customerFieldName12;

    protected $createPackageAfterBuy = true;

    protected $redirectToSSL = false;

    protected $imprintText;
    protected $privacyPolicyText;
    protected $cancellationTermsText;
    protected $conditionsText;

    protected $priceFactor = 1.0;

    protected $extraSettings;

    protected $theme;

    public function preSave($obj)
    {

        if ($this->uid == '') {
            $this->uid = TP_Util::uuid();
        }

        if ($this->identifier()) {
            $this->updated = date('Y-m-d H:i:s');
            if ($this->created == '') {
                $this->created = date('Y-m-d H:i:s');
            }
        } else {
            //$this->defaultfunc = "Overview";
            $this->useemailaslogin = true;
            $this->smtpusethis = false;
            $this->noverify = true;
            $this->created = date('Y-m-d H:i:s');
            $this->updated = date('Y-m-d H:i:s');

            if (!file_exists(PUBLIC_PATH . '/shops/' . $this->uid)) {
                mkdir(PUBLIC_PATH . '/shops/' . $this->uid);
            }
            if (!file_exists(PUBLIC_PATH . '/shops/' . $this->uid . '/images')) {
                mkdir(PUBLIC_PATH . '/shops/' . $this->uid . '/images');
            }
            if (!file_exists(PUBLIC_PATH . '/shops/' . $this->uid . '/images/cms')) {
                mkdir(PUBLIC_PATH . '/shops/' . $this->uid . '/images/cms');
            }
            if (!file_exists(PUBLIC_PATH . '/uploads/' . $this->uid)) {
                mkdir(PUBLIC_PATH . '/uploads/' . $this->uid);
            }
            if (!file_exists(PUBLIC_PATH . '/uploads/' . $this->uid . '/article')) {
                mkdir(PUBLIC_PATH . '/uploads/' . $this->uid . '/article');
            }
            if (!file_exists(PUBLIC_PATH . '/styles/' . $this->uid)) {
                mkdir(PUBLIC_PATH . '/styles/' . $this->uid);
            }
            if (!file_exists(PUBLIC_PATH . '/styles/' . $this->uid . '/design')) {
                mkdir(PUBLIC_PATH . '/styles/' . $this->uid . '/design');
            }
        }

        TP_Util::clearCache();
    }

    public function postSave($event)
    {


        //TP_Util::clearCache(TP_Util::CLEAR_ALL);
    }

    public function getRiakData()
    {
        return array(
            'name' => $this->name,
            'shop_id' => $this->id,
            'install_id' => $this->install_id,
            'title' => $this->title,
            'subtitle' => $this->subtitle,
            'display_market' => (bool) $this->display_market
        );
    }

    public function __toString()
    {
        return $this->name;
    }

    public function getRate()
    {
        if ($this->rate_count == 0) {
            return 0;
        }
        return round($this->rate / $this->rate_count, 0);
    }

    public function getLiveedit($field, $type = 'INPUT')
    {
        $mode = new Zend_Session_Namespace('adminmode');
        if ($mode->liveedit && $type == 'INPUT') {
            return '<div class="liveeditinput" table="shop" field="' . $field . '" id="' . $this->uid . '">' . $this->$field . '</div>';
        } elseif ($mode->liveedit && $type == 'RTE') {
            return '<div class="liveedit" table="shop" field="' . $field . '" id="' . $this->uid . '">' . $this->$field . '</div>';
        }

        return $this->$field;
    }

    public function getLogo1()
    {
        if ($this->logo1 != "") {
            return Doctrine_Query::create()->select()->from('Image i')->where('i.id = ?', array($this->logo1))->fetchOne();
        }
    }

    public function getLogo2()
    {
        if ($this->logo1 != "") {
            return Doctrine_Query::create()->select()->from('Image i')->where('i.id = ?', array($this->logo2))->fetchOne();
        }
    }

    /**
     * @return mixed
     */
    public function getOwnNumber()
    {
        $this->loadData();
        return $this->ownNumber;
    }

    /**
     * @param mixed $ownNumber
     */
    public function setOwnNumber($ownNumber)
    {
        $this->ownNumber = $ownNumber;
    }

    /**
     * @return mixed
     */
    public function getNumberStart()
    {
        $this->loadData();
        return $this->numberStart;
    }

    /**
     * @param mixed $numberStart
     */
    public function setNumberStart($numberStart)
    {
        $this->numberStart = $numberStart;
    }

    /**
     * @return mixed
     */
    public function getNumberPattern()
    {
        $this->loadData();
        return $this->numberPattern;
    }

    /**
     * @param mixed $numberPattern
     */
    public function setNumberPattern($numberPattern)
    {
        $this->numberPattern = $numberPattern;
    }

    /**
     * @return mixed
     */
    public function getContactNumberStart()
    {
        $this->loadData();
        return $this->contactNumberStart;
    }

    /**
     * @param mixed $contactNumberStart
     */
    public function setContactNumberStart($contactNumberStart)
    {
        $this->contactNumberStart = $contactNumberStart;
    }

    /**
     * @return mixed
     */
    public function getContactNumberPattern()
    {
        $this->loadData();
        return $this->contactNumberPattern;
    }

    /**
     * @param mixed $contactNumberPattern
     */
    public function setContactNumberPattern($contactNumberPattern)
    {
        $this->contactNumberPattern = $contactNumberPattern;
    }

    /**
     * @return mixed
     */
    public function getContactOwnNumber()
    {
        $this->loadData();
        return $this->contactOwnNumber;
    }

    /**
     * @return mixed
     */
    public function getDocInvoiceNotInPackage()
    {
        $this->loadData();
        return $this->docInvoiceNotInPackage;
    }

    /**
     * @return mixed
     */
    public function getDocJobticketNotInPackage()
    {
        $this->loadData();
        return $this->docJobticketNotInPackage;
    }

    /**
     * @return mixed
     */
    public function getDocOfferNotInPackage()
    {
        $this->loadData();
        return $this->docOfferNotInPackage;
    }

    /**
     * @return mixed
     */
    public function getDocOrderNotInPackage()
    {
        $this->loadData();
        return $this->docOrderNotInPackage;
    }

    /**
     * @return mixed
     */
    public function getDocLabelNotInPackage()
    {
        $this->loadData();
        return $this->docLabelNotInPackage;
    }

    /**
     * @return mixed
     */
    public function getDocDeliveryNotInPackage()
    {
        $this->loadData();
        return $this->docDeliveryNotInPackage;
    }



    /**
     * @param mixed $contactOwnNumber
     */
    public function setContactOwnNumber($contactOwnNumber)
    {
        $this->contactOwnNumber = $contactOwnNumber;
    }

    public function loadData()
    {
        if ($this->mongoLoaded) {
            return true;
        }

        $dbMongo = TP_Mongo::getInstance();
        $obj = $dbMongo->Shop->findOne(array('uid' => $this->id));
        if ($obj) {
            if(isset($obj['defaultLocale'])) {
                $this->defaultLocale = $obj['defaultLocale'];
            }
            if(isset($obj['defaultCurrency'])) {
                $this->defaultCurrency = $obj['defaultCurrency'];
            }
            $this->ownNumber = $obj['ownNumber'];
            $this->numberStart = $obj['numberStart'];
            $this->numberPattern = $obj['numberPattern'];
            $this->contactOwnNumber = $obj['contactOwnNumber'];
            $this->contactNumberPattern = $obj['contactNumberPattern'];
            $this->contactNumberStart = $obj['contactNumberStart'];
            $this->offerOwnNumber = $obj['offerOwnNumber'];
            $this->offerNumberPattern = $obj['offerNumberPattern'];
            $this->offerNumberStart = $obj['offerNumberStart'];
            $this->docInvoiceNotInPackage = $obj['docInvoiceNotInPackage'];
            $this->docJobticketNotInPackage = $obj['docJobticketNotInPackage'];
            $this->docOfferNotInPackage = $obj['docOfferNotInPackage'];
            $this->docOrderNotInPackage = $obj['docOrderNotInPackage'];
            $this->docStornoNotInPackage = $obj['docStornoNotInPackage'];
            $this->docLabelNotInPackage = $obj['docLabelNotInPackage'];
            $this->docDeliveryNotInPackage = $obj['docDeliveryNotInPackage'];
            $this->redirectAfterBuy = $obj['redirectAfterBuy'];
            $this->redirectToSSL = $obj['redirectToSSL'];
            $this->apiKey = $obj['apiKey'];
            $this->theme = $obj['theme'];
            $this->createPackageAfterBuy = $obj['createPackageAfterBuy'];
            $this->tpSaveUserData = $obj['tpSaveUserData'];
            $this->productFieldName1 = $obj['productFieldName1'];
            $this->productFieldName2 = $obj['productFieldName2'];
            $this->productFieldName3 = $obj['productFieldName3'];
            $this->productFieldName4 = $obj['productFieldName4'];
            $this->productFieldName5 = $obj['productFieldName5'];
            $this->productFieldName6 = $obj['productFieldName6'];
            $this->productFieldName7 = $obj['productFieldName7'];
            $this->productFieldName8 = $obj['productFieldName8'];
            $this->productFieldName9 = $obj['productFieldName9'];
            $this->productFieldName10 = $obj['productFieldName10'];
            $this->productFieldName11 = $obj['productFieldName11'];
            $this->productFieldName12 = $obj['productFieldName12'];
            $this->customerFieldName1 = $obj['customerFieldName1'];
            $this->customerFieldName2 = $obj['customerFieldName2'];
            $this->customerFieldName3 = $obj['customerFieldName3'];
            $this->customerFieldName4 = $obj['customerFieldName4'];
            $this->customerFieldName5 = $obj['customerFieldName5'];
            $this->customerFieldName6 = $obj['customerFieldName6'];
            $this->customerFieldName7 = $obj['customerFieldName7'];
            $this->customerFieldName8 = $obj['customerFieldName8'];
            $this->customerFieldName9 = $obj['customerFieldName9'];
            $this->customerFieldName10 = $obj['customerFieldName10'];
            $this->customerFieldName11 = $obj['customerFieldName11'];
            $this->customerFieldName12 = $obj['customerFieldName12'];
            $this->betreiberFax = $obj['betreiberFax'];
            $this->imprintText = $obj['imprintText'];
            $this->cancellationTermsText = $obj['cancellationTermsText'];
            $this->privacyPolicyText = $obj['privacyPolicyText'];
            $this->conditionsText = $obj['conditionsText'];
            $this->senderCompany = $obj['senderCompany'];
            $this->senderFirstname = $obj['senderFirstname'];
            $this->senderLastname = $obj['senderLastname'];
            $this->senderStreet = $obj['senderStreet'];
            $this->senderHouseNumber = $obj['senderHouseNumber'];
            $this->senderZip = $obj['senderZip'];
            $this->senderCity = $obj['senderCity'];
            $this->senderCountry = $obj['senderCountry'];
            $this->senderTel = $obj['senderTel'];
            $this->senderFax = $obj['senderFax'];
            $this->senderMobile = $obj['senderMobile'];
            $this->senderEmail = $obj['senderEmail'];
            $this->extraSettings = $obj['extraSettings'];
            $this->guestEnable = boolval($obj['guestEnable']);
            $this->priceFactor = $obj['priceFactor'];

            if(!$this->priceFactor) {
                $this->priceFactor = 1;
            }

            $this->mongoLoaded = true;
        }
    }

    public function getArray()
    {
        return array(
            'uid' => $this->id,
            'ownNumber' => $this->ownNumber,
            'numberStart' => $this->numberStart,
            'numberPattern' => $this->numberPattern,
            'contactOwnNumber' => $this->contactOwnNumber,
            'contactNumberPattern' => $this->contactNumberPattern,
            'contactNumberStart' => $this->contactNumberStart,
            'offerOwnNumber' => $this->offerOwnNumber,
            'offerNumberPattern' => $this->offerNumberPattern,
            'offerNumberStart' => $this->offerNumberStart,
            'docInvoiceNotInPackage' => $this->docInvoiceNotInPackage,
            'docJobticketNotInPackage' => $this->docJobticketNotInPackage,
            'docOfferNotInPackage' => $this->docOfferNotInPackage,
            'docOrderNotInPackage' => $this->docOrderNotInPackage,
            'docStornoNotInPackage' => $this->docStornoNotInPackage,
            'docLabelNotInPackage' => $this->docLabelNotInPackage,
            'docDeliveryNotInPackage' => $this->docDeliveryNotInPackage,
            'redirectAfterBuy' => $this->redirectAfterBuy,
            'redirectToSSL' => $this->redirectToSSL,
            'apiKey' => $this->apiKey,
            'theme' => $this->theme,
            'createPackageAfterBuy' => $this->createPackageAfterBuy,
            'productFieldName1' => $this->productFieldName1,
            'productFieldName2' => $this->productFieldName2,
            'productFieldName3' => $this->productFieldName3,
            'productFieldName4' => $this->productFieldName4,
            'productFieldName5' => $this->productFieldName5,
            'productFieldName6' => $this->productFieldName6,
            'productFieldName7' => $this->productFieldName7,
            'productFieldName8' => $this->productFieldName8,
            'productFieldName9' => $this->productFieldName9,
            'productFieldName10' => $this->productFieldName10,
            'productFieldName11' => $this->productFieldName11,
            'productFieldName12' => $this->productFieldName12,
            'customerFieldName1' => $this->customerFieldName1,
            'customerFieldName2' => $this->customerFieldName2,
            'customerFieldName3' => $this->customerFieldName3,
            'customerFieldName4' => $this->customerFieldName4,
            'customerFieldName5' => $this->customerFieldName5,
            'customerFieldName6' => $this->customerFieldName6,
            'customerFieldName7' => $this->customerFieldName7,
            'customerFieldName8' => $this->customerFieldName8,
            'customerFieldName9' => $this->customerFieldName9,
            'customerFieldName10' => $this->customerFieldName10,
            'customerFieldName11' => $this->customerFieldName11,
            'customerFieldName12' => $this->customerFieldName12,
            'betreiberFax' => $this->betreiberFax,
            'imprintText' => $this->imprintText,
            'cancellationTermsText' => $this->cancellationTermsText,
            'privacyPolicyText' => $this->privacyPolicyText,
            'conditionsText' => $this->conditionsText,
            'extraSettings' => $this->extraSettings,
            'guestEnable' => $this->guestEnable,
            'defaultLocale' => $this->defaultLocale,
            'defaultCurrency' => $this->defaultCurrency
        );
    }

    public function saveMongo()
    {
        $dbMongo = TP_Mongo::getInstance();

        $obj = $dbMongo->Shop->findOne(array('uid' => $this->id));
        if($obj) {
            $dbMongo->Shop->updateOne(array('uid' => $this->id), [ '$set' =>  array_merge((array)$obj, $this->getArray())]);
        }else{
            $dbMongo->Shop->insertOne($this->getArray());
        }
    }

    /**
     * @return mixed
     */
    public function getApiKey()
    {
        $this->loadData();
        return $this->apiKey;
    }

    /**
     * @return mixed
     */
    public function getRedirectAfterBuy()
    {
        $this->loadData();
        return $this->redirectAfterBuy;
    }

    /**
     * @param mixed $redirectAfterBuy
     */
    public function setRedirectAfterBuy($redirectAfterBuy)
    {
        $this->redirectAfterBuy = $redirectAfterBuy;
    }

    /**
     * @return boolean
     */
    public function isDocStornoNotInPackage()
    {
        $this->loadData();
        return $this->docStornoNotInPackage;
    }

    /**
     * @param boolean $docStornoNotInPackage
     */
    public function setDocStornoNotInPackage($docStornoNotInPackage)
    {
        $this->docStornoNotInPackage = $docStornoNotInPackage;
    }

    /**
     * @return boolean
     */
    public function isCreatePackageAfterBuy()
    {
        $this->loadData();
        return $this->createPackageAfterBuy;
    }

    /**
     * @param boolean $createPackageAfterBuy
     */
    public function setCreatePackageAfterBuy($createPackageAfterBuy)
    {
        $this->createPackageAfterBuy = $createPackageAfterBuy;
    }

    /**
     * @return mixed
     */
    public function getTheme()
    {
        $this->loadData();
        return $this->theme;
    }

    /**
     * @param mixed $theme
     */
    public function setTheme($theme)
    {
        $this->theme = $theme;
    }

    /**
     * @return boolean
     */
    public function isRedirectToSSL()
    {
        $this->loadData();
        return $this->redirectToSSL;
    }

    /**
     * @param boolean $redirectToSSL
     */
    public function setRedirectToSSL($redirectToSSL)
    {
        $this->redirectToSSL = $redirectToSSL;
    }

    /**
     * @return mixed
     */
    public function getProductFieldName1()
    {
        $this->loadData();
        return $this->productFieldName1;
    }

    /**
     * @param mixed $productFieldName1
     */
    public function setProductFieldName1($productFieldName1)
    {
        $this->productFieldName1 = $productFieldName1;
    }

    /**
     * @return mixed
     */
    public function getProductFieldName2()
    {
        $this->loadData();
        return $this->productFieldName2;
    }

    /**
     * @param mixed $productFieldName2
     */
    public function setProductFieldName2($productFieldName2)
    {
        $this->productFieldName2 = $productFieldName2;
    }

    /**
     * @return mixed
     */
    public function getProductFieldName3()
    {
        $this->loadData();
        return $this->productFieldName3;
    }

    /**
     * @param mixed $productFieldName3
     */
    public function setProductFieldName3($productFieldName3)
    {
        $this->productFieldName3 = $productFieldName3;
    }

    /**
     * @return mixed
     */
    public function getProductFieldName4()
    {
        $this->loadData();
        return $this->productFieldName4;
    }

    /**
     * @param mixed $productFieldName4
     */
    public function setProductFieldName4($productFieldName4)
    {
        $this->productFieldName4 = $productFieldName4;
    }

    /**
     * @return mixed
     */
    public function getProductFieldName5()
    {
        $this->loadData();
        return $this->productFieldName5;
    }

    /**
     * @param mixed $productFieldName5
     */
    public function setProductFieldName5($productFieldName5)
    {
        $this->productFieldName5 = $productFieldName5;
    }

    /**
     * @return mixed
     */
    public function getProductFieldName6()
    {
        $this->loadData();
        return $this->productFieldName6;
    }

    /**
     * @param mixed $productFieldName6
     */
    public function setProductFieldName6($productFieldName6)
    {
        $this->productFieldName6 = $productFieldName6;
    }

    /**
     * @return mixed
     */
    public function getProductFieldName7()
    {
        $this->loadData();
        return $this->productFieldName7;
    }

    /**
     * @param mixed $productFieldName7
     */
    public function setProductFieldName7($productFieldName7)
    {
        $this->productFieldName7 = $productFieldName7;
    }

    /**
     * @return mixed
     */
    public function getProductFieldName8()
    {
        $this->loadData();
        return $this->productFieldName8;
    }

    /**
     * @param mixed $productFieldName8
     */
    public function setProductFieldName8($productFieldName8)
    {
        $this->productFieldName8 = $productFieldName8;
    }

    /**
     * @return mixed
     */
    public function getProductFieldName9()
    {
        $this->loadData();
        return $this->productFieldName9;
    }

    /**
     * @param mixed $productFieldName9
     */
    public function setProductFieldName9($productFieldName9)
    {
        $this->productFieldName9 = $productFieldName9;
    }

    /**
     * @return mixed
     */
    public function getProductFieldName10()
    {
        $this->loadData();
        return $this->productFieldName10;
    }

    /**
     * @param mixed $productFieldName10
     */
    public function setProductFieldName10($productFieldName10)
    {
        $this->productFieldName10 = $productFieldName10;
    }

    /**
     * @return mixed
     */
    public function getProductFieldName11()
    {
        $this->loadData();
        return $this->productFieldName11;
    }

    /**
     * @param mixed $productFieldName11
     */
    public function setProductFieldName11($productFieldName11)
    {
        $this->productFieldName11 = $productFieldName11;
    }

    /**
     * @return mixed
     */
    public function getProductFieldName12()
    {
        $this->loadData();
        return $this->productFieldName12;
    }

    /**
     * @param mixed $productFieldName12
     */
    public function setProductFieldName12($productFieldName12)
    {
        $this->productFieldName12 = $productFieldName12;
    }

    /**
     * @return mixed
     */
    public function getCustomerFieldName1()
    {
        $this->loadData();
        return $this->customerFieldName1;
    }

    /**
     * @param mixed $customerFieldName1
     */
    public function setCustomerFieldName1($customerFieldName1)
    {
        $this->customerFieldName1 = $customerFieldName1;
    }

    /**
     * @return mixed
     */
    public function getCustomerFieldName2()
    {
        $this->loadData();
        return $this->customerFieldName2;
    }

    /**
     * @param mixed $customerFieldName2
     */
    public function setCustomerFieldName2($customerFieldName2)
    {
        $this->customerFieldName2 = $customerFieldName2;
    }

    /**
     * @return mixed
     */
    public function getCustomerFieldName3()
    {
        $this->loadData();
        return $this->customerFieldName3;
    }

    /**
     * @param mixed $customerFieldName3
     */
    public function setCustomerFieldName3($customerFieldName3)
    {
        $this->customerFieldName3 = $customerFieldName3;
    }

    /**
     * @return mixed
     */
    public function getCustomerFieldName4()
    {
        $this->loadData();
        return $this->customerFieldName4;
    }

    /**
     * @param mixed $customerFieldName4
     */
    public function setCustomerFieldName4($customerFieldName4)
    {
        $this->customerFieldName4 = $customerFieldName4;
    }

    /**
     * @return mixed
     */
    public function getCustomerFieldName5()
    {
        $this->loadData();
        return $this->customerFieldName5;
    }

    /**
     * @param mixed $customerFieldName5
     */
    public function setCustomerFieldName5($customerFieldName5)
    {
        $this->customerFieldName5 = $customerFieldName5;
    }

    /**
     * @return mixed
     */
    public function getCustomerFieldName6()
    {
        $this->loadData();
        return $this->customerFieldName6;
    }

    /**
     * @param mixed $customerFieldName6
     */
    public function setCustomerFieldName6($customerFieldName6)
    {
        $this->customerFieldName6 = $customerFieldName6;
    }

    /**
     * @return mixed
     */
    public function getCustomerFieldName7()
    {
        $this->loadData();
        return $this->customerFieldName7;
    }

    /**
     * @param mixed $customerFieldName7
     */
    public function setCustomerFieldName7($customerFieldName7)
    {
        $this->customerFieldName7 = $customerFieldName7;
    }

    /**
     * @return mixed
     */
    public function getCustomerFieldName8()
    {
        $this->loadData();
        return $this->customerFieldName8;
    }

    /**
     * @param mixed $customerFieldName8
     */
    public function setCustomerFieldName8($customerFieldName8)
    {
        $this->customerFieldName8 = $customerFieldName8;
    }

    /**
     * @return mixed
     */
    public function getCustomerFieldName9()
    {
        $this->loadData();
        return $this->customerFieldName9;
    }

    /**
     * @param mixed $customerFieldName9
     */
    public function setCustomerFieldName9($customerFieldName9)
    {
        $this->customerFieldName9 = $customerFieldName9;
    }

    /**
     * @return mixed
     */
    public function getCustomerFieldName10()
    {
        $this->loadData();
        return $this->customerFieldName10;
    }

    /**
     * @param mixed $customerFieldName10
     */
    public function setCustomerFieldName10($customerFieldName10)
    {
        $this->customerFieldName10 = $customerFieldName10;
    }

    /**
     * @return mixed
     */
    public function getCustomerFieldName11()
    {
        $this->loadData();
        return $this->customerFieldName11;
    }

    /**
     * @param mixed $customerFieldName11
     */
    public function setCustomerFieldName11($customerFieldName11)
    {
        $this->customerFieldName11 = $customerFieldName11;
    }

    /**
     * @return mixed
     */
    public function getCustomerFieldName12()
    {
        $this->loadData();
        return $this->customerFieldName12;
    }

    /**
     * @param mixed $customerFieldName12
     */
    public function setCustomerFieldName12($customerFieldName12)
    {
        $this->customerFieldName12 = $customerFieldName12;
    }

    /**
     * @return mixed
     */
    public function getImprintText()
    {
        $this->loadData();
        return $this->imprintText;
    }

    /**
     * @param mixed $imprintText
     */
    public function setImprintText($imprintText)
    {
        $this->imprintText = $imprintText;
    }

    /**
     * @return mixed
     */
    public function getPrivacyPolicyText()
    {
        $this->loadData();
        return $this->privacyPolicyText;
    }

    /**
     * @param mixed $privacyPolicyText
     */
    public function setPrivacyPolicyText($privacyPolicyText)
    {
        $this->privacyPolicyText = $privacyPolicyText;
    }

    /**
     * @return mixed
     */
    public function getCancellationTermsText()
    {
        $this->loadData();
        return $this->cancellationTermsText;
    }

    /**
     * @param mixed $cancellationTermsText
     */
    public function setCancellationTermsText($cancellationTermsText)
    {
        $this->cancellationTermsText = $cancellationTermsText;
    }

    /**
     * @return mixed
     */
    public function getConditionsText()
    {
        $this->loadData();
        return $this->conditionsText;
    }

    /**
     * @param mixed $conditionsText
     */
    public function setConditionsText($conditionsText)
    {
        $this->conditionsText = $conditionsText;
    }

    /**
     * @return mixed
     */
    public function getBetreiberFax()
    {
        $this->loadData();
        return $this->betreiberFax;
    }

    /**
     * @param mixed $betreiberFax
     */
    public function setBetreiberFax($betreiberFax)
    {
        $this->betreiberFax = $betreiberFax;
    }

    /**
     * @return mixed
     */
    public function getSenderCompany()
    {
        $this->loadData();
        return $this->senderCompany;
    }

    /**
     * @param mixed $senderCompany
     */
    public function setSenderCompany($senderCompany)
    {
        $this->senderCompany = $senderCompany;
    }

    /**
     * @return mixed
     */
    public function getSenderFirstname()
    {
        $this->loadData();
        return $this->senderFirstname;
    }

    /**
     * @param mixed $senderFirstname
     */
    public function setSenderFirstname($senderFirstname)
    {
        $this->senderFirstname = $senderFirstname;
    }

    /**
     * @return mixed
     */
    public function getSenderLastname()
    {
        $this->loadData();
        return $this->senderLastname;
    }

    /**
     * @param mixed $senderLastname
     */
    public function setSenderLastname($senderLastname)
    {
        $this->senderLastname = $senderLastname;
    }

    /**
     * @return mixed
     */
    public function getSenderStreet()
    {
        $this->loadData();
        return $this->senderStreet;
    }

    /**
     * @param mixed $senderStreet
     */
    public function setSenderStreet($senderStreet)
    {
        $this->senderStreet = $senderStreet;
    }

    /**
     * @return mixed
     */
    public function getSenderHouseNumber()
    {
        $this->loadData();
        return $this->senderHouseNumber;
    }

    /**
     * @param mixed $senderHouseNumber
     */
    public function setSenderHouseNumber($senderHouseNumber)
    {
        $this->senderHouseNumber = $senderHouseNumber;
    }

    /**
     * @return mixed
     */
    public function getSenderZip()
    {
        $this->loadData();
        return $this->senderZip;
    }

    /**
     * @param mixed $senderZip
     */
    public function setSenderZip($senderZip)
    {
        $this->senderZip = $senderZip;
    }

    /**
     * @return mixed
     */
    public function getSenderCity()
    {
        $this->loadData();
        return $this->senderCity;
    }

    /**
     * @param mixed $senderCity
     */
    public function setSenderCity($senderCity)
    {
        $this->senderCity = $senderCity;
    }

    /**
     * @return mixed
     */
    public function getSenderCountry()
    {
        $this->loadData();
        return $this->senderCountry;
    }

    /**
     * @param mixed $senderCountry
     */
    public function setSenderCountry($senderCountry)
    {
        $this->senderCountry = $senderCountry;
    }

    /**
     * @return mixed
     */
    public function getSenderTel()
    {
        $this->loadData();
        return $this->senderTel;
    }

    /**
     * @param mixed $senderTel
     */
    public function setSenderTel($senderTel)
    {
        $this->senderTel = $senderTel;
    }

    /**
     * @return mixed
     */
    public function getSenderFax()
    {
        $this->loadData();
        return $this->senderFax;
    }

    /**
     * @param mixed $senderFax
     */
    public function setSenderFax($senderFax)
    {
        $this->senderFax = $senderFax;
    }

    /**
     * @return mixed
     */
    public function getSenderMobile()
    {
        $this->loadData();
        return $this->senderMobile;
    }

    /**
     * @param mixed $senderMobile
     */
    public function setSenderMobile($senderMobile)
    {
        $this->senderMobile = $senderMobile;
    }

    /**
     * @return mixed
     */
    public function getSenderEmail()
    {
        $this->loadData();
        return $this->senderEmail;
    }

    /**
     * @param mixed $senderEmail
     */
    public function setSenderEmail($senderEmail)
    {
        $this->senderEmail = $senderEmail;
    }


    public function getOrderSenderSaveArray()
    {

        $arr = [];
        $arr['firstname'] = $this->getSenderFirstname();
        $arr['lastname'] = $this->getSenderLastname();
        $arr['street'] = $this->getSenderStreet();
        $arr['houseNumber'] = $this->getSenderHouseNumber();
        $arr['zip'] = $this->getSenderZip();
        $arr['city'] = $this->getSenderCity();
        $arr['fax'] = $this->getSenderFax();
        $arr['email'] = $this->getSenderEmail();
        $arr['phone'] = $this->getSenderTel();
        $arr['mobilPhone'] = $this->getSenderMobile();
        $arr['company'] = $this->getSenderCompany();
        $arr['country'] = $this->getSenderCountry();

        $arr['type'] = 3;
        $arr['_doctrine_class_name'] = "PSC\\Shop\\EntityBundle\\Document\\Embed\\ContactAddress";

        return $arr;
    }

    public function getOrderSenderSavedAsObject()
    {
        $address = new ContactAddress();
        $address->firstname = $this->getSenderFirstname();
        $address->lastname = $this->getSenderLastname();
        $address->company = $this->getSenderCompany();
        $address->zip = $this->getSenderZip();
        $address->city = $this->getSenderCity();
        $address->street = $this->getSenderStreet();
        $address->house_number = $this->getSenderHouseNumber();
        $address->country = $this->getSenderCountry();
        $address->email = $this->getSenderEmail();

        return $address;
    }

    /**
     * @return bool
     */
    public function isTpSaveUserData()
    {
        $this->loadData();
        return $this->tpSaveUserData;
    }

    /**
     * @param bool $tpSaveUserData
     */
    public function setTpSaveUserData($tpSaveUserData)
    {
        $this->tpSaveUserData = $tpSaveUserData;
    }

    /**
     * @return mixed
     */
    public function getExtraSettings()
    {
        $this->loadData();
        return $this->extraSettings;
    }

    /**
     * @param mixed $extraSettings
     */
    public function setExtraSettings($extraSettings)
    {
        $this->extraSettings = $extraSettings;
    }

    /**
     * @return bool
     */
    public function isOfferOwnNumber()
    {
        $this->loadData();
        return $this->offerOwnNumber;
    }

    /**
     * @param bool $offerOwnNumber
     */
    public function setOfferOwnNumber($offerOwnNumber)
    {
        $this->offerOwnNumber = $offerOwnNumber;
    }

    /**
     * @return mixed
     */
    public function getOfferNumberPattern()
    {
        $this->loadData();
        return $this->offerNumberPattern;
    }

    /**
     * @param mixed $offerNumberPattern
     */
    public function setOfferNumberPattern($offerNumberPattern)
    {
        $this->offerNumberPattern = $offerNumberPattern;
    }

    /**
     * @return mixed
     */
    public function getOfferNumberStart()
    {
        $this->loadData();
        return $this->offerNumberStart;
    }

    /**
     * @param mixed $offerNumberStart
     */
    public function setOfferNumberStart($offerNumberStart)
    {
        $this->offerNumberStart = $offerNumberStart;
    }

    /**
     * @return float
     */
    public function getPriceFactor(): float
    {
        $this->loadData();
        return $this->priceFactor;
    }

    /**
     * @param float $priceFactor
     */
    public function setPriceFactor(float $priceFactor): void
    {
        $this->priceFactor = $priceFactor;
    }

    /**
     * @return bool
     */
    public function isGuestEnable(): bool
    {
        $this->loadData();
        return $this->guestEnable;
    }

    /**
     * @param bool $guestEnable
     */
    public function setGuestEnable(bool $guestEnable): void
    {
        $this->guestEnable = $guestEnable;
    }
    
    public function getDefaultLocale(): string
    {
        $this->loadData();
        if(strlen($this->defaultLocale) === 2) {
            return $this->defaultLocale . '_' . strtoupper($this->defaultLocale);
        }
        return $this->defaultLocale;
    }

    public function getDefaultCurrency(): string
    {
        $this->loadData();
        return $this->defaultCurrency;
    }
}

