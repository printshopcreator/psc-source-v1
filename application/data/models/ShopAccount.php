<?php

/**
 * ShopAccount Modelclass
 *
 * ShopAccount Modelsuperclass
 *
 * PHP versions 4 and 5
 *
 * @category   Data
 * @package    Model
 * @subpackage Supermodel
 * @author     Thomas petersomn <tpeterson@printshopcreator.de>
 * @copyright  1997-2009 Thomas Peterson
 * @license    Not Public
 * @version    SVN: $Id: ShopAccount.php 5162 2010-11-06 01:42:44Z boonkerz $
 * @link       http://www.printshopcreator.de
 */

/**
 * ShopAccount Modelclass
 *
 * ShopAccount Modelsuperclass
 *
 * PHP versions 4 and 5
 *
 * @category   Data
 * @package    Model
 * @subpackage Supermodel
 * @author     Thomas petersomn <tpeterson@printshopcreator.de>
 * @copyright  1997-2009 Thomas Peterson
 * @license    Not Public
 * @version    Release: $Id: ShopAccount.php 5162 2010-11-06 01:42:44Z boonkerz $
 * @link       http://www.printshopcreator.de
 */

class ShopAccount extends BaseShopAccount
{

}