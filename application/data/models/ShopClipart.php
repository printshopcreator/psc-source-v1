<?php

/**
 * ShopClipart Modelclass
 *
 * ShopClipart Modelsuperclass
 *
 * PHP versions 4 and 5
 *
 * @category   Data
 * @package    Model
 * @subpackage Supermodel
 * @author     Thomas petersomn <tpeterson@printshopcreator.de>
 * @copyright  1997-2009 Thomas Peterson
 * @license    Not Public
 * @version    SVN: $Id: ShopClipart.php 6282 2011-04-27 11:56:43Z boonkerz $
 * @link       http://www.printshopcreator.de
 */

/**
 * ShopClipart Modelclass
 *
 * ShopClipart Modelsuperclass
 *
 * PHP versions 4 and 5
 *
 * @category   Data
 * @package    Model
 * @subpackage Supermodel
 * @author     Thomas petersomn <tpeterson@printshopcreator.de>
 * @copyright  1997-2009 Thomas Peterson
 * @license    Not Public
 * @version    Release: $Id: ShopClipart.php 6282 2011-04-27 11:56:43Z boonkerz $
 * @link       http://www.printshopcreator.de
 */

class ShopClipart extends BaseShopClipart
{

}