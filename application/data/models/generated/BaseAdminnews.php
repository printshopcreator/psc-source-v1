<?php

abstract class BaseAdminnews extends Doctrine_Record
{
    public function setTableDefinition() {
        $this->setTableName('admin_news');

        $this->hasColumn('id', 'varchar', 255, array(
            'primary' => true,
        ));
        $this->hasColumn('message', 'string', 255);
        $this->hasColumn('created', 'timestamp', 255);
        $this->hasColumn('status', 'integer', 1);
    }
}