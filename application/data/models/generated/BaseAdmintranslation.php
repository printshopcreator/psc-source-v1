<?php

abstract class BaseAdmintranslation extends Doctrine_Record
{
    public function setTableDefinition() {
        $this->setTableName('admin_translation');

        $this->hasColumn('id', 'varchar', 255, array(
            'primary' => true,
        ));
        $this->hasColumn('en', 'string', 255);
        $this->hasColumn('de', 'string', 255);

    }
}