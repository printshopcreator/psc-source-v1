<?php

abstract class BaseAdminusersettings extends Doctrine_Record
{
    public function setTableDefinition() {
        $this->setTableName('admin_user_settings');

        $this->hasColumn('id', 'varchar', 255, array(
            'primary' => true,
        ));
        $this->hasColumn('user_id', 'integer', 8);
        $this->hasColumn('name', 'string', 255);
        $this->hasColumn('data', 'string', 99999999999999);

    }
}