<?php

/**
 * BaseArticleReleated
 *
 * This class has been auto-generated by the Doctrine ORM Framework
 *
 * @property integer $article1
 * @property integer $article2
 * @property Article $Article
 * @property Motiv $Motiv
 *
 * @package    ##PACKAGE##
 * @subpackage ##SUBPACKAGE##
 * @author     ##NAME## <##EMAIL##>
 * @version    SVN: $Id: BaseArticleReleated.php 5162 2010-11-06 01:42:44Z boonkerz $
 */
abstract class BaseArticleReleated extends Doctrine_Record
{
    public function setTableDefinition() {
        $this->setTableName('base_article_releated');
        $this->hasColumn('article1', 'integer', 2147483647, array(
            'primary' => true,
            'type' => 'integer',
            'length' => '2147483647',
        ));
        $this->hasColumn('article2', 'integer', 2147483647, array(
            'primary' => true,
            'type' => 'integer',
            'length' => '2147483647',
        ));
    }

    public function setUp() {
        parent::setUp();
        $this->hasOne('Article', array(
            'local' => 'article1',
            'foreign' => 'id'));

    }
}