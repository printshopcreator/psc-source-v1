<?php

/**
 * BaseArticleThemeArticle
 *
 * This class has been auto-generated by the Doctrine ORM Framework
 *
 * @property integer $theme_id
 * @property integer $article_id
 * @property Article $Article
 * @property ArticleTheme $ArticleTheme
 * @property Motiv $Motiv
 *
 * @package    ##PACKAGE##
 * @subpackage ##SUBPACKAGE##
 * @author     ##NAME## <##EMAIL##>
 * @version    SVN: $Id: BaseArticleThemeArticle.php 5162 2010-11-06 01:42:44Z boonkerz $
 */
abstract class BaseArticleThemeArticle extends Doctrine_Record
{
    public function setTableDefinition() {
        $this->setTableName('article_theme_article');
        $this->hasColumn('theme_id', 'integer', 8, array(
            'type' => 'integer',
            'primary' => true,
            'length' => '8',
        ));
        $this->hasColumn('article_id', 'integer', 8, array(
            'type' => 'integer',
            'primary' => true,
            'length' => '8',
        ));
    }

    public function setUp() {
        parent::setUp();
        $this->hasOne('Article', array(
            'local' => 'article_id',
            'foreign' => 'id'));

        $this->hasOne('ArticleTheme', array(
            'local' => 'theme_id',
            'foreign' => 'id'));

    }
}