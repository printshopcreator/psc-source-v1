<?php

abstract class BaseArticlebuy extends Doctrine_Record
{
    public function setTableDefinition() {
        $this->setTableName('article_buy');

        $this->hasColumn('id', 'integer', 8, array('primary' => true, 'autoincrement' => true, 'type' => 'integer', 'length' => '8'));
        $this->hasColumn('article_id', 'integer', 8);
        $this->hasColumn('contact_id', 'integer', 8);

    }
}