<?php

/**
 * BaseMotivTheme
 *
 * This class has been auto-generated by the Doctrine ORM Framework
 *
 * @property integer $id
 * @property integer $install_id
 * @property integer $shop_id
 * @property timestamp $created
 * @property timestamp $updated
 * @property string $title
 * @property string $uuid
 * @property string $url
 * @property integer $parent_id
 * @property Install $Install
 * @property Shop $Shop
 * @property MotivTheme $MotivTheme
 * @property Doctrine_Collection $MotivThemeMotiv
 *
 * @package    ##PACKAGE##
 * @subpackage ##SUBPACKAGE##
 * @author     ##NAME## <##EMAIL##>
 * @version    SVN: $Id: BaseClipartTheme.php 5162 2010-11-06 01:42:44Z boonkerz $
 */
abstract class BaseClipartTheme extends Doctrine_Record
{
    public function setTableDefinition() {
        $this->setTableName('clipart_theme');
        $this->hasColumn('id', 'integer', 8, array(
            'primary' => true,
            'autoincrement' => true,
            'type' => 'integer',
            'length' => '8',
        ));
        $this->hasColumn('install_id', 'integer', 8, array(
            'type' => 'integer',
            'length' => '8',
        ));

        $this->hasColumn('created', 'timestamp', 25, array(
            'type' => 'timestamp',
            'length' => '25',
        ));
        $this->hasColumn('updated', 'timestamp', 25, array(
            'type' => 'timestamp',
            'length' => '25',
        ));
        $this->hasColumn('title', 'string', 255, array(
            'type' => 'string',
            'length' => '255',
        ));
        $this->hasColumn('uuid', 'string', 100, array(
            'type' => 'string',
            'length' => '100',
        ));
        $this->hasColumn('url', 'string', 255, array(
            'type' => 'string',
            'length' => '255',
        ));
        $this->hasColumn('parent_id', 'integer', 8, array(
            'type' => 'integer',
            'length' => '8',
        ));
    }

    public function setUp() {
        parent::setUp();
        $this->hasOne('Install', array(
            'local' => 'install_id',
            'foreign' => 'id'));

        $this->hasOne('ClipartTheme', array(
            'local' => 'parent_id',
            'foreign' => 'id'));

        $sluggable0 = new Doctrine_Template_Sluggable(array(
            'unique' => true,
            'fields' =>
            array(
                0 => 'title',
            ),
            'name' => 'url',
            'canUpdate' => true,
        ));
        $this->actAs($sluggable0);
    }
}