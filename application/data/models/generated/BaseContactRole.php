<?php

/**
 * BaseContactRole
 *
 * This class has been auto-generated by the Doctrine ORM Framework
 *
 * @property integer $role_id
 * @property integer $contact_id
 * @property timestamp $created
 * @property timestamp $updated
 * @property Contact $Contact
 * @property Role $Role
 *
 * @package    ##PACKAGE##
 * @subpackage ##SUBPACKAGE##
 * @author     ##NAME## <##EMAIL##>
 * @version    SVN: $Id: BaseContactRole.php 5162 2010-11-06 01:42:44Z boonkerz $
 */
abstract class BaseContactRole extends Doctrine_Record
{
    public function setTableDefinition() {
        $this->setTableName('contact_role');
        $this->hasColumn('role_id', 'integer', 8, array(
            'type' => 'integer',
            'primary' => true,
            'length' => '8',
        ));
        $this->hasColumn('contact_id', 'integer', 8, array(
            'type' => 'integer',
            'primary' => true,
            'length' => '8',
        ));
        $this->hasColumn('created', 'timestamp', 25, array(
            'type' => 'timestamp',
            'length' => '25',
        ));
        $this->hasColumn('updated', 'timestamp', 25, array(
            'type' => 'timestamp',
            'length' => '25',
        ));
    }

    public function setUp() {
        parent::setUp();
        $this->hasOne('Contact', array(
            'local' => 'contact_id',
            'foreign' => 'id'));

        $this->hasOne('Role', array(
            'local' => 'role_id',
            'foreign' => 'id'));
    }
}