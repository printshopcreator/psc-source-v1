<?php

abstract class BaseFrontendtranslation extends Doctrine_Record
{
    public function setTableDefinition() {
        $this->setTableName('frontend_translation');

        $this->hasColumn('id', 'integer', 8, array('primary' => true, 'autoincrement' => true, 'type' => 'integer', 'length' => '8'));
        $this->hasColumn('message_id', 'string', 255);
        $this->hasColumn('text', 'string', 2147483647);
        $this->hasColumn('shop_id', 'integer', 8);
        $this->hasColumn('locale', 'string', 255);

    }
}