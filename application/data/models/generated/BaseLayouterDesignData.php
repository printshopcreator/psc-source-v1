<?php

abstract class BaseLayouterDesignData extends Doctrine_Record
{

    public function setTableDefinition() {
        $this->setTableName('layouter_design_data');
        $this->hasColumn('id', 'integer', 8, array('primary' => true, 'autoincrement' => true, 'type' => 'integer', 'length' => '8'));
        $this->hasColumn('uuid', 'string', 40, array('type' => 'string', 'length' => '40'));
        $this->hasColumn('design', 'string', 9999999999);
    }

    public function setUp() {
        parent::setUp();


    }
}