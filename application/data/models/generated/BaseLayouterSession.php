<?php

abstract class BaseLayouterSession extends Doctrine_Record
{

    public function setTableDefinition() {
        $this->setTableName('layouter_session');
        $this->hasColumn('id', 'integer', 8, array('primary' => true, 'autoincrement' => true, 'type' => 'integer', 'length' => '8'));
        $this->hasColumn('uuid', 'string', 40, array('type' => 'string', 'length' => '40'));
        $this->hasColumn('org_article_id', 'string', 40, array('type' => 'string', 'length' => '40'));
        $this->hasColumn('created', 'timestamp', null, array('type' => 'timestamp'));
        $this->hasColumn('updated', 'timestamp', null, array('type' => 'timestamp'));
        $this->hasColumn('contact_id', 'integer', 8, array('type' => 'integer', 'length' => '8'));
        $this->hasColumn('xmlconfig', 'string', 255, array('type' => 'string', 'length' => '255'));
        $this->hasColumn('xmlpages', 'string', 255, array('type' => 'string', 'length' => '255'));
        $this->hasColumn('xmlpagetemplates', 'string', 255, array('type' => 'string', 'length' => '255'));
        $this->hasColumn('xmlpageobjects', 'string', 255, array('type' => 'string', 'length' => '255'));
        $this->hasColumn('xmlxslfo', 'string', 255, array('type' => 'string', 'length' => '255'));
        $this->hasColumn('xmlpreviewxslfo', 'string', 255, array('type' => 'string', 'length' => '255'));
        $this->hasColumn('xmlextendpreviewxslfo', 'string', 255, array('type' => 'string', 'length' => '255'));
        $this->hasColumn('title', 'string', 255, array('type' => 'string', 'length' => '255'));

        $this->hasColumn('preview_path', 'string', 255);
        $this->hasColumn('designer_xml', 'string', 9999999999);
        $this->hasColumn('template_print_id', 'string', 255);

        $this->hasColumn('layouter_modus', 'int', 1);
    }

    public function setUp() {
        parent::setUp();

        $this->hasOne('Contact', array('local' => 'contact_id', 'foreign' => 'id'));

    }
}