<?php

/**
 * BaseMotivThemeMotiv
 *
 * This class has been auto-generated by the Doctrine ORM Framework
 *
 * @property integer $theme_id
 * @property integer $motiv_id
 * @property Motiv $Motiv
 * @property MotivTheme $MotivTheme
 *
 * @package    ##PACKAGE##
 * @subpackage ##SUBPACKAGE##
 * @author     ##NAME## <##EMAIL##>
 * @version    SVN: $Id: BaseMotivThemeMotiv.php 5162 2010-11-06 01:42:44Z boonkerz $
 */
abstract class BaseMotivThemeMotiv extends Doctrine_Record
{
    public function setTableDefinition() {
        $this->setTableName('motiv_theme_motiv');
        $this->hasColumn('theme_id', 'integer', 8, array(
            'type' => 'integer',
            'primary' => true,
            'length' => '8',
        ));
        $this->hasColumn('motiv_id', 'integer', 8, array(
            'type' => 'integer',
            'primary' => true,
            'length' => '8',
        ));
    }

    public function setUp() {
        parent::setUp();
        $this->hasOne('Motiv', array(
            'local' => 'motiv_id',
            'foreign' => 'id'));

        $this->hasOne('MotivTheme', array(
            'local' => 'theme_id',
            'foreign' => 'id'));
    }
}