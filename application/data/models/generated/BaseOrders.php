<?php

/**
 * BaseOrders
 *
 * This class has been auto-generated by the Doctrine ORM Framework
 *
 * @property integer $id
 * @property date $created
 * @property date $updated
 * @property integer $install_id
 * @property integer $account_id
 * @property integer $shop_id
 * @property string $alias
 * @property float $preis
 * @property float $preissteuer
 * @property float $preisbrutto
 * @property integer $contact_id
 * @property string $status
 * @property integer $paymenttype_id
 * @property integer $shippingtype_id
 * @property boolean $enable
 * @property string $package
 * @property string $info
 * @property float $gutschein
 * @property float $gutscheinabzug
 * @property string $basketfield1
 * @property string $basketfield2
 * @property boolean $delivery_same
 * @property int $delivery_address
 * @property Contact $Contact
 * @property Install $Install
 * @property Account $Account
 * @property Shop $Shop
 * @property Paymenttype $Paymenttype
 * @property Doctrine_Collection $Orderspos
 *
 * @package    ##PACKAGE##
 * @subpackage ##SUBPACKAGE##
 * @author     ##NAME## <##EMAIL##>
 * @version    SVN: $Id: BaseOrders.php 6674 2011-09-16 12:15:05Z boonkerz $
 */
abstract class BaseOrders extends Doctrine_Record
{

    public function setTableDefinition() {
        $this->setTableName('orders');
        $this->hasColumn('id', 'integer', 8, array('primary' => true, 'autoincrement' => true, 'type' => 'integer', 'length' => '8'));
        $this->hasColumn('uuid', 'string', 40, array('type' => 'string', 'length' => '40'));
        $this->hasColumn('created', 'date', 25, array('type' => 'date', 'length' => '25'));
        $this->hasColumn('updated', 'date', 25, array('type' => 'date', 'length' => '25'));
        $this->hasColumn('install_id', 'integer', 8, array('type' => 'integer', 'length' => '8'));
        $this->hasColumn('account_id', 'integer', 8, array('type' => 'integer', 'length' => '8'));
        $this->hasColumn('shop_id', 'integer', 8, array('type' => 'integer', 'length' => '8'));
        $this->hasColumn('alias', 'string', 255, array('type' => 'string', 'length' => '255'));
        $this->hasColumn('preis', 'float', 2147483647, array('type' => 'float', 'length' => '2147483647'));
        $this->hasColumn('versandkosten', 'float', 2147483647, array('type' => 'float', 'length' => '2147483647'));
        $this->hasColumn('zahlkosten', 'float', 2147483647, array('type' => 'float', 'length' => '2147483647'));
        $this->hasColumn('preissteuer', 'float', 2147483647, array('type' => 'float', 'length' => '2147483647'));
        $this->hasColumn('preisbrutto', 'float', 2147483647, array('type' => 'float', 'length' => '2147483647'));
        $this->hasColumn('contact_id', 'integer', 8, array('type' => 'integer', 'length' => '8'));
        $this->hasColumn('status', 'string', 255, array('type' => 'string', 'length' => '255'));
        $this->hasColumn('paymenttype_id', 'integer', 8, array('type' => 'integer', 'length' => '8'));
        $this->hasColumn('shippingtype_id', 'integer', 8, array('type' => 'integer', 'length' => '8'));
        $this->hasColumn('enable', 'boolean', 25, array('type' => 'boolean', 'length' => '25'));
        $this->hasColumn('package', 'string', 255, array('type' => 'string', 'length' => '255'));
        $this->hasColumn('info', 'string', 2147483647, array('type' => 'string', 'length' => '2147483647'));
        $this->hasColumn('gutschein', 'float', 2147483647, array('type' => 'float', 'length' => '2147483647'));
        $this->hasColumn('gutscheinabzug', 'float', 2147483647, array('type' => 'float', 'length' => '2147483647'));
        $this->hasColumn('gutscheinabzugtyp', 'integer', 1);
        $this->hasColumn('basketfield1', 'string', 255, array('type' => 'string', 'length' => '255'));
        $this->hasColumn('basketfield2', 'string', 255, array('type' => 'string', 'length' => '255'));

        $this->hasColumn('delivery_same', 'boolean', 1);
        $this->hasColumn('delivery_address', 'integer', 8);

        $this->hasColumn('sender_same', 'boolean', 1);
        $this->hasColumn('sender_address', 'integer', 8);

        $this->hasColumn('invoice_same', 'boolean', 1);
        $this->hasColumn('invoice_address', 'integer', 8);

        $this->hasColumn('use_account_as_invoice', 'boolean', 1);
        $this->hasColumn('mwertalle', 'float', 2147483647, array('type' => 'float', 'length' => '2147483647'));
        $this->hasColumn('delivery_date', 'date');
        $this->hasColumn('version', 'integer', 8);
        $this->hasColumn('shippingtype_extra_label', 'string', 255);

        $this->hasColumn('file1', 'string', 255);

        $this->hasColumn('lang', 'string', 2);
        $this->hasColumn('type', 'integer', 1);
    }

    public function setUp() {
        parent::setUp();
        $this->hasOne('Contact', array('local' => 'contact_id', 'foreign' => 'id'));

        $this->hasOne('Install', array('local' => 'install_id', 'foreign' => 'id'));

        $this->hasOne('Account', array('local' => 'account_id', 'foreign' => 'id'));

        $this->hasOne('Shop', array('local' => 'shop_id', 'foreign' => 'id'));

        $this->hasOne('Paymenttype', array('local' => 'paymenttype_id', 'foreign' => 'id'));

        $this->hasOne('Shippingtype', array('local' => 'shippingtype_id', 'foreign' => 'id'));

        $this->hasMany('Orderspos', array('local' => 'id', 'foreign' => 'orders_id'));
    }
}