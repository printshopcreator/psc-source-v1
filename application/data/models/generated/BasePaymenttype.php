<?php

/**
 * BasePaymenttype
 *
 * This class has been auto-generated by the Doctrine ORM Framework
 *
 * @property integer $id
 * @property date $createdd
 * @property date $updatedd
 * @property time $createdt
 * @property time $updatedt
 * @property integer $install_id
 * @property integer $shop_id
 * @property boolean $enable
 * @property string $title
 * @property boolean $prozent
 * @property float $wert
 * @property boolean $private
 * @property Install $Install
 * @property Shop $Shop
 * @property Doctrine_Collection $Orders
 * @property Doctrine_Collection $ContactPaymenttype
 * @property Doctrine_Collection $AccountPaymenttype
 *
 * @package    ##PACKAGE##
 * @subpackage ##SUBPACKAGE##
 * @author     ##NAME## <##EMAIL##>
 * @version    SVN: $Id: BasePaymenttype.php 5162 2010-11-06 01:42:44Z boonkerz $
 */
abstract class BasePaymenttype extends Doctrine_Record
{
    public function setTableDefinition() {
        $this->setTableName('paymenttype');
        $this->hasColumn('id', 'integer', 8, array(
            'primary' => true,
            'autoincrement' => true,
            'type' => 'integer',
            'length' => '8',
        ));
        $this->hasColumn('created', 'timestamp', 25, array(
            'type' => 'timestamp',
            'length' => '25',
        ));
        $this->hasColumn('updated', 'timestamp', 25, array(
            'type' => 'timestamp',
            'length' => '25',
        ));
        $this->hasColumn('install_id', 'integer', 8, array(
            'type' => 'integer',
            'length' => '8',
        ));
        $this->hasColumn('shop_id', 'integer', 8, array(
            'type' => 'integer',
            'length' => '8',
        ));
        $this->hasColumn('enable', 'boolean', 25, array(
            'type' => 'boolean',
            'length' => '25',
        ));
        $this->hasColumn('title', 'string', 255, array(
            'type' => 'string',
            'length' => '255',
        ));
        $this->hasColumn('prozent', 'boolean', 25, array(
            'type' => 'boolean',
            'length' => '25',
        ));
        $this->hasColumn('wert', 'float', 2147483647, array(
            'type' => 'float',
            'length' => '2147483647',
        ));
        $this->hasColumn('private', 'boolean', 25, array(
            'type' => 'boolean',
            'length' => '25',
        ));
        $this->hasColumn('mwert', 'integer', 8, array(
            'type' => 'integer',
            'length' => '8',
        ));
        $this->hasColumn('pos', 'integer', 8);
        $this->hasColumn('description', 'string', 49876567557);
        $this->hasColumn('trustedshop_name', 'string', 255, array(
            'type' => 'string',
            'length' => '255',
        ));
    }

    public function setUp() {
        parent::setUp();
        $this->hasOne('Install', array(
            'local' => 'install_id',
            'foreign' => 'id'));

        $this->hasOne('Shop', array(
            'local' => 'shop_id',
            'foreign' => 'id'));

        $this->hasMany('Orders', array(
            'local' => 'id',
            'foreign' => 'paymenttype_id'));

        $this->hasMany('ContactPaymenttype', array(
            'local' => 'id',
            'foreign' => 'paymenttype_id'));

        $this->hasMany('AccountPaymenttype', array(
            'local' => 'id',
            'foreign' => 'paymenttype_id'));
    }
}