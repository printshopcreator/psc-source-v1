<?php

/**
 * BaseShippingtype
 *
 * This class has been auto-generated by the Doctrine ORM Framework
 *
 * @property integer $id
 * @property date $createdd
 * @property date $updatedd
 * @property time $createdt
 * @property time $updatedt
 * @property integer $install_id
 * @property integer $shop_id
 * @property boolean $enable
 * @property string $title
 * @property string $art
 * @property string $land
 * @property integer $mwert
 * @property string $kosten_a
 * @property string $kosten_b
 * @property string $kosten_c
 * @property string $kosten_d
 * @property string $kosten_e
 * @property string $kosten_f
 * @property string $kosten_fix
 * @property Install $Install
 * @property Shop $Shop
 *
 * @package    ##PACKAGE##
 * @subpackage ##SUBPACKAGE##
 * @author     ##NAME## <##EMAIL##>
 * @version    SVN: $Id: BaseShippingtype.php 5532 2010-12-13 22:54:01Z boonkerz $
 */
abstract class BaseShippingtype extends Doctrine_Record
{
    public function setTableDefinition() {
        $this->setTableName('shippingtype');
        $this->hasColumn('id', 'integer', 8, array(
            'primary' => true,
            'autoincrement' => true,
            'type' => 'integer',
            'length' => '8',
        ));
        $this->hasColumn('created', 'timestamp', 25, array(
            'type' => 'timestamp',
            'length' => '25',
        ));
        $this->hasColumn('updated', 'timestamp', 25, array(
            'type' => 'timestamp',
            'length' => '25',
        ));
        $this->hasColumn('install_id', 'integer', 8, array(
            'type' => 'integer',
            'length' => '8',
        ));
        $this->hasColumn('shop_id', 'integer', 8, array(
            'type' => 'integer',
            'length' => '8',
        ));
        $this->hasColumn('enable', 'boolean', 25, array(
            'type' => 'boolean',
            'length' => '25',
        ));
        $this->hasColumn('title', 'string', 255, array(
            'type' => 'string',
            'length' => '255',
        ));
        $this->hasColumn('art', 'string', 255, array(
            'type' => 'string',
            'length' => '255',
        ));
        $this->hasColumn('land', 'string', 255, array(
            'type' => 'string',
            'length' => '255',
        ));
        $this->hasColumn('mwert', 'integer', 8, array(
            'type' => 'integer',
            'length' => '8',
        ));
        $this->hasColumn('kosten_a', 'string', 255, array(
            'type' => 'string',
            'length' => '255',
        ));
        $this->hasColumn('kosten_b', 'string', 255, array(
            'type' => 'string',
            'length' => '255',
        ));
        $this->hasColumn('kosten_c', 'string', 255, array(
            'type' => 'string',
            'length' => '255',
        ));
        $this->hasColumn('kosten_d', 'string', 255, array(
            'type' => 'string',
            'length' => '255',
        ));
        $this->hasColumn('kosten_e', 'string', 255, array(
            'type' => 'string',
            'length' => '255',
        ));
        $this->hasColumn('kosten_f', 'string', 255, array(
            'type' => 'string',
            'length' => '255',
        ));
        $this->hasColumn('kosten_fix', 'string', 255, array(
            'type' => 'string',
            'length' => '255',
        ));

        $this->hasColumn('kosten_percent', 'float', 255, array(
            'type' => 'float',
            'length' => '255',
        ));

        $this->hasColumn('pos', 'integer', 8);

        $this->hasColumn('no_payment', 'boolean', 1);
        $this->hasColumn('description', 'string', 49876567557);
        $this->hasColumn('private', 'boolean', 1);

        $this->hasColumn('weight_from', 'integer', 8);
        $this->hasColumn('weight_to', 'integer', 8);

        $this->hasColumn('xml', 'string', 99999999999, array('type' => 'string', 'length' => '2147483647'));
    }

    public function setUp() {
        parent::setUp();
        $this->hasOne('Install', array(
            'local' => 'install_id',
            'foreign' => 'id'));

        $this->hasOne('Shop', array(
            'local' => 'shop_id',
            'foreign' => 'id'));
    }
}