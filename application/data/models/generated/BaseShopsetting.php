<?php

abstract class BaseShopsetting extends Doctrine_Record
{
    public function setTableDefinition() {
        $this->setTableName('shop_setting');

        $this->hasColumn('id', 'varchar', 255, array(
            'primary' => true,
        ));

        $this->hasColumn('shop_id', 'integer', 8);
        $this->hasColumn('keyid', 'string', 255);
        $this->hasColumn('value', 'string');

    }
}