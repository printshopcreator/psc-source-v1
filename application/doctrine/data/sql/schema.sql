CREATE TABLE shop_account (shop_id BIGINT, account_id BIGINT, PRIMARY KEY(shop_id, account_id)) ENGINE = INNODB;
CREATE TABLE paymenttype (id BIGINT AUTO_INCREMENT, createdd DATE, updatedd DATE, createdt TIME, updatedt TIME, install_id BIGINT, shop_id BIGINT, enable TINYINT(1), title VARCHAR(255), prozent TINYINT(1), wert DOUBLE, private TINYINT(1), INDEX install_id_idx (install_id), INDEX shop_id_idx (shop_id), PRIMARY KEY(id)) ENGINE = INNODB;
CREATE TABLE image (id VARCHAR(40), path VARCHAR(255), name VARCHAR(255), imagetype VARCHAR(255), PRIMARY KEY(id)) ENGINE = INNODB;
CREATE TABLE account_paymenttype (account_id BIGINT, paymenttype_id BIGINT, PRIMARY KEY(account_id, paymenttype_id)) ENGINE = INNODB;
CREATE TABLE orderspos (id BIGINT AUTO_INCREMENT, createdd DATE, updatedd DATE, createdt TIME, updatedt TIME, install_id BIGINT, shop_id BIGINT, account_id BIGINT, orders_id BIGINT, count INT, data LONGBLOB, priceone DOUBLE, priceall DOUBLE, uploadfinish TINYINT(1), INDEX install_id_idx (install_id), INDEX shop_id_idx (shop_id), INDEX account_id_idx (account_id), PRIMARY KEY(id, orders_id)) ENGINE = INNODB;
CREATE TABLE contact_paymenttype (contact_id BIGINT, paymenttype_id BIGINT, PRIMARY KEY(contact_id, paymenttype_id)) ENGINE = INNODB;
CREATE TABLE shop_contact (shop_id BIGINT, contact_id BIGINT, PRIMARY KEY(shop_id, contact_id)) ENGINE = INNODB;
CREATE TABLE article_group (id BIGINT AUTO_INCREMENT, pid BIGINT, createdd DATE, updatedd DATE, createdt TIME, updatedt TIME, install_id BIGINT, shop_id BIGINT, account_id BIGINT, parent BIGINT, title VARCHAR(255), text TEXT, language VARCHAR(255), image VARCHAR(255), enable TINYINT(1) DEFAULT 1, notinmenu TINYINT(1) DEFAULT 0, INDEX install_id_idx (install_id), INDEX shop_id_idx (shop_id), INDEX account_id_idx (account_id), PRIMARY KEY(id)) ENGINE = INNODB;
CREATE TABLE resource (id BIGINT AUTO_INCREMENT, created DATETIME, updated DATETIME, name VARCHAR(255), modul VARCHAR(255), PRIMARY KEY(id)) ENGINE = INNODB;
CREATE TABLE resource_privilege (id BIGINT AUTO_INCREMENT, resource_id BIGINT, privilege_id BIGINT, created DATETIME, updated DATETIME, tree TINYINT(1), treename VARCHAR(255), treeicon VARCHAR(255), treeparent VARCHAR(255), INDEX resource_id_idx (resource_id), INDEX privilege_id_idx (privilege_id), PRIMARY KEY(id)) ENGINE = INNODB;
CREATE TABLE contact (id BIGINT AUTO_INCREMENT, install_id BIGINT, account_id BIGINT, created DATE, updated DATE, enable TINYINT(1), name VARCHAR(255), hash VARCHAR(255), password VARCHAR(40), language VARCHAR(5), self_anrede INT, self_firstname VARCHAR(255), self_lastname VARCHAR(255), self_function VARCHAR(255), self_department VARCHAR(255), self_street VARCHAR(255), self_house_number VARCHAR(255), self_destrict VARCHAR(5), self_zip VARCHAR(255), self_city VARCHAR(255), self_state VARCHAR(255), self_country VARCHAR(255), self_email VARCHAR(255), self_web VARCHAR(255), self_foto VARCHAR(255), self_phone VARCHAR(255), self_phone_alternative_type INT, self_phone_alternative VARCHAR(255), self_phone_mobile VARCHAR(255), self_newsletter TINYINT(1), self_information TEXT, anrede INT, firstname VARCHAR(255), lastname VARCHAR(255), function VARCHAR(255), department VARCHAR(255), street VARCHAR(255), house_number VARCHAR(255), destrict VARCHAR(5), zip VARCHAR(255), city VARCHAR(255), state VARCHAR(255), country VARCHAR(255), email VARCHAR(255), web VARCHAR(255), foto VARCHAR(255), phone VARCHAR(255), phone_alternative_type INT, phone_alternative VARCHAR(255), phone_mobile VARCHAR(255), newsletter TINYINT(1), information TEXT, contact_correspondence_languages INT, mwert TINYINT(1) DEFAULT 1, INDEX install_id_idx (install_id), INDEX account_id_idx (account_id), PRIMARY KEY(id)) ENGINE = INNODB;
CREATE TABLE account_article (account_id BIGINT, article_id BIGINT, PRIMARY KEY(account_id, article_id)) ENGINE = INNODB;
CREATE TABLE account (id BIGINT AUTO_INCREMENT, install_id BIGINT, created DATE, updated DATE, company VARCHAR(255), appendix VARCHAR(255), street VARCHAR(255), destrict VARCHAR(5), zip VARCHAR(255), city VARCHAR(255), state VARCHAR(255), country VARCHAR(255), email VARCHAR(255), tel VARCHAR(255), tel1 VARCHAR(255), fax VARCHAR(255), homepage VARCHAR(255), bank_account VARCHAR(255), bank_owner VARCHAR(255), bank_code VARCHAR(255), bank_iban VARCHAR(255), bank_swift VARCHAR(255), usid VARCHAR(255), mwert TINYINT(1) DEFAULT 1, INDEX install_id_idx (install_id), PRIMARY KEY(id)) ENGINE = INNODB;
CREATE TABLE install (id BIGINT AUTO_INCREMENT, created DATETIME, updated DATETIME, name VARCHAR(255), PRIMARY KEY(id)) ENGINE = INNODB;
CREATE TABLE cms (id BIGINT AUTO_INCREMENT, install_id BIGINT, shop_id BIGINT, created DATETIME, updated DATETIME, pos VARCHAR(100), sor VARCHAR(10), title VARCHAR(255), menu VARCHAR(255), url VARCHAR(255), language VARCHAR(5), text1 TEXT, notinmenu TINYINT(1), INDEX install_id_idx (install_id), INDEX shop_id_idx (shop_id), PRIMARY KEY(id)) ENGINE = INNODB;
CREATE TABLE privilege (id BIGINT AUTO_INCREMENT, created DATETIME, updated DATETIME, name VARCHAR(255), PRIMARY KEY(id)) ENGINE = INNODB;
CREATE TABLE base_article_releated (article1 BIGINT, article2 BIGINT, PRIMARY KEY(article1, article2)) ENGINE = INNODB;
CREATE TABLE shop (id BIGINT AUTO_INCREMENT, install_id BIGINT, created DATETIME, updated DATETIME, name VARCHAR(255), layout VARCHAR(255), uid VARCHAR(255), defaultfunc VARCHAR(255), defaultparam VARCHAR(255), mwert VARCHAR(2), logo1 VARCHAR(255), logo2 VARCHAR(255), title VARCHAR(255), subtitle VARCHAR(255), parameter TEXT, formel TEXT, land BIGINT, INDEX install_id_idx (install_id), PRIMARY KEY(id)) ENGINE = INNODB;
CREATE TABLE shippingtype (id BIGINT AUTO_INCREMENT, createdd DATE, updatedd DATE, createdt TIME, updatedt TIME, install_id BIGINT, shop_id BIGINT, enable TINYINT(1), title VARCHAR(255), art VARCHAR(255), land VARCHAR(255), mwert BIGINT, kosten_a VARCHAR(255), kosten_b VARCHAR(255), kosten_c VARCHAR(255), kosten_d VARCHAR(255), kosten_e VARCHAR(255), kosten_f VARCHAR(255), kosten_fix VARCHAR(255), INDEX install_id_idx (install_id), INDEX shop_id_idx (shop_id), PRIMARY KEY(id)) ENGINE = INNODB;
CREATE TABLE orders (id BIGINT AUTO_INCREMENT, createdd DATE, updatedd DATE, createdt TIME, updatedt TIME, install_id BIGINT, account_id BIGINT, shop_id BIGINT, alias VARCHAR(255), contact_id BIGINT, status VARCHAR(255), paymenttype_id BIGINT, INDEX contact_id_idx (contact_id), INDEX install_id_idx (install_id), INDEX account_id_idx (account_id), INDEX shop_id_idx (shop_id), INDEX paymenttype_id_idx (paymenttype_id), PRIMARY KEY(id)) ENGINE = INNODB;
CREATE TABLE queues (id BIGINT AUTO_INCREMENT, createdd DATE, updatedd DATE, createdt TIME, updatedt TIME, install_id BIGINT, shop_id BIGINT, name VARCHAR(255), typ VARCHAR(255), data TEXT, enable TINYINT(1), action VARCHAR(255), INDEX install_id_idx (install_id), INDEX shop_id_idx (shop_id), PRIMARY KEY(id)) ENGINE = INNODB;
CREATE TABLE article (id BIGINT AUTO_INCREMENT, createdd DATE, updatedd DATE, createdt TIME, updatedt TIME, install_id BIGINT, shop_id BIGINT, account_id BIGINT, name VARCHAR(255), typ VARCHAR(255), data TEXT, enable TINYINT(1), private TINYINT(1), mwert BIGINT, versand VARCHAR(29), versandwert DOUBLE, pos BIGINT, upload TINYINT(1), special TINYINT(1), INDEX install_id_idx (install_id), INDEX shop_id_idx (shop_id), INDEX account_id_idx (account_id), PRIMARY KEY(id)) ENGINE = INNODB;
CREATE TABLE domain (id BIGINT AUTO_INCREMENT, shop_id BIGINT, created DATETIME, updated DATETIME, name VARCHAR(255), INDEX shop_id_idx (shop_id), PRIMARY KEY(id)) ENGINE = INNODB;
CREATE TABLE role (id BIGINT AUTO_INCREMENT, created DATETIME, updated DATETIME, name VARCHAR(255), level MEDIUMINT, PRIMARY KEY(id)) ENGINE = INNODB;
CREATE TABLE contact_article (contact_id BIGINT, article_id BIGINT, PRIMARY KEY(contact_id, article_id)) ENGINE = INNODB;
CREATE TABLE article_group_article (articlegroup_id BIGINT, article_id BIGINT, PRIMARY KEY(articlegroup_id, article_id)) ENGINE = INNODB;
CREATE TABLE role_privilege (id BIGINT AUTO_INCREMENT, role_id BIGINT, resource_privilege_id BIGINT, created DATETIME, updated DATETIME, INDEX role_id_idx (role_id), INDEX resource_privilege_id_idx (resource_privilege_id), PRIMARY KEY(id)) ENGINE = INNODB;
CREATE TABLE contact_role (created DATETIME, updated DATETIME, role_id BIGINT, contact_id BIGINT, PRIMARY KEY(role_id, contact_id)) ENGINE = INNODB;
ALTER TABLE shop_account ADD FOREIGN KEY (shop_id) REFERENCES shop(id);
ALTER TABLE shop_account ADD FOREIGN KEY (account_id) REFERENCES account(id);
ALTER TABLE paymenttype ADD FOREIGN KEY (shop_id) REFERENCES shop(id);
ALTER TABLE paymenttype ADD FOREIGN KEY (install_id) REFERENCES install(id);
ALTER TABLE account_paymenttype ADD FOREIGN KEY (paymenttype_id) REFERENCES paymenttype(id) ON DELETE CASCADE;
ALTER TABLE account_paymenttype ADD FOREIGN KEY (account_id) REFERENCES account(id);
ALTER TABLE orderspos ADD FOREIGN KEY (shop_id) REFERENCES shop(id);
ALTER TABLE orderspos ADD FOREIGN KEY (orders_id) REFERENCES orders(id);
ALTER TABLE orderspos ADD FOREIGN KEY (install_id) REFERENCES install(id);
ALTER TABLE orderspos ADD FOREIGN KEY (account_id) REFERENCES account(id);
ALTER TABLE contact_paymenttype ADD FOREIGN KEY (paymenttype_id) REFERENCES paymenttype(id) ON DELETE CASCADE;
ALTER TABLE contact_paymenttype ADD FOREIGN KEY (contact_id) REFERENCES contact(id);
ALTER TABLE shop_contact ADD FOREIGN KEY (shop_id) REFERENCES shop(id);
ALTER TABLE shop_contact ADD FOREIGN KEY (contact_id) REFERENCES contact(id) ON DELETE CASCADE;
ALTER TABLE article_group ADD FOREIGN KEY (shop_id) REFERENCES shop(id);
ALTER TABLE article_group ADD FOREIGN KEY (install_id) REFERENCES install(id);
ALTER TABLE article_group ADD FOREIGN KEY (account_id) REFERENCES account(id);
ALTER TABLE resource_privilege ADD FOREIGN KEY (resource_id) REFERENCES resource(id);
ALTER TABLE resource_privilege ADD FOREIGN KEY (privilege_id) REFERENCES privilege(id);
ALTER TABLE contact ADD FOREIGN KEY (install_id) REFERENCES install(id);
ALTER TABLE contact ADD FOREIGN KEY (account_id) REFERENCES account(id);
ALTER TABLE account_article ADD FOREIGN KEY (article_id) REFERENCES article(id) ON DELETE CASCADE;
ALTER TABLE account_article ADD FOREIGN KEY (account_id) REFERENCES account(id);
ALTER TABLE account ADD FOREIGN KEY (install_id) REFERENCES install(id);
ALTER TABLE cms ADD FOREIGN KEY (shop_id) REFERENCES shop(id);
ALTER TABLE cms ADD FOREIGN KEY (install_id) REFERENCES install(id);
ALTER TABLE shop ADD FOREIGN KEY (install_id) REFERENCES install(id);
ALTER TABLE shippingtype ADD FOREIGN KEY (shop_id) REFERENCES shop(id);
ALTER TABLE shippingtype ADD FOREIGN KEY (install_id) REFERENCES install(id);
ALTER TABLE orders ADD FOREIGN KEY (shop_id) REFERENCES shop(id);
ALTER TABLE orders ADD FOREIGN KEY (paymenttype_id) REFERENCES paymenttype(id);
ALTER TABLE orders ADD FOREIGN KEY (install_id) REFERENCES install(id);
ALTER TABLE orders ADD FOREIGN KEY (contact_id) REFERENCES contact(id) ON DELETE CASCADE;
ALTER TABLE orders ADD FOREIGN KEY (account_id) REFERENCES account(id);
ALTER TABLE queues ADD FOREIGN KEY (shop_id) REFERENCES shop(id);
ALTER TABLE queues ADD FOREIGN KEY (install_id) REFERENCES install(id);
ALTER TABLE article ADD FOREIGN KEY (shop_id) REFERENCES shop(id);
ALTER TABLE article ADD FOREIGN KEY (install_id) REFERENCES install(id);
ALTER TABLE article ADD FOREIGN KEY (account_id) REFERENCES account(id);
ALTER TABLE domain ADD FOREIGN KEY (shop_id) REFERENCES shop(id);
ALTER TABLE contact_article ADD FOREIGN KEY (contact_id) REFERENCES contact(id);
ALTER TABLE contact_article ADD FOREIGN KEY (article_id) REFERENCES article(id) ON DELETE CASCADE;
ALTER TABLE article_group_article ADD FOREIGN KEY (articlegroup_id) REFERENCES article_group(id);
ALTER TABLE article_group_article ADD FOREIGN KEY (article_id) REFERENCES article(id);
ALTER TABLE role_privilege ADD FOREIGN KEY (role_id) REFERENCES role(id);
ALTER TABLE role_privilege ADD FOREIGN KEY (resource_privilege_id) REFERENCES resource_privilege(id);
ALTER TABLE contact_role ADD FOREIGN KEY (role_id) REFERENCES role(id);
ALTER TABLE contact_role ADD FOREIGN KEY (contact_id) REFERENCES contact(id) ON DELETE CASCADE;
