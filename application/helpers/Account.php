<?php

class TP_View_Helper_Account
{

    public function Account() {
        return $this;
    }

    public function userHasEditAccounts() {

        if(Zend_Auth::getInstance()->hasIdentity()) {
            $contact = Zend_Auth::getInstance()->getIdentity();
            $accounts = Doctrine_Query::create()->from('ContactAccount a')->where('a.contact_id = ?', array(
                $contact['id']))->execute()->count();

            return $accounts;
        }

        return false;
    }



}
