<?php

class TP_View_Helper_Adminbar extends Zend_View_Helper_Abstract
{

    protected $_count = 0;

    public function Adminbar() {
        return $this;
    }

    public function generate() {

        if (Zend_Auth::getInstance()->hasIdentity()) {

            $user = Zend_Auth::getInstance()->getIdentity();

            if ($user['is_sek'] || $this->view->admin) {

                $this->view->headStyle()->appendStyle("
                    #admin_toolbar { z-index:3000; background: transparent url(/scripts/jquery-contactable/images/contact.png) no-repeat 350px 100px; height: 400px; position: absolute; margin-left: -350px; top: 100px; width: 400px; color: #ffffff; }
                    #admin_toolbar_content { z-index:3000; background-color: #464646; position: absolute; margin-left: -350px; top: 100px; width: 350px; color: #ffffff; }
                    
                    #admin_form { width: 350px; height: 350px; padding: 10px;}
                    #admin_form input { background-color: #ffffff; }
                    #live_admin { margin-right: 10px; color: #ffffff; float: left; margin-top: 5px; }
                    #user_select_admin { margin-right: 10px; color: #ffffff; margin-top: 5px; }
                    #admin_toolbar input, #user_select_admin select { color: #ffffff; margin-top: 5px; background: transparent !important; border: 1px solid #BBB !important; }
                    #user_select_admin select option { color: #ffffff; }
                ");

                $this->view->headLink()->appendStylesheet("/scripts/jquery-simpleautocomplete/css/simpleAutoComplete.css");

                if ($this->view->admin) {

                    $this->view->headScript()->appendScript("
                    $(document).ready(function() {
                        //$('.liveedit').aloha().parent().removeAttr('href');
                        /*$('.liveedit').editable({

	        	  		onSubmit:function submitData(content){
	        	  			$.ajax({
	          	  			  type: 'POST',
	          	  			  url: '/admin/' + $(this).attr('table') + '/liveedit/format/json',
	          	  			  data: {
		          	  			  content: content.current,
		          	  			  field: $(this).attr('field'),
		          	  			  id: $(this).attr('id')
	        	  				}
	          	  			});

	    	  			},
	        	  		submit:'Speichern',
	        	  		cancel: 'Abbrechen'
        	  		}).parent().attr('href', '#');*/
                        });
                    ");

                    $this->view->headLink()->appendStylesheet("/scripts/aloha/aloha/css/aloha.css");

                }
                $this->view->headScript()->prependFile('/scripts/jquery-simpleautocomplete/js/simpleAutoComplete.js');
                $this->view->headScript()->appendScript("
                    $(document).ready(function() {
                        $('#sek_admin_user_id').simpleAutoComplete('/service/index/contacts',{
                        autoCompleteClassName: 'autocomplete',
                        selectedClassName: 'sel',
                        attrCallBack: 'rel',
                        identifier: 'sek_admin_user_id'
                    },estadoCallback);
                    
                        
                    $('div#admin_toolbar').toggle(function() {
                    $(this).animate({'marginLeft': '-=5px'}, 'fast');
                    $('#admin_toolbar_content').animate({'marginLeft': '-=0px'}, 'fast');
                    $(this).animate({'marginLeft': '+=350px'}, 'slow');
                    $('#admin_toolbar_content').animate({'marginLeft': '+=350px'}, 'slow');
                    },
                     function() {
                    $('#admin_toolbar_content').animate({'marginLeft': '-=350px'}, 'slow');
                    $(this).animate({'marginLeft': '-=350px'}, 'slow').animate({'marginLeft': '+=5px'}, 'fast');
                    });
                    });
                    function estadoCallback( par )
	{
	    $('#sek_admin_user').val(par[0]);
	}
 
                    ");
            }
        }
    }

    public function content() {

        if (Zend_Auth::getInstance()->hasIdentity()) {

            $user = Zend_Auth::getInstance()->getIdentity();

            if ($user['is_sek'] || $this->view->admin) {

                $content = '<div id="admin_toolbar"></div><div id="admin_toolbar_content"><form id="admin_form" method="POST" action=""><input type="hidden" name="adminmode" value="1">';

                if ($this->view->admin) {
                    if ($this->view->liveedit) {
                        $content .= '<div id="live_admin">Live Edit: <input type="checkbox" name="liveedit" value="1" checked="checked"/> <br/> (Live Edit ermöglicht das Übersetzen und Überarbeiten von Inhalten und Texten direkt in der Webseite.)</div>';
                    } else {
                        $content .= '<div id="live_admin">Live Edit: <input type="checkbox" name="liveedit" value="1"/> <br/> (Live Edit ermöglicht das Übersetzen und Überarbeiten von Inhalten und Texten direkt in der Webseite.)</div>';
                    }
                }

                $shop = Zend_Registry::get('shop');

                if ($this->view->admin || ($user['is_sek'] && $shop['default_account'] != $user['account_id'] && $shop['default_account'] != "" && $shop['default_account'] != '0')) {

                    $content .= '<div id="user_select_admin">';
                    $mode = new Zend_Session_Namespace('adminmode');
                    if ($mode->over_ride_contact == false) {
                        $content .= '<input type="radio" name="sek_admin" value="no" checked> Nichts ausgewählt<br/>';
                    } else {
                        $content .= '<input type="radio" name="sek_admin" value="no"> Nichts ausgewählt<br/>';
                    }
                    if ($this->view->admin) {
                        if ($mode->over_ride_contact == 'new') {
                            $content .= '<input type="radio" name="sek_admin" value="new" checked/> neuer User<br/>';
                        } else {
                            $content .= '<input type="radio" name="sek_admin" value="new"/> neuer User<br/>';
                        }
                        $rows = Doctrine_Query::create()->select()->from('Contact c')->leftJoin('c.Shops s')->where('s.id = ? AND c.id != ?', array($shop['id'], $user['id']))->orderBy('c.self_lastname ASC')->execute();
                    } else {

                        $rows = Doctrine_Query::create()->select()->from('Contact c')->where('c.account_id = ? AND c.id != ?', array($user['account_id'], $user['id']))->orderBy('c.self_lastname ASC')->execute();
                    }
                    if ($mode->over_ride_contact != false && $mode->over_ride_contact != 'new') {
                        $content .= '<input type="radio" name="sek_admin" value="user" checked/><input id="sek_admin_user_id" name="sek_admin_user_id" value="' . $mode->over_ride_contact_id . '" />';
                    } else {
                        $content .= '<input type="radio" name="sek_admin" value="user"/><input id="sek_admin_user_id" name="sek_admin_user_id" value="' . $mode->over_ride_contact_id . '" />';
                    }

                    $content .= '<input type="hidden" id="sek_admin_user" name="sek_admin_user" value="' . $mode->over_ride_contact . '"/></div>';
                }

                $content .= '<input type="submit" value="Speichern" /></form></div>';

                return $content;
            }
        }
    }

}
