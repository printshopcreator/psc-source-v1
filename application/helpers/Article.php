<?php

class TP_View_Helper_Article
{

    protected $_count = 0;

    public function Article() {
        return $this;
    }

    public function hasCustomAccountImageAvailable($sub = "", $article) {

        if(Zend_Auth::getInstance()->hasIdentity()) {
            $user = Zend_Auth::getInstance()->getIdentity();
            $shop = Zend_Registry::get('shop');
            $user = Doctrine_Query::create()->from('Contact s')->where('s.id = ?')->fetchOne(array(
                $user['id']));
            if(file_exists('styles/' . $shop['uid']. '/design/articleimages/' . $user->Account->company . '/' . $article->id . $sub . '.png')) {
                return true;
            }

        }

        return false;
    }

    public function giveCustomAccountImageAvailable($sub = "", $article) {

        if(Zend_Auth::getInstance()->hasIdentity()) {
            $user = Zend_Auth::getInstance()->getIdentity();
            $shop = Zend_Registry::get('shop');
            $user = Doctrine_Query::create()->from('Contact s')->where('s.id = ?')->fetchOne(array(
                $user['id']));
            if(file_exists('styles/' . $shop['uid']. '/design/articleimages/' . $user->Account->company . '/' . $article->id . $sub . '.png')) {
                return 'styles/' . $shop['uid']. '/design/articleimages/' . $user->Account->company . '/' . $article->id . $sub . '.png';
            }

        }

        return "";
    }

    public function getNearest(Article $article, array $id) {

        if ($article->a6_org_article != "" && $article->a6_org_article != 0) {
            $article = Doctrine_Query::create()
                ->from('Article c')
                ->where('c.id = ?', array($article->a6_org_article))
                ->fetchOne();
        }

        foreach ($article->ArticleGroup as $ag) {
            if ($ag->parent != 0 && !in_array($ag->id, $id)) {
                return $ag;
            }
        }
        $ag = new stdClass();
        $ag->title = '';
        return $ag;
    }

    public function checkTemplatePrintExists(Article $article) {
        $path = str_split($article->id);

        if (!file_exists(APPLICATION_PATH . '/../market/templateprint/orginal/' . implode('/', $path) . '')) {
            $an = '';
            foreach ($path as $pt) {
                $an .= $pt . '/';
                if (!file_exists(APPLICATION_PATH . '/../market/templateprint/orginal/' . $an)) {
                    mkdir(APPLICATION_PATH . '/../market/templateprint/orginal/' . $an);
                }
                if (!file_exists(APPLICATION_PATH . '/../market/templateprint/orginal/' . $an)) {
                    mkdir(APPLICATION_PATH . '/../market/templateprint/orginal/' . $an);
                }
            }

            return "";
        } else {
            if (!file_exists(APPLICATION_PATH . '/../market/templateprint/orginal/' . implode('/', $path) . '/product.zip')) {
                return "";
            } else {
                return "&w2pproductid=" . $article->uuid;
            }
        }

    }

    public function getCountFavoriten() {
        if (Zend_Auth::getInstance()->hasIdentity()) {
            $user = Zend_Auth::getInstance()->getIdentity();
            return Doctrine_Query::create()->select()->from('ContactFavArticle a')->where('a.contact_uuid = ?', array($user['id']))->execute()->count();
        }
        return 0;
    }

    public function Group($group) {
        $shop = Zend_Registry::get('shop');
        $install = Zend_Registry::get('install');

        if ($install['defaultmarket'] == $shop['id']) {
            $articles = Doctrine_Query::create()
                ->from('ArticleGroupArticle c')
                ->leftJoin('c.Article a')
                ->where('a.install_id = ? AND a.display_market = 1 AND a.private = 0 AND a.enable = 1 AND c.articlegroup_id = ?', array($shop['install_id'], $group))
                ->orderBy('a.pos ASC')
                ->execute();
        } else {
            $articles = Doctrine_Query::create()
                ->from('ArticleGroupArticle c')
                ->leftJoin('c.Article a')
                ->where('a.shop_id = ? AND a.private = 0 AND a.enable = 1 AND c.articlegroup_id = ?', array($shop['id'], $group))
                ->orderBy('a.pos ASC')
                ->execute();
        }

        $temp = array();
        foreach ($articles as $art) {
            array_push($temp, $art->Article);
        }

        return $temp;
    }

    public function getGroups($group) {

        if ($group != "") {

            $shop = Zend_Registry::get('shop');

            $articlesgroups = Doctrine_Query::create()
                ->from('Articlegroup c')
                ->where('c.shop_id = ? AND c.id IN (' . $group . ')', array($shop['id']))
                ->execute();
            return $articlesgroups;

        } else {
            return array();
        }
    }

    public function getRandomArticle($limit = 8, $expect = array()) {

        $shop = Zend_Registry::get('shop');

        if (count($expect) > 0) {

            return Doctrine_Query::create()
                ->from('Article c')
                ->leftJoin('c.OrgArticle l')
                ->select('c.*, RANDOM() AS rand, (c.rate/c.rate_count) as ratesum, l.*, (l.a4_abpreis+c.a6_resale_price) as sum')
                ->where('c.enable = 1 AND c.private = 0 AND c.display_not_in_overview = 0 AND c.shop_id = ?', array($shop['id']))
                ->andWhereNotIn('c.id', $expect)
                ->orderBy('rand')
                ->limit($limit)
                ->execute();
        }

        return Doctrine_Query::create()
            ->from('Article c')
            ->leftJoin('c.OrgArticle l')
            ->select('c.*, RANDOM() AS rand, (c.rate/c.rate_count) as ratesum, l.*, (l.a4_abpreis+c.a6_resale_price) as sum')
            ->where('c.enable = 1 AND c.private = 0 AND c.display_not_in_overview = 0 AND c.shop_id = ?', array($shop['id']))
            ->orderBy('rand')
            ->limit($limit)
            ->execute();
    }

    public function getStartArticle() {
        $shop = Zend_Registry::get('shop');
        $row = Doctrine_Query::create()
            ->from('Article c')
            ->where('c.enable = 1 AND c.private = 0 AND c.shop_id = ? AND c.display_not_in_overview = 0', array($shop['id']))
            ->fetchOne();

        return $row;


    }

    public function getArticle($var) {

        if (is_array($var)) {
            $temp = new ArrayIterator();

            foreach($var as $key) {
                $row = Doctrine_Query::create()
                ->from('Article c')
                ->leftJoin('c.OrgArticle l')
                ->select('c.*, (c.rate/c.rate_count) as ratesum, l.*, (l.a4_abpreis+c.a6_resale_price) as sum')
                ->where('c.id = '.$key)
                ->fetchOne();

                if($row) {
                    $temp->append($row);
                }

            }

            return $temp;

        }
        return Doctrine_Query::create()
            ->from('Article c')
            ->leftJoin('c.OrgArticle l')
            ->select('c.*, (c.rate/c.rate_count) as ratesum, l.*, (l.a4_abpreis+c.a6_resale_price) as sum')
            ->where('c.id = ?', array($var))
            ->fetchOne();
    }

    public function getArticleByUUID($var) {

        return Doctrine_Query::create()
            ->from('Article c')
            ->where('c.uuid = ?', array($var))
            ->fetchOne();
    }

    public function Grouppsc($group) {
        $shop = Zend_Registry::get('shop');
        $install = Zend_Registry::get('install');

        $articles = Doctrine_Query::create()
            ->from('ArticleGroupArticle c')
            ->leftJoin('c.Article a')
            ->where('a.shop_id = ? AND a.private = 0 AND a.enable = 1 AND c.articlegroup_id = ?', array($shop['id'], $group))
            ->orderBy('a.pos ASC')
            ->execute();

        $temp = array();
        foreach ($articles as $art) {
            array_push($temp, $art->Article);
        }

        return $temp;
    }

    public function getArticleByArray($items) {

        if (!is_array($items) || empty($items)) {
            return null;
        }

        $allowIdsContacts = [1];

        if (Zend_Auth::getInstance()->hasIdentity()) {
            $user = Zend_Auth::getInstance()->getIdentity();
            $user = Doctrine_Query::create()->from('Contact m')->useQueryCache(true)->where('id = ?')->fetchOne(array(
                $user['id']));
            if ($user != false) {

                foreach($user->ContactArticle as $ca) {
                    $allowIdsContacts[] = $ca->article_id;
                }

                foreach($user->Account->AccountArticle as $ca) {
                    $allowIdsContacts[] = $ca->article_id;
                }
            }
        }

        $shop = Zend_Registry::get('shop');

        $rows = Doctrine_Query::create()
            ->select('a.*, (a.rate/a.rate_count) as ratesum, (a.a4_abpreis+a.motive_price) as sum')
            ->from('Article a')
            ->where('a.enable = 1 AND a.id IN (' . implode(',', $items) . ') AND (a.private = 0 OR (a.private = 1 AND a.id IN ('.implode(",", $allowIdsContacts).')))')->orderBy('a.pos ASC')->execute();

        return $rows;
    }

    public function TopSeller($limit = 4) {

        $shop = Zend_Registry::get('shop');
        $install = Zend_Registry::get('install');

        if ($shop['pmb'] == 1) {
            $articles = Doctrine_Query::create()
                ->from('Article c')
                ->where('c.install_id = ? AND c.display_market = 1 AND c.private = 0 AND c.enable = 1 AND c.typ = 6', array($shop['install_id']))
                ->orderBy('c.used ASC')
                ->limit($limit)
                ->execute();
        } else {
            $articles = Doctrine_Query::create()
                ->from('Article c')
                ->where('c.shop_id = ? AND c.private = 0 AND c.enable = 1 AND c.typ = 6', array($shop['id']))
                ->orderBy('c.used ASC')
                ->limit($limit)
                ->execute();
        }
        return $articles;
    }

    public function getArticleGroups() {
        $shop = Zend_Registry::get('shop');
        $install = Zend_Registry::get('install');

        $articlegroupsd = Doctrine_Query::create()->from('ArticleGroup c')->where('c.shop_id = ? AND c.notinmenu = 0 AND c.enable = 1 AND c.parent=0 AND (language = ? OR language = \'all\')', array(
            $shop['id'],
            Zend_Registry::get('locale')))->orderBy('c.pos ASC')->execute();

        return $articlegroupsd;
    }

    public function getSubArticleGroups($articlegroup_id) {
        $shop = Zend_Registry::get('shop');
        $install = Zend_Registry::get('install');

        $articlegroupsd = Doctrine_Query::create()->from('ArticleGroup c')->where('c.shop_id = ? AND c.notinmenu = 0 AND c.enable = 1 AND c.parent=? AND (language = ? OR language = \'all\')', array(
            $shop['id'], $articlegroup_id,
            Zend_Registry::get('locale')))->orderBy('c.pos ASC')->execute();

        return $articlegroupsd;
    }

}
