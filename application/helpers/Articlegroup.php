<?php

class TP_View_Helper_Articlegroup
{

    public function Articlegroup() {
        return $this;
    }

    public function getTree() {

        $allowIdsGroups = [1];

        $shop = Zend_Registry::get('shop');

        if (Zend_Auth::getInstance()->hasIdentity()) {
            $user = Zend_Auth::getInstance()->getIdentity();
            $user = Doctrine_Query::create()->from('Contact m')->useQueryCache(true)->where('id = ?')->fetchOne(array(
                $user['id']));
            if ($user != false) {

                foreach($user->ContactArticlegroup as $group) {
                    $allowIdsGroups[] = $group->productgroup_id;
                }

                foreach($user->Account->AccountArticlegroup as $group) {
                    $allowIdsGroups[] = $group->productgroup_id;
                }
            }
        }
        $articlesgroups = Doctrine_Query::create()
            ->from('Articlegroup c')
            ->where('c.shop_id = ? AND (c.parent = 0 OR c.parent IS NULL) AND c.notinmenu = 0 AND c.enable = 1 AND (c.private = 0 OR (c.private = 1 AND c.id IN ('.implode(",", $allowIdsGroups).')))', array($shop['id']))->orderby('c.pos ASC')
            ->execute();

        $allowIdsContacts = [1];

        if (Zend_Auth::getInstance()->hasIdentity()) {
            $user = Zend_Auth::getInstance()->getIdentity();
            $user = Doctrine_Query::create()->from('Contact m')->useQueryCache(true)->where('id = ?')->fetchOne(array(
                $user['id']));
            if ($user != false) {

                foreach($user->ContactArticle as $ca) {
                    $allowIdsContacts[] = $ca->article_id;
                }

                foreach($user->Account->AccountArticle as $ca) {
                    $allowIdsContacts[] = $ca->article_id;
                }
            }
        }

        $temp = [];

        foreach($articlesgroups as $row) {
            $articles = Doctrine_Query::create()->from('Article a')
                ->leftJoin('a.ArticleGroupArticle c')->where('c.articlegroup_id = ? AND a.display_not_in_overview = 0 AND (a.private = 0 OR (a.private = 1 AND a.id IN ('.implode(",", $allowIdsContacts).')))', [$row->id])->execute()->count();

            if($articles > 0 || $this->checkSubGroups($row, $allowIdsGroups, $allowIdsContacts)) {
                $temp[] = $row;
            }
        }

        return $temp;

    }


    public function getSubTree($articleGroup) {

        $allowIdsGroups = [1];

        $shop = Zend_Registry::get('shop');

        if (Zend_Auth::getInstance()->hasIdentity()) {
            $user = Zend_Auth::getInstance()->getIdentity();
            $user = Doctrine_Query::create()->from('Contact m')->useQueryCache(true)->where('id = ?')->fetchOne(array(
                $user['id']));
            if ($user != false) {

                foreach($user->ContactArticlegroup as $group) {
                    $allowIdsGroups[] = $group->productgroup_id;
                }

                foreach($user->Account->AccountArticlegroup as $group) {
                    $allowIdsGroups[] = $group->productgroup_id;
                }
            }
        }
        $articlesgroups = Doctrine_Query::create()
            ->from('Articlegroup c')
            ->where('c.shop_id = ? AND c.parent = ? AND c.notinmenu = 0 AND c.enable = 1 AND (c.private = 0 OR (c.private = 1 AND c.id IN ('.implode(",", $allowIdsGroups).')))', array($shop['id'], $articleGroup->id))->orderby('c.pos ASC')
            ->execute();

        $allowIdsContacts = [1];

        if (Zend_Auth::getInstance()->hasIdentity()) {
            $user = Zend_Auth::getInstance()->getIdentity();
            $user = Doctrine_Query::create()->from('Contact m')->useQueryCache(true)->where('id = ?')->fetchOne(array(
                $user['id']));
            if ($user != false) {

                foreach($user->ContactArticle as $ca) {
                    $allowIdsContacts[] = $ca->article_id;
                }

                foreach($user->Account->AccountArticle as $ca) {
                    $allowIdsContacts[] = $ca->article_id;
                }
            }
        }

        $temp = [];

        foreach($articlesgroups as $row) {
            $articles = Doctrine_Query::create()->from('Article a')
                ->leftJoin('a.ArticleGroupArticle c')->where('c.articlegroup_id = ? AND a.display_not_in_overview = 0 AND (a.private = 0 OR (a.private = 1 AND a.id IN ('.implode(",", $allowIdsContacts).')))', [$row->id])->execute()->count();

            if($articles > 0 || $this->checkSubGroups($row, $allowIdsGroups, $allowIdsContacts)) {
                $temp[] = $row;
            }
        }

        return $temp;

    }

    public function getSubgroups($group_id) {
        if ($group_id != "") {

            $shop = Zend_Registry::get('shop');

            $articlesgroups = Doctrine_Query::create()
                ->from('Articlegroup c')
                ->where('c.shop_id = ? AND c.parent = ? AND c.enable = 1', array($shop['id'], $group_id))->orderby('c.pos ASC')
                ->execute();
            return $articlesgroups;

        } else {
            return array();
        }
    }

    public function getTopArticlegroup() {
        $shop = Zend_Registry::get('shop');

        $articlesgroups = Doctrine_Query::create()
            ->from('Articlegroup c')
            ->where('c.shop_id = ? AND c.parent = 0 AND c.notinmenu = 0 AND c.enable = 1', array($shop['id']))->orderby('c.pos ASC')
            ->execute();
        return $articlesgroups;
    }

    public function getByArray($items) {

        if (!is_array($items) || empty($items)) {
            return null;
        }
        $allowIdsGroups = [1];

        if (Zend_Auth::getInstance()->hasIdentity()) {
            $user = Zend_Auth::getInstance()->getIdentity();
            $user = Doctrine_Query::create()->from('Contact m')->useQueryCache(true)->where('id = ?')->fetchOne(array(
                $user['id']));
            if ($user != false) {

                foreach($user->ContactArticlegroup as $group) {
                    $allowIdsGroups[] = $group->productgroup_id;
                }

                foreach($user->Account->AccountArticlegroup as $group) {
                    $allowIdsGroups[] = $group->productgroup_id;
                }
            }
        }

        $rows = Doctrine_Query::create()
            ->from('ArticleGroup a')
            ->where('a.enable = 1 AND a.id IN (' . implode(',', $items) . ') AND (a.private = 0 OR (a.private = 1 AND a.id IN ('.implode(",", $allowIdsGroups).')))')
            ->orderBy('a.pos ASC')->execute();


        $allowIdsContacts = [1];

        if (Zend_Auth::getInstance()->hasIdentity()) {
            $user = Zend_Auth::getInstance()->getIdentity();
            $user = Doctrine_Query::create()->from('Contact m')->useQueryCache(true)->where('id = ?')->fetchOne(array(
                $user['id']));
            if ($user != false) {

                foreach($user->ContactArticle as $ca) {
                    $allowIdsContacts[] = $ca->article_id;
                }

                foreach($user->Account->AccountArticle as $ca) {
                    $allowIdsContacts[] = $ca->article_id;
                }
            }
        }

        $temp = [];

        foreach($rows as $row) {
            $articles = Doctrine_Query::create()->from('Article a')
                ->leftJoin('a.ArticleGroupArticle c')->where('c.articlegroup_id = ? AND a.display_not_in_overview = 0 AND (a.private = 0 OR (a.private = 1 AND a.id IN ('.implode(",", $allowIdsContacts).')))', [$row->id])->execute()->count();

            if($articles > 0 || $this->checkSubGroups($row, $allowIdsGroups, $allowIdsContacts)) {
                $temp[] = $row;
            }
        }

        return $temp;
    }

    private function checkSubGroups($row, array $allowIdsGroups, array $allowIdsContacts)
    {
        $rows = Doctrine_Query::create()
            ->from('ArticleGroup a')
            ->where('a.enable = 1 AND a.parent = ? AND (a.private = 0 OR (a.private = 1 AND a.id IN ('.implode(",", $allowIdsGroups).')))', [$row->id])
            ->orderBy('a.pos ASC')->execute();

        foreach($rows as $row) {

            $articles = Doctrine_Query::create()->from('Article a')
                ->leftJoin('a.ArticleGroupArticle c')->where('c.articlegroup_id = ? AND a.display_not_in_overview = 0 AND (a.private = 0 OR (a.private = 1 AND a.id IN ('.implode(",", $allowIdsContacts).')))', [$row->id])->execute()->count();

            if($articles > 0 || $this->checkSubGroups($row, $allowIdsGroups, $allowIdsContacts)) {
                return true;
            }

        }

        return false;

    }


}