<?php
class TP_View_Helper_Basket
{
    protected $_count = 0;

    public function basket() {
        return $this;
    }

    public function getCount() {
        $basket = new TP_Basket();
        return $basket->getAllCount();
    }

    public function getItems() {
        $basket = new TP_Basket();
        return $basket->getAllArtikel();
    }

    public function getTempProduct() {
        $basket = new TP_Basket();
        return $basket->getTempProduct();
    }

    public function getBasketSum() {
        $basket = new TP_Basket();
        return $basket->getPreisBrutto();
    }

    public function getBasketWeight() {
        $basket = new TP_Basket();
        return $basket->getTempProduct()->getWeight();
    }

    public function isDeliveryZipSameUserZip() {
        $basket = new TP_Basket();
        if ($basket->getPLZ() == $basket->getDeliveryPLZ()) {
            return true;
        }
        return false;
    }
    public function isArticleInBasket($uuid) {
        $basket = new TP_Basket();
        $article = $basket->getBasketArtikelId($uuid);
        if($article == null) {
            return false;
        }
        return $article->getCount();
    }
}
