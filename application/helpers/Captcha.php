<?php

class TP_View_Helper_Captcha
{

    public function Captcha() {
        return $this;
    }

    public function getCaptcha() {
        $captcha = $element = new Zend_Form_Element_Captcha('cp', array(
            'label' => "",
            'captcha' => array(
                'captcha' => 'Image',
                'font' => PUBLIC_PATH . '/layouter/fonts/verdana.ttf',
                'imgDir' => PUBLIC_PATH . '/temp/thumb/',
                'imgUrl' => '/temp/thumb/',
                'wordLen' => '4'
            ),
        ));
        return $captcha;
    }
}