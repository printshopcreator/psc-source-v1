<?php

class TP_View_Helper_Cloud
{

    public function Cloud() {
        return $this;
    }

    public function getProductCloud() {

        $shop = Zend_Registry::get('shop');
        require_once('MongoMapReduce.php');

        $tmp = array();

        if ($shop['pmb']) {

            $mongodb = new MongoClient("mongodb://mongodb:27017");
            $db = $mongodb->selectDB("printshopcreator");

            if(count($db->listCollections()) == 0) {
                $db->createCollection("motive");
                $db->createCollection("shops");
                $db->createCollection("articles");
            }

            $map = new MongoCode('
                    function()	{
                        if(this.private != true && this.install_id == ' . $shop['install_id'] . ' && this.tags) {
                            this.tags.forEach(function(x)   {
                                emit(x.name, {count: 1, url: x.url });
                            });
                        }
                    }
            ');

            $reduce = new MongoCode('
                function(k, v)  {
                    var total = 0;
                    var name = "";
                    for (var i = 0; i < v.length; i++)  {
                        total += v[i].count;
                        name = v[i].url;
                    }
                    return {count: total, url: name};
                }');


            $results = $db->command(array(
                "mapreduce" => "articles",
                "map" => $map,
                "reduce" => $reduce,
                "out" => "articles_tags"));

            $tags = $db->selectCollection($results['result'])->find();
            foreach ($tags as $tag) {
                $tmp[] = array('title' => $tag["_id"], 'weight' => $tag["value"]["count"], 'params' => array('url' => '/overview/all/mode//page/1/sort//desc/' . $tag["value"]['url']));
            }

        } else {

            $mongodb = new MongoClient("mongodb://mongodb:27017");
            $db = $mongodb->selectDB("printshopcreator");
            if(count($db->listCollections()) == 0) {
                $db->createCollection("motive");
                $db->createCollection("shops");
                $db->createCollection("articles");
            }

            $map = new MongoCode('
                    function()	{
                        if(this.private != true && this.shop_id == ' . $shop['id'] . ' && this.tags) {
                            this.tags.forEach(function(x)   {
                                emit(x.name, {count: 1, url: x.url });
                            });
                        }
                    }
            ');

            $reduce = new MongoCode('
                    function(k, v)  {
                    var total = 0;
                    var name = "";
                    for (var i = 0; i < v.length; i++)  {
                        total += v[i].count;
                        name = v[i].url;
                    }
                    return {count: total, url: name};
                }');

            $results = $db->command(array(
                "mapreduce" => "articles",
                "map" => $map,
                "reduce" => $reduce,
                "out" => "articles_tags"));

            $tags = $db->selectCollection($results['result'])->find();

            foreach ($tags as $tag) {
                $tmp[] = array('title' => $tag["_id"], 'weight' => $tag["value"]["count"], 'params' => array('url' => '/overview/all/mode//page/1/sort//desc/' . $tag["value"]['url']));
            }

        }

        return new Zend_Tag_Cloud(array(
            'tags' => $tmp
        ));
    }

    public function getMotivCloud() {

        $shop = Zend_Registry::get('shop');
        require_once('MongoMapReduce.php');

        $tmp = array();

        if ($shop['pmb']) {

            $mongodb = new Mongo();
            $db = $mongodb->selectDB("printshopcreator");

            $map = new MongoCode('
                    function()	{
                        if(this.resale_market == true && this.status >= 50 && this.install_id == ' . $shop['install_id'] . ' && this.tags) {
                            this.tags.forEach(function(x)   {
                                emit(x.name, {count: 1, url: x.url });
                            });
                        }
                    }
            ');

            $reduce = new MongoCode('
                    function(k, v)  {
                    var total = 0;
                    var name = "";
                    for (var i = 0; i < v.length; i++)  {
                        total += v[i].count;
                        name = v[i].url;
                    }
                    return {count: total, url: name};
                }');

            $results = $db->command(array(
                "mapreduce" => "motive",
                "map" => $map,
                "reduce" => $reduce,
                "out" => "motive_tags"));

            $tags = $db->selectCollection($results['result'])->find();

            foreach ($tags as $tag) {
                $tmp[] = array('title' => $tag["_id"], 'weight' => $tag["value"]["count"], 'params' => array('url' => 'motiv/page/1/sort/nothing/asc/' . $tag["value"]['url']));
            }


        } else {

            $mongodb = new Mongo();
            $db = $mongodb->selectDB("printshopcreator");

            $map = new MongoCode('
                    function()	{
                        if(this.resale_shop == true && this.status >= 50 && this.shop_id == ' . $shop['id'] . ' && this.tags) {
                            this.tags.forEach(function(x)   {
                                emit(x.name, {count: 1, url: x.url });
                            });
                        }
                    }
            ');

            $reduce = new MongoCode('
                    function(k, v)  {
                    var total = 0;
                    var name = "";
                    for (var i = 0; i < v.length; i++)  {
                        total += v[i].count;
                        name = v[i].url;
                    }
                    return {count: total, url: name};
                }');

            $results = $db->command(array(
                "mapreduce" => "motive",
                "map" => $map,
                "reduce" => $reduce,
                "out" => "motive_tags"));

            $tags = $db->selectCollection($results['result'])->find();

            foreach ($tags as $tag) {
                $tmp[] = array('title' => $tag["_id"], 'weight' => $tag["value"]["count"], 'params' => array('url' => '/motiv/page/1/sort/nothing/asc/' . $tag["value"]['url']));
            }

        }
        return new Zend_Tag_Cloud(array(
            'tags' => $tmp
        ));
    }
}