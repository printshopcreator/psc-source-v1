<?php
class TP_View_Helper_Cms
{

    public function Cms() {
        return $this;
    }

    public function getCms($where) {

        $shop = Zend_Registry::get('shop');

        $isLoggedIn = "";

        if(!Zend_Auth::getInstance()->hasIdentity()) {
            $isLoggedIn = ' AND c.display_only_logged_in = 0';
        }

        if (is_array($where)) {

            $cms = Doctrine_Query::create()->from('Cms c')->where('c.private = 0 AND c.enable = 1 AND c.notinmenu = 0'.$isLoggedIn.' AND c.id IN (' . implode(',', $where) . ')')->orderBy("FIND_IN_SET(c.id, '" . implode(',', $where) . "')")->execute();
            return $cms;
        }

        $cms = Doctrine_Query::create()->from('Cms c')->where('c.private = 0 AND c.enable = 1 AND c.notinmenu = 0'.$isLoggedIn.' AND c.pos = ? AND c.shop_id = ? AND
                (c.language = ? OR language = \'all\')', array($where, $shop ['id'], Zend_Registry::get('locale')))->orderBy('c.sor ASC')->execute();

        return $cms;

    }

    public function getMyCmsSites() {

        $shop = Zend_Registry::get('shop');

        $user = Zend_Auth::getInstance()->getIdentity();

        $cms = Doctrine_Query::create()->from('Cms c')->leftJoin('c.ContactCms a')->where('c.private = 1 AND c.enable = 1 AND a.contact_id = ? AND c.shop_id = ? AND
                (c.language = ? OR language = \'all\')', array($user['id'], $shop ['id'], Zend_Registry::get('locale')))->orderBy('c.sor ASC')->execute();

        return $cms;

    }

    public function getCmsByParent($id) {

        $shop = Zend_Registry::get('shop');

        $cms = Doctrine_Query::create()->from('Cms c')->where('c.private = 0 AND c.enable = 1 AND c.parent = ?', array($id))->orderBy('c.sor ASC')->execute();

        return $cms;

    }

    public function getCmsById($id) {

        $install = Zend_Registry::get('install');

        $cms = Doctrine_Query::create()->from('Cms c')->where('c.private = 0 AND c.enable = 1 AND c.id = ? AND c.install_id = ? AND
                (c.language = ? OR language = \'all\')', array($id, $install ['id'], Zend_Registry::get('locale')))->fetchOne();
        if (!$cms) {
            $cms = new Cms ();
        }

        return $cms;

    }

    public function getCmsByParameter($id) {

        $shop = Zend_Registry::get('shop');

        $cms = Doctrine_Query::create()->from('Cms c')->where('c.private = 0 AND c.enable = 1 AND c.parameter LIKE ? AND c.shop_id = ? AND
                (c.language = ? OR language = \'all\')', array('%, ' . $id . ',%', $shop ['id'], Zend_Registry::get('locale')))->fetchOne();
        if (!$cms) {

            $cms = Doctrine_Query::create()->from('Cms c')->where('c.private = 0 AND c.enable = 1 AND c.parameter LIKE ? AND c.shop_id = ? AND
                (c.language = ? OR language = \'all\')', array('%' . $id . ',%', $shop ['id'], Zend_Registry::get('locale')))->fetchOne();
            if (!$cms) {
                $cms = Doctrine_Query::create()->from('Cms c')->where('c.private = 0 AND c.enable = 1 AND c.parameter LIKE ? AND c.shop_id = ? AND
                (c.language = ? OR language = \'all\')', array('%, ' . $id . '%', $shop ['id'], Zend_Registry::get('locale')))->fetchOne();

            }

        }
        if (!$cms) {
            $cms = new Cms ();
        }
        return $cms;

    }

    public function getCmsByIdMarket($id) {

        $shop = Zend_Registry::get('shop');

        $cms = Doctrine_Query::create()->from('Cms c')->where('c.id = ?', array($shop[$id]))->fetchOne();

        if (!$cms) {
            $cms = new Cms ();
        }
        return $cms;

    }

    public function getMarketCms() {

        $shop = Zend_Registry::get('shop');
        $install = Zend_Registry::get('install');

        if ($shop ['pmb']) {
            $cms = Doctrine_Query::create()->from('Cms c')->where('c.id = ?', array($shop ['template_display_help_cms']))->fetchOne();
        } else {

            $cms = Doctrine_Query::create()->from('Cms c')->leftJoin('c.Shop as s')->where('s.id = ? AND s.template_display_help_cms = c.id', array($install ['defaultmarket']))->fetchOne();
        }
        if (!$cms) {
            return new Cms ();
        }
        return $cms;

    }

    public function getMarketCmsMenu($pos = 'custom4') {

        $shop = Zend_Registry::get('shop');
        $install = Zend_Registry::get('install');

        if ($shop ['pmb']) {
            $cms = Doctrine_Query::create()->from('Cms c')->where('c.pos = ? AND c.shop_id = ?', array($pos, $shop['id']))->execute();
        } else {

            $cms = Doctrine_Query::create()->from('Cms c')->leftJoin('c.Shop as s')->where('s.id = ? AND c.pos = ?', array($install ['defaultmarket'], $pos))->execute();
        }
        return $cms;

    }
}
