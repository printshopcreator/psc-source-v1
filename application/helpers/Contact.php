<?php

class TP_View_Helper_Contact
{

    public function Contact() {
        return $this;
    }

    public function getContactByUUID($var) {

        return Doctrine_Query::create()
            ->from('Contact c')
            ->where('c.uuid = ?', array($var))
            ->fetchOne();
    }
}
