<?php
/**
 *
 * @author boonkerz
 * @version
 */

/**
 * Date helper
 *
 * @uses viewHelper TP_View_Helper
 */
class TP_View_Helper_Date
{

    /**
     * @var Zend_View_Interface
     */
    public $view;

    /**
     *
     */
    public function date($data) {
        return new Zend_Date($data);
    }

    /**
     * Sets the view field
     * @param $view Zend_View_Interface
     */
    public function setView(Zend_View_Interface $view) {
        $this->view = $view;
    }
}

