<?php

class TP_View_Helper_Designsettings
{

    public function Designsettings() {
        return $this;
    }

    public function get($key = false) {

        $shop = Zend_Registry::get('shop');

        if (!isset($shop['layout_settings'])) {
            return false;
        }

        $settings = Zend_Json::decode($shop['layout_settings']);

        if (!isset($settings[$shop['layout']][$key])) {
            return false;
        }

        return $settings[$shop['layout']][$key];
    }

    public function getStylePath($key) {
        $shop = Zend_Registry::get('shop');
        $market = Zend_Registry::get('market_shop');

        return 'styles/' . $market['uid'] . '/design/subshopdesigns/public/' . $shop['layout'] . '/design/' . $key . '/bootstrap.css';

    }
}
