<?php
class TP_View_Helper_File
{

    public function File() {
        return $this;
    }

    public function getFilePath($id) {
        $dbMongo = TP_Mongo::getInstance();
        $obj = $dbMongo->Media->findOne(array('_id' => new \MongoDB\BSON\ObjectId($id)));
        return '/apps/'.$obj['url'];
    }

    public function getFileName($id) {
        $dbMongo = TP_Mongo::getInstance();
        $obj = $dbMongo->Media->findOne(array('_id' => new \MongoDB\BSON\ObjectId($id)));
        return $obj['title'];
    }
}