<?php

class TP_View_Helper_Image
{

    protected $_count = 0;
    protected $_mode = "";

    public function image()
    {
        return $this;
    }

    public function thumbnailMotiv($alt, $mode, $path, $onlyPath = false, $motiv, $renderNoCopyright = false)
    {

        $this->_mode = $mode;

        if (file_exists(APPLICATION_PATH . '/../market/motive/' . $path)) {
            $image = new Image();
            $image->path = APPLICATION_PATH . '/../market/motive/' . $path;
            $image->id = md5($path);
            if (!$renderNoCopyright) {
                if ($motiv->copyright != "") {
                    Zend_Registry::set('render_copyright_text', $motiv->copyright);
                } else {
                    Zend_Registry::set('render_copyright_shop_id', $motiv->shop_id);
                }
            }

            $imageObj = TP_Image::getInstance();
            $temp = $imageObj->createThumb($mode, $image);

            if ($onlyPath == true) {
                return $temp;
            }
            return $this->getImgCode($alt, $temp);
        }
    }

    public function thumbnailDesigner($alt, $mode, $path, $onlyPath = false)
    {

        $this->_mode = $mode;

        if (file_exists($path)) {
            $image = new Image();
            $image->path = $path;
            $image->id = md5($path);

            $imageObj = TP_Image::getInstance();
            $temp = $imageObj->createThumb($mode, $image, true);

            if ($onlyPath == true) {
                return $temp;
            }
            return $this->getImgCode($alt, $temp);
        }
    }

    public function getObj($id)
    {
        if ($id == "") {
            return false;
        }
        if (strlen($id) < 30) {
            $dbMongo = TP_Mongo::getInstance();
            $obj = $dbMongo->Media->findOne(array('_id' => new \MongoDB\BSON\ObjectId($id)));

            $image = new Image();
            $image->path = 'apps' . $obj['url'];
            $image->id = $obj['_id'];
            $image->name = $obj['title'];
        } else {
            $image = Doctrine_Query::create()
                ->from('Image as i')
                ->where('i.id = ?')->fetchOne(array($id));
        }

        if (!$image) {
            new Image();
        }

        return $image;
    }

    public function getPath($id)
    {
        if ($id == "") {
            return false;
        }

        $image = Doctrine_Query::create()
            ->from('Image as i')
            ->where('i.id = ?')->fetchOne(array($id));

        if (!$image) {
            return false;
        }

        return $image->path;
    }

    public function getLink($id, $ownPath = false)
    {
        if ($id == "") {
            return "";
        }

        $image = Doctrine_Query::create()
            ->from('Image as i')
            ->where('i.id = ?')->fetchOne(array($id));
        if (!$image) {
            return "";
        }
        if ($ownPath == false) {
            return '<a href="' . $image->path . '" target="_blank"><img src="/images/admin/download.png" /></a>';
        } else {
            return '<a href="' . $image->path . '" target="_blank"><img src="' . $ownPath . '" /></a>';
        }
    }

    public function thumbnailSteplayouter($alt, $mode, $article, $session, $onlyPath = false, $class = "")
    {
        Zend_Registry::set('font_path', APPLICATION_PATH . '/design/clients/' . $article->Shop['uid'] . '/fonts');
        $steplayouter = new TP_Steplayouter(json_decode($session->getDesignerXML(), true), $article);
        $steplayouter->buildRenderStack();
        $path = $steplayouter->exportPNG(PUBLIC_PATH . '/steplayouter/temp/' . $session->getArticleId(), 400, 400, 1);



        if (file_exists($path)) {
            $image = new Image();
            $image->path = $path;
            $image->id = md5($path);

            $imageObj = TP_Image::getInstance();
            $temp = $imageObj->createThumb($mode, $image, true);

            if ($onlyPath == true) {

                return $temp;
            }
            return $this->getImgCode($alt, $temp, false, $class);
        }
    }

    public function thumbnailSteplayouter2($alt, $mode, $position, $onlyPath = false, $class = "")
    {

        $pro = unserialize($position->data);
        if ($pro) {
            if ($onlyPath) {
                return '/apps/component/steplayouter/pdf/imagepreview/' . $pro->getLayouterId() . '/500';
            }
            return '<img class="' . $class . '" src="/apps/component/steplayouter/pdf/imagepreview/' . $pro->getLayouterId() . '/500" />';
        }
        return '';
    }

    public function thumbnailSteplayouter2Article($alt, $mode, $article, $onlyPath = false, $class = "")
    {

        return '<img class="' . $class . '" src="/apps/component/steplayouter/pdf/imagepreview/' . $article->uuid . '/500" />';
    }

    public function thumbnailMyOrders(Orderspos $pos, $mode, $onlyPath = false)
    {

        if ($pos->isTemplatePrintId() && TP_Templateprint::isTemplateExistsInBasket($pos)) {

            $path = TP_Templateprint::generatePreviewFromBasket($pos);

            if ($onlyPath == true) {

                return $path;
            }
            return $this->getImgCode($pos->Article->title, $path, false);
        }
    }

    public function thumbnailFop($alt, $mode, $path, $onlyPath = false, $articleId = false, $class = "")
    {

        $this->_mode = $mode;

        if ($articleId) {
            $article = Doctrine_Query::create()
                ->from('Article c')
                ->where('c.id = ?', array(intval($articleId)))
                ->fetchOne();
            if ($article->upload_steplayouter_data != "") {
                if ($onlyPath == true) {

                    return '/apps/component/steplayouter/pdf/imagepreview/' . $article->uuid . '/500';
                }

                return '<img class="' . $class . '" src="/apps/component/steplayouter/pdf/imagepreview/' . $article->uuid . '/500" />';
            } elseif (TP_Templateprint::isTemplateExists($article)) {

                $path = str_replace("..", "/apps", TP_Templateprint::generatePreview($article));

                if ($onlyPath == true) {

                    return $path;
                }
                return $this->getImgCode($alt, $path, false, $class);
            }
        }
        if ($articleId != false && !file_exists($path)) {
            TP_FOP::generatePreview($articleId);
        }

        if (file_exists($path)) {
            $image = new Image();
            $image->path = $path;
            $image->id = md5($path);

            $imageObj = TP_Image::getInstance();
            $temp = $imageObj->createThumb($mode, $image, true);

            if ($onlyPath == true) {

                return $temp;
            }
            return $this->getImgCode($alt, $temp, false, $class);
        }
    }

    public function thumbnailMotivBuy($alt, $mode, $id, $onlyPath = false, $orginal = false, $layouter = false)
    {

        $this->_mode = $mode;

        if ($id != '' && $layouter == false && !is_array($id)) {

            if (file_exists($id)) {
                $imageObj = TP_Image::getInstance();

                $image = new stdClass();
                $image->path = $id;
                $image->id = $alt;

                if ($onlyPath == true) {
                    if ($orginal == true) {
                        return $image->path;
                    } else {
                        $temp = $imageObj->createThumb($mode, $image);
                        return $temp;
                    }
                }
                $temp = $imageObj->createThumb($mode, $image);
                if ($temp == false) {
                    return "";
                }
                return $this->getImgCode($alt, $temp);
            }
        }

        return "";
    }

    public
    function thumbnailImage($alt, $mode, $id, $onlyPath = false, $orginal = false, $layouter = false, $class = "")
    {

        $this->_mode = $mode;
        if ($id != '' && $layouter == false && !is_array($id)) {

            if (strlen($id) < 30) {
                $dbMongo = TP_Mongo::getInstance();
                $obj = $dbMongo->Media->findOne(array('_id' => new \MongoDB\BSON\ObjectId($id)));
                $image = new Image();
                $image->path = '.' . $obj['url'];
                $image->id = $obj['_id'];
            } else {
                $image = Doctrine_Query::create()
                    ->from('Image as i')
                    ->where('i.id = ?')->fetchOne(array($id));
            }

            if ($image != false && file_exists($image->path)) {
                $imageObj = TP_Image::getInstance();

                if ($onlyPath == true) {
                    if ($orginal == true || $image->imagetype == "image/gif") {
                        return '/' . $image->path;
                    } else {
                        $temp = $imageObj->createThumb($mode, $image);
                        return $temp;
                    }
                }
                $temp = $imageObj->createThumb($mode, $image);
                if ($temp == false) {
                    return "";
                }
                return $this->getImgCode($alt, $temp, false, $class);
            }
        }

        if ($layouter == false && is_array($id)) {

            $shop = Zend_Registry::get('shop');

            $image = new Image();
            $image->path = 'uploads/' . $shop['uid'] . '/article/' . $id['value'];
            $image->id = $id['key'];

            if (filesize($image->path) > 50000000) {
                return "";
            }

            if ($image != false && file_exists($image->path)) {

                $imageObj = TP_Image::getInstance();
                $temp = $imageObj->createThumb($mode, $image);

                if ($onlyPath == true) {
                    if ($orginal == true) {
                        return $image->path;
                    } else {
                        return $temp;
                    }
                }
                if ($temp == false) {
                    return "";
                }
                return $this->getImgCode($alt, $temp, false, $class);
            }
        }
        if ($id != '' && $layouter == true) {
            $fileid = md5(time());

            $shop = Zend_Registry::get('shop');

            exec('gm convert -geometry 200 ' . PUBLIC_PATH . '/uploads/' . $shop['uid'] . '/article/' . $id[1] . ' ' . PUBLIC_PATH . '/temp/thumb/' . $id[0] . '.png');
            return $this->getImgCode($alt, 'temp/thumb/' . $id[0] . '.png', true, $class);
        }
        return "";
    }

    public
    function thumbnailPage($alt, $mode, $id, $onlyPath = false, $orginal = false, $layouter = false)
    {

        $this->_mode = $mode;

        if ($layouter == false && is_array($id)) {

            $shop = Zend_Registry::get('shop');

            $image = new Image();
            $image->path = 'uploads/' . $shop['uid'] . '/pages/' . $id['value'];
            $image->id = $id['key'];

            if ($image != false && file_exists($image->path)) {

                $imageObj = TP_Image::getInstance();
                $temp = $imageObj->createThumb($mode, $image);

                if ($onlyPath == true) {
                    if ($orginal == true) {
                        return $image->path;
                    } else {
                        return $temp;
                    }
                }
                if ($temp == false) {
                    return "";
                }
                return $this->getImgCode($alt, $temp);
            }
        }

        return "";
    }

    protected
    function getImgCode($alt, $path, $border = false, $class = "")
    {
        if ($border == true) {
            return '<img id="' . $this->_mode . '" border="1" src="' . $path . '" class="articlelistimg ' . $class . '" alt="' . $alt . '" title="' . $alt . '" />';
        }
        return '<img id="' . $this->_mode . '" src="' . $path . '" class="articlelistimg ' . $class . '" alt="' . $alt . '" title="' . $alt . '" />';
    }
}
