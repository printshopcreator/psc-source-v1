<?php
class TP_View_Helper_JQuery
{
    protected $_count = 0;

    public function JQuery() {
        return $this;
    }

    public function fgmenu(Zend_View_Helper_Navigation_Breadcrumbs $nav) {
        $container = $nav->getContainer();
        if (!$active = $nav->findActive($container)) {
            return '';
        }

        $active = $active['page'];
        $html = "";
        while ($parent = $active->getParent()) {
            if ($parent instanceof Zend_Navigation_Page) {
                // prepend crumb to html
                $html = "$('#menu-" . $parent->get('id') . "-e').click();" . $html;
            }

            if ($parent === $container) {
                // at the root of the given container
                break;
            }

            $active = $parent;
        }
        return $html;
    }
}
