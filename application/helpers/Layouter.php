<?php

class TP_View_Helper_Layouter
{

    protected $_count = 0;

    public function Layouter() {
        return $this;
    }

    public function getTitle($layouterId) {

        $articleSession = new TP_Layoutersession();
        $articleSess = $articleSession->getLayouterArticle($layouterId);

        return $articleSess->getTitle();
    }

}
