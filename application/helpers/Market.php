<?php
class TP_View_Helper_Market
{
    protected $_count = 0;

    public function Market() {
        $shop = Zend_Registry::get('shop');
        $install = Zend_Registry::get('install');

        if ($install['defaultmarket'] == $shop['id']) {

            return false;

        } else {
            $shop = Doctrine_Query::create()
                ->from('Shop c')
                ->where('c.id = ?', array($install['defaultmarket']))
                ->fetchOne();
        }
        return $shop;
    }
}
