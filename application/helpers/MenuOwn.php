<?php
class TP_View_Helper_MenuOwn
{
    protected $_count = 0;

    public function menuOwn($menu = 'left') {
        $shop = Zend_Registry::get('shop');

        $rows = Doctrine_Query::create()
            ->from('Cms m')
            ->where('notinmenu != 1 AND shop_id = ? AND pos = ? AND (language = ? OR language = \'all\')', array(intval($shop['id']), $menu, Zend_Registry::get('locale')))
            ->orderBy('sor ASC')->execute();

        return $rows;
    }
}
