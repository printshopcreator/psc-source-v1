<?php
class TP_View_Helper_Motive
{
    protected $_count = 0;

    public function motive() {
        return $this;
    }

    public function getMotiveByArray($items) {
        $shop = Zend_Registry::get('shop');

        $rows = Doctrine_Query::create()
            ->from('Motiv m')
            ->where('m.id IN (' . implode(',', $items) . ')')->execute();

        return $rows;
    }
}
