<?php

class TP_View_Helper_MotivpriceElement extends Zend_View_Helper_FormElement
{

    protected $html = "";

    public function motivpriceElement($name, $value = null, $attribs = array()) {

        $helper = new Zend_View_Helper_FormText();
        $helper->setView($this->view);

        $this->html = $helper->formText($name, $value, $attribs);
        $this->html .= " € incl. 19% MwSt";

        return $this->html;

    }

}