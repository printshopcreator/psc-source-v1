<?php
class TP_View_Helper_Msp
{

    public function msp() {
        return $this;
    }

    public function getKostenstelleKusch($kst) {
        if (Zend_Registry::get('locale')->getLanguage() . '_' . Zend_Registry::get('locale')->getRegion() != 'de_DE') {

            $tmp['1'] = 'International';
            $tmp['9037'] = '9037&nbsp;Airport Seating Division';
            $tmp['9035'] = '9035&nbsp;Asien/Pazifik und S&uuml;dafrika ';
            $tmp['9021'] = '9021&nbsp;Belgium';
            $tmp['9023'] = '9023&nbsp;Eastern Europe and Russia';
            $tmp['9013'] = '9013&nbsp;Export Director';
            $tmp['9017'] = '9017&nbsp;France';
            $tmp['9020'] = '9020&nbsp;Great Britain and Ireland';
            $tmp['9025'] = '9025&nbsp;Middle East';
            $tmp['9015'] = '9015&nbsp;Netherlands';
            $tmp['9014'] = '9014&nbsp;Other countries';
            $tmp['9010'] = '9010&nbsp;Sales support international';
            $tmp['9024'] = '9024&nbsp;Scandinavia';
            $tmp['9022'] = '9022&nbsp;Spain and South America';
            $tmp['9026'] = '9026&nbsp;Switzerland and South Tyrol';
            $tmp['9016'] = '9016&nbsp;USA';
            $tmp['2'] = 'Germany';
            $tmp['9390.2'] = '9390&nbsp;Kusch+Co DK Sachsen GmbH, D&ouml;beln';
            $tmp['9470'] = '9470&nbsp;Office Gro&szlig;-Gerau';
            $tmp['9410'] = '9410&nbsp;Office Munich';
            $tmp['9330'] = '9330&nbsp;Office Pohle';
            $tmp['9432'] = '9432&nbsp;Sales Arndt, Gerd';
            $tmp['9433'] = '9433&nbsp;Sales Becker, J&ouml;rg';
            $tmp['9474'] = '9474&nbsp;Sales Becker, Petra';
            $tmp['9416'] = '9416&nbsp;Sales Bitter, Christine';
            $tmp['9300'] = '9300&nbsp;Sales Director Germany';
            $tmp['9331'] = '9331&nbsp;Sales Fries, Hans J&uuml;rgen ';
            $tmp['9434'] = '9434&nbsp;Sales Garthoff, Kai';
            $tmp['9451'] = '9451&nbsp;Sales Hering, Monika';
            $tmp['9352'] = '9352&nbsp;Sales Hesse, Heiko';
            $tmp['9392'] = '9392&nbsp;Sales Wolf, Jan';
            $tmp['9471'] = '9471&nbsp;Sales Kalisch, Olaf';
            $tmp['9412'] = '9412&nbsp;Sales Lang, Uwe';
            $tmp['9311'] = '9311&nbsp;Sales Mehlig, Tobias';
            $tmp['9435'] = '9435&nbsp;Sales M&uuml;nch, Lothar';
            $tmp['9312'] = '9312&nbsp;Sales P&ouml;llmann, Thomas ';
            $tmp['9453'] = '9453&nbsp;Sales Reichelt, Jens';
            $tmp['9414'] = '9414&nbsp;Sales Rother, Fathia';
            $tmp['9333'] = '9333&nbsp;Sales Schmidt, Hans Joachim ';
            $tmp['9332'] = '9332&nbsp;Sales Schwarz, Wilhelm';
            $tmp['9415'] = '9415&nbsp;Sales Zeidler, Simone Gesine ';
            $tmp['9000'] = '9000&nbsp;Sales support Germany';
            $tmp['9310'] = '9310&nbsp;Showroom Berlin';
            $tmp['9160'] = '9160&nbsp;Showroom Cologne';
            $tmp['9390.1'] = '9390&nbsp;Thomas Lange GmbH & Co KG, Hamburg';
            $tmp['3'] = 'Hallenberg';
            $tmp['9112'] = '9112&nbsp;Business Development';
            $tmp['9150'] = '9150&nbsp;Infocenter';
            $tmp['9110'] = '9110&nbsp;Marketing';
            $tmp['9120'] = '9120&nbsp;Exhibition Germany';
            $tmp['9121'] = '9121&nbsp;Exhibition Orgatec';
            $tmp['9122'] = '9122&nbsp;Exhibition Medica';
            $tmp['9123'] = '9123&nbsp;Exhibition inter airport';
            $tmp['9124'] = '9124&nbsp;Exhibition Altenpflege';
            $tmp['9125'] = '9125&nbsp;Exhibition EVVC';
            $tmp['9130'] = '9130&nbsp;Exhibition International';
            $tmp['9131'] = '9131&nbsp;Exhibition Interieur';
            $tmp['9137'] = '9137&nbsp;Exhibition Mailand';
            $tmp['9132'] = '9132&nbsp;Exhibition Passenger Terminal EXPO';
            $tmp['9133'] = '9133&nbsp;Exhibition Stockholm Furniture Fair';
            $tmp['9134'] = '9134&nbsp;Exhibition ACI Veranstaltungen';
            $tmp['9135'] = '9135&nbsp;Exhibition Health Care Mechelen';
            $tmp['9136'] = '9136&nbsp;Exhibition EXPO60+';
            $tmp['8970'] = '8970&nbsp;Sample stock Hallenberg';
            $tmp['8140'] = '8140&nbsp;Human Resoure Department';
            $tmp['8930'] = '8930&nbsp;Project team Hallenberg';

        } else {

            $tmp['1'] = 'Ausland';
            $tmp['9037'] = '9037&nbsp;Airport Seating Division';
            $tmp['9035'] = '9035&nbsp;Asia/Pacific and South Africa ';
            $tmp['9021'] = '9021&nbsp;Belgien';
            $tmp['9017'] = '9017&nbsp;Frankreich';
            $tmp['9020'] = '9020&nbsp;Gro&szlig;britannien und Irland';
            $tmp['9025'] = '9025&nbsp;Mittlerer Osten';
            $tmp['9015'] = '9015&nbsp;Niederlande';
            $tmp['9023'] = '9023&nbsp;Osteuropa und Russland ';
            $tmp['9013'] = '9013&nbsp;Ressortleitung Vertrieb International';
            $tmp['9014'] = '9014&nbsp;restliche Welt';
            $tmp['9026'] = '9026&nbsp;Schweiz und S&uuml;dtirol';
            $tmp['9024'] = '9024&nbsp;Skandinavien';
            $tmp['9022'] = '9022&nbsp;Spanien und S&uuml;damerika';
            $tmp['9016'] = '9016&nbsp;USA';
            $tmp['9010'] = '9010&nbsp;Vertriebsinnendienst International';
            $tmp['2'] = 'Inland';
            $tmp['9470'] = '9470&nbsp;B&uuml;ro Gro&szlig;-Gerau';
            $tmp['9410'] = '9410&nbsp;B&uuml;ro M&uuml;nchen';
            $tmp['9330'] = '9330&nbsp;B&uuml;ro Pohle';
            $tmp['9390'] = '9390&nbsp;Kusch+Co DK Sachsen GmbH, D&ouml;beln';
            $tmp['9300'] = '9300&nbsp;Ressortleitung Vertrieb National';
            $tmp['9310'] = '9310&nbsp;Showroom Berlin';
            $tmp['9160'] = '9160&nbsp;Showroom K&ouml;ln';
            $tmp["9390.1"] = '9390&nbsp;Thomas Lange GmbH & Co KG, Hamburg';
            $tmp['9432'] = '9432&nbsp;Vertrieb Arndt, Gerd';
            $tmp['9433'] = '9433&nbsp;Vertrieb Becker, J&ouml;rg';
            $tmp['9474'] = '9474&nbsp;Vertrieb Becker, Petra';
            $tmp['9416'] = '9416&nbsp;Vertrieb Bitter, Christine';
            $tmp['9331'] = '9331&nbsp;Vertrieb Fries, Hans J&uuml;rgen ';
            $tmp['9434'] = '9434&nbsp;Vertreib Garthoff, Kai';
            $tmp['9451'] = '9451&nbsp;Vertrieb Hering, Monika';
            $tmp['9352'] = '9352&nbsp;Vertrieb Hesse, Heiko';
            $tmp['9392'] = '9392&nbsp;Vertrieb Wolf, Jan';
            $tmp['9471'] = '9471&nbsp;Vertrieb Kalisch, Olaf';
            $tmp['9412'] = '9412&nbsp;Vertrieb Lang, Uwe';
            $tmp['9311'] = '9311&nbsp;Vertrieb Mehlig, Tobias';
            $tmp['9435'] = '9435&nbsp;Vertrieb M&uuml;nch, Lothar';
            $tmp['9312'] = '9312&nbsp;Vertrieb P&ouml;llmann, Thomas ';
            $tmp['9453'] = '9453&nbsp;Vertrieb Reichelt, Jens';
            $tmp['9414'] = '9414&nbsp;Vertrieb Rother, Fathia';
            $tmp['9333'] = '9333&nbsp;Vertrieb Schmidt, Hans Joachim ';
            $tmp['9332'] = '9332&nbsp;Vertrieb Schwarz, Wilhelm';
            $tmp['9415'] = '9415&nbsp;Vertrieb Zeidler, Simone Gesine ';
            $tmp['9000'] = '9000&nbsp;Vertriebsinnendienst National';
            $tmp['3'] = 'Hallenberg';
            $tmp['9112'] = '9112&nbsp;Business Development';
            $tmp['9150'] = '9150&nbsp;Infocenter';
            $tmp['9110'] = '9110&nbsp;Marketing';
            $tmp['9120'] = '9120&nbsp;Messen Inland';
            $tmp['9121'] = '9121&nbsp;Messe Orgatec';
            $tmp['9122'] = '9122&nbsp;Messe Medica';
            $tmp['9123'] = '9123&nbsp;Messe inter airport';
            $tmp['9124'] = '9124&nbsp;Messe Altenpflege';
            $tmp['9125'] = '9125&nbsp;Messe EVVC';
            $tmp['9130'] = '9130&nbsp;Messen Ausland';
            $tmp['9131'] = '9131&nbsp;Messe Interieur';
            $tmp['9137'] = '9137&nbsp;Messe Mailand';
            $tmp['9132'] = '9132&nbsp;Messe Passenger Terminal EXPO';
            $tmp['9133'] = '9133&nbsp;Messe Stockholm Furniture Fair';
            $tmp['9134'] = '9134&nbsp;Messe ACI Veranstaltungen';
            $tmp['9135'] = '9135&nbsp;Messe Health Care Mechelen';
            $tmp['9136'] = '9136&nbsp;Messe EXPO60+';
            $tmp['8970'] = '8970&nbsp;Musterlager Hallenberg';
            $tmp['8140'] = '8140&nbsp;Personalwesen';
            $tmp['8930'] = '8930&nbsp;Projektteam Hallenberg';

        }

        return $tmp[$kst];
    }
}
