<?php

class TP_View_Helper_News
{

    protected $_count = 0;

    public function news() {
        return $this;
    }

    public function getShopNews($limit = 5) {
        $shop = Zend_Registry::get('shop');

        $news = Doctrine_Query::create()
            ->from('News c')
            ->where('c.shop_id = ? AND c.active = 1 AND
                            (c.language = ? OR language = \'all\')', array($shop['id'], Zend_Registry::get('locale')))
            ->orderBy('c.sort_date DESC')
            ->limit($limit)
            ->execute();

        return $news;
    }

    public function getMarketShopNews($limit = 5) {
        $install = Zend_Registry::get('install');

        $news = Doctrine_Query::create()
            ->from('News c')
            ->where('c.shop_id = ? AND c.active = 1 AND
                            (c.language = ? OR language = \'all\')', array($install['defaultmarket'], Zend_Registry::get('locale')))
            ->orderBy('c.sort_date DESC')
            ->limit($limit)
            ->execute();
        return $news;
    }

}
