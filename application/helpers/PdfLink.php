<?php
class TP_View_Helper_PdfLink
{
    protected $_count = 0;

    public function PdfLink($id) {
        if ($id != '') {
            $image = Doctrine_Query::create()
                ->from('Image as i')
                ->where('i.id = ?')->fetchOne(array($id));
            if ($image != null) {
                return $image->path;
            }
        }
        return "";
    }

}
