<?php
class TP_View_Helper_Price
{

    public $highRole;

    public function price($price, $article) {

        if (Zend_Auth::getInstance()->hasIdentity()) {

            $user = Zend_Auth::getInstance()->getIdentity();
            $user = Doctrine_Query::create()
                ->from('Contact m')
                ->where('id = ?')->fetchOne(array($user['id']));
            if ($user != false) {
                foreach ($user->Roles as $rol) {
                    if ($this->highRole == null || $rol->level > $this->highRole->level) {
                        $this->highRole = $rol;
                    }
                }
            }
            if ($user->mwert != true) {
                return $price;
            }
            if ($user->Account->mwert != true) {
                return $price;
            }
        }
        return $price + ($price / 100 * $article->mwert);
    }

}
