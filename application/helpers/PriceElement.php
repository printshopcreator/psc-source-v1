<?php

class TP_View_Helper_PriceElement extends Zend_View_Helper_FormElement
{

    protected $html = "";

    public function priceElement($name, $value = null, $attribs = array()) {

        $helper = new Zend_View_Helper_FormText();
        $helper->setView($this->view);

        $wiz = new TP_ResaleWizard();
        $article = $wiz->getArticle();

        if ($article->a6_org_article != 0 && $article->a6_org_article != "") {
            $article = $article->OrgArticle;
        }
        $this->html .= $helper->formText($name, $value, $attribs);
        $this->html .= " € incl. " . $article->mwert . "% MwSt";

        return $this->html;

    }

}