<?php

class TP_View_Helper_Rights
{

    public function Rights() {
        return $this;
    }

    public function isShopAdmin() {

        if (Zend_Auth::getInstance()->hasIdentity()) {
            $user = Zend_Auth::getInstance()->getIdentity();
            $user = Doctrine_Query::create()->from('Contact m')->useQueryCache(true)->where('id = ?')->fetchOne(array(
                $user['id']));
            if ($user != false) {

                foreach ($user->ShopContact as $shop) {
                    if ($shop->admin == true) {
                        return true;
                    }
                }
            }
        }

        return false;
    }

    public function role() {
        if (Zend_Auth::getInstance()->hasIdentity()) {
            $user = Zend_Auth::getInstance()->getIdentity();
            $user = Doctrine_Query::create()->from('Contact m')->useQueryCache(true)->where('id = ?')->fetchOne(array(
                $user['id']));
            if ($user != false) {
                $highRole = false;
                foreach ($user->Roles as $rol) {
                    if ($highRole == null || $rol->level > $highRole->level)
                        $highRole = $rol;
                }
                return $highRole;
            }
        }
        return false;
    }

    public function getShopAdminUrl() {

        if (Zend_Auth::getInstance()->hasIdentity()) {
            $user = Zend_Auth::getInstance()->getIdentity();
            $user = Doctrine_Query::create()->from('Contact m')->useQueryCache(true)->where('id = ?')->fetchOne(array(
                $user['id']));
            if ($user != false) {

                foreach ($user->ShopContact as $shop) {
                    if ($shop->admin == true) {
                        return 'http://' . $shop->Shop->Domain[0]->name . '/admin/app';
                    }
                }
            }
        }

        return false;

    }
}
