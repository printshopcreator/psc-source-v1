<?php

class TP_View_Helper_Setting
{

    public function setting() {
        return $this;
    }

    public function getSlides() {
        $shop = Zend_Registry::get('shop');

        $slides = Doctrine_Query::create()
            ->from('Shopsetting c')
            ->where("c.shop_id = ? AND c.keyid = 'index_slides'", array($shop['id']))
            ->execute();
        $tempslides = array();
        foreach ($slides as $slide) {
            $tempslides[] = array_merge($slide->toArray(), Zend_Json::decode($slide->value));
        }
        return $tempslides;
    }

}
