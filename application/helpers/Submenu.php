<?php
class TP_View_Helper_Submenu
{
    protected $_count = 0;

    public function submenu($id, $parent, $level = 1) {

        $rootline = array();

        $current = $this->getRecord($id);

        array_push($rootline, $current);

        while (true) {

            if ($current['parent'] == 0 || $current['parent'] == '' || $current['parent'] == '0') {
                break;
            }
            $current = $this->getRecord($current['parent']);
            //if($current['parent'] == 0 || $current['parent'] == '' || $current['parent'] == '0') break;
            array_push($rootline, $current);

        }
        //var_dump($rootline);
        $rootline = array_reverse($rootline);
        //var_dump($rootline);

        if (isset($rootline[$level]['id'])) {
            $current = $this->getRecords($rootline[$level]['id']);
        } else {
            $current = array();
        }

        return $current;
    }

    protected function getRecord($id) {

        $shop = Zend_Registry::get('shop');

        $var = Doctrine_Query::create()
            ->from('Cms m')
            ->where('id = ? AND (language = ? OR language = \'all\')', array($id, Zend_Registry::get('locale')))
            ->orderBy('sor ASC')->fetchOne();
        return $var->toArray();

    }

    protected function getRecords($id) {

        $shop = Zend_Registry::get('shop');

        return Doctrine_Query::create()
            ->from('Cms m')
            ->where('notinmenu != 1 AND shop_id = ? AND parent = ? AND (language = ? OR language = \'all\')', array(intval($shop['id']), $id, Zend_Registry::get('locale')))
            ->orderBy('sor ASC')->execute();

    }
}
