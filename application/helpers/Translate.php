<?php

class TP_View_Helper_Translate
{

    public function translate() {
        $argss = func_get_args();
        $args = array_shift($argss);
        $modeRte = strtolower(array_shift($argss));
        $saveId = array_shift($argss);

        $translate = Zend_Registry::get('Zend_Translate');
        if ($saveId != null) {

            $str = $translate->translate($saveId);

            if ($str == $saveId) {
                $str = $args;
            }
            $mode = new Zend_Session_Namespace('adminmode');
            if ($mode->liveedit && $modeRte == 'text') {
                return '<span class="liveedit" mode="text" table="system" field="frontend" id="' . $saveId . '">' . $str . '</span>';
            } elseif ($mode->liveedit && $modeRte == 'rte') {
                return '<span class="liveedit" mode="wysiwyg" table="system" field="frontend" id="' . $saveId . '">' . $str . '</span>';
            } elseif ($mode->liveedit && $modeRte == 'textarea') {
                return '<span class="liveedit" mode="textarea" table="system" field="frontend" id="' . $saveId . '">' . $str . '</span>';
            }
            return $str;
        }

        $str = $translate->translate($args);

        $mode = new Zend_Session_Namespace('adminmode');
        if ($mode->liveedit && $modeRte == 'text') {
            return '<span class="liveedit" mode="text" table="system" field="frontend" id="' . $args . '">' . $str . '</span>';
        } elseif ($mode->liveedit && $modeRte == 'rte') {
            return '<span class="liveedit" mode="wysiwyg" table="system" field="frontend" id="' . $args . '">' . $str . '</span>';
        } elseif ($mode->liveedit && $modeRte == 'textarea') {
            return '<span class="liveedit" mode="textarea" table="system" field="frontend" id="' . $args . '">' . $str . '</span>';
        }

        return $str;
    }

}
