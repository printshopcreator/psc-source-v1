<?php

class Admin_AccountController extends TP_Admin_Controller_Action
{

    /**
     * IndexAction
     *
     * @todo Dokumentation der IndexAction
     *
     * @return void
     */
    public function allAction() {

        if ($this->highRole->level < 35) {

            if ($this->_request->getParam('config') == 1) {

                $temp = array();

                $rows = Doctrine_Query::create()
                    ->from('Orderspos m')
                    ->limit(intval($this->_request->getParam('limit')))
                    ->offset(intval($this->_request->getParam('start')))
                    ->leftJoin('m.Orders as d')
                    ->leftJoin('d.Contact as c')
                    ->addWhere('d.enable = 1 AND m.shop_id IN (' . implode(',', $this->shopIds) . ')');

                if (intval($this->_request->getParam('sid')) != 0) {
                    $rows->where('m.shop_id = ?', array(intval($this->_request->getParam('sid'))));
                }
                if (intval($this->_request->getParam('account_id')) != 0) {
                    $rows->where('m.account_id = ?', array(intval($this->_request->getParam('account_id'))));
                }
                if (intval($this->_request->getParam('contact_id')) != 0) {
                    $rows->where('c.contact_id = ?', array(intval($this->_request->getParam('contact_id'))));
                }
                if ($this->_request->getParam('sort')) {
                    $rows->orderBy($this->_request->getParam('sort') . ' ' . $this->_request->getParam('dir'));
                }

                TP_Filter::proccess($rows);

                $rows = $rows->execute();
                $configStatus = new Zend_Config_Ini(APPLICATION_PATH . '/configs/' . '/systemdefaults.ini', 'status');

                $translateStatus = Zend_Registry::get('translate');

                foreach ($rows as $row) {

                    array_push($temp, array('id' => $row->id,
                        'd/account' => $row->Orders->Account->company,
                        'c/self_firstname' => $row->Orders->Contact->self_firstname,
                        'c/self_lastname' => $row->Orders->Contact->self_lastname,
                        'c/self_department' => $row->Orders->Contact->self_department,
                        'd/alias' => $row->Orders->alias,
                        'm/shop_id' => $row->Orders->Shop->name,
                        'm/createdd' => $row->Orders->created,
                        'm/price' => $row->resale_price));
                }

                $count = Doctrine_Query::create()
                    ->from('Orderspos m')
                    ->leftJoin('m.Orders as d')
                    ->leftJoin('d.Contact as c')
                    ->addWhere('m.shop_id IN (' . implode(',', $this->shopIds) . ')');
                if (intval($this->_request->getParam('account_id')) != 0) {
                    $count->addWhere('m.account_id = ?', array(intval($this->_request->getParam('account_id'))));
                }
                if (intval($this->_request->getParam('sid')) != 0) {
                    $count->where('m.shop_id = ?', array(intval($this->_request->getParam('sid'))));
                }
                if (intval($this->_request->getParam('contact_id')) != 0) {
                    $count->addWhere('c.contact_id = ?', array(intval($this->_request->getParam('contact_id'))));
                }
                TP_Filter::proccess($count);

                $config = array(
                    "success" => true
                , "totalCount" => $count->count()
                , "rows" => $temp
                );

                $this->getHelper('Json')->sendJson($config);
            }
        }

        if ($this->_request->getParam('config') == 1) {

            $temp = array();

            $rows = Doctrine_Query::create()
                ->select('m.id')
                ->from('Orders m')
                ->limit(intval($this->_request->getParam('limit')))
                ->offset(intval($this->_request->getParam('start')))
                ->leftJoin('m.Contact as c')
                ->leftJoin('m.Shop as s')
                ->addWhere('m.shop_id IN (' . implode(',', $this->shopIds) . ')');

            if (intval($this->_request->getParam('sid')) != 0) {
                $rows->where('shop_id = ?', array(intval($this->_request->getParam('sid'))));
            } else {
                if ($this->_getParam('market') == 1) {
                    $rows->addWhere('s.market = 1');
                } else {
                    $rows->addWhere('s.market = 0');
                }
            }
            if (intval($this->_request->getParam('account_id')) != 0) {
                $rows->where('account_id = ?', array(intval($this->_request->getParam('account_id'))));
            }
            if (intval($this->_request->getParam('contact_id')) != 0) {
                $rows->where('contact_id = ?', array(intval($this->_request->getParam('contact_id'))));
            }
            if ($this->_request->getParam('sort')) {
                $rows->orderBy(str_replace('/', '.', $this->_request->getParam('sort') . ' ' . $this->_request->getParam('dir')));
            }

            TP_Filter::proccess($rows);

            $rows = $rows->execute();
            $configStatus = new Zend_Config_Ini(APPLICATION_PATH . '/configs/' . '/systemdefaults.ini', 'status');

            $translateStatus = Zend_Registry::get('translate');
            $tmp = array();
            if ($this->install->offline_custom2 != "") {
                try {

                    $maschienen = new SimpleXMLElement($this->install->offline_custom2, null, false);

                    foreach ($maschienen as $maschine) {
                        $tmp[(int)$maschine['id']] = $translateStatus->translate((string)$maschine);
                    }
                } catch (Exception $e) {

                }
            }

            foreach ($rows as $row) {

                if ($row->status) {
                    $tempStatus = $configStatus->toArray();

                    $row->status = $translateStatus->translate($row->status);
                }

                if (isset($tmp[$row->status])) {
                    $row->status = $tmp[$row->status];
                }

                array_push($temp, array('id' => $row->id,
                    'account' => $row->Account->company,
                    'c/self_firstname' => $row->Contact->self_firstname,
                    'c/self_lastname' => $row->Contact->self_lastname,
                    'c/self_department' => $row->Contact->self_department,
                    'status' => $row->status,
                    'alias' => $row->alias,
                    'shop_id' => $row->Shop->name,
                    'created' => $row->created,
                    'basketfield2' => $row->basketfield2,
                    'enable' => $row->enable));
            }

            $count = Doctrine_Query::create()
                ->select('m.id')
                ->from('Orders m')
                ->leftJoin('m.Contact as c')
                ->addWhere('m.shop_id IN (' . implode(',', $this->shopIds) . ')');
            if (intval($this->_request->getParam('account_id')) != 0) {
                $count->addWhere('account_id = ?', array(intval($this->_request->getParam('account_id'))));
            }
            if (intval($this->_request->getParam('sid')) != 0) {
                $count->where('shop_id = ?', array(intval($this->_request->getParam('sid'))));
            }
            if (intval($this->_request->getParam('contact_id')) != 0) {
                $count->addWhere('contact_id = ?', array(intval($this->_request->getParam('contact_id'))));
            }
            TP_Filter::proccess($count);

            $config = array(
                "success" => true
            , "totalCount" => $count->count()
            , "rows" => $temp
            );

            $this->getHelper('Json')->sendJson($config);
        }

        if ($this->_request->getParam('config') == 2) {

            $rows = Doctrine_Query::create()
                ->from('Account m')
                ->limit(intval($this->_request->getParam('limit')))
                ->offset(intval($this->_request->getParam('start')))
                ->leftJoin('m.Shops as s')
                ->addWhere('s.id IN (' . implode(',', $this->shopIds) . ')');
            if (intval($this->_request->getParam('sid')) != 0) {
                $rows->addWhere('s.id = ?', array(intval($this->_request->getParam('sid'))));
            } else {
                if ($this->_getParam('market') == 1) {
                    $rows->addWhere('s.market = 1');
                } else {
                    $rows->addWhere('s.market = 0');
                }
            }
            if (intval($this->_request->getParam('account_id')) != 0) {
                $rows->addWhere('account_id = ?', array(intval($this->_request->getParam('account_id'))));
            }
            if ($this->_request->getParam('sort')) {
                $rows->orderBy($this->_request->getParam('sort') . ' ' . $this->_request->getParam('dir'));
            }

            TP_Filter::proccess($rows);

            $rows = $rows->execute();

            $count = Doctrine_Query::create()
                ->from('Account m')
                ->leftJoin('m.Shops as s')
                ->addWhere('s.id IN (' . implode(',', $this->shopIds) . ')');
            if (intval($this->_request->getParam('sid')) != 0) {
                $count->addWhere('s.id = ?', array(intval($this->_request->getParam('sid'))));
            } else {
                if ($this->_getParam('market') == 1) {
                    $count->addWhere('s.market = 1');
                } else {
                    $count->addWhere('s.market = 0');
                }
            }
            if (intval($this->_request->getParam('account_id')) != 0) {
                $count->where('account_id = ?', array(intval($this->_request->getParam('account_id'))));
            }

            TP_Filter::proccess($count);

            $config = array(
                "success" => true
            , "totalCount" => $count->count()
            , "rows" => $rows->toArray(false)
            );

            $this->getHelper('Json')->sendJson($config);
        }

        if ($this->_request->getParam('config') == 3) {

            $rows = Doctrine_Query::create()
                ->select('m.self_firstname')
                ->from('Contact m')
                ->limit(intval($this->_request->getParam('limit')))
                ->offset(intval($this->_request->getParam('start')))
                ->leftJoin('m.ShopContact as s')
                ->leftJoin('s.Shop as sh')
                ->addWhere('m.id != 5 AND s.shop_id IN (' . implode(',', $this->shopIds) . ')');

            if ($this->highRole->level < 40) {
                $rows->addWhere('m.id != ?', $this->install->admincontact);
            }
            if (intval($this->_request->getParam('sid')) != 0) {
                $rows->addWhere('s.shop_id = ?', array(intval($this->_request->getParam('sid'))));
            } else {
                if ($this->_getParam('market') == 1) {
                    $rows->addWhere('sh.market = 1');
                } else {
                    $rows->addWhere('sh.market = 0');
                }
            }
            if (intval($this->_request->getParam('account_id')) != 0) {
                $rows->addWhere('m.account_id = ?', array(intval($this->_request->getParam('account_id'))));
            }
            if ($this->_request->getParam('sort')) {
                $rows->orderBy($this->_request->getParam('sort') . ' ' . $this->_request->getParam('dir'));
            }

            TP_Filter::proccess($rows);
            $rows = $rows->execute();

            $count = Doctrine_Query::create()
                ->select('m.self_firstname')
                ->from('Contact m')
                ->leftJoin('m.ShopContact as s')
                ->leftJoin('s.Shop as sh')
                ->addWhere('m.id != 5 AND s.shop_id IN (' . implode(',', $this->shopIds) . ')');
            if ($this->highRole->level < 40) {
                $count->addWhere('m.id != ?', $this->install->admincontact);
            }
            if (intval($this->_request->getParam('sid')) != 0) {
                $count->addWhere('s.shop_id = ?', array(intval($this->_request->getParam('sid'))));
            } else {
                if ($this->_getParam('market') == 1) {
                    $count->addWhere('sh.market = 1');
                } else {
                    $count->addWhere('sh.market = 0');
                }
            }
            if (intval($this->_request->getParam('account_id')) != 0) {
                $count->where('account_id = ?', array(intval($this->_request->getParam('account_id'))));
            }

            TP_Filter::proccess($count);

            $count = $count->execute();

            $config = array(
                "success" => true
            , "totalCount" => $count->count(false)
            , "rows" => $rows->toArray(false)
            );

            $this->getHelper('Json')->sendJson($config);
        }

        if ($this->_getParam('id') == 11) {
            $this->view->initialData[] = array('market' => 1);
        } else {
            $this->view->initialData[] = array('market' => 0);
        }

        $this->_helper->layout->setLayout('simple');
    }

    public function editAction() {

        $isNew = false;
        $id = 0;

        if (intval($this->_request->getParam('config')) == 5) {

            $translate = Zend_Registry::get('translate');

            $user = Zend_Auth::getInstance()->getIdentity();
            $user = Doctrine_Query::create()
                ->from('Contact m')
                ->where('id = ?')->fetchOne(array($user['id']));

            $role = null;
            foreach ($user->Roles as $rol) {
                if ($role == null || $rol->level > $role->level) {
                    $role = $rol;
                }
            }
            $config = new Zend_Config_Ini(APPLICATION_PATH . '/modules/admin/config/' . 'account/settings.ini', $role->name);

            $fieldsarray = array();
            foreach ($config->tabs as $tab) {
                $tab = $tab->toArray();
                $fields = array();
                foreach ($tab['fields'] as $key => $field) {
                    $field['fieldLabel'] = $translate->translate($field['fieldLabel']);

                    if (isset($field['items'])) {
                        $tempitems = array();
                        foreach ($field['items'] as $subit) {
                            $subit ['fieldLabel'] = $translate->translate($subit ['fieldLabel']);
                            array_push($tempitems, $subit);
                        }
                        $field['items'] = $tempitems;
                    }

                    array_push($fields, $field);
                }

                array_push($fieldsarray, array('general' => $tab['general'], 'fields' => $fields));
            }

            $configarray = $config->general->toArray();
            $configarray['title'] = $translate->translate($configarray['title']);

            $data = false;

            if (intval($this->_request->getParam('uid')) != 0) {

                $data = Doctrine_Query::create()
                    ->from('Account a')
                    ->where('a.install_id = ?', array($this->shop->Install->id))
                    ->where('a.id = ?')->fetchOne(array(intval($this->_request->getParam('uid'))));
            }
            if (intval($this->_request->getParam('save')) == 1 && intval($this->_request->getParam('uid')) > 0) {

                $params = $this->_request->getParams();

                if (isset($params['articles'])) {
                    $articles = implode(',', array_slice($params['articles'], 0, count($params['articles']) - 1));
                    unset($params['articles']);
                }
                if (isset($params ['shippingtype'])) {
                    $shippingtype = implode(',', array_slice($params ['shippingtype'], 0, count($params ['shippingtype']) - 1));
                    unset($params ['shippingtype']);
                }

                unset($params['config']);
                unset($params['save']);
                unset($params['module']);
                unset($params['controller']);
                unset($params['action']);
                unset($params['sid']);
                unset($params['uid']);
                unset($params['headingsize']);

                if (isset($params['mwert']) && $params['mwert'] == 'on') {
                    $data->mwert = true;
                } else {
                    $data->mwert = false;
                }
                unset($params['mwert']);
                foreach ($params as $key => $value) {

                    if (isset($data[$key]) && false == strpos($value, 'service/index?type=upload&mode=get')) {
                        if ($value == 'true' || $value == 'on') {
                            $data->$key = true;
                        } elseif ($value == 'false' || $value == 'off') {
                            $data->$key = false;
                        } else {
                            $data->$key = $value;
                        }
                    }
                }

                $data->save();

                if (isset($articles)) {
                    Doctrine_Query::create()
                        ->delete()
                        ->from('AccountArticle as a')
                        ->where('a.account_id = ?', array(intval($data->id)))
                        ->execute();

                    if ($articles != '') {
                        foreach (explode(',', $articles) as $key) {
                            $group = new AccountArticle();
                            $group->account_id = $data->id;
                            $group->article_id = $key;
                            $group->save();
                        }
                    }
                }
                if (isset($shippingtype)) {

                    Doctrine_Query::create()->delete()->from('AccountShippingtype as a')->where('a.account_id = ?', array(intval($data->id)))->execute();

                    if ($shippingtype != '') {
                        foreach (explode(',', $shippingtype) as $key) {
                            $group = new AccountShippingtype();
                            $group->account_id = $data->id;
                            $group->shippingtype_id = $key;
                            $group->save();
                        }
                    }
                }
            } elseif (intval($this->_request->getParam('save')) == 1 && intval($this->_request->getParam('uid')) == 0) {

                $params = $this->_request->getParams();

                if (isset($params['articles'])) {
                    $articles = implode(',', array_slice($params['articles'], 0, count($params['articles']) - 1));
                    unset($params['articles']);
                }
                if (isset($params ['shippingtype'])) {
                    $shippingtype = implode(',', array_slice($params ['shippingtype'], 0, count($params ['shippingtype']) - 1));
                    unset($params ['shippingtype']);
                }
                unset($params['config']);
                unset($params['save']);
                unset($params['module']);
                unset($params['controller']);
                unset($params['action']);
                unset($params['headingsize']);

                $shopsid = $params ['sid'];

                unset($params['sid']);
                unset($params['uid']);

                $data = new Account();
                $data->Install = $user->Install;
                if (isset($params['mwert']) && $params['mwert'] == 'on') {
                    $data->mwert = true;
                } else {
                    $data->mwert = false;
                }
                unset($params['mwert']);
                foreach ($params as $key => $value) {

                    if (isset($data[$key]) && false == strpos($value, 'service/index?type=upload&mode=get')) {
                        if ($value == 'true' || $value == 'on') {
                            $data->$key = true;
                        } elseif ($value == 'false' || $value == 'off') {
                            $data->$key = false;
                        } else {
                            $data->$key = $value;
                        }
                    }
                }

                $data->save();

                if (intval($this->_request->getParam('sid')) > 0) {

                    $articleAccount = new ShopAccount();
                    $articleAccount->shop_id = intval($this->_request->getParam('sid'));
                    $articleAccount->account_id = $data->id;
                    $articleAccount->save();
                }

                if (isset($articles)) {
                    Doctrine_Query::create()
                        ->delete()
                        ->from('AccountArticle as a')
                        ->where('a.account_id = ?', array(intval($data->id)))
                        ->execute();

                    if ($articles != '') {
                        foreach (explode(',', $articles) as $key) {
                            $group = new AccountArticle();
                            $group->account_id = $data->id;
                            $group->article_id = $key;
                            $group->save();
                        }
                    }
                }
                if (isset($shippingtype)) {

                    Doctrine_Query::create()->delete()->from('AccountShippingtype as a')->where('a.account_id = ?', array(intval($data->id)))->execute();

                    if ($shippingtype != '') {
                        foreach (explode(',', $shippingtype) as $key) {
                            $group = new AccountShippingtype();
                            $group->account_id = $data->id;
                            $group->shippingtype_id = $key;
                            $group->save();
                        }
                    }
                }
                $isNew = true;
                $id = $data->id;
            }

            if ($data != false) {

                $temp = array();
                foreach ($data->AccountArticle as $row) {
                    array_push($temp, $row->article_id);
                }
                $temps = array();
                foreach ($data->AccountShippingtype as $row) {
                    array_push($temps, $row->shippingtype_id);
                }
                $config = new Zend_Config_Ini(APPLICATION_PATH . '/configs/' . '/systemdefaults.ini', 'account_typ');
                $config = $config->toArray();
                $anrede1 = $data->getAnrede1();
                $anrede2 = $data->getAnrede2();
                $data = $data->toArray();
                $data['articles[]'] = implode(',', $temp);
                $data['shippingtype[]'] = implode(',', $temps);
                $data['typ_title'] = $config[$data['typ']];
                $data ['anrede1_title'] = $anrede1;
                $data ['anrede2_title'] = $anrede2;
                if (intval($data['filiale_id']) != 0) {

                    $account = Doctrine_Query::CREATE()->select()->from('Account a')->where('a.id = ?', array($data['filiale_id']))->fetchOne();
                    if ($account) {
                        $data['filiale'] = $account->company." ".$account->appendix;
                    } else {
                        $data['filiale'] = '';
                    }
                } else {
                    $data['filiale'] = '';
                }
            } else {
                $data = array();
            }

            $config = array(
                "success" => true
            , "isNew" => $isNew
            , "uid" => $id
            , "metaData" => array(
                    "fields" => $fieldsarray
                , "formConfig" => $configarray
                , "data" => $data
                )
            , "data" => $data
            );

            $this->getHelper('Json')->sendJson($config);
        }

        $this->_helper->layout->setLayout('popupjs');
    }

}
