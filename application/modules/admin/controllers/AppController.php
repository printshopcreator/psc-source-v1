<?php
class Admin_AppController extends TP_Admin_Controller_Action
{
    /**
     * IndexAction
     *
     * @todo Dokumentation der IndexAction
     *
     * @return void
     */
    public function init() {
        parent::init();
        $contextSwitch = $this->_helper->contextSwitch();
        $contextSwitch->addActionContext('login', self::CONTEXT_JSON)->setAutoJsonSerialization(true)->initContext();
    }

    public function indexAction() {
        $this->_helper->layout->setLayout('login');
        //$this->view->user = Zend_Auth::getInstance()->getIdentity();
    }

    public function loginAction() {
        $this->view->success = false;
    }

    public function portalAction() {
        $this->_helper->layout->setLayout('popupjs');
    }

    public function overviewAction() {
        $this->_helper->layout->setLayout('simple');
    }

    public function treeAction() {
        $user = Zend_Auth::getInstance();
        $user = $user->getIdentity();
        $user = Doctrine_Query::create()->from('Contact m')->where('id = ?')->fetchOne(array(
            $user['id']));
        $node = null;
        $nodeor = null;
        if ($this->_request->getParam('node') != 'source') {
            $nodeor = str_replace('_', '', strrchr($this->_request->getParam('node'), '_'));
            $node = $this->_request->getParam('node') . '_';
        } else {
            $nodeor = 'root';
            $node = 'root_';
        }
        $nodes = array();
        $role = null;
        foreach ($user->Roles as $rol) {
            if ($role == null || $rol->level > $role->level) {
                $role = $rol;
            }
        }
        $config = new Zend_Config_Ini(APPLICATION_PATH . '/configs/tree.ini', $role->name);

        if ((isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on')) {
            $basepath = 'https://' . $_SERVER["SERVER_NAME"];
        } else {
            $basepath = 'http://' . $_SERVER["SERVER_NAME"];
        }

        foreach ($config->items as $key => $item) {
            if (strstr($nodeor, $item->parent) !== false && strpos($nodeor, 'S') === false && (isset($item->group) && $item->group <= $role->level)) {
                if ($this->checkArray($nodes, $node . $key) == false) {
                    if ($item->leaf == true && (!isset($item->cls) || $item->cls != 'window')) {
                        $nodes[] = array(
                            'text' => $this->translate->translate($item->name),
                            'id' => '' . $node . $key,
                            'href' => $item->href,
                            'icon' => '/images/admin/icons/' . $item->icon,
                            'leaf' => true,
                            'cls' => 'file');
                    } elseif (isset($item->cls) && $item->cls == 'window') {
                        if ($key == 16 || $key == 17) {
                            $nodes[] = array(
                                'text' => $this->translate->translate($item->name),
                                'id' => '' . $node . $key,
                                'href' => $basepath . $item->href,
                                'icon' => '/images/admin/icons/' . $item->icon,
                                'leaf' => true,
                                'cls' => 'window');
                        } else {
                            $nodes[] = array(
                                'text' => $this->translate->translate($item->name),
                                'id' => '' . $node . $key,
                                'href' => $basepath . $item->href,
                                'icon' => '/images/admin/icons/' . $item->icon,
                                'leaf' => true,
                                'cls' => 'window');
                        }
                    }
                    else {
                        $nodes[] = array(
                            'text' => $this->translate->translate($item->name),
                            'id' => '' . $node . $key,
                            'href' => '',
                            'cls' => 'folder');
                    }
                }
            }
        }
        if ($nodeor == '14') {
            if ($role->level >= 30 && $role->level <= 50) {
                foreach ($user->ShopContact as $shopcontact) {
                    $shop = $shopcontact->Shop->toArray(false);
                    if ($this->checkArray($nodes, $node . 'S' . $shop['id']) == false && $shop['market'] == false && $shopcontact->admin == true) {
                        $nodes[] = array(
                            'text' => $shop['name'],
                            'id' => '' . $node . 'S' . $shop['id'],
                            'href' => '',
                            'cls' => 'folder',
                            'icon' => '/images/admin/icons/shop1.png');
                    }
                }
            }
        }
        if ($nodeor == '13') {
            if ($role->level >= 30 && $role->level <= 50) {
                foreach ($user->ShopContact as $shopcontact) {
                    $shop = $shopcontact->Shop->toArray(false);
                    if ($this->checkArray($nodes, $node . 'S' . $shop['id']) == false && $shop['market'] == true && $shopcontact->admin == true) {
                        $nodes[] = array(
                            'text' => $shop['name'],
                            'id' => '' . $node . 'S' . $shop['id'],
                            'href' => '',
                            'cls' => 'folder',
                            'icon' => '/images/admin/icons/shop1.png');
                    }
                }
            }
        }
        $shop = Doctrine_Query::create()->from('Shop m')->where('id = ?')->fetchOne(array(
            str_replace('S', '', $nodeor)));
        foreach ($user->Roles as $role) {
            foreach ($role->RolePrivilege as $roleprev) {
                if ($roleprev->ResourcePrivilege->id == 64 && $user->id == 1) {
                    continue;
                }
                if ($roleprev->ResourcePrivilege->tree != "1" ||
                    $roleprev->ResourcePrivilege->treename == "admin_tree_cliparts" ||
                    $roleprev->ResourcePrivilege->treename == "admin_tree_booking" ||
                    $roleprev->ResourcePrivilege->treename == "admin_tree_newsletter" ||
                    $roleprev->ResourcePrivilege->treename == "admin_tree_paymentmethods" ||
                    $roleprev->ResourcePrivilege->treename == "admin_tree_motiv" ||
                    $roleprev->ResourcePrivilege->treename == "News" ||
                    $roleprev->ResourcePrivilege->treename == "admin_tree_orders-customers" ||
                    $roleprev->ResourcePrivilege->treename == "admin_tree_paperdb" ||
                    $roleprev->ResourcePrivilege->treename == "admin_tree_cms" ||
                   // $roleprev->ResourcePrivilege->treename == "admin_tree_productgroups" ||
                    $roleprev->ResourcePrivilege->treename == "admin_tree_creditsystem" ||
                    $roleprev->ResourcePrivilege->treename == "admin_tree_preflight" ||
                    $roleprev->ResourcePrivilege->treename == "admin_tree_shippingmethods"
                ) {
                    continue;
                }
                if (in_array(str_replace('S', '', $nodeor), $roleprev->ResourcePrivilege->ResourcePrivilegeShop->toKeyValueArray('shop_id', 'shop_id')) && strpos($nodeor, 'S') !== false && ($shop == false || ($roleprev->ResourcePrivilege->market == false && $shop->market == false) || $roleprev->ResourcePrivilege->market == true)) {
                    if ($this->checkArray($nodes, $node . $roleprev->ResourcePrivilege->treename) == false) {
                        if ($roleprev->ResourcePrivilege->treename == 'admin_tree_displayshop' || $roleprev->ResourcePrivilege->treename == 'admin_tree_motiv') {
                            if ($roleprev->ResourcePrivilege->treename == 'admin_tree_motiv') {
                                $nodes[] = array(
                                    'text' => $this->translate->translate($roleprev->ResourcePrivilege->treename),
                                    'id' => '' . $node . $roleprev->ResourcePrivilege->treename,
                                    'href' => 'http://' .$shop->Domain[0]->name . '/apps/backend/motiv/list/index',
                                    'icon' => '/images/admin/icons/' . $roleprev->ResourcePrivilege->treeicon,
                                    'leaf' => true,
                                    'cls' => 'window');
                            } else {
                                $nodes[] = array(
                                    'text' => $this->translate->translate($roleprev->ResourcePrivilege->treename),
                                    'id' => '' . $node . $roleprev->ResourcePrivilege->treename,
                                    'href' => 'http://' .$shop->Domain[0]->name,
                                    'icon' => '/images/admin/icons/' . $roleprev->ResourcePrivilege->treeicon,
                                    'leaf' => true,
                                    'cls' => 'window');
                            }
                        } else {
                            $nodes[] = array(
                                'text' => $this->translate->translate($roleprev->ResourcePrivilege->treename),
                                'id' => '' . $node . $roleprev->ResourcePrivilege->treename,
                                'href' => '/' . $roleprev->ResourcePrivilege->Resource->modul . '/' . $roleprev->ResourcePrivilege->Resource->name . '/' . $roleprev->ResourcePrivilege->Privilege->name . '?id=' . $nodeor,
                                'icon' => '/images/admin/icons/' . $roleprev->ResourcePrivilege->treeicon,
                                'leaf' => true,
                                'cls' => 'file');
                        }
                    }
                }
                if (strstr($roleprev->ResourcePrivilege->treeparent, ',') === false) {
                    if ($nodeor == $roleprev->ResourcePrivilege->treeparent) {
                        if ($this->checkArray($nodes, $node . $roleprev->ResourcePrivilege->treename) == false) {
                            if ($roleprev->ResourcePrivilege->treename == 'admin_tree_displayshop' || $roleprev->ResourcePrivilege->treename == 'admin_tree_motiv') {
                                $shop = Doctrine_Query::create()->from('Shop m')->where('id = ?')->fetchOne(array(
                                    str_replace('S', '', $nodeor)));
                                $nodes[] = array(
                                    'text' => $this->translate->translate($roleprev->ResourcePrivilege->treename),
                                    'id' => '' . $node . $roleprev->ResourcePrivilege->treename,
                                    'href' => '/' . $shop->Domain->name . '/' . $roleprev->ResourcePrivilege->Resource->name . '/' . $roleprev->ResourcePrivilege->Privilege->name . '?id=' . $nodeor,
                                    'icon' => '/images/admin/icons/' . $roleprev->ResourcePrivilege->treeicon,
                                    'leaf' => true,
                                    'cls' => 'window');
                            } else {
                                $nodes[] = array(
                                    'text' => $this->translate->translate($roleprev->ResourcePrivilege->treename),
                                    'id' => '' . $node . $roleprev->ResourcePrivilege->treename,
                                    'href' => '/' . $roleprev->ResourcePrivilege->Resource->modul . '/' . $roleprev->ResourcePrivilege->Resource->name . '/' . $roleprev->ResourcePrivilege->Privilege->name . '?id=' . $nodeor,
                                    'icon' => '/images/admin/icons/' . $roleprev->ResourcePrivilege->treeicon,
                                    'leaf' => true,
                                    'cls' => 'file');
                            }
                        }
                    }
                } else {
                    foreach (explode(',', $roleprev->ResourcePrivilege->treeparent) as $ids) {
                        if ($nodeor == $ids && ($shop == false || ($roleprev->ResourcePrivilege->market == false && $shop->market == false) || $roleprev->ResourcePrivilege->market == true)) {
                            if ($this->checkArray($nodes, $node . $roleprev->ResourcePrivilege->treename) == false) {
                                if ($roleprev->ResourcePrivilege->treename == 'admin_tree_displayshop') {
                                    $nodes[] = array(
                                        'text' => $this->translate->translate($roleprev->ResourcePrivilege->treename),
                                        'id' => '' . $node . $roleprev->ResourcePrivilege->treename,
                                        'href' => '/' . $shop->Domain[0]->name,
                                        'icon' => '/images/admin/icons/' . $roleprev->ResourcePrivilege->treeicon,
                                        'leaf' => true,
                                        'cls' => 'window');
                                } else {
                                    $nodes[] = array(
                                        'text' => $this->translate->translate($roleprev->ResourcePrivilege->treename),
                                        'id' => '' . $node . $roleprev->ResourcePrivilege->treename,
                                        'href' => '/' . $roleprev->ResourcePrivilege->Resource->modul . '/' . $roleprev->ResourcePrivilege->Resource->name . '/' . $roleprev->ResourcePrivilege->Privilege->name . '?id=' . $nodeor,
                                        'icon' => '/images/admin/icons/' . $roleprev->ResourcePrivilege->treeicon,
                                        'leaf' => true,
                                        'cls' => 'file');
                                }
                            }
                        }
                    }
                }
            }
        }
        $this->getHelper('Json')->sendJson($nodes);
    }

    protected function checkArray($array, $search) {
        foreach ($array as $arr) {
            if ($arr['id'] == $search) {
                return true;
            }
        }
        return false;
    }
}
