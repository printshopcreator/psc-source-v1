<?php

class Admin_ArticleController extends TP_Admin_Controller_Action
{

    protected function readfile_chunked($filename) {
        $chunksize = 1 * (1024 * 1024); // how many bytes per chunk
        $buffer = '';
        $handle = fopen($filename, 'rb');
        if ($handle === false) {
            return false;
        }
        while (!feof($handle)) {
            $buffer = fread($handle, $chunksize);
            print $buffer;
            ob_flush();
            flush();
        }
        return fclose($handle);
        die();
    }

    public function langselectAction() {

        $this->view->clearVars();
        $temp = array();
        $temp[] = array('name' => 'en_US', 'id' => 'en');
        $temp[] = array('name' => 'fr_FR', 'id' => 'fr');

        $this->view->results = $temp;
    }

    public function langaddAction() {

        $article = Doctrine_Query::create()
            ->from('Article c')
            ->where('c.id = ?', array($this->_getParam('article')))
            ->fetchOne();

        $language = $article->getLangData();

        if (!isset($language[$this->_getParam('value')])) {
            $language[$this->_getParam('value')] = array('uuid' => TP_Util::uuid());
        }
        $article->setLangData($language);
        $article->save();
        $this->view->clearVars();
        $this->view->success = true;
    }

    public function langfetchAction() {

        $article = Doctrine_Query::create()
            ->from('Article c')
            ->where('c.id = ?', array($this->_getParam('uid')))
            ->fetchOne();

        $language = $article->getLangData();
        $settings = json_decode($this->_getParam('results'), true);

        if ($this->_request->getParam('xaction', false) == "update") {

            $language[$settings['lang']] = array('uuid' => $settings['uuid'], 'title' => $settings['title'], 'text_art' => $settings['text_art'], 'info' => $settings['info'], 'einleitung' => $settings['einleitung']);
            $article->setLangData($language);
            $article->save();

            die($this->getHelper('Json')->sendJson(array('success' => true, 'newID' => $article->id)));

        }

        $temp = array();
        foreach ($language as $key => $row) {
            $temp[] = array(
                'lang' => $key,
                'title' => @$row['title'],
                'einleitung' => @$row['einleitung'],
                'text_art' => @$row['text_art'],
                'info' => @$row['info'],
                'uuid' => @$row['uuid']
            );
        }

        $this->view->clearVars();
        $this->view->results = $temp;
    }

    /**
     * IndexAction
     *
     * @todo Dokumentation der IndexAction
     *
     * @return void
     */
    public function init() {
        parent::init();
        $contextSwitch = $this->_helper->contextSwitch();
        $contextSwitch->addActionContext('fetcharticletemplates', self::CONTEXT_JSON)
            ->addActionContext('clearlayouter', self::CONTEXT_JSON)
            ->addActionContext('cleartemplateprint', self::CONTEXT_JSON)
            ->addActionContext('liveedit', self::CONTEXT_JSON)
            ->addActionContext('downloadtp', self::CONTEXT_JSON)
            ->addActionContext('uploadtp', self::CONTEXT_JSON)
            ->addActionContext('langselect', self::CONTEXT_JSON)
            ->addActionContext('langadd', self::CONTEXT_JSON)
            ->addActionContext('langfetch', self::CONTEXT_JSON)
            ->setAutoJsonSerialization(true)->initContext();
    }

    public function downloadtpAction() {
        $this->getHelper('layout')->disableLayout();
        $this->_helper->viewRenderer->setNoRender();

        $article = Doctrine_Query::create()
            ->from('Article c')
            ->where('c.uuid = ?', array($this->_getParam('uuid')))
            ->fetchOne();

        $path = str_split($article->id);

        header("Content-Transfer-Encoding: binary");
        header("Content-type: application/zip");
        $len = filesize(APPLICATION_PATH . '/../market/templateprint/orginal/' . implode('/', $path) . '/product.zip');
        header("Content-Length: $len");
        header('Content-Disposition: attachment; filename="product.zip"');
        if (file_exists(APPLICATION_PATH . '/../market/templateprint/orginal/' . implode('/', $path) . '/product.zip')) {
            print file_get_contents(APPLICATION_PATH . '/../market/templateprint/orginal/' . implode('/', $path) . '/product.zip');
        }
    }

    public function uploadtpAction() {
        $this->getHelper('layout')->disableLayout();
        $this->_helper->viewRenderer->setNoRender();

        $article = Doctrine_Query::create()
            ->from('Article c')
            ->where('c.id = ? AND shop_id IN (' . implode(',', $this->shopIds) . ')', array($this->_getParam('uuid_upload')))
            ->fetchOne();

        $path = str_split($article->id);

        if (!file_exists(APPLICATION_PATH . '/../market/templateprint/orginal/' . implode('/', $path) . '')) {
            $an = '';
            foreach ($path as $pt) {
                $an .= $pt . '/';
                if (!file_exists(APPLICATION_PATH . '/../market/templateprint/orginal/' . $an)) {
                    mkdir(APPLICATION_PATH . '/../market/templateprint/orginal/' . $an, 0777, true);
                }

            }
        }

        $adapter = new Zend_File_Transfer_Adapter_Http();
        Zend_Registry::get('log')->debug(APPLICATION_PATH . '/../market/templateprint/orginal/' . implode('/', $path));
        $adapter->setDestination(APPLICATION_PATH . '/../market/templateprint/orginal/' . implode('/', $path));
        $adapter->addFilter('Rename', 'product.zip');
        if (!$adapter->receive()) {
            $messages = $adapter->getMessages();
        }

        Zend_Registry::get('log')->debug(print_r($messages, true));
        die($messages);
    }

    public function exportAction() {
        $this->getHelper('layout')->disableLayout();
        $this->_helper->viewRenderer->setNoRender();

        require_once('PHPExcel/PHPExcel.php');

        if ($this->install->export_template_product == "") {
            echo "Kein Exporttemplate hochgeladen";
            return;
        }

        $file = Doctrine_Query::create()->select()
            ->from('Image i')->where('i.id = ?', array($this->install->export_template_product))->fetchOne();

        $objPHPExcel = PHPExcel_IOFactory::load($file->path);
        $columns = 30;
        $mapping = array();
        $cells = array('A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', 'AA', 'AB', 'AC', 'AD');
        for ($i = 0; $i < $columns; $i++) {
            if ($objPHPExcel->getActiveSheet()->getCell($cells[$i] . '2')->getValue() == "") {
                break;
            }
            $mapping[] = $objPHPExcel->getActiveSheet()->getCell($cells[$i] . '2')->getValue();
        }

        if($this->_getParam("sid", false) ) {
            $rows = Doctrine_Query::create()
                ->from('Article m')
                ->addWhere('shop_id IN (' . implode(',', $this->shopIds) . ') AND shop_id = ' . $this->_getParam("sid", false))->execute();
        }else{
            $rows = Doctrine_Query::create()
                ->from('Article m')
                ->addWhere('shop_id IN (' . implode(',', $this->shopIds) . ')')->execute();
        }


        $i = 2;
        foreach ($rows as $row) {
            reset($mapping);
            $ii = 0;
            foreach ($mapping as $key => $value) {
                $objPHPExcel->getActiveSheet()->setCellValue($cells[$key] . $i, $row->$value);
                $ii++;
            }

            $i++;
        }

        // Redirect output to a client’s web browser (Excel5)
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="export.xls"');
        header('Cache-Control: max-age=0');

        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        $objWriter->save('php://output');
    }

    public function liveeditAction() {

        $cms = Doctrine_Query::create()
            ->from('Article m')
            ->addWhere('shop_id IN (' . implode(',', $this->shopIds) . ') AND id = ?', array($this->_getParam('id')))->fetchOne();

        if (!$cms) {
            $this->view->success = false;
            return;
        }

        $field = $this->_getParam('field');
        if ($cms->getTable()->hasField($field)) {
            $cms->$field = $this->_getParam('content');
            $cms->save();

            $this->view->success = true;
            return;
        }

        $this->view->success = false;
        return;
    }

    public function pdfAction() {

        $this->getHelper('layout')->disableLayout();
        $this->_helper->viewRenderer->setNoRender();

        if ($this->_getParam('uuid', false)) {
            $row = Doctrine_Query::create()
                ->from('Article a')
                ->where('uuid = ? AND shop_id IN (' . implode(',', $this->shopIds) . ')', array($this->_request->getParam('uuid')))
                ->fetchOne();

            if (!$row) {
                return;
            }

            $file = TP_FOP::generatePdfPreview($row);

            header('Content-type: application/pdf');

            // Es wird downloaded.pdf benannt
            header('Content-Disposition: attachment; filename="preview.pdf"');

            // Die originale PDF Datei heißt original.pdf
            readfile($file);
        }
    }

    public function allAction() {

        if ($this->_request->getParam('config') == 1) {

            $rows = Doctrine_Query::create()
                ->from('Article m')
                ->limit(intval($this->_request->getParam('limit')))
                ->offset(intval($this->_request->getParam('start')))
                ->addWhere('AND shop_id IN (' . implode(',', $this->shopIds) . ')');
            if (intval(str_replace('S', '', $this->_request->getParam('sid'))) != 0) {
                $rows->where('shop_id = ?', array(intval(str_replace('S', '', $this->_request->getParam('sid')))));
            }
            if ($this->_request->getParam('sort')) {
                if ($this->_request->getParam('sort') == "typName") {
                    $rows->orderBy('typ ' . $this->_request->getParam('dir'));
                } else {
                    $rows->orderBy($this->_request->getParam('sort') . ' ' . $this->_request->getParam('dir'));
                }
            }

            TP_Filter::proccess($rows);
            $rows = $rows->execute();

            $count = Doctrine_Query::create()
                ->from('Article m')
                ->addWhere('AND shop_id IN (' . implode(',', $this->shopIds) . ')');
            if (intval(str_replace('S', '', $this->_request->getParam('sid'))) != 0) {
                $count->where('shop_id = ?', array(intval(str_replace('S', '', $this->_request->getParam('sid')))));
            }

            TP_Filter::proccess($count);

            $count = $count->execute();

            $list = array();
            $articles = Zend_Registry::get('articles');
            foreach ($rows as $row) {
                if (isset($articles[$row->typ])) {
                    $typ = new $articles[$row->typ]();
                    $rowOrg = $row;
                    $row = $row->toArray(false);
                    $row['typName'] = $typ->name;
                    if ($row['typ'] == 6 && $row['a6_org_article'] != 0) {
                        $row['typName'] = 'Sub Product';
                    } else {
                        $row['typName'] = $typ->name;
                    }
                    $row['link'] = 'http://' . $rowOrg->Shop->Domain[0]->name . '/article/show/uuid/' . $row['uuid'];

                    $articlegroups = array();
                    foreach ($rowOrg->ArticleGroup as $group) {
                        $articlegroups[] = $group->title;
                    }
                    $row['articlegroups'] = implode(', ', $articlegroups);
                    array_push($list, $row);
                }
            }
            $config = array(
                "success" => true
            , "totalCount" => $count->count()
            , "rows" => $list
            );

            $this->getHelper('Json')->sendJson($config);
        }

        if ($this->_request->getParam('config') == 2) {

            $rows = array();

            $articles = Zend_Registry::get('articles');

            foreach ($articles as $key => $article) {
                $article = new $article();
                if ($key != 7 && $key != 9) {
                    if ($key == 6) {
                        array_push($rows, array('title' => $article->name, 'id' => $key));
                        array_push($rows, array('title' => 'Sub Product', 'id' => '6b'));
                    } else {
                        array_push($rows, array('title' => $article->name, 'id' => $key));
                    }
                }
            }
            $config = array(
                "count" => count($rows)
            , "rows" => $rows
            );

            $this->getHelper('Json')->sendJson($config);

            unset($articles);
            unset($article);
        }

        $this->view->sid = intval(str_replace('S', '', $this->_request->getParam('id')));

        $row = Doctrine_Query::create()
            ->from('Shop a')
            ->where('id = ? AND id IN (' . implode(',', $this->shopIds) . ')', array($this->view->sid))
            ->fetchOne();

        $this->view->market = false;

        if ($row->market) {
            $this->view->market = true;
        }

        $this->_helper->layout->setLayout('simple');
    }

    public function editAction() {

        $isNew = false;
        $id = 0;

        if (intval($this->_request->getParam('delete')) == 1) {

            $error = false;
            $message = "";
            $row = Doctrine_Query::create()
                ->from('Article a')
                ->where('uuid = ? AND shop_id IN (' . implode(',', $this->shopIds) . ')', array($this->_request->getParam('uuid')))
                ->fetchOne();

            $results = Doctrine_Query::create()
                ->from('Article a')
                ->where('a6_org_article = ?', array($row->id))
                ->execute();

            if (count($results) > 0) {
                $error = true;
                $message = 'Ist ein Quellprodukt von:<br/>';
                foreach ($results as $result) {
                    $message = $message . $result->title . '<br/>';
                }
            }

            $results = Doctrine_Query::create()
                ->from('Orderspos a')
                ->where('a.article_id = ?', array($row->id))
                ->execute();

            if (count($results) > 0) {
                $error = true;
                $message .= 'Gehört zu einem Auftrag:<br/>';
                foreach ($results as $result) {
                    $message = $message . $result->Orders->alias . '<br/>';
                }
            }

            if ($error == true) {
                die($this->getHelper('Json')->sendJson(array('error' => true, 'message' => $message)));
            }

            $articles = Zend_Registry::get('articles');

            $article = new $articles[$row->typ]();

            if (method_exists($article, 'deletePreDispatch')) {
                $article->deletePreDispatch($row);
            }

            $obj = TP_Mongo::getInstance();
            $collection = $obj->articles->remove(array('_id' => $row->uuid));
            ;

            $row->delete();

            die($this->getHelper('Json')->sendJson(array('success' => true)));
        }

        if (intval($this->_request->getParam('copy')) == 1) {

            $row = Doctrine_Query::create()
                ->from('Article a')
                ->where('a.uuid = ? AND a.shop_id IN (' . implode(',', $this->shopIds) . ')', array($this->_request->getParam('uuid')))
                ->fetchOne();

            $row = $row->copy();
            $row->uuid = "";
            $row->used = 0;
            $row->template_admin = 0;
            $row->template_system_operator = 0;
            $row->contact_id = $this->user->id;
            $row->save();

            $articles = Zend_Registry::get('articles');

            $article = new $articles[$row->typ]();

            if (method_exists($article, 'copyPreDispatch')) {
                $article->copyPreDispatch($row);
            }

            die($this->getHelper('Json')->sendJson(array('success' => true)));
        }

        if (intval($this->_request->getParam('copymove')) == 1) {

            $orgrow = Doctrine_Query::create()
                ->from('Article a')
                ->where('a.uuid = ?', array($this->_request->getParam('uuid')))
                ->fetchOne();

            $row = $orgrow->copy();
            $row->uuid = "";
            $row->used = 0;
            $row->template_admin = 0;
            $row->template_system_operator = 0;
            $row->contact_id = $this->user->id;

            if (in_array(intval($this->_request->getParam('target')), $this->shopIds)) {
                $row->shop_id = intval($this->_request->getParam('target'));

                $shop = Doctrine_Query::create()
                    ->from('Shop a')
                    ->where('a.id = ?', array(intval($this->_request->getParam('target'))))
                    ->fetchOne();

                $row->install_id = $shop->install_id;
                $row->save();

                $articles = Zend_Registry::get('articles');

                $article = new $articles[$row->typ]();

                if (method_exists($article, 'copyPreDispatch')) {
                    $article->copyPreDispatch($row);
                }
                if (method_exists($article, 'copyAdminPreDispatch')) {
                    $article->copyAdminPreDispatch($orgrow, $row);
                }
            }

            die($this->getHelper('Json')->sendJson(array('success' => true)));
        }

        if (intval($this->_request->getParam('config')) == 3) {

            $data = false;

            $articles = Zend_Registry::get('articles');

            $article = new $articles[$this->_request->getParam('type')]();

            $translate = Zend_Registry::get('translate');
            $translation = APPLICATION_PATH . '/articles/' . $article->path . '/locale';

            $translate->addTranslation($translation, Zend_Registry::get('locale'));

            $user = Zend_Auth::getInstance()->getIdentity();
            $user = Doctrine_Query::create()
                ->from('Contact m')
                ->where('id = ?')->fetchOne(array($user['id']));
            $role = null;
            foreach ($user->Roles as $rol) {
                if ($role == null || $rol->level > $role->level) {
                    $role = $rol;
                }
            }

            $config = new Zend_Config_Ini(APPLICATION_PATH . '/articles/' . $article->backend, $role->name);
            $configmain = new Zend_Config_Ini(APPLICATION_PATH . '/modules/admin/config/' . 'article/backend.ini', $role->name);

            if (intval($this->_request->getParam('uid')) != 0) {

                $data = Doctrine_Query::create()
                    ->from('Article a')
                    ->where('a.id = ?')->fetchOne(array(intval($this->_request->getParam('uid'))));

                if ($data->a6_org_article != 0 && $data->typ != 8) {
                    $config = new Zend_Config_Ini(APPLICATION_PATH . '/articles/' . $article->backend_market, $role->name);
                }
            }

            $buttons = array();

            $fieldsarray = array();
            $fieldstemp = array();
            foreach ($configmain->tabs as $keyt => $tab) {

                $tab = $tab->toArray();
                $fields = array();
                foreach ($tab['fields'] as $key => $field) {

                    if (isset($field['fieldLabel'])) {
                        $field['fieldLabel'] = $translate->translate($field['fieldLabel']);
                    }
                    if (isset($field['items'])) {
                        $tempitems = array();
                        foreach ($field['items'] as $subit) {

                            $subit ['fieldLabel'] = $translate->translate($subit ['fieldLabel']);

                            array_push($tempitems, $subit);
                        }

                        $field['items'] = $tempitems;
                    }

                    array_push($fields, $field);

                }

                if ($config->tabs->$keyt) {
                    foreach ($config->tabs->$keyt->fields->toArray() as $key => $field) {

                        if (isset($field['fieldLabel'])) {
                            $field['fieldLabel'] = $translate->translate($field['fieldLabel']);
                        }

                        if (isset($field['items'])) {
                            $tempitems = array();
                            foreach ($field['items'] as $subit) {
                                $subit ['fieldLabel'] = $translate->translate($subit ['fieldLabel']);
                                array_push($tempitems, $subit);
                            }
                            $field['items'] = $tempitems;
                        }

                        array_push($fields, $field);
                    }
                }

                $tab['general']['title'] = $translate->translate($tab['general']['title']);
                $fieldstemp[$keyt] = '1';
                $fieldsarray[] = array('general' => $tab['general'], 'fields' => $fields);
            }
            if ($config->tabs) {
                foreach ($config->tabs as $keyt => $tab) {
                    $tab = $tab->toArray();
                    $fields = array();
                    foreach ($tab['fields'] as $key => $field) {
                        $field['fieldLabel'] = $translate->translate($field['fieldLabel']);

                        if (isset($field['items'])) {
                            $tempitems = array();
                            foreach ($field['items'] as $subit) {
                                if (isset($subit ['fieldLabel'])) {
                                    $subit ['fieldLabel'] = $translate->translate($subit ['fieldLabel']);
                                }
                                array_push($tempitems, $subit);
                            }
                            $field['items'] = $tempitems;
                        }

                        if (!isset($fieldstemp[$keyt])) {
                            array_push($fields, $field);
                        }
                    }
                    if (!isset($fieldstemp[$keyt])) {
                        $tab['general']['title'] = $translate->translate($tab['general']['title']);
                        $fieldsarray[] = array('general' => $tab['general'], 'fields' => $fields);
                    }
                }
            }
            if ($configmain->buttons) {
                foreach ($configmain->buttons as $button) {
                    $button = $button->toArray();
                    $button['title'] = $translate->translate($button['title']);
                    if ($data != false || (isset($button['require']) && (bool)$button['require'] == false)) {
                        eval('$button["href"] = ' . $button["href"] . ';');
                        array_push($buttons, $button);
                    }
                }
            }
            if ($config->buttons) {
                foreach ($config->buttons as $button) {
                    $button = $button->toArray();
                    $button['title'] = $translate->translate($button['title']);
                    if ($data != false || (isset($button['require']) && (bool)$button['require'] == false)) {
                        eval('$button["href"] = ' . $button["href"] . ';');
                        array_push($buttons, $button);
                    }
                }
            }

            $configarray = $configmain->general->toArray();
            $configarray['title'] = $translate->translate($configarray['title']);

            if (intval($this->_request->getParam('save')) == 1 && intval($this->_request->getParam('uid')) > 0) {

                $params = $this->_request->getParams();

                unset($params['config']);
                unset($params['save']);
                unset($params['module']);
                unset($params['controller']);
                unset($params['action']);
                unset($params['sid']);
                unset($params['htmlsnippet']);
                unset($params['linkextern']);
                unset($params['headingsize']);

                if (isset($params['tags'])) {
                    $tags = $params['tags'];
                }
                unset($params['tags']);

                if (isset($params['articlegroups'])) {
                    $groups = implode(',', array_slice($params['articlegroups'], 0, count($params['articlegroups']) - 1));
                }
                unset($params['articlegroups']);

                if (isset($params['releatedarticle'])) {
                    $releatedarticle = implode(',', array_slice($params['releatedarticle'], 0, count($params['releatedarticle']) - 1));
                }
                unset($params['releatedarticle']);

                if (isset($params['releatedthemes'])) {
                    $releatedthemes = implode(',', array_slice($params['releatedthemes'], 0, count($params['releatedthemes']) - 1));
                }
                unset($params['releatedthemes']);

                if (isset($params['confirmcontact'])) {
                    $confirmcontact = implode(',', array_slice($params['confirmcontact'], 0, count($params['confirmcontact']) - 1));
                }
                unset($params['confirmcontact']);

                if (isset($params['articlegroupsmarket'])) {
                    $groupsmarket = implode(',', array_slice($params['articlegroupsmarket'], 0, count($params['articlegroupsmarket']) - 1));
                }
                unset($params['articlegroupsmarket']);

                if (isset($params['releatedthememarket'])) {
                    $releatedthemesmarket = implode(',', array_slice($params['releatedthememarket'], 0, count($params['releatedthememarket']) - 1));
                }
                unset($params['releatedthememarket']);
                $filter = new TP_Filter_Badwords();
                foreach ($params as $key => $value) {

                    if (isset($data[$key]) && false == strpos($value, 'service/index?type=upload&mode=get')) {
                        if ($value == 'true' || $value == 'on') {
                            $data->$key = true;
                        } elseif ($value == 'false' || $value == 'off') {
                            $data->$key = false;
                        } else {
                            $data->$key = $filter->filter($value);
                        }
                    }
                }

                $tag = array();
                if (isset($tags) && trim($tags) != "") {
                    $tagsTemp = array();
                    foreach (explode(',', $tags) as $key) {
                        $tag = strtolower(trim($filter->filter($key)));
                        if ($tag) {
                            $tagsTemp[] = array('name' => $tag, 'url' => Doctrine_Inflector::urlize($tag));
                        }
                    }

                    $obj = TP_Mongo::getInstance();
                    $collection = $obj->articles;
                    $collection->save(array_merge($data->getRiakData(), array('tags' => $tagsTemp, '_id' => $data->uuid)));
                } else {

                }

                $data->save();

                if (isset($groups)) {
                    Doctrine_Query::create()
                        ->delete()
                        ->from('ArticleGroupArticle as a')
                        ->where('a.article_id = ?', array(intval($data->id)))
                        ->execute();

                    if ($groups != '') {
                        foreach (explode(',', $groups) as $key) {
                            $group = new ArticleGroupArticle();
                            $group->articlegroup_id = $key;
                            $group->article_id = $data->id;
                            $group->save();
                        }
                    }
                }

                if (isset($groupsmarket)) {
                    Doctrine_Query::create()
                        ->delete()
                        ->from('ArticleGroupMarketArticle as a')
                        ->where('a.article_id = ?', array(intval($data->id)))
                        ->execute();

                    if ($groupsmarket != '') {
                        foreach (explode(',', $groupsmarket) as $key) {
                            $group = new ArticleGroupMarketArticle();
                            $group->articlegroup_id = $key;
                            $group->article_id = $data->id;
                            $group->save();
                        }
                    }
                }

                if (isset($releatedthemes)) {
                    Doctrine_Query::create()
                        ->delete()
                        ->from('ArticleThemeArticle as a')
                        ->where('a.article_id = ?', array(intval($data->id)))
                        ->execute();

                    if ($releatedthemes != '') {
                        foreach (explode(',', $releatedthemes) as $key) {
                            $group = new ArticleThemeArticle();
                            $group->theme_id = $key;
                            $group->article_id = $data->id;
                            $group->save();
                        }
                    }
                }

                if (isset($confirmcontact)) {
                    Doctrine_Query::create()
                        ->delete()
                        ->from('ArticleConfirmContact as a')
                        ->where('a.article_id = ?', array(intval($data->id)))
                        ->execute();

                    if ($confirmcontact != '') {
                        foreach (explode(',', $confirmcontact) as $key) {
                            $group = new ArticleConfirmContact();
                            $group->contact_id = $key;
                            $group->article_id = $data->id;
                            $group->save();
                        }
                    }
                }

                if (isset($releatedthemesmarket)) {
                    Doctrine_Query::create()
                        ->delete()
                        ->from('ArticleThemeMarketArticle as a')
                        ->where('a.article_id = ?', array(intval($data->id)))
                        ->execute();

                    if ($releatedthemesmarket != '') {
                        foreach (explode(',', $releatedthemesmarket) as $key) {
                            $group = new ArticleThemeMarketArticle();
                            $group->theme_id = $key;
                            $group->article_id = $data->id;
                            $group->save();
                        }
                    }
                }

                if (isset($releatedarticle)) {
                    Doctrine_Query::create()
                        ->delete()
                        ->from('ArticleReleated as a')
                        ->where('a.article1 = ?', array(intval($data->id)))
                        ->execute();

                    if ($releatedarticle != '') {
                        foreach (explode(',', $releatedarticle) as $key) {
                            $group = new ArticleReleated();
                            $group->article2 = $key;
                            $group->article1 = $data->id;
                            $group->save();
                        }
                    }
                }


            } elseif (intval($this->_request->getParam('save')) == 1 && intval($this->_request->getParam('uid')) == 0) {

                $params = $this->_request->getParams();

                unset($params['config']);
                unset($params['save']);
                unset($params['module']);
                unset($params['controller']);
                unset($params['action']);
                unset($params['sid']);
                unset($params['htmlsnippet']);
                unset($params['linkextern']);
                unset($params['headingsize']);

                $data = new Article();
                $data->shop_id = intval($this->_request->getParam('sid'));
                $data->Install = $user->Install;
                $data->contact_id = $user->id;
                $data->typ = $article->id;
                $data->a6_isroot = true;

                if (isset($params['tags'])) {
                    $tags = $params['tags'];
                }
                unset($params['tags']);

                if (isset($params['articlegroups'])) {
                    $groups = implode(',', array_slice($params['articlegroups'], 0, count($params['articlegroups']) - 1));
                }
                unset($params['articlegroups']);

                if (isset($params['releatedarticle'])) {
                    $releatedarticle = implode(',', array_slice($params['releatedarticle'], 0, count($params['releatedarticle']) - 1));
                }
                unset($params['releatedarticle']);
                if (isset($params['releatedthemes'])) {
                    $releatedthemes = implode(',', array_slice($params['releatedthemes'], 0, count($params['releatedthemes']) - 1));
                }
                unset($params['releatedthemes']);
                if (isset($params['confirmcontact'])) {
                    $confirmcontact = implode(',', array_slice($params['confirmcontact'], 0, count($params['confirmcontact']) - 1));
                }
                unset($params['confirmcontact']);
                if (isset($params['articlegroupsmarket'])) {
                    $groupsmarket = implode(',', array_slice($params['articlegroupsmarket'], 0, count($params['articlegroupsmarket']) - 1));
                }
                unset($params['articlegroupsmarket']);
                if (isset($params['releatedthememarket'])) {
                    $releatedthemesmarket = implode(',', array_slice($params['releatedthememarket'], 0, count($params['releatedthememarket']) - 1));
                }
                unset($params['releatedthememarket']);
                $filter = new TP_Filter_Badwords();
                foreach ($params as $key => $value) {

                    if (isset($data[$key]) && false == strpos($value, 'service/index?type=upload&mode=get')) {
                        if ($value == 'true' || $value == 'on') {
                            $data->$key = true;
                        } elseif ($value == 'false' || $value == 'off') {
                            $data->$key = false;
                        } else {
                            $data->$key = $filter->filter($value);
                        }
                    }
                }
                if ($data->typ == 6) {
                    $data->display_market = true;
                }

                if (isset($tags) && trim($tags) != "") {
                    $tagsTemp = array();
                    foreach (explode(',', $tags) as $key) {
                        $tag = strtolower(trim($filter->filter($key)));
                        if ($tag) {
                            $tagsTemp[] = array('name' => $tag, 'url' => Doctrine_Inflector::urlize($tag));
                        }
                    }

                    $obj = TP_Mongo::getInstance();
                    $collection = $obj->articles;
                    $collection->save(array_merge($data->getRiakData(), array('tags' => $tagsTemp, '_id' => $data->uuid)));
                }

                $data->save();

                if (method_exists($article, 'createPreDispatch')) {
                    $article->createPreDispatch($data);
                }

                if (isset($groups)) {
                    Doctrine_Query::create()
                        ->delete()
                        ->from('ArticleGroupArticle as a')
                        ->where('a.article_id = ?', array(intval($data->id)))
                        ->execute();

                    if ($groups != '') {
                        foreach (explode(',', $groups) as $key) {
                            $group = new ArticleGroupArticle();
                            $group->articlegroup_id = $key;
                            $group->article_id = $data->id;
                            $group->save();
                        }
                    }
                }

                if (isset($groupsmarket)) {
                    Doctrine_Query::create()
                        ->delete()
                        ->from('ArticleGroupMarketArticle as a')
                        ->where('a.article_id = ?', array(intval($data->id)))
                        ->execute();

                    if ($groupsmarket != '') {
                        foreach (explode(',', $groupsmarket) as $key) {
                            $group = new ArticleGroupMarketArticle();
                            $group->articlegroup_id = $key;
                            $group->article_id = $data->id;
                            $group->save();
                        }
                    }
                }

                if (isset($releatedthemes)) {
                    Doctrine_Query::create()
                        ->delete()
                        ->from('ArticleThemeArticle as a')
                        ->where('a.article_id = ?', array(intval($data->id)))
                        ->execute();

                    if ($releatedthemes != '') {
                        foreach (explode(',', $releatedthemes) as $key) {
                            $group = new ArticleThemeArticle();
                            $group->theme_id = $key;
                            $group->article_id = $data->id;
                            $group->save();
                        }
                    }
                }
                if (isset($confirmcontact)) {
                    Doctrine_Query::create()
                        ->delete()
                        ->from('ArticleConfirmContact as a')
                        ->where('a.article_id = ?', array(intval($data->id)))
                        ->execute();

                    if ($confirmcontact != '') {
                        foreach (explode(',', $confirmcontact) as $key) {
                            $group = new ArticleConfirmContact();
                            $group->contact_id = $key;
                            $group->article_id = $data->id;
                            $group->save();
                        }
                    }
                }
                if (isset($releatedthemesmarket)) {
                    Doctrine_Query::create()
                        ->delete()
                        ->from('ArticleThemeMarketArticle as a')
                        ->where('a.article_id = ?', array(intval($data->id)))
                        ->execute();

                    if ($releatedthemesmarket != '') {
                        foreach (explode(',', $releatedthemesmarket) as $key) {
                            $group = new ArticleThemeMarketArticle();
                            $group->theme_id = $key;
                            $group->article_id = $data->id;
                            $group->save();
                        }
                    }
                }

                if (isset($releatedarticle)) {
                    Doctrine_Query::create()
                        ->delete()
                        ->from('ArticleReleated as a')
                        ->where('a.article1 = ?', array(intval($data->id)))
                        ->execute();

                    if ($releatedarticle != '') {
                        foreach (explode(',', $releatedarticle) as $key) {
                            $group = new ArticleReleated();
                            $group->article2 = $key;
                            $group->article1 = $data->id;
                            $group->save();
                        }
                    }
                }

                $isNew = true;
                $id = $data->id;
            }

            if (intval($this->_request->getParam('uid')) != 0) {

                $temp = array();
                foreach ($data->ArticleGroupArticle as $row) {
                    array_push($temp, $row->articlegroup_id);
                }
                $temp1 = array();
                foreach ($data->Releated as $row) {
                    array_push($temp1, $row->id);
                }
                $temp2 = array();
                foreach ($data->ArticleThemeArticle as $row) {
                    array_push($temp2, $row->theme_id);
                }
                $temp3 = array();
                foreach ($data->ArticleThemeMarketArticle as $row) {
                    array_push($temp3, $row->theme_id);
                }

                $temp4 = array();
                foreach ($data->ArticleGroupMarketArticle as $row) {
                    array_push($temp4, $row->articlegroup_id);
                }

                $obj = TP_Mongo::getInstance();
                $obj = $obj->articles->findOne(array('_id' => $data->uuid));
                $temp5 = array();
                $searchtags = array();
                if ($obj) {
                    foreach ($obj['tags'] as $row) {
                        $temp5[] = $row['name'];
                    }
                    $searchtags = $temp5;
                    $temp5 = implode(',', $temp5);
                }
                $data->save();

                if ($data->a4_abpreis_calc == true && $data->a1_xml != "") {

                    $this->getRequest()->setParam('s1', 1);
                    $articlesObjs = Zend_Registry::get('articles');

                    $articleObj = new $articlesObjs [$data->typ] ();
                    unset($articlesObjs);

                    $this->view->article = $data;
                    $this->view->form = array();

                    $this->view->mwert = $data->mwert;
                    $price = $articleObj->createFrontend($this, $this->translate, $data, true);

                    $data->a4_abpreis = $price + ($price / 100 * $data->mwert);
                    $data->save();
                }

                $temp6 = array();
                foreach ($data->ArticleConfirmContact as $row) {
                    array_push($temp6, $row->contact_id);
                }

                $rowOrg = $data;
                $data = $data->toArray(false);

                $data['mwert'] = $data['mwert'] . '%';
                $data['tags'] = $temp5;

                $account = Doctrine_Query::create()
                    ->select()
                    ->from('Account as a')
                    ->where('a.id = ?', array(intval($data['confirmaccount_id'])))
                    ->fetchOne();
                if ($account) {
                    $data['confirmaccount'] = $account->company;
                } else {
                    $data['confirmaccount'] = '';
                }
                $data['articlegroups[]'] = implode(',', $temp);
                $data['releatedarticle[]'] = implode(',', $temp1);
                $data['releatedthemes[]'] = implode(',', $temp2);
                $data['releatedthememarket[]'] = implode(',', $temp3);
                $data['articlegroupsmarket[]'] = implode(',', $temp4);
                $data['confirmcontact[]'] = implode(',', $temp6);

                $data['producttype'] = $article->name;

                $data['linkextern'] = 'http://' . $rowOrg->Shop->Domain[0]->name . '/article/show/uuid/' . $data['uuid'];
                $data['url'] = 'http://' . $rowOrg->Shop->Domain[0]->name . '/article/show/' . $data['url'];
                require_once(APPLICATION_PATH . '/helpers/Image.php');
                $img = new TP_View_Helper_Image();

                if ($rowOrg->typ == 6 && $rowOrg->a6_org_article != 0) {

                    $data['htmlsnippet'] = '<div id="pscwrapper">
						<div id="titel">' . $rowOrg->title . '</div>
						<div id="bild"><img src="http://' . $rowOrg->Shop->Domain[0]->name . '/' . $img->thumbnailFop($rowOrg->title, 'articlelist', $rowOrg->getMarketFile(), true) . '"/></div>
						<div id="einleitung">' . $rowOrg->einleitung . '</div>
						<div id="link"><a href="http://' . $rowOrg->Shop->Domain[0]->name . '/article/show/uuid/' . $data['uuid'] . '">Weiter</a></div>
					</div>';
                } else {
                    if ($rowOrg->file != "") {
                        $data['htmlsnippet'] = '<div id="pscwrapper">
							<div id="titel">' . $rowOrg->title . '</div>
							<div id="bild"><img src="http://' . $rowOrg->Shop->Domain[0]->name . '/' . $img->thumbnailImage($rowOrg->title, 'articlelist', $rowOrg->file, true) . '"/></div>
							<div id="einleitung">' . $rowOrg->einleitung . '</div>
							<div id="link"><a href="http://' . $rowOrg->Shop->Domain[0]->name . '/article/show/uuid/' . $data['uuid'] . '">Weiter</a></div>
						</div>';
                    } else {
                        $data['htmlsnippet'] = '<div id="pscwrapper">
							<div id="titel">' . $rowOrg->title . '</div>
							<div id="bild"><img width="171" src="http://' . $rowOrg->Shop->Domain[0]->name . '/styles/vorlagen/design3/img/no-img.png"/></div>
							<div id="einleitung">' . $rowOrg->einleitung . '</div>
							<div id="link"><a href="http://' . $rowOrg->Shop->Domain[0]->name . '/article/show/uuid/' . $data['uuid'] . '">Weiter</a></div>
						</div>';
                    }
                }

                if ($rowOrg->init_status) {
                    $config = new Zend_Config_Ini(APPLICATION_PATH . '/configs/' . '/systemdefaults.ini', 'status');

                    $translate = Zend_Registry::get('translate');

                    $temp = array();

                    $status = $config->toArray();

                    $data['init_status_title'] = $translate->translate($rowOrg->init_status);
                }
                if ($rowOrg->motiv_group) {
                    $config = new Zend_Config_Ini(APPLICATION_PATH . '/configs/' . '/systemdefaults.ini', 'motivstaffel');

                    $translate = Zend_Registry::get('translate');

                    $temp = array();

                    $status = $config->toArray();

                    $data['motiv_group_title'] = $status[$rowOrg->motiv_group];
                }
                if ($rowOrg->upload_article_status) {
                    $data['upload_article_status_title'] = $this->translate->translate($rowOrg->upload_article_status);
                }
                if ($rowOrg->upload_multi_status) {
                    $data['upload_multi_status_title'] = $this->translate->translate($rowOrg->upload_multi_status);
                }
                if ($rowOrg->upload_center_status) {
                    $data['upload_center_status_title'] = $this->translate->translate($rowOrg->upload_center_status);
                }
                if ($rowOrg->upload_weblayouter_status) {
                    $data['upload_weblayouter_status_title'] = $this->translate->translate($rowOrg->upload_weblayouter_status);
                }
                if ($rowOrg->upload_post_status) {
                    $data['upload_post_status_title'] = $this->translate->translate($rowOrg->upload_post_status);
                }
                if ($rowOrg->upload_email_status) {
                    $data['upload_email_status_title'] = $this->translate->translate($rowOrg->upload_email_status);
                }
                if ($rowOrg->upload_collecting_orders_status) {
                    $data['upload_collecting_orders_status_title'] = $this->translate->translate($rowOrg->upload_collecting_orders_status);
                }
                if ($rowOrg->upload_templateprint_status) {
                    $data['upload_templateprint_status_title'] = $this->translate->translate($rowOrg->upload_templateprint_status);
                }
                if ($rowOrg->upload_steplayouter_status) {
                    $data['upload_steplayouter_status_title'] = $this->translate->translate($rowOrg->upload_steplayouter_status);
                }
                if ($rowOrg->upload_steplayouter2_status) {
                    $data['upload_steplayouter2_status_title'] = $this->translate->translate($rowOrg->upload_steplayouter2_status);
                }

                $data['orgarticle'] = $rowOrg->OrgArticle->id . ' ' . $rowOrg->OrgArticle->title;
                $data['producttype'] = $rowOrg->id . ' - ' . $data['producttype'];
                if ($rowOrg->a6_org_article != 0 && $data->typ != 8) {
                    $data['producttype'] = 'Sub Product';
                }
                $data ['a9_left1_title'] = $this->translate->translate($data ['a9_left1']);
                $data = $this->cleanArray($data);
            } else {
                $data = array();
                $data['producttype'] = $article->name;
                $data ['a9_left1'] = 'all';
                $data ['a9_left1_title'] = $this->translate->translate($data ['a9_left1']);
                if (method_exists($article, 'defaultSettingsPreDispatch')) {
                    $data = array_merge($data, $article->defaultSettingsPreDispatch());
                }
            }

            $config = array(
                "success" => true
            , "isNew" => $isNew
            , "uid" => $id
            , "metaData" => array(
                    "fields" => $fieldsarray
                , "buttons" => $buttons
                , "formConfig" => $configarray
                , "data" => ($data != false) ? $data : array()
                )
            , "data" => ($data != false) ? $data : array()
            );
            $this->getHelper('Json')->sendJson($config);
        }

        $this->_helper->layout->setLayout('popupjs');
    }

    public function templatesAction() {

    }

    public function clearlayouterAction() {
        $row = Doctrine_Query::create()
            ->from('Article a')
            ->where('id = ? AND shop_id IN (' . implode(',', $this->shopIds) . ')', array($this->_request->getParam('uuid')))
            ->fetchOne();

        if ($row && $this->highRole->level >= 40) {
            $articles = Zend_Registry::get('articles');

            $article = new $articles[$row->typ]();

            if (method_exists($article, 'clearPreDispatch')) {
                $article->clearPreDispatch($row);
            }
        }
    }

    public function cleartemplateprintAction() {
        $row = Doctrine_Query::create()
            ->from('Article a')
            ->where('id = ? AND shop_id IN (' . implode(',', $this->shopIds) . ')', array($this->_request->getParam('uuid')))
            ->fetchOne();

        if(file_exists(APPLICATION_PATH . '/../market/templateprint/orginal/' . $row->a6_directory . '/product.zip')) {
            unlink(APPLICATION_PATH . '/../market/templateprint/orginal/' . $row->a6_directory . '/product.zip');
        }
        if(file_exists(APPLICATION_PATH . '/../market/templateprint/orginal/' . $row->a6_directory . '/final.pdf')) {
            unlink(APPLICATION_PATH . '/../market/templateprint/orginal/' . $row->a6_directory . '/final.pdf');
        }
        if(file_exists(APPLICATION_PATH . '/../market/templateprint/orginal/' . $row->a6_directory . '/preview.pdf')) {
            unlink(APPLICATION_PATH . '/../market/templateprint/orginal/' . $row->a6_directory . '/preview.pdf');
        }
    }

    public function fetcharticletemplatesAction() {

        if ($this->highRole->level == 50) {
            $rows = Doctrine_Query::create()
                ->from('Article m')
                ->limit(intval($this->_request->getParam('limit')))
                ->offset(intval($this->_request->getParam('start')))
                ->addWhere('(template_admin = 1 OR template_system_operator = 1)');
            if ($this->_request->getParam('sort')) {
                $rows->orderBy($this->_request->getParam('sort') . ' ' . $this->_request->getParam('dir'));
            }

            TP_Filter::proccess($rows);

            $rows = $rows->execute();

            $count = Doctrine_Query::create()
                ->from('Article m')
                ->addWhere('(template_admin = 1 OR template_system_operator = 1)');
            if (intval(str_replace('S', '', $this->_request->getParam('sid'))) != 0) {
                $count->where('shop_id = ?', array(intval(str_replace('S', '', $this->_request->getParam('sid')))));
            }

            TP_Filter::proccess($count);

            $count = $count->execute();

            $list = array();
            $articles = Zend_Registry::get('articles');
            foreach ($rows as $row) {
                if (isset($articles[$row->typ])) {
                    $typ = new $articles[$row->typ]();
                    $rowOrg = $row;
                    $row = $row->toArray(false);
                    $row['typName'] = $typ->name;
                    $row['link'] = 'http://' . $rowOrg->Shop->Domain[0]->name . '/article/show/uuid/' . $row['uuid'];
                    array_push($list, $row);
                }
            }
            $this->view->success = true;
            $this->view->totalCount = $count->count();
            $this->view->rows = $list;
        } elseif ($this->highRole->level == 40) {

            $rows = Doctrine_Query::create()
                ->from('Article m')
                ->limit(intval($this->_request->getParam('limit')))
                ->offset(intval($this->_request->getParam('start')))
                ->addWhere('template_system_operator = 1');
            if ($this->_request->getParam('sort')) {
                $rows->orderBy($this->_request->getParam('sort') . ' ' . $this->_request->getParam('dir'));
            }

            TP_Filter::proccess($rows);

            $rows = $rows->execute();

            $count = Doctrine_Query::create()
                ->from('Article m')
                ->addWhere('template_system_operator = 1');
            if (intval(str_replace('S', '', $this->_request->getParam('sid'))) != 0) {
                $count->where('shop_id = ?', array(intval(str_replace('S', '', $this->_request->getParam('sid')))));
            }

            TP_Filter::proccess($count);

            $count = $count->execute();

            $list = array();
            $articles = Zend_Registry::get('articles');
            foreach ($rows as $row) {
                if (isset($articles[$row->typ])) {
                    $typ = new $articles[$row->typ]();
                    $rowOrg = $row;
                    $row = $row->toArray(false);
                    $row['typName'] = $typ->name;
                    $row['link'] = 'http://' . $rowOrg->Shop->Domain[0]->name . '/article/show/uuid/' . $row['uuid'];
                    array_push($list, $row);
                }
            }
            $this->view->success = true;
            $this->view->totalCount = $count->count();
            $this->view->rows = $list;
        }
    }

    public function cleanArray($array) {
        foreach ($array as $key => $arr) {
            $array[$key] = str_replace('\\\\', '', $arr);
        }
        return $array;
    }

}
