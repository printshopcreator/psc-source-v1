<?php

class Admin_ArticlegroupController extends TP_Admin_Controller_Action
{

    public function init() {
        if (!$this->_request->getParam('sid', false)) {
            $wizard = new TP_ResaleWizard();
            $this->_request->setParam('sid', $wizard->getArticleShop());
        }
        parent::init();
        $contextSwitch = $this->_helper->contextSwitch();
        $contextSwitch->addActionContext('loadsavearticletheme', self::CONTEXT_JSON)

            ->addActionContext('langselect', self::CONTEXT_JSON)
            ->addActionContext('langadd', self::CONTEXT_JSON)
            ->addActionContext('langfetch', self::CONTEXT_JSON)->setAutoJsonSerialization(true)->initContext();
    }

    public function loadsavearticlethemeAction() {

        $data = new ArticleGroup();
        $data->enable = true;
        $data->title = $this->_getParam('title');
        $wizard = new TP_ResaleWizard();
        $data->shop_id = $wizard->getArticleShop();
        $data->install_id = $this->user->install_id;
        $data->parent = $this->_getParam('parent_id', 0);
        $data->save();

    }

    public function langselectAction() {

        $this->view->clearVars();
        $temp = array();
        $temp[] = array('name' => 'en_US', 'id' => 'en');
        $temp[] = array('name' => 'fr_FR', 'id' => 'fr');

        $this->view->results = $temp;
    }

    public function langaddAction() {

        $article = Doctrine_Query::create()
            ->from('ArticleGroup c')
            ->where('c.id = ?', array($this->_getParam('articlegroup')))
            ->fetchOne();

        $language = $article->getLangData();

        if (!isset($language[$this->_getParam('value')])) {
            $language[$this->_getParam('value')] = array('uuid' => TP_Util::uuid());
        }
        $article->setLangData($language);
        $article->save();
        $this->view->clearVars();
        $this->view->success = true;
    }

    public function langfetchAction() {

        $article = Doctrine_Query::create()
            ->from('ArticleGroup c')
            ->where('c.id = ?', array($this->_getParam('uid')))
            ->fetchOne();

        $language = $article->getLangData();
        $settings = json_decode($this->_getParam('results'), true);

        if ($this->_request->getParam('xaction', false) == "update") {

            $language[$settings['lang']] = array('uuid' => $settings['uuid'], 'title' => $settings['title']);
            $article->setLangData($language);
            $article->save();

            die($this->getHelper('Json')->sendJson(array('success' => true, 'newID' => $article->id)));

        }

        $temp = array();
        foreach ($language as $key => $row) {
            $temp[] = array(
                'lang' => $key,
                'title' => @$row['title'],
                'uuid' => @$row['uuid']
            );
        }

        $this->view->clearVars();
        $this->view->results = $temp;
    }

    public function allAction() {

        if ($this->_request->getParam('config') == 1) {

            $rows = Doctrine_Query::create()->from('ArticleGroup m')->limit(intval($this->_request->getParam('limit')))->offset(intval($this->_request->getParam('start')));
            if (intval(str_replace('S', '', $this->_request->getParam('sid'))) != 0) {
                $rows->where('shop_id = ?', array(intval(str_replace('S', '', $this->_request->getParam('sid')))));
            }
            if ($this->_request->getParam('sort')) {
                $rows->orderBy($this->_request->getParam('sort') . ' ' . $this->_request->getParam('dir'));
            }

            TP_Filter::proccess($rows);

            $rows = $rows->execute();

            $count = Doctrine_Query::create()->from('ArticleGroup m');
            if (intval(str_replace('S', '', $this->_request->getParam('sid'))) != 0) {
                $count->where('shop_id = ?', array(intval(str_replace('S', '', $this->_request->getParam('sid')))));
            }

            TP_Filter::proccess($count);

            $count = $count->execute();

            $temp = array();
            foreach ($rows->toArray() as $row) {
                if ($row ['parent'] != 0) {
                    $parent = Doctrine_Query::create()->from('ArticleGroup m')->where('id = ?')->fetchOne(array($row ['parent']));
                    if ($parent != false) {
                        if($parent->parent != 0) {
                            $row ['parent'] = $this->getParent($parent);
                        }else{
                            $row ['parent'] = $parent->title;
                        }


                    }
                } else {
                    $row ['parent'] = $this->translate->translate('articlegroup_all_root');
                }
                array_push($temp, $row);

            }

            $config = array("success" => true, "totalCount" => $count->count(), "rows" => $temp);

            $this->getHelper('Json')->sendJson($config);
        }

        $this->view->sid = intval(str_replace('S', '', $this->_request->getParam('id')));

        $this->_helper->layout->setLayout('simple');

    }

    protected function getParent($articlegroup) {
        $text = $articlegroup->title;
        if($articlegroup->parent != 0) {
            $parent = Doctrine_Query::create()->from('ArticleGroup m')->where('id = ?')->fetchOne(array($articlegroup->parent));
            if($parent->parent != 0) {
                $text = $text . ' -> ' . $this->getParent($parent);
            }else{
                $text = $text . ' -> ' . $parent->title;
            }
        }
        return $text;
    }

    public function editAction() {

        $isNew = false;
        $id = 0;

        if (intval($this->_request->getParam('delete')) == 1) {

            Doctrine_Query::create()->from('ArticleGroup a')->where('id = ?', array($this->_request->getParam('uid')))->delete()->execute();

            die ($this->getHelper('Json')->sendJson(array('success' => true)));
        }

        if (intval($this->_request->getParam('config')) == 3) {

            $translate = Zend_Registry::get('translate');

            $user = Zend_Auth::getInstance()->getIdentity();
            $user = Doctrine_Query::create()->from('Contact m')->where('id = ?')->fetchOne(array($user ['id']));
            $role = null;
            foreach ($user->Roles as $rol) {
                if ($role == null || $rol->level > $role->level) {
                    $role = $rol;
                }
            }

            $config = new Zend_Config_Ini (APPLICATION_PATH . '/modules/admin/config/' . 'articlegroup/settings.ini', $role->name);

            $fieldsarray = array();
            foreach ($config->tabs as $tab) {
                $tab = $tab->toArray();
                $fields = array();
                $tab ['general'] ['title'] = $translate->translate($tab ['general'] ['title']);
                foreach ($tab ['fields'] as $key => $field) {
                    $field ['fieldLabel'] = $translate->translate($field ['fieldLabel']);
                    array_push($fields, $field);
                }

                array_push($fieldsarray, array('general' => $tab ['general'], 'fields' => $fields));
            }

            $configarray = $config->general->toArray();
            $configarray ['title'] = $translate->translate($configarray ['title']);

            $data = false;

            if (intval($this->_request->getParam('uid')) != 0) {

                $data = Doctrine_Query::create()->from('ArticleGroup a')->where('a.id = ?')->fetchOne(array(intval($this->_request->getParam('uid'))));
            }

            if (intval($this->_request->getParam('save')) == 1 && intval($this->_request->getParam('uid')) > 0) {

                $params = $this->_request->getParams();

                unset ($params ['config']);
                unset ($params ['save']);
                unset ($params ['module']);
                unset ($params ['controller']);
                unset ($params ['action']);
                unset ($params ['sid']);
                unset ($params ['uid']);
                unset($params['headingsize']);
                $filter = new TP_Filter_Badwords ();

                foreach ($params as $key => $value) {

                    if (isset ($data [$key]) && false == strpos($value, 'service/index?type=upload&mode=get')) {
                        if ($value == 'true' || $value == 'on') {
                            $data->$key = true;
                        } elseif ($value == 'false' || $value == 'off') {
                            $data->$key = false;
                        }
                        else {
                            $data->$key = $filter->filter($value);
                        }
                    }
                }
                if($data->parent == $data->id) {
                    $data->parent = 0;
                }
                $data->save();

            } elseif (intval($this->_request->getParam('save')) == 1 && intval($this->_request->getParam('uid')) == 0) {

                $params = $this->_request->getParams();

                unset ($params ['config']);
                unset ($params ['save']);
                unset ($params ['module']);
                unset ($params ['controller']);
                unset ($params ['action']);
                unset ($params ['sid']);
                unset ($params ['uid']);
                unset($params['headingsize']);
                unset ($params ['file']);

                $data = new ArticleGroup ();
                $data->shop_id = intval($this->_request->getParam('sid'));
                $data->Install = $user->Install;
                $filter = new TP_Filter_Badwords ();
                foreach ($params as $key => $value) {

                    if (isset ($data [$key]) && false == strpos($value, 'service/index?type=upload&mode=get')) {
                        if ($value == 'true' || $value == 'on') {
                            $data->$key = true;
                        } elseif ($value == 'false' || $value == 'off') {
                            $data->$key = false;
                        }
                        else {
                            $data->$key = $filter->filter($value);
                        }
                    }
                }

                $data->save();

                $isNew = true;
                $id = $data->id;
            }

            $temp = array();
            $ArticleGroup = $data->ArticleGroup;
            if ($data != false) {
                $temp = $data->toArray();
                if ($ArticleGroup->title == "") {
                    $temp ['parent_id'] = $this->translate->translate('articlegroup_all_root');
                    $temp ['parent'] = 0;
                } else {
                    $temp ['parent_id'] = $ArticleGroup->title;
                    $temp ['parent'] = $ArticleGroup->id;
                }
                $temp ['language_title'] = $this->translate->translate($temp ['language']);

            } else {
                $temp ['language'] = 'all';
                $temp ['display_title'] = 1;
                $temp ['language_title'] = $this->translate->translate($temp ['language']);
            }

            $config = array("success" => true, "isNew" => $isNew, "uid" => $id, "metaData" => array("fields" => $fieldsarray, "formConfig" => $configarray, "data" => $temp), "data" => $temp);
            $this->getHelper('Json')->sendJson($config);
        }

        $this->_helper->layout->setLayout('popupjs');

    }

}
