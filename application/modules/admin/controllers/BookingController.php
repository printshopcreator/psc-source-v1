<?php

class Admin_BookingController extends TP_Admin_Controller_Action
{

    public function init() {
        parent::init();
        $contextSwitch = $this->_helper->contextSwitch();
        $contextSwitch->addActionContext('fetchbooking', self::CONTEXT_JSON)
            ->addActionContext('loadsavebooking', self::CONTEXT_JSON)
            ->setAutoJsonSerialization(true)->initContext();
    }

    public function allAction() {

    }

    public function editAction() {
        $this->_helper->layout->setLayout('popupjs');
    }

    public function fetchbookingAction() {
        $rows = Doctrine_Query::create()->from('Booking m')
            ->limit(intval($this->_request->getParam('limit')))
            ->offset(intval($this->_request->getParam('start')))
            ->where('m.install_id = ?', array($this->install->id));
        if ($this->_request->getParam('sort')) {
            $rows->orderBy($this->_request->getParam('sort') . ' ' . $this->_request->getParam('dir'));
        }
        $rows = $rows->execute();
        $count = Doctrine_Query::create()->from('Booking m')->where('m.install_id = ?', array($this->install->id))->execute();

        $temp = array();
        foreach ($rows->toArray() as $row) {

            array_push($temp, $row);
        }

        $this->view->success = true;
        $this->view->totalCount = $count->count();
        $this->view->rows = $temp;
    }

    public function loadsavebookingAction() {
        $isNew = false;
        $id = 0;
        $translate = Zend_Registry::get('translate');
        $user = Zend_Auth::getInstance()->getIdentity();
        $user = Doctrine_Query::create()->from('Contact m')->where('id = ?')->fetchOne(array(
            $user['id']));
        $role = null;

        foreach ($user->Roles as $rol) {
            if ($role == null || $rol->level > $role->level) {
                $role = $rol;
            }
        }
        $config = new Zend_Config_Ini(APPLICATION_PATH . '/modules/admin/config/booking/settings.ini', $role->name);
        $fieldsarray = array();
        foreach ($config->tabs as $tab) {
            $tab = $tab->toArray();
            $fields = array();
            foreach ($tab['fields'] as $key => $field) {
                $field['fieldLabel'] = $translate->translate($field['fieldLabel']);
                array_push($fields, $field);
            }
            array_push($fieldsarray, array(
                'general' => $tab['general'],
                'fields' => $fields));
        }
        $configarray = $config->general->toArray();
        $configarray['title'] = $translate->translate($configarray['title']);
        $data = array();
        if (intval($this->_request->getParam('uid')) != 0) {
            $data = Doctrine_Query::create()->from('Booking a')->where('a.id = ? AND a.install_id = ?')->fetchOne(array(
                intval($this->_request->getParam('uid')), $this->install->id));
        }
        if (intval($this->_request->getParam('save')) == 1 && intval($this->_request->getParam('uid')) > 0) {
            $params = TP_Util::clearParams($this->_request->getParams());
            $filter = new TP_Filter_Badwords();
            foreach ($params as $key => $value) $data->$key = $filter->filter($value);
            $data->save();
        } elseif (intval($this->_request->getParam('save')) == 1 && intval($this->_request->getParam('uid')) == 0) {
            $params = TP_Util::clearParams($this->_request->getParams());
            $data = new Booking();
            $data->install_id = $this->install->id;
            $filter = new TP_Filter_Badwords();
            foreach ($params as $key => $value) $data->$key = $filter->filter($value);
            $data->save();
            $isNew = true;
            $id = $data->id;
        }

        if ($data) {

            $shop = $data->Shop->name;
            $data = $data->toArray();
            $data['shop_title'] = $shop;
        }
        if ($data['type'] == 2) {
            $data['type_title'] = "Motiv";
        } else {
            $data['type_title'] = "Product";
        }
        if ($data['booking_typ'] == 1) {
            $data['booking_typ_title'] = "Zugang";
        } elseif ($data['booking_typ'] == 2) {
            $data['booking_typ_title'] = "Abgang";
        } elseif ($data['booking_typ'] == 3) {
            $data['booking_typ_title'] = "Stonierung";
        } else {
            $data['booking_typ_title'] = "Manuelle Buchung";
        }

        $this->view->success = true;
        $this->view->uid = $id;
        $this->view->isNew = $isNew;
        $this->view->metaData = array(
            "fields" => $fieldsarray,
            "formConfig" => $configarray,
            "data" => $data);

        $this->view->data = $data;

    }
}
