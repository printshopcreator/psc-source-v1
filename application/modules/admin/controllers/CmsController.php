<?php

class Admin_CmsController extends TP_Admin_Controller_Action
{

    /**
     * IndexAction
     *
     * @todo Dokumentation der IndexAction
     *
     * @return void
     */
    public function init() {
        parent::init();
        $contextSwitch = $this->_helper->contextSwitch();
        $contextSwitch->addActionContext('liveedit', self::CONTEXT_JSON)
            ->setAutoJsonSerialization(true)->initContext();
    }

    public function liveeditAction() {

        $cms = Doctrine_Query::create()
            ->from('Cms m')
            ->addWhere('shop_id IN (' . implode(',', $this->shopIds) . ') AND id = ?', array($this->_getParam('id')))->fetchOne();

        if (!$cms) {
            $this->view->success = false;
            return;
        }

        $field = $this->_getParam('field');
        if ($cms->getTable()->hasField($field)) {
            $cms->$field = $this->_getParam('content');
            $cms->save();

            $this->view->success = true;
            return;
        }

        $this->view->success = false;
        return;
    }

    public function allAction() {
        if ($this->_request->getParam('config') == 1) {

            $rows = Doctrine_Query::create()
                ->from('Cms m')
                ->limit(intval($this->_request->getParam('limit')))
                ->offset(intval($this->_request->getParam('start')));
            if (intval(str_replace('S', '', $this->_request->getParam('sid'))) != 0) {
                $rows->where('shop_id = ?', array(intval(str_replace('S', '', $this->_request->getParam('sid')))));
            }
            if ($this->_request->getParam('sort')) {
                $rows->orderBy($this->_request->getParam('sort') . ' ' . $this->_request->getParam('dir'));
            }

            TP_Filter::proccess($rows);
            $rows = $rows->execute();

            $count = Doctrine_Query::create()
                ->from('Cms m');
            if (intval(str_replace('S', '', $this->_request->getParam('sid'))) != 0) {
                $count->where('shop_id = ?', array(intval(str_replace('S', '', $this->_request->getParam('sid')))));
            }

            TP_Filter::proccess($count);
            $count = $count->execute();

            $temp = array();
            foreach ($rows->toArray() as $row) {
                if ($row['parent'] != 0) {
                    $parent = Doctrine_Query::create()
                        ->from('Cms m')
                        ->where('id = ?')
                        ->fetchOne(array($row['parent']));
                    if ($parent != false) {
                        $row['parent'] = $parent->title;
                    }
                } else {
                    $row['parent'] = $this->translate->translate('articlegroup_all_root');
                }
                array_push($temp, $row);
            }

            $config = array(
                "success" => true
            , "totalCount" => $count->count()
            , "rows" => $temp
            );

            $this->getHelper('Json')->sendJson($config);
        }

        $this->view->sid = intval(str_replace('S', '', $this->_request->getParam('id')));

        $this->_helper->layout->setLayout('simple');
    }

    public function editAction() {

        $isNew = false;
        $id = 0;

        if (intval($this->_request->getParam('delete')) == 1) {

            Doctrine_Query::create()
                ->from('Cms a')
                ->where('id = ?', array($this->_request->getParam('uid')))
                ->delete()
                ->execute();

            die($this->getHelper('Json')->sendJson(array('success' => true)));
        }

        if (intval($this->_request->getParam('config')) == 3) {

            $translate = Zend_Registry::get('translate');

            $user = Zend_Auth::getInstance()->getIdentity();
            $user = Doctrine_Query::create()
                ->from('Contact m')
                ->where('id = ?')->fetchOne(array($user['id']));

            $role = null;
            foreach ($user->Roles as $rol) {
                if ($role == null || $rol->level > $role->level) {
                    $role = $rol;
                }
            }
            $config = new Zend_Config_Ini(APPLICATION_PATH . '/modules/admin/config/' . 'cms/settings.ini', $role->name);

            $fieldsarray = array();
            foreach ($config->tabs as $tab) {
                $tab = $tab->toArray();
                $fields = array();
                foreach ($tab['fields'] as $key => $field) {
                    $field['fieldLabel'] = $translate->translate($field['fieldLabel']);
                    array_push($fields, $field);
                }

                array_push($fieldsarray, array('general' => $tab['general'], 'fields' => $fields));
            }

            $configarray = $config->general->toArray();
            $configarray['title'] = $translate->translate($configarray['title']);

            $data = false;

            if (intval($this->_request->getParam('uid')) != 0) {

                $data = Doctrine_Query::create()
                    ->from('Cms a')
                    ->where('a.id = ?')->fetchOne(array(intval($this->_request->getParam('uid'))));
            }
            if (intval($this->_request->getParam('save')) == 1 && intval($this->_request->getParam('uid')) > 0) {

                $params = $this->_request->getParams();

                unset($params['config']);
                unset($params['save']);
                unset($params['module']);
                unset($params['controller']);
                unset($params['action']);
                unset($params['sid']);
                unset($params['uid']);
                unset($params['headingsize']);

                if (isset($params['notinmenu']) && $params['notinmenu'] == 'on') {
                    $data->notinmenu = true;
                } else {
                    $data->notinmenu = false;
                }
                if (isset($params['private']) && $params['private'] == 'on') {
                    $data->private = true;
                } else {
                    $data->private = false;
                }
                unset($params['notinmenu']);
                unset($params['private']);
                $filter = new TP_Filter_Badwords();
                foreach ($params as $key => $value)
                    $data->$key = $filter->filter($value);
                $data->save();
            } elseif (intval($this->_request->getParam('save')) == 1 && intval($this->_request->getParam('uid')) == 0) {

                $params = $this->_request->getParams();

                unset($params['config']);
                unset($params['save']);
                unset($params['module']);
                unset($params['controller']);
                unset($params['action']);
                unset($params['sid']);
                unset($params['uid']);
                unset($params['headingsize']);

                $data = new Cms();
                $data->shop_id = intval($this->_request->getParam('sid'));
                $data->Install = $user->Install;

                if (isset($params['notinmenu']) && $params['notinmenu'] == 'on') {
                    $data->notinmenu = true;
                } else {
                    $data->notinmenu = false;
                }
                if (isset($params['private']) && $params['private'] == 'on') {
                    $data->private = true;
                } else {
                    $data->private = false;
                }
                unset($params['notinmenu']);
                unset($params['private']);
                $filter = new TP_Filter_Badwords();
                foreach ($params as $key => $value)
                    $data->$key = $filter->filter($value);
                $data->save();

                $isNew = true;
                $id = $data->id;

            }

            $temp = array();
            if ($data != false) {

                $Cms = $data->Cms->title;
                $temp = $data->toArray();
                $temp['language_title'] = $this->translate->translate($temp['language']);
                $temp['parent_id'] = $Cms;
            } else {
                $temp['language'] = 'all';
                $temp['display_title'] = 1;

                $temp['language_title'] = $this->translate->translate($temp['language']);
            }

            $config = array(
                "success" => true
            , "isNew" => $isNew
            , "uid" => $id
            , "metaData" => array(
                    "fields" => $fieldsarray
                , "formConfig" => $configarray
                , "data" => $temp
                )
            , "data" => $temp
            );

            $this->getHelper('Json')->sendJson($config);
        }
        if ($this->_getParam('sid', false) == "") {
            die("WRONG PARAMS");
        }
        $this->_helper->layout->setLayout('popupjs');
    }

}
