<?php

class Admin_ContactController extends TP_Admin_Controller_Action
{

    public function init() {
        parent::init();

        $contextSwitch = $this->_helper->contextSwitch();

        $contextSwitch->addActionContext('contactshop', self::CONTEXT_JSON)
            ->addActionContext('contactaddress', self::CONTEXT_JSON)
            ->addActionContext('contactshopdelete', self::CONTEXT_JSON)
            ->addActionContext('contactshopselect', self::CONTEXT_JSON)
            ->addActionContext('contactshopadd', self::CONTEXT_JSON)
            ->addActionContext('contactshopchangeadmin', self::CONTEXT_JSON)
            ->addActionContext('contactaddresschangedisplay', self::CONTEXT_JSON)
            ->setAutoJsonSerialization(true)->initContext();
    }

    public function contactaddresschangedisplayAction() {
        if (intval($this->_request->getParam('changeadmin')) == 1) {

            $row = Doctrine_Query::create()->from('ContactAddress a')->where('uuid = ?', array($this->_request->getParam('uuid')))->fetchOne();

            $row->display = !$row->display;

            $row->save();

            $this->view->success = true;
        }
    }

    public function contactshopAction() {

        $temp = array();
        if ($this->highRole->level >= 40) {
            $contact = Doctrine_Query::create()->from('Contact a')->where('id = ?', array($this->_request->getParam('uid')))->fetchOne();
        } else {
            $contact = Doctrine_Query::create()->from('Contact a')->leftJoin('a.ShopContact s')->where('s.shop_id != ? AND a.id = ?', array($this->install->defaultmarket, $this->_request->getParam('uid')))->fetchOne();
        }

        foreach ($contact->ShopContact as $row) {
            $rowTemp = $row->toArray();
            $rowTemp ['shop'] = $row->Shop->name;

            $temp [] = $rowTemp;
        }
        $this->view->results = $temp;
    }

    public function contactaddressAction() {

        $temp = array();

        $contacts = Doctrine_Query::create()->from('ContactAddress a')->where('contact_id = ?', array($this->_request->getParam('uid')))->execute();

        foreach ($contacts as $row) {

            $temp [] = $row->toArray();
        }
        $this->view->results = $temp;
    }

    public function contactshopselectAction() {

        $temp = array();

        foreach ($this->user->ShopContact as $row) {
            if ($row->admin == true) {
                $rowTemp = $row->toArray();
                $rowTemp ['shop'] = $row->Shop->name;

                $temp [] = $rowTemp;
            }
        }
        $this->view->results = $temp;
    }

    public function contactshopdeleteAction() {
        if (intval($this->_request->getParam('delete')) == 1 && $this->_request->getParam('uuid') != "") {

            Doctrine_Query::create()->from('ShopContact a')->where('uuid = ?', array($this->_request->getParam('uuid')))->delete()->execute();

            $this->view->success = true;
        }
    }

    public function contactshopchangeadminAction() {
        if (intval($this->_request->getParam('changeadmin')) == 1 && $this->_request->getParam('uuid') != "") {

            if ($this->highRole->level >= 40) {
                $row = Doctrine_Query::create()->from('ShopContact a')->where('uuid = ?', array($this->_request->getParam('uuid')))->fetchOne();
            } else {
                $row = Doctrine_Query::create()->from('ShopContact a')->where('shop_id != ? AND uuid = ?', array($this->install->defaultmarket, $this->_request->getParam('uuid')))->fetchOne();
            }
            if ($row) {
                $row->admin = !$row->admin;

                $row->save();
            }
            $this->view->success = true;
        }
    }

    public function contactshopaddAction() {

        $shopcontact = new ShopContact ();
        $shopcontact->shop_id = $this->_getParam('value');
        $shopcontact->contact_id = $this->_getParam('contact');
        $shopcontact->save();
    }

    public function exportAction() {
        $this->getHelper('layout')->disableLayout();
        $this->_helper->viewRenderer->setNoRender();

        require_once('PHPExcel/PHPExcel.php');

        if ($this->install->export_template_contact == "") {
            echo "Kein Exporttemplate hochgeladen";
            return;
        }

        $file = Doctrine_Query::create()->select()
            ->from('Image i')->where('i.id = ?', array($this->install->export_template_contact))->fetchOne();

        $objPHPExcel = PHPExcel_IOFactory::load($file->path);
        $columns = 26;
        $mapping = array();
        $cells = array('A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z');
        for ($i = 0; $i < $columns; $i++) {
            if ($objPHPExcel->getActiveSheet()->getCell($cells[$i] . '2')->getValue() == "") {
                //continue;
            }
            $mapping[] = $objPHPExcel->getActiveSheet()->getCell($cells[$i] . '2')->getValue();
        }

        $rows = Doctrine_Query::create()
            ->select('m.self_firstname, m.self_lastname, m.self_city, m.self_zip, m.self_street, m.self_house_number, m.self_phone, m.self_email')
            ->from('Contact m')
            ->leftJoin('m.ShopContact as s')
            ->leftJoin('s.Shop as sh')
            ->addWhere('s.shop_id IN (' . implode(',', $this->shopIds) . ')');

        if ($this->highRole->level < 40) {
            $rows->addWhere('m.id != ?', $this->install->admincontact);
        }
        if (intval($this->_request->getParam('sid')) != 0) {
            $rows->addWhere('s.shop_id = ?', array(intval($this->_request->getParam('sid'))));
        } else {

        }

        $rows = $rows->execute();

        $loader = new Twig_Loader_String();
        $twig = new Twig_Environment($loader);

        $i = 2;

        foreach ($rows as $row) {

            reset($mapping);
            $ii = 0;
            foreach ($mapping as $key => $value) {

                $template = $twig->loadTemplate($value);
                $content = $template->render(array('contact' => $row));

                $objPHPExcel->getActiveSheet()->setCellValue($cells[$key] . $i, $content);
                $ii++;
            }

            $i++;
        }

        // Redirect output to a client’s web browser (Excel5)

        if ($this->_getParam('mode', false)) {

            header('Content-Type: text/csv');
            header('Content-Disposition: attachment;filename="export.csv"');
            header('Cache-Control: max-age=0');

            $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'CSV');
            $objWriter->save('php://output');
            return;

        }

        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="export.xls"');
        header('Cache-Control: max-age=0');

        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        $objWriter->save('php://output');
    }

    /**
     * IndexAction
     *
     * @todo Dokumentation der IndexAction
     *
     * @return void
     */
    public function editAction() {

        $isNew = false;
        $id = 0;

        if (intval($this->_request->getParam('config')) == 5) {

            $translate = Zend_Registry::get('translate');

            $user = Zend_Auth::getInstance()->getIdentity();
            $user = Doctrine_Query::create()->from('Contact m')->where('id = ?')->fetchOne(array($user ['id']));

            $role = null;
            foreach ($user->Roles as $rol) {
                if ($role == null || $rol->level > $role->level) {
                    $role = $rol;
                }
            }
            $config = new Zend_Config_Ini(APPLICATION_PATH . '/modules/admin/config/' . 'contact/settings.ini', $role->name);

            $fieldsarray = array();
            $fieldkeys = array();
            foreach ($config->tabs as $tab) {
                $tab = $tab->toArray();
                $fields = array();
                $tab ['general'] ['title'] = $translate->translate($tab ['general'] ['title']);
                foreach ($tab ['fields'] as $key => $field) {
                    $field ['fieldLabel'] = $translate->translate($field ['fieldLabel']);

                    if (isset($field['items'])) {
                        $tempitems = array();
                        foreach ($field['items'] as $subit) {
                            $subit ['fieldLabel'] = $translate->translate($subit ['fieldLabel']);
                            array_push($tempitems, $subit);
                            array_push($fieldkeys, $subit ['name']);
                        }
                        $field['items'] = $tempitems;
                    }

                    array_push($fields, $field);
                    array_push($fieldkeys, $field ['name']);
                    if (isset($field ['nameid'])) {
                        array_push($fieldkeys, $field ['nameid']);
                    }
                }

                array_push($fieldsarray, array('general' => $tab ['general'], 'fields' => $fields));
            }
            array_push($fieldkeys, "self_anrede");
            $configarray = $config->general->toArray();
            $configarray ['title'] = $translate->translate($configarray ['title']);

            $data = false;

            if (intval($this->_request->getParam('uid')) != 0 && intval($this->_request->getParam('delete')) == 1) {

                $data = Doctrine_Query::create()->delete('Contact')->from('Contact as a')->leftJoin('a.Shops as c')->whereIn('c.id', $this->shopIds)->andWhere('a.id = ?', array(intval($this->_request->getParam('id'))))->execute();
            }

            $active = false;

            if (intval($this->_request->getParam('uid')) != 0) {

                $data = Doctrine_Query::create()->from('Contact a')->leftJoin('a.Shops c')->whereIn('c.id', $this->shopIds)->andWhere('a.id = ?', array(intval($this->_request->getParam('uid'))));
                $data = $data->fetchOne();
                $active = $data->enable;

            }
            if (intval($this->_request->getParam('save')) == 1 && intval($this->_request->getParam('uid')) > 0) {

                $data->setKundenNr($this->_request->getParam('kundenNr'));
                $params = TP_Util::cleanArray($this->_request->getParams(), $fieldkeys);

                if (isset($params ['articles'])) {
                    $articles = implode(',', array_slice($params ['articles'], 0, count($params ['articles']) - 1));
                    unset($params ['articles']);
                }
                if (isset($params ['accounts'])) {
                    $accounts = implode(',', array_slice($params ['accounts'], 0, count($params ['accounts']) - 1));
                    unset($params ['accounts']);
                }
                if (isset($params ['paymenttype'])) {
                    $paymenttype = implode(',', array_slice($params ['paymenttype'], 0, count($params ['paymenttype']) - 1));
                    unset($params ['paymenttype']);
                }
                if (isset($params ['cms'])) {
                    $cms = implode(',', array_slice($params ['cms'], 0, count($params ['cms']) - 1));
                    unset($params ['cms']);
                }
                if (isset($params ['shops'])) {
                    $shops = implode(',', array_slice($params ['shops'], 0, count($params ['shops']) - 1));
                    unset($params ['shops']);
                }
                if (isset($params ['groups'])) {
                    $groups = implode(',', array_slice($params ['groups'], 0, count($params ['groups']) - 1));
                    unset($params ['groups']);
                }
                unset($params ['config']);
                unset($params ['save']);
                unset($params ['module']);
                unset($params ['controller']);
                unset($params ['action']);
                unset($params ['sid']);
                unset($params ['uid']);
                unset($params['headingsize']);
                if (isset($params ['enable']) && $params ['enable'] == 'on') {
                    $data->enable = true;
                } else {
                    $data->enable = false;
                }
                unset($params ['enable']);
                if (isset($params ['newsletter']) && $params ['newsletter'] == 'on') {
                    $data->newsletter = true;
                } else {
                    $data->newsletter = false;
                }
                unset($params ['newsletter']);
                if (isset($params ['liefer']) && $params ['liefer'] == 'on') {
                    $data->liefer = true;
                } else {
                    $data->liefer = false;
                }
                unset($params ['liefer']);
                if (isset($params ['mwert']) && $params ['mwert'] == 'on') {
                    $data->mwert = true;
                } else {
                    $data->mwert = false;
                }
                unset($params ['mwert']);
                foreach ($params as $key => $value) {

                    if (isset($data [$key]) && false == strpos($value, 'service/index?type=upload&mode=get')) {
                        if ($value == 'true' || $value == 'on') {
                            $data->$key = true;
                        } elseif ($value == 'false' || $value == 'off') {
                            $data->$key = false;
                        } else {
                            $data->$key = $value;
                        }
                    }
                }

                $data->save();
                if ($data->enable != $active && $data->enable) {
                    TP_Queue::process('changecontacttoactive', 'global', $data);
                }
                if ($data->enable != $active && !$data->enable) {
                    TP_Queue::process('changecontacttoinactive', 'global', $data);
                }

                if (isset($articles)) {

                    Doctrine_Query::create()->delete()->from('ContactArticle as a')->where('a.contact_id = ?', array(intval($data->id)))->execute();

                    if ($articles != '') {
                        foreach (explode(',', $articles) as $key) {
                            $group = new ContactArticle ();
                            $group->contact_id = $data->id;
                            $group->article_id = $key;
                            $group->save();
                        }
                    }
                }

                if (isset($accounts)) {

                    Doctrine_Query::create()->delete()->from('ContactAccount as a')->where('a.contact_id = ?', array(intval($data->id)))->execute();

                    if ($accounts != '') {
                        foreach (explode(',', $accounts) as $key) {
                            $group = new ContactAccount ();
                            $group->contact_id = $data->id;
                            $group->account_id = $key;
                            $group->save();
                        }
                    }
                }

                if (isset($shops)) {

                    Doctrine_Query::create()->delete()->from('ShopContact as a')->where('a.contact_id = ?', array(intval($data->id)))->execute();

                    if ($shops != '') {
                        foreach (explode(',', $shops) as $key) {
                            $group = new ShopContact ();
                            $group->contact_id = $data->id;
                            $group->shop_id = $key;
                            $group->save();
                        }
                    }
                }
                if (isset($groups)) {
                    Doctrine_Query::create()->delete()->from('ContactRole as a')->where('a.contact_id = ?', array(intval($data->id)))->execute();

                    if ($groups != '') {
                        foreach (explode(',', $groups) as $key) {
                            $group = new ContactRole ();
                            $group->contact_id = $data->id;
                            $group->role_id = $key;
                            $group->save();
                        }
                    }
                }
                if (isset($paymenttype)) {

                    Doctrine_Query::create()->delete()->from('ContactPaymenttype as a')->where('a.contact_id = ?', array(intval($data->id)))->execute();

                    if ($paymenttype != '') {
                        foreach (explode(',', $paymenttype) as $key) {
                            $group = new ContactPaymenttype ();
                            $group->contact_id = $data->id;
                            $group->paymenttype_id = $key;
                            $group->save();
                        }
                    }
                }
                if (isset($cms)) {

                    Doctrine_Query::create()->delete()->from('ContactCms as c')->where('c.contact_id = ?', array(intval($data->id)))->execute();

                    if ($cms != '') {
                        foreach (explode(',', $cms) as $key) {
                            $group = new ContactCms ();
                            $group->contact_id = $data->id;
                            $group->cms_id = $key;
                            $group->save();
                        }
                    }
                }
            } elseif (intval($this->_request->getParam('save')) == 1 && intval($this->_request->getParam('uid')) == 0) {

                $params = TP_Util::cleanArray($this->_request->getParams(), $fieldkeys);

                if (isset($params ['articles'])) {
                    $articles = implode(',', array_slice($params ['articles'], 0, count($params ['articles']) - 1));
                    unset($params ['articles']);
                }
                if (isset($params ['accounts'])) {
                    $accounts = implode(',', array_slice($params ['accounts'], 0, count($params ['accounts']) - 1));
                    unset($params ['accounts']);
                }
                if (isset($params ['paymenttype'])) {
                    $paymenttype = implode(',', array_slice($params ['paymenttype'], 0, count($params ['paymenttype']) - 1));
                    unset($params ['paymenttype']);
                }
                if (isset($params ['cms'])) {
                    $cms = implode(',', array_slice($params ['cms'], 0, count($params ['cms']) - 1));
                    unset($params ['cms']);
                }
                if (isset($params ['shops'])) {
                    $shops = implode(',', array_slice($params ['shops'], 0, count($params ['shops']) - 1));
                    unset($params ['shops']);
                }
                if (isset($params ['groups'])) {
                    $groups = implode(',', array_slice($params ['groups'], 0, count($params ['groups']) - 1));
                    unset($params ['groups']);
                }
                unset($params ['config']);
                unset($params ['save']);
                unset($params ['module']);
                unset($params ['controller']);
                unset($params ['action']);
                unset($params['headingsize']);
                $shopsid = $params ['sid'];

                unset($params ['sid']);
                unset($params ['uid']);

                if(isset($params['password']) && $params['password'] != '') {
                    $params['password'] = password_hash($params['password'], PASSWORD_DEFAULT);
                }

                $data = new Contact ();
                $data->setKundenNr($this->_request->getParam('kundenNr'));
                $data->Install = $user->Install;
                if (isset($params ['enable']) && $params ['enable'] == 'on') {
                    $data->enable = true;
                } else {
                    $data->enable = false;
                }
                unset($params ['enable']);
                if (isset($params ['newsletter']) && $params ['newsletter'] == 'on') {
                    $data->newsletter = true;
                } else {
                    $data->newsletter = false;
                }
                unset($params ['newsletter']);
                if (isset($params ['liefer']) && $params ['liefer'] == 'on') {
                    $data->liefer = true;
                } else {
                    $data->liefer = false;
                }
                unset($params ['liefer']);
                if (isset($params ['mwert']) && $params ['mwert'] == 'on') {
                    $data->mwert = true;
                } else {
                    $data->mwert = false;
                }
                unset($params ['mwert']);
                foreach ($params as $key => $value) {

                    if (isset($data [$key]) && false == strpos($value, 'service/index?type=upload&mode=get')) {
                        if ($value == 'true' || $value == 'on') {
                            $data->$key = true;
                        } elseif ($value == 'false' || $value == 'off') {
                            $data->$key = false;
                        } else {
                            $data->$key = $value;
                        }
                    }
                }

                $data->save();

                if (isset($articles)) {
                    Doctrine_Query::create()->delete()->from('ContactArticle as a')->where('a.contact_id = ?', array(intval($data->id)))->execute();

                    if ($articles != '') {
                        foreach (explode(',', $articles) as $key) {
                            $group = new ContactArticle ();
                            $group->contact_id = $data->id;
                            $group->article_id = $key;
                            $group->save();
                        }
                    }
                }
                if (isset($accounts)) {

                    Doctrine_Query::create()->delete()->from('ContactAccount as a')->where('a.contact_id = ?', array(intval($data->id)))->execute();

                    if ($accounts != '') {
                        foreach (explode(',', $accounts) as $key) {
                            $group = new ContactAccount ();
                            $group->contact_id = $data->id;
                            $group->account_id = $key;
                            $group->save();
                        }
                    }
                }
                if (isset($shops)) {

                    Doctrine_Query::create()->delete()->from('ShopContact as a')->where('a.contact_id = ?', array(intval($data->id)))->execute();

                    if ($shops != '') {
                        foreach (explode(',', $shops) as $key) {
                            $group = new ShopContact ();
                            $group->contact_id = $data->id;
                            $group->shop_id = $key;
                            $group->save();
                        }
                    }
                } else {
                    $group = new ShopContact ();
                    $group->contact_id = $data->id;
                    $group->shop_id = intval($this->_request->getParam('sid'));
                    $group->save();
                }
                if (isset($groups)) {

                    Doctrine_Query::create()->delete()->from('ContactRole as a')->where('a.contact_id = ?', array(intval($data->id)))->execute();

                    if ($groups != '') {
                        foreach (explode(',', $groups) as $key) {
                            $group = new ContactRole ();
                            $group->contact_id = $data->id;
                            $group->role_id = $key;
                            $group->save();
                        }
                    }
                }
                if (isset($paymenttype)) {

                    Doctrine_Query::create()->delete()->from('ContactPaymenttype as a')->where('a.contact_id = ?', array(intval($data->id)))->execute();

                    if ($paymenttype != '') {
                        foreach (explode(',', $paymenttype) as $key) {
                            $group = new ContactPaymenttype ();
                            $group->contact_id = $data->id;
                            $group->paymenttype_id = $key;
                            $group->save();
                        }
                    }
                }
                if (isset($cms)) {

                    Doctrine_Query::create()->delete()->from('ContactCms as c')->where('c.contact_id = ?', array(intval($data->id)))->execute();

                    if ($cms != '') {
                        foreach (explode(',', $cms) as $key) {
                            $group = new ContactCms ();
                            $group->contact_id = $data->id;
                            $group->cms_id = $key;
                            $group->save();
                        }
                    }
                }
                $isNew = true;
                $id = $data->id;
            }

            if ($data != false) {

                $temp = array();
                foreach ($data->ContactArticle as $row) {
                    array_push($temp, $row->article_id);
                }
                $tempAccount = array();
                foreach ($data->ContactAccount as $row) {
                    array_push($tempAccount, $row->account_id);
                }
                $temps = array();
                foreach ($data->ContactPaymenttype as $row) {
                    array_push($temps, $row->paymenttype_id);
                }
                $tempcms = array();
                foreach ($data->ContactCms as $row) {
                    array_push($tempcms, $row->cms_id);
                }
                $tempshops = array();
                foreach ($data->Shops as $row) {
                    array_push($tempshops, $row->id);
                }
                $tempgroup = array();
                foreach ($data->ContactRole as $row) {
                    array_push($tempgroup, $row->role_id);
                }
                $account = $data->Account->company." ".$data->Account->appendix;
                $anrede = $data->getAnrede();

                $kundenNr = $data->getKundenNr();
                $data = $data->toArray(false);
                $data['kundenNr'] = $kundenNr;

                if ($data['production_status']) {
                    $config = new Zend_Config_Ini(APPLICATION_PATH . '/configs/' . '/systemdefaults.ini', 'status');

                    $translate = Zend_Registry::get('translate');

                    $temp = array();

                    $status = $config->toArray();

                    $data['production_status_title'] = $translate->translate($data['production_status']);

                }

                $data ['articles[]'] = implode(',', $temp);
                $data ['accounts[]'] = implode(',', $tempAccount);
                $data ['paymenttype[]'] = implode(',', $temps);
                $data ['cms[]'] = implode(',', $tempcms);
                $data ['shops[]'] = implode(',', $tempshops);
                $data ['groups[]'] = implode(',', $tempgroup);
                $data ['self_anrede_title'] = $anrede;

                $data ['account'] = $account;
            } else {
                $data = array();
            }
            array_push($fieldkeys, 'self_anrede');
            $data = TP_Util::cleanArray($data, $fieldkeys);

            $config = array("success" => true, "isNew" => $isNew, "uid" => $id, "metaData" => array("fields" => $fieldsarray, "formConfig" => $configarray, "data" => $data), "data" => $data);

            $this->getHelper('Json')->sendJson($config);
        }

        $this->_helper->layout->setLayout('popupjs');
    }

}
