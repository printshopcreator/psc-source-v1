<?php

class Admin_CreditsystemController extends TP_Admin_Controller_Action
{

    public function init() {
        parent::init();

        $contextSwitch = $this->_helper->contextSwitch();

        $contextSwitch
            ->addActionContext('fetchcredit', self::CONTEXT_JSON)
            ->addActionContext('loadsavecredit', self::CONTEXT_JSON)
            ->addActionContext('deletecredit', self::CONTEXT_JSON)
            ->addActionContext('exportcsv', self::CONTEXT_JSON)
            ->setAutoJsonSerialization(true)
            ->initContext();
    }

    public function exportcsvAction() {

        $csvExportClass = new Admin_Model_Export_Csv_CreditSystemDetail();

        $creditSystem = Doctrine_Query::create()
            ->from('CreditSystem a')
            ->where('a.id = ? AND a.shop_id IN (' . implode(',', $this->shopIds) . ')', array($this->_request->getParam('uid')))
            ->fetchOne();

        if ($creditSystem == false) {
            die('NOT ALLOWED');
        }

        $result = $csvExportClass->exportContacts($creditSystem);

        header("Pragma: public");
        header("Cache-Control: max-age=0");
        header("Content-Disposition: attachment; filename=credits_export.csv");
        header("Content-Description: csv File");
        header("Content-type: text/csv");
        readfile($result);
        exit;
    }

    public function allAction() {

    }

    public function addcreditAction() {
        $this->_helper->layout->setLayout('popupjs');
    }

    public function fetchcreditsAction() {
        $this->view->success = true;
    }

    public function deletecreditAction() {

        if (intval($this->_request->getParam('delete')) == 1) {

            Doctrine_Query::create()
                ->from('CreditSystem a')
                ->where('a.id = ? AND a.shop_id IN (' . implode(',', $this->shopIds) . ')', array($this->_request->getParam('uid')))
                ->delete()
                ->execute();

            $this->view->success = true;
        }

    }

    public function fetchcreditAction() {
        $rows = Doctrine_Query::create()
            ->select('m.*')
            ->from('CreditSystem m')
            ->limit(intval($this->_request->getParam('limit')))
            ->offset(intval($this->_request->getParam('start')));
        $rows->where('m.shop_id = ? AND m.shop_id IN (' . implode(',', $this->shopIds) . ')', array(intval(str_replace('S', '', $this->_request->getParam('sid')))));

        if ($this->_request->getParam('sort')) {
            $rows->orderBy($this->_request->getParam('sort') . ' ' . $this->_request->getParam('dir'));
        }

        $temp = array();
        foreach ($rows->execute() as $row) {
            $r = $row->toArray();

            $rows = Doctrine_Query::create()
                ->select('COUNT(*) as used')
                ->from('CreditSystemDetail m')
                ->where('m.creditsystem_id = ? AND m.used = 1', array($row->id))->fetchOne();

            $rowsgen = Doctrine_Query::create()
                ->select('COUNT(*) as used')
                ->from('CreditSystemDetail m')
                ->where('m.creditsystem_id = ?', array($row->id))->fetchOne();

            $r['used'] = $rows->used;
            $r['generated'] = $rowsgen->used;
            array_push($temp, $r);
        }

        $count = Doctrine_Query::create()
            ->from('CreditSystem m');
        if (intval(str_replace('S', '', $this->_request->getParam('sid'))) != 0) {
            $count->where('shop_id = ?', array(intval(str_replace('S', '', $this->_request->getParam('sid')))));
        }

        $count = $count->execute();

        $this->view->success = true;
        $this->view->totalCount = $count->count();
        $this->view->rows = $temp;

    }

    public function loadsavecreditAction() {

        $isNew = false;
        $id = 0;

        $translate = Zend_Registry::get('translate');

        $user = Zend_Auth::getInstance()->getIdentity();
        $user = Doctrine_Query::create()
            ->from('Contact m')
            ->where('id = ?')->fetchOne(array($user['id']));

        $role = null;
        foreach ($user->Roles as $rol) {
            if ($role == null || $rol->level > $role->level) {
                $role = $rol;
            }
        }
        $config = new Zend_Config_Ini(APPLICATION_PATH . '/modules/admin/config/creditsystem/creditsettings.ini', $role->name);

        $fieldsarray = array();
        foreach ($config->tabs as $tab) {
            $tab = $tab->toArray();
            $fields = array();
            foreach ($tab['fields'] as $key => $field) {
                $field['fieldLabel'] = $translate->translate($field['fieldLabel']);
                array_push($fields, $field);
            }

            array_push($fieldsarray, array('general' => $tab['general'], 'fields' => $fields));
        }

        $configarray = $config->general->toArray();
        $configarray['title'] = $translate->translate($configarray['title']);

        $data = array();

        if (intval($this->_request->getParam('uid')) != 0) {

            $data = Doctrine_Query::create()
                ->from('CreditSystem a')
                ->where('a.id = ? AND a.shop_id IN (' . implode(',', $this->shopIds) . ')')->fetchOne(array(intval($this->_request->getParam('uid'))));

        }

        if (intval($this->_request->getParam('save')) == 1 && intval($this->_request->getParam('uid')) > 0) {

            $params = TP_Util::clearParams($this->_request->getParams());

            foreach ($params as $key => $value) $data->$key = stripslashes($value);

            $data->save();

        } elseif (intval($this->_request->getParam('save')) == 1 && intval($this->_request->getParam('uid')) == 0) {

            $params = TP_Util::clearParams($this->_request->getParams());

            $data = new CreditSystem();
            $data->shop_id = intval($this->_request->getParam('sid'));
            $data->Install = $user->Install;

            foreach ($params as $key => $value) $data->$key = $value;

            $data->save();

            $isNew = true;
            $id = $data->id;
        }

        $this->view->success = true;
        $this->view->uid = $id;
        $this->view->isNew = $isNew;
        $this->view->metaData = array(
            "fields" => $fieldsarray
        , "formConfig" => $configarray
        , "data" => ($data != false) ? $data->toArray() : array()
        );

        $this->view->data = ($data != false) ? $data->toArray() : array();

    }

    public function generateAction() {

        if (intval($this->_request->getParam('generate')) == 1) {

            $creditsystem = Doctrine_Query::create()
                ->from('CreditSystem a')
                ->where('a.id = ? AND a.shop_id IN (' . implode(',', $this->shopIds) . ')', array($this->_request->getParam('uid')))
                ->fetchOne();

            for ($i = 1; $i <= $creditsystem->count; $i++) {

                $credit = new CreditSystemDetail();
                $credit->CreditSystem = $creditsystem; #
                $credit->Shop = $this->shop;
                $credit->Install = $this->shop->Install;
                if ($creditsystem->more == true & $creditsystem->count == 1 && $creditsystem->pre_code != "") {
                    $credit->uuid = $creditsystem->pre_code;
                } else {
                    $credit->uuid = $creditsystem->pre_code . TP_Util::uuidCredit();
                }
                $credit->used = false;

                $credit->save();
            }

            $this->view->success = true;
        }
    }
}