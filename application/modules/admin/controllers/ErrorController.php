<?php

class ErrorController extends Zend_Controller_Action
{

    /**
     * ErrorAction
     *
     * @todo Dokumentation der ErrorAction
     */
    public function errorAction() {
        $error = $this->_getParam('error_handler');

        /**
         * Layout ausschalten und evt. schon gerenderten Body entfernen.
         *
         * Der Grund ist, das schon gerenderter Inhalt erhalten bleibt. Dies ist
         * innerhalb eines View Scripts das eine Exception auswirft sehr
         * unangenehm.
         */
        $this->getHelper('Layout')->getLayoutInstance()->disableLayout();
        $this->getResponse()->clearBody();

        switch ($error->type) {
            case Zend_Controller_Plugin_ErrorHandler::EXCEPTION_NO_CONTROLLER:
            case Zend_Controller_Plugin_ErrorHandler::EXCEPTION_NO_ACTION:
            case Zend_Controller_Plugin_ErrorHandler::EXCEPTION_OTHER:
                $this->getResponse()->setHttpResponseCode(404);
                $this->getResponse()->setRawHeader('HTTP/1.1 404 Not Found');
                $errorType = 404;
                $errorFile = realpath(APPLICATION_PATH . '/layouts/') . '/error404.phtml';
                # 404 Error Handling
                break;
            default:
                # Generelles Error Handling
                $this->getResponse()->setHttpResponseCode(500);
                $this->getResponse()->setRawHeader('HTTP/1.1 500 Internal Server Error');
                $errorType = 500;
                $errorFile = realpath(APPLICATION_PATH . '/layouts/') . '/error500.phtml';
                break;
        }

        #Use XMPPHP_Log::LEVEL_VERBOSE to get more logging for error reports
        #If this doesn't work, are you running 64-bit PHP with < 5.2.6?
        /*$conn = new XMPPHP_XMPP('localhost', 5222, 'admin', '38412914', 'xmpphp', 'localhost', $printlog=false, $loglevel=XMPPHP_Log::LEVEL_INFO);

        try {
            $conn->connect();
            $conn->processUntil('session_start');
            $conn->presence();
            $conn->message('bot@boonkerz-7', 'psc log ' . $this->shop->Domain[0]->name . ' ' . print_r($errors->exception->getMessage(),true));
            $conn->disconnect();
        } catch(XMPPHP_Exception $e) {
            //die($e->getMessage());
        }*/

        $this->view->errorType = $errorType;
        $this->view->errorFile = $errorFile;
        $this->view->errorException = $error->exception;
    }
}
