<?php

class Admin_FileController extends TP_Admin_Controller_Action
{

    public function allAction() {

        $this->view->sid = intval(str_replace('S', '', $this->_request->getParam('id')));

        $conf = Doctrine_Query::create()
            ->from('Shop s')
            ->where('s.id = ?')->fetchOne(array($this->view->sid));

        if ($conf == false) {
            die();
        }

        if ($this->getRequest()->getParam('path', false)) {
            if (!strpos($this->getRequest()->getParam('path'), 'Root')) {
                die(strpos($this->getRequest()->getParam('path'), '/Root'));
            }
        }

        $basePathTemplates = APPLICATION_PATH . '/design/clients/' . $conf['uid'] . '/' . $conf['layout'] . '/';
        $basePathReports = APPLICATION_PATH . '/design/clients/' . $conf['uid'] . '/reports/';
        $basePathImages = PUBLIC_PATH . '/shops/' . $conf['uid'] . '/';
        $basePathUploads = PUBLIC_PATH . '/uploads/' . $conf['uid'] . '/';

        if (!file_exists($basePathImages)) {
            mkdir($basePathImages, 0777, true);
        }

        $basePathStyles = PUBLIC_PATH . '/styles/' . $conf['uid'] . '/design/' . $conf['layout'] . '/';

        switch ($this->getRequest()->getParam('mode')) {

            case 'get-folders':
                $requestedPath = $this->getRequest()->getParam('path');
                $dirs = array();

                if ($requestedPath == '/Root') {

                    $dirs['Shop'] = array('text' => 'Shop', 'leaf' => false);
                    $dirs['Reports'] = array('text' => 'Reports', 'leaf' => false);
                    $dirs['Uploads'] = array('text' => 'Uploads', 'leaf' => false);

                    $this->getHelper('layout')->disableLayout();
                    $this->_helper->viewRenderer->setNoRender();
                    die(Zend_Json::encode(array_values($dirs)));
                }

                $browsePath = preg_replace('/^\/Root\/Templates\/?/', $basePathTemplates, $requestedPath) . '/';
                $browsePath = preg_replace('/^\/Root\/Styles\/?/', $basePathStyles, $browsePath) . '/';
                $browsePath = preg_replace('/^\/Root\/Shop\/?/', $basePathImages, $browsePath) . '/';
                $browsePath = preg_replace('/^\/Root\/Reports\/?/', $basePathReports, $browsePath) . '/';
                $browsePath = preg_replace('/^\/Root\/Uploads\/?/', $basePathUploads, $browsePath) . '/';
                $browsePath = preg_replace('/^\/Root\/?/', $basePathTemplates, $browsePath) . '/';

                $iterator = new DirectoryIterator($browsePath);

                foreach ($iterator as $item) {
                    if (!$item->isDot() && $item->isDir() && $item != '.svn') {
                        $dirs[(string)$item] = array(
                            'text' => (string)$item,
                            'leaf' => false
                        );
                    }
                }

                uksort($dirs, 'strcasecmp');
                $success = true;
                $data = array_values($dirs);
                break;

            case 'get-files':
                $requestedPath = $this->getRequest()->getParam('path');
                $files = array();
                if ($requestedPath == '/Root') {
                    die(Zend_Json::encode(array_values($files)));
                }

                $browsePath = preg_replace('/^\/Root\/Templates\/?/', $basePathTemplates, $requestedPath) . '/';
                $browsePath = preg_replace('/^\/Root\/Styles\/?/', $basePathStyles, $browsePath) . '/';
                $browsePath = preg_replace('/^\/Root\/Shop\/?/', $basePathImages, $browsePath) . '/';
                $browsePath = preg_replace('/^\/Root\/Reports\/?/', $basePathReports, $browsePath) . '/';
                $browsePath = preg_replace('/^\/Root\/Uploads\/?/', $basePathUploads, $browsePath) . '/';
                $browsePath = preg_replace('/^\/Root\/?/', $basePathTemplates, $browsePath) . '/';
                $rowClasses = array(
                    '7z' => 'ux-filebrowser-icon-archive-file',
                    'aac' => 'ux-filebrowser-icon-audio-file',
                    'ai' => 'ux-filebrowser-icon-vector-file',
                    'avi' => 'ux-filebrowser-icon-video-file',
                    'bmp' => 'ux-filebrowser-icon-image-file',
                    'divx' => 'ux-filebrowser-icon-video-file',
                    'doc' => 'ux-filebrowser-icon-document-file',
                    'eps' => 'ux-filebrowser-icon-vector-file',
                    'flac' => 'ux-filebrowser-icon-audio-file',
                    'flv' => 'ux-filebrowser-icon-video-file',
                    'gif' => 'ux-filebrowser-icon-image-file',
                    'jpg' => 'ux-filebrowser-icon-image-file',
                    'mov' => 'ux-filebrowser-icon-video-file',
                    'mp3' => 'ux-filebrowser-icon-audio-file',
                    'mpg' => 'ux-filebrowser-icon-video-file',
                    'pdf' => 'ux-filebrowser-icon-acrobat-file',
                    'png' => 'ux-filebrowser-icon-image-file',
                    'pps' => 'ux-filebrowser-icon-presentation-file',
                    'ppt' => 'ux-filebrowser-icon-presentation-file',
                    'rar' => 'ux-filebrowser-icon-archive-file',
                    'psd' => 'ux-filebrowser-icon-image-file',
                    'svg' => 'ux-filebrowser-icon-vector-file',
                    'swf' => 'ux-filebrowser-icon-flash-file',
                    'tif' => 'ux-filebrowser-icon-image-file',
                    'txt' => 'ux-filebrowser-icon-text-file',
                    'wav' => 'ux-filebrowser-icon-audio-file',
                    'wma' => 'ux-filebrowser-icon-video-file',
                    'xls' => 'ux-filebrowser-icon-spreadsheet-file',
                    'zip' => 'ux-filebrowser-icon-archive-file'
                );

                $iterator = new DirectoryIterator($browsePath);
                foreach ($iterator as $item) {
                    if ($item->isFile()) {

                        $url = "";
                        if (strpos($browsePath, 'public/shops')) {
                            $url = '/shops/' . $conf['uid'] . '/' . str_replace('/Root/Shop/', '', $requestedPath) . '/' . (string)$item;
                        }

                        $files[(string)$item] = array(
                            'name' => (string)$item,
                            'size' => $item->getSize(),
                            'type' => $item->getType(),
                            'url' => $url,
                            'date_modified' => $item->getMTime()
                        );

                        $ext = pathinfo($item->getFilename(), PATHINFO_EXTENSION);
                        $thumb = 'file_' . $ext . '.png';
                        if (file_exists('media/icons/48/' . $thumb) == true) {
                            $files[(string)$item]['thumbnail'] = 'media/icons/48/' . $thumb;
                        }

                        if (array_key_exists($ext, $rowClasses) == true) {
                            $files[(string)$item]['row_class'] = $rowClasses[$ext];
                        }
                    }
                }

                uksort($files, 'strcasecmp');
                $success = true;
                $data = array(
                    'success' => $success,
                    'data' => array_values($files)
                );
                break;

            case 'delete-file':
                $files = $_POST['files'];

                $success = true;
                $failCount = 0;
                $message = '';
                $data = array(
                    'successful' => array(),
                    'failed' => array()
                );

                foreach ($files as $recordId => $file) {

                    $filePath = preg_replace('/^\/Root\/Templates\/?/', $basePathTemplates, $file);
                    $filePath = preg_replace('/^\/Root\/Styles\/?/', $basePathStyles, $filePath);
                    $filePath = preg_replace('/^\/Root\/Shop\/?/', $basePathImages, $filePath);
                    $filePath = preg_replace('/^\/Root\/Reports\/?/', $basePathReports, $filePath);
                    $filePath = preg_replace('/^\/Root\/Uploads\/?/', $basePathUploads, $filePath);
                    $filePath = preg_replace('/^\/Root\/?/', $basePathTemplates, $filePath);
                    try {
                        if (file_exists($filePath) == false) {
                            throw new Exception("The file '" . basename($filePath) . "' does not exist");
                        }
                        if (is_file($filePath) == false) {
                            throw new Exception("'" . htmlspecialchars($filePath) . "' is not a file");
                        }
                        if (is_writable($filePath) == false) {
                            throw new Exception("You do not have permission to delete '" . basename($filePath) . "'");
                        }
                        if (@unlink($filePath) == false) {
                            throw new Exception("Could not delete '" . basename($filePath) . "' for unknow reason");
                        } else {
                            $data['successful'][] = array(
                                'recordId' => $recordId
                            );
                        }
                    } catch (Exception $e) {
                        $success = false;
                        $failCount++;
                        $data['failed'][] = array(
                            'recordId' => $recordId,
                            'reason' => $e->getMessage()
                        );
                    }
                }

                if ($failCount > 0) {
                    $message = 'Failed to delete ' . (integer)$failCount . ' file(s)';
                }

                $data = array(
                    'success' => $success,
                    'message' => $message,
                    'data' => $data
                );
                break;

            case 'rename-file':
                $requestedPath = $_POST['path'] . '/';
                $oldName = $_POST['oldName'];
                $newName = $_POST['newName'];
                $renamePath = preg_replace('/^\/Root\/Templates\/?/', $basePathTemplates, $requestedPath);
                $renamePath = preg_replace('/^\/Root\/Styles\/?/', $basePathStyles, $renamePath);
                $renamePath = preg_replace('/^\/Root\/Shop\/?/', $basePathImages, $renamePath);
                $renamePath = preg_replace('/^\/Root\/Reports\/?/', $basePathReports, $renamePath);
                $renamePath = preg_replace('/^\/Root\/Uploads\/?/', $basePathUploads, $renamePath);
                $renamePath = preg_replace('/^\/Root\/?/', $basePathTemplates, $renamePath);
                $success = true;
                $message = '';

                try {
                    if (file_exists($renamePath . $oldName) == false) {
                        throw new Exception("The file '" . htmlspecialchars($renamePath . $oldName) . "' does not exist");
                    }
                    if (is_file($renamePath . $oldName) == false) {
                        throw new Exception("'" . htmlspecialchars($oldName) . "' is not a file");
                    }
                    if (is_writable($renamePath . $oldName) == false) {
                        throw new Exception("You do not have permission to rename '" . htmlspecialchars($oldName) . "'");
                    }
                    if (file_exists($renamePath . $newName) == true) {
                        throw new Exception("Could not rename '" . htmlspecialchars($oldName) . "'. A file or folder with the specified name already exists");
                    }
                    if (@rename($renamePath . $oldName, $renamePath . $newName) == false) {
                        throw new Exception("Could not rename '" . htmlspecialchars($oldName) . "' for unknow reason");
                    }
                } catch (Exception $e) {
                    $success = false;
                    $message = $e->getMessage();
                }

                $data = array(
                    'success' => $success,
                    'message' => $message
                );
                break;

            case 'download-file':
                $requestedPath = $this->getRequest()->getParam('path');
                if (strpos($requestedPath, 'Root') == false) {
                    die('NOT ALLOWED');
                }
                $downloadPath = preg_replace('/^\/Root\/Templates\/?/', $basePathTemplates, $requestedPath);
                $downloadPath = preg_replace('/^\/Root\/Styles\/?/', $basePathStyles, $downloadPath);
                $downloadPath = preg_replace('/^\/Root\/Shop\/?/', $basePathImages, $downloadPath);
                $downloadPath = preg_replace('/^\/Root\/Reports\/?/', $basePathReports, $downloadPath);
                $downloadPath = preg_replace('/^\/Root\/Uploads\/?/', $basePathUploads, $downloadPath);
                $downloadPath = preg_replace('/^\/Root\/?/', $basePathTemplates, $downloadPath);
                try {
                    if (file_exists($downloadPath) == false) {
                        throw new Exception("The file '" . basename($downloadPath) . "' does not exist");
                    }
                    if (is_file($downloadPath) == false) {
                        throw new Exception("'" . htmlspecialchars($requestedPath) . "' is not a file");
                    }
                    if (is_readable($downloadPath) == false) {
                        throw new Exception("You do not have permission to download '" . basename($downloadPath) . "'", 1);
                    }

                    header('Pragma: public');
                    header('Expires: 0');
                    header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
                    header('Content-Description: File Transfer');
                    header('Content-Type: application/force-download');
                    header('Content-Disposition: attachment; filename="' . basename($downloadPath) . '"');
                    header('Content-Transfer-Encoding: binary');
                    header('Content-Length: ' . filesize($downloadPath));

                    readfile($downloadPath);
                } catch (Exception $e) {
                    if ($e->getCode() === 1) {
                        header('HTTP/1.1 403 Forbidden');
                    } else {
                        header('HTTP/1.1 404 Not Found');
                    }
                    echo $e->getMessage();
                }

                exit;
                break;

            case 'move-file':

                $sourcePath = $_POST['sourcePath'];
                $destinationPath = $_POST['destinationPath'];
                $files = $_POST['files'];
                $overwrite = $_POST['overwrite'];
                $overwrite = ($overwrite === 'true') ? true : false;

                $sourceFolder = preg_replace('/^\/Root\/Templates\/?/', $basePathTemplates, $sourcePath) . '/';
                $sourceFolder = preg_replace('/^\/Root\/Styles\/?/', $basePathStyles, $sourceFolder) . '/';
                $sourceFolder = preg_replace('/^\/Root\/Shop\/?/', $basePathImages, $sourceFolder) . '/';
                $sourceFolder = preg_replace('/^\/Root\/Reports\/?/', $basePathReports, $sourceFolder) . '/';
                $sourceFolder = preg_replace('/^\/Root\/Uploads\/?/', $basePathUploads, $sourceFolder) . '/';
                $sourceFolder = preg_replace('/^\/Root\/?/', $basePathTemplates, $sourceFolder) . '/';

                $destinationFolder = preg_replace('/^\/Root\/Templates\/?/', $basePathTemplates, $destinationPath) . '/';
                $destinationFolder = preg_replace('/^\/Root\/Styles\/?/', $basePathStyles, $destinationFolder) . '/';
                $destinationFolder = preg_replace('/^\/Root\/Shop\/?/', $basePathImages, $destinationFolder) . '/';
                $destinationFolder = preg_replace('/^\/Root\/Reports\/?/', $basePathReports, $destinationFolder) . '/';
                $destinationFolder = preg_replace('/^\/Root\/Uploads\/?/', $basePathUploads, $destinationFolder) . '/';
                $destinationFolder = preg_replace('/^\/Root\/?/', $basePathTemplates, $destinationFolder) . '/';

                $success = true;
                $failCount = 0;
                $message = '';
                $data = array(
                    'successful' => array(),
                    'failed' => array(),
                    'existing' => array()
                );

                foreach ($files as $recordId => $fileName) {

                    $from = $sourceFolder . $fileName;
                    $to = $destinationFolder . $fileName;

                    try {

                        if (file_exists($from) == false) {
                            throw new Exception("The file '" . htmlspecialchars($fileName) . "' does not exist");
                        }
                        if (is_file($from) == false) {
                            throw new Exception("'" . htmlspecialchars($fileName) . "' is not a file");
                        }
                        if (is_readable($from) == false) {
                            throw new Exception("'" . htmlspecialchars($fileName) . "' is not readable");
                        }

                        if (file_exists($destinationFolder) == false) {
                            throw new Exception("The destination folder '" . htmlspecialchars($destinationPath) . "' does not exist");
                        }
                        if (is_dir($destinationFolder) == false) {
                            throw new Exception("The destination folder '" . htmlspecialchars($destinationPath) . "' is not a directory");
                        }
                        if (is_writable($destinationFolder) == false) {
                            throw new Exception("You do not have permission to move files to '" . htmlspecialchars($destinationPath) . "'");
                        }

                        if ($overwrite === true) {
                            if (file_exists($to) == true && is_writable($to) == false) {
                                throw new Exception("You do not have permission to overwrite '" . htmlspecialchars($fileName) . "'");
                            }
                        } else {
                            if (file_exists($to) == true) {
                                throw new Exception("Could not move '" . htmlspecialchars($fileName) . "'. A file with the same name in the destination folder already exists", 1);
                            }
                        }

                        if (@rename($from, $to) == false) {
                            throw new Exception("Could not move '" . htmlspecialchars($fileName) . "' for unknown reason");
                        } else {
                            $data['successful'][] = array(
                                'recordId' => $recordId
                            );
                        }

                    } catch (Exception $e) {
                        $success = false;
                        $failCount++;

                        if ($e->getCode() === 1) {
                            $data['existing'][] = array(
                                'recordId' => $recordId,
                                'reason' => $e->getMessage()
                            );
                        } else {
                            $data['failed'][] = array(
                                'recordId' => $recordId,
                                'reason' => $e->getMessage()
                            );
                        }
                    }
                }

                if ($failCount > 0) {
                    $message = 'Failed to move ' . (integer)$failCount . ' file(s)';
                }

                $data = array(
                    'success' => $success,
                    'message' => $message,
                    'data' => $data
                );
                break;

            case 'create-folder':
                $requestedPath = $_POST['path'];
                $createPath = preg_replace('/^\/Root\/Templates\/?/', $basePathTemplates, $requestedPath);
                $createPath = preg_replace('/^\/Root\/Styles\/?/', $basePathStyles, $createPath);
                $createPath = preg_replace('/^\/Root\/Reports\/?/', $basePathReports, $createPath);
                $createPath = preg_replace('/^\/Root\/Shop\/?/', $basePathImages, $createPath);
                $createPath = preg_replace('/^\/Root\/Uploads\/?/', $basePathUploads, $createPath);
                $createPath = preg_replace('/^\/Root\/?/', $basePathTemplates, $createPath);
                $success = true;
                $message = '';

                try {
                    $parentPath = $createPath . '/../';

                    if (@mkdir($createPath, 0777, true) == false) {
                        throw new Exception("Could not create '" . htmlspecialchars($createPath) . "' for unknow reason");
                    }
                } catch (Exception $e) {
                    $success = false;
                    $message = $e->getMessage();
                }

                $data = array(
                    'success' => $success,
                    'message' => $message
                );
                break;

            case 'rename-folder':
                $requestedPath = $_POST['path'];
                $oldName = $_POST['oldName'];
                $newName = $_POST['newName'];
                $renamePath = preg_replace('/^\/Root\/Templates\/?/', $basePathTemplates, $requestedPath) . '/';
                $renamePath = preg_replace('/^\/Root\/Styles\/?/', $basePathStyles, $renamePath) . '/';
                $renamePath = preg_replace('/^\/Root\/Shop\/?/', $basePathImages, $renamePath) . '/';
                $renamePath = preg_replace('/^\/Root\/Uploads\/?/', $basePathUploads, $renamePath) . '/';
                $renamePath = preg_replace('/^\/Root\/Reports\/?/', $basePathReports, $renamePath) . '/';
                $renamePath = preg_replace('/^\/Root\/?/', $basePathTemplates, $renamePath) . '/';
                try {
                    if (file_exists($renamePath . $oldName) == false) {
                        throw new Exception("The folder '" . htmlspecialchars($oldName) . "' does not exist");
                    }
                    if (is_dir($renamePath . $oldName) == false) {
                        throw new Exception("'" . htmlspecialchars($oldName) . "' is not a directory");
                    }
                    if (is_writable($renamePath . $oldName) == false) {
                        throw new Exception("You do not have permission to rename '" . htmlspecialchars($oldName) . "'");
                    }
                    if (file_exists($renamePath . $newName) == true) {
                        throw new Exception("Could not rename '" . htmlspecialchars($oldName) . "'. A file or folder with the specified name already exists");
                    }
                    if (@rename($renamePath . $oldName, $renamePath . $newName) == false) {
                        throw new Exception("Could not rename '" . htmlspecialchars($oldName) . "' for unknow reason");
                    }
                } catch (Exception $e) {
                    $success = false;
                    $message = $e->getMessage();
                }

                $data = array(
                    'success' => $success,
                    'message' => $message
                );
                break;

            case 'delete-folder':
                $requestedPath = $_POST['path'];

                $deletePath = preg_replace('/^\/Root\/Templates\/?/', $basePathTemplates, $requestedPath);
                $deletePath = preg_replace('/^\/Root\/Styles\/?/', $basePathStyles, $deletePath);
                $deletePath = preg_replace('/^\/Root\/Shop\/?/', $basePathImages, $deletePath);
                $deletePath = preg_replace('/^\/Root\/Reports\/?/', $basePathReports, $deletePath);
                $deletePath = preg_replace('/^\/Root\/Uploads\/?/', $basePathUploads, $deletePath);
                $deletePath = preg_replace('/^\/Root\/?/', $basePathTemplates, $deletePath);
                try {

                    if (@rmdir($deletePath) == false) {
                        throw new Exception("Could not delete '" . htmlspecialchars($requestedPath) . "' for unknown reason");
                    }
                } catch (Exception $e) {
                    $success = false;
                    $message = $e->getMessage();
                }

                $data = array(
                    'success' => $success,
                    'message' => $message
                );
                break;

            default:
                $this->_helper->layout->setLayout('simple');
                break;
        }

        if (isset($data)) {
            $this->getHelper('layout')->disableLayout();
            $this->_helper->viewRenderer->setNoRender();
            die(Zend_Json::encode($data));
        }
    }

}