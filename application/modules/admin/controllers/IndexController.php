<?php

class Admin_IndexController extends Zend_Controller_Action
{
    /**
     * IndexAction
     *
     * @todo Dokumentation der IndexAction
     *
     * @return void
     */
    public function indexAction() {

        $frontendOptions = array(
            'master_file' => APPLICATION_PATH . 'application/languages/de.php',
        );

        $backendOptions = array(
            'cache_dir' => APPLICATION_PATH . 'cache',
            'file_locking' => true,
            'read_control' => true,
            'read_control_type' => 'crc32',
            'hashed_directory_level' => 0,
            'hashed_directory_umask' => 0700,
            'file_name_prefix' => 'translation_cache',
            'cache_file_umask' => 0600,
            'metadatas_array_max_size' => 100
        );
        $cache = Zend_Cache::factory('File', 'File', $frontendOptions, $backendOptions);
        Zend_Translate::setCache($cache);
        $translate = new Zend_Translate('array', APPLICATION_PATH . 'application/languages/de.php', 'de');
        $translate->addTranslation(APPLICATION_PATH . 'application/languages/en.php', 'en');
        Zend_Registry::set('Zend_Translate', $translate);

        /*$messages = Doctrine_Query::create()
                  ->from('Message m')
                  ->orderBy('m.posted DESC')
                  ->execute();
      $this->view->messages = $messages;*/

    }

    /**
     * CRUD Actions.
     *
     * Erstmal für reine Testzwecke gedacht. Beispiel "MenuModel". Ziel ist es
     * CRUD Operationen zu vereinfachen - solang das Projekt noch am Anfang ist.
     *
     * Generell gilt: Models benehmen sich gleich. Löschen, Editieren,
     * Hinzufügen, Auslesen usw. Die erste Annahme ist: Alles Models sind vom
     * Typ "Zend_Db_Table_Abstract".
     *
     * Ausgehend von dieser Annahme:
     * default/index/crud = liste der "models".
     * default/index/crud/list/<modelName> = liste der rows vom model
     * default/index/crud/add/<modelName> = ein eintrag hinzufügen
     * default/index/crud/delete/<modelName>/pk/<primaryKey> = einen eintrag löschen
     * default/index/crud/edit/<modelName>/pk/<primaryKey> = einen eintrag editieren
     *
     */
    public function crudAction() {
        /**
         * @todo ModelLoader finden/bauen. Evt. den von Müsli?
         */
        require_once realpath(APPLICATION_PATH . 'application/modules/default/models/') . '/PageModel.php';
        $configPage = array(
            'doctype' => PageModel::XHTML_1_1_DocType,
            'contentType' => PageModel::XHTML1_1,
        );

        $pageModel = new PageModel($configPage);

        $this->view->placeholder('Model_Page')->set($pageModel);
    }

}
