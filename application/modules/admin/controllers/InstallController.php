<?php

class Admin_InstallController extends TP_Admin_Controller_Action
{
    /**
     * IndexAction
     *
     * @todo Dokumentation der IndexAction
     *
     * @return void
     */
    public function indexAction() {

    }

    public function settingsAction() {

        if (intval($this->_request->getParam('config')) == 1) {

            $translate = Zend_Registry::get('translate');

            $user = Zend_Auth::getInstance()->getIdentity();
            $user = Doctrine_Query::create()
                ->from('Contact m')
                ->where('id = ?')->fetchOne(array($user['id']));
            $role = null;
            foreach ($user->Roles as $rol) {
                if ($role == null || $rol->level > $role->level) {
                    $role = $rol;
                }
            }
            $config = new Zend_Config_Ini(APPLICATION_PATH . '/modules/admin/config/install/settings.ini', $role->name);

            $fieldsarray = array();
            foreach ($config->tabs as $tab) {
                $tab = $tab->toArray();
                $fields = array();
                foreach ($tab['fields'] as $key => $field) {
                    $field['fieldLabel'] = $translate->translate($field['fieldLabel']);
                    array_push($fields, $field);
                }

                array_push($fieldsarray, array('general' => $tab['general'], 'fields' => $fields));
            }

            $configarray = $config->general->toArray();
            $configarray['title'] = $translate->translate($configarray['title']);

            $data = $user->Install;

            if (intval($this->_request->getParam('save')) == 1) {

                $params = $this->_request->getParams();

                unset($params['config']);
                unset($params['save']);
                unset($params['module']);
                unset($params['controller']);
                unset($params['action']);
                unset($params['sid']);
                unset($params['headingsize']);
                foreach ($params as $key => $value) {
                    if (isset($data[$key]) && false == strpos($value, 'service/index?type=upload&mode=get')) {
                        if ($value == 'true') {
                            $data->$key = true;
                        } elseif ($value == 'false') {
                            $data->$key = false;
                        } else {
                            $data->$key = $value;
                        }
                    }
                }

                $data->save();
            }

            $data->paperdb = str_replace('\\', '', $data->paperdb);
            $data->containerdb = str_replace('\\', '', $data->containerdb);

            $data = $data->toArray();
            $data['offline_custom2_vorgabe'] = '10=new order
20=new request
30=wait for upload
40=files uploaded
50=upload finshed
60=upload in prove
70=upload error
80=preflightcheck error
90=wait for approval one
100=wait for approval all
110=approval accepted
120=approval non-accepted
130=open offer
140=wait for payment
145=payment received
150=downloadable
160=In process
170=canceled
173=produced
175=readyforpickup
180=export to Hotfolder
190=ready for shipping
200=shipped
210=finished
220=request for payment
            ';

            $data['offline_custom3_vorgabe'] = '30=wait for upload
40=files uploaded
50=upload finshed
60=upload in prove
70=upload error
80=preflightcheck error
160=In process
173=produced
175=readyforpickup
180=export to Hotfolder
190=ready for shipping
200=shipped
210=finished
            ';

            $config = array(
                "success" => true
            , "metaData" => array(
                    "fields" => $fieldsarray
                , "formConfig" => $configarray
                , "data" => $data
                )
            , "data" => $data
            );

            $this->getHelper('Json')->sendJson($config);
        }

        $this->view->id = $this->_request->getParam('id');

        $this->_helper->layout->setLayout('simple');

    }

}
