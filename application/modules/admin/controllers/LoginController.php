<?php

class Admin_LoginController extends TP_Admin_Controller_Action
{
    /**
     * IndexAction
     *
     * @todo Dokumentation der IndexAction
     *
     * @return void
     */

    public function indexAction() {
        if (intval($this->_request->getParam('logout')) == 1) {
            Zend_Auth::getInstance()->clearIdentity();
            die(Zend_Json::encode(array('success' => true)));

        }
        $this->_helper->layout->setLayout('login');

    }
}
