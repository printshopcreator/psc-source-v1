<?php

class Admin_MotivController extends TP_Admin_Controller_Action
{

    public function init() {
        parent::init();

        $contextSwitch = $this->_helper->contextSwitch();

        $contextSwitch->addActionContext('fetchmymotive', self::CONTEXT_JSON)
            ->addActionContext('loadsavemotiv', self::CONTEXT_JSON)
            ->setAutoJsonSerialization(true)
            ->initContext();
    }

    public function mymotivAction() {

    }

    public function fetchmymotiveAction() {

        $rows = Doctrine_Query::create()
            ->from('Motiv m')
            ->limit(intval($this->_request->getParam('limit')))
            ->offset(intval($this->_request->getParam('start')))
            ->where('contact_id = ?', array($this->user->id));

        $rows = $rows->execute();

        $count = Doctrine_Query::create()
            ->from('Motiv m')
            ->where('contact_id = ?', array($this->user->id));

        $count = $count->execute();

        $this->view->success = true;
        $this->view->totalCount = $count->count();
        $this->view->rows = $rows->toArray();
    }

    public function editAction() {
        $this->_helper->layout->setLayout('popupjs');
    }

    public function loadsavemotivAction() {

        $isNew = false;
        $id = 0;

        $translate = Zend_Registry::get('translate');

        $user = Zend_Auth::getInstance()->getIdentity();
        $user = Doctrine_Query::create()
            ->from('Contact m')
            ->where('id = ?')->fetchOne(array($user['id']));

        $config = new Zend_Config_Ini(APPLICATION_PATH . '/modules/admin/config/motiv/backend.ini', $this->highRole->name);

        $fieldsarray = array();
        foreach ($config->tabs as $tab) {
            $tab = $tab->toArray();
            $fields = array();
            foreach ($tab['fields'] as $key => $field) {
                $field['fieldLabel'] = $translate->translate($field['fieldLabel']);
                array_push($fields, $field);
            }

            array_push($fieldsarray, array('general' => $tab['general'], 'fields' => $fields));
        }

        $configarray = $config->general->toArray();
        $configarray['title'] = $translate->translate($configarray['title']);

        $data = false;

        if ($this->_request->getParam('uid') != '') {

            $data = Doctrine_Query::create()
                ->from('Motiv a')
                ->where('a.uuid = ? AND (a.shop_id IN (' . implode(',', $this->shopIds) . ') OR a.contact_id = ?)')->fetchOne(array($this->_request->getParam('uid'), $this->user->id));

        }

        if (intval($this->_request->getParam('save')) == 1 && $this->_request->getParam('uid') != '') {

            $params = TP_Util::clearParams($this->_request->getParams());

            if (isset($params['tags'])) {
                $tags = $params['tags'];
            }
            unset($params['tags']);

            if (isset($params['releatedthemes'])) {
                $releatedthemes = implode(',', array_slice($params['releatedthemes'], 0, count($params['releatedthemes']) - 1));
            }
            unset($params['releatedthemes']);

            if (isset($params['releatedthememarket'])) {
                $releatedthemesmarket = implode(',', array_slice($params['releatedthememarket'], 0, count($params['releatedthememarket']) - 1));
            }
            unset($params['releatedthememarket']);

            foreach ($params as $key => $value) $data->$key = stripslashes($value);

            $data->save();

            if (isset($releatedthemes)) {
                Doctrine_Query::create()
                    ->delete()
                    ->from('MotivThemeMotiv as a')
                    ->where('a.motiv_id = ?', array(intval($data->id)))
                    ->execute();

                if ($releatedthemes != '') {
                    foreach (explode(',', $releatedthemes) as $key) {
                        $group = new MotivThemeMotiv();
                        $group->theme_id = $key;
                        $group->motiv_id = $data->id;
                        $group->save();
                    }
                }
            }

            if (isset($releatedthemesmarket)) {
                Doctrine_Query::create()
                    ->delete()
                    ->from('MotivThemeMarketMotiv as a')
                    ->where('a.motiv_id = ?', array(intval($data->id)))
                    ->execute();

                if ($releatedthemesmarket != '') {
                    foreach (explode(',', $releatedthemesmarket) as $key) {
                        $group = new MotivThemeMarketMotiv();
                        $group->theme_id = $key;
                        $group->motiv_id = $data->id;
                        $group->save();
                    }
                }
            }
            if (isset($tags) && trim($tags) != "") {
                $tagsTemp = array();
                $filter = new TP_Filter_Badwords();
                foreach (explode(',', $tags) as $key) {
                    $tag = strtolower(trim($filter->filter($key)));
                    if ($tag) {
                        $tagsTemp[] = array('name' => $tag, 'url' => Doctrine_Inflector::urlize($tag));
                    }
                }

                $m = TP_Mongo::getInstance();
                $collection = $m->motive;
                $collection->save(array_merge($data->getRiakData(), array('tags' => $tagsTemp, '_id' => $data->uuid)));
            }

        } elseif (intval($this->_request->getParam('save')) == 1 && $this->_request->getParam('uid') == '') {

            $params = TP_Util::clearParams($this->_request->getParams());

            if (isset($params['tags'])) {
                $tags = $params['tags'];
            }
            unset($params['tags']);

            $data = new Motiv();
            $data->shop_id = intval($this->_request->getParam('sid'));
            $data->Install = $user->Install;
            $filter = new TP_Filter_Badwords();
            foreach ($params as $key => $value) $data->$key = $filter->filter($value);

            $data->save();

            $isNew = true;
            $id = $data->id;

            if (isset($tags) && trim($tags) != "") {
                $tagsTemp = array();
                $filter = new TP_Filter_Badwords();
                foreach (explode(',', $tags) as $key) {
                    $tag = strtolower(trim($filter->filter($key)));
                    if ($tag) {
                        $tagsTemp[] = array('name' => $tag, 'url' => Doctrine_Inflector::urlize($tag));
                    }
                }

                $m = TP_Mongo::getInstance();
                $collection = $m->motive;
                $collection->save(array_merge($data->getRiakData(), array('tags' => $tagsTemp, '_id' => $data->uuid)));
            }
        }

        if ($this->_request->getParam('uid') != '') {

            $temp2 = array();
            foreach ($data->MotivThemeMotiv as $row) {
                array_push($temp2, $row->theme_id);
            }
            $temp3 = array();
            foreach ($data->MotivThemeMarketMotiv as $row) {
                array_push($temp3, $row->theme_id);
            }

            $data = $data->toArray(false);

            $m = TP_Mongo::getInstance();
            $obj = $m->motive->findOne(array('_id' => $data['uuid']));
            $temp5 = array();
            if ($obj) {
                foreach ($obj['tags'] as $row) {
                    $temp5[] = $row['name'];
                }
                $temp5 = implode(',', $temp5);
            }
            $data['tags'] = $temp5;
            $data['releatedthemes[]'] = implode(',', $temp2);
            $data['releatedthememarket[]'] = implode(',', $temp3);
            $config = new Zend_Config_Ini(APPLICATION_PATH . '/configs/' . '/systemdefaults.ini', 'motivstatus');
            $config = $config->toArray();
            $data['statustitle'] = $translate->translate($config[$data['status']]);
        } else {

            $data = array();

        }

        $this->view->success = true;
        $this->view->uid = $id;
        $this->view->isNew = $isNew;
        $this->view->metaData = array(
            "fields" => $fieldsarray
        , "formConfig" => $configarray
        , "data" => ($data != false) ? $data : array()
        );

        $this->view->data = ($data != false) ? $data : array();

    }

}