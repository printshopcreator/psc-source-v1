<?php

class Admin_MyordersController extends TP_Admin_Controller_Action
{
    /**
     * IndexAction
     *
     * @todo Dokumentation der IndexAction
     *
     * @return void
     */
    public function allAction() {
        if ($this->_request->getParam('config') == 1) {

            $temp = array();
            $user = Zend_Auth::getInstance()->getIdentity();
            $rows = Doctrine_Query::create()
                ->from('Orders m')
                ->limit(intval($this->_request->getParam('limit')))
                ->offset(intval($this->_request->getParam('start')));
            if (intval($this->_request->getParam('account_id')) != 0) {
                $rows->where('account_id = ?', array(intval($this->_request->getParam('account_id'))));
            }

            $rows->where('contact_id = ?', array(intval($user['id'])));

            if ($this->_request->getParam('sort')) {
                $rows->orderBy($this->_request->getParam('sort') . ' ' . $this->_request->getParam('dir'));
            }

            $rows = $rows->execute();

            $configStatus = new Zend_Config_Ini(APPLICATION_PATH . '/configs/' . '/systemdefaults.ini', 'status');

            $translateStatus = Zend_Registry::get('translate');

            foreach ($rows as $row) {

                if ($row->status) {
                    $tempStatus = array();

                    foreach ($configStatus->toArray() as $value) {
                        array_push($tempStatus, array('name' => $value, 'label' => $translateStatus->translate($value)));
                    }

                    $row->status = $tempStatus[($row->status - 1)]['name'];
                }

                array_push($temp, array('id' => $row->id,
                    'account' => $row->Account->company,
                    'contact' => $row->Contact->self_firstname . ' ' . $row->Contact->self_lastname,
                    'status' => $row->status,
                    'alias' => $row->alias,
                    'created' => $row->created));
            }
            $count = Doctrine_Query::create()
                ->from('Orders m');
            if (intval($this->_request->getParam('account_id')) != 0) {
                $count->where('account_id = ?', array(intval($this->_request->getParam('account_id'))));
            }
            if (intval($this->_request->getParam('contact_id')) != 0) {
                $count->where('contact_id = ?', array(intval($this->_request->getParam('contact_id'))));
            }
            $count = $count->execute();

            $config = array(
                "success" => true
            , "totalCount" => $count->count()
            , "rows" => $temp

            );

            $this->getHelper('Json')->sendJson($config);
        }

        $this->_helper->layout->setLayout('simple');

    }

    public function editAction() {

        if (intval($this->_request->getParam('config')) == 3) {

            $translate = Zend_Registry::get('translate');

            $user = Zend_Auth::getInstance()->getIdentity();
            $user = Doctrine_Query::create()
                ->from('Contact m')
                ->where('id = ?')->fetchOne(array($user['id']));

            $role = null;
            foreach ($user->Roles as $rol) {
                if ($role == null || $rol->level > $role->level) {
                    $role = $rol;
                }
            }
            $config = new Zend_Config_Ini(APPLICATION_PATH . '/modules/admin/config/' . 'orders/order.ini', $role->name);

            $fieldsarray = array();
            foreach ($config->tabs as $tab) {
                $tab = $tab->toArray();
                $fields = array();
                foreach ($tab['fields'] as $key => $field) {
                    $field['fieldLabel'] = $translate->translate($field['fieldLabel']);
                    array_push($fields, $field);
                }

                array_push($fieldsarray, array('general' => $tab['general'], 'fields' => $fields));
            }

            $configarray = $config->general->toArray();
            $configarray['title'] = $translate->translate($configarray['title']);

            $data = false;

            if (intval($this->_request->getParam('uid')) != 0) {

                $data = Doctrine_Query::create()
                    ->from('Orders a')
                    ->where('a.id = ?')->fetchOne(array(intval($this->_request->getParam('uid'))));

                $products = array();
                foreach ($data->Orderspos as $product) {
                    $pro = unserialize($product->data);
                    $proa = $pro->getArticle();
                    $proo = $pro->getOptions();

                    $options = array();
                    if (is_array($proo)) {
                        foreach ($proo as $key => $value) array_push($options, $key . ': ' . $value);
                    }

                    array_push($products,
                        array(
                            'articlename' => $proa['name']
                        , 'count' => $product->count
                        , 'priceone' => $this->view->currency->toCurrency($product->priceone)
                        , 'priceall' => $this->view->currency->toCurrency($product->priceall)
                        , 'options' => implode(' | ', $options)
                        )
                    );
                }

            }

            $config = array(
                "success" => true
            , "totalCount" => count($products)
            , "rows" => $products
            );

            $this->getHelper('Json')->sendJson($config);
        }

        if (intval($this->_request->getParam('config')) == 4) {

            $translate = Zend_Registry::get('translate');

            $user = Zend_Auth::getInstance()->getIdentity();
            $user = Doctrine_Query::create()
                ->from('Contact m')
                ->where('id = ?')->fetchOne(array($user['id']));

            $role = null;
            foreach ($user->Roles as $rol) {
                if ($role == null || $rol->level > $role->level) {
                    $role = $rol;
                }
            }
            $config = new Zend_Config_Ini(APPLICATION_PATH . '/modules/admin/config/' . 'orders/order.ini', $role->name);

            $fieldsarray = array();
            foreach ($config->tabs as $tab) {
                $tab = $tab->toArray();
                $fields = array();
                foreach ($tab['fields'] as $key => $field) {
                    $field['fieldLabel'] = $translate->translate($field['fieldLabel']);
                    array_push($fields, $field);
                }

                array_push($fieldsarray, array('general' => $tab['general'], 'fields' => $fields));
            }

            $configarray = $config->general->toArray();
            $configarray['title'] = $translate->translate($configarray['title']);

            $data = false;

            if (intval($this->_request->getParam('uid')) != 0) {

                $data = Doctrine_Query::create()
                    ->from('Orders a')
                    ->where('a.id = ?')->fetchOne(array(intval($this->_request->getParam('uid'))));

                if ($data->status) {
                    $config = new Zend_Config_Ini(APPLICATION_PATH . '/configs/' . '/systemdefaults.ini', 'status');

                    $translate = Zend_Registry::get('translate');

                    $temp = array();

                    foreach ($config->toArray() as $key => $value) array_push($temp, array('name' => $value, 'label' => $translate->translate($value)));

                    $data->status = $temp[($data->status - 1)]['name'];

                }

                $datatemp = $data->toArray();
                $datatemp['netto'] = 0;
                $datatemp['brutto'] = 0;
                foreach ($data->Orderspos as $product) {
                    $datatemp['netto'] = $datatemp['netto'] + $product->priceall;
                }
                $netto = $datatemp['netto'];
                $datatemp['netto'] = $this->view->currency->toCurrency($netto);
                if ($data->preis > 0) {
                    $datatemp['brutto'] = $this->view->currency->toCurrency($data->preis);
                }

                $paymenttype = Doctrine_Query::create()
                    ->from('Paymenttype as p')
                    ->where('p.id = ?', $datatemp['paymenttype_id'])
                    ->fetchOne();

                $datatemp['paymenttype_id'] = $paymenttype->title;

            }

            $config = array(
                "success" => true
            , "metaData" => array(
                    "fields" => $fieldsarray
                , "formConfig" => $configarray
                , "data" => ($datatemp != false) ? $datatemp : array()
                )
            , "data" => ($datatemp != false) ? $datatemp : array()
            );

            $this->getHelper('Json')->sendJson($config);
        }

        if (intval($this->_request->getParam('config')) == 5) {

            $translate = Zend_Registry::get('translate');

            $user = Zend_Auth::getInstance()->getIdentity();
            $user = Doctrine_Query::create()
                ->from('Contact m')
                ->where('id = ?')->fetchOne(array($user['id']));

            $role = null;
            foreach ($user->Roles as $rol) {
                if ($role == null || $rol->level > $role->level) {
                    $role = $rol;
                }
            }
            $config = new Zend_Config_Ini(APPLICATION_PATH . '/modules/admin/config/' . 'orders/contact.ini', $role->name);

            $fieldsarray = array();
            foreach ($config->tabs as $tab) {
                $tab = $tab->toArray();
                $fields = array();
                foreach ($tab['fields'] as $key => $field) {
                    $field['fieldLabel'] = $translate->translate($field['fieldLabel']);
                    array_push($fields, $field);
                }

                array_push($fieldsarray, array('general' => $tab['general'], 'fields' => $fields));
            }

            $configarray = $config->general->toArray();
            $configarray['title'] = $translate->translate($configarray['title']);

            $data = false;

            if (intval($this->_request->getParam('uid')) != 0) {

                $data = Doctrine_Query::create()
                    ->from('Orders a')
                    ->where('a.id = ?')->fetchOne(array(intval($this->_request->getParam('uid'))));

                $data = $data->Contact;

            }

            $config = array(
                "success" => true
            , "metaData" => array(
                    "fields" => $fieldsarray
                , "formConfig" => $configarray
                , "data" => ($data != false) ? $data->toArray(false) : array()
                )
            , "data" => ($data != false) ? $data->toArray(false) : array()
            );

            $this->getHelper('Json')->sendJson($config);
        }

        $this->_helper->layout->setLayout('popupjs');

    }

}
