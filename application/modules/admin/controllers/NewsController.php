<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of NewsController
 *
 * @author boonkerz
 */
class Admin_NewsController extends TP_Admin_Controller_Action
{

    public function init() {
        parent::init();
        $contextSwitch = $this->_helper->contextSwitch();
        $contextSwitch->addActionContext('liveedit', self::CONTEXT_JSON)
            ->addActionContext('systemnews', self::CONTEXT_JSON)
            ->setAutoJsonSerialization(true)->initContext();
    }

    public function systemnewsAction() {

        $rows = Doctrine_Query::create()->select()->from('Adminnews n')->where('n.id > ?', array($this->_getParam('id')))->orderBy('n.created DESC')->limit(10)->execute();

        $this->view->type = 'event';
        $this->view->name = 'alert';
        $this->view->rows = $rows->toArray();

        foreach ($rows as $r) {
            if ($r['status'] == 9) {
                $this->view->popup = true;
            }
        }
    }

    public function liveeditAction() {

        $row = Doctrine_Query::create()
            ->from('News m')
            ->addWhere('shop_id IN (' . implode(',', $this->shopIds) . ') AND id = ?', array($this->_getParam('id')))->fetchOne();

        if (!$row) {
            $this->view->success = false;
            return;
        }
        $field = $this->_getParam('field');
        if ($row->getTable()->hasField($field)) {
            $row->$field = $this->_getParam('content');
            $row->save();

            $this->view->success = true;
            return;
        }

        $this->view->success = false;
        return;
    }

    public function allAction() {
        if ($this->_request->getParam('config') == 1) {

            $rows = Doctrine_Query::create()
                ->from('News m')
                ->limit(intval($this->_request->getParam('limit')))
                ->offset(intval($this->_request->getParam('start')));
            if (intval(str_replace('S', '', $this->_request->getParam('sid'))) != 0) {
                $rows->where('shop_id = ?', array(intval(str_replace('S', '', $this->_request->getParam('sid')))));
            }
            if ($this->_request->getParam('sort')) {
                $rows->orderBy($this->_request->getParam('sort') . ' ' . $this->_request->getParam('dir'));
            }

            $rows = $rows->execute();

            $count = Doctrine_Query::create()
                ->from('News m');
            if (intval(str_replace('S', '', $this->_request->getParam('sid'))) != 0) {
                $count->where('shop_id = ?', array(intval(str_replace('S', '', $this->_request->getParam('sid')))));
            }

            $count = $count->execute();

            $config = array(
                "success" => true
            , "totalCount" => $count->count()
            , "rows" => $rows->toArray(false)
            );

            $this->getHelper('Json')->sendJson($config);
        }

        $this->view->sid = intval(str_replace('S', '', $this->_request->getParam('id')));

        $this->_helper->layout->setLayout('simple');
    }

    public function editAction() {

        $isNew = false;
        $id = 0;

        if (intval($this->_request->getParam('delete')) == 1) {

            Doctrine_Query::create()
                ->from('News a')
                ->where('id = ?', array($this->_request->getParam('uid')))
                ->delete()
                ->execute();

            die($this->getHelper('Json')->sendJson(array('success' => true)));
        }

        if (intval($this->_request->getParam('config')) == 3) {

            $translate = Zend_Registry::get('translate');

            $user = Zend_Auth::getInstance()->getIdentity();
            $user = Doctrine_Query::create()
                ->from('Contact m')
                ->where('id = ?')->fetchOne(array($user['id']));

            $role = null;
            foreach ($user->Roles as $rol) {
                if ($role == null || $rol->level > $role->level) {
                    $role = $rol;
                }
            }
            $config = new Zend_Config_Ini(APPLICATION_PATH . '/modules/admin/config/' . 'news/settings.ini', $role->name);

            $fieldsarray = array();
            foreach ($config->tabs as $tab) {
                $tab = $tab->toArray();
                $fields = array();
                foreach ($tab['fields'] as $key => $field) {
                    $field['fieldLabel'] = $translate->translate($field['fieldLabel']);
                    array_push($fields, $field);
                }

                array_push($fieldsarray, array('general' => $tab['general'], 'fields' => $fields));
            }

            $configarray = $config->general->toArray();
            $configarray['title'] = $translate->translate($configarray['title']);

            $data = false;

            if (intval($this->_request->getParam('uid')) != 0) {

                $data = Doctrine_Query::create()
                    ->from('News a')
                    ->where('a.id = ?')->fetchOne(array(intval($this->_request->getParam('uid'))));
            }
            if (intval($this->_request->getParam('save')) == 1 && intval($this->_request->getParam('uid')) > 0) {

                $params = $this->_request->getParams();

                unset($params['config']);
                unset($params['save']);
                unset($params['module']);
                unset($params['controller']);
                unset($params['action']);
                unset($params['sid']);
                unset($params['uid']);
                unset($params['headingsize']);
                foreach ($params as $key => $value)
                    $data->$key = stripslashes($value);

                $data->save();
            } elseif (intval($this->_request->getParam('save')) == 1 && intval($this->_request->getParam('uid')) == 0) {

                $params = $this->_request->getParams();

                unset($params['config']);
                unset($params['save']);
                unset($params['module']);
                unset($params['controller']);
                unset($params['action']);
                unset($params['sid']);
                unset($params['uid']);
                unset($params['headingsize']);
                $data = new News();
                $data->shop_id = intval($this->_request->getParam('sid'));
                $data->Install = $user->Install;

                foreach ($params as $key => $value)
                    $data->$key = $value;

                $data->save();

                $isNew = true;
                $id = $data->id;
            }

            $temp = array();
            if ($data) {
                $temp = $data->toArray();
                $temp['language_title'] = $this->translate->translate($temp['language']);
            }

            if ($temp['sort_date'] != '0000-00-00' && $temp['sort_date'] != '') {
                $date = new Zend_Date($temp['sort_date']);
                $temp['sort_date'] = $date->toString(Zend_Date::DATE_MEDIUM);

            } else {
                $date = new Zend_Date(date('Y-m-d'));
                $temp['sort_date'] = $date->toString(Zend_Date::DATE_MEDIUM);
            }

            $config = array(
                "success" => true
            , "isNew" => $isNew
            , "uid" => $id
            , "metaData" => array(
                    "fields" => $fieldsarray
                , "formConfig" => $configarray
                , "data" => $temp
                )
            , "data" => $temp
            );

            $this->getHelper('Json')->sendJson($config);
        }

        $this->_helper->layout->setLayout('popupjs');
    }

}