<?php

class Admin_NewsletterController extends TP_Admin_Controller_Action
{

    public function init() {
        parent::init();

        $contextSwitch = $this->_helper->contextSwitch();

        $contextSwitch
            ->addActionContext('fetchnewsletter', self::CONTEXT_JSON)
            ->addActionContext('loadsavenewsletter', self::CONTEXT_JSON)
            ->addActionContext('sendtest', self::CONTEXT_JSON)
            ->setAutoJsonSerialization(true)
            ->initContext();
    }

    public function allAction() {

    }

    public function sendtestAction() {

        $newsletter = Doctrine_Query::create()->select()->from('Newsletter m')->where('m.id = ?', array($this->_getParam('uid')))
            ->addWhere('m.shop_id IN (' . implode(',', $this->shopIds) . ')')->fetchOne();

        $this->view->success = false;
        if ($newsletter) {

            $loader = new Twig_Loader_String();
            $twig = new Twig_Environment($loader);
            $template = $twig->loadTemplate(str_replace('\\', '', $newsletter->text));
            $templatehtml = $twig->loadTemplate(str_replace('\\', '', $newsletter->text_html));
            $templateSubj = $twig->loadTemplate(str_replace('\\', '', $newsletter->subject));

            $templateVars = array(
                'shop' => $this->shop,
                'contact' => $this->user);

            $content = $template->render($templateVars);
            $contenthtml = $templatehtml->render($templateVars);
            $subject = $templateSubj->render($templateVars);

            TP_Mail::sendTextMail('queue', $content, $contenthtml, $subject, $this->user->self_email, $newsletter->from_name, $newsletter->from_mail, array());

            $this->view->success = true;
        }
    }

    public function sendAction() {

        $newsletter = Doctrine_Query::create()->select()->from('Newsletter m')->where('m.id = ?', array($this->_getParam('uid')))
            ->addWhere('m.shop_id IN (' . implode(',', $this->shopIds) . ')')->fetchOne();

        $this->view->success = false;
        if ($newsletter) {

            $data = new Newsletterqueue();
            $data->shop_id = $newsletter->shop_id;
            $data->install_id = $newsletter->install_id;
            $data->contact_id = $this->user->id;
            $data->status = 1;
            $data->send_count = 0;
            $data->newsletter_id = $newsletter->id;
            $data->save();

            $this->view->success = true;
        }
    }

    public function fetchnewsletterAction() {
        try {
            $rows = Doctrine_Query::create()->from('Newsletter m')->limit(intval($this->_request->getParam('limit')))->offset(intval($this->_request->getParam('start')));
            if (intval(str_replace('S', '', $this->_request->getParam('sid'))) != 0) {
                $rows->where('shop_id = ?', array(
                    intval(str_replace('S', '', $this->_request->getParam('sid')))));
            }
            if ($this->_request->getParam('sort')) {
                $rows->orderBy($this->_request->getParam('sort') . ' ' . $this->_request->getParam('dir'));
            }
            $rows = $rows->addWhere('shop_id IN (' . implode(',', $this->shopIds) . ')')->execute();

            $count = Doctrine_Query::create()->from('Newsletter m');
            if (intval(str_replace('S', '', $this->_request->getParam('sid'))) != 0) {
                $count->where('shop_id = ?', array(
                    intval(str_replace('S', '', $this->_request->getParam('sid')))));
            }
            $count = $count->addWhere('shop_id IN (' . implode(',', $this->shopIds) . ')')->execute();

            $this->view->totalCount = $count->count();
            $this->view->rows = $rows->toArray();
            $this->view->success = true;
        } catch (Exception $e) {
            Zend_Registry::get('log')->debug($e->getMessage());
            $this->view->success = false;
        }
    }

    public function addnewsletterAction() {
        $this->_helper->layout->setLayout('popupjs');
    }

    public function loadsavenewsletterAction() {
        $isNew = false;
        $id = 0;
        $translate = Zend_Registry::get('translate');
        $user = Zend_Auth::getInstance()->getIdentity();
        $user = Doctrine_Query::create()->from('Contact m')->where('id = ?')->fetchOne(array(
            $user['id']));
        $role = null;
        foreach ($user->Roles as $rol) {
            if ($role == null || $rol->level > $role->level) {
                $role = $rol;
            }
        }
        $config = new Zend_Config_Ini(APPLICATION_PATH . '/modules/admin/config/newsletter/settings.ini', $role->name);
        $fieldsarray = array();
        foreach ($config->tabs as $tab) {
            $tab = $tab->toArray();
            $fields = array();
            foreach ($tab['fields'] as $key => $field) {
                $field['fieldLabel'] = $translate->translate($field['fieldLabel']);
                array_push($fields, $field);
            }
            array_push($fieldsarray, array(
                'general' => $tab['general'],
                'fields' => $fields));
        }
        $configarray = $config->general->toArray();
        $configarray['title'] = $translate->translate($configarray['title']);
        $data = false;
        if (intval($this->_request->getParam('uid')) != 0) {
            $data = Doctrine_Query::create()->from('Newsletter a')->where('a.id = ?')->fetchOne(array(
                intval($this->_request->getParam('uid'))));
        }
        if (intval($this->_request->getParam('save')) == 1 && intval($this->_request->getParam('uid')) > 0) {
            $params = TP_Util::clearParams($this->_request->getParams());
            $filter = new TP_Filter_Badwords();
            foreach ($params as $key => $value)
                $data->$key = $filter->filter($value);
            $data->save();
        } elseif (intval($this->_request->getParam('save')) == 1 && intval($this->_request->getParam('uid')) == 0) {
            $params = TP_Util::clearParams($this->_request->getParams());
            $data = new Newsletter();
            $data->shop_id = intval($this->_request->getParam('sid'));
            $data->Install = $user->Install;
            $filter = new TP_Filter_Badwords();
            foreach ($params as $key => $value)
                $data->$key = $filter->filter($value);
            $data->save();
            $isNew = true;
            $id = $data->id;
        }
        $this->view->success = true;
        $this->view->uid = $id;
        $this->view->isNew = $isNew;
        $this->view->metaData = array(
            "fields" => $fieldsarray,
            "formConfig" => $configarray,
            "data" => ($data != false) ? $data->toArray() : array());
        $this->view->data = ($data != false) ? $data->toArray() : array();
    }

}