<?php

class Admin_OrdersController extends TP_Admin_Controller_Action
{

    public function init() {
        parent::init();
        $contextSwitch = $this->_helper->contextSwitch();
        $contextSwitch
            ->addActionContext('changeposstatus', 'json')
            ->setAutoJsonSerialization(true)->initContext();

    }

    /**
     * IndexAction
     *
     * @todo Dokumentation der IndexAction
     *
     * @return void
     */
    public function indexAction() {

    }

    public function changeposstatusAction() {

        $this->view->clearVars();
        $position = Doctrine_Query::create()
            ->select()
            ->from('Orderspos m')
            ->addWhere('m.uuid = ?', array($this->_getParam('uuid')))->fetchOne();

        $position->status = $this->_getParam('newstatus');

        $pro = unserialize($position->data);
        $proo = $pro->getOptions();

        if (isset($proo['produktionszeit']) && $this->_getParam('newstatus') == 160) {
            $obj = new TP_Delivery(date('Y-m-d'));
            $result = $obj->getDeliveryDate($proo['produktionszeit']);
            $position->delivery_date = $result->format('Y-m-d');
        }

        $position->save();
        $order = $position->Orders;
        $order->updated = date('Y-m-d H:i:s');
        if ($this->install->default_status_pos_status_change != "" && $this->install->default_status_pos_status_change != 0) {
            $order->status = $this->install->default_status_pos_status_change;
        }
        $order->save();

        TP_Queue::process('orderposstatuschange', 'global', $position);
        $this->view->success = true;
        return;
    }

    public function editAction() {

        if (intval($this->_request->getParam('config')) == 3) {

            $translate = Zend_Registry::get('translate');

            $user = Zend_Auth::getInstance()->getIdentity();
            $user = Doctrine_Query::create()
                ->from('Contact m')
                ->where('id = ?')->fetchOne(array($user['id']));

            $role = null;
            foreach ($user->Roles as $rol) {
                if ($role == null || $rol->level > $role->level) {
                    $role = $rol;
                }
            }
            $config = new Zend_Config_Ini(APPLICATION_PATH . '/modules/admin/config/' . 'orders/order.ini', $role->name);

            $fieldsarray = array();
            foreach ($config->tabs as $tab) {
                $tab = $tab->toArray();
                $fields = array();
                foreach ($tab['fields'] as $key => $field) {
                    $field['fieldLabel'] = $translate->translate($field['fieldLabel']);
                    array_push($fields, $field);
                }

                array_push($fieldsarray, array('general' => $tab['general'], 'fields' => $fields));
            }

            $configarray = $config->general->toArray();
            $configarray['title'] = $translate->translate($configarray['title']);

            $data = false;

            if (intval($this->_request->getParam('uid')) != 0) {

                $data = Doctrine_Query::create()
                    ->from('Orders a')
                    ->where('a.id = ?')->fetchOne(array(intval($this->_request->getParam('uid'))));

                $products = array();
                foreach ($data->Orderspos as $product) {
                    $pro = unserialize($product->data);
                    $proa = $pro->getArticle();
                    $proo = $pro->getOptions();

                    $article = Doctrine_Query::create()
                        ->from('Article c')
                        ->where('c.id = ?', array($product->article_id))
                        ->fetchOne();

                    if ($proa['typ'] == 9) {
                        $options = $article->getMarketOptArray($product->data, true, $product->a9_designer_data);
                    } else {
                        $options = $article->getOptArray($product->data, true);
                    }
                    $options[] = $pro->getLayouterId();
                    $options[] = $pro->getTemplatePrintId();
                    $translate = Zend_Registry::get('translate');

                    if($product->getPostionData('auflage')) {
                        $product->count = $product->getPostionData('auflage');
                    }

                    if ($product->xmlxslfofile != "" && $product->render_print == false) {

                        array_push($products,
                            array(
                                'articlename' => $proa['title']
                            , 'special' => $pro->getSpecial()
                            , 'count' => $product->count
                            , 'id' => $product->uuid
                            , 'status' => $product->status
                            , 'status_title' => $translate->translate($product->status)
                            , 'priceone' => $this->view->currency->toCurrency($product->priceone)
                            , 'priceall' => $this->view->currency->toCurrency($product->priceall)
                            , 'options' => implode(' | ', $options)
                            , 'pdf' => '<a href="http://' . $_SERVER["SERVER_NAME"] . '/service/index?type=render_print&uid=' . $product->uuid . '">Pdf</a>'
                            )
                        );
                    } else {
                        array_push($products,
                            array(
                                'articlename' => $proa['title']
                            , 'special' => $pro->getSpecial()
                            , 'id' => $product->uuid
                            , 'count' => $product->count
                            , 'status' => $product->status
                            , 'status_title' => $translate->translate($product->status)
                            , 'priceone' => $this->view->currency->toCurrency($product->priceone)
                            , 'priceall' => $this->view->currency->toCurrency($product->priceall)
                            , 'options' => implode(' | ', $options)
                            , 'pdf' => ''
                            )
                        );
                    }
                }

            }

            $config = array(
                "success" => true
            , "totalCount" => count($products)
            , "rows" => $products
            );

            $this->getHelper('Json')->sendJson($config);
        }

        if (intval($this->_request->getParam('config')) == 4) {

            $translate = Zend_Registry::get('translate');

            $user = Zend_Auth::getInstance()->getIdentity();
            $user = Doctrine_Query::create()
                ->from('Contact m')
                ->where('id = ?')->fetchOne(array($user['id']));

            $role = null;
            foreach ($user->Roles as $rol) {
                if ($role == null || $rol->level > $role->level) {
                    $role = $rol;
                }
            }
            $config = new Zend_Config_Ini(APPLICATION_PATH . '/modules/admin/config/' . 'orders/order.ini', $role->name);

            $fieldsarray = array();
            foreach ($config->tabs as $tab) {
                $tab = $tab->toArray();
                $fields = array();
                foreach ($tab['fields'] as $key => $field) {
                    $field['fieldLabel'] = $translate->translate($field['fieldLabel']);
                    array_push($fields, $field);
                }

                array_push($fieldsarray, array('general' => $tab['general'], 'fields' => $fields));
            }

            $configarray = $config->general->toArray();
            $configarray['title'] = $translate->translate($configarray['title']);

            $data = false;

            if (intval($this->_request->getParam('uid')) != 0) {

                $data = Doctrine_Query::create()
                    ->from('Orders a')
                    ->where('a.id = ?')->fetchOne(array(intval($this->_request->getParam('uid'))));



                if ($data->status) {
                    $config = new Zend_Config_Ini(APPLICATION_PATH . '/configs/' . '/systemdefaults.ini', 'status');

                    $translate = Zend_Registry::get('translate');

                    $temp = array();

                    $status = $config->toArray();

                    $data->status = $translate->translate($data->status);

                }


                $tmp = array();
                if ($this->install->offline_custom2 != "") {
                    try {

                        $maschienen = new SimpleXMLElement($this->install->offline_custom2, null, false);

                        foreach ($maschienen as $maschine) {
                            $tmp[(int)$maschine['id']] = $translate->translate((string)$maschine);
                        }
                    } catch (Exception $e) {

                    }
                }
                if (isset($tmp[$data->status])) {
                    $data->status = $tmp[$data->status];
                }

                $datatemp = $data->toArray();
                $datatemp['netto'] = 0;
                $datatemp['brutto'] = 0;
                foreach ($data->Orderspos as $product) {
                    $datatemp['netto'] = $datatemp['netto'] + $product->priceall;
                }
                $netto = $datatemp['netto'];
                $datatemp['netto'] = $this->view->currency->toCurrency($netto);
                if ($data->preis > 0) {
                    $datatemp['brutto'] = $this->view->currency->toCurrency($data->preis);
                }

                $paymenttype = Doctrine_Query::create()
                    ->from('Paymenttype as p')
                    ->where('p.id = ?', $datatemp['paymenttype_id'])
                    ->fetchOne();

                $datatemp['paymenttype_id'] = $paymenttype->title;

            }

            if ($data->delivery_same == true) {
                $datatemp['lieferan'] = "gleich Rechnungsanschrift";
            } else {

                $data = Doctrine_Query::create()
                    ->from('ContactAddress a')
                    ->where('a.id = ?')->fetchOne(array(intval($data->delivery_address)));
                $datatemp['lieferan'] = $data->company . '
' . $data->firstname . ' ' . $data->lastname . '
' . $data->street . ' ' . $data->house_number . '
' . $data->zip . ' ' . $data->city;
            }

            $config = array(
                "success" => true
            , "metaData" => array(
                    "fields" => $fieldsarray
                , "formConfig" => $configarray
                , "data" => ($datatemp != false) ? $datatemp : array()
                )
            , "data" => ($datatemp != false) ? $datatemp : array()
            );

            $this->getHelper('Json')->sendJson($config);
        }

        if (intval($this->_request->getParam('config')) == 5) {

            $translate = Zend_Registry::get('translate');

            $user = Zend_Auth::getInstance()->getIdentity();
            $user = Doctrine_Query::create()
                ->from('Contact m')
                ->where('id = ?')->fetchOne(array($user['id']));

            $role = null;
            foreach ($user->Roles as $rol) {
                if ($role == null || $rol->level > $role->level) {
                    $role = $rol;
                }
            }
            $config = new Zend_Config_Ini(APPLICATION_PATH . '/modules/admin/config/' . 'orders/contact.ini', $role->name);

            $fieldsarray = array();
            foreach ($config->tabs as $tab) {
                $tab = $tab->toArray();
                $fields = array();
                foreach ($tab['fields'] as $key => $field) {
                    $field['fieldLabel'] = $translate->translate($field['fieldLabel']);
                    array_push($fields, $field);
                }

                array_push($fieldsarray, array('general' => $tab['general'], 'fields' => $fields));
            }

            $configarray = $config->general->toArray();
            $configarray['title'] = $translate->translate($configarray['title']);

            $data = false;

            if (intval($this->_request->getParam('uid')) != 0) {

                $data = Doctrine_Query::create()
                    ->from('Orders a')
                    ->where('a.id = ?')->fetchOne(array(intval($this->_request->getParam('uid'))));

                $order = $data;
                $data = $data->Contact;

            }

            if (intval($this->_request->getParam('save')) == 1) {

                $params = $this->_request->getParams();

                unset($params['config']);
                unset($params['save']);
                unset($params['module']);
                unset($params['controller']);
                unset($params['action']);
                unset($params['sid']);
                unset($params['uid']);
                unset($params['headingsize']);
                $config = new Zend_Config_Ini(APPLICATION_PATH . '/configs/' . '/systemdefaults.ini', 'status');

                $translate = Zend_Registry::get('translate');

                $temp = array();

                foreach ($config->toArray() as $key => $value) {
                    $temp[($key)] = $value;
                }
                $install = Zend_Registry::get('install');
                if ($install['offline_custom2'] != "") {
                    try {

                        $maschienen = new SimpleXMLElement($install['offline_custom2'], null, false);

                        foreach ($maschienen as $maschine) {
                            $temp[(int)$maschine['id']] = (string)$maschine;
                        }
                    } catch (Exception $e) {

                    }
                }

                foreach ($temp as $key => $value) {

                    if ($key == $params['statusid']) {

                        if ($order->status != $key) {
                            $orgstatus = $order->status;
                            $order->status = $key;
                            $order->save();

                            if ($orgstatus < 140 && $key >= 140 && $key != 170) {

                                $produktionszeit = 0;
                                foreach ($order->Orderspos as $pos) {
                                    $pro = unserialize($pos->data);
                                    $proo = $pro->getOptions();

                                    if (isset($proo['produktionszeit']) && $proo['produktionszeit'] > $produktionszeit) {
                                        $produktionszeit = $proo['produktionszeit'];
                                    }
                                    if (isset($proo['produktionszeit'])) {
                                        $obj = new TP_Delivery(date('Y-m-d'));
                                        $result = $obj->getDeliveryDate($proo['produktionszeit']);
                                        $pos->delivery_date = $result->format('Y-m-d');
                                        $pos->save();
                                    }
                                }

                                if ($produktionszeit > 0) {
                                    $obj = new TP_Delivery(date('Y-m-d'));
                                    $result = $obj->getDeliveryDate($produktionszeit);
                                    $order->delivery_date = $result->format('Y-m-d');
                                    $order->save();
                                }

                            }
                            TP_Queue::process('orderstatuschange', 'global', $order);
                        }
                    }
                }

                unset($params['statusid']);

                if (isset($params['enable']) && $params['enable'] == 'on') {
                    $order->enable = true;
                } else {
                    $order->enable = false;
                }
                unset($params['enable']);

                foreach ($params as $key => $value) $order->$key = $value;

                $order->save();
            }

            $datat = $data->toArray(false);
            $datat['self_anrede_title'] = $data->getAnrede();

            $config = array(
                "success" => true
            , "metaData" => array(
                    "fields" => $fieldsarray
                , "formConfig" => $configarray
                , "data" => $datat
                )
            , "data" => $datat
            );

            $this->getHelper('Json')->sendJson($config);
        }

        $this->_helper->layout->setLayout('popupjs');

    }

}
