<?php
class Admin_PapierdbController extends TP_Admin_Controller_Action
{

    public function init() {
        if (!$this->_request->getParam('sid', false)) {
            $wizard = new TP_ResaleWizard ();
            $this->_request->setParam('sid', $wizard->getArticleShop());
        }
        parent::init();
        $contextSwitch = $this->_helper->contextSwitch();
        $contextSwitch->addActionContext('fetchpapers', self::CONTEXT_JSON)->addActionContext('deletepapier', self::CONTEXT_JSON)->addActionContext('loadsavepapier', self::CONTEXT_JSON)->setAutoJsonSerialization(true)->initContext();
    }

    public function allAction() {
    }

    public function fetchpapersAction() {

        $rows = Doctrine_Query::create()->from('Papier m')->where('m.install_id = ?', array($this->install->id))->limit(intval($this->_request->getParam('limit')))->offset(intval($this->_request->getParam('start')));
        if ($this->_request->getParam('sort')) {
            $rows->orderBy($this->_request->getParam('sort') . ' ' . $this->_request->getParam('dir'));
        }

        TP_Filter::proccess($rows);
        $rows = $rows->execute();
        $count = Doctrine_Query::create()->from('Papier m')->where('m.install_id = ?', array($this->install->id));
        TP_Filter::proccess($count);
        $count = $count->execute();

        $this->view->success = true;
        $this->view->totalCount = $count->count();
        $this->view->rows = $rows->toArray();
    }

    public function deletepapierAction() {
        if (intval($this->_request->getParam('delete')) == 1) {
            Doctrine_Query::create()->from('Papier a')->where('uuid = ? AND install_id = ?', array($this->_request->getParam('uid'), $this->install->id))->delete()->execute();
            $this->view->success = true;
        }
    }

    public function addpapierAction() {
        $this->_helper->layout->setLayout('popupjs');
    }

    public function loadsavepapierAction() {
        $isNew = false;
        $id = 0;
        $translate = Zend_Registry::get('translate');
        $user = Zend_Auth::getInstance()->getIdentity();
        $user = Doctrine_Query::create()->from('Contact m')->where('id = ?')->fetchOne(array($user ['id']));
        $role = null;

        foreach ($user->Roles as $rol) {
            if ($role == null || $rol->level > $role->level) {
                $role = $rol;
            }
        }
        $config = new Zend_Config_Ini (APPLICATION_PATH . '/modules/admin/config/papierdb/papiersettings.ini', $role->name);
        $fieldsarray = array();
        foreach ($config->tabs as $tab) {
            $tab = $tab->toArray();
            $fields = array();
            foreach ($tab ['fields'] as $key => $field) {
                $field ['fieldLabel'] = $translate->translate($field ['fieldLabel']);
                array_push($fields, $field);
            }
            array_push($fieldsarray, array('general' => $tab ['general'], 'fields' => $fields));
        }
        $configarray = $config->general->toArray();
        $configarray ['title'] = $translate->translate($configarray ['title']);
        $data = false;
        if (intval($this->_request->getParam('uid')) != 0) {
            $data = Doctrine_Query::create()->from('Papier a')->where('a.uuid = ?')->fetchOne(array($this->_request->getParam('uid')));
        }
        if (intval($this->_request->getParam('save')) == 1 && $this->_request->getParam('uid') != 0) {
            $params = TP_Util::clearParams($this->_request->getParams());
            $filter = new TP_Filter_Badwords ();
            foreach ($params as $key => $value)
                $data->$key = $filter->filter($value);
            $data->save();
        } elseif (intval($this->_request->getParam('save')) == 1 && $this->_request->getParam('uid') == 0) {
            $params = TP_Util::clearParams($this->_request->getParams());
            $data = new Papier ();
            $data->install_id = $this->install->id;
            $filter = new TP_Filter_Badwords ();
            foreach ($params as $key => $value)
                $data->$key = $filter->filter($value);
            $data->save();
            $isNew = true;
            $id = $data->uuid;
        }
        $this->view->success = true;
        $this->view->uid = $id;
        $this->view->isNew = $isNew;
        $this->view->metaData = array("fields" => $fieldsarray, "formConfig" => $configarray, "data" => ($data != false) ? $data->toArray() : array());
        $this->view->data = ($data != false) ? $data->toArray() : array();
    }
}