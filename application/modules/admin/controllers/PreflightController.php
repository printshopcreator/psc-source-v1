<?php

class Admin_PreflightController extends TP_Admin_Controller_Action
{

    public function init() {
        parent::init();
        $contextSwitch = $this->_helper->contextSwitch();
        $contextSwitch
            ->addActionContext('loadsavepreflight', self::CONTEXT_JSON)
            ->addActionContext('fetchpreflight', self::CONTEXT_JSON)
            ->addActionContext('deletepreflight', self::CONTEXT_JSON)
            ->addActionContext('copypreflight', self::CONTEXT_JSON)
            ->setAutoJsonSerialization(true)->initContext();
    }

    public function deletepreflightAction() {
        if (intval($this->_request->getParam('delete')) == 1) {
            Doctrine_Query::create()->from('Preflight m')->where('m.id = ? AND m.install_id = ?', array(
                $this->_request->getParam('uid'), $this->install->id))->delete()->execute();
            $this->view->success = true;
        }
    }

    public function copypreflightAction() {
        if (intval($this->_request->getParam('copy')) == 1) {
            $row = Doctrine_Query::create()->from('Preflight m')->where('m.id = ? AND m.install_id = ?', array(
                $this->_request->getParam('uid'), $this->install->id))->fetchOne();

            $newrow = $row->copy();
            $newrow->id = '';
            $newrow->nr = $newrow->nr . ' new';
            $newrow->save();
            $this->view->success = true;
        }
    }

    public function allAction() {

    }

    public function fetchpreflightAction() {
        $rows = Doctrine_Query::create()->from('Preflight m')
            ->limit(intval($this->_request->getParam('limit')))
            ->offset(intval($this->_request->getParam('start')))
            ->where('m.install_id = ?', array($this->install->id));
        if ($this->_request->getParam('sort')) {
            $rows->orderBy($this->_request->getParam('sort') . ' ' . $this->_request->getParam('dir'));
        }
        $rows = $rows->execute();
        $count = Doctrine_Query::create()->from('Preflight m')->where('m.install_id = ?', array($this->install->id))->execute();

        $temp = array();
        foreach ($rows->toArray() as $row) {

            array_push($temp, $row);
        }

        $this->view->success = true;
        $this->view->totalCount = $count->count();
        $this->view->rows = $temp;
    }

    public function loadsavepreflightAction() {
        $isNew = false;
        $id = 0;
        $translate = Zend_Registry::get('translate');
        $user = Zend_Auth::getInstance()->getIdentity();
        $user = Doctrine_Query::create()->from('Contact m')->where('id = ?')->fetchOne(array(
            $user['id']));
        $role = null;

        foreach ($user->Roles as $rol) {
            if ($role == null || $rol->level > $role->level) {
                $role = $rol;
            }
        }
        $config = new Zend_Config_Ini(APPLICATION_PATH . '/modules/admin/config/preflight/settings.ini', $role->name);
        $fieldsarray = array();
        foreach ($config->tabs as $tab) {
            $tab = $tab->toArray();
            $fields = array();
            foreach ($tab['fields'] as $key => $field) {
                $field['fieldLabel'] = $translate->translate($field['fieldLabel']);
                if (isset($field['items'])) {
                    $tempitems = array();
                    foreach ($field['items'] as $subit) {

                        $subit ['fieldLabel'] = $translate->translate($subit ['fieldLabel']);

                        array_push($tempitems, $subit);
                    }

                    $field['items'] = $tempitems;
                }
                array_push($fields, $field);
            }
            array_push($fieldsarray, array(
                'general' => $tab['general'],
                'fields' => $fields));
        }
        $configarray = $config->general->toArray();
        $configarray['title'] = $translate->translate($configarray['title']);
        $data = false;
        if (intval($this->_request->getParam('uid')) != 0) {
            $data = Doctrine_Query::create()->from('Preflight a')->where('a.id = ?')->fetchOne(array(
                intval($this->_request->getParam('uid'))));
        }
        if (intval($this->_request->getParam('save')) == 1 && intval($this->_request->getParam('uid')) > 0) {
            $params = TP_Util::clearParams($this->_request->getParams());
            $filter = new TP_Filter_Badwords();
            foreach ($params as $key => $value) {
                if (isset($data[$key]) && false == strpos($value, 'service/index?type=upload&mode=get')) {
                    $data->$key = $filter->filter($value);
                }
            }
            $data->save();
        } elseif (intval($this->_request->getParam('save')) == 1 && intval($this->_request->getParam('uid')) == 0) {
            $params = TP_Util::clearParams($this->_request->getParams());
            $params['parent_id'] = intval($params['parent_id']);
            $data = new Preflight();
            $data->install_id = $this->install->id;
            $filter = new TP_Filter_Badwords();
            foreach ($params as $key => $value)
                if (isset($data[$key]) && false == strpos($value, 'service/index?type=upload&mode=get')) {
                    $data->$key = $filter->filter($value);
                }
            $data->save();
            $isNew = true;
            $id = $data->id;
        }
        $this->view->success = true;
        $this->view->uid = $id;
        $this->view->isNew = $isNew;
        $this->view->metaData = array(
            "fields" => $fieldsarray,
            "formConfig" => $configarray,
            "data" => ($data != false) ? $data->toArray() : array());
        $this->view->data = ($data != false) ? $data->toArray() : array();

    }

    public function addpreflightAction() {
        $this->_helper->layout->setLayout('popupjs');
    }

}