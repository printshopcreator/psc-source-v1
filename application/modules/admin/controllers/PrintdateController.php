<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of PrintdateController
 *
 * @author boonkerz
 */
class Admin_PrintdateController extends TP_Admin_Controller_Action
{

    public function allAction() {
        if ($this->_request->getParam('config') == 1) {

            $rows = Doctrine_Query::create()
                ->from('Printdate m')
                ->limit(intval($this->_request->getParam('limit')))
                ->offset(intval($this->_request->getParam('start')));
            if (intval(str_replace('S', '', $this->_request->getParam('sid'))) != 0) {
                $rows->where('shop_id = ?', array(intval(str_replace('S', '', $this->_request->getParam('sid')))));
            }
            if ($this->_request->getParam('sort')) {
                $rows->orderBy($this->_request->getParam('sort') . ' ' . $this->_request->getParam('dir'));
            }

            $rows = $rows->execute();

            $count = Doctrine_Query::create()
                ->from('Printdate m');
            if (intval(str_replace('S', '', $this->_request->getParam('sid'))) != 0) {
                $count->where('shop_id = ?', array(intval(str_replace('S', '', $this->_request->getParam('sid')))));
            }

            $count = $count->execute();

            $config = array(
                "success" => true
            , "totalCount" => $count->count()
            , "rows" => $rows->toArray(false)
            );

            $this->getHelper('Json')->sendJson($config);
        }

        $this->view->sid = intval(str_replace('S', '', $this->_request->getParam('id')));

        $this->_helper->layout->setLayout('simple');
    }

    public function editAction() {

        $isNew = false;
        $id = 0;

        if (intval($this->_request->getParam('delete')) == 1) {

            Doctrine_Query::create()
                ->from('Printdate a')
                ->where('id = ?', array($this->_request->getParam('uid')))
                ->delete()
                ->execute();

            die($this->getHelper('Json')->sendJson(array('success' => true)));
        }

        if (intval($this->_request->getParam('config')) == 3) {

            $translate = Zend_Registry::get('translate');

            $user = Zend_Auth::getInstance()->getIdentity();
            $user = Doctrine_Query::create()
                ->from('Contact m')
                ->where('id = ?')->fetchOne(array($user['id']));

            $role = null;
            foreach ($user->Roles as $rol) {
                if ($role == null || $rol->level > $role->level) {
                    $role = $rol;
                }
            }
            $config = new Zend_Config_Ini(APPLICATION_PATH . '/modules/admin/config/' . 'printdate/settings.ini', $role->name);

            $fieldsarray = array();
            foreach ($config->tabs as $tab) {
                $tab = $tab->toArray();
                $fields = array();
                foreach ($tab['fields'] as $key => $field) {
                    $field['fieldLabel'] = $translate->translate($field['fieldLabel']);
                    array_push($fields, $field);
                }

                $rows = Doctrine_Query::create()
                    ->from('Shippingtype m')
                    ->limit(intval($this->_request->getParam('limit')))
                    ->offset(intval($this->_request->getParam('start')));
                if (intval(str_replace('S', '', $this->_request->getParam('sid'))) != 0) {
                    $rows->where('shop_id = ?', array(intval(str_replace('S', '', $this->_request->getParam('sid')))));
                }
                if ($this->_request->getParam('sort')) {
                    $rows->orderBy($this->_request->getParam('sort') . ' ' . $this->_request->getParam('dir'));
                }

                $rows = $rows->execute();

                foreach ($rows as $row) {
                    $field['name'] = 'count[' . $row->id . ']';
                    $field['fieldLabel'] = $row->title;
                    array_push($fields, $field);
                }

                array_push($fieldsarray, array('general' => $tab['general'], 'fields' => $fields));
            }

            $configarray = $config->general->toArray();
            $configarray['title'] = $translate->translate($configarray['title']);

            $data = false;

            if (intval($this->_request->getParam('uid')) != 0) {

                $data = Doctrine_Query::create()
                    ->from('Printdate a')
                    ->where('a.id = ?')->fetchOne(array(intval($this->_request->getParam('uid'))));
            }
            if (intval($this->_request->getParam('save')) == 1 && intval($this->_request->getParam('uid')) > 0) {

                $params = $this->_request->getParams();

                unset($params['config']);
                unset($params['save']);
                unset($params['module']);
                unset($params['controller']);
                unset($params['action']);
                unset($params['sid']);
                unset($params['uid']);
                unset($params['headingsize']);
                $params['startdate'] = substr($params['startdate'], 6, 4) . '-' . substr($params['startdate'], 3, 2) . '-' . substr($params['startdate'], 0, 2);

                $counts = $params['count'];
                $tempCounts = array();
                foreach ($counts as $key => $var)
                    $tempCounts['count[' . $key . ']'] = $var;
                $data->data = Zend_Json::encode($tempCounts);
                unset($params['count']);

                foreach ($params as $key => $value)
                    $data->$key = stripslashes($value);

                $data->save();
            } elseif (intval($this->_request->getParam('save')) == 1 && intval($this->_request->getParam('uid')) == 0) {

                $params = $this->_request->getParams();

                unset($params['config']);
                unset($params['save']);
                unset($params['module']);
                unset($params['controller']);
                unset($params['action']);
                unset($params['sid']);
                unset($params['uid']);
                unset($params['headingsize']);
                $params['startdate'] = substr($params['startdate'], 6, 4) . '-' . substr($params['startdate'], 4, 2) . '-' . substr($params['startdate'], 0, 2);

                $data = new Printdate();
                $data->shop_id = intval($this->_request->getParam('sid'));
                $data->Install = $user->Install;

                $counts = $params['count'];
                $tempCounts = array();
                foreach ($counts as $key => $var)
                    $tempCounts['count[' . $key . ']'] = $var;
                $data->data = Zend_Json::encode($tempCounts);
                unset($params['count']);

                foreach ($params as $key => $value)
                    $data->$key = $value;

                $data->save();

                $isNew = true;
                $id = $data->id;
            }

            $config = array(
                "success" => true
            , "isNew" => $isNew
            , "uid" => $id
            , "metaData" => array(
                    "fields" => $fieldsarray
                , "formConfig" => $configarray
                , "data" => ($data != false) ? array_merge(Zend_Json::decode($data->data), $data->toArray()) : array()
                )
            , "data" => ($data != false) ? array_merge(Zend_Json::decode($data->data), $data->toArray()) : array()
            );

            $this->getHelper('Json')->sendJson($config);
        }

        $this->_helper->layout->setLayout('popupjs');
    }

}

?>
