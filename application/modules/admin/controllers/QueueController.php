<?php
class Admin_QueueController extends TP_Admin_Controller_Action
{
    /**
     * IndexAction
     *
     * @todo Dokumentation der IndexAction
     *
     * @return void
     */
    public function allAction() {
        if ($this->_request->getParam('config') == 1) {
            $rows = Doctrine_Query::create()->from('Queues m')->limit(intval($this->_request->getParam('limit')))->offset(intval($this->_request->getParam('start')));
            if (intval(str_replace('S', '', $this->_request->getParam('sid'))) != 0) {
                $rows->where('shop_id = ?', array(
                    intval(str_replace('S', '', $this->_request->getParam('sid')))));
            }
            if ($this->_request->getParam('sort')) {
                $rows->orderBy($this->_request->getParam('sort') . ' ' . $this->_request->getParam('dir'));
            }
            $rows = $rows->execute();
            $count = Doctrine_Query::create()->from('Queues m');
            if (intval(str_replace('S', '', $this->_request->getParam('sid'))) != 0) {
                $count->where('shop_id = ?', array(
                    intval(str_replace('S', '', $this->_request->getParam('sid')))));
            }
            $count = $count->execute();
            $list = array();
            $queues = Zend_Registry::get('queues');
            foreach ($rows as $row) {
                $typ = new $queues[$row->typ]();
                $row = $row->toArray(false);
                $row['typName'] = $typ->name;
                array_push($list, $row);
            }
            $config = array(
                "success" => true,
                "totalCount" => $count->count(),
                "rows" => $list);
            $this->getHelper('Json')->sendJson($config);
        }
        if ($this->_request->getParam('config') == 2) {
            $rows = array();
            $queues = Zend_Registry::get('queues');
            foreach ($queues as $key => $queuess) {
                $queue = new $queuess();
                if(!isset($queue->install_id) || (isset($queue->install_id) && $this->install->id == $queue->install_id)) {
                    array_push($rows, array(
                        'name' => $queue->name,
                        'id' => $key));
                }
            }
            $config = array(
                "count" => 2,
                "rows" => $rows);
            $this->getHelper('Json')->sendJson($config);
            unset($articles);
            unset($article);
        }
        $this->view->sid = intval(str_replace('S', '', $this->_request->getParam('id')));
        $this->_helper->layout->setLayout('simple');
    }

    public function editAction() {
        $isNew = false;
        $id = 0;
        if (intval($this->_request->getParam('copy')) == 1) {

            $row = Doctrine_Query::create()
                ->from('Queues a')
                ->where('a.id = ? AND a.shop_id IN (' . implode(',', $this->shopIds) . ')', array($this->_request->getParam('uuid')))
                ->fetchOne();

            $row = $row->copy();
            $row->id = false;
            $row->name = $row->name . ' ' . date('d.m.Y');
            $row->save();

            die($this->getHelper('Json')->sendJson(array('success' => true)));
        }

        if (intval($this->_request->getParam('copymove')) == 1) {

            $orgrow = Doctrine_Query::create()
                ->from('Queues a')
                ->where('a.id = ?', array($this->_request->getParam('uuid')))
                ->fetchOne();

            $row = $orgrow->copy();
            $row->name = $row->name . ' ' . date('d.m.Y');
            $row->id = false;

            if (in_array(intval($this->_request->getParam('target')), $this->shopIds)) {
                $row->shop_id = intval($this->_request->getParam('target'));

                $shop = Doctrine_Query::create()
                    ->from('Shop a')
                    ->where('a.id = ?', array(intval($this->_request->getParam('target'))))
                    ->fetchOne();

                $row->install_id = $shop->install_id;
                $row->save();

            }

            die($this->getHelper('Json')->sendJson(array('success' => true)));
        }
        if (intval($this->_request->getParam('delete')) == 1) {
            Doctrine_Query::create()->from('Queues a')->where('id = ?', array(
                $this->_request->getParam('uid')))->delete()->execute();
            die($this->getHelper('Json')->sendJson(array(
                'success' => true)));
        }
        if (intval($this->_request->getParam('config')) == 3) {
            $queues = Zend_Registry::get('queues');
            $translate = Zend_Registry::get('translate');
            $user = Zend_Auth::getInstance()->getIdentity();
            $user = Doctrine_Query::create()->from('Contact m')->where('id = ?')->fetchOne(array(
                $user['id']));
            $role = null;
            foreach ($user->Roles as $rol) {
                if ($role == null || $rol->level > $role->level) {
                    $role = $rol;
                }
            }
            $queue = new $queues[$this->_request->getParam('type')]();
            $translate->addTranslation(APPLICATION_PATH . '/queues/' . $queue->name . '/locale/', Zend_Registry::get('locale'));
            $translate->setLocale(Zend_Registry::get('locale'));
            $config = new Zend_Config_Ini(APPLICATION_PATH . '/queues/' . $queue->form, $role->name);
            $fieldsarray = array();
            foreach ($config->tabs as $tab) {
                $tab = $tab->toArray();
                $fields = array();
                $tab['general']['title'] = $translate->translate($tab['general']['title']);
                foreach ($tab['fields'] as $key => $field) {
                    $field['fieldLabel'] = $translate->translate($field['fieldLabel']);
                    array_push($fields, $field);
                }
                array_push($fieldsarray, array(
                    'general' => $tab['general'],
                    'fields' => $fields));
            }
            $configarray = $config->general->toArray();
            $configarray['title'] = $translate->translate($configarray['title']);
            if (intval($this->_request->getParam('uid')) != 0) {
                $data = Doctrine_Query::create()->from('Queues a')->where('a.id = ?')->fetchOne(array(
                    intval($this->_request->getParam('uid'))));
            }
            if (intval($this->_request->getParam('save')) == 1 && intval($this->_request->getParam('uid')) > 0) {
                $queue = unserialize($data->data);
                $params = $this->_request->getParams();
                unset($params['config']);
                unset($params['save']);
                unset($params['module']);
                unset($params['controller']);
                unset($params['action']);
                unset($params['sid']);
                unset($params['headingsize']);
                $data->name = $params['title'];
                $data->pos = $params['pos'];
                $data->enable = $params['enable'];
                $data->action = $params['when'];
                if (isset($params['shop']) && $params['shop'] != "") {
                    $data->shop = $params['shop'];
                }
                if(isset($params['product_xml'])) {
                    $data->xml = $params['product_xml'];
                }
                unset($params['product_xml']);
                foreach ($params as $key => $value) {
                    if ($value == 'true' || $value == 'on') {
                        $data->$key = true;
                    } elseif ($value == 'false' || $value == 'off') {
                        $data->$key = false;
                    } else {
                        $queue->$key = $value;
                    }
                }
                $data->typ = $queue->id;
                $data->data = serialize($queue);
                $data->save();
            } elseif (intval($this->_request->getParam('save')) == 1 && intval($this->_request->getParam('uid')) == 0) {
                $params = $this->_request->getParams();
                unset($params['config']);
                unset($params['save']);
                unset($params['module']);
                unset($params['controller']);
                unset($params['action']);
                unset($params['sid']);
                unset($params['headingsize']);
                $data = new Queues();
                $data->shop_id = intval($this->_request->getParam('sid'));
                $data->Install = $user->Install;
                $data->name = $params['title'];
                $data->action = $params['when'];
                $data->pos = $params['pos'];
                $data->enable = $params['enable'];
                $data->typ = $queue->id;
                if ($params['shop'] && $params['shop'] != "") {
                    $data->shop = $params['shop'];
                } else {
                    $data->shop = 0;
                }
                if(isset($params['product_xml'])) {
                    $data->xml = $params['product_xml'];
                }
                unset($params['product_xml']);
                foreach ($params as $key => $value) {
                    if ($value == 'true' || $value == 'on') {
                        $data->$key = true;
                    } elseif ($value == 'false' || $value == 'off') {
                        $data->$key = false;
                    } else {
                        $queue->$key = $value;
                    }
                }
                $data->data = serialize($queue);
                $data->save();
                $isNew = true;
                $id = $data->id;
            }
            if (intval($this->_request->getParam('uid')) != 0) {
                $orgdata = $data;
                $data = unserialize($data->data);
                $data = $data->toArray();
                $data['language_title'] = $this->translate->translate($data['language']);
                $data['product_xml'] = $orgdata->xml;
                $data['enable'] = $orgdata->enable;
            } else {
                $data = array();
            }
            $config = array(
                "success" => true,
                "isNew" => $isNew,
                "uid" => $id,
                "metaData" => array(
                    "fields" => $fieldsarray,
                    "formConfig" => $configarray,
                    "data" => $data),
                "data" => $data);
            $this->getHelper('Json')->sendJson($config);
        }
        $this->_helper->layout->setLayout('popupjs');
    }
}