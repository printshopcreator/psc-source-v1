<?php

class Admin_ShippingController extends TP_Admin_Controller_Action
{
    public function allAction() {

        if ($this->_request->getParam('config') == 1) {

            $rows = Doctrine_Query::create()
                ->from('Shippingtype m')
                ->limit(intval($this->_request->getParam('limit')))
                ->offset(intval($this->_request->getParam('start')));
            if (intval(str_replace('S', '', $this->_request->getParam('sid'))) != 0) {
                $rows->where('shop_id = ?', array(intval(str_replace('S', '', $this->_request->getParam('sid')))));
            }
            if ($this->_request->getParam('sort')) {
                $rows->orderBy($this->_request->getParam('sort') . ' ' . $this->_request->getParam('dir'));
            }

            $rows = $rows->execute();

            $count = Doctrine_Query::create()
                ->from('Shippingtype m');
            if (intval(str_replace('S', '', $this->_request->getParam('sid'))) != 0) {
                $count->where('shop_id = ?', array(intval(str_replace('S', '', $this->_request->getParam('sid')))));
            }

            $count = $count->execute();

            $config = array(
                "success" => true
            , "totalCount" => $count->count()
            , "rows" => $rows->toArray()

            );

            $this->getHelper('Json')->sendJson($config);
        }

        $this->view->sid = intval(str_replace('S', '', $this->_request->getParam('id')));

        $this->_helper->layout->setLayout('simple');

    }

    public function editAction() {

        $isNew = false;
        $id = 0;

        if (intval($this->_request->getParam('delete')) == 1) {

            Doctrine_Query::create()
                ->from('Shippingtype a')
                ->where('id = ?', array($this->_request->getParam('uid')))
                ->delete()
                ->execute();

            die($this->getHelper('Json')->sendJson(array('success' => true)));
        }

        if (intval($this->_request->getParam('config')) == 3) {

            $translate = Zend_Registry::get('translate');

            $user = Zend_Auth::getInstance()->getIdentity();
            $user = Doctrine_Query::create()
                ->from('Contact m')
                ->where('id = ?')->fetchOne(array($user['id']));
            $role = null;
            foreach ($user->Roles as $rol) {
                if ($role == null || $rol->level > $role->level) {
                    $role = $rol;
                }
            }

            $config = new Zend_Config_Ini(APPLICATION_PATH . '/modules/admin/config/' . 'shipping/settings.ini', $role->name);

            $fieldsarray = array();
            foreach ($config->tabs as $tab) {
                $tab = $tab->toArray();
                $fields = array();
                $tab['general']['title'] = $translate->translate($tab['general']['title']);
                foreach ($tab['fields'] as $key => $field) {
                    $field['fieldLabel'] = $translate->translate($field['fieldLabel']);
                    array_push($fields, $field);
                }

                array_push($fieldsarray, array('general' => $tab['general'], 'fields' => $fields));
            }

            $configarray = $config->general->toArray();
            $configarray['title'] = $translate->translate($configarray['title']);

            if (intval($this->_request->getParam('uid')) != 0) {

                $data = Doctrine_Query::create()
                    ->from('Shippingtype a')
                    ->where('a.id = ?')->fetchOne(array(intval($this->_request->getParam('uid'))));
                $data->mwert = $data->mwert . '%';
            } else {
                $data = new Shippingtype();
            }

            if (intval($this->_request->getParam('save')) == 1 && intval($this->_request->getParam('uid')) > 0) {

                $params = $this->_request->getParams();

                unset($params['config']);
                unset($params['save']);
                unset($params['module']);
                unset($params['controller']);
                unset($params['action']);
                unset($params['sid']);
                unset($params['uid']);
                unset($params['headingsize']);
                $filter = new TP_Filter_Badwords ();
                foreach ($params as $key => $value) {
                    if ($value == 'true' || $value == 'on') {
                        $data->$key = true;
                    } elseif ($value == 'false' || $value == 'off') {
                        $data->$key = false;
                    }
                    else {
                        $data->$key = $filter->filter($value);
                    }
                }

                $data->save();

            } elseif (intval($this->_request->getParam('save')) == 1 && intval($this->_request->getParam('uid')) == 0) {

                $params = $this->_request->getParams();

                unset($params['config']);
                unset($params['save']);
                unset($params['module']);
                unset($params['controller']);
                unset($params['action']);
                unset($params['sid']);
                unset($params['uid']);
                unset($params['headingsize']);
                //TODO: UPLOAD Articlegroup
                unset($params['file']);
                //
                $data = new Shippingtype();
                $data->shop_id = intval($this->_request->getParam('sid'));
                $data->Install = $user->Install;
                $filter = new TP_Filter_Badwords ();
                foreach ($params as $key => $value) {
                    if ($value == 'true' || $value == 'on') {
                        $data->$key = true;
                    } elseif ($value == 'false' || $value == 'off') {
                        $data->$key = false;
                    }
                    else {
                        $data->$key = $filter->filter($value);
                    }
                }

                $data->save();

                $isNew = true;
                $id = $data->id;
            }

            $config = array(
                "success" => true
            , "isNew" => $isNew
            , "uid" => $id
            , "metaData" => array(
                    "fields" => $fieldsarray
                , "formConfig" => $configarray
                , "data" => ($data != false) ? $data->toArray() : array()
                )
            , "data" => ($data != false) ? $data->toArray() : array()
            );
            $this->getHelper('Json')->sendJson($config);
        }

        $this->_helper->layout->setLayout('popupjs');

    }

}
