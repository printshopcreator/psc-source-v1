<?php

class Admin_ShopController extends TP_Admin_Controller_Action
{

    public function init() {
        parent::init();

        $contextSwitch = $this->_helper->contextSwitch();

        $contextSwitch
            ->addActionContext('fetchmotivthemes', self::CONTEXT_JSON)
            ->addActionContext('liveedit', self::CONTEXT_JSON)
            ->setAutoJsonSerialization(true)
            ->initContext();
    }

    public function liveeditAction() {

        $row = Doctrine_Query::create()
            ->from('Shop m')
            ->addWhere('id IN (' . implode(',', $this->shopIds) . ') AND uid = ?', array($this->_getParam('id')))->fetchOne();

        if (!$row) {
            $this->view->success = false;
            return;
        }
        $field = $this->_getParam('field');
        if ($row->getTable()->hasField($field)) {
            $row->$field = $this->_getParam('content');
            $row->save();

            $this->view->success = true;
            return;
        }

        $this->view->success = false;
        return;
    }

    public function savesettingsAction() {

        if (!in_array($this->shop->id, $this->shopIds, true)) {
            return;
        }

        $row = Doctrine_Query::select()->from('Shopsetting s')->where('s.shop_id = ?', array($this->_getParam('index')))->fetchOne();
        if (!$row) {
            $row = new Shopsetting();
            $row->shop_id = $this->shop->id;
            $row->index = $this->_getParam('index');
        }

        $row->value = $this->_getParam('value');
        $row->save();

        $this->view->success = true;
    }

    public function createthemeshopAction() {
        $this->_helper->layout->setLayout('simple');
    }

    public function indexAction() {

    }

    public function settingsAction() {

        if (intval($this->_request->getParam('config')) == 1) {

            $translate = Zend_Registry::get('translate');

            $user = Zend_Auth::getInstance()->getIdentity();
            $user = Doctrine_Query::create()
                ->from('Contact m')
                ->where('id = ?')->fetchOne(array($user['id']));
            $role = null;

            foreach ($user->Roles as $rol) {
                if ($role == null || $rol->level > $role->level) {
                    $role = $rol;
                }
            }

            $config = new Zend_Config_Ini(APPLICATION_PATH . '/modules/admin/config/' . 'shop/settings.ini', $role->name);

            $fieldsarray = array();
            foreach ($config->tabs as $tab) {
                $tab = $tab->toArray();
                $fields = array();
                foreach ($tab['fields'] as $key => $field) {

                    if (isset($field['fieldLabel'])) {
                        $field['fieldLabel'] = $translate->translate($field['fieldLabel']);
                    }
                    if (isset($field['items'])) {
                        $tempitems = array();
                        foreach ($field['items'] as $subit) {
                            $subit ['fieldLabel'] = $translate->translate($subit ['fieldLabel']);
                            array_push($tempitems, $subit);
                            //array_push ( $fieldkeys, $subit ['name'] );
                        }
                        $field['items'] = $tempitems;
                    }

                    array_push($fields, $field);
                }

                array_push($fieldsarray, array('general' => $tab['general'], 'fields' => $fields));
            }

            $configarray = $config->general->toArray();
            $configarray['title'] = $translate->translate($configarray['title']);

            $data = Doctrine_Query::create()
                ->from('Shop s')
                ->where('id = ?')->fetchOne(array(str_replace('S', '', $this->_request->getParam('sid'))));

            if (intval($this->_request->getParam('id')) != 0) {

                $data = Doctrine_Query::create()
                    ->from('Account a')
                    ->where('a.id = ?')->fetchOne(array(intval($this->_request->getParam('id'))));
            }
            if (intval($this->_request->getParam('save')) == 1) {

                $params = $this->_request->getParams();

                if (isset($params['releatedthemes'])) {
                    $releatedthemes = implode(',', array_slice($params['releatedthemes'], 0, count($params['releatedthemes']) - 1));
                }
                unset($params['releatedthemes']);
                if (isset($params ['shippingtype'])) {
                    $shippingtype = implode(',', array_slice($params ['shippingtype'], 0, count($params ['shippingtype']) - 1));
                    unset($params ['shippingtype']);
                }

                unset($params['config']);
                unset($params['save']);
                unset($params['module']);
                unset($params['controller']);
                unset($params['action']);
                unset($params['sid']);
                unset($params['headingsize']);
                $filter = new TP_Filter_Badwords();
                foreach ($params as $key => $value) {
                    if (isset($data[$key]) && false == strpos($value, 'service/index?type=upload&mode=get')) {
                        if ($value == 'true') {
                            $data->$key = true;
                        } elseif ($value == 'false') {
                            $data->$key = false;
                        } else {
                            $data->$key = $filter->filter($value);
                        }
                    }
                }

                $data->save();

                if (isset($releatedthemes)) {
                    Doctrine_Query::create()
                        ->delete()
                        ->from('ShopThemeShop as a')
                        ->where('a.shop_id = ?', array(intval($data->id)))
                        ->execute();

                    if ($releatedthemes != '') {
                        foreach (explode(',', $releatedthemes) as $key) {
                            $group = new ShopThemeShop();
                            $group->theme_id = $key;
                            $group->shop_id = $data->id;
                            $group->save();
                        }
                    }
                }
                if (isset($shippingtype)) {

                    Doctrine_Query::create()->delete()->from('ShopShippingtype as a')->where('a.shop_id = ?', array(intval($data->id)))->execute();

                    if ($shippingtype != '') {
                        foreach (explode(',', $shippingtype) as $key) {
                            $group = new ShopShippingtype();
                            $group->shop_id = $data->id;
                            $group->shippingtype_id = $key;
                            $group->save();
                        }
                    }
                }
            }

            $data->formel = str_replace('\\', '', $data->formel);
            $data->parameter = str_replace('\\', '', $data->parameter);

            $temp2 = array();
            foreach ($data->ShopThemeShop as $row) {
                array_push($temp2, $row->theme_id);
            }

            $temps = array();
            foreach ($data->ShopShippingtype as $row) {
                array_push($temps, $row->shippingtype_id);
            }

            $data = $data->toArray(false);

            $data['releatedthemes[]'] = implode(',', $temp2);

            if ($data['template_display_help_cms'] != "") {
                $cms = Doctrine_Query::create()
                    ->from('Cms s')
                    ->where('id = ?')->fetchOne(array($data['template_display_help_cms']));

                if ($cms != false) {
                    $cmstitle = $cms->title;
                } else {
                    $cmstitle = "";
                }
            } else {
                $cmstitle = "";
            }

            if ($data['template_display_uploadcenter'] != "") {
                $cms = Doctrine_Query::create()
                    ->from('Cms s')
                    ->where('id = ?')->fetchOne(array($data['template_display_uploadcenter']));

                if ($cms != false) {
                    $cmsuploadtitle = $cms->title;
                } else {
                    $cmsuploadtitle = "";
                }
            } else {
                $cmsuploadtitle = "";
            }

            if ($data['template_display_lead_box'] != "") {
                $cms = Doctrine_Query::create()
                    ->from('Cms s')
                    ->where('id = ?')->fetchOne(array($data['template_display_lead_box']));

                if ($cms != false) {
                    $cmsleadbox = $cms->title;
                } else {
                    $cmsleadbox = "";
                }
            } else {
                $cmsleadbox = "";
            }

            if ($data['template_display_orders'] != "") {
                $cms = Doctrine_Query::create()
                    ->from('Cms s')
                    ->where('id = ?')->fetchOne(array($data['template_display_orders']));

                if ($cms != false) {
                    $cmsordertitle = $cms->title;
                } else {
                    $cmsordertitle = "";
                }
            } else {
                $cmsordertitle = "";
            }
            if ($data['slider_cms_1'] != "") {
                $cms = Doctrine_Query::create()
                    ->from('Cms s')
                    ->where('id = ?')->fetchOne(array($data['slider_cms_1']));

                if ($cms != false) {
                    $cmsslidercms1 = $cms->title;
                } else {
                    $cmsslidercms1 = "";
                }
            } else {
                $cmsslidercms1 = "";
            }
            if ($data['slider_cms_2'] != "") {
                $cms = Doctrine_Query::create()
                    ->from('Cms s')
                    ->where('id = ?')->fetchOne(array($data['slider_cms_2']));

                if ($cms != false) {
                    $cmsslidercms2 = $cms->title;
                } else {
                    $cmsslidercms2 = "";
                }
            } else {
                $cmsslidercms2 = "";
            }
            if ($data['slider_cms_3'] != "") {
                $cms = Doctrine_Query::create()
                    ->from('Cms s')
                    ->where('id = ?')->fetchOne(array($data['slider_cms_3']));

                if ($cms != false) {
                    $cmsslidercms3 = $cms->title;
                } else {
                    $cmsslidercms3 = "";
                }
            } else {
                $cmsslidercms3 = "";
            }
            if ($data['slider_cms_4'] != "") {
                $cms = Doctrine_Query::create()
                    ->from('Cms s')
                    ->where('id = ?')->fetchOne(array($data['slider_cms_4']));

                if ($cms != false) {
                    $cmsslidercms4 = $cms->title;
                } else {
                    $cmsslidercms4 = "";
                }
            } else {
                $cmsslidercms4 = "";
            }
            if ($data['slider_cms_5'] != "") {
                $cms = Doctrine_Query::create()
                    ->from('Cms s')
                    ->where('id = ?')->fetchOne(array($data['slider_cms_5']));

                if ($cms != false) {
                    $cmsslidercms5 = $cms->title;
                } else {
                    $cmsslidercms5 = "";
                }
            } else {
                $cmsslidercms5 = "";
            }
            if ($data['slider_a_1'] != "") {
                $cms = Doctrine_Query::create()
                    ->from('Article s')
                    ->where('id = ?')->fetchOne(array($data['slider_a_1']));

                if ($cms != false) {
                    $cmsslidera1 = $cms->title;
                } else {
                    $cmsslidera1 = "";
                }
            } else {
                $cmsslidera1 = "";
            }
            if ($data['slider_a_2'] != "") {
                $cms = Doctrine_Query::create()
                    ->from('Article s')
                    ->where('id = ?')->fetchOne(array($data['slider_a_2']));

                if ($cms != false) {
                    $cmsslidera2 = $cms->title;
                } else {
                    $cmsslidera2 = "";
                }
            } else {
                $cmsslidera2 = "";
            }
            if ($data['slider_a_3'] != "") {
                $cms = Doctrine_Query::create()
                    ->from('Article s')
                    ->where('id = ?')->fetchOne(array($data['slider_a_3']));

                if ($cms != false) {
                    $cmsslidera3 = $cms->title;
                } else {
                    $cmsslidera3 = "";
                }
            } else {
                $cmsslidera3 = "";
            }
            if ($data['slider_a_4'] != "") {
                $cms = Doctrine_Query::create()
                    ->from('Article s')
                    ->where('id = ?')->fetchOne(array($data['slider_a_4']));

                if ($cms != false) {
                    $cmsslidera4 = $cms->title;
                } else {
                    $cmsslidera4 = "";
                }
            } else {
                $cmsslidera4 = "";
            }
            if ($data['slider_a_5'] != "") {
                $cms = Doctrine_Query::create()
                    ->from('Article s')
                    ->where('id = ?')->fetchOne(array($data['slider_a_5']));

                if ($cms != false) {
                    $cmsslidera5 = $cms->title;
                } else {
                    $cmsslidera5 = "";
                }
            } else {
                $cmsslidera5 = "";
            }

            if ($data['template_lead_product_1'] != "") {
                $cms = Doctrine_Query::create()
                    ->from('Article s')
                    ->where('id = ?')->fetchOne(array($data['template_lead_product_1']));

                if ($cms != false) {
                    $templatelead1 = $cms->title;
                } else {
                    $templatelead1 = "";
                }
            } else {
                $templatelead1 = "";
            }

            if ($data['template_lead_product_2'] != "") {
                $cms = Doctrine_Query::create()
                    ->from('Article s')
                    ->where('id = ?')->fetchOne(array($data['template_lead_product_2']));

                if ($cms != false) {
                    $templatelead2 = $cms->title;
                } else {
                    $templatelead2 = "";
                }
            } else {
                $templatelead2 = "";
            }

            if ($data['template_lead_product_3'] != "") {
                $cms = Doctrine_Query::create()
                    ->from('Article s')
                    ->where('id = ?')->fetchOne(array($data['template_lead_product_3']));

                if ($cms != false) {
                    $templatelead3 = $cms->title;
                } else {
                    $templatelead3 = "";
                }
            } else {
                $templatelead3 = "";
            }

            if ($data['template_lead_product_4'] != "") {
                $cms = Doctrine_Query::create()
                    ->from('Article s')
                    ->where('id = ?')->fetchOne(array($data['template_lead_product_4']));

                if ($cms != false) {
                    $templatelead4 = $cms->title;
                } else {
                    $templatelead4 = "";
                }
            } else {
                $templatelead4 = "";
            }

            if ($data['template_lead_product_5'] != "") {
                $cms = Doctrine_Query::create()
                    ->from('Article s')
                    ->where('id = ?')->fetchOne(array($data['template_lead_product_5']));

                if ($cms != false) {
                    $templatelead5 = $cms->title;
                } else {
                    $templatelead5 = "";
                }
            } else {
                $templatelead5 = "";
            }

            $config = new Zend_Config_Ini(APPLICATION_PATH . '/configs/' . '/systemdefaults.ini', 'land');
            $config = $config->toArray();
            $data['landtitle'] = $config[$data['land']];
            $config = new Zend_Config_Ini(APPLICATION_PATH . '/configs/' . '/systemdefaults.ini', 'motivstatus');
            $config = $config->toArray();
            $data['defaultmotivstatustitle'] = $translate->translate($config[$data['default_motiv_status']]);
            if (strpos('.' . $data['default_motiv_status'], "/")) {
                $data['layout_title'] = substr(strrchr($data['layout'], "/"), 1);
            } else {
                $data['layout_title'] = $data['layout'];
            }
            $data['template_display_help_cms_title'] = $cmstitle;
            $data['template_display_uploadcenter_title'] = $cmsuploadtitle;
            $data['template_display_orders_title'] = $cmsordertitle;
            $data['template_display_lead_box_title'] = $cmsleadbox;
            $data['shippingtype[]'] = implode(',', $temps);
            $data['slider_a_1_title'] = $cmsslidera1;
            $data['slider_a_2_title'] = $cmsslidera2;
            $data['slider_a_3_title'] = $cmsslidera3;
            $data['slider_a_4_title'] = $cmsslidera4;
            $data['slider_a_5_title'] = $cmsslidera5;

            $data['slider_cms_1_title'] = $cmsslidercms1;
            $data['slider_cms_2_title'] = $cmsslidercms2;
            $data['slider_cms_3_title'] = $cmsslidercms3;
            $data['slider_cms_4_title'] = $cmsslidercms4;
            $data['slider_cms_5_title'] = $cmsslidercms5;

            $data['template_lead_product_1_title'] = $templatelead1;
            $data['template_lead_product_2_title'] = $templatelead2;
            $data['template_lead_product_3_title'] = $templatelead3;
            $data['template_lead_product_4_title'] = $templatelead4;
            $data['template_lead_product_5_title'] = $templatelead5;

            $config = array(
                "success" => true
            , "metaData" => array(
                    "fields" => $fieldsarray
                , "formConfig" => $configarray
                , "data" => $data
                )
            , "data" => $data
            );

            $this->getHelper('Json')->sendJson($config);
        }

        $this->view->id = $this->_request->getParam('id');

        $this->_helper->layout->setLayout('simple');
    }

}
