<?php

class Admin_StatsController extends TP_Admin_Controller_Action
{

    public function init() {
        parent::init();
        $contextSwitch = $this->_helper->contextSwitch();
        $contextSwitch->addActionContext('fetchbooking', self::CONTEXT_JSON)
            ->setAutoJsonSerialization(true)->initContext();
    }

    public function allAction() {

        $this->view->stats = array();
        if ($this->highRole->level >= 40) {
            foreach ($this->shopIds as $id) {

                $shop = Doctrine_Query::create()->select('s.name')->from('Shop s')->where('s.id = ?', array($id))->fetchOne();

                $rows = Doctrine_Query::create()->select('o.created, s.Name, month(o.created) as month, sum(o.preisbrutto) as sum')->from('Orders o')
                    ->leftJoin('o.Shop as s')
                    ->where('s.id = ? AND o.created >= ? AND o.created <= ?', array($id, date('Y') . '-01-01', date('Y') . '-12-31'))->groupBy('month(o.created)')->execute();

                $articles = Doctrine_Query::create()->select('a.title as title, a.visits as visits')->from('Article a')
                    ->where('a.shop_id = ? AND a.private = 0 AND a.enable = 1', array($id))->orderBy('a.visits DESC')->limit(10)->execute();

                $articlesbuyed = Doctrine_Query::create()->select('o.createdd, a.title, count(*) as sum')->from('Orderspos o')
                    ->leftJoin('o.Article as a')
                    ->where('o.shop_id = ? AND a.title != "" AND o.createdd >= ? AND o.createdd <= ?', array($id, date('Y') . '-01-01', date('Y') . '-12-31'))->groupBy('o.article_id')
                    ->orderBy('sum DESC')->limit(10)
                    ->execute();

                if (count($rows) >= 1) {
                    if (!isset($this->view->stats[$id])) {
                        $this->view->stats[$id] = array();
                    }
                    $this->view->stats[$id]['umst'] = $rows;
                }

                if (count($articles) >= 1) {
                    if (!isset($this->view->stats[$id])) {
                        $this->view->stats[$id] = array();
                    }
                    $this->view->stats[$id]['top_article'] = $articles;
                }

                if (count($articlesbuyed) >= 1) {
                    if (!isset($this->view->stats[$id])) {
                        $this->view->stats[$id] = array();
                    }
                    $this->view->stats[$id]['top_article_buy'] = $articlesbuyed;
                }

                if (isset($this->view->stats[$id])) {
                    $this->view->stats[$id]['shop'] = $shop;
                }
            }
        } else {
            foreach ($this->shopIds as $id) {

                $shop = Doctrine_Query::create()->select('s.name')->from('Shop s')->where('s.id = ?', array($id))->fetchOne();

                $rows = Doctrine_Query::create()->select('o.created, s.Name, month(o.created) as month, sum(o.payment_value) as sum')->from('Booking o')
                    ->leftJoin('o.Shop as s')
                    ->where('s.id = ? AND o.booking_typ = 1 AND o.created >= ? AND o.created <= ?', array($id, date('Y') . '-01-01 00:00:00', date('Y') . '-12-31 23:59:59'))->groupBy('month(o.created)')->execute();

                $articles = Doctrine_Query::create()->select('a.title as title, a.visits as visits')->from('Article a')
                    ->where('a.shop_id = ? AND a.private = 0 AND a.enable = 1', array($id))->orderBy('a.visits DESC')->limit(10)->execute();

                $articlesbuyed = Doctrine_Query::create()->select('o.createdd, a.title, count(*) as sum')->from('Orderspos o')
                    ->leftJoin('o.Article as a')
                    ->where('o.shop_id = ? AND a.title != "" AND o.createdd >= ? AND o.createdd <= ?', array($id, date('Y') . '-01-01', date('Y') . '-12-31'))->groupBy('o.article_id')
                    ->orderBy('sum DESC')->limit(10)
                    ->execute();

                if (count($rows) >= 1) {
                    if (!isset($this->view->stats[$id])) {
                        $this->view->stats[$id] = array();
                    }
                    $this->view->stats[$id]['umst'] = $rows;
                }

                if (count($articles) >= 1) {
                    if (!isset($this->view->stats[$id])) {
                        $this->view->stats[$id] = array();
                    }
                    $this->view->stats[$id]['top_article'] = $articles;
                }

                if (count($articlesbuyed) >= 1) {
                    if (!isset($this->view->stats[$id])) {
                        $this->view->stats[$id] = array();
                    }
                    $this->view->stats[$id]['top_article_buy'] = $articlesbuyed;
                }

                if (isset($this->view->stats[$id])) {
                    $this->view->stats[$id]['shop'] = $shop;
                }
            }
        }
    }

}
