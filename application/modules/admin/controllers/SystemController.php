<?php

class Admin_SystemController extends TP_Admin_Controller_Action
{

    public function init() {
        parent::init();
        $contextSwitch = $this->_helper->contextSwitch();
        $contextSwitch->addActionContext('fetchtranslation', self::CONTEXT_JSON);
        $contextSwitch->addActionContext('liveedit', self::CONTEXT_JSON)
            ->addActionContext('savesettings', self::CONTEXT_JSON)
            ->setAutoJsonSerialization(true)->initContext();
    }

    public function usersettingsAction() {
        $this->view->success = true;
    }

    public function savesettingsAction() {

        if (!in_array($this->shop->id, $this->shopIds, true)) {
            return;
        }

        $row = Doctrine_Query::Create()->from('Shopsetting as s')->where('s.shop_id = ? AND s.keyid = ?', array($this->shop->id, $this->_getParam('index')))->fetchOne();
        if (!$row) {
            $row = new Shopsetting();
            $row->shop_id = $this->shop->id;
            $row->keyid = $this->_getParam('index');
        }

        $row->value = $this->_getParam('value');
        $row->save();

        $this->view->success = true;
    }

    public function liveeditAction() {

        $row = Doctrine_Query::create()
            ->from('Frontendtranslation m')
            ->addWhere('shop_id IN (' . implode(',', $this->shopIds) . ') AND shop_id = ? AND message_id = ?', array($this->shop->id, $this->_getParam('id')))->fetchOne();

        if (!in_array($this->shop->id, $this->shopIds)) {
            $this->view->success = false;
            return;
        }

        $locale = Zend_Registry::get('locale');

        if (!$row) {

            $row = new Frontendtranslation();
            $row->shop_id = $this->shop->id;
            $row->message_id = $this->_getParam('id');
            $row->locale = $locale->getLanguage();
            $row->text = $this->_getParam('content');
            $row->save();

            $this->view->success = true;
            return;
        }

        $row->text = $this->_getParam('content');
        $row->save();

        $this->view->success = true;
        return;
    }

    public function fetchtranslationAction() {

        if ($this->_getParam('mode', '') == 'update') {

            $data = Zend_Json::decode($this->_getParam('rows'));

            $row = Doctrine_Query::create()->from('Admintranslation m')->where('m.id = ?', array($data['id']))->fetchOne();
            $row->de = $data['de'];
            $row->en = $data['en'];
            $row->save();
        }

        if ($this->_getParam('mode', '') == 'create') {

            $data = Zend_Json::decode($this->_getParam('rows'));

            $row = new Admintranslation();
            $row->id = $data['id'];
            $row->de = $data['de'];
            $row->en = $data['en'];
            $row->save();
        }

        $rows = Doctrine_Query::create()->from('Admintranslation m')->limit(intval($this->_request->getParam('limit')))->offset(intval($this->_request->getParam('start')));
        if ($this->_request->getParam('sort')) {
            $rows->orderBy($this->_request->getParam('sort') . ' ' . $this->_request->getParam('dir'));
        }

        TP_Filter::proccess($rows);
        $rows = $rows->execute();
        $count = Doctrine_Query::create()->from('Admintranslation m');
        TP_Filter::proccess($count);
        $count = $count->execute();

        $temp = array();

        $this->view->success = true;
        $this->view->totalCount = $count->count();
        $this->view->rows = $rows->toArray();
    }

    public function translationAction() {

    }

    public function domainsAction() {

        if ($this->_request->getParam('task') == 'updategs') {

            if (intval($this->_request->getParam('uid')) == 0) {
                $domain = new Domain();
                $domain->shop_id = intval($this->_request->getParam('sid'));
                $domain->name = $this->_request->getParam('value');
                $domain->save();

                die($this->getHelper('Json')->sendJson(array('newID' => $domain->id)));
            } else {

                $domain = Doctrine_Query::create()
                    ->from('Domain m')
                    ->where('m.id = ?', array($this->_request->getParam('uid')))
                    ->fetchOne();

                $domain->name = $this->_request->getParam('value');
                $domain->save();

                die($this->getHelper('Json')->sendJson(array('newID' => $domain->id)));
            }
        }

        if ($this->_request->getParam('config') == 1) {

            $rows = Doctrine_Query::create()
                ->from('Domain m')
                ->where('m.shop_id = ?', array($this->_request->getParam('uid')));

            $rows = $rows->execute();

            $config = array(
                "success" => true
            , "totalCount" => $rows->count()
            , "rows" => $rows->toArray(false)
            );

            $this->getHelper('Json')->sendJson($config);
        }

        if ($this->_request->getParam('uid') && $this->_request->getParam('delete') == 1) {

            $rows = Doctrine_Query::create()
                ->from('Domain m')
                ->where('m.id = ?', array($this->_request->getParam('uid')))
                ->delete();

            $rows = $rows->execute();

            $config = array(
                "success" => true
            );

            $this->getHelper('Json')->sendJson($config);
        }

        $this->view->sid = intval(str_replace('S', '', $this->_request->getParam('id')));

        $this->_helper->layout->setLayout('popupjs');
    }

    public function shopsAction() {

        if (intval($this->getRequest()->getParam('copy')) == 1) {

            $shop = Doctrine_Query::create()
                ->from('Shop m')
                ->where('m.id = ?', array(intval($this->getRequest()->getParam('uid'))))
                ->fetchOne();

            $shopnew = $shop->copy(false);

            $shopid = $shop->uid;

            $shopnew->uid = time();

            $this->CopyDirectory(APPLICATION_PATH . '/design/clients/' . $shopid . '/', APPLICATION_PATH . '/design/clients/' . $shopnew->uid . '/');

            $this->CopyDirectory('uploads/' . $shopid . '/', 'uploads/' . $shopnew->uid . '/', true);

            $this->CopyDirectory('shops/' . $shopid . '/', 'shops/' . $shopnew->uid . '/', true);

            $this->CopyDirectory('styles/' . $shopid . '/', 'styles/' . $shopnew->uid . '/', true);

            $shopnew->save();

            $prevs = Doctrine_Query::create()
                ->from('ResourcePrivilege m')
                ->where('m.treeparent LIKE \'%,%\'')
                ->execute();
            foreach ($prevs as $prev) {
                $prev->treeparent = $prev->treeparent . ',S' . $shopnew->id;
                $prev->save();
            }

            $shopnew->save();

            $shopcontact = new ShopContact();
            $shopcontact->shop_id = $shopnew->id;
            $shopcontact->contact_id = $this->user->id;
            $shopcontact->save();

            $config = array(
                "success" => true
            );

            $this->getHelper('Json')->sendJson($config);
        }

        if ($this->_request->getParam('config') == 1) {

            $rows = Doctrine_Query::create()
                ->from('Shop m')
                ->limit(intval($this->_request->getParam('limit')))
                ->offset(intval($this->_request->getParam('start')))
                ->where('m.install_id = ?', array($this->shop->Install->id));
            if ($this->_request->getParam('sort')) {
                $rows->orderBy($this->_request->getParam('sort') . ' ' . $this->_request->getParam('dir'));
            }

            $rows = $rows->execute();

            $temp = array();
            foreach ($rows as $row) {
                $domain = $row->Domain[0]->name;
                $rowf = $row->toArray(false);
                $rowf['domain'] = $domain;

                if ($row->pmb == true) {
                    $rowf['typ'] = 'Marktplatz';
                } elseif ($row->pmb == false && $row->market == true && $row->private == false) {
                    $rowf['typ'] = 'Offener Themenmarktplatz';
                } elseif ($row->pmb == false && $row->market == true && $row->private == true) {
                    $rowf['typ'] = 'Passwortgeschützter Mandantenshop';
                } elseif ($row->pmb == false && $row->market == false && $row->private == true) {
                    $rowf['typ'] = 'eigenständiger, passwortgeschützter Mandantenshop (MyWeb2Print)';
                } else {
                    $rowf['typ'] = 'eigenständiger, offener Printshop (MyWeb2Print)';
                }

                $rowf['contact'] = $row->Creator->self_firstname . ' ' . $row->Creator->self_lastname . ' ' . $row->Creator->self_email;

                array_push($temp, $rowf);
            }

            $count = Doctrine_Query::create()
                ->from('Shop m')
                ->where('m.install_id = ?', array($this->shop->Install->id));

            $count = $count->execute();

            $config = array(
                "success" => true
            , "totalCount" => $count->count()
            , "rows" => $temp
            );

            $this->getHelper('Json')->sendJson($config);
        }

        $this->view->sid = intval(str_replace('S', '', $this->_request->getParam('id')));
    }

    private function CopyDirectory($SourceDirectory, $TargetDirectory, $onlyDir = false) {

        // add trailing slashes
        if (substr($SourceDirectory, -1) != '/') {
            $SourceDirectory .= '/';
        }
        if (substr($TargetDirectory, -1) != '/') {
            $TargetDirectory .= '/';
        }

        $handle = @opendir($SourceDirectory);
        if (!$handle) {
            die("Das Verzeichnis $SourceDirectory konnte nicht geöffnet werden.");
        }

        if (!is_dir($TargetDirectory)) {
            mkdir($TargetDirectory);
            chmod($TargetDirectory, 0777);
        }

        $entry = readdir($handle);
        while ($entry) {
            if ($entry[0] == '.') {
                continue;
            }

            if (is_dir($SourceDirectory . $entry)) {
                // Unterverzeichnis
                $this->CopyDirectory($SourceDirectory . $entry, $TargetDirectory . $entry, $onlyDir);
            } else {
                if ($onlyDir == false) {
                    $target = $TargetDirectory . $entry;
                    copy($SourceDirectory . $entry, $target);
                    chmod($target, 0777);
                }
            }
        }
        return true;
    }

}
