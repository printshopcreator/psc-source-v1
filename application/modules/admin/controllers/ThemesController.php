<?php
class Admin_ThemesController extends TP_Admin_Controller_Action
{
    public function init() {
        if (!$this->_request->getParam('sid', false)) {
            $wizard = new TP_ResaleWizard();
            $this->_request->setParam('sid', $wizard->getArticleShop());
        }
        parent::init();
        $contextSwitch = $this->_helper->contextSwitch();
        $contextSwitch->addActionContext('fetchmotivthemes', self::CONTEXT_JSON)->addActionContext('fetcharticlethemes', self::CONTEXT_JSON)->addActionContext('fetchshopthemes', self::CONTEXT_JSON)->addActionContext('loadsavearticletheme', self::CONTEXT_JSON)->addActionContext('loadsavemotivtheme', self::CONTEXT_JSON)->addActionContext('loadsaveshoptheme', self::CONTEXT_JSON)->addActionContext('deletemotivtheme', self::CONTEXT_JSON)->addActionContext('deletearticletheme', self::CONTEXT_JSON)->addActionContext('deleteshoptheme', self::CONTEXT_JSON)->setAutoJsonSerialization(true)->initContext();
    }

    public function allAction() {
    }

    public function deletemotivthemeAction() {
        if (intval($this->_request->getParam('delete')) == 1) {
            $motiv = Doctrine_Query::create()->from('MotivTheme a')->where('id = ?', array(
                $this->_request->getParam('uid')))->fetchOne();
            $motiv->delete();
            $this->view->success = true;
        }
    }

    public function deleteshopthemeAction() {
        if (intval($this->_request->getParam('delete')) == 1) {
            Doctrine_Query::create()->from('ShopTheme a')->where('id = ?', array(
                $this->_request->getParam('uid')))->delete()->execute();
            $this->view->success = true;
        }
    }

    public function deletearticlethemeAction() {
        if (intval($this->_request->getParam('delete')) == 1) {
            Doctrine_Query::create()->from('ArticleTheme a')->where('id = ?', array(
                $this->_request->getParam('uid')))->delete()->execute();
            $this->view->success = true;
        }
    }

    public function fetchmotivthemesAction() {
        $rows = Doctrine_Query::create()->from('MotivTheme m')->limit(intval($this->_request->getParam('limit')))->offset(intval($this->_request->getParam('start')));
        if (intval(str_replace('S', '', $this->_request->getParam('sid'))) != 0) {
            $rows->where('shop_id = ?', array(
                intval(str_replace('S', '', $this->_request->getParam('sid')))));
        }
        if ($this->_request->getParam('sort')) {
            $rows->orderBy($this->_request->getParam('sort') . ' ' . $this->_request->getParam('dir'));
        }
        $rows = $rows->execute();
        $count = Doctrine_Query::create()->from('MotivTheme m');
        if (intval(str_replace('S', '', $this->_request->getParam('sid'))) != 0) {
            $count->where('shop_id = ?', array(
                intval(str_replace('S', '', $this->_request->getParam('sid')))));
        }
        $count = $count->execute();

        $temp = array();
        foreach ($rows->toArray() as $row) {
            if ($row['parent_id'] != 0) {
                $parent = Doctrine_Query::create()
                    ->from('MotivTheme m')
                    ->where('id = ?')
                    ->fetchOne(array($row['parent_id']));
                if ($parent != false) {
                    $row['parent'] = $parent->title;
                }
            } else {
                $row['parent'] = $this->translate->translate('articlegroup_all_root');
            }
            array_push($temp, $row);

        }

        $this->view->success = true;
        $this->view->totalCount = $count->count();
        $this->view->rows = $temp;
    }

    public function fetcharticlethemesAction() {
        $rows = Doctrine_Query::create()->from('ArticleTheme m')->limit(intval($this->_request->getParam('limit')))->offset(intval($this->_request->getParam('start')));
        if (intval(str_replace('S', '', $this->_request->getParam('sid'))) != 0) {
            $rows->where('shop_id = ?', array(
                intval(str_replace('S', '', $this->_request->getParam('sid')))));
        }
        if ($this->_request->getParam('sort')) {
            $rows->orderBy($this->_request->getParam('sort') . ' ' . $this->_request->getParam('dir'));
        }
        $rows = $rows->execute();
        $count = Doctrine_Query::create()->from('ArticleTheme m');
        if (intval(str_replace('S', '', $this->_request->getParam('sid'))) != 0) {
            $count->where('shop_id = ?', array(
                intval(str_replace('S', '', $this->_request->getParam('sid')))));
        }
        $count = $count->execute();

        $temp = array();
        foreach ($rows->toArray() as $row) {
            if ($row['parent_id'] != 0) {
                $parent = Doctrine_Query::create()
                    ->from('ArticleTheme m')
                    ->where('id = ?')
                    ->fetchOne(array($row['parent_id']));
                if ($parent != false) {
                    $row['parent'] = $parent->title;
                }
            } else {
                $row['parent'] = $this->translate->translate('articlegroup_all_root');
            }
            array_push($temp, $row);

        }

        $this->view->success = true;
        $this->view->totalCount = $count->count();
        $this->view->rows = $temp;
    }

    public function fetchshopthemesAction() {
        $rows = Doctrine_Query::create()->from('ShopTheme m')->limit(intval($this->_request->getParam('limit')))->offset(intval($this->_request->getParam('start')));
        if (intval(str_replace('S', '', $this->_request->getParam('sid'))) != 0) {
            $rows->where('shop_id = ?', array(
                intval(str_replace('S', '', $this->_request->getParam('sid')))));
        }
        if ($this->_request->getParam('sort')) {
            $rows->orderBy($this->_request->getParam('sort') . ' ' . $this->_request->getParam('dir'));
        }
        $rows = $rows->execute();
        $count = Doctrine_Query::create()->from('ShopTheme m');
        if (intval(str_replace('S', '', $this->_request->getParam('sid'))) != 0) {
            $count->where('shop_id = ?', array(
                intval(str_replace('S', '', $this->_request->getParam('sid')))));
        }
        $count = $count->execute();

        $temp = array();
        foreach ($rows->toArray() as $row) {
            if ($row['parent_id'] != 0) {
                $parent = Doctrine_Query::create()
                    ->from('ShopTheme m')
                    ->where('id = ?')
                    ->fetchOne(array($row['parent_id']));
                if ($parent != false) {
                    $row['parent'] = $parent->title;
                }
            } else {
                $row['parent'] = $this->translate->translate('articlegroup_all_root');
            }
            array_push($temp, $row);

        }

        $this->view->success = true;
        $this->view->totalCount = $count->count();
        $this->view->rows = $temp;
    }

    public function addarticlethemeAction() {
        $this->_helper->layout->setLayout('popupjs');
    }

    public function addshopthemeAction() {
        $this->_helper->layout->setLayout('popupjs');
    }

    public function addmotivthemeAction() {
        $this->_helper->layout->setLayout('popupjs');
    }

    public function loadsavearticlethemeAction() {
        $isNew = false;
        $id = 0;
        $translate = Zend_Registry::get('translate');
        $user = Zend_Auth::getInstance()->getIdentity();
        $user = Doctrine_Query::create()->from('Contact m')->where('id = ?')->fetchOne(array(
            $user['id']));
        $role = null;

        if (!$this->_request->getParam('sid', false)) {
            $wizard = new TP_ResaleWizard();
            $this->_request->setParam('sid', $wizard->getArticleShop());
        }

        foreach ($user->Roles as $rol) {
            if ($role == null || $rol->level > $role->level) {
                $role = $rol;
            }
        }
        $config = new Zend_Config_Ini(APPLICATION_PATH . '/modules/admin/config/themes/articlesettings.ini', $role->name);
        $fieldsarray = array();
        foreach ($config->tabs as $tab) {
            $tab = $tab->toArray();
            $fields = array();
            foreach ($tab['fields'] as $key => $field) {
                $field['fieldLabel'] = $translate->translate($field['fieldLabel']);
                array_push($fields, $field);
            }
            array_push($fieldsarray, array(
                'general' => $tab['general'],
                'fields' => $fields));
        }
        $configarray = $config->general->toArray();
        $configarray['title'] = $translate->translate($configarray['title']);
        $data = false;
        if (intval($this->_request->getParam('uid')) != 0) {
            $data = Doctrine_Query::create()->from('ArticleTheme a')->where('a.id = ?')->fetchOne(array(
                intval($this->_request->getParam('uid'))));
        }
        if (intval($this->_request->getParam('save')) == 1 && intval($this->_request->getParam('uid')) > 0) {
            $params = TP_Util::clearParams($this->_request->getParams());
            $filter = new TP_Filter_Badwords();
            foreach ($params as $key => $value) $data->$key = $filter->filter($value);
            $data->save();
        } elseif (intval($this->_request->getParam('save')) == 1 && intval($this->_request->getParam('uid')) == 0) {
            $params = TP_Util::clearParams($this->_request->getParams());
            $params['parent_id'] = intval($params['parent_id']);
            $data = new ArticleTheme();
            $data->shop_id = intval($this->_request->getParam('sid'));
            $data->Install = $user->Install;
            $filter = new TP_Filter_Badwords();
            foreach ($params as $key => $value) $data->$key = $filter->filter($value);
            $data->save();
            $isNew = true;
            $id = $data->id;
        }
        $this->view->success = true;
        $this->view->uid = $id;
        $this->view->isNew = $isNew;
        $this->view->metaData = array(
            "fields" => $fieldsarray,
            "formConfig" => $configarray,
            "data" => ($data != false) ? $data->toArray() : array());
        $this->view->data = ($data != false) ? $data->toArray() : array();
        $this->view->data['parent'] = $data->ArticleTheme->title;
        if ($data->ArticleTheme) {
            $this->view->metaData['data']['parent'] = $data->ArticleTheme->title;
        } else {
            $this->view->metaData['data']['parent'] = '';
        }
    }

    public function loadsavemotivthemeAction() {
        $isNew = false;
        $id = 0;
        $translate = Zend_Registry::get('translate');
        $user = Zend_Auth::getInstance()->getIdentity();
        $user = Doctrine_Query::create()->from('Contact m')->where('id = ?')->fetchOne(array(
            $user['id']));
        $role = null;
        foreach ($user->Roles as $rol) {
            if ($role == null || $rol->level > $role->level) {
                $role = $rol;
            }
        }
        $config = new Zend_Config_Ini(APPLICATION_PATH . '/modules/admin/config/themes/motivsettings.ini', $role->name);
        $fieldsarray = array();
        foreach ($config->tabs as $tab) {
            $tab = $tab->toArray();
            $fields = array();
            foreach ($tab['fields'] as $key => $field) {
                $field['fieldLabel'] = $translate->translate($field['fieldLabel']);
                array_push($fields, $field);
            }
            array_push($fieldsarray, array(
                'general' => $tab['general'],
                'fields' => $fields));
        }
        $configarray = $config->general->toArray();
        $configarray['title'] = $translate->translate($configarray['title']);
        $data = false;
        if (intval($this->_request->getParam('uid')) != 0) {
            $data = Doctrine_Query::create()->from('MotivTheme a')->where('a.id = ?')->fetchOne(array(
                intval($this->_request->getParam('uid'))));
        }
        if (intval($this->_request->getParam('save')) == 1 && intval($this->_request->getParam('uid')) > 0) {
            $params = TP_Util::clearParams($this->_request->getParams());
            $filter = new TP_Filter_Badwords();
            foreach ($params as $key => $value) $data->$key = $filter->filter($value);
            $data->save();
        } elseif (intval($this->_request->getParam('save')) == 1 && intval($this->_request->getParam('uid')) == 0) {
            $params = TP_Util::clearParams($this->_request->getParams());
            $data = new MotivTheme();
            $data->shop_id = intval($this->_request->getParam('sid'));
            $data->Install = $user->Install;
            $filter = new TP_Filter_Badwords();
            foreach ($params as $key => $value) $data->$key = $filter->filter($value);
            $data->save();
            $isNew = true;
            $id = $data->id;
        }
        $this->view->success = true;
        $this->view->uid = $id;
        $this->view->isNew = $isNew;
        $this->view->metaData = array(
            "fields" => $fieldsarray,
            "formConfig" => $configarray,
            "data" => ($data != false) ? $data->toArray() : array());
        $this->view->data = ($data != false) ? $data->toArray() : array();
        $this->view->data['parent'] = $data->MotivTheme->title;
        $this->view->metaData['data']['parent'] = $data->MotivTheme->title;
    }

    public function loadsaveshopthemeAction() {
        $isNew = false;
        $id = 0;
        $translate = Zend_Registry::get('translate');
        $user = Zend_Auth::getInstance()->getIdentity();
        $user = Doctrine_Query::create()->from('Contact m')->where('id = ?')->fetchOne(array(
            $user['id']));
        $role = null;
        foreach ($user->Roles as $rol) {
            if ($role == null || $rol->level > $role->level) {
                $role = $rol;
            }
        }
        $config = new Zend_Config_Ini(APPLICATION_PATH . '/modules/admin/config/themes/shopsettings.ini', $role->name);
        $fieldsarray = array();
        foreach ($config->tabs as $tab) {
            $tab = $tab->toArray();
            $fields = array();
            foreach ($tab['fields'] as $key => $field) {
                $field['fieldLabel'] = $translate->translate($field['fieldLabel']);
                array_push($fields, $field);
            }
            array_push($fieldsarray, array(
                'general' => $tab['general'],
                'fields' => $fields));
        }
        $configarray = $config->general->toArray();
        $configarray['title'] = $translate->translate($configarray['title']);
        $data = false;
        if (intval($this->_request->getParam('uid')) != 0) {
            $data = Doctrine_Query::create()->from('ShopTheme a')->where('a.id = ?')->fetchOne(array(
                intval($this->_request->getParam('uid'))));
        }
        if (intval($this->_request->getParam('save')) == 1 && intval($this->_request->getParam('uid')) > 0) {
            $params = TP_Util::clearParams($this->_request->getParams());
            $filter = new TP_Filter_Badwords();
            foreach ($params as $key => $value) $data->$key = $filter->filter($value);
            $data->save();
        } elseif (intval($this->_request->getParam('save')) == 1 && intval($this->_request->getParam('uid')) == 0) {
            $params = TP_Util::clearParams($this->_request->getParams());
            $data = new ShopTheme();
            $data->shop_id = intval($this->_request->getParam('sid'));
            $data->Install = $user->Install;
            $filter = new TP_Filter_Badwords();
            foreach ($params as $key => $value) $data->$key = $filter->filter($value);
            $data->save();
            $isNew = true;
            $id = $data->id;
        }
        $this->view->success = true;
        $this->view->uid = $id;
        $this->view->isNew = $isNew;
        $this->view->metaData = array(
            "fields" => $fieldsarray,
            "formConfig" => $configarray,
            "data" => ($data != false) ? $data->toArray() : array());
        $this->view->data = ($data != false) ? $data->toArray() : array();
        $this->view->data['parent'] = $data->ShopTheme->title;
        $this->view->metaData['data']['parent'] = $data->ShopTheme->title;
    }
}