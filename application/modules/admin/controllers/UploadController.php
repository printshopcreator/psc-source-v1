<?php

class Admin_UploadController extends TP_Admin_Controller_Action
{
    /**
     * IndexAction
     *
     * @todo Dokumentation der IndexAction
     *
     * @return void
     */
    public function allAction() {

        if (intval($this->_request->getParam('config')) == 1) {

            if ($this->highRole->level < 30) {

                $rows = Doctrine_Query::create()
                    ->from('Orderspos m')
                    ->limit(intval($this->_request->getParam('limit')))
                    ->offset(intval($this->_request->getParam('start')));

                if (intval($this->_request->getParam('uid')) != 0) {
                    $rows->where('orders_id = ?', array(intval($this->_request->getParam('uid'))));
                }
                if ($this->_request->getParam('sort')) {
                    $rows->orderBy($this->_request->getParam('sort') . ' ' . $this->_request->getParam('dir'));
                }

                $rows = $rows->execute();

                $count = Doctrine_Query::create()
                    ->from('Orderspos m');
                if (intval($this->_request->getParam('uid')) != 0) {
                    $count->where('orders_id = ?', array(intval($this->_request->getParam('uid'))));
                }

                $count = $count->execute();

                $temp = array();

                foreach ($rows as $row) {

                    $pro = unserialize($row->data);
                    $proa = $pro->getArticle();

                    $prof = Doctrine_Query::create()
                        ->from('Upload m')
                        ->where('orderpos_id = ?', array(intval($row->id)))->execute();

                    foreach ($prof as $file) {
                        array_push($temp, array(
                            'id' => $file->id,
                            'created' => $row->created,
                            'name' => $row->id . ' ' . $proa['title'],
                            'count' => $row->count,
                            'file' => $file->name,
                            'size' => filesize('uploads/' . $this->shop->uid . '/article/' . $file->path),
                            'uid' => $file->path,
                            'download' => '/uploads/' . $this->shop->uid . '/article/' . $file->path,
                            'uploadfinish' => $row->uploadfinish
                        ));
                    }
                }

                $config = array(
                    "success" => true
                , "totalCount" => $count->count()
                , "rows" => $temp

                );

                $this->getHelper('Json')->sendJson($config);

            } else {

                $rows = Doctrine_Query::create()
                    ->from('Orderspos m')
                    ->limit(intval($this->_request->getParam('limit')))
                    ->offset(intval($this->_request->getParam('start')));
                if (intval($this->_request->getParam('uid')) != 0) {
                    $rows->where('orders_id = ?', array(intval($this->_request->getParam('uid'))));
                }
                if ($this->_request->getParam('sort')) {
                    $rows->orderBy($this->_request->getParam('sort') . ' ' . $this->_request->getParam('dir'));
                }

                $rows = $rows->execute();

                $temp = array();

                foreach ($rows as $row) {

                    $pro = unserialize($row->data);
                    $proa = $pro->getArticle();

                    $prof = Doctrine_Query::create()
                        ->from('Upload m')
                        ->where('orderpos_id = ?', array(intval($row->id)))->execute();

                    foreach ($prof as $file) {
                        array_push($temp, array(
                            'id' => $file->id,
                            'created' => $row->createdd,
                            'name' => $row->id . ' ' . $proa['title'],
                            'count' => $row->count,
                            'file' => $file->name,
                            'size' => filesize('uploads/' . $this->shop->uid . '/article/' . $file->path),
                            'uid' => $file->path,
                            'download' => '/uploads/' . $this->shop->uid . '/article/' . $file->path,
                            'uploadfinish' => $row->uploadfinish
                        ));
                    }

                }

                $config = array(
                    "success" => true
                , "totalCount" => count($temp)
                , "rows" => $temp

                );

                $this->getHelper('Json')->sendJson($config);
            }
        }

        if (intval($this->_request->getParam('config')) == 2) {

            if (intval(strtok(str_replace('name-', '', $this->_request->getParam('posid')), ' ')) != 0) {

                $data = Doctrine_Query::create()
                    ->from('Orderspos a')
                    ->where('a.id = ?')->fetchOne(array(intval(strtok(str_replace('name-', '', $this->_request->getParam('posid')), ' '))));

            }
            if ($data != false) {
                $data->uploadfinish = true;
                $data->save();

                TP_Queue::process('orderposuploadcomplete', 'article', $data);
                TP_Queue::process('orderposuploadcomplete', 'global', $data);

                $config = array(
                    "success" => true
                );

            } else {
                $config = array(
                    "success" => false
                );
            }

            $this->getHelper('Json')->sendJson($config);
        }
        if (intval($this->_request->getParam('config')) == 3) {

            $rows = Doctrine_Query::create()
                ->from('Orderspos m')
                ->limit(intval($this->_request->getParam('limit')))
                ->offset(intval($this->_request->getParam('start')));
            if (intval($this->_request->getParam('uid')) != 0) {
                $rows->where('orders_id = ?', array(intval($this->_request->getParam('uid'))));
            }
            if ($this->_request->getParam('sort')) {
                $rows->orderBy($this->_request->getParam('sort') . ' ' . $this->_request->getParam('dir'));
            }

            $rows = $rows->execute();

            $temp = array();

            foreach ($rows as $row) {

                $pro = unserialize($row->data);
                $proa = $pro->getArticle();
                $prof = $pro->getFiles();

                array_push($temp, array(
                    'value' => $row->id,
                    'label' => $proa['title']
                ));

            }

            $config = array(
                "items" => $temp
            );

            $this->getHelper('Json')->sendJson($config);
        }

        $this->_helper->layout->setLayout('popupjs');

    }

}