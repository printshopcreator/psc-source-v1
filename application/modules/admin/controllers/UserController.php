<?php

class Admin_UserController extends TP_Admin_Controller_Action
{

    /**
     * IndexAction
     *
     * @todo Dokumentation der IndexAction
     *
     * @return void
     */
    public function init() {
        parent::init();

        $contextSwitch = $this->_helper->contextSwitch();
        $contextSwitch
            ->addActionContext('extjssettings', self::CONTEXT_JSON)
            ->setAutoJsonSerialization(true)->initContext();
    }

    public function extjssettingsAction() {

        if ($this->_getParam('readState') == 'loadState') {

        } elseif ($this->_getParam('cmd') == 'saveState') {

            $args = Zend_Json::decode($this->_getParam('data'));

            foreach ($args as $data) {

                $user = Doctrine_Query::create()->select()->from('Adminusersettings s')->where('s.user_id = ? AND s.name = ?', array($this->_getParam('user'), $data['name']))->fetchOne();

                if ($user) {
                    $user->data = $data['value'];
                    $user->save();
                } else {
                    $user = new Adminusersettings();
                    $user->user_id = $this->_getParam('user');
                    $user->name = $data['name'];
                    $user->data = $data['value'];
                    $user->save();
                }
            }

        }
        $this->view->success = true;

    }

    public function indexAction() {

        if (intval($this->_request->getParam('logout')) == 1) {
            Zend_Auth::getInstance()->clearIdentity();
            die(Zend_Json::encode(array('success' => true)));
        }
        $this->_helper->layout->setLayout('login');
    }

    public function settingsAction() {

        if (intval($this->_request->getParam('save')) == 1) {

            $user = Zend_Auth::getInstance()->getIdentity();
            $user = Doctrine_Query::create()
                ->from('Contact m')
                ->where('id = ?')->fetchOne(array($user['id']));

            $params = $this->_request->getParams();
            unset($params['config']);
            unset($params['save']);
            unset($params['module']);
            unset($params['controller']);
            unset($params['action']);
            unset($params['sid']);
            unset($params['headingsize']);
            foreach ($params as $key => $value)
                $user->$key = $value;

            $user->save();

            Zend_Auth::getInstance()->getStorage()->write($user->toArray(true));
        }

        if (intval($this->_request->getParam('config')) == 1) {

            $user = Zend_Auth::getInstance()->getIdentity();
            $user = Doctrine_Query::create()
                ->from('Contact m')
                ->where('id = ?')->fetchOne(array($user['id']));
            $role = null;
            foreach ($user->Roles as $rol) {
                if ($role == null || $rol->level > $role->level) {
                    $role = $rol;
                }
            }
            $config = new Zend_Config_Ini(APPLICATION_PATH . '/modules/admin/config/' . 'user/settings.ini', $role->name);

            $fieldsarray = array();
            foreach ($config->tabs as $tab) {
                $tab = $tab->toArray();
                $fields = array();
                foreach ($tab['fields'] as $key => $field) {
                    $field['fieldLabel'] = $this->translate->translate($field['fieldLabel']);
                    array_push($fields, $field);
                }

                array_push($fieldsarray, array('general' => $tab['general'], 'fields' => $fields));
            }

            $configarray = $config->general->toArray();
            $configarray['title'] = $this->translate->translate($configarray['title']);

            $config = array(
                "success" => true
            , "metaData" => array(
                    "fields" => $fieldsarray
                , "formConfig" => $configarray
                , "data" => Zend_Auth::getInstance()->getIdentity()
                )
            , "data" => Zend_Auth::getInstance()->getIdentity()
            );

            $this->getHelper('Json')->sendJson($config);
        }

        $this->_helper->layout->setLayout('simple');
    }

}
