<?php

class Admin_Model_Export_Csv_CreditSystemDetail extends TP_Export_Csv
{
    /**
     * export timesheets to csv file
     *
     * @param Addressbook_Model_ContactFilter $_filter
     * @return string filename
     */
    public function exportContacts($data, $_toStdout = FALSE) {

        $skipFields = array(
            'id',
            'shop_id',
            'install_id',
            'created',
            'updated',
            'creditsystem_id',
            'used'
        );

        $filename = parent::exportRecords($data->CreditSystemDetail, $_toStdout, $skipFields);
        return $filename;
    }
}
