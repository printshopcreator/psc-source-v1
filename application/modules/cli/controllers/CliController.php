<?php

class Cli_CliController extends Zend_Controller_Action
{

    public function init() {
        $this->_helper->viewRenderer->setNoRender();
    }

    public function cliAction() {
        $result = false;
        ini_set('max_execution_time', 0);
        try {
            $rows = Doctrine_Query::create()
                ->from('Queues a')
                ->where('a.action = ?', array('cli'))->execute();

            foreach ($rows as $row) {

                $object = unserialize(preg_replace('!s:(\d+):"(.*?)";!se', "'s:'.strlen('$2').':\"$2\";'", $row->data));
                if(!is_object($row)) {
                    $object->process($object, $row);
                }else{
                    $object->process($object, clone($row));
                }

            }
        } catch (Exception $e) {
            var_dump($e->getMessage());
        }
    }

}

