<?php

class Cli_IpController extends Zend_Controller_Action
{

    public function init() {
        $this->_helper->viewRenderer->setNoRender();
    }

    public function cliAction() {
        $result = false;
        ini_set('max_execution_time', 0);
        try {
            $rows = Doctrine_Query::create()
                ->from('Contact a')->execute();

            foreach ($rows as $user) {

                $budget = array('budget_all' => 0, 'budget_mon' => 0, 'mon' => date("m"), 'contact_budget' => $user->budget);
                if(trim($user->self_information) != "") {
                    $budget = json_decode($user->self_information, true);
                }
                if($budget['contact_budget'] == null) {
                    $budget['contact_budget'] = $user->budget;
                }
                if(date("m") != $budget['mon']) {

                    $budget['mon'] = date("m");
                    $budget['contact_budget'] = $budget['contact_budget'] + $user->budget - $budget['budget_mon'];
                    $budget['budget_mon'] = 0;
                }

                if(date("m") == 1 && (!isset($budget['last_null']) || ($budget['last_null'] != date("y")))) {
                    $budget['mon'] = date("m");
                    $budget['contact_budget'] = $user->budget;
                    $budget['budget_mon'] = 0;
                    $budget['last_null'] = date("y");
                }


                $user->self_information = json_encode($budget);
                $user->save();

            }
        } catch (Exception $e) {
            var_dump($e->getMessage());
        }
    }

}

