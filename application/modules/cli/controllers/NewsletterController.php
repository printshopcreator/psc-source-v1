<?php

class Cli_NewsletterController extends Zend_Controller_Action
{

    public function init() {
        $this->_helper->viewRenderer->setNoRender();
    }

    public function sendAction() {
        $result = false;
        ini_set('max_execution_time', 0);

        try {
            $rows = Doctrine_Query::create()
                ->from('Newsletterqueue a')
                ->where('a.status = 1')->execute();

            foreach ($rows as $row) {

                $contacts = Doctrine_Query::create()
                    ->from('Contact a')
                    ->leftJoin('a.ShopContact as s')
                    ->where('a.newsletter = 1 AND s.shop_id = ?', array($row->shop_id))->execute();

                $i = 0;



                Zend_Registry::set('shop', $row->Shop->toArray());
                Zend_Registry::set('shop_path', APPLICATION_PATH . '/design/clients/' . $row->Shop->uid);

                foreach ($contacts as $contact) {

                    $loader = new Twig_Loader_String();
                    $twig = new Twig_Environment($loader);
                    $template = $twig->loadTemplate(str_replace('\\', '', $row->Newsletter->text));
                    $templatehtml = $twig->loadTemplate(str_replace('\\', '', $row->Newsletter->text_html));
                    $templateSubj = $twig->loadTemplate(str_replace('\\', '', $row->Newsletter->subject));

                    $templateVars = array(
                        'shop' => $row->Shop,
                        'contact' => $contact);

                    $content = $template->render($templateVars);
                    $contenthtml = $templatehtml->render($templateVars);
                    $subject = $templateSubj->render($templateVars);

                    TP_Mail::sendTextMail('queue', $content, $contenthtml, $subject, $contact->self_email, $row->Newsletter->from_name, $row->Newsletter->from_mail, array());
                    $i++;
                }

                $contacts = Doctrine_Query::create()
                    ->from('Contactnewsletter a')
                    ->where('a.shop_id = ?', array($row->shop_id))->execute();

                foreach ($contacts as $contact) {

                    $loader = new Twig_Loader_String();
                    $twig = new Twig_Environment($loader);
                    $template = $twig->loadTemplate(str_replace('\\', '', $row->Newsletter->text));
                    $templatehtml = $twig->loadTemplate(str_replace('\\', '', $row->Newsletter->text_html));
                    $templateSubj = $twig->loadTemplate(str_replace('\\', '', $row->Newsletter->subject));

                    $templateVars = array(
                        'shop' => $row->Shop,
                        'contact' => new Contact());

                    $content = $template->render($templateVars);
                    $contenthtml = $templatehtml->render($templateVars);
                    $subject = $templateSubj->render($templateVars);

                    TP_Mail::sendTextMail('queue', $content, $contenthtml, $subject, $contact->email, $row->Newsletter->from_name, $row->Newsletter->from_mail, array());
                    $i++;
                }


                $row->send_count = $i;
                $row->status = 2;
                $row->save();

            }
        } catch (Exception $e) {
            var_dump($e->getMessage());
        }
    }

}

