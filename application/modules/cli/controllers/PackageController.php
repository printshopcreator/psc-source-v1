<?php

class Cli_PackageController extends Zend_Controller_Action
{

    public function init()
    {
        $this->_helper->viewRenderer->setNoRender();
    }

    public function generateAction()
    {

        if(($pid = TP_Cron::lock("fop")) !== FALSE) {
            $writer = new Zend_Log_Writer_Null();
            $logger = new Zend_Log($writer);
            Zend_Registry::set('log', $logger);


            try {
                $rows = Doctrine_Query::create()
                    ->from('Orderspos a')
                    ->where('a.render_print = 1 AND a.layouter_mode != 8')
                    ->orderBy('a.id DESC')->limit(10)->execute();

                foreach ($rows as $row) {
                    if($row->data == "") {
                        continue;
                    }
                    $layouterID = unserialize($row->data)->getLayouterId();
                    if($layouterID == "") {
                        continue;
                    }

                    try {

                        $path = implode('/', str_split($row->id));
                        if(!file_exists(APPLICATION_PATH . '/../market/steplayouter/basket/' . $row->orders_id . '/' . $row->pos)) {
                            mkdir(APPLICATION_PATH . '/../market/steplayouter/basket/' . $row->orders_id . '/' . $row->pos, 0777, true);
                        }

                        $outfile = APPLICATION_PATH . '/../market/steplayouter/basket/' . $row->orders_id . '/' . $row->pos . '/'.$row->Orders->alias . '_' . $row->pos.'.pdf';
                        exec('/usr/local/bin/php /data/www/new/bin/console --env=prod component:steplayouter:pdf:print "'.$outfile.'" '.$layouterID.' '.$row->Orders->alias);

                        var_dump(APPLICATION_PATH . '/../market/steplayouter/basket/' . $row->orders_id . '/' . $row->pos . '/production.pdf');
                        $row->render_print = false;
                        $row->save();

                        $order = $row->Orders;
                        $order->setPackageExported(false);
                        $order->saveMongo();

                    } catch (Exception $e) {
                        var_dump($e->getTraceAsString());
                    }

                }
            } catch (Exception $e) {
                var_dump($e->getTraceAsString());
                var_dump($e->getMessage());
            }

            TP_Cron::unlock("fop");
        }


        if (($pid = TP_Cron::lock("pack")) !== FALSE) {
            ini_set('max_execution_time', 500);
            $locale = new Zend_Locale('de_DE');

            Zend_Registry::set('locale', $locale);
            Zend_Registry::set('shop_path', '/opt/lampp/htdocs/trunk/application/design/clients/1/');
            Zend_Registry::set('layout_path', '/opt/lampp/htdocs/trunk/application/design/clients/1/');
            $writer = new Zend_Log_Writer_Null();
            $logger = new Zend_Log($writer);
            Zend_Registry::set('log', $logger);

            try {
                $rows = Doctrine_Query::create()
                    ->from('Orders a')->orderBy('a.id DESC')->limit(30)->execute();

                /** @var Orders $row */
                foreach ($rows as $row) {

                    if(!$row->isPackageExported()) {
                        Zend_Registry::set('install', $row->Install);
                        Zend_Registry::set('shop', $row->Shop);
                        if (file_exists('/data/www/old/data/packages/' . $row->alias . '.zip')) {
                            if ((filesize('/data/www/old/data/packages/' . $row->alias . '.zip') / 1024 / 1024) > 5) {
                                var_dump("Größer als 5mb: " . $row->alias . '.zip' . "/" . (filesize('/data/www/old/data/packages/' . $row->alias . '.zip') / 1024 / 1024));
                                continue;
                            }
                        }
                        $package = new TP_Package();
                        $package->loadOrder($row);

                        $package->createZip();
                        rename($package->getFileName(), '/data/www/old/data/packages/' . $row->alias . '.zip');

                        $row->setPackageExported(true);
                        $row->saveMongo();

                        $dbMongo = TP_Mongo::getInstance();
                        $dbMongo->Job->insertOne(array(
                            "shop" =>  $row->Shop->id,
                            "event" => "package_created",
                            "data" => [ "order" => $row->uuid, "path" => '/data/www/old/data/packages/' . $row->alias . '.zip'],
                            "created" => new MongoDB\BSON\UTCDateTime(),
                            "updated" => new MongoDB\BSON\UTCDateTime()
                        ));
                    }
                }
            } catch (Exception $e) {
                var_dump($e->getMessage());
            }

            TP_Cron::unlock("pack");
        }
    }

}

