<?php

/**
 * User Controller
 *
 * Stellt das Usersystem fürs Frontend da
 *
 * PHP versions 4 and 5
 *
 * @category   Modul
 * @package    Default
 * @subpackage Controller
 * @author     Thomas petersomn <tpeterson@printshopcreator.de>
 * @copyright  1997-2009 Thomas Peterson
 * @license    Not Public
 * @version    SVN: $Id: UserController.php 7378 2012-03-10 16:45:56Z boonkerz $
 * @link       http://www.printshopcreator.de
 */

/**
 * User Controller
 *
 * Stellt das Usersystem fürs Frontend da
 *
 * PHP versions 4 and 5
 *
 * @category   Modules
 * @package    Default
 * @subpackage Controller
 * @author     Thomas petersomn <tpeterson@printshopcreator.de>
 * @copyright  1997-2009 Thomas Peterson
 * @license    Not Public
 * @version    Release: $Id: UserController.php 7378 2012-03-10 16:45:56Z boonkerz $
 * @link       http://www.printshopcreator.de
 */
class AccountController extends TP_Controller_Action
{

    public function init() {
        parent::init();

        $contextSwitch = $this->_helper->contextSwitch();
        $contextSwitch
            ->setAutoJsonSerialization(true)->initContext();

    }

    public function indexAction() {

        if (!Zend_Auth::getInstance()->hasIdentity()) {
            $this->_forward('login', 'user');
        }

        $temp = array();
        foreach($this->user->ContactAccount as $artCount) {
            $temp[$artCount->account_id] = $artCount->Account;
            $temp = $this->checkSub($temp, $artCount->account_id);
        }

        $this->view->accounts = $temp;
    }

    public function checkSub($temp, $account_id) {
        $accounts = Doctrine_Query::create()->from('Account a')->leftJoin("a.ShopAccount s")->where('s.shop_id = ? AND a.filiale_id = ?', array(
            $this->shop->id, $account_id))->execute();

        foreach($accounts as $artCount) {
            if(!isset($temp[$artCount->id])) {
                $temp[$artCount->id] = $artCount;
            }
            $this->checkSub($temp, $artCount->id);
        }
        return $temp;

    }

    public function editmailAction() {
        if (!Zend_Auth::getInstance()->hasIdentity()) {
            $this->_forward('login', 'user');
        }


        $temp = array();
        foreach($this->user->ContactAccount as $artCount) {
            $temp[$artCount->account_id] = $artCount->Account;
            $temp = $this->checkSub($temp, $artCount->account_id);
        }

        $contactAccount = $temp[$this->_getParam('id')];

        $config = new Zend_Config_Ini($this->_configPath . '/account/editmail.ini', 'add');
        if ($config->global) {
            $form = new EasyBib_Form($config->user);
        } else {
            $form = new Zend_Form($config->user);
        }


        if ($this->_request->isPost()) {

            $formData = $this->getRequest()->getParams();

            if ($form->isValid($formData)) {

                TP_Queue::process('accounteditmail', 'global', $this->getRequest()->getParams());
                $this->view->priorityMessenger('Änderungsanforderung per Mail versendet', 'success');
                $this->_redirect("/account/index");
            }else{
                $form->populate($formData);
            }
        }else{
            $form->populate($contactAccount->toArray());
        }

        $this->view->form = $form;
    }

}