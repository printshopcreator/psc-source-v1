<?php

class ArticleController extends TP_Controller_Action
{

    public function init()
    {
        parent::init();
        $this->view->showProduct = true;
        $contextSwitch = $this->_helper->contextSwitch();
        $contextSwitch->addActionContext('calc', 'json')->addActionContext('printoffer', 'json')
            ->addActionContext('addfav', 'json')
            ->addActionContext('delfav', 'json')
            ->addActionContext('getnewdesignarticles', 'json')
            ->addActionContext('getarticle', 'json')
            ->addActionContext('getmarketarticle', 'json')
            ->addActionContext('getmarketarticlegroups', 'json')
            ->addActionContext('getcollectingcontacts', 'json')
            ->addActionContext('updatecollectingcontact', 'json')
            ->addActionContext('uploadtoarticle', 'json')
            ->addActionContext('copylatestuploadfromordertosession', 'json')
            ->addActionContext('getarticlegroups', 'json')->setAutoJsonSerialization(true)->initContext();
    }

    public function copylatestuploadfromordertosessionAction()
    {
        $this->view->clearVars();
        $this->view->success = true;

        $row = Doctrine_Query::create()
            ->from('Article m')
            ->where('id = ?', array(intval($this->_request->getParam('id'))))
            ->fetchOne();


        $uploads = TP_Upload::getLastUploadForUser($row);

        $basket = new TP_Basket();

        $article = $basket->getTempProduct();

        /** @var Upload $upload */
        foreach ($uploads as $upload) {
            $article->setFiles(TP_Util::uuid(), $upload->path, $upload->name, $upload->chunktitle, $upload->typ);
        }

        $basket->setTempProductClass($article);

    }

    public function uploadtoarticleAction()
    {
        Zend_Registry::get('log')->debug('UPLOAD');
        $this->getHelper('layout')->disableLayout();
        $this->_helper->viewRenderer->setNoRender();
        $mode = $this->_getParam('mode', 'articlelist');
        $row = Doctrine_Query::create()
            ->from('Orderspos m')
            ->where('id = ?', array(intval($this->_request->getParam('id'))))
            ->fetchOne();


        $adapter = new Zend_File_Transfer_Adapter_Http();
        $adapter->addValidator('Extension', false, 'ppt,inx,idml,indd,qxd,ps,cdr,cdx,jpg,gif,pdf,xls,png,doc,jpeg,tif,tiff,ai,eps,psd,phtml,zip,jrxml,ttf,otf');

        $id = $row->Orders->alias . '_' . $row->id;

        if (!file_exists('uploads/' . $this->shop->uid . '/article')) {
            mkdir('uploads/' . $this->shop->uid . '/article', 0777, true);
        }

        $adapter->setDestination('uploads/' . $this->shop->uid . '/article');

        $orginalFilename = $adapter->getFileName(null, false);

        $adapter->addFilter('Rename', 'uploads/' . $this->shop->uid . '/article/' . $id . '_' . $adapter->getFileName(null, false));

        if (file_exists('uploads/' . $this->shop->uid . '/article/' . $id . '_' . $adapter->getFileName(null, false))) {

            $id .= time();
            $adapter->addFilter('Rename', 'uploads/' . $this->shop->uid . '/article/' . $id . '_' . $adapter->getFileName(null, false));
        } else {
            $adapter->addFilter('Rename', 'uploads/' . $this->shop->uid . '/article/' . $id . '_' . $adapter->getFileName(null, false));
        }

        if (!$adapter->receive() || !$adapter->isValid()) {
            $error = $adapter->getMessages();
            Zend_Registry::get('log')->debug(var_dump($adapter->getMessages(), true));
            if (isset($error['fileExtensionFalse'])) {
                $arguments = array('error' => true, 'message' => "Falsche Extension");
            } else {
                $arguments = array('error' => true, 'message' => 'Upload Error');
            }
            die(Zend_Json::encode($arguments));
        }

        //TP_Queue::process('upload_in_process', 'article', 'uploads/'.$this->shop->uid.'/article/'.$adapter->getFileName(null,false));

        $upload = new UploadArticle();

        $upload->name = $orginalFilename;
        $upload->path = $adapter->getFileName(null, false);
        $upload->article_id = intval($this->_request->getParam('id'));

        $upload->save();
        $this->view->clearVars();
        $mode = $this->_getParam('mode', 'articlelist');
        $this->view->content = array();
        require_once(APPLICATION_PATH . '/helpers/Image.php');
        $img = new TP_View_Helper_Image();

        $image = array('key' => $upload->uuid, 'value' => $upload->path);


        $this->view->content[] = array(
            'delete_url' => '/service/upload/deletecenter?uuid=' . $upload->uuid,
            'key' => $upload->uuid,
            'md5' => md5($upload->uuid),
            'mode' => 'product',
            'name' => $upload->name,
            'error' => false
        );
    }

    public function getfileAction()
    {

        $basket = new TP_Basket();

        $noImg = $this->_getParam('no_image', 0);

        $row = $basket->getTempProduct()->getFile($this->_getParam('uuid'));
        $row['key'] = $this->_getParam('uuid');

        $this->view->clearVars();
        $this->view->key = $row['key'];


        $this->view->name = $row['name'];
        $this->view->shop = $this->shop->uid;
        $this->view->value = $row['value'];
        $this->view->size = $this->FileSize('uploads/' . $this->shop->uid . '/article/' . $row['value']);

        $this->view->success = true;
    }

    /**
     * Gibt aktuelle Dateien zur Auftragsposition zurück
     *
     * @return void
     */
    public function uploadtoarticledeletefileAction()
    {
        $this->getHelper('layout')->disableLayout();
        $this->_helper->viewRenderer->setNoRender();

        $rows = Doctrine_Query::create()
            ->from('UploadArticle m')
            ->where('uuid = ?', array($this->_request->getParam('uuid')))
            ->delete()->execute();


        echo Zend_Json::encode(array('success' => true));
    }

    /**
     * Gibt aktuelle Dateien zur Auftragsposition zurück
     *
     * @return void
     */
    public function uploadtoarticlegetfilesAction()
    {
        $this->getHelper('layout')->disableLayout();
        $this->_helper->viewRenderer->setNoRender();

        $rows = Doctrine_Query::create()
            ->from('UploadArticle m')
            ->where('article_id = ?', array(intval($this->_request->getParam('id'))))->orderBy('m.id ASC')
            ->execute();

        $content = array();
        $i = 1;
        foreach ($rows as $row) {
            $content[] = array(
                'key' => $row->uuid,
                'name' => $row->name . ' Version ' . $i,
                'shop' => $this->shop->uid,
                'value' => $row->path
            );
            $i++;
        }

        echo Zend_Json::encode($content);
    }

    public function updatecollectingcontactAction()
    {

        $this->view->clearVars();
        $this->view->success = false;

        if ($this->highRole->level >= 40 || $this->user->is_sek) {
            $contact = Doctrine_Query::create()->from('Contact a')->leftJoin('a.ShopContact s')->where('s.shop_id = ? AND a.collecting_orders = 1 AND a.uuid = ?', array($this->shop->id, $this->_getParam("uuid")))->fetchOne();

            $contact->collecting_orders = false;
            $contact->enable = false;
            $contact->save();
        }

        $this->view->success = true;
    }

    public function getcollectingcontactsAction()
    {

        $this->view->clearVars();
        $this->view->success = false;

        if ($this->highRole->level >= 40 || $this->user->is_sek) {

            if ($this->_getParam('selectedContacts', false)) {
                $contacts = Doctrine_Query::create()->from('Contact a')->leftJoin('a.ShopContact s')->where('s.shop_id = ? AND a.collecting_orders = 1 AND a.enable = 1 AND a.locked = 0 AND a.locked_master = 0 AND a.uuid in ("0", "' . implode('","', $this->_getParam('selectedContacts', false)) . '")', array($this->shop->id))->execute();
                $contactsCount = count($contacts);
            } else {
                if ($this->_getParam('account_id', false)) {


                    if ($this->_getParam('search', false)) {
                        $term = "%" . $this->_getParam('search', false) . "%";

                        $contacts = Doctrine_Query::create()->from('Contact a')->leftJoin('a.Account c')->leftJoin('a.ShopContact s')->where('s.shop_id = ? AND a.collecting_orders = 1 AND a.enable = 1 AND a.locked = 0 AND a.locked_master = 0 AND a.account_id = ? AND (self_department LIKE ? OR self_city LIKE ? OR self_zip LIKE ? OR self_firstname LIKE ? OR self_lastname LIKE ? OR self_street LIKE ? OR self_department2 LIKE ? OR self_email LIKE ? OR uuid LIKE ? OR c.company = ? OR c.street = ? OR c.house_number = ? OR c.zip = ? OR c.city = ? OR c.email = ?)', array($this->shop->id, $this->_getParam('account_id'), $term, $term, $term, $term, $term, $term, $term, $term, $term, $term, $term, $term, $term, $term, $term))->limit(300)->execute();
                        $contactsCount = Doctrine_Query::create()->from('Contact a')->leftJoin('a.Account c')->leftJoin('a.ShopContact s')->where('s.shop_id = ? AND a.collecting_orders = 1 AND a.enable = 1 AND a.locked = 0 AND a.locked_master = 0 AND a.account_id = ? AND (self_department LIKE ? OR self_city LIKE ? OR self_zip LIKE ? OR self_firstname LIKE ? OR self_lastname LIKE ? OR self_street LIKE ? OR self_department2 LIKE ? OR self_email LIKE ? OR uuid LIKE ? OR c.company = ? OR c.street = ? OR c.house_number = ? OR c.zip = ? OR c.city = ? OR c.email = ?)', array($this->shop->id, $this->_getParam('account_id'), $term, $term, $term, $term, $term, $term, $term, $term, $term, $term, $term, $term, $term, $term, $term))->count();

                        if ($this->_getParam('account_sub', false)) {
                        }
                    } else {
                        $contacts = Doctrine_Query::create()->from('Contact a')->leftJoin('a.ShopContact s')->where('s.shop_id = ? AND a.collecting_orders = 1 AND a.enable = 1 AND a.locked = 0 AND a.locked_master = 0 AND a.account_id = ?', array($this->shop->id, $this->_getParam('account_id')))->limit(300)->execute();
                        $contactsCount = Doctrine_Query::create()->from('Contact a')->leftJoin('a.ShopContact s')->where('s.shop_id = ? AND a.collecting_orders = 1 AND a.enable = 1 AND a.locked = 0 AND a.locked_master = 0 AND a.account_id = ?', array($this->shop->id, $this->_getParam('account_id')))->count();

                        if ($this->_getParam('account_sub', false)) {
                        }
                    }
                } else {
                    if ($this->_getParam('search', false)) {
                        $term = "%" . $this->_getParam('search', false) . "%";

                        $contacts = Doctrine_Query::create()->from('Contact a')->leftJoin('a.Account c')->leftJoin('a.ShopContact s')->where('s.shop_id = ? AND a.collecting_orders = 1 AND a.enable = 1 AND a.locked = 0 AND a.locked_master = 0 AND (self_department LIKE ? OR self_city LIKE ? OR self_zip LIKE ? OR self_firstname LIKE ? OR self_lastname LIKE ? OR self_street LIKE ? OR self_department2 LIKE ? OR self_email LIKE ? OR uuid LIKE ? OR c.company = ? OR c.street = ? OR c.house_number = ? OR c.zip = ? OR c.city = ? OR c.email = ?)', array($this->shop->id, $term, $term, $term, $term, $term, $term, $term, $term, $term, $term, $term, $term, $term, $term, $term))->limit(300)->execute();
                        $contactsCount = Doctrine_Query::create()->from('Contact a')->leftJoin('a.Account c')->leftJoin('a.ShopContact s')->where('s.shop_id = ? AND a.collecting_orders = 1 AND a.enable = 1 AND a.locked = 0 AND a.locked_master = 0 AND (self_department LIKE ? OR self_city LIKE ? OR self_zip LIKE ? OR self_firstname LIKE ? OR self_lastname LIKE ? OR self_street LIKE ? OR self_department2 LIKE ? OR self_email LIKE ? OR uuid LIKE ? OR c.company = ? OR c.street = ? OR c.house_number = ? OR c.zip = ? OR c.city = ? OR c.email = ?)', array($this->shop->id, $term, $term, $term, $term, $term, $term, $term, $term, $term, $term, $term, $term, $term, $term, $term))->count();
                    } else {
                        if ($this->_getParam('selectMode', false)) {
                            $contacts = [];
                            $contactsCount = 0;
                        } else {
                            $contacts = Doctrine_Query::create()->from('Contact a')->leftJoin('a.ShopContact s')->where('s.shop_id = ? AND a.collecting_orders = 1 AND a.enable = 1 AND a.locked = 0 AND a.locked_master = 0', array($this->shop->id))->limit(300)->execute();
                            $contactsCount = Doctrine_Query::create()->from('Contact a')->leftJoin('a.ShopContact s')->where('s.shop_id = ? AND a.collecting_orders = 1 AND a.enable = 1 AND a.locked = 0 AND a.locked_master = 0', array($this->shop->id))->count();
                        }
                    }
                }
            }

            $tmp = array();

            foreach ($contacts as $contact) {

                $t = array(
                    'uuid' => $contact->uuid,
                    'id' => $contact->id,
                    'self_email' => $contact->self_email,
                    'self_department' => $contact->self_department,
                    'self_firstname' => $contact->self_firstname,
                    'self_lastname' => $contact->self_lastname,
                    'self_street' => $contact->self_street,
                    'self_house_number' => $contact->self_house_number,
                    'self_zip' => $contact->self_zip,
                    'self_city' => $contact->self_city,
                    'accountCompany' => $contact->Account->company,
                    'accountStreet' => $contact->Account->street,
                    'accountHouseNumber' => $contact->Account->house_number,
                    'accountZip' => $contact->Account->zip,
                    'accountCity' => $contact->Account->city,
                    'accountAppendix' => $contact->Account->appendix,
                    'accountHotelName' => $contact->Account->hotel_name,
                    'accountSubHotelName' => $contact->Account->sub_hotel_name
                );

                $tmp[] = $t;
            }

            $this->view->result = $tmp;
            $this->view->count = $contactsCount;
            $this->view->success = true;
        }
    }

    public function thumbnailAction()
    {
        $article = Doctrine_Query::create()->from('Article c')->where('c.enable = 1 AND c.uuid = ?', array($this->_getParam('uuid')))->fetchOne();

        if ($article->file != "") {
            $image = Doctrine_Query::create()
                ->from('Image as i')
                ->where('i.id = ?')->fetchOne(array($article->file));

            if ($image != false && file_exists($image->path)) {
                $imageObj = TP_Image::getInstance();
                $temp = $imageObj->createThumb('embed', $image);
            }
        }

        if ((($article->typ == 6 && $article->a6_org_article != 0) || $article->typ == 8) && $article->file == "") {

            if ($article->id != false && !file_exists($article->getMarketFile())) {
                TP_FOP::generatePreview($article->id);
            }

            if (file_exists($article->getMarketFile())) {
                $image = new Image();
                $image->path = $article->getMarketFile();
                $image->id = md5($article->getMarketFile());

                $imageObj = TP_Image::getInstance();
                $temp = $imageObj->createThumb('embed', $image, true);
            }
        }

        $this->_redirect($this->view->basepath . '/' . $temp);
    }

    public function getmarketarticlegroupsAction()
    {
        $shop = Zend_Registry::get('shop');
        $install = Zend_Registry::get('install');

        if ($this->shop->market) {
            if ($this->_getParam('id', false)) {
                $articlegroups = Doctrine_Query::create()->from('ArticleGroup c')->where('c.shop_id = ? AND c.notinmenu = 0 AND c.enable = 1 AND c.parent=? AND (language = ? OR language = \'all\')', array(
                    $install['defaultmarket'],
                    intval($this->_getParam('id', false)),
                    Zend_Registry::get('locale')
                ))->orderBy('c.pos ASC')->execute();
            } else {
                $articlegroups = Doctrine_Query::create()->from('ArticleGroup c')->where('c.shop_id = ? AND c.notinmenu = 0 AND c.enable = 1 AND c.parent=0 AND (language = ? OR language = \'all\')', array(
                    $install['defaultmarket'],
                    Zend_Registry::get('locale')
                ))->orderBy('c.pos ASC')->execute();
            }
        } else {
            if ($this->_getParam('id', false)) {
                $articlegroups = Doctrine_Query::create()->from('ArticleGroup c')->where('c.shop_id = ? AND c.notinmenu = 0 AND c.enable = 1 AND c.parent=? AND (language = ? OR language = \'all\')', array(
                    $this->shop->id,
                    intval($this->_getParam('id', false)),
                    Zend_Registry::get('locale')
                ))->orderBy('c.pos ASC')->execute();
            } else {
                $articlegroups = Doctrine_Query::create()->from('ArticleGroup c')->where('c.shop_id = ? AND c.notinmenu = 0 AND c.enable = 1 AND c.parent=0 AND (language = ? OR language = \'all\')', array(
                    $this->shop->id,
                    Zend_Registry::get('locale')
                ))->orderBy('c.pos ASC')->execute();
            }
        }
        $this->view->clearVars();
        $this->view->success = true;
        $data = array();
        $ids = array();

        $mode = $this->_getParam('mode', 'productbarimage');
        require_once(APPLICATION_PATH . '/helpers/Image.php');
        $img = new TP_View_Helper_Image();
        foreach ($articlegroups as $articlegroup) {
            $subgroups = false;
            $as = Doctrine_Query::create()->from('ArticleGroup c')->where('c.parent=?', array($articlegroup->id))->orderBy('c.pos ASC')->execute();
            if (count($as) > 0) {
                $subgroups = true;
            }

            $count = Doctrine_Query::create()->from('Article a')
                ->leftJoin('a.ArticleGroupArticle c')
                ->where('a.shop_id = ? AND a.private = 0 AND a.enable = 1 AND a.not_edit = 0 AND a.a6_org_article = 0 AND a.upload_weblayouter = 1 AND c.articlegroup_id = ?', array($this->shop->id, $articlegroup->id))->orderBy('a.pos ASC')->execute()->count();

            if ($count == 0) {
                continue;
            }
            $data[] = array(
                'id' => $articlegroup->id,
                'url' => $articlegroup->url,
                'subgroups' => $subgroups,
                'title' => $articlegroup->title,
                'image_html' => $img->thumbnailImage($articlegroup->title, 'productbarimage', $articlegroup->image)
            );
        }

        $this->view->result = $data;
    }

    public function getnewdesignarticlesAction()
    {
        $this->view->clearVars();
        $this->view->success = true;
        if ($this->shop->market == false || ($this->shop->market && $this->shop->template_display_products_custom_layouter == true)) {
            $this->view->products = 2;
            return;
        }

        $articles = Doctrine_Query::create()->from('Article a')
            ->leftJoin('a.ArticleGroupArticle c')
            ->where('a.shop_id = ? AND a.private = 0 AND a.enable = 1 AND a.not_edit = 0 AND a.upload_weblayouter = 1', array($this->shop->id))->orderBy('a.pos ASC')->execute();

        $this->view->products = count($articles);
        if (count($articles) == 1) {
            $this->view->product = $articles->getFirst()->uuid;
        }
    }

    public function getarticlegroupsAction()
    {
        $shop = Zend_Registry::get('shop');
        $install = Zend_Registry::get('install');

        if ($this->_getParam('id', false)) {
            $articlegroups = Doctrine_Query::create()->from('ArticleGroup c')->where('c.shop_id = ? AND c.notinmenu = 0 AND c.enable = 1 AND c.parent=? AND (language = ? OR language = \'all\')', array(
                $shop['id'],
                intval($this->_getParam('id', false)),
                Zend_Registry::get('locale')
            ))->orderBy('c.pos ASC')->execute();
        } else {
            $articlegroups = Doctrine_Query::create()->from('ArticleGroup c')->where('c.shop_id = ? AND c.notinmenu = 0 AND c.enable = 1 AND c.parent=0 AND (language = ? OR language = \'all\')', array(
                $shop['id'],
                Zend_Registry::get('locale')
            ))->orderBy('c.pos ASC')->execute();
        }
        $this->view->clearVars();
        $this->view->success = true;
        $data = array();

        $ids = array();

        $mode = $this->_getParam('mode', 'productbarimage');
        require_once(APPLICATION_PATH . '/helpers/Image.php');
        $img = new TP_View_Helper_Image();
        foreach ($articlegroups as $articlegroup) {
            $subgroups = false;
            $as = Doctrine_Query::create()->from('ArticleGroup c')->where('c.parent=?', array($articlegroup->id))->orderBy('c.pos ASC')->execute();
            if (count($as) > 0) {
                $subgroups = true;
            }
            $data[] = array(
                'id' => $articlegroup->id,
                'url' => $articlegroup->url,
                'subgroups' => $subgroups,
                'title' => $articlegroup->title,
                'image_html' => $img->thumbnailImage($articlegroup->title, 'productbarimage', $articlegroup->image)
            );
        }

        $this->view->result = $data;
    }

    public function getarticleAction()
    {
        $shop = Zend_Registry::get('shop');
        $install = Zend_Registry::get('install');
        if ($this->shop->market) {
            if ($this->_getParam('id', false)) {
                $articles = Doctrine_Query::create()->from('Article a')
                    ->leftJoin('a.ArticleGroupArticle c')
                    ->where('a.shop_id = ? AND a.private = 0 AND a.enable = 1 AND a.not_edit = 0 AND a.upload_weblayouter = 1 AND c.articlegroup_id = ?', array($this->shop->id, $this->_getParam('id', false)))->orderBy('a.pos ASC')->execute();
            } else {
                $articles = Doctrine_Query::create()->from('Article a')
                    ->leftJoin('a.ArticleGroupArticle c')
                    ->where('a.shop_id = ? AND a.private = 0 AND a.enable = 1 AND a.not_edit = 0 AND a.upload_weblayouter = 1', array($this->shop->id))->orderBy('a.pos ASC')->execute();
            }
        } else {
            if ($this->_getParam('id', false)) {
                $articles = Doctrine_Query::create()->from('Article a')
                    ->leftJoin('a.ArticleGroupArticle c')
                    ->where('a.shop_id = ? AND a.private = 0 AND a.enable = 1 AND a.not_edit = 0 AND a.a6_org_article = 0 AND a.upload_weblayouter = 1 AND c.articlegroup_id = ?', array($this->shop->id, $this->_getParam('id', false)))->orderBy('a.pos ASC')->execute();
            } else {
                $articles = Doctrine_Query::create()->from('Article a')
                    ->leftJoin('a.ArticleGroupArticle c')
                    ->where('a.shop_id = ? AND a.private = 0 AND a.enable = 1 AND a.not_edit = 0 AND a.a6_org_article = 0 AND a.upload_weblayouter = 1', array($this->shop->id))->orderBy('a.pos ASC')->execute();
            }
        }
        $this->view->clearVars();
        $this->view->success = true;
        $data = array();
        $ids = array();

        $mode = $this->_getParam('mode', 'productbarimage');
        require_once(APPLICATION_PATH . '/helpers/Image.php');
        $im = new TP_View_Helper_Image();
        foreach ($articles as $at) {

            $img = "";
            if ($at->file1 != "") {
                $img = $im->thumbnailImage($at->title, 'articlelist', $at->file1);
            } elseif ($at->file != "") {
                $img = $im->thumbnailImage($at->title, 'articlelist', $at->file);
            } elseif ($at->a6_org_article != 0) {
                $prev = new market_article();
                $img = $prev->generatePreview($at->id, false);
            }

            $data[] = array(
                'id' => $at->id,
                'uuid' => $at->uuid,
                'url' => $at->url,
                'title' => $at->title,
                'image_html' => $img
            );
        }

        $this->view->result = $data;
    }

    public function getmarketarticleAction()
    {
        $shop = Zend_Registry::get('shop');
        $install = Zend_Registry::get('install');

        if ($this->shop->market) {
            if ($this->_getParam('id', false)) {
                $articles = Doctrine_Query::create()->from('Article a')
                    ->leftJoin('a.ArticleGroupArticle c')
                    ->where('a.shop_id = ? AND a.private = 0 AND a.enable = 1 AND a.not_edit = 0 AND a.a6_org_article = 0 AND a.upload_weblayouter = 1 AND c.articlegroup_id = ?', array($install['defaultmarket'], $this->_getParam('id', false)))->orderBy('a.pos ASC')->execute();
            } else {
                $articles = Doctrine_Query::create()->from('Article a')
                    ->leftJoin('a.ArticleGroupArticle c')
                    ->where('a.shop_id = ? AND a.private = 0 AND a.enable = 1 AND a.not_edit = 0 AND a.a6_org_article = 0 AND a.upload_weblayouter = 1', array($install['defaultmarket']))->orderBy('a.pos ASC')->execute();
            }
        } else {
            if ($this->_getParam('id', false)) {
                $articles = Doctrine_Query::create()->from('Article a')
                    ->leftJoin('a.ArticleGroupArticle c')
                    ->where('a.shop_id = ? AND a.private = 0 AND a.enable = 1 AND a.not_edit = 0 AND a.a6_org_article = 0 AND a.upload_weblayouter = 1 AND c.articlegroup_id = ?', array($this->shop->id, $this->_getParam('id', false)))->orderBy('a.pos ASC')->execute();
            } else {
                $articles = Doctrine_Query::create()->from('Article a')
                    ->leftJoin('a.ArticleGroupArticle c')
                    ->where('a.shop_id = ? AND a.private = 0 AND a.enable = 1 AND a.not_edit = 0 AND a.a6_org_article = 0 AND a.upload_weblayouter = 1', array($this->shop->id))->orderBy('a.pos ASC')->execute();
            }
        }
        $this->view->clearVars();
        $this->view->success = true;
        $data = array();
        $ids = array();

        $mode = $this->_getParam('mode', 'productbarimage');
        require_once(APPLICATION_PATH . '/helpers/Image.php');
        $im = new TP_View_Helper_Image();
        foreach ($articles as $at) {

            $img = "";
            if ($at->file1 != "") {
                $img = $im->thumbnailImage($at->title, 'articlelist', $at->file1);
            } elseif ($at->file != "") {
                $img = $im->thumbnailImage($at->title, 'articlelist', $at->file);
            } elseif ($at->a6_org_article != 0) {
                $prev = new market_article();
                $img = $prev->generatePreview($at->id, false);
            }

            $data[] = array(
                'id' => $at->id,
                'uuid' => $at->uuid,
                'url' => $at->url,
                'title' => $at->title,
                'image_html' => $img
            );
        }

        $this->view->result = $data;
    }

    public function addfavAction()
    {
        $this->view->clearVars();
        $this->view->success = true;

        if (!Zend_Auth::getInstance()->hasIdentity()) {
            $this->view->success = false;
            $this->view->error = 'notloggedin';
            return;
        }

        $item = Doctrine_Query::create()->from('ContactFavArticle c')->where('c.article_uuid = ? AND c.contact_uuid = ?', array($this->_getParam('article'), $this->user->id))->fetchOne();

        if ($item) {
            return;
        }

        $item = new ContactFavArticle();
        $item->contact_uuid = $this->user->id;
        $item->article_uuid = $this->_getParam('article');
        $item->save();
    }

    public function delfavAction()
    {
        $this->view->clearVars();
        $this->view->success = true;

        if (!Zend_Auth::getInstance()->hasIdentity()) {
            $this->view->success = false;
            $this->view->error = 'notloggedin';
            return;
        }

        $item = Doctrine_Query::create()->from('ContactFavArticle c')->where('c.article_uuid = ? AND c.contact_uuid = ?', array($this->_getParam('article'), $this->user->id))->fetchOne();

        if ($item) {
            $item->delete();
        }
        return;
    }

    public function rebuyAction()
    {

        if (!Zend_Auth::getInstance()->hasIdentity()) {
            $this->_forward('login', 'user', 'default', array(
                'mode' => 'basket'
            ));
            return;
        }

        if ($this->_getParam("pos_uuid", false)) {
            $orderspos = Doctrine_Query::create()->from('Orderspos c')->where('c.uuid = ?', array(
                $this->_getParam("pos_uuid", false)
            ))->fetchOne();

            $basket = new TP_Basket();

            /** @var TP_Basket_Item $data */
            $data = unserialize($orderspos->data);

            $opts = $data->getOptions();
            unset($opts['loaded']);
            $data->setOptions($opts);

            $data->clearFiles();

            $basket->setTempProductClass($data);

            if ($orderspos->layouter_mode == 4 && $data->getLayouterId() != "") {

                $dataCopy = json_decode(file_get_contents($this->view->basepath . "/apps/component/steplayouter/store/copy/" . $data->getLayouterId()), true);
                $articleSess = new LayouterSession();
                $articleSess->contact_id = $orderspos->Orders->contact_id;
                $articleSess->setOrgArticleId($orderspos->Article->uuid);
                $articleSess->setArticleId($dataCopy['uuid']);
                $articleSess->title = $orderspos->Article->title;
                $articleSess->setLayouterModus(4);
                $articleSess->setIsEdit();
                $articleSess->save();
                $data->setLayouterId($articleSess->getArticleId());
                $basket->setTempProductClass($data);
                $this->_redirect("/article/show/uuid/" . $orderspos->Article->uuid . '/' . $data->getLayouterId() . "?rebuy=1");
            }

            if ($orderspos->layouter_mode == 3 && $data->getLayouterId() != "") {
                $articleSession = new TP_Layoutersession();
                $articleSess = $articleSession->getLayouterArticle($orderspos->Article->uuid);
                $articleSess->setDesignerXML($orderspos->a9_designer_data);
                $articleSess->setLayouterModus(2);
                $articleSess->setIsEdit();
                $data->setLayouterId($articleSess->getArticleId());
                $basket->setTempProductClass($data);
                $this->_redirect("/article/show/uuid/" . $orderspos->Article->uuid . '/' . $data->getLayouterId() . "?rebuy=1");
            }

            if ($orderspos->layouter_mode == 2 && $data->getTemplatePrintId() != "") {
                $data->setTemplatePrintId(TP_Util::uuid());
                $articleSession = new TP_Layoutersession();
                $articleSess = $articleSession->getLayouterArticle($orderspos->Article->uuid);
                $articleSess->setLayouterModus(3);
                $articleSess->copyFromBasket($orderspos->Orders, $orderspos, $data->getTemplatePrintId());
                $articleSess->setTemplatePrintId($data->getTemplatePrintId());
                $data->setLayouterId($articleSess->getArticleId());
                $basket->setTempProductClass($data);
                $this->_redirect("/article/show/uuid/" . $orderspos->Article->uuid . '/' . $data->getLayouterId() . "?rebuy=1");
            }

            $this->_redirect("/article/show/uuid/" . $orderspos->Article->uuid . "?rebuy=1");
        }
    }

    public function createAction()
    {
        $this->getHelper('layout')->disableLayout();
    }

    public function getShop()
    {

        return $this->shop;
    }

    public function approvalAction()
    {

        $orderpos = Doctrine_Query::create()->from('Orderspos c')->where('c.uuid = ?', array($this->_getParam('uuid')))->fetchOne();

        if (!Zend_Auth::getInstance()->hasIdentity()) {
            $this->_redirect('/user/login?mode=approval&uuid=' . $this->_getParam('uuid'));
        }

        $user = Zend_Auth::getInstance()->getIdentity();

        $confirm = Doctrine_Query::create()->from('OrdersposConfirmContact c')->where('c.orderspos_id = ? AND c.contact_id = ?', array($orderpos->id, $user['id']))->fetchOne();

        if (!$confirm) {
            $this->view->priorityMessenger('Keine Berechtigung', 'error');
            $this->_redirect('/');
        }

        $config = new Zend_Config_Ini($this->_configPath . '/article/approval.ini', 'approval');
        $form = new EasyBib_Form($config->user->approval);
        $form->addAttribs(array('class' => 'form-horizontal', 'id' => 'approval'));
        $form->addElement('hidden', 'uuid', array('value' => $this->_request->getParam('uuid')));

        $this->view->form = $form;

        $this->view->article = $orderpos->Article;

        $this->view->orderpos = $orderpos;

        $articlesObjs = Zend_Registry::get('articles');

        $articleObj = new $articlesObjs[$this->view->article->typ]();
        unset($articlesObjs);

        $this->view->articleObj = $articleObj;

        if ($this->_request->isPost()) {
            $formData = $this->_request->getPost();
            if ($form->isValid($formData)) {

                $approval = Doctrine_Query::create()->from('Approval c')->where('c.contact_id = ? AND c.orderpos_id = ?', array($user['id'], $orderpos->id))->fetchOne();

                if (!$approval) {
                    $approval = new Approval();
                    $approval->shop_id = $this->shop->id;
                    $approval->install_id = $this->shop->install_id;
                    $approval->text = $formData['text'];
                    $approval->type = $formData['type'];
                    $approval->orderpos_id = $orderpos->id;
                    $approval->contact_id = $user['id'];
                    $approval->save();
                } else {
                    $approval->text = $formData['text'];
                    $approval->type = $formData['type'];
                    $approval->save();
                }

                $confirm = Doctrine_Query::create()->from('ArticleConfirmContact c')
                    ->where('c.article_id = ?', array($orderpos->article_id))->execute();

                foreach ($confirm as $contact) {
                    TP_Queue::process('orderpos_approval_user', 'global', array('pos' => $orderpos, 'contact' => $contact->Contact));
                }

                if ($orderpos->Article->confirmone && $formData['type'] == '2') {
                    $orderpos->status = 110;
                    $orderpos->save();

                    TP_Queue::process('orderpos_approval_accepted', 'global', array('orderspos' => $orderpos, 'approval' => $approval));
                    TP_Queue::process('orderposstatuschange', 'global', $orderpos);

                    $dbMongo = TP_Mongo::getInstance();
                    $dbMongo->Job->insertOne(array(
                        "shop" => $this->shop->id,
                        "event" => "position_status_change",
                        "data" => ["position" => $orderpos->uuid, "status" => 110],
                        "created" => new MongoDB\BSON\UTCDateTime(),
                        "updated" => new MongoDB\BSON\UTCDateTime()
                    ));

                    $dbMongo->Job->insertOne(array(
                        "shop" => $this->shop->id,
                        "event" => "position_contact_approval_accept",
                        "data" => ["contact" => $user['uuid'], "position" => $orderpos->uuid, "order" => $orderpos->Orders->uuid, 'message' => $approval->text],
                        "created" => new MongoDB\BSON\UTCDateTime(),
                        "updated" => new MongoDB\BSON\UTCDateTime()
                    ));
                } elseif (!$orderpos->Article->confirmone && $formData['type'] == '2') {

                    if($orderpos->Article->a6_org_article != 0) {
                        $confirm = Doctrine_Query::create()->from('ArticleConfirmContact c')
                            ->where('c.article_id = ?', array($orderpos->Article->a6_org_article))->execute()->toKeyValueArray('contact_id', 'contact_id');

                    }else{
                        $confirm = Doctrine_Query::create()->from('ArticleConfirmContact c')
                            ->where('c.article_id = ?', array($orderpos->article_id))->execute()->toKeyValueArray('contact_id', 'contact_id');
                    }


                    $approvalArray = Doctrine_Query::create()->from('Approval c')
                        ->where('c.orderpos_id = ? AND c.type = 2 AND c.contact_id IN (' . implode(',', array_keys($confirm)) . ')', array($orderpos->id))->execute();

                    if (count($confirm) == count($approvalArray)) {
                        $orderpos->status = 110;
                        $orderpos->save();

                        TP_Queue::process('orderpos_approval_accepted', 'global', array('orderspos' => $orderpos, 'approval' => $approval));
                        TP_Queue::process('orderposstatuschange', 'global', $orderpos);

                        $dbMongo = TP_Mongo::getInstance();
                        $dbMongo->Job->insertOne(array(
                            "shop" => $this->shop->id,
                            "event" => "position_status_change",
                            "data" => ["position" => $orderpos->uuid, "status" => 110],
                            "created" => new MongoDB\BSON\UTCDateTime(),
                            "updated" => new MongoDB\BSON\UTCDateTime()
                        ));

                        $dbMongo->Job->insertOne(array(
                            "shop" => $this->shop->id,
                            "event" => "position_contact_approval_accept",
                            "data" => ["contact" => $user['uuid'], "position" => $orderpos->uuid, "order" => $orderpos->Orders->uuid, 'message' => $approval->text],
                            "created" => new MongoDB\BSON\UTCDateTime(),
                            "updated" => new MongoDB\BSON\UTCDateTime()
                        ));
                    }
                } elseif ($formData['type'] == '1') {
                    $orderpos->status = 120;
                    $orderpos->save();

                    TP_Queue::process('orderpos_approval_rejected', 'global', array('orderspos' => $orderpos, 'approval' => $approval));

                    $dbMongo = TP_Mongo::getInstance();
                    $dbMongo->Job->insertOne(array(
                        "shop" => $this->shop->id,
                        "event" => "position_status_change",
                        "data" => ["position" => $orderpos->uuid, "status" => 120],
                        "created" => new MongoDB\BSON\UTCDateTime(),
                        "updated" => new MongoDB\BSON\UTCDateTime()
                    ));

                    $dbMongo->Job->insertOne(array(
                        "shop" => $this->shop->id,
                        "event" => "position_contact_approval_declined",
                        "data" => ["contact" => $user['uuid'], "position" => $orderpos->uuid, "order" => $orderpos->Orders->uuid, 'message' => $approval->text],
                        "created" => new MongoDB\BSON\UTCDateTime(),
                        "updated" => new MongoDB\BSON\UTCDateTime()
                    ));
                }

                $ordersposes = Doctrine_Query::create()->from('Orderspos c')->where('c.orders_id = ?', array($orderpos->orders_id))->execute();

                $allApproved = true;
                $isNonApproved = false;

                foreach ($ordersposes as $orderspose) {

                    if ($orderspose->status == 90 || $orderspose->status == 100 || $orderspose->status == 120) {
                        $allApproved = false;
                    }
                    if ($orderspose->status == 120 && !$isNonApproved) {
                        $isNonApproved = true;
                    }
                }

                if ($allApproved && !$isNonApproved) {

                    $order = Doctrine_Query::create()->from('Orders c')->where('c.id = ?', array($orderpos->orders_id))->fetchOne();
                    $order->status = 110;
                    $order->save();
                    TP_Queue::process('order_approval_accepted', 'global', $order);
                    TP_Queue::process('orderstatuschange', 'global', $order);
                    $dbMongo = TP_Mongo::getInstance();
                    $dbMongo->Job->insertOne(array(
                        "shop" => $this->shop->id,
                        "event" => "order_status_change",
                        "data" => ["order" => $order->uuid, "status" => 110],
                        "created" => new MongoDB\BSON\UTCDateTime(),
                        "updated" => new MongoDB\BSON\UTCDateTime()
                    ));

                    $dbMongo->Job->insertOne(array(
                        "shop" => $this->shop->id,
                        "event" => "order_contact_approval_accept",
                        "data" => ["contact" => $user['uuid'], "order" => $orderpos->uuid, 'message' => $approval->text],
                        "created" => new MongoDB\BSON\UTCDateTime(),
                        "updated" => new MongoDB\BSON\UTCDateTime()
                    ));
                } elseif ($isNonApproved) {
                    $order = Doctrine_Query::create()->from('Orders c')->where('c.id = ?', array($orderpos->orders_id))->fetchOne();
                    $order->status = 120;
                    $order->save();
                    TP_Queue::process('order_approval_rejected', 'global', array('orders' => $order, 'approval' => $approval));

                    $dbMongo = TP_Mongo::getInstance();
                    $dbMongo->Job->insertOne(array(
                        "shop" => $this->shop->id,
                        "event" => "order_status_change",
                        "data" => ["order" => $order->uuid, "status" => 120],
                        "created" => new MongoDB\BSON\UTCDateTime(),
                        "updated" => new MongoDB\BSON\UTCDateTime()
                    ));

                    $dbMongo->Job->insertOne(array(
                        "shop" => $this->shop->id,
                        "event" => "order_contact_approval_declined",
                        "data" => ["contact" => $user['uuid'], "order" => $order->uuid, 'message' => $approval->text],
                        "created" => new MongoDB\BSON\UTCDateTime(),
                        "updated" => new MongoDB\BSON\UTCDateTime()
                    ));
                }

                $this->view->priorityMessenger('Erfolgreich gespeichert', 'success');
                $this->_redirect('/user/myapproval');
            }
            $this->view->form->populate($formData);
        }
    }

    /**
     * Render Preview
     *
     * @return void
     */
    public function previewAction()
    {

        $this->getHelper('layout')->disableLayout();

        if ($this->_getParam('uuid') != '') {
            $article = Doctrine_Query::create()->from('Article c')->where('c.uuid = ?', array($this->_getParam('uuid')))->fetchOne();

            if ($article == false) {
                $this->_forward('notfound', 'error');
            }

            if ($this->_getParam('gen')) {
                $this->_helper->viewRenderer->setNoRender();

                $articleSession = new TP_Layoutersession();
                $articleSess = $articleSession->getLayouterArticle($this->_getParam('layouterid'));

                if ($articleSess->getTemplatePrintId() != "") {
                    return TP_Image::generateWidthImgTag(TP_Image::LAYOUTERPREVIEW, TP_Templateprint::generatePreviewBig($article, $layouterPreviewId));
                } else {

                    $file = TP_FOP::generatePngPreview($article, $this->_getParam('layouterid'));
                }
                $this->getHelper('Json')->sendJson(array('success' => true, 'file' => $file));
            }

            $this->view->uuid = $this->_getParam('uuid');
            $this->view->layouterid = $this->_getParam('layouterid');
        } else {
            $this->_forward('notfound', 'error');
        }
    }

    /**
     * Render Preview
     *
     * @return void
     */
    public function previewbigAction()
    {

        $this->getHelper('layout')->disableLayout();

        if ($this->_getParam('uuid') != '') {
            $article = Doctrine_Query::create()->from('Article c')->where('c.uuid = ?', array($this->_getParam('uuid')))->fetchOne();

            if ($article == false) {
                $this->_forward('notfound', 'error');
            }

            if ($this->_getParam('gen')) {
                $this->_helper->viewRenderer->setNoRender();

                $articleSession = new TP_Layoutersession();
                $articleSess = $articleSession->getLayouterArticle($this->_getParam('layouterid'));
                $path = str_split($article->id);
                if ($articleSess->getTemplatePrintId() != "" || file_exists(APPLICATION_PATH . "/../market/templateprint/orginal/" . implode('/', $path) . "/preview.png")) {
                    $file = TP_Image::generate(TP_Image::LAYOUTERPREVIEWBIG, TP_Templateprint::generatePreview($article, $this->_getParam('layouterid')));
                } else {
                    $file = TP_FOP::generatePngPreview($article, $this->_getParam('layouterid'));
                }
                $this->getHelper('Json')->sendJson(array('success' => true, 'file' => $file));
            }

            $this->view->uuid = $this->_getParam('uuid');
            $this->view->layouterid = $this->_getParam('layouterid');
        } else {
            $this->_forward('notfound', 'error');
        }
    }

    public function rateAction()
    {
        $this->getHelper('layout')->disableLayout();
        $this->_helper->viewRenderer->setNoRender();

        $article = Doctrine_Query::create()->from('Article c')->where('c.uuid = ?', array($this->_getParam('uuid')))->fetchOne();

        $ratings = $this->getRequest()->getCookie('rateings');
        if ($ratings) {
            $ratings = json_decode($ratings, true);
        } else {
            $ratings = array();
        }

        if (isset($ratings[$this->_getParam('uuid')]) == false && $article && intval($this->_getParam('vote')) >= 1 && intval($this->_getParam('vote')) <= 5) {
            $article->rate = $article->rate + intval($this->_getParam('vote'));
            $article->rate_count++;
            $article->save();
            $ratings[$this->_getParam('uuid')] = 1;
            $cookie = new Zend_Http_Cookie(
                'rateings',
                json_encode($ratings),
                $this->shop->Domain[0]->name
            );
            setcookie("rateings", json_encode($ratings));

            TP_Queue::process('ratesend', 'global', array('data' => $article->toArray(), 'vote' => intval($this->_getParam('vote'))));
        }
    }

    /**
     * IndexAction
     *
     * @return void
     * @todo Dokumentation der IndexAction
     *
     */
    public function indexAction()
    {

        $filter = new TP_Themesfilter();

        $select = "";
        $this->view->mode = 1;
        if ($this->_getParam('mode')) {
            switch ($this->_getParam('mode')) {
                case 'article':
                    $this->view->mode = 2;
                    $this->view->mode2 = 3;
                    $this->view->mode3 = 1;
                    $this->view->mode4 = 4;
                    $this->view->mode5 = 5;
                    $this->view->mode6 = 6;
                    $this->view->mode7 = 7;
                    $this->view->mode8 = 8;
                    $this->view->mode9 = 9;
                    $select = " AND c.resale != 0";
                    break;
                case 'templates':
                    $this->view->mode = 4;
                    $this->view->mode2 = 3;
                    $this->view->mode3 = 1;
                    $this->view->mode4 = 2;
                    $this->view->mode5 = 5;
                    $this->view->mode6 = 6;
                    $this->view->mode7 = 7;
                    $this->view->mode8 = 8;
                    $this->view->mode9 = 9;
                    $select = " AND c.resale != 0";
                    break;
                case 'articles':
                    $this->view->mode = 3;
                    $this->view->mode2 = 4;
                    $this->view->mode3 = 1;
                    $this->view->mode4 = 2;
                    $this->view->mode5 = 5;
                    $this->view->mode6 = 6;
                    $this->view->mode7 = 7;
                    $this->view->mode8 = 8;
                    $this->view->mode9 = 9;
                    $select = " AND c.not_edit = 1";
                    break;
                case 'customs':
                    $this->view->mode = 5;
                    $this->view->mode2 = 3;
                    $this->view->mode3 = 1;
                    $this->view->mode4 = 2;
                    $this->view->mode5 = 4;
                    $this->view->mode6 = 6;
                    $this->view->mode7 = 7;
                    $this->view->mode8 = 8;
                    $this->view->mode9 = 9;
                    $select = " AND c.a6_org_article != 0";
                    break;
                case 'calc':
                    $this->view->mode = 8;
                    $this->view->mode2 = 3;
                    $this->view->mode3 = 1;
                    $this->view->mode4 = 2;
                    $this->view->mode5 = 4;
                    $this->view->mode6 = 6;
                    $this->view->mode7 = 7;
                    $this->view->mode8 = 5;
                    $this->view->mode9 = 9;
                    $select = " AND (c.typ IN (1,3,6) OR c.upload = 1)";
                    break;
                case 'stock':
                    $this->view->mode = 9;
                    $this->view->mode2 = 3;
                    $this->view->mode3 = 1;
                    $this->view->mode4 = 2;
                    $this->view->mode5 = 4;
                    $this->view->mode6 = 6;
                    $this->view->mode7 = 7;
                    $this->view->mode8 = 8;
                    $this->view->mode9 = 5;
                    $select = " AND c.stock = 1";
                    break;
            }
        }

        if (count($filter->getArticleFilter()) > 0) {
            if ($this->shop->pmb) {
                $articles = Doctrine_Query::create()->from('Article c')->where('c.install_id = ? AND c.private = 0
                                AND c.enable = 1 AND c.display_market = 1' . $select . ' AND c.id IN (SELECT d.article_id
    FROM ArticleThemeMarketArticle as d WHERE
    d.theme_id = ' . implode(' OR d.theme_id =', array_keys($filter->getArticleFilter())) . '
    GROUP BY d.article_id HAVING count(*) = ' . count($filter->getArticleFilter()) . ') ', array($this->shop->install_id))->orderBy('c.pos ASC');
            } else {
                $articles = Doctrine_Query::create()->from('Article c')->where('c.shop_id = ? AND c.private = 0
                                AND c.enable = 1' . $select . ' AND c.id IN (SELECT d.article_id
    FROM ArticleThemeArticle as d WHERE
    d.theme_id = ' . implode(' OR d.theme_id =', array_keys($filter->getArticleFilter())) . '
    GROUP BY d.article_id HAVING count(*) = ' . count($filter->getArticleFilter()) . ') ', array($this->shop->id))->orderBy('c.pos ASC');
            }
        } else {
            if ($this->shop->pmb) {
                $articles = Doctrine_Query::create()->from('Article c')->where('c.install_id = ?' . $select . ' AND c.private = 0 AND c.enable = 1 AND c.display_market = 1', array($this->shop->install_id))->orderBy('c.pos ASC');
            } else {
                $articles = Doctrine_Query::create()->from('Article c')->where('c.shop_id = ?' . $select . ' AND c.private = 0 AND c.enable = 1', array($this->shop->id))->orderBy('c.pos ASC');
            }
        }
        $this->view->sort = "nothing";

        $articles->leftJoin('c.OrgArticle l ON c.a6_org_article = l.id');
        $articles->select('c.*,l.*, (l.a4_abpreis+c.a6_resale_price) as sum');

        if ($this->_getParam('sort')) {
            switch ($this->_getParam('sort')) {
                case 'date':
                    $articles->orderBy('c.created DESC');
                    $this->view->sort = "date";
                    break;
                case 'buy':
                    $articles->orderBy('c.used DESC');
                    $this->view->sort = "buy";
                    break;
                case 'price':
                    $this->view->sort = "price";
                    $articles->orderBy('sum ASC');
                    break;
                case 'visits':
                    $articles->orderBy('c.visits DESC');
                    $this->view->sort = "visits";
                    break;
                case 'author':
                    $this->view->sort = "author";
                    $articles->leftJoin('c.Contact f');
                    $articles->orderBy('f.article_count DESC');
                    break;
            }
        }

        $pager = new Doctrine_Pager($articles, $this->_getParam('page', 1), $this->shop->display_article_count);

        $this->view->articles = $pager->execute();

        $pagerLayout = new TP_Doctrine_Pager_Arrows($pager, new Doctrine_Pager_Range_Sliding(array('chunk' => 5)), '/article/page/{%page_number}/sort/');

        $pagerLayout->setTemplate('<a href="{%url}"><span>{%page}</span></a> ');
        $pagerLayout->setSelectedTemplate('<a href="{%url}"><span><b>{%page}</b></span></a> ');
        $this->view->paginator = $pagerLayout;
    }

    public function myprodchangesAction()
    {

        $articles = $this->_getParam('ok', false);
        if ($articles) {
            foreach ($articles as $key) {
                $article = Doctrine_Query::create()
                    ->from('Article a')
                    ->where('a.id = ? AND a.shop_id = ?', array($key, $this->shop->id))->fetchOne();

                $params = json_decode($article->params, true);
                $changes = json_decode($article->changes, true);

                if (isset($changes['a2_count'])) {
                    unset($params['seitenanzahl']);
                }
                if (isset($changes['text_format'])) {
                    unset($params['text_format']);
                }
                if (isset($changes['text_art'])) {
                    unset($params['produktart']);
                }

                $article->params = json_encode($params);

                $article->changes = "";
                $article->enable = true;
                $article->save();
            }
        }

        $articlegroupselect = "";
        if ($this->_getParam('articlegrouppod', false)) {
            $articlegroupselect = " AND c.articlegroup_id = '" . $this->_getParam('articlegrouppod', false) . "'";
        }

        $articles = Doctrine_Query::create()->from('Article a')
            ->leftJoin('a.ArticleGroupArticle c')
            ->where('a.shop_id = ? AND a.a5_buy = 0 AND a.changes != ""' . $articlegroupselect, array($this->shop->id));

        $this->view->articlegroups = Doctrine_Query::create()
            ->from('ArticleGroup c')
            ->where('c.shop_id = ? AND c.parent = 0 AND c.enable = 1', array($this->shop->id))
            ->orderBy('c.pos ASC')
            ->useQueryCache(true)
            ->execute();

        if ($this->_getParam('searchpod', false)) {
            $articles = $articles->andwhere('(a.text_format LIKE "%' . $this->_getParam('searchpod', false) . '%" OR a.text_art LIKE "%' . $this->_getParam('searchpod', false) . '%" OR a.info LIKE "%' . $this->_getParam('searchpod', false) . '%" OR a.article_nr_extern LIKE "%' . $this->_getParam('searchpod', false) . '%" OR a.einleitung LIKE "%' . $this->_getParam('searchpod', false) . '%" OR a.title LIKE "%' . $this->_getParam('searchpod', false) . '%" OR a.article_nr_intern LIKE "%' . $this->_getParam('searchpod', false) . '%")');
        }

        $this->view->searchpod = $this->_getParam('searchpod', false);
        $this->view->articlegrouppod = $this->_getParam('articlegrouppod', false);


        $articles = $articles->orderBy('a.pos ASC');

        $pager = new Doctrine_Pager($articles, $this->_getParam('page', 1), $this->shop->display_article_count);

        $this->view->articles = $pager->execute();

        $pagerLayout = new TP_Doctrine_Pager_Arrows($pager, new Doctrine_Pager_Range_Sliding(array('chunk' => 5)), '/article/myprodchanges?page={%page_number}');

        $pagerLayout->setTemplate('<a href="{%url}"><span>{%page}</span></a> ');
        $pagerLayout->setSelectedTemplate('<a href="{%url}"><span><b>{%page}</b></span></a> ');

        $this->view->paginator = $pagerLayout;
    }

    public function lastseenAction()
    {
        $lastSeen = new Zend_Session_Namespace('lastseen');

        $this->view->lastSeen = $lastSeen->article;
    }

    public function myprodpodAction()
    {

        $articlegroupselect = "";
        if ($this->_getParam('articlegrouppod', false)) {
            $articlegroupselect = " AND c.articlegroup_id = '" . $this->_getParam('articlegrouppod', false) . "'";
        }

        $articles = Doctrine_Query::create()->from('Article a')
            ->leftJoin('a.ArticleGroupArticle c')
            ->where('a.shop_id = ? AND a.enable = 1' . $articlegroupselect, array($this->shop->id));

        $this->view->articlegroups = Doctrine_Query::create()
            ->from('ArticleGroup c')
            ->where('c.shop_id = ? AND c.parent = 0 AND c.enable = 1', array($this->shop->id))
            ->orderBy('c.pos ASC')
            ->execute();

        $this->view->onlypod = $this->_getParam('onlypod', false);
        $this->view->searchpod = $this->_getParam('searchpod', false);
        $this->view->articlegrouppod = $this->_getParam('articlegrouppod', false);

        if ($this->_getParam('onlypod', false)) {
            $articles = $articles->andwhere('a.a3_special = 1');
        }

        if ($this->_getParam('searchpod', false)) {
            $articles = $articles->andwhere('(a.text_format LIKE "%' . $this->_getParam('searchpod', false) . '%" OR a.text_art LIKE "%' . $this->_getParam('searchpod', false) . '%" OR a.info LIKE "%' . $this->_getParam('searchpod', false) . '%" OR a.article_nr_extern LIKE "%' . $this->_getParam('searchpod', false) . '%" OR a.einleitung LIKE "%' . $this->_getParam('searchpod', false) . '%" OR a.title LIKE "%' . $this->_getParam('searchpod', false) . '%" OR a.article_nr_intern LIKE "%' . $this->_getParam('searchpod', false) . '%")');
        }


        $articles = $articles->orderBy('a.pos ASC');

        $pager = new Doctrine_Pager($articles, $this->_getParam('page', 1), $this->shop->display_article_count);

        $this->view->articles = $pager->execute();

        $pagerLayout = new TP_Doctrine_Pager_Arrows($pager, new Doctrine_Pager_Range_Sliding(array('chunk' => 5)), '/article/myprodpod?page={%page_number}');

        $pagerLayout->setTemplate('<a href="{%url}"><span>{%page}</span></a> ');
        $pagerLayout->setSelectedTemplate('<a href="{%url}"><span><b>{%page}</b></span></a> ');

        $this->view->paginator = $pagerLayout;
    }


    public function myarticleAction()
    {
        $this->view->myarticles = array();

        $this->view->inworkCount = 0;
        $articleSession = new TP_Layoutersession();
        $this->view->inworkCount = count($articleSession->getAllLayouterArticle());

        if ($this->getRequest()->getParam('articlegroup') != "" && $this->getRequest()->getParam('articlegroup') != "all") {

            $this->view->articlegroup = Doctrine_Query::create()
                ->from('ArticleGroup c')
                ->where('c.shop_id = ? AND c.url = ?', array($this->shop->id, $this->getRequest()->getParam('articlegroup')))
                ->orderBy('c.pos ASC')
                ->useQueryCache(true)
                ->fetchOne();
        } else {
            $this->view->articlegroup = new ArticleGroup();
            $this->view->articlegroup->url = 'all';
        }

        $this->view->mode = 6;
        $this->view->mode2 = 3;
        $this->view->mode3 = 1;
        $this->view->mode4 = 2;
        $this->view->mode5 = 4;
        $this->view->mode6 = 5;
        $this->view->mode7 = 7;
        $this->view->mode8 = 8;
        $this->view->mode9 = 9;
        $this->view->mode10 = 10;

        if (Zend_Auth::getInstance()->hasIdentity() == true) {

            $identity = Zend_Auth::getInstance()->getIdentity();

            if ($this->shop->market) {
                $articles = Doctrine_Query::create()->from('Article c')->where('c.enable = 1 AND c.contact_id = ?', array($identity['id']))->orderBy('c.created DESC');
            } else {
                $articles = Doctrine_Query::create()->from('Article c')->where('c.enable = 1 AND c.shop_id = ? AND c.contact_id = ?', array($this->shop->id, $identity['id']))->orderBy('c.created DESC');
            }

            $this->view->sort = "nothing";
            $this->view->dir = "nothing";

            $articles->leftJoin('c.OrgArticle l ON c.a6_org_article = l.id');
            $articles->select('c.*, (c.rate/c.rate_count) as ratesum, l.*, (l.a4_abpreis+c.a6_resale_price) as sum');

            if ($this->shop->product_sort_dir != "" && !$this->_getParam('dir', false)) {
                $this->_setParam('dir', $this->shop->product_sort_dir);
            }

            if ($this->_getParam('dir')) {
                switch ($this->_getParam('dir')) {

                    case 'asc':
                        $dir = 'asc';
                        $this->view->dir = "asc";
                        break;
                    case 'desc':
                    default:
                        $dir = 'desc';
                        $this->view->dir = "desc";
                        break;
                }
            } else {
                $dir = 'DESC';
            }
            if ($this->shop->product_sort != "" && !$this->_getParam('sort', false)) {
                $this->_setParam('sort', $this->shop->product_sort);
            }
            if ($this->_getParam('sort')) {
                switch ($this->_getParam('sort')) {
                    case 'date':
                        $articles->orderBy('c.created ' . $dir);
                        $this->view->sort = "date";
                        break;
                    case 'title':
                        $articles->orderBy('c.title ' . $dir);
                        $this->view->sort = "title";
                        break;
                    case 'buy':
                        $articles->orderBy('c.used ' . $dir);
                        $this->view->sort = "buy";
                        break;
                    case 'price':
                        $this->view->sort = "price";
                        $articles->orderBy('sum ' . $dir);
                        break;
                    case 'visits':
                        $articles->orderBy('c.visits ' . $dir);
                        $this->view->sort = "visits";
                        break;
                    case 'rate':
                        $articles->orderBy('ratesum ' . $dir);
                        $this->view->sort = "rate";
                        break;
                    case 'author':
                        $this->view->sort = "author";
                        $articles->leftJoin('c.Contact f');
                        $articles->orderBy('f.article_count ' . $dir);
                        break;
                }
            } else {
                $articles->orderBy('c.created ' . $dir);
                $this->view->sort = "date";
            }

            $pager = new Doctrine_Pager($articles, $this->_getParam('page', 1), $this->shop->display_article_count);

            $this->view->myarticles = $pager->execute();

            $pagerLayout = new TP_Doctrine_Pager_Arrows($pager, new Doctrine_Pager_Range_Sliding(array('chunk' => 5)), '/article/myarticle/' . $this->view->articlegroup->url . '/page/{%page_number}/sort/');

            $pagerLayout->setTemplate('<a href="{%url}"><span>{%page}</span></a> ');
            $pagerLayout->setSelectedTemplate('<a href="{%url}"><span><b>{%page}</b></span></a> ');

            $this->view->paginator = $pagerLayout;
        }
    }

    protected function getAccountArticle($articles, $account_id)
    {
        $account_article = Doctrine_Query::create()->select('c.article_id as article_id')->from('AccountArticle c')->where('c.account_id = ?', array($account_id))->execute();
        $account = Doctrine_Query::create()->from('Account a')->where('a.id = ?', array($account_id))->fetchOne();

        foreach ($account_article as $art) {
            if (!in_array($art['article_id'], $articles)) {
                $articles[] = $art['article_id'];
            }
        }

        if ($account['filiale_id'] != "" && $account['filiale_id'] != 0 && $account['filiale_id'] != $account_id) {
            $articles = $this->getAccountArticle($articles, $account['filiale_id']);
        }

        return $articles;
    }

    public function inworkAction()
    {

        $this->view->mode = 'inwork';

        $this->view->myarticles = array();

        if ($this->getRequest()->getParam('delall', false)) {
            $articleSession = new TP_Layoutersession();
            $articleSession->deleteAllLayouterArticle($this->shop->id);
        }

        if ($this->getRequest()->getParam('del') != "") {
            $articleSession = new TP_Layoutersession();
            $articleSession->deleteLayouterArticle($this->getRequest()->getParam('del'));
        }

        if ($this->getRequest()->getParam('articlegroup') != "" && $this->getRequest()->getParam('articlegroup') != "all") {

            $this->view->articlegroup = Doctrine_Query::create()
                ->from('ArticleGroup c')
                ->where('c.shop_id = ? AND c.url = ?', array($this->shop->id, $this->getRequest()->getParam('articlegroup')))
                ->orderBy('c.pos ASC')
                ->useQueryCache(true)
                ->fetchOne();
        } else {
            $this->view->articlegroup = new ArticleGroup();
            $this->view->articlegroup->url = 'all';
        }

        $articleSession = new TP_Layoutersession();

        $temp = array();
        foreach ($articleSession->getAllLayouterArticle() as $art) {
            $article = Doctrine_Query::create()->from('Article a')->where('a.uuid = ?', array($art->getOrgArticleId()))->fetchOne();
            if ($article == false) {
                //$articleSession->deleteLayouterArticle($art->getArticleId());
            } else {
                if ($article->Shop->id != $this->shop->id) continue;
                $articlesObjs = Zend_Registry::get('articles');
                if (isset($articlesObjs[$article['typ']])) {
                    $articleObj = new $articlesObjs[$article['typ']]();
                    unset($articlesObjs);
                    array_push($temp, array('session' => $art, 'orginal' => $article, 'obj' => $articleObj));
                }
            }
        }
        $this->view->myarticles = $temp;
        $this->view->inworkCount = count($temp);
    }

    public function offersAction()
    {

        if ($this->_getParam('use', false) && Zend_Auth::getInstance()->hasIdentity()) {
            $row = Doctrine_Query::create()
                ->from('Articleoffer m')
                ->where('m.id = ?', array($this->_getParam('use', false)))
                ->fetchOne();

            $basket = new TP_Basket();
            $basket->setTempProductClass(unserialize($row->data));

            $this->_redirect('/article/show/' . $row->Article->url . '?load=1');
            return;
        }

        if ($this->_getParam('delall', false) && Zend_Auth::getInstance()->hasIdentity()) {
            $rows = Doctrine_Query::create()
                ->from('Articleoffer m')
                ->where('m.contact_id = ?', array($this->user->id))
                ->delete()->execute();
        }

        if ($this->_getParam('del', false) && Zend_Auth::getInstance()->hasIdentity()) {
            $rows = Doctrine_Query::create()
                ->from('Articleoffer m')
                ->where('m.id = ?', array($this->_getParam('del', false)))
                ->delete()->execute();
        }

        if ($this->_getParam('add', false) && Zend_Auth::getInstance()->hasIdentity()) {
            $articleoffer = new Articleoffer();
            $articleoffer->article_id = $this->_getParam('add', false);
            $articleoffer->contact_id = $this->user->id;

            $basket = new TP_Basket();
            $articleoffer->data = serialize($basket->getTempProduct());
            $articleoffer->save();

            $this->_redirect('/article/offers');
        }

        if (Zend_Auth::getInstance()->hasIdentity()) {
            $this->view->articles = Doctrine_Query::create()
                ->from('Articleoffer c')
                ->where('c.contact_id = ?', array($this->user->id))
                ->orderBy('c.created DESC')
                ->execute();
        }
    }

    public function mypersarticleAction()
    {
        $this->view->myarticles = array();
        $this->view->help_mode = $this->_getParam('help_mode', false);
        $this->view->inworkCount = 0;
        $articleSession = new TP_Layoutersession();
        $this->view->inworkCount = count($articleSession->getAllLayouterArticle());

        $defaultNamespace = new Zend_Session_Namespace('settings');

        Zend_Registry::get('log')->debug($this->getRequest()->getParam('articlegroup'));

        if ($this->getRequest()->getParam('articlegroup') != "" && $this->getRequest()->getParam('articlegroup') != "all") {

            $this->view->articlegroup = Doctrine_Query::create()
                ->from('ArticleGroup c')
                ->where('c.shop_id = ? AND c.url = ?', array($this->shop->id, $this->getRequest()->getParam('articlegroup')))
                ->orderBy('c.pos ASC')
                ->useQueryCache(true)
                ->fetchOne();
            $defaultNamespace->article_mypersarticle_url = $this->view->articlegroup->url;
            $container = $this->view->navigation()->getContainer();
            if ($container->findOneBy('id', 'mypersarticle')) {
                $container->findOneBy('id', 'mypersarticle')->addPage(array(
                    'label' => $this->view->articlegroup->getTitle(),
                    'active' => true,
                    'class' => 'active',
                    'id' => 'mypersarticle_produkt',
                    'uri' => '/article/mypersarticle/' . $this->view->articlegroup->url
                ));

                $this->view->navigation($container);
            }
            $page = $container->findOneBy('id', $this->view->articlegroup->id);
            if ($page) {
                $page->setActive(true);
            }
        } else {
            $this->view->articlegroup = new ArticleGroup();
            $this->view->articlegroup->url = 'all';
            $defaultNamespace->article_mypersarticle_url = 'all';
            $container = $this->view->navigation()->getContainer();
            $page = $container->findOneBy('id', "All");
            if ($page) {
                $page->setActive(true);
            }
        }

        $this->view->mode = 7;
        $this->view->mode2 = 3;
        $this->view->mode3 = 1;
        $this->view->mode4 = 2;
        $this->view->mode5 = 4;
        $this->view->mode6 = 5;
        $this->view->mode7 = 6;
        $this->view->mode8 = 8;
        $this->view->mode9 = 9;
        $this->view->mode10 = 10;

        if (Zend_Auth::getInstance()->hasIdentity() == true) {

            $identity = Zend_Auth::getInstance()->getIdentity();

            $mode = new Zend_Session_Namespace('adminmode');
            if ($mode->over_ride_contact != false && $mode->over_ride_contact != 'new') {
                $articles = Doctrine_Query::create()->from('Article a')->leftJoin('a.ContactArticle c')->where('a.enable = 1 AND a.a6_org_article = 0 AND (c.contact_id = ? OR c.contact_id = ?)', array($identity['id'], $mode->over_ride_contact))->orderBy('a.pos ASC');
            } else {
                if ($this->shop->install_id == 17) {

                    $articles_contact = Doctrine_Query::create()->from('ContactArticle c')->where('c.contact_id = ?', array($identity['id']))->execute()->toArray();

                    $temp = array();
                    $temp[] = 0;
                    foreach ($articles_contact as $art) {
                        $temp[] = $art['article_id'];
                    }
                    Zend_Registry::get('log')->debug("BEFORE");
                    $temp = $this->getAccountArticle($temp, $identity['account_id']);
                    Zend_Registry::get('log')->debug("AFTER");
                    $articles = Doctrine_Query::create()->from('Article a')->where('a.enable = 1 AND a.a6_org_article = 0 AND a.shop_id = ? AND a.id IN (' . implode(',', $temp) . ')', array($this->shop->id))->orderBy('a.pos ASC');

                    $articlegroupsd = Doctrine_Query::create()->from('ArticleGroup c')->where('c.shop_id = ? AND c.notinmenu = 0 AND c.enable = 1 AND c.parent=0 AND (language = ? OR language = \'all\')', array(
                        $this->shop->id,
                        Zend_Registry::get('locale')
                    ))->orderBy('c.pos ASC')->execute();


                    foreach ($articlegroupsd as $articlegroup) {
                        $count = Doctrine_Query::create()->from('ArticleGroupArticle c')->where('c.articlegroup_id = ? AND c.article_id IN (' . implode(',', $temp) . ')', array(
                            $articlegroup->id
                        ))->execute()->count();

                        if ($count == 0) {
                            $page = $this->view->navigation()->findOneBy('id', $articlegroup->id);
                            if ($page)
                                $page->setVisible(false);
                        }
                    }
                } else {
                    $articles_contact = Doctrine_Query::create()->from('ContactArticle c')->where('c.contact_id = ?', array($identity['id']))->execute()->toArray();

                    $temp = array();
                    $temp[] = 0;
                    foreach ($articles_contact as $art) {
                        $temp[] = $art['article_id'];
                    }

                    $temp = $this->getAccountArticle($temp, $identity['account_id']);


                    $articles = Doctrine_Query::create()->from('Article a')->where('a.enable = 1 AND a.a6_org_article = 0 AND a.shop_id = ? AND a.id IN (' . implode(',', $temp) . ')', array($this->shop->id))->orderBy('a.pos ASC');
                }
            }

            $this->view->sort = "nothing";
            $this->view->dir = "nothing";

            if ($this->view->articlegroup->url != "all") {
                $articles->leftJoin('a.ArticleGroupArticle c')->addWhere("c.articlegroup_id = ?", array($this->view->articlegroup->id));
            }
            $articles->leftJoin('a.OrgArticle l ON a.a6_org_article = l.id');
            $articles->select('a.*, (a.rate/a.rate_count) as ratesum ,l.*, (l.a4_abpreis+a.a6_resale_price) as sum');

            if ($this->shop->product_sort_dir != "" && !$this->_getParam('dir', false)) {
                $this->_setParam('dir', $this->shop->product_sort_dir);
            }

            if ($this->_getParam('dir')) {
                switch ($this->_getParam('dir')) {

                    case 'asc':
                        $dir = 'asc';
                        $this->view->dir = "asc";
                        break;
                    case 'desc':
                    default:
                        $dir = 'desc';
                        $this->view->dir = "desc";
                        break;
                }
            } else {
                $dir = 'desc';
                $this->view->dir = "desc";
            }

            if ($this->shop->product_sort != "" && !$this->_getParam('sort', false)) {
                $this->_setParam('sort', $this->shop->product_sort);
            }

            if ($this->_getParam('sort')) {
                switch ($this->_getParam('sort')) {
                    case 'date':
                        $articles->orderBy('a.created ' . $dir);
                        $this->view->sort = "date";
                        break;
                    case 'buy':
                        $articles->orderBy('a.used ' . $dir);
                        $this->view->sort = "buy";
                        break;
                    case 'price':
                        $this->view->sort = "price";
                        $articles->orderBy('sum ' . $dir);
                        break;
                    case 'visits':
                        $articles->orderBy('a.visits ' . $dir);
                        $this->view->sort = "visits";
                        break;
                    case 'rate':
                        $articles->orderBy('ratesum ' . $dir);
                        $this->view->sort = "rate";
                        break;
                    case 'name' :
                        $articles->orderBy('a.title ' . $dir);
                        $this->view->sort = "name";
                        break;
                    case 'pos' :
                        $articles->orderBy('a.pos ' . $dir);
                        $this->view->sort = "pos";
                        break;
                    case 'author':
                        $this->view->sort = "author";
                        $articles->leftJoin('a.Contact f');
                        $articles->orderBy('f.article_count ' . $dir);
                        break;
                }
            } else {
                $articles->orderBy('a.pos ' . $dir);
                $this->view->sort = "pos";
            }

            $pager = new Doctrine_Pager($articles, $this->_getParam('page', 1), $this->shop->display_article_count);

            $this->view->myarticles = $pager->execute();

            $pagerLayout = new TP_Doctrine_Pager_Arrows($pager, new Doctrine_Pager_Range_Sliding(array('chunk' => 5)), '/article/mypersarticle/' . $this->view->articlegroup->url . '/page/{%page_number}/sort/');

            $pagerLayout->setTemplate('<a href="{%url}"><span>{%page}</span></a> ');
            $pagerLayout->setSelectedTemplate('<a href="{%url}"><span><b>{%page}</b></span></a> ');

            $this->view->paginator = $pagerLayout;
        }
    }

    public function calcAction()
    {
        $this->view->success = true;

        $this->view->layouterPreviewId = $this->getRequest()->getParam('layouter', false);

        if ($this->shop->pmb) {
            if ($this->_getParam('article') != '') {
                $article = Doctrine_Query::create()->from('Article c')->where('c.enable = 1 AND c.shop_id = ? AND c.url = ?', array($this->shop->id, $this->_getParam('article')))->fetchOne();
            } elseif ($this->_getParam('id') != '') {
                $article = Doctrine_Query::create()->from('Article c')->where('c.enable = 1 AND c.shop_id = ? AND c.id = ?', array($this->shop->id, intval($this->_getParam('id'))))->fetchOne();
            } elseif ($this->_getParam('uuid') != '') {
                $article = Doctrine_Query::create()->from('Article c')->where('c.enable = 1 AND c.shop_id = ? AND c.uuid = ?', array($this->shop->id, $this->_getParam('uuid')))->fetchOne();
            }
        } else {
            if ($this->_getParam('article') != '') {
                $article = Doctrine_Query::create()->from('Article c')->where('c.enable = 1 AND c.shop_id = ? AND c.url = ?', array($this->shop->id, $this->_getParam('article')))->fetchOne();
            } elseif ($this->_getParam('id') != '') {
                $article = Doctrine_Query::create()->from('Article c')->where('c.enable = 1 AND c.shop_id = ? AND c.id = ?', array($this->shop->id, intval($this->_getParam('id'))))->fetchOne();
            } elseif ($this->_getParam('uuid') != '') {
                $article = Doctrine_Query::create()->from('Article c')->where('c.enable = 1 AND c.shop_id = ? AND c.uuid = ?', array($this->shop->id, $this->_getParam('uuid')))->fetchOne();
            }
        }

        $articlesObjs = Zend_Registry::get('articles');

        $articleObj = new $articlesObjs[$article->typ]();
        unset($articlesObjs);

        $this->view->article = $article;
        $this->view->form = array();

        $this->view->mwert = $article->mwert;
        $this->view->netto = $articleObj->createFrontend($this, $this->translate, $article, true);
        if (Zend_Auth::getInstance()->hasIdentity()) {
            $this->view->netto = $this->view->netto * $this->user->getPriceFactor() * $this->shop->getPriceFactor() * $this->user->Account->getPriceFactor();
        } else {
            $this->view->netto = $this->view->netto * $this->shop->getPriceFactor();
        }
        $this->view->netto_raw = $this->view->netto;

        $this->view->form[] = array('type' => 'hidden', 'name' => 'ajax_calc_id', 'value' => $article->url);
        if ($this->_getParam('loaded') == 1 || $this->_getParam('load') == 1) {
            $this->view->form[] = array('type' => 'hidden', 'name' => 'loaded', 'value' => 1);
        }
        $formDataT = $this->_request->getPost();

        $formData = array();
        foreach ($formDataT as $key => $var) {
            if ($var != '') {
                $formData[$key] = $var;
            }
        }
        $basket = new TP_Basket();
        $sessart = $basket->getTempProduct($article->uuid);

        $this->view->weight = $sessart->getWeight();
        $this->view->weight_single = $sessart->getWeightSingle();
        $sessart->setOptions($formData);
        $sessart->setNetto($this->view->netto);
        $sessart->setSteuer(($this->view->netto / 100 * $article->mwert));
        $sessart->setBrutto($this->view->netto + ($this->view->netto / 100 * $article->mwert));

        if ($this->getRequest()->getParam('layouter') != "") {
            $sessart->setLayouterId($this->getRequest()->getParam('layouter'));
            $this->view->form[] = array('type' => 'hidden', 'name' => 'layouter', 'value' => $this->getRequest()->getParam('layouter'));
        }

        $count = intval($this->_getParam('count', 1));
        $this->view->steuer_all = $this->view->currency->toCurrency((($this->view->netto * $count) / 100 * $article->mwert));
        $this->view->mwert_all = $article->mwert;
        $this->view->brutto_all = $this->view->currency->toCurrency(($this->view->netto * $count) + (($this->view->netto * $count) / 100 * $article->mwert));
        $this->view->netto_all = $this->view->currency->toCurrency(($this->view->netto * $count));

        $this->view->steuer = $this->view->currency->toCurrency(($this->view->netto / 100 * $article->mwert));
        $this->view->mwert = $article->mwert;
        $this->view->brutto = $this->view->currency->toCurrency($this->view->netto + ($this->view->netto / 100 * $article->mwert));
        $this->view->netto = $this->view->currency->toCurrency($this->view->netto);

        $this->view->article_uuid = $article->uuid;
        $this->view->sessart_netto = $sessart->getNetto();
        $this->view->layouterPreviewId = $sessart->getLayouterId();
    }

    public function printofferAction()
    {
        $this->view->success = true;

        $this->view->layouterPreviewId = $this->getRequest()->getParam('layouter', false);

        if ($this->shop->pmb) {
            if ($this->_getParam('article') != '') {
                $article = Doctrine_Query::create()->from('Article c')->where('c.enable = 1 AND c.url = ?', array($this->_getParam('article')))->fetchOne();
            } elseif ($this->_getParam('id') != '') {
                $article = Doctrine_Query::create()->from('Article c')->where('c.enable = 1 AND c.id = ?', array(intval($this->_getParam('id'))))->fetchOne();
            } elseif ($this->_getParam('uuid') != '') {
                $article = Doctrine_Query::create()->from('Article c')->where('c.enable = 1 AND c.uuid = ?', array($this->_getParam('uuid')))->fetchOne();
            }
        } else {
            if ($this->_getParam('article') != '') {
                $article = Doctrine_Query::create()->from('Article c')->where('c.enable = 1 AND c.shop_id = ? AND c.url = ?', array($this->shop->id, $this->_getParam('article')))->fetchOne();
            } elseif ($this->_getParam('id') != '') {
                $article = Doctrine_Query::create()->from('Article c')->where('c.enable = 1 AND c.shop_id = ? AND c.id = ?', array($this->shop->id, intval($this->_getParam('id'))))->fetchOne();
            } elseif ($this->_getParam('uuid') != '') {
                $article = Doctrine_Query::create()->from('Article c')->where('c.enable = 1 AND c.shop_id = ? AND c.uuid = ?', array($this->shop->id, $this->_getParam('uuid')))->fetchOne();
            }
        }

        $articlesObjs = Zend_Registry::get('articles');

        $articleObj = new $articlesObjs[$article->typ]();
        unset($articlesObjs);

        $this->view->article = $article;
        $this->view->form = array();

        $basket = new TP_Basket();
        $sessart = $basket->getTempProduct();

        $this->_request->setPost($sessart->getOptions());

        $this->view->mwert = $article->mwert;
        $this->view->netto = $articleObj->createFrontend($this, $this->translate, $article, true);

        $shop = $this->shop;

        $reportPath = Zend_Registry::get('shop_path') . "/reports/";

        $what = 'offer_blank';
        $params = array();
        if (Zend_Auth::getInstance()->hasIdentity()) {
            $what = 'offer_contact';

            $params = $this->user->toArray();
            $params['company'] = $params['self_department'];
            if ($params['self_anrede'] == 1) {
                $params['self_anrede'] = "Herr";
            }
            if ($params['self_anrede'] == 2) {
                $params['self_anrede'] = "Frau";
            }
            if ($params['self_anrede'] == 3) {
                $params['self_anrede'] = "Firma";
            }
            if ($params['self_anrede'] == 4) {
                $params['self_anrede'] = "Herr Dr.";
            }
            if ($params['self_anrede'] == 5) {
                $params['self_anrede'] = "Frau Dr.";
            }
            if ($params['self_anrede'] == 6) {
                $params['self_anrede'] = "Herr Prof.";
            }
            if ($params['self_anrede'] == 7) {
                $params['self_anrede'] = "Frau Prof.";
            }
            if ($params['self_anrede'] == 8) {
                $params['self_anrede'] = "Herr Prof. Dr.";
            }
            if ($params['self_anrede'] == 9) {
                $params['self_anrede'] = "Frau Prof. Dr.";
            }
            if ($params['self_anrede'] == 10) {
                $params['self_anrede'] = "Schwester";
            }
        }

        if (file_exists(APPLICATION_PATH . '/design/clients/' . $this->shop->uid . '/reports/')) {
            $reportPath = APPLICATION_PATH . '/design/clients/' . $this->shop->uid . '/reports/';
        }

        if ($this->shop->market == 1 && $this->shop->reportsite1 == "") {

            $shop = Doctrine_Query::create()->select()->from('Shop s')->where('s.id = ?', array($this->Install->defaultmarket))->fetchOne();

            $reportPath = APPLICATION_PATH . "/design/clients/" . $shop->uid . "/reports/";
        } elseif (!file_exists(Zend_Registry::get('shop_path') . "/reports/" . $what . ".jrxml")) {
            if (!file_exists(Zend_Registry::get('shop_path') . "/reports")) {
                mkdir(Zend_Registry::get('shop_path') . "/reports", 0777, true);
            }

            if (!file_exists(Zend_Registry::get('layout_path') . "/reports/" . $what . ".jrxml")) {

                return false;
            }

            copy(Zend_Registry::get('layout_path') . "/reports/" . $what . ".jrxml", Zend_Registry::get('shop_path') . "/reports/" . $what . ".jrxml");
        }

        $params['netto'] = $this->view->currency->toCurrency($this->view->netto);
        $params['brutto'] = $this->view->currency->toCurrency($this->view->netto + ($this->view->netto / 100 * $article->mwert));

        $params['article'] = $article->toArray();

        $params['mwerttable'] = array($article->mwert => $this->view->currency->toCurrency(($this->view->netto / 100 * $article->mwert)));

        $data = array();
        foreach ($this->view->form as $fm) {
            if ($fm['type'] == "hidden" || $fm['type'] == "submit" || $fm['value'] == "") {
                continue;
            }
            if ($fm['type'] == 'select') {
                foreach ($fm['options'] as $opt) {
                    if ($opt['key'] == $fm['value']) {
                        $fm['value'] = $opt['value'];
                    }
                }
                $data[] = $fm;
            } elseif ($fm['type'] == 'radio') {
                foreach ($fm['options'] as $key => $opt) {
                    if ($key == $fm['value']) {
                        $fm['value'] = $opt;
                    }
                }
                $data[] = $fm;
            } else {
                $data[] = $fm;
            }
        }

        $this->view->steuer = $this->view->currency->toCurrency(($this->view->netto / 100 * $article->mwert));
        $this->view->mwert = $article->mwert;
        $this->view->brutto = $this->view->currency->toCurrency($this->view->netto + ($this->view->netto / 100 * $article->mwert));
        $this->view->netto = $this->view->currency->toCurrency($this->view->netto);

        $this->view->clearVars();
        Zend_Registry::get('log')->debug(print_r($data, true));
        try {
            $file = new TP_Rdl(
                $reportPath,
                $what . ".jrxml",
                1,
                $params,
                $data
            );

            $background = false;
            if ($what == "offer_contact" && $shop->report_background_offer_contact) {
                $background = true;
            } elseif ($what == "offer_blank" && $shop->report_background_offer_blank) {
                $background = true;
            }
            $host = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] && !in_array(strtolower($_SERVER['HTTPS']), array('off', 'no'))) ? 'https' : 'http';
            $host .= '://' . $_SERVER['HTTP_HOST'];


            if ($shop->reportsite1 != "" && $background) {
                $pdf = Doctrine_Query::create()
                    ->from('Image as i')
                    ->where('i.id = ?')->fetchOne(array($shop->reportsite1));

                if (!file_exists(PUBLIC_PATH . '/temp/thumb/' . $shop->uid . '/' . $shop->reportsite1 . '.jpg')) {
                    $fileDownload = 'https://cdn.shopsrv.de/' . TP_Util::encode($host . '/' . $pdf->path) . '?w=2480&fm=jpg';

                    file_put_contents(PUBLIC_PATH . '/temp/thumb/' . $shop->uid . '/' . $shop->reportsite1 . '.jpg', file_get_contents($fileDownload));
                }


                $file->addSubmittals(PUBLIC_PATH . '/temp/thumb/' . $shop->uid . '/' . $shop->reportsite1 . '.jpg');
            }
            if ($shop->reportsite2 != "" && $background) {
                $pdf = Doctrine_Query::create()
                    ->from('Image as i')
                    ->where('i.id = ?')->fetchOne(array($shop->reportsite2));

                if (!file_exists(PUBLIC_PATH . '/temp/thumb/' . $shop->uid . '/' . $shop->reportsite2 . '.jpg')) {
                    $fileDownload = 'https://cdn.shopsrv.de/' . TP_Util::encode($host . '/' . $pdf->path) . '?w=2480&fm=jpg';

                    file_put_contents(PUBLIC_PATH . '/temp/thumb/' . $shop->uid . '/' . $shop->reportsite2 . '.jpg', file_get_contents($fileDownload));
                }


                $file->addSubmittals(PUBLIC_PATH . '/temp/thumb/' . $shop->uid . '/' . $shop->reportsite2 . '.jpg');
            }

            if ($shop->report_agb != "" && $background) {
                $pdf = Doctrine_Query::create()
                    ->from('Image as i')
                    ->where('i.id = ?')->fetchOne(array($shop->report_agb));


                if (!file_exists(PUBLIC_PATH . '/temp/thumb/' . $shop->uid . '/' . $shop->report_agb . '.jpg')) {
                    $fileDownload = 'https://cdn.shopsrv.de/' . TP_Util::encode($host . '/' . $pdf->path) . '?w=2480&fm=jpg';

                    file_put_contents(PUBLIC_PATH . '/temp/thumb/' . $shop->uid . '/' . $shop->report_agb . '.jpg', file_get_contents($fileDownload));
                }


                $file->setAgb(PUBLIC_PATH . '/temp/thumb/' . $shop->uid . '/' . $shop->report_agb . '.jpg');
            }

            if (!file_exists(APPLICATION_PATH . '/../data/customerdocs/' . $shop->id)) {
                mkdir(APPLICATION_PATH . '/../data/customerdocs/' . $shop->id);
            }

            $alias = Zend_Session::getId();

            if (!file_exists(APPLICATION_PATH . '/../data/customerdocs')) {
                mkdir(APPLICATION_PATH . '/../data/customerdocs', 0777);
            }
            if (!file_exists(APPLICATION_PATH . '/../data/customerdocs/' . $shop->id)) {
                mkdir(APPLICATION_PATH . '/../data/customerdocs/' . $shop->id, 0777);
            }

            $file->export(new TP_Rdl_Renderer_Pdf(APPLICATION_PATH . '/../data/customerdocs/' . $shop->id . "/" . $alias . '_' . $what . '.pdf'));
        } catch (Exception $e) {
            Zend_Registry::get('log')->debug($e->getMessage());
            return false;
        }

        header('Content-type: application/pdf');

        // Es wird downloaded.pdf benannt
        header('Content-Disposition: attachment; filename="offer.pdf"');

        // Die originale PDF Datei heißt original.pdf
        readfile(APPLICATION_PATH . '/../data/customerdocs/' . $shop->id . "/" . $alias . '_' . $what . '.pdf');
        unlink(APPLICATION_PATH . '/../data/customerdocs/' . $shop->id . "/" . $alias . '_' . $what . '.pdf');
        die();
    }

    public function showAction()
    {

        if (strpos($_SERVER['REQUEST_URI'], 'png') !== false || strpos($_SERVER['REQUEST_URI'], 'jpg') !== false) {
            die();
        }

        $articleSession = new TP_Layoutersession();

        $basket = new TP_Basket();
        /*if($this->getRequest()->getParam('layouter')) {
            if($basket->checkIsLayouterIdInUse($this->getRequest()->getParam('layouter')) && !$this->_getParam('load')) {
                
                $id = $articleSession->copySession($this->getRequest()->getParam('layouter'));

                $this->getRequest()->setParam('layouter', $id);
            }
        }*/


        if ($this->shop->pmb) {
            if ($this->_getParam('article') != '') {
                $article = Doctrine_Query::create()->from('Article c')->where('c.enable = 1 AND c.install_id = ? AND (c.display_market = 1 OR c.shop_id = ?) AND c.url = ?', array($this->install->id, $this->shop->id, $this->_getParam('article')))->fetchOne();
            } elseif ($this->_getParam('id') != '') {
                $article = Doctrine_Query::create()->from('Article c')->where('c.enable = 1 AND c.install_id = ? AND (c.display_market = 1 OR c.shop_id = ?) AND c.id = ?', array($this->install->id, $this->shop->id, intval($this->_getParam('id'))))->fetchOne();
            } elseif ($this->_getParam('uuid') != '') {
                $article = Doctrine_Query::create()->from('Article c')->where('c.enable = 1 AND c.install_id = ? AND (c.display_market = 1 OR c.shop_id = ?) AND c.uuid = ?', array($this->install->id, $this->shop->id, $this->_getParam('uuid')))->fetchOne();
            }
        } else {
            if ($this->_getParam('article') != '') {
                $article = Doctrine_Query::create()->from('Article c')->where('c.enable = 1 AND c.install_id = ? AND c.shop_id = ? AND c.url = ?', array($this->install->id, $this->shop->id, $this->_getParam('article')))->fetchOne();
            } elseif ($this->_getParam('id') != '') {
                $article = Doctrine_Query::create()->from('Article c')->where('c.enable = 1 AND c.install_id = ? AND c.shop_id = ? AND c.id = ?', array($this->install->id, $this->shop->id, intval($this->_getParam('id'))))->fetchOne();
            } elseif ($this->_getParam('uuid') != '') {
                $article = Doctrine_Query::create()->from('Article c')->where('c.enable = 1 AND c.install_id = ? AND c.shop_id = ? AND c.uuid = ?', array($this->install->id, $this->shop->id, $this->_getParam('uuid')))->fetchOne();
            }
        }

        /** @var TP_Basket_Item $sessart */
        $sessart = $basket->getTempProduct($article->uuid);
        if ($this->getRequest()->getParam('layouter') != "") {
            $sessart->setLayouterId($this->getRequest()->getParam('layouter'));
        }

        $this->view->layouterPreviewId = $this->getRequest()->getParam('layouter');

        $this->view->inworkCount = 0;

        $this->view->inworkCount = count($articleSession->getAllLayouterArticle());

        if ($this->view->layouterPreviewId) {
            $this->view->layouterSession = $articleSession->getLayouterArticle($sessart->getLayouterId());
            $sessart->setTemplatePrintId($this->view->layouterSession->getTemplatePrintId());

            $tmpOptions = $sessart->getOptions();
            unset($tmpOptions['weight']);
            unset($tmpOptions['ajax_calc_id']);
            unset($tmpOptions['upload_mode']);
            unset($tmpOptions['layouter']);

            $this->getRequest()->setParams($tmpOptions);

        } else {
            $sessart->setTemplatePrintId(false);
        }

        if (($this->shop->private_product && !Zend_Auth::getInstance()->hasIdentity()) && !$article->not_buy) {
            $this->_forward('login', 'user', 'default', array('mode' => 'product_show', 'uuid' => '/article/show/' . $this->_getParam('article') . '/' . $this->_getParam('articlegroup', '')));

            return;
        }

        if ($article == false) {
            $this->_forward('notfound', 'error');
            return;
        }

        if ($article->upload_collecting_orders) {
            if ($this->shop->install_id == 16) {
                $config = new Zend_Config_Ini($this->_configPath . '/user/addcontactdp.ini', 'register');
            } else {
                $config = new Zend_Config_Ini($this->_configPath . '/user/addcontact.ini', 'register');
            }

            if ($config->global) {
                $addcontactform = new EasyBib_Form($config->global);
            } else {
                $addcontactform = new Zend_Form();
                $addcontactform->addAttribs(array('class' => 'niceform', 'id' => 'userreg'));
            }

            $subform = new Zend_Form_SubForm($config->user->login);

            $addcontactform->addSubForm($subform, 'login');

            $addcontactform->login->self_email->addPrefixPath('TP_Validate', 'TP/Validate/', 'Validate');

            if (isset($addcontactform->login->private_products)) {
                $private_article = Doctrine_Query::create()->from('Article c')->where('c.enable = 1 AND c.private = 1 AND c.install_id = ? AND c.shop_id = ?', array($this->install->id, $this->shop->id))->execute();

                foreach ($private_article as $privateArticle) {
                    $addcontactform->login->private_products->addMultiOption($privateArticle->id, $privateArticle->title);
                }
            }

            $rech = new Zend_Form_SubForm();
            $rech->addPrefixPath("TP_Form", "TP/Form");
            $rech->setConfig($config->user->rech);

            $addcontactform->addSubForm($rech, 'rech');
            if ($this->shop->install_id == 16) {
                $rech = new Zend_Form_SubForm();
                $rech->addPrefixPath("TP_Form", "TP/Form");
                $rech->setConfig($config->user->addr2);

                $addcontactform->addSubForm($rech, 'addr2');

                $rech = new Zend_Form_SubForm();
                $rech->addPrefixPath("TP_Form", "TP/Form");
                $rech->setConfig($config->user->addr3);

                $addcontactform->addSubForm($rech, 'addr3');
            }

            if (isset($formData['rech']) && isset($formData['rech']['lieferja']) && $formData['rech']['lieferja'] == 1) {
                $addcontactform->addSubForm(new Zend_Form_SubForm($config->user->liefer), 'liefer');
            }

            $addcontactform->addElement($config->user->submit->elements->submit->type, 'submit', $config->user->submit->elements->submit->options);

            $this->view->addcontactform = $addcontactform;
        }

        $this->setHeadData($article);
        $container = $this->view->navigation()->getContainer();
        $container->findOneBy('id', 'Article')->addPage(array(
            'label' => $article->getTitle(),
            'active' => true,
            'class' => 'active',
            'id' => 'articleshow',
            'uri' => '/article/show/' . $article->url
        ));

        $this->view->navigation($container);

        if (isset($_SERVER["HTTP_REFERER"]) && stristr($_SERVER["HTTP_REFERER"], $article->url) == false) {
            $article->visits = $article->visits + 1;
            $article->save();
        }

        $lastseen = new Zend_Session_Namespace('lastseen');
        if (!isset($lastseen->article)) {
            $lastseen->article = array();
        }
        if (!in_array($article->url, array_column($lastseen->article, 'url'))) {
            array_unshift($lastseen->article, array('url' => $article->url, 'title' => $article->title));
        }
        if (count($lastseen->article) > 5) {
            $lastseen->article = array_slice($lastseen->article, 5);
        }

        $this->view->article = $article;
        require_once(APPLICATION_PATH . '/helpers/Image.php');
        $img = new TP_View_Helper_Image();
        if ($article->typ == 6 && $article->a6_org_article != 0) {

            $this->view->htmlsnippet = '<div id="pscwrapper">
						<div id="titel">' . $article->title . '</div>
						<div id="bild"><img src="' . $this->view->basepath . '/article/thumbnail/' . $article->uuid . '"/></div>
						<div id="einleitung">' . $article->einleitung . '</div>
						<div id="link"><a href="http://' . $article->Shop->Domain[0]->name . '/article/show/uuid/' . $article->uuid . '">Weiter</a></div>
					</div>';
        } else {
            if ($article->file != "") {
                $this->view->htmlsnippet = '<div id="pscwrapper">
							<div id="titel">' . $article->title . '</div>
							<div id="bild"><img src="' . $this->view->basepath . '/article/thumbnail/' . $article->uuid . '"/></div>
							<div id="einleitung">' . $article->einleitung . '</div>
							<div id="link"><a href="http://' . $article->Shop->Domain[0]->name . '/article/show/uuid/' . $article->uuid . '">Weiter</a></div>
						</div>';
            } else {
                $this->view->htmlsnippet = '<div id="pscwrapper">
							<div id="titel">' . $article->title . '</div>
							<div id="bild"><img width="171" src="' . $this->view->basepath . '/article/thumbnail/' . $article->uuid . '"/></div>
							<div id="einleitung">' . $article->einleitung . '</div>
							<div id="link"><a href="http://' . $article->Shop->Domain[0]->name . '/article/show/uuid/' . $article->uuid . '">Weiter</a></div>
						</div>';
            }
        }

        $this->view->form = new Zend_Form();

        $this->view->form->setAttrib('enctype', 'application/x-www-form-urlencoded');
        //$this->view->form->setAttrib ( 'action', '/article/show/' . $article->url );

        if ($this->_getParam('articlegroup', '') != "") {
            $activeNav = $this->view->navigation()->findOneById('ArticlegroupsArticle')->findByUri('/overview/' . $this->_getParam('articlegroup', ''));
            if ($activeNav != null) {
                $activeNav->active = false;
                $activeNav->setClass();

                $activeNav->addPage(array('label' => $article->title, 'id' => $article->id . '_' . $article->url, 'uri' => '/article/show/' . $article->url . '/' . $this->_getParam('articlegroup', ''), 'active' => false));

                $activeNav = $this->view->navigation()->findOneById('ArticlegroupsArticle')->findById($article->id . '_' . $article->url);

                if ($activeNav != null) {
                    $activeNav->active = true;
                    $activeNav->setClass("active");
                }
            }

            $this->view->articlegroup = Doctrine_Query::create()
                ->from('ArticleGroup c')
                ->where('c.shop_id = ? AND c.url = ?', array($this->shop->id, $this->getRequest()->getParam('articlegroup')))
                ->orderBy('c.pos ASC')
                ->useQueryCache(true)
                ->fetchOne();

            $this->view->form->setAttrib('action', '/article/show/' . $article->url . '/' . $this->_getParam('articlegroup', ''));
        }

        if ($article->private == true) {

            if (Zend_Auth::getInstance()->hasIdentity() == false) {
                $this->_forward('index', 'index', 'default');
            } else {

                $ident = Zend_Auth::getInstance()->getIdentity();
                $success = false;
                foreach ($article->ContactArticle as $contactArticle) {
                    if ($contactArticle->contact_id == $ident['id']) {
                        $success = true;
                    }
                }
                foreach ($article->AccountArticle as $contactArticle) {
                    if ($contactArticle->account_id == $ident['account_id']) {
                        $success = true;
                    }
                }
                if ($article->contact_id == $ident['id'] || $this->highRole->level >= 40) {
                    $success = true;
                }
                $temp = $this->getAccountArticle(array(), $ident['account_id']);
                if (in_array($article->id, $temp)) {
                    $success = true;
                }

                if ($success == false) {
                    $this->_forward('index', 'index', 'default');
                }
            }
        }
        $articlesObjs = Zend_Registry::get('articles');

        $articleObj = new $articlesObjs[$article->typ]();
        unset($articlesObjs);

        $this->view->articleObj = $articleObj;

        if (!$this->_request->isPost()) {
            if (Zend_Auth::getInstance()->hasIdentity() == true) {
                $idend = Zend_Auth::getInstance()->getIdentity();
                $this->getRequest()->setParams($idend);
            }
        }

        $this->view->special = false;
        if ($this->getRequest()->getParam('special')) {
            $this->view->special = true;
        }

        $this->view->files = $sessart->getFiles();

        $this->view->load = 0;
        if ($this->_getParam('load') == 1) {
            $this->view->load = 1;
            $vars = $sessart->getOptions();
            if ($this->getRequest()->getParam('layouter', false)) {
                $vars['layouter'] = $this->getRequest()->getParam('layouter', "");
            }
            $this->getRequest()->setParams($vars);
            $this->view->form->setAction($this->view->url(array('article' => $article->url, 'layouter' => $this->view->layouterPreviewId), 'article'));
        }
        if ($this->_getParam('rebuy') == 1) {
            $vars = $sessart->getOptions();
            if ($this->getRequest()->getParam('layouter', false)) {
                $vars['layouter'] = $this->getRequest()->getParam('layouter', "");
            }
            $this->getRequest()->setParams($vars);
            $this->view->form->setAction($this->view->url(array('article' => $article->url, 'layouter' => $this->view->layouterPreviewId), 'article'));
        }
        if ($this->_getParam('reload') == '1') {
            $kalk_artikel = $this->getRequest()->getParam('kalk_artikel');
            $_POST = array();
            //$this->view->load = 1;
            $this->getRequest()->clearParams();
            $this->getRequest()->setParams(array('reload' => '1', 'kalk_artikel' => $kalk_artikel));
            $this->getRequest()->setPost(array('reload' => '1', 'kalk_artikel' => $kalk_artikel));
        }

        if ($article->params != "" && !$this->_request->isPost() && !$this->getRequest()->getParam('layouter', false)) {
            $formDataT = json_decode($article->params, true);
            if (!empty($formDataT)) {
                if ($this->view->form) {
                    $this->getRequest()->setParams($formDataT);
                    $this->getRequest()->setPost($formDataT);
                }
            }
        }

        $this->view->mwert = $article->mwert;
        $this->view->netto = $articleObj->createFrontend($this, $this->translate, $article);

        if (Zend_Auth::getInstance()->hasIdentity()) {
            $this->view->netto = $this->view->netto * $this->user->getPriceFactor() * $this->shop->getPriceFactor() * $this->user->Account->getPriceFactor();
        } else {
            $this->view->netto = $this->view->netto * $this->shop->getPriceFactor();
        }
        $this->view->netto_raw = $this->view->netto;
        $this->view->steuer = round(($this->view->netto / 100 * $article->mwert), 2);
        $this->view->brutto = $this->view->netto + round(($this->view->netto / 100 * $article->mwert), 2);

        if (file_exists($this->_configPath . '/calc.ini')) {

            $configCalc = new Zend_Config_Ini($this->_configPath . '/calc.ini', 'calc');

            $this->view->form->setElementDecorators($configCalc->calc->elementDecorators->toArray());
            $this->view->form->setDecorators($configCalc->calc->decorators->toArray());
        }

        $this->view->form->addElement('hidden', 'ajax_calc_id', array('value' => $article->url));
        if ($this->getRequest()->getParam('layouter') != "") {
            $this->view->form->addElement('hidden', 'layouter', array('value' => $this->getRequest()->getParam('layouter')));
            $this->view->form->layouter->removeDecorator('label')->removeDecorator('HtmlTag');
        }

        $this->view->form->ajax_calc_id->removeDecorator('label')->removeDecorator('HtmlTag');
        $this->view->load = 0;
        if ($this->_getParam('loaded') == 1 || $this->_getParam('load') == 1) {
            $this->view->load = 1;
            $this->view->form->addElement('hidden', 'loaded', array('value' => '1'));
            $this->getRequest()->setParam('loaded', 1);
            $this->getRequest()->setPost('loaded', 1);
        }

        if ($this->_request->isPost() || $this->_getParam('load') == 1) {
            if ($this->_getParam('load') == 1) {
                $formDataT = $this->_request->getParams();
            } else {
                $formDataT = $this->_request->getPost();
            }
            $formData = array();
            foreach ($formDataT as $key => $var) {
                if ($var != '') {
                    $formData[$key] = $var;
                }
            }

            $this->view->weight = $sessart->getWeight();
            $this->view->weight_single = $sessart->getWeightSingle();
            $formData['ajax_calc_id'] = $article->url;
            if ($this->view->form->isValid($formData)) {
                $sessart->setArticleId($article->uuid);
                $sessart->setArticleType($article->typ);
                $sessart->setArticle($article->toArray());
                $sessart->setOptions($formData);

                $sessart->setNetto($this->view->netto);
                $sessart->setSteuer($this->view->steuer);
                $sessart->setBrutto($this->view->brutto);

                if ($this->getRequest()->getParam('special')) {
                    $sessart->setSpecial(true);
                }

                $basket->setTempProductClass($sessart);
                $sessart = $basket->getTempProduct($article->uuid);
            } else {
                if ($this->view->form) {
                    $this->view->form->populate($formData);
                }
            }
        } else {

            if ($this->getRequest()->getParam('layouter') != "") {
                $sessart->setRef($this->view->layouterSession->getRef());
                $sessart->setKst($this->view->layouterSession->getKst());
                $sessart->setTemplatePrintContactId($this->view->layouterSession->getTemplatePrintContactId());
            } else {
                $sessart->setLayouterId("");
                $sessart->setTemplatePrintId(false);
            }

            $sessart->setArticleId($article->uuid);
            $sessart->setArticleType($article->typ);
            $sessart->setArticle($article->toArray());
            $sessart->setNetto($this->view->netto);
            if ($article['typ'] == 2) {
                $options = $sessart->getOptions();
                if (isset($options['auflage'])) {
                    $this->_request->setParam('auflage', $options['auflage']);
                }
            }
            $sessart->setOptions($this->_request->getParams());
            $sessart->setSteuer($this->view->steuer);
            $sessart->setBrutto($this->view->brutto);
            $basket->setTempProductClass($sessart);
            $this->view->weight = $sessart->getWeight();
            $this->view->weight_single = $sessart->getWeightSingle();
        }
        $this->view->articleTemplate = $this->view->render($article['typ'] . '.phtml');

        $this->_helper->layout->setLayout('default');

        if ($this->_getParam('send_anfrage')) {

            $basket = new TP_Basket();

            $sessart = $basket->getTempProduct($article->uuid);
            $sessart->setCount(intval($this->_getParam('count')));

            $articlesObjs = Zend_Registry::get('articles');
            $articleObj = new $articlesObjs[$article->typ]();
            unset($articlesObjs);

            if (method_exists($articleObj, 'buyPreDispatch')) {
                $articleObj->buyPreDispatch($article);
            }

            if ($sessart->getArticleId() == "") {

                if ($sessart->getArticle() == NULL) {
                    $sessart->setArticleId($article->uuid);
                    $sessart->setArticle($article->toArray());
                }
                $basket->setTempProductClass($sessart);
            }

            $basket->addProduct(false, $article->uuid);

            if (Zend_Auth::getInstance()->hasIdentity()) {
                $this->_redirect('/basket/anfrage');
            } else {
                $this->_redirect('/user/index?mode=anfrage');
            }
        }

        if ($this->getRequest()->isPost() && $this->getRequest()->getParam('anfrage', false) && $this->getRequest()->getParam('page', false)) {
            TP_Queue::process(
                'overviewpost',
                'global',
                array_merge(array('page' => $this->view->articlegroup->id), $this->getRequest()->getParams())
            );

            $this->view->priorityMessenger('Nachricht versendet.');

            $files = new Zend_Session_Namespace('Basket');

            if (isset($files->files[$this->view->articlegroup->id])) {
                unset($files->files[$this->view->articlegroup->id]);
            }
        }
    }

    public function buymanyAction()
    {
        $basket = new TP_Basket();
        $articles = $this->_getParam('articleid');
        foreach ($articles as $key => $count) {
            if ($count != 0) {

                $article = Doctrine_Query::create()->from('Article c')->where('c.id = ?', array(intval($key)))->fetchOne();

                $sessart = $basket->getTempProduct($article->uuid);

                $sessart->setArticleId($article->uuid);
                $sessart->setArticleType($article->typ);
                $sessart->setArticle($article->toArray());
                $sessart->setNetto($article->preis);
                $sessart->setCount(intval($count));
                $sessart->setSteuer($article->preis / 100 * $article->mwert);
                $sessart->setBrutto(($article->preis / 100 * $article->mwert) + $article->preis);
                $basket->setTempProductClass($sessart);
                $basket->addProduct(false, $article->uuid);
            }
        }
        $this->_redirect('/basket/index');
    }

    public function buyAction()
    {
        $basket = new TP_Basket();
        if ($this->_getParam('uuid', false)) {
            $article = Doctrine_Query::create()->from('Article c')->where('c.uuid = ?', array($this->_getParam('uuid')))->fetchOne();
        } else {
            $article = Doctrine_Query::create()->from('Article c')->where('c.id = ?', array(intval($this->_getParam('id'))))->fetchOne();
        }

        $sessart = $basket->getTempProduct($article->uuid);

        if ($sessart->getNetto() == 0 && !$article->can_buy_with_no_price) {
            $this->view->priorityMessenger('Bitte überprüfen Sie den Preis', 'success');
            $this->_redirect('/article/show/uuid/' . $article->uuid);
        }

        if ($this->_getParam('delpod', false)) {
            $article->a3_special = false;
            $article->to_export = true;
            $article->a5_buy = false;
            $article->save();
            $this->_redirect('/article/myprodpod');
            return;
        }

        if ($this->_getParam('setpod', false)) {
            $params = $sessart->getOptions();
            $article->a3_special = true;
            $article->stock_count = $params['produktionsmenge'];
            $article->stock_count_min = $params['minBestand'];
            $article->a2_count = $params['seitenanzahl'];
            //$article->a4_auflagen = $params['produktionsmenge'];
            $article->to_export = true;
            $article->a4_abpreis = $sessart->getNetto();
            $article->a5_buy = true;
            $xml = simplexml_load_string(str_replace('\\', '', $article->a1_xml));

            foreach ($xml->artikel[0]->option as $option) {
                if ($option['id'] == 'produktionsmenge') {
                    $option['default'] = $params['produktionsmenge'];
                }
                if ($option['id'] == 'minBestand') {
                    $option['default'] = $params['minBestand'];
                }
            }
            $article->a1_xml = $xml->asXML();
            $article->params = json_encode($params);
            $article->save();
            $this->_redirect('/article/myprodpod');
            return;
        }

        $sessart->setCount(intval($this->_getParam('count')));

        $articlesObjs = Zend_Registry::get('articles');
        $articleObj = new $articlesObjs[$article->typ]();
        unset($articlesObjs);

        if (method_exists($articleObj, 'buyPreDispatch')) {
            $articleObj->buyPreDispatch($sessart, $article);
        }

        if ($sessart->getArticleId() == "" || $sessart->getArticleId() != $article->uuid) {

            if ($sessart->getArticle() == NULL || $sessart->getArticleId() != $article->uuid) {
                $sessart->setArticleId($article->uuid);
                $sessart->setArticle($article->toArray());
            }
            $basket->setTempProductClass($sessart);
        }

        if ($basket->getAllCount() == 0) {
            if (!Zend_Auth::getInstance()->hasIdentity()) {
                $basket->setShippingtype(0);
            }
        }

        if ($this->_getParam('collecting_orders_data', false)) {

            $id = $this->_getParam('collecting_orders_data', false);

            $user = Doctrine_Query::create()
                ->from('Contact m')
                ->where('uuid = ?')->fetchOne(array($id));

            $sessart = $basket->getTempProduct($article->uuid);
            $sessart->setRef($user->self_firstname . ' ' . $user->self_lastname);
            $sessart->setTemplatePrintId(md5(TP_Util::uuid()));

            $basket->setTempProductClass($sessart);

            $newTempId = $basket->addProduct(true, $article->uuid);
            $this->_helper->json->sendJson(array('success' => true, 'genUrl' => 'http://tp:8080/w2p/batch?w2pinframeredirect=true&w2pproductid=' . $article->uuid . '&w2puserid=' . $id . '&ARTID=' . TP_Crypt::encryptDocker(array('title' => $user->self_firstname . ' ' . $user->self_lastname, 'userid' => $id, 'basketposid' => $newTempId, 'UUID' => $article->uuid, 'ARTID' => Zend_Session::getId(), 'COPY' => 2, 'load' => 4, 'SERVER' => $this->view->basepath, 'LAYOUTERID' => TP_Util::uuid())) . '&time=' . time()));
            return;
        }
        if ($this->_getParam('clearBasket', false)) {
            $basket->clearBasketItems();
        }
        if ($this->_getParam('mode', false) && $this->_getParam('mode', false) == "offer") {
            $basket->clearBasketItemsOf($article->uuid);
            $basket->addProduct(true, $article->uuid);
            $this->_redirect('/basket/index?mode=offer');
        }

        if ($this->_getParam('loaded') == 1 || $this->_getParam('load') == 1) {
            $basket->updateProduct($article->uuid);
        } else {
            $basket->addProduct(false, $article->uuid);
        }

        $this->view->priorityMessenger('Produkt dem Warenkorb zugefügt', 'success');
        if (file_exists($this->_templatePath . '/basket/simple.phtml')) {
            $this->_redirect('/basket/simple');
            return;
        }


        $this->_redirect('/basket/index');
    }

    protected function curl_post_async($url, $params)
    {
        foreach ($params as $key => &$val) {
            if (is_array($val)) $val = implode(',', $val);
            $post_params[] = $key . '=' . urlencode($val);
        }
        $post_string = implode('&', $post_params);

        $parts = parse_url($url);

        $fp = fsockopen(
            $parts['host'],
            isset($parts['port']) ? $parts['port'] : 80,
            $errno,
            $errstr,
            30
        );

        $out = "POST " . $parts['path'] . " HTTP/1.1\r\n";
        $out .= "Host: " . $parts['host'] . "\r\n";
        $out .= "Content-Type: application/x-www-form-urlencoded\r\n";
        $out .= "Content-Length: " . strlen($post_string) . "\r\n";
        $out .= "Connection: Close\r\n\r\n";
        if (isset($post_string)) $out .= $post_string;

        fwrite($fp, $out);
        fclose($fp);
    }

    public function loadAction()
    {

        if ($this->_getParam('key') != "") {

            $basket = new TP_Basket();
            $article = $basket->getBasketArtikel($this->_getParam('key'));
            $article->setInLoad($this->_getParam('key'));
            $basket->setTempProductClass($article);
            $art = $article->getArticle();
            $this->_redirect($this->view->url(array('article' => $art['url'], 'layouter' => $article->getLayouterId()), 'article') . '?load=1');
        }
    }
}
