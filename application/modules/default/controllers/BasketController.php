<?php

class BasketController extends TP_Controller_Action
{
    public function init()
    {
        parent::init();

        $contextSwitch = $this->_helper->contextSwitch();
        $contextSwitch->addActionContext('saveinfo', 'json')
            ->addActionContext('getdeliverydata', 'json')
            ->addActionContext('savedeliverydata', 'json')
            ->setAutoJsonSerialization(true)->initContext();
    }

    public function getdeliverydataAction()
    {
        $this->view->clearVars();
        $basket = new TP_Basket();

        $position = $basket->getAllArtikel();

        $pos = $position->getFirstItem();

        $this->view->rows = $pos->getDeliverys();
        $this->view->sumAmount = $pos->getAuflage();

        $this->view->success = true;
    }

    public function savedeliverydataAction()
    {
        $this->view->clearVars();
        $basket = new TP_Basket();

        $position = $basket->getAllArtikel();

        $pos = $position->getFirstItem();

        $pos->setDeliverys($this->_getParam('item'));
        $this->view->rows = $this->_getParam('item');
        $this->view->success = true;
    }


    public function saveinfoAction()
    {
        $this->view->clearVars();
        $basket = new TP_Basket();

        $basket->setInfo($this->getRequest()->getParam('data', array()));
        $this->view->data = $this->getRequest()->getParam('data', array());

        $this->view->success = true;
    }

    public function rebuyAction()
    {

        if (!Zend_Auth::getInstance()->hasIdentity()) {
            $this->forwardSpeak('login', 'user', 'default', array(
                'mode' => 'basket'
            ));
            return;
        }

        if ($this->_getParam("pos_uuid", false)) {
            $orderspos = Doctrine_Query::create()->from('Orderspos c')->where('c.uuid = ?', array(
                $this->_getParam("pos_uuid", false)
            ))->fetchOne();

            $basket = new TP_Basket();
            $data = unserialize($orderspos->data);

            $basket->setTempProductClass($data);



            if ($orderspos->layouter_mode == 4 && $data->getLayouterId() != "") {


                $dataCopy = json_decode(file_get_contents($this->view->basepath . "/apps/component/steplayouter/store/copy/" . $data->getLayouterId()), true);
                $articleSess = new LayouterSession();
                $articleSess->contact_id = $orderspos->Orders->contact_id;
                $articleSess->setOrgArticleId($orderspos->Article->uuid);
                $articleSess->setArticleId($dataCopy['uuid']);
                $articleSess->title = $orderspos->Article->title;
                $articleSess->setLayouterModus(4);
                $articleSess->setIsEdit();
                $articleSess->save();
                $data->setLayouterId($articleSess->getArticleId());
                $basket->setTempProductClass($data);
                $basket->addProduct(false, $orderspos->Article->uuid);
                $this->redirectSpeak("/basket");
            }

            if ($orderspos->layouter_mode == 3 && $data->getLayouterId() != "") {
                $articleSession = new TP_Layoutersession();
                $articleSess = $articleSession->getLayouterArticle($orderspos->Article->uuid);
                $articleSess->setDesignerXML($orderspos->a9_designer_data);
                $articleSess->setLayouterModus(2);
                $articleSess->setIsEdit();
                $data->setLayouterId($articleSess->getArticleId());
                $basket->setTempProductClass($data);
            }

            if ($orderspos->layouter_mode == 2 && $data->getTemplatePrintId() != "") {
                $data->setTemplatePrintId(TP_Util::uuid());
                $articleSession = new TP_Layoutersession();
                $articleSess = $articleSession->getLayouterArticle($orderspos->Article->uuid);
                $articleSess->setLayouterModus(3);
                $articleSess->copyFromBasket($orderspos->Orders, $orderspos, $data->getTemplatePrintId());
                $articleSess->setTemplatePrintId($data->getTemplatePrintId());
                $data->setLayouterId($articleSess->getArticleId());
                $basket->setTempProductClass($data);
            }


            $basket->addProduct(false, $data->getArticleId());

            if ($orderspos->shop_id == 212 || $orderspos->shop_id == 257 || $orderspos->shop_id == 261) {
                $this->redirectSpeak("/basket/index");
                return;
            }
        } elseif ($this->_getParam("uuid", false)) {

            $order = Doctrine_Query::create()->from('Orders c')->where('c.uuid = ?', array(
                $this->_getParam("uuid", false)
            ))->fetchOne();

            $orders = Doctrine_Query::create()->from('Orderspos c')->leftJoin("c.Orders a")->where('a.uuid = ?', array(
                $this->_getParam("uuid", false)
            ))->execute();

            if ($order->shop_id == 212) {

                $contactaddress = Doctrine_Query::create()->from('ContactAddress c')->where('c.id = ?', array(
                    $order->delivery_address
                ))->fetchOne();

                $this->user->self_anrede = $contactaddress->anrede;
                $this->user->self_department = $contactaddress->company;
                $this->user->self_country = $contactaddress->country;
                $this->user->liefer_firstname = $contactaddress->firstname;
                $this->user->liefer_lastname = $contactaddress->lastname;
                $this->user->self_zip = $contactaddress->zip;
                $this->user->self_city = $contactaddress->city;
                $this->user->self_house_number = $contactaddress->house_number;
                $this->user->self_street = $contactaddress->street;
                $this->user->email = $contactaddress->email;
                $this->user->self_phone = $contactaddress->phone;
                $this->user->self_position = $contactaddress->position;
                $this->user->street = $contactaddress->zusatz1;
                $this->user->phone_alternative = $contactaddress->zusatz2;
                $this->user->ustid = $contactaddress->ustid;

                $this->user->save();
            }

            $basket = new TP_Basket();

            foreach ($orders as $orderspos) {
                $data = unserialize($orderspos->data);

                $basket->setTempProductClass($data);

                if ($orderspos->layouter_mode == 3 && $data->getLayouterId() != "") {
                    $articleSession = new TP_Layoutersession();
                    $articleSess = $articleSession->getLayouterArticle($orderspos->Article->uuid);
                    $articleSess->setDesignerXML($orderspos->a9_designer_data);
                    $articleSess->setLayouterModus(2);
                    $articleSess->setIsEdit();
                    $data->setLayouterId($articleSess->getArticleId());
                    $basket->setTempProductClass($data);
                }

                if ($orderspos->layouter_mode == 2 && $data->getTemplatePrintId() != "") {
                    $data->setTemplatePrintId(TP_Util::uuid());
                    $articleSession = new TP_Layoutersession();
                    $articleSess = $articleSession->getLayouterArticle($orderspos->Article->uuid);
                    $articleSess->setLayouterModus(3);
                    $articleSess->copyFromBasket($orderspos->Orders, $orderspos, $data->getTemplatePrintId());
                    $articleSess->setTemplatePrintId($data->getTemplatePrintId());
                    $data->setLayouterId($articleSess->getArticleId());
                    $basket->setTempProductClass($data);
                }

                $basket->addProduct(false, $orderspos->Article->uuid);
            }


            if ($order->shop_id == 212 || $order->shop_id == 257 || $order->shop_id == 261) {
                $this->redirectSpeak("/basket/index");
                return;
            }
        }

        $this->redirectSpeak("/basket/simple");
    }



    public function simpleAction()
    {
        $this->view->inworkCount = 0;
        $articleSession = new TP_Layoutersession();
        $this->view->inworkCount = count($articleSession->getAllLayouterArticle());

        $basket = new TP_Basket();
        if ($this->_request->getParam('del') != '') {
            $basket->removeProduct($this->_request->getParam('del'));
        }

        if ($this->_getParam('plz', false)) {
            $basket->setPLZ($this->_getParam('plz', false));
        }

        $temp = array();
        $netto = 0;
        $versand = 0;
        $versandpos = 0;
        $mwert = 0;
        $mwertalle = array();
        $summewarenwert = array();
        $this->view->paymenttypecontact = array();
        $this->view->errorGutscheincode = false;
        $articles = $basket->getAllArtikel();

        if ($this->_getParam('gutscheincode', false)) {
            $basket->setGutschein($this->_getParam('gutscheincode', false));
        }

        if ($this->_getParam('basketfield1')) {
            $basket->setBasketField1($this->_getParam('basketfield1'));
        }
        if ($this->_getParam('basketfield2')) {
            $basket->setBasketField2($this->_getParam('basketfield2'));
        }
        $this->view->basketfield1 = $basket->getBasketField1();
        $this->view->basketfield2 = $basket->getBasketField2();
        $this->view->plz = $basket->getPLZ();
        $this->view->zip = $basket->getPLZ();
        if (count($articles) == 0) {
            $this->view->basketIsEmpty = true;
        } else {
            $this->view->basketIsEmpty = false;
        }
        $gewicht = 0;
        $gewichtpos = 0;
        foreach ($articles as $uuid => $art) {
            $article = Doctrine_Query::create()->from('Article c')->where('c.uuid = ?', array(
                $art->getArticleId()
            ))->fetchOne();

            if ($article->typ != '9') {
                $field = 'a1_xml';
                $options = array();

                if ($article->a6_org_article != 0) {
                    $options = $article->OrgArticle->getOptArray($art, false);
                } else {
                    $options = $article->getOptArray($art, false);
                }
                $gewicht += $art->getWeight();
                $gewichtpos = $art->getWeight();
                if (($this->getRequest()->isPost() && $this->_request->getParam('ref_' . $uuid) != '')) {
                    $art->setRef($this->_request->getParam('ref_' . $uuid));
                    $this->view->priorityMessenger('Erfolgreich gesetzt', 'success');
                }
                if (($this->getRequest()->isPost() && $this->_request->getParam('kst_' . $uuid) != '')) {
                    $art->setKst($this->_request->getParam('kst_' . $uuid));
                    $this->view->priorityMessenger('Erfolgreich gesetzt', 'success');
                }
                if (($this->getRequest()->isPost() && $this->_request->getParam('count_' . $uuid) != '')) {
                    $art->setCount($this->_request->getParam('count_' . $uuid));
                }
                /**
                Versandart Berechnung
                 **/
                $shippingtype_pos = array();
                if ($this->shop->shipping_mode >= 2) {
                    if (($this->getRequest()->isPost() && $this->_request->getParam('shippingtype_' . $uuid) != '') || $art->getShippingtype() != 0) {

                        if ($this->getRequest()->isPost() && $this->_request->getParam('shippingtype_' . $uuid) != '') {
                            $shippingtype = $this->_request->getParam('shippingtype_' . $uuid);
                        } else {
                            $shippingtype = $art->getShippingtype();
                        }

                        $shippingtype_pos = Doctrine_Query::create()->from('Shippingtype c')->where('c.id = ?', array(
                            $shippingtype
                        ))->fetchOne();

                        $art->setShippingType($shippingtype_pos['id']);
                        $art->setShippingPrice($shippingtype_pos->calcVersand(($art->getnetto() * $art->getCount()), $gewichtpos, 'de', $this->user));
                        $art->setShippingPriceMWert($art->getShippingPrice() / 100 * str_replace('%', '', $shippingtype_pos['mwert']));
                        $versand = $art->getShippingPrice() + $versand;
                        $mwert = $mwert + $art->getShippingPriceMWert();
                        $mwertalle[str_replace('%', '', $shippingtype_pos['mwert'])] = $mwertalle[str_replace('%', '', $shippingtype_pos['mwert'])] + ($art->getShippingPrice() / 100 * str_replace('%', '', $shippingtype_pos['mwert']));
                    } else {
                        if ($this->shop->id == $this->shop->Install->defaultmarket) {
                            $shippingtype_pos = Doctrine_Query::create()->from('Shippingtype c')->where('c.shop_id = ? AND ( (c.weight_from = 0 AND c.weight_to = 0) OR (c.weight_from <= ? AND c.weight_to >= ?) )', array(
                                $this->shop->id, $art->getWeight(), $art->getWeight()
                            ))->orderBy('c.pos ASC')->fetchOne();
                        } else {
                            $shippingtype_pos = Doctrine_Query::create()->from('Shippingtype c')->where('c.shop_id = ? AND ( (c.weight_from = 0 AND c.weight_to = 0) OR (c.weight_from <= ? AND c.weight_to >= ?) )', array(
                                $this->shop->Install->defaultmarket, $art->getWeight(), $art->getWeight()
                            ))->orderBy('c.pos ASC')->fetchOne();
                        }

                        $art->setShippingType($shippingtype_pos['id']);
                        $art->setShippingPrice($shippingtype_pos->calcVersand(($art->getnetto() * $art->getCount()), $gewichtpos, 'de', $this->user));
                        $versand = $art->getShippingPrice() + $versand;
                        $mwert = $mwert + ($art->getShippingPrice() / 100 * str_replace('%', '', $shippingtype_pos['mwert']));
                        $mwertalle[str_replace('%', '', $shippingtype_pos['mwert'])] = $mwertalle[str_replace('%', '', $shippingtype_pos['mwert'])] + ($art->getShippingPrice() / 100 * str_replace('%', '', $shippingtype_pos['mwert']));
                    }
                }

                /**
                Ende
                 **/

                array_push($temp, array(
                    'uuid' => $uuid,
                    'article' => $article,
                    'basketarticle' => $art,
                    'shippingtype' => $shippingtype_pos,
                    'options' => $options
                ));

                if ($article['versand'] == 'Fix') {
                    $versandpos = $versandpos + $article['versandwert'];
                } elseif ($article['versand'] == 'Position') {
                }
                $art->setHasGutschein(false);
                if ($this->_getParam('gutscheincode') != "" || $basket->getGutschein() != "") {
                    $articleGroups = array_keys($article->ArticleGroupArticle->toKeyValueArray('articlegroup_id', 'article_id'));
                    Zend_Registry::get('log')->debug(print_r($articleGroups, true));

                    $articlegroup = "";
                    if (count($articleGroups)) {
                        unset($articleGroups[0]);
                        if (trim($articleGroups) != "") {
                            $articlegroup = 'OR a.articlegroup_id IN (' . implode(',', $articleGroups) . ')';
                        }
                    }
                    $gutscheincode = Doctrine_Query::create()->from('CreditSystemDetail c')->leftJoin('c.CreditSystem a')->andWhere('(a.product_id IN (' . $article->id . ',' . $article->a6_org_article . ') ' . $articlegroup . ') AND a.install_id = ? AND a.product_id != 0 AND c.uuid = ? AND a.f <= ? AND a.t >= ?', array(
                        $this->install->id,
                        ($basket->getGutschein() != "" && !$this->_getParam('gutscheincode', false)) ? $basket->getGutschein() : $this->_getParam('gutscheincode'), date('Y-m-d'), date('Y-m-d')
                    ))->fetchOne();
                    if ($gutscheincode && $gutscheincode->used == true && $gutscheincode->CreditSystem->more == false) {
                        $this->view->errorGutscheincode = true;
                        $basket->setGutschein("");
                        $this->view->priorityMessenger('Gutschein nicht mehr gültig', 'error');
                    } elseif ($gutscheincode && $gutscheincode->CreditSystem->more == true) {
                        $gutscheincodee = Doctrine_Query::create()->from('CreditSystemDetail c')->leftJoin('c.CreditSystem a')->addWhere('(a.product_id IN (' . $article->id . ',' . $article->a6_org_article . ') ' . $articlegroup . ') AND a.install_id = ? AND a.product_id != 0 AND c.contact_id = ? AND c.uuid = ? AND a.f <= ? AND a.t >= ?', array(
                            $this->install->id,
                            $this->user->id,
                            $basket->getGutschein(), date('Y-m-d'), date('Y-m-d')
                        ))->fetchOne();

                        if ($gutscheincodee) {
                            $this->view->errorGutscheincode = true;
                            $basket->setGutschein("");
                            $this->view->priorityMessenger('Gutschein nicht mehr gültig', 'error');
                        } else {
                            $this->view->errorGutscheincode = false;
                        }
                    } else {
                        $this->view->errorGutscheincode = false;
                    }

                    if ($gutscheincode && $this->view->errorGutscheincode == false) {

                        $this->view->errorGutscheincode = false;
                        $basket->setGutschein($gutscheincode->uuid);
                        if ($gutscheincode->CreditSystem->percent == true) {
                            $abzug = (($art->getnetto() * $art->getCount()) + (($art->getNetto() / 100 * $prozent) * $art->getCount())) / 100 * $gutscheincode->CreditSystem->wert;
                            $basket->setGutscheinAbzug($abzug);
                        } else {
                            $basket->setGutscheinAbzug($gutscheincode->CreditSystem->wert);
                        }
                        if ($basket->getPreisBrutto() < 0) {
                            $basket->setPreisBrutto(0);
                        }
                        $this->view->gutscheincode = $gutscheincode->uuid;
                        $this->view->gutscheincodeabzug = $basket->getGutscheinAbzug() * 1;

                        $basket->setGutscheinAbzugTyp(TP_Basket::GUTSCHEIN_PRODUCT);

                        $netto = $netto + ($art->getnetto() * $art->getCount()) + $art->getShippingPrice();
                        $prozent = str_replace('%', '', $article->mwert);
                        $mwert = $mwert + (($art->getNetto() / 100 * $prozent) * $art->getCount());
                        $mwertalle[$prozent] = $mwertalle[$prozent] + (($art->getNetto() / 100 * $prozent) * $art->getCount());
                        $art->setHasGutschein(true);

                        $summewarenwert[$prozent] = $summewarenwert[$prozent] + ($art->getnetto() * $art->getCount()) + (($art->getNetto() / 100 * $prozent) * $art->getCount());
                    } else {
                        $netto = $netto + ($art->getnetto() * $art->getCount()) + $art->getShippingPrice();
                        $prozent = str_replace('%', '', $article->mwert);
                        $mwert = $mwert + (($art->getNetto() / 100 * $prozent) * $art->getCount());
                        $mwertalle[$prozent] = $mwertalle[$prozent] + (($art->getNetto() / 100 * $prozent) * $art->getCount());
                        $summewarenwert[$prozent] = $summewarenwert[$prozent] + ($art->getnetto() * $art->getCount()) + (($art->getNetto() / 100 * $prozent) * $art->getCount());
                    }
                } else {
                    $netto = $netto + ($art->getnetto() * $art->getCount()) + $art->getShippingPrice();
                    $prozent = str_replace('%', '', $article->mwert);
                    $mwert = $mwert + (($art->getNetto() / 100 * $prozent) * $art->getCount());
                    if (!isset($mwertalle[$prozent])) {
                        $mwertalle[$prozent] = 0;
                    }
                    if (!isset($summewarenwert[$prozent])) {
                        $summewarenwert[$prozent] = 0;
                    }
                    $mwertalle[$prozent] = $mwertalle[$prozent] + (($art->getNetto() / 100 * $prozent) * $art->getCount());
                    $summewarenwert[$prozent] = $summewarenwert[$prozent] + ($art->getnetto() * $art->getCount()) + (($art->getNetto() / 100 * $prozent) * $art->getCount());
                }
            } else {
                array_push($temp, array(
                    'uuid' => $uuid,
                    'article' => $article,
                    'basketarticle' => $art
                ));

                if ($article['versand'] == 'Fix') {
                    $versand = $versand + $article['versandwert'];
                } elseif ($article['versand'] == 'Position') {
                }
                $art->setHasGutschein(false);

                if ($this->_getParam('gutscheincode') != "" || $basket->getGutschein() != "") {
                    $articleGroups = array_keys($article->ArticleGroupArticle->toKeyValueArray('articlegroup_id', 'article_id'));
                    $articlegroup = "";
                    if (count($articleGroups)) {
                        if (trim($articleGroups) != "") {
                            $articlegroup = 'OR a.articlegroup_id IN (' . implode(',', $articleGroups) . ')';
                        }
                    }
                    $gutscheincode = Doctrine_Query::create()->from('CreditSystemDetail c')->leftJoin('c.CreditSystem a')->addWhere('(a.product_id IN (' . $article->id . ',' . $article->a6_org_article . ') ' . $articlegroup . ') AND a.install_id = ? AND a.product_id != 0 AND c.uuid = ? AND a.f <= ? AND a.t >= ?', array(
                        $this->install->id,
                        ($basket->getGutschein() != "" && !$this->_getParam('gutscheincode', false)) ? $basket->getGutschein() : $this->_getParam('gutscheincode'), date('Y-m-d'), date('Y-m-d')
                    ))->fetchOne();

                    if ($gutscheincode && $gutscheincode->used == true && $gutscheincode->CreditSystem->more == false) {
                        $this->view->errorGutscheincode = true;
                        $basket->setGutschein();
                        $this->view->priorityMessenger('Gutschein nicht mehr gültig', 'error');
                    } elseif ($gutscheincode && $gutscheincode->CreditSystem->more == true) {
                        $gutscheincodee = Doctrine_Query::create()->from('CreditSystemDetail c')->leftJoin('c.CreditSystem a')->addWhere('(a.product_id IN (' . $article->id . ',' . $article->a6_org_article . ') ' . $articlegroup . ') AND a.install_id = ? AND a.product_id != 0 AND c.contact_id = ? AND c.uuid = ? AND a.f <= ? AND a.t >= ?', array(
                            $this->install->id,
                            $this->user->id,
                            $basket->getGutschein(), date('Y-m-d'), date('Y-m-d')
                        ))->fetchOne();

                        if ($gutscheincodee) {
                            $this->view->errorGutscheincode = true;
                            $basket->setGutschein("");
                            $this->view->priorityMessenger('Gutschein nicht mehr gültig', 'error');
                        } else {
                            $this->view->errorGutscheincode = false;
                        }
                    } else {
                        $this->view->errorGutscheincode = false;
                    }

                    if ($gutscheincode && $this->view->errorGutscheincode == false) {

                        $this->view->errorGutscheincode = false;
                        $basket->setGutschein($gutscheincode->uuid);
                        if ($gutscheincode->CreditSystem->percent == true) {
                            $abzug = $art->getDesignerCost() / 100 * $gutscheincode->CreditSystem->wert;
                            $basket->setGutscheinAbzug($abzug);
                        } else {
                            $basket->setGutscheinAbzug($gutscheincode->CreditSystem->wert);
                        }
                        $netto = $netto + $art->getDesignerCost() - $basket->getGutscheinAbzug();
                        if ($basket->getPreisBrutto() < 0) {
                            $basket->setPreisBrutto(0);
                        }
                        $this->view->gutscheincode = $gutscheincode->uuid;
                        $this->view->gutscheincodeabzug = $basket->getGutscheinAbzug() * 1;

                        $basket->setGutscheinAbzugTyp(TP_Basket::GUTSCHEIN_PRODUCT);

                        $prozent = str_replace('%', '', $article->mwert);
                        $mwert = $mwert + (($art->getDesignerCost() - $art->getDesignerCost()) / 100 * $prozent);
                        $mwertalle[$prozent] = $mwertalle[$prozent] + (($art->getDesignerCost() - $art->getDesignerCost()) / 100 * $prozent);
                        if (!isset($mwertalle[$prozent])) {
                            $mwertalle[$prozent] = 0;
                        }
                        if (!isset($summewarenwert[$prozent])) {
                            $summewarenwert[$prozent] = 0;
                        }
                        $summewarenwert[$prozent] = $summewarenwert[$prozent] + $art->getDesignerCost() + ($art->getDesignerCost() / 100 * $prozent);
                        $art->setHasGutschein(true);
                    } else {
                        $netto = $netto + $art->getDesignerCost();
                        $prozent = str_replace('%', '', $article->mwert);
                        $mwert = $mwert + ($art->getDesignerCost() / 100 * $prozent);
                        $mwertalle[$prozent] = $mwertalle[$prozent] + ($art->getDesignerCost() / 100 * $prozent);
                        if (!isset($mwertalle[$prozent])) {
                            $mwertalle[$prozent] = 0;
                        }
                        if (!isset($summewarenwert[$prozent])) {
                            $summewarenwert[$prozent] = 0;
                        }
                        $summewarenwert[$prozent] = $summewarenwert[$prozent] + $art->getDesignerCost() + ($art->getDesignerCost() / 100 * $prozent);
                    }
                } else {
                    $netto = $netto + $art->getDesignerCost();
                    $prozent = str_replace('%', '', $article->mwert);
                    $mwert = $mwert + ($art->getDesignerCost() / 100 * $prozent);
                    $mwertalle[$prozent] = $mwertalle[$prozent] + ($art->getDesignerCost() / 100 * $prozent);
                    if (!isset($mwertalle[$prozent])) {
                        $mwertalle[$prozent] = 0;
                    }
                    if (!isset($summewarenwert[$prozent])) {
                        $summewarenwert[$prozent] = 0;
                    }
                    $summewarenwert[$prozent] = $summewarenwert[$prozent] + $art->getDesignerCost() + ($art->getDesignerCost() / 100 * $prozent);
                }
            }
        }

        $this->view->productmwert = $mwert;
        $this->view->productbrutto = $netto + $mwert;
        $this->view->productnetto = $netto;
        if ($this->_getParam('gutscheincode') != "" || $basket->getGutschein() != "") {

            $gutscheincode = Doctrine_Query::create()->from('CreditSystemDetail c')->leftJoin('c.CreditSystem a')->where('a.install_id = ? AND c.uuid = ? AND a.product_id = 0 AND a.articlegroup_id = 0 AND a.f <= ? AND a.t >= ?', array(
                $this->install->id,
                ($basket->getGutschein() != "") ? $basket->getGutschein() : $this->_getParam('gutscheincode'), date('Y-m-d'), date('Y-m-d')
            ))->fetchOne();

            if ($gutscheincode && $gutscheincode->used == true && $gutscheincode->CreditSystem->more == false) {
                $this->view->errorGutscheincode = true;
                $basket->setGutschein();
                $this->view->priorityMessenger('Gutschein nicht mehr gültig', 'error');
            } elseif ($gutscheincode && $gutscheincode->CreditSystem->more == true) {
                $gutscheincodet = Doctrine_Query::create()->from('CreditSystemDetail c')->where('c.install_id = ? AND c.contact_id = ? AND c.uuid = ?', array(
                    $this->install->id,
                    $this->user->id,
                    $basket->getGutschein()
                ))->fetchOne();

                if ($gutscheincodet) {
                    $this->view->errorGutscheincode = true;
                    $basket->setGutschein();
                    $this->view->priorityMessenger('Gutschein nicht mehr gültig', 'error');
                } else {
                    $this->view->errorGutscheincode = false;
                }
            } else {
                $this->view->errorGutscheincode = false;
            }

            if ($gutscheincode && $this->view->errorGutscheincode == false) {
                $basket->setGutschein($gutscheincode->uuid);
                if ($gutscheincode->CreditSystem->percent == true) {
                    $abzug = ($netto + $mwert) / 100 * $gutscheincode->CreditSystem->wert;
                    $basket->setGutscheinAbzug($abzug);
                    if ($netto + $mwert - $abzug < 0) {
                        $netto = 0;
                        $brutto = 0;
                        $mwert = 0;
                        $mwertalle = array();
                        $summewarenwert = array();
                    }
                } else {
                    $basket->setGutscheinAbzug($gutscheincode->CreditSystem->wert);
                    if ($netto + $mwert - $gutscheincode->CreditSystem->wert < 0) {
                        $gutscheincode->CreditSystem->wert = $netto + $mwert;
                        $basket->setGutscheinAbzug($netto + $mwert);
                        $mwertalle = array();
                        $summewarenwert = array();
                        $netto = 0;
                        $brutto = 0;
                        $mwert = 0;
                        $mwertalle = array();
                    }
                }

                $this->view->gutscheincode = $gutscheincode->uuid;
                $this->view->gutscheincodeabzug = $basket->getGutscheinAbzug() * 1;
                $basket->setGutscheinAbzugTyp(TP_Basket::GUTSCHEIN_ALL);
            }
        }
        $this->view->productbruttogutschein = 0;
        if ($netto > 0) {
            $this->view->productbruttogutschein = $netto + $mwert - ($basket->getGutscheinAbzug() * 1);
        }

        /**
         * Adressenverwaltung
         */

        if ($this->getRequest()->getParam('sender') != '' && $this->getRequest()->getParam('sender') != 's') {
            $basket->setSenderIsSame(false);
        } elseif ($this->getRequest()->getParam('sender') != '' && $this->getRequest()->getParam('sender') == 's') {
            $basket->setSenderIsSame(true);
        }

        if ($this->getRequest()->getParam('delivery') != '' && $this->getRequest()->getParam('delivery') == 's') {
            if ($this->getRequest()->getParam('invoice', false)) {
                $basket->setDelivery($this->getRequest()->getParam('invoice', false));
                $basket->setInvoice($this->getRequest()->getParam('invoice', false));
            } else {
                $basket->setDelivery($basket->getInvoice());
                $basket->setInvoice($basket->getInvoice());
            }

            $basket->setDeliveryIsSame(true);
            $basket->setSender(0);
            //$this->redirectSpeak('/basket/finish');
        } elseif (
            $this->getRequest()->getParam('delivery') != '' && $this->getRequest()->getParam('delivery') != 's' &&
            $this->getRequest()->getParam('sender') != '' && $this->getRequest()->getParam('invoice') != ''
        ) {

            $basket->setDeliveryIsSame(false);
            $basket->setDelivery($this->getRequest()->getParam('delivery'));
            $basket->setInvoice($this->getRequest()->getParam('invoice'));
            $basket->setSender($this->getRequest()->getParam('sender'));
            //$this->redirectSpeak('/basket/finish');

        } elseif ($this->getRequest()->getParam('delivery') != '' && $this->getRequest()->getParam('delivery') != 's') {
            $basket->setDeliveryIsSame(false);
            $basket->setDelivery($this->getRequest()->getParam('delivery'));
            //$this->redirectSpeak('/basket/finish');
        }


        if ($mode->over_ride_contact != false && $mode->over_ride_contact != 'new') {
            $this->view->addresses = Doctrine_Query::create()->from('ContactAddress a')->where('type = 2 AND contact_id = ? AND display = 1', array(
                $mode->over_ride_contact
            ))->execute();
            $this->view->addresses_invoice = Doctrine_Query::create()->from('ContactAddress a')->where('type = 1 AND contact_id = ? AND display = 1', array(
                $mode->over_ride_contact
            ))->execute();
            $this->view->addresses_sender = Doctrine_Query::create()->from('ContactAddress a')->where('type = 3 AND contact_id = ? AND display = 1', array(
                $mode->over_ride_contact
            ))->execute();
        } else {
            $this->view->addresses = Doctrine_Query::create()->from('ContactAddress a')->where('type = 2 AND contact_id = ? AND display = 1', array(
                $this->user->id
            ))->execute();
            $this->view->addresses_invoice = Doctrine_Query::create()->from('ContactAddress a')->where('type = 1 AND contact_id = ? AND display = 1', array(
                $this->user->id
            ))->execute();
            $this->view->addresses_sender = Doctrine_Query::create()->from('ContactAddress a')->where('type = 3 AND contact_id = ? AND display = 1', array(
                $this->user->id
            ))->execute();
        }

        $this->view->addresses_delivery = $this->view->addresses;

        if ($basket->getDelivery() == 0) {
            if ($this->user->standart_delivery == 0) {
                $cs = Doctrine_Query::create()->select()->from('ContactAddress s')->where('s.contact_id = ? AND type = 2 AND display = 1', array($this->user->id))->limit(1)->fetchOne();

                if ($cs) {
                    $this->user->standart_delivery = $cs->id;
                    $this->user->save();
                } else {
                    $contact = $this->user;
                    $contactaddress = new ContactAddress();
                    $contactaddress->anrede = $contact->self_anrede;
                    $contactaddress->company = $contact->self_department;
                    $contactaddress->firstname = $contact->self_firstname;
                    $contactaddress->lastname = $contact->self_lastname;
                    $contactaddress->zip = $contact->self_zip;
                    $contactaddress->city = $contact->self_city;
                    $contactaddress->house_number = $contact->self_house_number;
                    $contactaddress->street = $contact->self_street;
                    $contactaddress->email = $contact->self_email;
                    $contactaddress->phone = $contact->self_phone;

                    $contactaddress->contact_id = $contact->id;
                    $contactaddress->install_id = $contact->install_id;
                    $contactaddress->display = true;
                    $contactaddress->type = 2;

                    $contactaddress->save();

                    $this->user->standart_delivery = $cs->id;
                    $this->user->save();
                    $cs = $contactaddress;
                }
                $basket->setDelivery($cs->uuid);
            } else {
                $cs = Doctrine_Query::create()->select()->from('ContactAddress s')->where('s.contact_id = ? AND display = 1 AND s.id = ?', array($this->user->id, $this->user->standart_delivery))->fetchOne();

                if ($cs) {
                    $basket->setDelivery($cs->uuid);
                } else {
                    $cs = Doctrine_Query::create()->select()->from('ContactAddress s')->where('s.contact_id = ? AND display = 1 AND type = 2', array($this->user->id))->limit(1)->fetchOne();

                    if ($cs) {
                        $this->user->standart_delivery = $cs->id;
                        $this->user->save();
                    } else {
                        $contact = $this->user;
                        $contactaddress = new ContactAddress();
                        $contactaddress->anrede = $contact->self_anrede;
                        $contactaddress->company = $contact->self_department;
                        $contactaddress->firstname = $contact->self_firstname;
                        $contactaddress->lastname = $contact->self_lastname;
                        $contactaddress->zip = $contact->self_zip;
                        $contactaddress->city = $contact->self_city;
                        $contactaddress->house_number = $contact->self_house_number;
                        $contactaddress->street = $contact->self_street;
                        $contactaddress->email = $contact->self_email;
                        $contactaddress->phone = $contact->self_phone;

                        $contactaddress->contact_id = $contact->id;
                        $contactaddress->install_id = $contact->install_id;
                        $contactaddress->display = true;
                        $contactaddress->type = 2;

                        $contactaddress->save();

                        $this->user->standart_delivery = $cs->id;
                        $this->user->save();
                        $cs = $contactaddress;
                    }
                    $basket->setDelivery($cs->uuid);
                }
            }
        } else {
            $cs = Doctrine_Query::create()->select()->from('ContactAddress s')->where('s.contact_id = ? AND display = 1 AND s.uuid = ?', array($this->user->id, $basket->getDelivery()))->fetchOne();

            if ($cs) {
                $basket->setDelivery($cs->uuid);
            } else {
                $cs = Doctrine_Query::create()->select()->from('ContactAddress s')->where('s.contact_id = ? AND display = 1 AND type = 2', array($this->user->id))->limit(1)->fetchOne();

                if ($cs) {
                    $this->user->standart_delivery = $cs->id;
                    $this->user->save();
                } else {
                    $contact = $this->user;
                    $contactaddress = new ContactAddress();
                    $contactaddress->anrede = $contact->self_anrede;
                    $contactaddress->company = $contact->self_department;
                    $contactaddress->firstname = $contact->self_firstname;
                    $contactaddress->lastname = $contact->self_lastname;
                    $contactaddress->zip = $contact->self_zip;
                    $contactaddress->city = $contact->self_city;
                    $contactaddress->house_number = $contact->self_house_number;
                    $contactaddress->street = $contact->self_street;
                    $contactaddress->email = $contact->self_email;
                    $contactaddress->phone = $contact->self_phone;

                    $contactaddress->contact_id = $contact->id;
                    $contactaddress->install_id = $contact->install_id;
                    $contactaddress->display = true;
                    $contactaddress->type = 2;

                    $contactaddress->save();

                    $this->user->standart_delivery = $cs->id;
                    $this->user->save();
                    $cs = $contactaddress;
                }
                $basket->setDelivery($cs->uuid);
            }
        }

        if ($basket->getSender() == 0) {
            if ($this->user->standart_sender == 0) {
                $cs = Doctrine_Query::create()->select()->from('ContactAddress s')->where('s.contact_id = ? AND display = 1 AND type = 3', array($this->user->id))->fetchOne();
                if ($cs) {
                    $this->user->standart_sender = $cs->id;
                    $this->user->save();

                    $basket->setSender($cs->uuid);
                }
            } else {
                $cs = Doctrine_Query::create()->select()->from('ContactAddress s')->where('s.contact_id = ? AND display = 1 AND s.id = ?', array($this->user->id, $this->user->standart_sender))->fetchOne();
                $basket->setSender($cs->uuid);
            }
        }
        if ($basket->getInvoice() == 0) {
            if ($this->user->standart_invoice == 0) {

                $cs = Doctrine_Query::create()->select()->from('ContactAddress s')->where('s.contact_id = ? AND display = 1 AND type = 1', array($this->user->id))->fetchOne();
                if ($cs) {
                    $this->user->standart_invoice = $cs->id;
                    $this->user->save();
                } else {
                    $contact = $this->user;
                    $contactaddress = new ContactAddress();
                    $contactaddress->anrede = $contact->self_anrede;
                    $contactaddress->company = $contact->self_department;
                    $contactaddress->firstname = $contact->self_firstname;
                    $contactaddress->lastname = $contact->self_lastname;
                    $contactaddress->zip = $contact->self_zip;
                    $contactaddress->city = $contact->self_city;
                    $contactaddress->house_number = $contact->self_house_number;
                    $contactaddress->street = $contact->self_street;
                    $contactaddress->email = $contact->self_email;
                    $contactaddress->phone = $contact->self_phone;

                    $contactaddress->contact_id = $contact->id;
                    $contactaddress->install_id = $contact->install_id;
                    $contactaddress->display = true;
                    $contactaddress->type = 1;

                    $contactaddress->save();

                    $this->user->standart_delivery = $cs->id;
                    $this->user->save();
                    $cs = $contactaddress;
                }
                $basket->setInvoice($cs->uuid);
            } else {
                $cs = Doctrine_Query::create()->select()->from('ContactAddress s')->where('s.contact_id = ? AND display = 1 AND s.id = ?', array($this->user->id, $this->user->standart_invoice))->fetchOne();

                if ($cs) {
                    $basket->setInvoice($cs->uuid);
                } else {
                    $cs = Doctrine_Query::create()->select()->from('ContactAddress s')->where('s.contact_id = ? AND display = 1 AND type = 1', array($this->user->id))->fetchOne();
                    if ($cs) {
                        $this->user->standart_invoice = $cs->id;
                        $this->user->save();
                    } else {
                        $contact = $this->user;
                        $contactaddress = new ContactAddress();
                        $contactaddress->anrede = $contact->self_anrede;
                        $contactaddress->company = $contact->self_department;
                        $contactaddress->firstname = $contact->self_firstname;
                        $contactaddress->lastname = $contact->self_lastname;
                        $contactaddress->zip = $contact->self_zip;
                        $contactaddress->city = $contact->self_city;
                        $contactaddress->house_number = $contact->self_house_number;
                        $contactaddress->street = $contact->self_street;
                        $contactaddress->email = $contact->self_email;
                        $contactaddress->phone = $contact->self_phone;

                        $contactaddress->contact_id = $contact->id;
                        $contactaddress->install_id = $contact->install_id;
                        $contactaddress->display = true;
                        $contactaddress->type = 1;

                        $contactaddress->save();

                        $this->user->standart_delivery = $cs->id;
                        $this->user->save();
                        $cs = $contactaddress;
                    }
                    $basket->setInvoice($cs->uuid);
                }
            }
        } else {

            $cs = Doctrine_Query::create()->select()->from('ContactAddress s')->where('s.contact_id = ? AND display = 1 AND s.uuid = ?', array($this->user->id, $basket->getInvoice()))->fetchOne();

            if ($cs) {
                $basket->setInvoice($cs->uuid);
            } else {
                $cs = Doctrine_Query::create()->select()->from('ContactAddress s')->where('s.contact_id = ? AND display = 1 AND type = 1', array($this->user->id))->limit(1)->fetchOne();

                if ($cs) {
                    $this->user->standart_invoice = $cs->id;
                    $this->user->save();
                } else {
                    $contact = $this->user;
                    $contactaddress = new ContactAddress();
                    $contactaddress->anrede = $contact->self_anrede;
                    $contactaddress->company = $contact->self_department;
                    $contactaddress->firstname = $contact->self_firstname;
                    $contactaddress->lastname = $contact->self_lastname;
                    $contactaddress->zip = $contact->self_zip;
                    $contactaddress->city = $contact->self_city;
                    $contactaddress->house_number = $contact->self_house_number;
                    $contactaddress->street = $contact->self_street;
                    $contactaddress->email = $contact->self_email;
                    $contactaddress->phone = $contact->self_phone;

                    $contactaddress->contact_id = $contact->id;
                    $contactaddress->install_id = $contact->install_id;
                    $contactaddress->display = true;
                    $contactaddress->type = 1;

                    $contactaddress->save();

                    $this->user->standart_invoice = $cs->id;
                    $this->user->save();
                    $cs = $contactaddress;
                }
                $basket->setInvoice($cs->uuid);
            }
        }

        if ($basket->getDeliveryIsSame()) {
            $basket->setDelivery($basket->getInvoice());
        }

        $this->view->sender = $basket->getSender();
        $this->view->invoice = $basket->getInvoice();
        $this->view->delivery = $basket->getDelivery();

        $this->view->sender_address = Doctrine_Query::create()->select()->from('ContactAddress s')->where('s.contact_id = ? AND display = 1 AND s.uuid = ?', array($this->user->id, $basket->getSender()))->fetchOne();
        $this->view->invoice_address = Doctrine_Query::create()->select()->from('ContactAddress s')->where('s.contact_id = ? AND display = 1 AND s.uuid = ?', array($this->user->id, $basket->getInvoice()))->fetchOne();
        $this->view->delivery_address = Doctrine_Query::create()->select()->from('ContactAddress s')->where('s.contact_id = ? AND display = 1 AND s.uuid = ?', array($this->user->id, $basket->getDelivery()))->fetchOne();


        if ($this->_getParam('basketfield1')) {
            $basket->setBasketField1($this->_getParam('basketfield1'));
        }
        if ($this->_getParam('basketfield2')) {
            $basket->setBasketField2($this->_getParam('basketfield2'));
        }


        $this->view->senderIsSame = $basket->getSenderIsSame();
        $this->view->deliveryIsSame = $basket->getDeliveryIsSame();

        if ($this->view->delivery_address) {
            $basket->setDeliveryPLZ($this->view->delivery_address->zip);
        }

        if (file_exists($this->_configPath . '/user/address.ini')) {

            $config = new Zend_Config_Ini($this->_configPath . '/user/address.ini', 'add');
            if ($config->global) {
                $form = new EasyBib_Form($config->user);
            } else {
                $form = new Zend_Form($config->user);
            }

            $form->removeElement('update');
            $form->removeElement('del');

            $countrys = new Zend_Config_Ini(APPLICATION_PATH . '/configs/' . '/systemdefaults.ini', 'countrys');

            if (isset($form->country)) {
                $form->country->addMultiOptions($countrys->toArray());
                $form->country->setValue("DE");
            }

            if (!$this->shop->display_sender && isset($form->type)) {
                $form->type->removeMultiOption('3');
            }


            if (!$this->shop->display_delivery && isset($form->type)) {
                $form->type->removeMultiOption('2');
            }

            if ($this->_request->isPost()) {

                $formData = $this->getRequest()->getParams();

                if ($form->isValid($formData)) {

                    $contactaddress = new ContactAddress();

                    $contactaddress->anrede = 1;
                    $contactaddress->email = "";
                    $contactaddress->mobil_phone = "";
                    $contactaddress->company = "";
                    if (isset($formData['anrede'])) {
                        $contactaddress->anrede = $formData['anrede'];
                    }
                    if (isset($formData['mobil_phone'])) {
                        $contactaddress->mobil_phone = $formData['mobil_phone'];
                    }
                    if (isset($formData['email'])) {
                        $contactaddress->email = $formData['email'];
                    }
                    if (isset($formData['country'])) {
                        $contactaddress->country = $formData['country'];
                    }
                    if (isset($formData['company'])) {
                        $contactaddress->company = $formData['company'];
                    }
                    if (isset($formData['company2'])) {
                        $contactaddress->company2 = $formData['company2'];
                    }
                    $contactaddress->firstname = $formData['firstname'];
                    $contactaddress->lastname = $formData['lastname'];
                    $contactaddress->zip = $formData['zip'];
                    $contactaddress->city = $formData['city'];
                    $contactaddress->house_number = $formData['house_number'];
                    $contactaddress->street = $formData['street'];
                    $contactaddress->phone = $formData['phone'];
                    $contactaddress->display = true;
                    if ($mode->over_ride_contact != false && $mode->over_ride_contact != 'new') {
                        $contactaddress->contact_id = $mode->over_ride_contact;
                    } else {
                        $contactaddress->contact_id = $this->user->id;
                    }
                    $contactaddress->install_id = $this->user->install_id;
                    $contactaddress->display = true;

                    if (!$this->_getParam('uuid')) {
                        if (in_array('1', $formData['type'])) {
                            $cs = $contactaddress->copy();
                            $cs->uuid = "";
                            $cs->type = 1;
                            $cs->save();
                            $basket->setInvoice($cs->uuid);
                        }
                        if (in_array('2', $formData['type'])) {
                            $cs = $contactaddress->copy();
                            $cs->uuid = "";
                            $cs->type = 2;
                            $cs->save();
                            $basket->setDelivery($cs->uuid);
                        }
                        if (in_array('3', $formData['type'])) {
                            $cs = $contactaddress->copy();
                            $cs->uuid = "";
                            $cs->type = 3;
                            $cs->save();
                            $basket->setSender($cs->uuid);
                        }
                    } else {
                        $contactaddress->save();
                    }

                    $this->view->priorityMessenger('Adresse erfolgreich gespeichert', 'success');
                    $this->redirectSpeak('/basket/review');
                }
            }

            if ($config->global) {
                $editform = new EasyBib_Form($config->user);
            } else {
                $editform = new Zend_Form($config->user);
            }
            $editform->setAttrib('id', 'updateaddress');
            $editform->setName('updateaddress');
            $editform->removeElement('submit');
            $editform->removeElement('type');
            $editform->removeElement('del');
            $countrys = new Zend_Config_Ini(APPLICATION_PATH . '/configs/' . '/systemdefaults.ini', 'countrys');

            if (isset($form->country)) {
                $editform->country->addMultiOptions($countrys->toArray());
                $editform->country->setValue("DE");
                $editform->anrede->setAttrib('id', 'update_anrede');
                $editform->country->setAttrib('id', 'update_country');
            }
            $this->view->editform = $editform;
        } else {
            $config = new Zend_Config_Ini($this->_configPath . '/user/registercontact.ini', 'register');
            $form = new Zend_Form();
            $form->addAttribs(array(
                'class' => 'niceform',
                'id' => 'userreg'
            ));
            $form->addSubForm(new Zend_Form_SubForm($config->user->liefer), 'liefer');
            $form->addElement('submit', 'submit', $config->user->liefersubmit->elements->submit->options);
            if ($this->_request->isPost()) {
                $formData = $this->_request->getPost();
                if ($form->isValid($formData) && $this->_getParam('name') != 'Benutzername') {
                    $contactaddress = new ContactAddress();
                    if (isset($formData['liefer']['anrede'])) {
                        $contactaddress->anrede = $formData['liefer']['anrede'];
                    }
                    $contactaddress->firstname = $formData['liefer']['firstname'];
                    $contactaddress->lastname = $formData['liefer']['lastname'];
                    $contactaddress->zip = $formData['liefer']['zip'];
                    $contactaddress->city = $formData['liefer']['city'];
                    $contactaddress->house_number = $formData['liefer']['house_number'];
                    $contactaddress->street = $formData['liefer']['street'];
                    $contactaddress->email = $formData['liefer']['email'];
                    $contactaddress->phone = $formData['liefer']['phone'];
                    $contactaddress->company = $formData['liefer']['company'];
                    if (isset($formData['liefer']['company2'])) {
                        $contactaddress->company2 = $formData['liefer']['company2'];
                    }
                    if ($mode->over_ride_contact != false && $mode->over_ride_contact != 'new') {
                        $contactaddress->contact_id = $mode->over_ride_contact;
                    } else {
                        $contactaddress->contact_id = $this->user->id;
                    }
                    $contactaddress->install_id = $this->user->install_id;
                    $contactaddress->display = true;
                    $contactaddress->type = 2;
                    $contactaddress->save();
                    $basket->setDeliveryIsSame(false);
                    $basket->setDelivery($contactaddress->uuid);
                    $this->redirectSpeak('/basket/finish');
                } else {
                    $form->populate($formData);
                }
            }
        }

        $this->view->form = $form;

        $this->view->paymenttype = 0;
        $this->view->shippingtype = 0;
        if ($this->shop->shipping_mode == 1) {
            if (($this->getRequest()->isPost() && $this->_request->getParam('shippingtype') != '') || $basket->getShippingtype() != 0) {

                if ($this->getRequest()->isPost() && $this->_request->getParam('shippingtype') != '') {
                    $shippingtype = $this->_request->getParam('shippingtype');
                } else {
                    $shippingtype = $basket->getShippingtype();
                }

                $shippingtype = Doctrine_Query::create()->from('Shippingtype c')->where('c.id = ? AND ( (weight_from = 0 AND weight_to = 0) OR (weight_from <= ? AND weight_to >= ?) )', array(
                    $shippingtype, $gewicht, $gewicht
                ))->fetchOne();
                if (!$shippingtype) {
                    if ($this->shop->id == $this->shop->Install->defaultmarket || !$this->shop->market) {
                        $shippingtype = Doctrine_Query::create()->from('Shippingtype c')->where('c.shop_id = ? AND ( (weight_from = 0 AND weight_to = 0) OR (weight_from <= ? AND weight_to >= ?) )', array(
                            $this->shop->id, $gewicht, $gewicht
                        ))->fetchOne();
                    } else {
                        $shippingtype = Doctrine_Query::create()->from('Shippingtype c')->where('c.shop_id = ? AND ( (weight_from = 0 AND weight_to = 0) OR (weight_from <= ? AND weight_to >= ?) )', array(
                            $this->shop->Install->defaultmarket, $gewicht, $gewicht
                        ))->fetchOne();
                    }
                }

                $basket->setShippingtype($shippingtype['id']);
                $this->view->shippingselected = $shippingtype['id'];
                $this->view->shippingtypeselected = $shippingtype;

                $versand = $versand + $shippingtype->calcVersand($netto, $gewicht, $this->view->delivery_address->country, $this->user);
                if ($versand > 0 || $versandpos > 0) {
                    $versand = $versand + $versandpos;
                }
                $mwert = $mwert + ($versand / 100 * str_replace('%', '', $shippingtype['mwert']));
                $mwertalle[str_replace('%', '', $shippingtype['mwert'])] = $mwertalle[str_replace('%', '', $shippingtype['mwert'])] + ($versand / 100 * str_replace('%', '', $shippingtype['mwert']));
                $this->view->versandbrutto = $versand + ($versand / 100 * str_replace('%', '', $shippingtype['mwert']));
                if ($shippingtype['no_payment'] == 1) {
                    $this->view->no_payment = true;
                }
            } else {
                if ($this->shop->id == $this->shop->Install->defaultmarket || !$this->shop->market) {
                    $shippingtype = Doctrine_Query::create()->from('Shippingtype c')->where('c.shop_id = ? AND c.private=0 AND ( (weight_from = 0 AND weight_to = 0) OR (weight_from <= ? AND weight_to >= ?) )', array(
                        $this->shop->id, $gewicht, $gewicht
                    ))->orderBy('c.pos ASC')->fetchOne();
                } else {
                    $shippingtype = Doctrine_Query::create()->from('Shippingtype c')->where('c.shop_id = ? AND c.private=0 AND ( (weight_from = 0 AND weight_to = 0) OR (weight_from <= ? AND weight_to >= ?) )', array(
                        $this->shop->Install->defaultmarket, $gewicht, $gewicht
                    ))->orderBy('c.pos ASC')->fetchOne();
                }

                foreach ($this->shop->ShopShippingtype as $shipprivate) {
                    if ($shipprivate->Shippingtype->pos < $shippingtype->pos) {
                        $shippingtype = $shipprivate->Shippingtype;
                        break;
                    }
                }

                if (Zend_Auth::getInstance()->hasIdentity()) {
                    $user = Zend_Auth::getInstance()->getIdentity();
                    $user = Doctrine_Query::create()->from('Contact m')->where('id = ?')->fetchOne(array(
                        $user['id']
                    ));

                    foreach ($user->Account->AccountShippingtype as $shipprivate) {
                        if ($shipprivate->Shippingtype->pos < $shippingtype->pos) {
                            $shippingtype = $shipprivate->Shippingtype;
                            break;
                        }
                    }
                }

                $basket->setShippingtype($shippingtype['id']);
                $this->view->shippingselected = $shippingtype['id'];
                $this->view->shippingtypeselected = $shippingtype;
                if ($shippingtype) {
                    $versand = $versand + $shippingtype->calcVersand($netto, $gewicht, $this->view->delivery_address->country, $this->user);
                }

                if ($versand > 0 || $versandpos > 0) {
                    $versand = $versand + $versandpos;
                }
                $mwert = $mwert + ($versand / 100 * str_replace('%', '', $shippingtype['mwert']));
                $mwertalle[str_replace('%', '', $shippingtype['mwert'])] = $mwertalle[str_replace('%', '', $shippingtype['mwert'])] + ($versand / 100 * str_replace('%', '', $shippingtype['mwert']));
                $this->view->versandbrutto = $versand + ($versand / 100 * str_replace('%', '', $shippingtype['mwert']));
                if ($shippingtype['no_payment'] == 1) {
                    $this->view->no_payment = true;
                }
            }
        }
        if ($this->view->no_payment != true) {
            if ($this->getRequest()->isPost() && $this->_request->getParam('paymenttype') != '' || $basket->getPaymentType() != 0) {

                if ($this->getRequest()->isPost() && $this->_request->getParam('paymenttype') != '') {
                    $paymenttype = $this->_request->getParam('paymenttype');
                } else {
                    $paymenttype = $basket->getPaymentType();
                }

                $paymenttype = Doctrine_Query::create()->from('Paymenttype c')->where('c.id = ?', array(
                    $paymenttype
                ))->fetchOne();
                if (!$paymenttype) {
                    if ($this->shop->id == $this->shop->Install->defaultmarket || !$this->shop->market) {
                        $paymenttype = Doctrine_Query::create()->from('Paymenttype c')->where('c.shop_id = ?', array(
                            $this->shop->id
                        ))->orderBy('c.pos ASC')->fetchOne();
                    } else {
                        $paymenttype = Doctrine_Query::create()->from('Paymenttype c')->where('c.shop_id = ?', array(
                            $this->shop->Install->defaultmarket
                        ))->orderBy('c.pos ASC')->fetchOne();
                    }
                }

                $paymenttype = $paymenttype->toArray();

                $basket->setPaymenttype($paymenttype['id']);
                $this->view->paymentselected = $paymenttype['id'];
                $this->view->paymenttypeselected = $paymenttype;
                if ($paymenttype['prozent'] == 1) {
                    $this->view->paymentwert = $netto / 100 * $paymenttype['wert'];
                } else {
                    $this->view->paymentwert = $paymenttype['wert'];
                }
            } else {
                if ($this->shop->id == $this->shop->Install->defaultmarket || !$this->shop->market) {
                    $paymenttype = Doctrine_Query::create()->from('Paymenttype c')->where('c.shop_id = ? AND private = 0', array(
                        $this->shop->id
                    ))->orderBy('c.pos ASC')->fetchOne()->toArray();
                } else {
                    $paymenttype = Doctrine_Query::create()->from('Paymenttype c')->where('c.shop_id = ? AND private = 0', array(
                        $this->shop->Install->defaultmarket
                    ))->orderBy('c.pos ASC')->fetchOne()->toArray();
                }

                if (Zend_Auth::getInstance()->hasIdentity()) {
                    $user = Zend_Auth::getInstance()->getIdentity();
                    $user = Doctrine_Query::create()->from('Contact m')->where('id = ?')->fetchOne(array(
                        $user['id']
                    ));

                    foreach ($user->ContactPaymenttype as $payment) {
                        if ($payment->Paymenttype->pos < $paymenttype->pos) {
                            $paymenttype = $payment->Paymenttype;
                            break;
                        }
                    }
                }

                $basket->setPaymenttype($paymenttype['id']);
                $this->view->paymentselected = $paymenttype['id'];
                $this->view->paymenttypeselected = $paymenttype;
                if ($paymenttype['prozent'] == 1) {
                    $this->view->paymentwert = $netto / 100 * $paymenttype['wert'];
                } else {
                    $this->view->paymentwert = $paymenttype['wert'];
                }
            }
            $mwert = $mwert + ($this->view->paymentwert / 100 * $paymenttype['mwert']);
            $mwertalle[$paymenttype['mwert']] = $mwertalle[$paymenttype['mwert']] + ($this->view->paymentwert / 100 * $paymenttype['mwert']);
            $this->view->paymentwertbrutto = $this->view->paymentwert + ($this->view->paymentwert / 100 * $paymenttype['mwert']);
        } else {
            $this->view->paymentwert = 0;
            $this->view->paymentwertbrutto = 0;
        }
        if (strtoupper($this->view->delivery_address->getCountry()) != "DE" && $this->shop->id == 176) {
            $mwert = 0;
            $mwertalle = array();
        }
        $this->view->articles = $temp;
        if ($this->shop->market == true) {
            $this->view->paymenttype = Doctrine_Query::create()->from('Paymenttype c')->where('c.shop_id = ? AND c.private = 0', array(
                $this->shop->Install->defaultmarket
            ))->orderBy('pos ASC')->fetchArray();
            $this->view->shippingtype = Doctrine_Query::create()->from('Shippingtype c')->where('c.shop_id = ? AND c.private = 0 AND ( (weight_from = 0 AND weight_to = 0) OR (weight_from <= ? AND weight_to >= ?) )', array(
                $this->shop->Install->defaultmarket, $gewicht, $gewicht
            ))->orderBy('pos ASC')->fetchArray();
        } else {
            $this->view->paymenttype = Doctrine_Query::create()->from('Paymenttype c')->where('c.shop_id = ? AND c.private = 0', array(
                $this->shop->id
            ))->orderBy('pos ASC')->fetchArray();
            $this->view->shippingtype = Doctrine_Query::create()->from('Shippingtype c')->where('c.shop_id = ? AND c.private = 0 AND ( (weight_from = 0 AND weight_to = 0) OR (weight_from <= ? AND weight_to >= ?) )', array(
                $this->shop->id, $gewicht, $gewicht
            ))->orderBy('pos ASC')->fetchArray();
        }
        $this->_helper->layout->setLayout('default');

        if (count($this->view->articles) == 0) {
            $this->view->netto = 0;
            $this->view->versand = 0;
            $this->view->mwert = 0;
            unset($mwertalle['0']);
            $this->view->mwertalle = array();
            $basket->setPreisNetto(0);
            $basket->setPreisSteuer(0);
            $basket->setPreisBrutto(0);
            $basket->setVersandkosten(0);
            $basket->setZahlkosten(0);
        } else {

            $basket->setPreisNetto($netto);
            if ($netto > 0) {
                $basket->setPreisBrutto(($netto + $versand + $mwert + $this->view->paymentwert) - ($basket->getGutscheinAbzug() * 1));
            } else {
                $basket->setPreisBrutto(($versand + $mwert + $this->view->paymentwert));
            }
            $basket->setVersandkosten($versand);
            $basket->setZahlkosten($this->view->paymentwert);

            if ($this->view->gutscheincode != "") {
                $tempMwert = array();
                $mwert = 0;
                foreach ($mwertalle as $key => $mw) {

                    $anteil = (((($summewarenwert[$key] / $this->view->productbrutto) * $this->view->gutscheincodeabzug) * 100) / (100 + $key)) * ($key * 0.01);
                    $tempMwert[$key] = $mw - $anteil;
                    $mwert = $mwert + ($mw - $anteil);
                }
                $mwertalle = $tempMwert;
            }
            $this->view->netto = $basket->getPreisNetto();
            $this->view->versand = $versand;
            $this->view->mwert = $mwert;
            $basket->setPreisSteuer($mwert);
            unset($mwertalle['0']);
            $this->view->mwertalle = $mwertalle;
            $this->view->brutto = $basket->getPreisBrutto();
            $basket->setAdditional($this->view->productnetto, $this->view->productbrutto, $this->view->paymentwertbrutto, $this->view->versandbrutto, $mwertalle);
        }
        $this->view->shippingtype_extra_label = $basket->getShippingtypeExtraLabel();
        $this->view->paymenttypecontact = array();
        $this->view->shippingtypecontact = array();
        $this->view->weight_brutto = $gewicht;

        if (Zend_Auth::getInstance()->hasIdentity()) {
            $user = Zend_Auth::getInstance()->getIdentity();
            $user = Doctrine_Query::create()->from('Contact m')->where('id = ?')->fetchOne(array(
                $user['id']
            ));

            foreach ($user->ContactPaymenttype as $Paymenttype) {
                array_push($this->view->paymenttypecontact, $Paymenttype->Paymenttype->toArray());
            }

            foreach ($user->Account->AccountShippingtype as $shippingtype) {
                array_push($this->view->shippingtypecontact, $shippingtype->Shippingtype->toArray());
            }

            $mode = new Zend_Session_Namespace('adminmode');
            if ($mode->over_ride_contact != false && $mode->over_ride_contact != 'new') {
                $contact = Doctrine_Query::create()->from('Contact c')->where('c.id = ?', array($mode->over_ride_contact))->fetchOne();

                foreach ($contact->ContactPaymenttype as $Paymenttype) {
                    array_push($this->view->paymenttypecontact, $Paymenttype->Paymenttype->toArray());
                }
            }
        }

        foreach ($this->shop->ShopShippingtype as $shippingtype) {
            array_push($this->view->shippingtypecontact, $shippingtype->Shippingtype->toArray());
        }

        if ($this->_getParam('buying', false)) {
            $this->forwardSpeak('finish', 'basket', 'default', array('template_mode' => 'simple'));
        }
    }

    public function offerAction()
    {
        if(!$this->_getParam('full_layout', false)) {
            $this->_helper->layout->setLayout('simple');
        }

        $this->view->send = false;
        $this->view->step1 = true;
        $this->view->step2 = false;

        $basket = new TP_Basket();

        $this->view->basket = $basket;

        $this->view->basket_gutschein = $basket->getGutscheinAbzug();
        $this->view->basket_gutschein_enable = $basket->getGutschein();

        $this->view->gutschein = false;

        if ($basket->getGutschein() != "") {
            $gutscheincode = Doctrine_Query::create()->from('CreditSystemDetail c')->leftJoin('c.CreditSystem a')->where('a.shop_id = ? AND c.uuid = ? AND (a.product_id = 0 OR a.product_id IS NULL) AND (a.articlegroup_id = 0 OR a.articlegroup_id IS NULL) AND a.f <= ? AND a.t >= ?', array(
                $this->shop->id,
                $basket->getGutschein(), date('Y-m-d'), date('Y-m-d')
            ))->fetchOne();

            $this->view->gutschein = $gutscheincode->CreditSystem;
        }

        if ($this->getRequest()->isPost()) {

            if ($this->getRequest()->getParam('step') == 1) {
                $basket->generateOfferNumber();
                $basket->setOfferContact(array(
                    'company' => $this->getRequest()->getParam('company', ''),
                    'salutation' => $this->getRequest()->getParam('salutation', ''),
                    'firstname' => $this->getRequest()->getParam('firstname', ''),
                    'lastname' => $this->getRequest()->getParam('lastname', ''),
                    'street' => $this->getRequest()->getParam('street', ''),
                    'zip' => $this->getRequest()->getParam('zip', ''),
                    'city' => $this->getRequest()->getParam('city', ''),
                    'country' => $this->getRequest()->getParam('country', ''),
                    'ust_id' => $this->getRequest()->getParam('ust_id', ''),
                    'email' => $this->getRequest()->getParam('email', ''),
                    'title' => $this->getRequest()->getParam('title', '')
                ));
                $this->view->step1 = false;
                $this->view->step2 = true;
            }
            if ($this->getRequest()->getParam('step') == 3) {
                TP_Queue::process('basket_send_offer', 'global', $basket);


                $order = new Orders();
                $order->Shop = $this->shop;
                $order->Account = $this->shop->Acc;
                $order->created = date('Y-m-d H:i:s');
                $order->updated = date('Y-m-d H:i:s');
                $order->Install = $this->shop->Install;
                $order->enable = 1;
                $order->gutschein = $basket->getGutschein();
                $order->gutscheinabzug = $basket->getGutscheinAbzug();
                $order->gutscheinabzugtyp = $basket->getGutscheinAbzugTyp();


                $m = TP_Mongo::getInstance();
                $obj = $m->Shop->findOne(array('uid' => $this->shop->id));

                if ($obj !== NULL && $obj['offerOwnNumber']) {

                    $templates = array('template' => $obj['offerNumberPattern']);
                    $twig = new \Twig\Environment(new \Twig\Loader\ArrayLoader($templates));
                    $order->alias = $twig->render('template', array('number'=> $obj['offerNumberStart']));
                    $obj['offerNumberStart'] +=1;
                    $m->Shop->updateOne(array('uid' => $this->shop->id), [ '$set' =>  $obj]);

                }else{
                    $obj = $m->Instance->findOne(array('appId' => "1"));

                    $templates = array('template' => $obj['offerNumberPattern']);
                    $twig = new \Twig\Environment(new \Twig\Loader\ArrayLoader($templates));
                    $order->alias = $twig->render('template', array('number' => $obj['offerNumberStart']));
                    $obj['offerNumberStart'] += 1;
                    $m->Instance->updateOne(array('appId' => "1"), [ '$set' =>  $obj]);
                }
                $order->mongoLoaded = true;
                $order->status = 10;
                $order->type = 2;

                $contactAddress = new ContactAddress();
                $contactAddress->firstname = $basket->getOfferContact()['firstname'];
                $contactAddress->lastname = $basket->getOfferContact()['lastname'];
                $contactAddress->street = $basket->getOfferContact()['street'];
                $contactAddress->house_number = $basket->getOfferContact()['house_number'];
                $contactAddress->zip = $basket->getOfferContact()['zip'];
                $contactAddress->city = $basket->getOfferContact()['city'];
                $contactAddress->email = $basket->getOfferContact()['email'];
                $contactAddress->company = $basket->getOfferContact()['company'];
                $contactAddress->anrede = $basket->getOfferContact()['salutation'];
                $contactAddress->ustid = $basket->getOfferContact()['ust_id'];
                $contactAddress->pos = $basket->getOfferContact()['title'];

                $order->setDeliveryAddressSaved($contactAddress->getOrderSaveArray());
                $order->setInvoiceAddressSaved($contactAddress->getOrderSaveArray());
                $order->setSenderAddressSaved($contactAddress->getOrderSaveArray());

                $order->preis = ($basket->getPreisNetto() + $basket->getZahlkosten() + $basket->getVersandkosten());
                $order->preissteuer = $basket->getPreisSteuer();
                $order->preisbrutto = $basket->getPreisBrutto();
                $order->zahlkosten = $basket->getZahlkosten();
                $order->versandkosten = $basket->getVersandkosten();
                $order->lang = strtolower(Zend_Registry::get('locale')->getLanguage());
                $order->mwertalle = Zend_Json::encode($basket->getMWert());

                $order->save();
                $order->saveMongo();

                $posId = 1;

                /** @var TP_Basket_Item $artikel */
                foreach ($basket->getAllArtikel() as $artikel) {
                    $article = Doctrine_Query::create()->from('Article c')->where('c.uuid = ?', array(
                        $artikel->getArticleId()
                    ))->fetchOne();

                    $art = new Orderspos();
                    $art->Shop = $this->shop;
                    $art->Account = $this->shop->Acc;
                    $art->Install = $this->shop->Install;
                    $art->createdd = date('Y-m-d');
                    $art->updatedd = date('Y-m-d');
                    $art->createdt = date('H:i');
                    $art->updatedt = date('H:i');
                    $art->orders_id = $order->id;
                    $art->pos = $posId;
                    $art->calc_xml = $article->a1_xml;
                    $art->count = $artikel->getCount();
                    $art->article_id = $article->id;
                    $art->priceone = $artikel->getNetto();
                    $art->priceonesteuer = $artikel->getSteuer();
                    $art->priceonebrutto = $artikel->getBrutto();
                    $art->priceall = $artikel->getNetto() * $artikel->getCount();
                    $art->priceallsteuer = $artikel->getSteuer() * $artikel->getCount();
                    $art->priceallbrutto = $artikel->getBrutto() * $artikel->getCount();

                    $art->data = serialize($artikel);
                    $art->resale_price = $artikel->getResalePrice();
                    $art->ref = $artikel->getRef();
                    $art->kst = $artikel->getKst();
                    $art->hasgutschein = intval($artikel->getHasGutschein());
                    $art->typ = $article->typ;
                    $art->shipping_type = intval($artikel->getShippingtype());
                    $art->shipping_price = $artikel->getShippingPrice();
                    $art->calc_values = Zend_Json::encode($artikel->getCalcValues());
                    $art->save();

                    $art->setLayouterId($artikel->getLayouterId());
                    $art->setTemplatePrintId($artikel->getTemplatePrintId());
                    $art->setTemplatePrintContactId($artikel->getTemplatePrintContactId());
                    $art->setDeliveryData($artikel->getDeliverys());
                    $art->setCalcReferences($artikel->getCalcReferences());
                    $art->setAdditionalInfos($artikel->getAdditionalInfos());
                    $art->saveMongo();
                }

                $dbMongo = TP_Mongo::getInstance();
                $dbMongo->Job->insertOne(array(
                    "shop" =>  $this->shop->id,
                    "event" => "offer_create",
                    "data" => ["order" => $order->uuid],
                    "created" => new MongoDB\BSON\UTCDateTime(),
                    "updated" => new MongoDB\BSON\UTCDateTime()
                ));
                $this->view->step1 = false;
                $this->view->step2 = true;
                $this->view->send = true;
            }
            if ($this->getRequest()->getParam('step') == 2 || $this->view->step2) {

                if ($this->_request->getParam('shippingtype') == 0 && $this->getRequest()->getParam('step') != 3) {
                    $basket->setShippingtype(0);
                    $basket->setPreisNetto($basket->getProduktPreisNetto());
                    $basket->setPreisSteuer($basket->getProduktPreisSteuer());
                    $basket->setPreisBrutto($basket->getProduktPreisSteuer() + $basket->getProduktPreisNetto()- ($basket->getGutscheinAbzug() * 1));
                    $basket->setVersandkosten(0);
                    $basket->setVersandBrutto(0);
                } else {
                    if ($this->_request->getParam('shippingtype') != '') {
                        $shippingtype = $this->_request->getParam('shippingtype');
                    } else {
                        $shippingtype = $basket->getShippingtype();
                    }
                    $versand = 0;

                    $shippingtype = Doctrine_Query::create()->from('Shippingtype c')->where('c.id = ? AND ( (weight_from = 0 AND weight_to = 0) OR (weight_from <= ? AND weight_to >= ?) )', array(
                        $shippingtype, $basket->getWeight(), $basket->getWeight()
                    ))->fetchOne();


                    $basket->setShippingtype($shippingtype['id']);
                    $this->view->shippingselected = $shippingtype['id'];
                    $this->view->shippingtypeselected = $shippingtype;
                    if ($shippingtype) {
                        if ($basket->getOfferContact()['zip'] == '') {
                            $versand = $shippingtype->calcVersand($basket->getProduktPreisNetto(), $basket->getWeight(), $basket->getOfferContact()['country'], $this->user, 17506);
                        } else {
                            $versand = $shippingtype->calcVersand($basket->getProduktPreisNetto(), $basket->getWeight(), $basket->getOfferContact()['country'], $this->user, $basket->getOfferContact()['zip']);
                        }
                    }

                    $mwert = ($versand / 100 * str_replace('%', '', $shippingtype['mwert']));

                    $basket->setPreisNetto($basket->getProduktPreisNetto() + $versand);
                    $basket->setPreisSteuer($basket->getProduktPreisSteuer() + $mwert);
                    $basket->setPreisBrutto($basket->getPreisNetto() + $basket->getPreisSteuer() - ($basket->getGutscheinAbzug() * 1));
                    $basket->setVersandkosten($versand);
                    $basket->setVersandBrutto($versand + $mwert);
                }

                $cur = new Zend_Currency(Zend_Registry::get('locale')->getLanguage() . '_' . Zend_Registry::get('locale')->getRegion());

                $this->view->contact = $basket->getOfferContact();
                $this->view->positions = array();


                /**
                 * @var String $uuid
                 * @var TP_Basket_Item $art
                 */
                foreach ($basket->getAllArtikel() as $uuid => $art) {
                    $article = Doctrine_Query::create()->from('Article c')->where('c.uuid = ?', array(
                        $art->getArticleId()
                    ))->fetchOne();

                    $options = array();

                    if ($article->a6_org_article != 0) {
                        $options = $article->OrgArticle->getOptArray($art, false);
                        $arrayOptions = $article->getOptArray($art, false, true);
                    } else {
                        $options = $article->getOptArray($art, false);
                        $arrayOptions = $article->getOptArray($art, false, true);
                    }
                    $gewichtpos = $art->getWeight();

                    array_push($this->view->positions, array(
                        'uuid' => $uuid,
                        'article' => $article,
                        'basketarticle' => $art,
                        'options' => $options,
                        'arrayOptions' => $arrayOptions,
                        'weight' => $gewichtpos,
                        'price' => array(
                            'netto' => $cur->toCurrency(1 * $art->getNetto()),
                            'steuer' => $cur->toCurrency(1 * $art->getSteuer()),
                            'brutto' => $cur->toCurrency(1 * $art->getBrutto())
                        )
                    ));
                }



                $this->view->step1 = false;
                $this->view->step2 = true;
            }
        }
    }


    public function printofferAction()
    {
        $this->view->success = true;

        $basket = new TP_Basket();

        $shop = $this->shop;

        $reportPath = Zend_Registry::get('shop_path') . "/reports/";

        $what = 'offer_basket';

        $currency = new Zend_Currency(Zend_Registry::get('locale')->getLanguage() . '_' . Zend_Registry::get('locale')->getRegion());

        $tmpWert = array();

        foreach ($basket->getMWert() as $key => $mw) {
            $tmpWert[$key] = $currency->toCurrency($mw);
        }

        $params = array(
            'order' =>
            array(
                'alias' => $this->view->shop->basket_offer_number,
                'basketfield1' => $basket->getBasketField1(),
                'basketfield2' => $basket->getBasketField2()
            ),
            'netto' => $currency->toCurrency($basket->getPreisBrutto() - $basket->getPreisSteuer()),
            'brutto' => $currency->toCurrency($basket->getPreisBrutto())

        );
        $params['mwerttable'] = $tmpWert;

        $shippingtype = Doctrine_Query::create()
            ->from('Shippingtype c')
            ->where('c.id = ?', array(intval($basket->getShippingtype())))
            ->fetchOne();
        $paymenttype = Doctrine_Query::create()
            ->from('Paymenttype c')
            ->where('c.id = ?', array(intval($basket->getPaymenttype())))
            ->fetchOne();
        if ($shippingtype != false) {
            $params['shippingtype'] = $shippingtype->toArray(false);
        }

        if ($paymenttype != false) {
            $params['paymenttype'] = $paymenttype->toArray(false);
        }

        $params['shippingprice'] = $currency->toCurrency(0);
        $params['paymentprice'] = $currency->toCurrency(0);
        $params['gutscheinabzug'] = $currency->toCurrency(0);

        $data = array();
        $this->view->shop->basket_offer_number++;
        $this->view->shop->save();
        if (file_exists(APPLICATION_PATH . '/design/clients/' . $this->shop->uid . '/reports/')) {
            $reportPath = APPLICATION_PATH . '/design/clients/' . $this->shop->uid . '/reports/';
        }

        if ($this->shop->market == 1 && $this->shop->reportsite1 == "") {

            $shop = Doctrine_Query::create()->select()->from('Shop s')->where('s.id = ?', array($this->Install->defaultmarket))->fetchOne();

            $reportPath = APPLICATION_PATH . "/design/clients/" . $shop->uid . "/reports/";
        } elseif (!file_exists(Zend_Registry::get('shop_path') . "/reports/" . $what . ".jrxml")) {
            if (!file_exists(Zend_Registry::get('shop_path') . "/reports")) {
                mkdir(Zend_Registry::get('shop_path') . "/reports", 0777, true);
            }

            if (!file_exists(Zend_Registry::get('layout_path') . "/reports/" . $what . ".jrxml")) {

                return false;
            }

            copy(Zend_Registry::get('layout_path') . "/reports/" . $what . ".jrxml", Zend_Registry::get('shop_path') . "/reports/" . $what . ".jrxml");
        }

        if ($basket->getDelivery() != "") {
            $contactaddress = Doctrine_Query::create()->from('ContactAddress c')->where('c.uuid = ?', array(
                $basket->getDelivery()
            ))->fetchOne();
            $params['deliveryAddress'] = $contactaddress->toArray();
        }
        if ($basket->getInvoice() != "") {
            $contactaddress = Doctrine_Query::create()->from('ContactAddress c')->where('c.uuid = ?', array(
                $basket->getInvoice()
            ))->fetchOne();
            $params['invoiceAddress'] = $contactaddress->toArray();
        }
        if ($basket->getSender() != "") {
            $contactaddress = Doctrine_Query::create()->from('ContactAddress c')->where('c.uuid = ?', array(
                $basket->getSender()
            ))->fetchOne();
            $params['senderAddress'] = $contactaddress->toArray();
        }
        $i = 1;
        require_once(APPLICATION_PATH . '/helpers/Article.php');
        $helper = new TP_View_Helper_Article();

        foreach ($basket->getAllArtikel() as $artikel) {

            $article = Doctrine_Query::create()->from('Article c')->where('c.uuid = ?', array(
                $artikel->getArticleId()
            ))->fetchOne();

            if ($article->a6_org_article != 0) {
                $options = $article->OrgArticle->getOptArray($artikel, false);
            } else {
                $options = $article->getOptArray($artikel, false);
            }

            array_push($data, array(
                'article' => $article->toArray(), 'name' => $artikel->getArticleName(), 'count' => $artikel->getCount(), 'options' => implode(' | ', $options), 'options_array' => $options, 'pos' => $i, 'obj' => $artikel, 'priceall' => $currency->toCurrency($artikel->getNetto() * $artikel->getCount()), 'articletype' => $helper->getNearest($article, array(431))->title
            ));
            $i++;

            if (!isset($params['shippingtype']) && $artikel->getShippingtype() != false) {
                $shippingtype = Doctrine_Query::create()
                    ->from('Shippingtype c')
                    ->where('c.id = ?', array(intval($artikel->getShippingtype())))
                    ->fetchOne();

                if ($shippingtype != false) {
                    $params['shippingtype'] = $shippingtype->toArray(false);
                }
            }
        }

        $this->view->clearVars();
        try {
            $file = new TP_Rdl(
                $reportPath,
                $what . ".jrxml",
                1,
                $params,
                $data
            );

            $background = false;
            if ($what == "label" && $shop->report_background_label) {
                $background = true;
            } elseif ($what == "jobtiket" && $shop->report_background_job) {
                $background = true;
            } elseif ($what == "invoice" && $shop->report_background_invoice) {
                $background = true;
            } elseif ($what == "delivery" && $shop->report_background_delivery) {
                $background = true;
            } elseif ($what == "offer" && $shop->report_background_offer) {
                $background = true;
            } elseif ($what == "order" && $shop->report_background_order) {
                $background = true;
            }

            $host = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] && !in_array(strtolower($_SERVER['HTTPS']), array('off', 'no'))) ? 'https' : 'http';
            $host .= '://' . $_SERVER['HTTP_HOST'];


            if ($shop->reportsite1 != "" && $background) {
                $pdf = Doctrine_Query::create()
                    ->from('Image as i')
                    ->where('i.id = ?')->fetchOne(array($shop->reportsite1));

                if (!file_exists(PUBLIC_PATH . '/temp/thumb/' . $shop->uid . '/' . $shop->reportsite1 . '.jpg')) {
                    $fileDownload = 'https://cdn.shopsrv.de/' . TP_Util::encode($host . '/' . $pdf->path) . '?w=2480&fm=jpg';

                    file_put_contents(PUBLIC_PATH . '/temp/thumb/' . $shop->uid . '/' . $shop->reportsite1 . '.jpg', file_get_contents($fileDownload));
                }


                $file->addSubmittals(PUBLIC_PATH . '/temp/thumb/' . $shop->uid . '/' . $shop->reportsite1 . '.jpg');
            }
            if ($shop->reportsite2 != "" && $background) {
                $pdf = Doctrine_Query::create()
                    ->from('Image as i')
                    ->where('i.id = ?')->fetchOne(array($shop->reportsite2));

                if (!file_exists(PUBLIC_PATH . '/temp/thumb/' . $shop->uid . '/' . $shop->reportsite2 . '.jpg')) {
                    $fileDownload = 'https://cdn.shopsrv.de/' . TP_Util::encode($host . '/' . $pdf->path) . '?w=2480&fm=jpg';

                    file_put_contents(PUBLIC_PATH . '/temp/thumb/' . $shop->uid . '/' . $shop->reportsite2 . '.jpg', file_get_contents($fileDownload));
                }


                $file->addSubmittals(PUBLIC_PATH . '/temp/thumb/' . $shop->uid . '/' . $shop->reportsite2 . '.jpg');
            }

            if ($shop->report_agb != "" && $background) {
                $pdf = Doctrine_Query::create()
                    ->from('Image as i')
                    ->where('i.id = ?')->fetchOne(array($shop->report_agb));


                if (!file_exists(PUBLIC_PATH . '/temp/thumb/' . $shop->uid . '/' . $shop->report_agb . '.jpg')) {
                    $fileDownload = 'https://cdn.shopsrv.de/' . TP_Util::encode($host . '/' . $pdf->path) . '?w=2480&fm=jpg';



                    file_put_contents(PUBLIC_PATH . '/temp/thumb/' . $shop->uid . '/' . $shop->report_agb . '.jpg', file_get_contents($fileDownload));
                }


                $file->setAgb(PUBLIC_PATH . '/temp/thumb/' . $shop->uid . '/' . $shop->report_agb . '.jpg');
            }

            if (!file_exists(APPLICATION_PATH . '/../data/customerdocs/' . $shop->id)) {
                mkdir(APPLICATION_PATH . '/../data/customerdocs/' . $shop->id);
            }

            $alias = Zend_Session::getId();
            $file->export(new TP_Rdl_Renderer_Pdf(APPLICATION_PATH . '/../data/customerdocs/' . $shop->id . "/" . $alias . '_' . $what . '.pdf'));
        } catch (Exception $e) {
            Zend_Registry::get('log')->ERR($e->getMessage());
            return false;
        }

        header('Content-type: application/pdf');

        // Es wird downloaded.pdf benannt
        header('Content-Disposition: attachment; filename="offer.pdf"');

        // Die originale PDF Datei heißt original.pdf
        readfile(APPLICATION_PATH . '/../data/customerdocs/' . $shop->id . "/" . $alias . '_' . $what . '.pdf');
        unlink(APPLICATION_PATH . '/../data/customerdocs/' . $shop->id . "/" . $alias . '_' . $what . '.pdf');
        die();
    }

    public function doneAction()
    {
        if ($this->_getParam('uuid', false)) {
            $order = Doctrine_Query::create()->from('Orders c')->where('c.id = ? AND c.contact_id = ?', array(
                $this->_getParam('uuid', false), $this->user->id
            ))->fetchOne();

            $this->view->order = $order;
            $this->view->contact = $order->Contact;
            $this->view->payment = $order->Paymenttype;
        }
        if ($this->_getParam('id', false)) {
            $order = Doctrine_Query::create()->from('Orders c')->where('c.id = ? AND c.contact_id = ?', array(
                $this->_getParam('id', false), $this->user->id
            ))->fetchOne();

            $this->view->order = $order;
            $this->view->contact = $order->Contact;
            $this->view->payment = $order->Paymenttype;
        }
    }

    /**
     * IndexAction
     *
     * Basket Controller
     *
     * @return void
     */
    public function indexAction()
    {
        $this->view->inworkCount = 0;
        $articleSession = new TP_Layoutersession();

        $basket = new TP_Basket();
        if ($this->_request->getParam('del') != '') {
            $basket->removeProduct($this->_request->getParam('del'));
            if (file_exists($this->_templatePath . '/basket/simple.phtml')) {
                $this->redirectSpeak('/basket/simple');
                return;
            }
        }

        $this->view->inworkCount = count($articleSession->getAllLayouterArticle());

        if ($this->_request->getParam('delall') != '') {
            $basket->clear();
        }

        if ($this->_getParam('plz', false)) {
            $basket->setPLZ($this->_getParam('plz', false));
        }

        $temp = array();
        $netto = 0;
        $versand = 0;
        $versandpos = 0;
        $mwert = 0;
        $mwertalle = array();
        $summewarenwert = array();
        $this->view->stockError = false;
        $this->view->paymenttypecontact = array();
        $this->view->errorGutscheincode = false;
        $articles = $basket->getAllArtikel();

        if ($this->_getParam('gutscheincode', false)) {
            $basket->setGutschein($this->_getParam('gutscheincode', false));
        }

        if ($this->_getParam('basketfield1')) {
            $basket->setBasketField1($this->_getParam('basketfield1'));
        }
        if ($this->_getParam('basketfield1')) {
            $basket->setBasketField1($this->_getParam('basketfield1'));
        }
        if ($this->_getParam('payment_zusatz')) {
            $basket->setPaymentZusatz($this->_getParam('payment_zusatz'));
        }
        if ($this->_getParam('iban')) {
            $basket->setIban($this->_getParam('iban'));
        }
        if ($this->_getParam('bic')) {
            $basket->setBic($this->_getParam('bic'));
        }
        $this->view->basketfield1 = $basket->getBasketField1();
        $this->view->basketfield2 = $basket->getBasketField2();

        $this->view->paymentzusatz = $basket->getPaymentZusatz();
        $this->view->bic = $basket->getBic();
        $this->view->iban = $basket->getIban();
        $this->view->plz = $basket->getPLZ();
        $this->view->zip = $basket->getPLZ();
        $basket->setGutscheinAbzug(0);
        if (count($articles) == 0) {
            $this->view->basketIsEmpty = true;
        } else {
            $this->view->basketIsEmpty = false;
        }
        $gewicht = 0;
        $pos_article_versand = false;
        $calcVoucher = 0;


        /**
         * @var  $uuid
         * @var TP_Basket_Item $art
         */
        foreach ($articles as $uuid => $art) {
            $article = Doctrine_Query::create()->from('Article c')->where('c.uuid = ?', array(
                $art->getArticleId()
            ))->fetchOne();

            if(isset($art->getRabatte()['calc_voucher'])) {
                $calcVoucher = $calcVoucher+$art->getRabatte()['calc_voucher'];
            }


            $field = 'a1_xml';
            $options = array();

            if ($article->a6_org_article != 0) {
                $options = $article->OrgArticle->getOptArray($art, false);
            } else {
                $options = $article->getOptArray($art, false);
            }
            $gewicht += $art->getWeight();
            $gewichtpos = $art->getWeight();
            if (($this->getRequest()->isPost() && $this->_request->getParam('ref_' . $uuid) != '')) {
                $art->setRef($this->_request->getParam('ref_' . $uuid));
                $this->view->priorityMessenger('Erfolgreich gesetzt', 'success');
            }
            if (($this->getRequest()->isPost() && $this->_request->getParam('kst_' . $uuid) != '')) {
                $art->setKst($this->_request->getParam('kst_' . $uuid));
                $this->view->priorityMessenger('Erfolgreich gesetzt', 'success');
            }
            if (($this->getRequest()->isPost() && $this->_request->getParam('count_' . $uuid) != '')) {
                $art->setCount($this->_request->getParam('count_' . $uuid));
            }


            /**
            Versandart Berechnung
             **/
            $shippingtype_pos = array();
            if ($this->shop->shipping_mode >= 2) {
                if (($this->getRequest()->isPost() && $this->_request->getParam('shippingtype_' . $uuid) != '') || $art->getShippingtype() != 0) {

                    if ($this->getRequest()->isPost() && $this->_request->getParam('shippingtype_' . $uuid) != '') {
                        $shippingtype = $this->_request->getParam('shippingtype_' . $uuid);
                    } else {
                        $shippingtype = $art->getShippingtype();
                    }

                    $shippingtype_pos = Doctrine_Query::create()->from('Shippingtype c')->where('c.id = ?', array(
                        $shippingtype
                    ))->fetchOne();

                    $art->setShippingType($shippingtype_pos['id']);
                    $art->setShippingPrice($shippingtype_pos->calcVersand(($art->getnetto() * $art->getCount()), $gewichtpos, 'de', $this->user));
                    $art->setShippingPriceMWert($art->getShippingPrice() / 100 * str_replace('%', '', $shippingtype_pos['mwert']));
                    $versand = $art->getShippingPrice() + $versand;
                    $mwert = $mwert + $art->getShippingPriceMWert();
                    $mwertalle[str_replace('%', '', $shippingtype_pos['mwert'])] = $mwertalle[str_replace('%', '', $shippingtype_pos['mwert'])] + ($art->getShippingPrice() / 100 * str_replace('%', '', $shippingtype_pos['mwert']));
                } else {
                    if ($this->shop->id == $this->shop->Install->defaultmarket) {
                        $shippingtype_pos = Doctrine_Query::create()->from('Shippingtype c')->where('c.shop_id = ? AND ( (c.weight_from = 0 AND c.weight_to = 0) OR (c.weight_from <= ? AND c.weight_to >= ?) )', array(
                            $this->shop->id, $art->getWeight(), $art->getWeight()
                        ))->orderBy('c.pos ASC')->fetchOne();
                    } else {
                        $shippingtype_pos = Doctrine_Query::create()->from('Shippingtype c')->where('c.shop_id = ? AND ( (c.weight_from = 0 AND c.weight_to = 0) OR (c.weight_from <= ? AND c.weight_to >= ?) )', array(
                            $this->shop->Install->defaultmarket, $art->getWeight(), $art->getWeight()
                        ))->orderBy('c.pos ASC')->fetchOne();
                    }

                    $art->setShippingType($shippingtype_pos['id']);
                    $art->setShippingPrice($shippingtype_pos->calcVersand(($art->getnetto() * $art->getCount()), $gewichtpos, 'de', $this->user));
                    $versand = $art->getShippingPrice() + $versand;
                    $mwert = $mwert + ($art->getShippingPrice() / 100 * str_replace('%', '', $shippingtype_pos['mwert']));
                    $mwertalle[str_replace('%', '', $shippingtype_pos['mwert'])] = $mwertalle[str_replace('%', '', $shippingtype_pos['mwert'])] + ($art->getShippingPrice() / 100 * str_replace('%', '', $shippingtype_pos['mwert']));
                }
            }

            /**
            Ende
             **/

            $stockError = false;
            $stockErrorCount = 1;

            if ($article->stock && $art->getAuflage() > $article->stock_count && $article->set_config == "") {
                $stockError = true;
                $stockErrorCount = $article->stock_count;
                $this->view->stockError = true;
            }

            array_push($temp, array(
                'uuid' => $uuid,
                'article' => $article,
                'stockError' => $stockError,
                'stockErrorCount' => $stockErrorCount,
                'basketarticle' => $art,
                'shippingtype' => $shippingtype_pos,
                'options' => $options
            ));

            if ($article['versand'] == 'Fix') {
                if (!$pos_article_versand || $pos_article_versand != $article->id) {
                    $versandpos = $versandpos + $article['versandwert'];
                    $pos_article_versand = $article->id;
                }
            } elseif ($article['versand'] == 'Position') {
            }
            $art->setHasGutschein(false);
            if ($this->_getParam('gutscheincode') != "" || $basket->getGutschein() != "") {
                $articleGroups = array_keys($article->ArticleGroupArticle->toKeyValueArray('articlegroup_id', 'article_id'));

                $articlegroup = "";
                if (count($articleGroups)) {
                    if (count($articleGroups) > 1 && $articleGroups[0] == 0) {
                        unset($articleGroups[0]);
                    }

                    $articlegroup = 'AND a.articlegroup_id IN (' . implode(',', $articleGroups) . ')';
                }

                $gutscheincode = Doctrine_Query::create()->from('CreditSystemDetail c')->leftJoin('c.CreditSystem a')->andWhere('a.product_id IN (' . $article->id . ',' . $article->a6_org_article . ') AND a.shop_id = ? AND a.product_id != 0 AND c.uuid = ? AND a.f <= ? AND a.t >= ?', array(
                    $this->shop->id,
                    ($basket->getGutschein() != "" && !$this->_getParam('gutscheincode', false)) ? $basket->getGutschein() : $this->_getParam('gutscheincode'), date('Y-m-d'), date('Y-m-d')
                ))->fetchOne();
                if (!$gutscheincode) {
                    $gutscheincode = Doctrine_Query::create()->from('CreditSystemDetail c')->leftJoin('c.CreditSystem a')->andWhere('1=1 ' . $articlegroup . ' AND a.shop_id = ? AND a.articlegroup_id != 0 AND c.uuid = ? AND a.f <= ? AND a.t >= ?', array(
                        $this->shop->id,
                        ($basket->getGutschein() != "" && !$this->_getParam('gutscheincode', false)) ? $basket->getGutschein() : $this->_getParam('gutscheincode'), date('Y-m-d'), date('Y-m-d')
                    ))->fetchOne();
                }

                if ($gutscheincode && $gutscheincode->used == true && $gutscheincode->CreditSystem->more == false) {
                    $this->view->errorGutscheincode = true;
                    $basket->setGutschein("");
                    $basket->setGutscheinAbzug(0);
                    $this->view->priorityMessenger('Gutschein nicht mehr gültig', 'error');
                } elseif ($gutscheincode && $gutscheincode->CreditSystem->more == true) {

                    $gutscheincodee = Doctrine_Query::create()->from('CreditSystemDetail c')->leftJoin('c.CreditSystem a')->addWhere('(a.product_id IN (' . $article->id . ',' . $article->a6_org_article . ') ' . $articlegroup . ') AND a.shop_id = ? AND (a.product_id != 0 OR a.articlegroup_id != 0) AND c.contact_id = ? AND c.uuid = ? AND a.f <= ? AND a.t >= ?', array(
                        $this->shop->id,
                        $this->user->id,
                        $basket->getGutschein(), date('Y-m-d'), date('Y-m-d')
                    ))->fetchOne();

                    if ($gutscheincodee && $this->install->id != 34) {
                        $this->view->errorGutscheincode = true;
                        $basket->setGutschein("");
                        $basket->setGutscheinAbzug(0);
                        $this->view->priorityMessenger('Gutschein nicht mehr gültig', 'error');
                    } else {
                        $this->view->errorGutscheincode = false;
                    }
                } else {
                    $this->view->errorGutscheincode = false;
                }

                if ($gutscheincode && $this->view->errorGutscheincode == false) {

                    $this->view->errorGutscheincode = false;
                    $prozent = str_replace('%', '', $article->mwert);
                    $basket->setGutschein($gutscheincode->uuid);
                    if ($gutscheincode->CreditSystem->percent == true) {
                        $abzug = (($art->getnetto() * $art->getCount()) + (($art->getNetto() / 100 * $prozent) * $art->getCount())) / 100 * $gutscheincode->CreditSystem->wert;
                        $basket->setGutscheinAbzug($basket->getGutscheinAbzug() + $abzug);
                    } else {
                        $basket->setGutscheinAbzug($basket->getGutscheinAbzug() + $gutscheincode->CreditSystem->wert);
                    }
                    if ($basket->getPreisBrutto() < 0) {
                        $basket->setPreisBrutto(0);
                    }
                    $this->view->gutscheincode = $gutscheincode->uuid;
                    $this->view->gutscheincodeabzug = $basket->getGutscheinAbzug() * 1;

                    $basket->setGutscheinAbzugTyp(TP_Basket::GUTSCHEIN_PRODUCT);
                    $art->setHasGutschein(true);
                    $netto = $netto + ($art->getnetto() * $art->getCount()) + $art->getShippingPrice();
                    $mwertalle[$prozent] = $mwertalle[$prozent] + ($art->getNetto() * $art->getCount());
                    $summewarenwert[$prozent] = $summewarenwert[$prozent] + ($art->getnetto() * $art->getCount()) + ($art->getNetto() * $art->getCount());

                } else {
                    $basket->setGutscheinAbzug(0);
                    $netto = $netto + ($art->getnetto() * $art->getCount()) + $art->getShippingPrice();
                    $prozent = str_replace('%', '', $article->mwert);
                    $mwertalle[$prozent] = $mwertalle[$prozent] + ($art->getNetto() * $art->getCount());
                    $summewarenwert[$prozent] = $summewarenwert[$prozent] + ($art->getnetto() * $art->getCount()) + ($art->getNetto() * $art->getCount());
                }
            } else {
                $basket->setGutscheinAbzug(0);
                $netto = $netto + ($art->getnetto() * $art->getCount()) + $art->getShippingPrice();
                $prozent = str_replace('%', '', $article->mwert);
                if (!isset($mwertalle[$prozent])) {
                    $mwertalle[$prozent] = 0;
                }
                if (!isset($summewarenwert[$prozent])) {
                    $summewarenwert[$prozent] = 0;
                }
                $mwertalle[$prozent] = $mwertalle[$prozent] + ($art->getNetto() * $art->getCount());
                $summewarenwert[$prozent] = $summewarenwert[$prozent] + ($art->getnetto() * $art->getCount()) + ($art->getNetto() * $art->getCount());
            }
        }
        $mwert = 0;
        foreach($mwertalle as $key => $wert) {
            $mwert = $mwert + round($wert/100*$key, 2);
        }

        $basket->setProduktPreisSteuer($mwert);
        $this->view->productmwert = $mwert;
        $this->view->productbrutto = $netto + $mwert;
        $this->view->productnetto = $netto;
        $this->view->gutschein = false;

        if ($this->_getParam('gutscheincode') != "" || $basket->getGutschein() != "") {

            $gutscheincode = Doctrine_Query::create()->from('CreditSystemDetail c')->leftJoin('c.CreditSystem a')->where('a.shop_id = ? AND c.uuid = ? AND (a.product_id = 0 OR a.product_id IS NULL) AND (a.articlegroup_id = 0 OR a.articlegroup_id IS NULL) AND a.f <= ? AND a.t >= ?', array(
                $this->shop->id,
                ($basket->getGutschein() != "") ? $basket->getGutschein() : $this->_getParam('gutscheincode'), date('Y-m-d'), date('Y-m-d')
            ))->fetchOne();

            $gutscheincodeEverySame = Doctrine_Query::create()->from('CreditSystem a')->where('a.shop_id = ? AND a.pre_code = ? AND (a.product_id = 0 OR a.product_id IS NULL) AND (a.articlegroup_id = 0 OR a.articlegroup_id IS NULL) AND a.f <= ? AND a.t >= ?', array(
                $this->shop->id,
                ($basket->getGutschein() != "") ? $basket->getGutschein() : $this->_getParam('gutscheincode'), date('Y-m-d'), date('Y-m-d')
            ))->fetchOne();

            if ($gutscheincode && $gutscheincode->used == true && $gutscheincode->CreditSystem->more == false) {
                $this->view->errorGutscheincode = true;
                $basket->setGutschein("");
                $this->view->priorityMessenger('Gutschein nicht mehr gültig', 'error');
            } elseif ($gutscheincode && $gutscheincode->CreditSystem->more == true) {
                $gutscheincodet = Doctrine_Query::create()->from('CreditSystemDetail c')->where('c.shop_id = ? AND c.contact_id = ? AND c.uuid = ?', array(
                    $this->shop->id,
                    $this->user->id,
                    $basket->getGutschein()
                ))->fetchOne();

                if ($gutscheincodet && $this->install->id != 34) {
                    $this->view->errorGutscheincode = true;
                    $basket->setGutschein("");
                    $this->view->priorityMessenger('Gutschein nicht mehr gültig', 'error');
                } else {
                    $this->view->errorGutscheincode = false;
                }
            } else {
                $this->view->errorGutscheincode = false;
            }

            if(!$gutscheincode && $gutscheincodeEverySame) {
                $gc = $gutscheincodeEverySame;
                $gcUUid = $gutscheincodeEverySame->pre_code;
            }elseif($gutscheincode) {
                $gc = $gutscheincode->CreditSystem;
                $gcUUid = $gutscheincode->uuid;
            }

            if ($gc && $this->view->errorGutscheincode == false) {
                $basket->setGutschein($gcUUid);
                if($gc->mode == 1) {
                    if ($gc->percent == true) {
                        $abzug = round(($netto + $mwert) / 100 * $gc->wert,2);
                        $abzugNetto = round(($netto) / 100 * $gc->wert,2);
                        $basket->setGutscheinAbzug($abzug);
                        $basket->setGutscheinAbzugNetto($abzugNetto);
                        if ($netto + $mwert - $abzug < 0) {
                            $netto = 0;
                            $brutto = 0;
                            $mwert = 0;
                            $mwertalle = array();
                            $summewarenwert = array();
                        }
                    } else {
                        $basket->setGutscheinAbzug($gc->wert);
                        if ($netto + $mwert - $gc->wert < 0) {
                            $gc->wert = $netto + $mwert;
                            $basket->setGutscheinAbzug($netto + $mwert);
                            $mwertalle = array();
                            $summewarenwert = array();
                            $netto = 0;
                            $brutto = 0;
                            $mwert = 0;
                            $mwertalle = array();
                        }
                    }
                }else{
                    if ($gc->percent == true) {
                        $abzug = round($calcVoucher / 100 * $gc->wert, 2);
                        $basket->setGutscheinAbzug($abzug);
                        if ($netto + $mwert - $abzug < 0) {
                            $netto = 0;
                            $brutto = 0;
                            $mwert = 0;
                            $mwertalle = array();
                            $summewarenwert = array();
                        }
                    } else {
                        $basket->setGutscheinAbzug($gc->wert);
                        if ($netto + $mwert - $gc->wert < 0) {
                            $gc->wert = $netto + $mwert;
                            $basket->setGutscheinAbzug($netto + $mwert);
                            $mwertalle = array();
                            $summewarenwert = array();
                            $netto = 0;
                            $brutto = 0;
                            $mwert = 0;
                            $mwertalle = array();
                        }
                    }
                }

                $this->view->gutscheincode = $gcUUid;
                $this->view->gutschein = $gc;
                $this->view->gutscheincodeabzug = $basket->getGutscheinAbzug() * 1;
                $basket->setGutscheinAbzugTyp(TP_Basket::GUTSCHEIN_ALL);
            }
        }


        $this->view->productbruttogutschein = 0;
        if ($netto > 0) {
            $this->view->productbruttogutschein = $netto + $mwert - ($basket->getGutscheinAbzug() * 1);
            $this->view->productnettogutschein = $netto - ($basket->getGutscheinAbzug() * 1);
        }

        $this->view->paymenttype = 0;
        $this->view->shippingtype = 0;
        $basket->setPreisNetto($netto);
        if ($this->shop->shipping_mode == 1) {
            if (($this->getRequest()->isPost() && $this->_request->getParam('shippingtype') != '') || $basket->getShippingtype() != 0) {

                if ($this->getRequest()->isPost() && $this->_request->getParam('shippingtype') != '') {
                    $shippingtype = $this->_request->getParam('shippingtype');
                } else {
                    $shippingtype = $basket->getShippingtype();
                }

                $shippingtype = Doctrine_Query::create()->from('Shippingtype c')->where('c.id = ? AND c.enable=1 AND ( (weight_from = 0 AND weight_to = 0) OR (weight_from <= ? AND weight_to >= ?) ) AND ( (price_from = 0 AND price_to = 0) OR (price_from <= ? AND price_to >= ?) )', array(
                    $shippingtype, $gewicht, $gewicht, $basket->getPreisNetto(), $basket->getPreisNetto()
                ))->fetchOne();
                if (!$shippingtype) {
                    if ($this->shop->id == $this->shop->Install->defaultmarket || !$this->shop->market) {
                        $shippingtype = Doctrine_Query::create()->from('Shippingtype c')->where('c.shop_id = ? AND c.enable=1 AND ( (weight_from = 0 AND weight_to = 0) OR (weight_from <= ? AND weight_to >= ?) ) AND ( (price_from = 0 AND price_to = 0) OR (price_from <= ? AND price_to >= ?) )', array(
                            $this->shop->id, $gewicht, $gewicht, $basket->getPreisNetto(), $basket->getPreisNetto()
                        ))->fetchOne();
                    } else {
                        $shippingtype = Doctrine_Query::create()->from('Shippingtype c')->where('c.shop_id = ? AND c.enable=1 AND ( (weight_from = 0 AND weight_to = 0) OR (weight_from <= ? AND weight_to >= ?) ) AND ( (price_from = 0 AND price_to = 0) OR (price_from <= ? AND price_to >= ?) )', array(
                            $this->shop->Install->defaultmarket, $gewicht, $gewicht, $basket->getPreisNetto(), $basket->getPreisNetto()
                        ))->fetchOne();
                    }
                }

                $basket->setShippingtype($shippingtype['id']);
                $this->view->shippingselected = $shippingtype['id'];
                $this->view->shippingtypeselected = $shippingtype;
                if ($shippingtype) {
                    $basket->setDeliveryPLZ($this->user->self_zip);
                    $basket->setDeliveryCountry($this->user->self_country);
                    if ($basket->getDelivery() != "") {
                        $delivery_address = Doctrine_Query::create()->select()->from('ContactAddress s')->where('s.contact_id = ? AND s.uuid = ?', array($this->user->id, $basket->getDelivery()))->fetchOne();
                        $basket->setDeliveryPLZ($delivery_address->zip);
                        $basket->setDeliveryCountry($delivery_address->country);
                    }
                    if (Zend_Auth::getInstance()->hasIdentity()) {
                        $versand = $versand + $shippingtype->calcVersand($netto, $gewicht, $basket->getDeliveryCountry(), $this->user);
                    } else {
                        $versand = $versand + $shippingtype->calcVersand($netto, $gewicht, $basket->getDeliveryCountry(), $this->user, 17506);
                    }
                }
                if ($versand > 0 || $versandpos > 0) {
                    $versand = $versand + $versandpos;
                }
                $mwert = $mwert + ($versand / 100 * str_replace('%', '', $shippingtype['mwert']));
                $mwertalle[str_replace('%', '', $shippingtype['mwert'])] = $mwertalle[str_replace('%', '', $shippingtype['mwert'])] + ($versand);
                $this->view->versandbrutto = $versand + ($versand / 100 * str_replace('%', '', $shippingtype['mwert']));
                if ($shippingtype['no_payment'] == 1) {
                    $this->view->no_payment = true;
                }
            } else {
                if ($this->shop->id == $this->shop->Install->defaultmarket || !$this->shop->market) {
                    $shippingtype = Doctrine_Query::create()->from('Shippingtype c')->where('c.shop_id = ? AND c.enable=1 AND c.private=0 AND ( (weight_from = 0 AND weight_to = 0) OR (weight_from <= ? AND weight_to >= ?) ) AND ( (price_from = 0 AND price_to = 0) OR (price_from <= ? AND price_to >= ?) )', array(
                        $this->shop->id, $gewicht, $gewicht, $basket->getPreisNetto(), $basket->getPreisNetto()
                    ))->orderBy('c.pos ASC')->fetchOne();
                } else {
                    $shippingtype = Doctrine_Query::create()->from('Shippingtype c')->where('c.shop_id = ? AND c.enable=1 AND c.private=0 AND ( (weight_from = 0 AND weight_to = 0) OR (weight_from <= ? AND weight_to >= ?) ) AND ( (price_from = 0 AND price_to = 0) OR (price_from <= ? AND price_to >= ?) )', array(
                        $this->shop->Install->defaultmarket, $gewicht, $gewicht, $basket->getPreisNetto(), $basket->getPreisNetto()
                    ))->orderBy('c.pos ASC')->fetchOne();
                }

                foreach ($this->shop->ShopShippingtype as $shipprivate) {
                    if ($shipprivate->Shippingtype->pos < $shippingtype->pos) {
                        $shippingtype = $shipprivate->Shippingtype;
                        break;
                    }
                }

                if (Zend_Auth::getInstance()->hasIdentity()) {
                    $user = Zend_Auth::getInstance()->getIdentity();
                    $user = Doctrine_Query::create()->from('Contact m')->where('id = ?')->fetchOne(array(
                        $user['id']
                    ));
                    foreach ($user->ContactShippingtype as $shipprivate) {
                        if ($shipprivate->Shippingtype->pos < $shippingtype->pos || $shippingtype == null) {
                            $shippingtype = $shipprivate->Shippingtype;
                            break;
                        }
                    }

                    foreach ($user->Account->AccountShippingtype as $shipprivate) {
                        if ($shipprivate->Shippingtype->pos < $shippingtype->pos || $shippingtype == null) {
                            $shippingtype = $shipprivate->Shippingtype;
                            break;
                        }
                    }
                }

                $basket->setShippingtype($shippingtype['id']);
                $this->view->shippingselected = $shippingtype['id'];
                $this->view->shippingtypeselected = $shippingtype;
                if ($shippingtype) {
                    if (Zend_Auth::getInstance()->hasIdentity()) {
                        $versand = $versand + $shippingtype->calcVersand($netto, $gewicht, $this->user->self_country, $this->user);
                    } else {
                        $versand = $versand + $shippingtype->calcVersand($netto, $gewicht, $this->user->self_country, $this->user, 17506);
                    }
                }

                if ($versand > 0 || $versandpos > 0) {
                    $versand = $versand + $versandpos;
                }
                $mwert = $mwert + ($versand / 100 * str_replace('%', '', $shippingtype['mwert']));
                $mwertalle[str_replace('%', '', $shippingtype['mwert'])] = $mwertalle[str_replace('%', '', $shippingtype['mwert'])] + ($versand);
                $this->view->versandbrutto = $versand + ($versand / 100 * str_replace('%', '', $shippingtype['mwert']));
                if ($shippingtype['no_payment'] == 1) {
                    $this->view->no_payment = true;
                }
            }
        }

        $this->view->paymenttypecontact = array();
        $paymenttypeids = array(0);
        $this->view->shippingtypecontact = array();
        $shippingtypeids = array(0);

        if (Zend_Auth::getInstance()->hasIdentity()) {
            $user = Zend_Auth::getInstance()->getIdentity();
            $user = Doctrine_Query::create()->from('Contact m')->where('id = ?')->fetchOne(array(
                $user['id']
            ));

            foreach ($user->ContactPaymenttype as $Paymenttype) {
                array_push($paymenttypeids, $Paymenttype->paymenttype_id);
            }

            foreach ($user->ContactShippingtype as $shippingtype) {
                array_push($shippingtypeids, $shippingtype->shippingtype_id);
            }

            foreach ($user->Account->AccountPaymenttype as $Paymenttype) {
                array_push($paymenttypeids, $Paymenttype->paymenttype_id);
            }

            foreach ($user->Account->AccountShippingtype as $shippingtype) {
                array_push($shippingtypeids, $shippingtype->shippingtype_id);
            }
        }

        foreach ($this->shop->ShopShippingtype as $shippingtype) {
            if ($shippingtype->shop_id == $this->shop->id) {
                array_push($shippingtypeids, $shippingtype->shippingtype_id);
            }
        }
        $this->view->shippingtype = Doctrine_Query::create()->from('Shippingtype c')->where('c.shop_id = ? AND c.enable = 1 AND (c.private = 0 OR (c.private = 1 AND c.id in (' . implode(',', $shippingtypeids) . '))) AND ( (weight_from = 0 AND weight_to = 0) OR (weight_from <= ? AND weight_to >= ?) ) AND ( (price_from = 0 AND price_to = 0) OR (price_from <= ? AND price_to >= ?) )', array(
            $this->shop->id, $gewicht, $gewicht, $basket->getPreisNetto(), $basket->getPreisNetto()
        ))->orderBy('pos ASC')->fetchArray();

        $this->view->paymenttype = [];

        $payments = Doctrine_Query::create()->from('Paymenttype c')->where('c.shop_id = ? AND c.enable = 1 AND (c.private = 0 OR (c.private = 1 AND c.id in (' . implode(',', $paymenttypeids) . ')))', array(
            $this->shop->id
        ))->orderBy('pos ASC')->execute();

        foreach ($payments as $payment) {
            if (count($payment->getShippings()) == 0 || in_array($this->view->shippingselected, (array)$payment->getShippings())) {
                $this->view->paymenttype[] = $payment->toArray();
            }
        }

        if ($this->view->no_payment != true) {
            if ($this->getRequest()->isPost() && $this->_request->getParam('paymenttype') != '' || $basket->getPaymentType() != 0) {

                if ($this->getRequest()->isPost() && $this->_request->getParam('paymenttype') != '') {
                    $paymenttype = $this->_request->getParam('paymenttype');
                } else {
                    $paymenttype = $basket->getPaymentType();
                }

                $paymenttype = array_filter($this->view->paymenttype, function ($obj) use ($paymenttype) {
                    return $obj['id'] == $paymenttype;
                });

                if ($paymenttype == null) {
                    $paymenttype = $this->view->paymenttype[0];
                } else {
                    $paymenttype = current($paymenttype);
                }

                $basket->setPaymenttype($paymenttype['id']);
                $this->view->paymentselected = $paymenttype['id'];
                $this->view->paymenttypeselected = $paymenttype;
                if ($paymenttype['prozent'] == 1) {
                    $this->view->paymentwert = $netto / 100 * $paymenttype['wert'];
                    if ($this->install->id == 7 && round($basket->getProduktPreisBrutto(), 2) == round($basket->getGutscheinAbzug(), 2)) {
                        $this->view->paymentwert = 0;
                    }
                } else {
                    $this->view->paymentwert = $paymenttype['wert'];
                }
            } elseif (count($this->view->paymenttype) > 0) {
                $paymenttype = $this->view->paymenttype[0];

                $basket->setPaymenttype($paymenttype['id']);
                $this->view->paymentselected = $paymenttype['id'];
                $this->view->paymenttypeselected = $paymenttype;
                if ($paymenttype['prozent'] == 1) {
                    $this->view->paymentwert = $netto / 100 * $paymenttype['wert'];
                    if ($this->install->id == 7 && round($basket->getProduktPreisBrutto(), 2) == round($basket->getGutscheinAbzug(), 2)) {
                        $this->view->paymentwert = 0;
                    }
                } else {
                    $this->view->paymentwert = $paymenttype['wert'];
                }
            }
            if ($this->shop->uid == '4ewlieu') {
                $mwert = $mwert + ($this->view->paymentwert / 100 * 20);
                $mwertalle[20] = $mwertalle[20] + ($this->view->paymentwert);
                $this->view->paymentwertbrutto = $this->view->paymentwert + ($this->view->paymentwert / 100 * 20);
            } else {
                $mwert = ($mwert + ($this->view->paymentwert / 100 * $paymenttype['mwert']));
                $mwertalle[$paymenttype['mwert']] = ($mwertalle[$paymenttype['mwert']] + ($this->view->paymentwert));
                $this->view->paymentwertbrutto = $this->view->paymentwert + ($this->view->paymentwert / 100 * $paymenttype['mwert']);
            }
        } else {
            $this->view->paymentwert = 0;
            $this->view->paymentwertbrutto = 0;
        }
        $this->view->articles = $temp;

        $this->_helper->layout->setLayout('default');

        $tmpMwertAlle = [];
        foreach($mwertalle as $key => $wert) {
            $tmpMwertAlle[$key] = round($wert/100*$key, 2);
        }
        $mwertalle = $tmpMwertAlle;

        if (count($this->view->articles) == 0) {
            $this->view->netto = 0;
            $this->view->versand = 0;
            $this->view->mwert = 0;
            unset($mwertalle['0']);
            $this->view->mwertalle = array();
            $basket->setPreisNetto(0);
            $basket->setPreisSteuer(0);
            $basket->setPreisBrutto(0);
            $basket->setVersandkosten(0);
            $basket->setZahlkosten(0);
        } else {

            $basket->setPreisNetto($netto);
            if ($netto > 0) {
                $basket->setPreisBrutto(($netto + $versand + $mwert + $this->view->paymentwert) - ($basket->getGutscheinAbzug() * 1));
            } else {
                $basket->setPreisBrutto(($versand + $mwert + $this->view->paymentwert));
            }
            $basket->setVersandkosten($versand);
            $basket->setZahlkosten($this->view->paymentwert);

            if ($this->view->gutscheincode != "" && $basket->getGutscheinAbzugTyp() != TP_Basket::GUTSCHEIN_PRODUCT) {
                $tempMwert = array();
                $mwert = 0;
                foreach ($mwertalle as $key => $mw) {

                    $anteil = (((($summewarenwert[$key] / $this->view->productbrutto) * $this->view->gutscheincodeabzug) * 100) / (100 + $key)) * ($key * 0.01);
                    $tempMwert[$key] = $mw - $anteil;
                    $mwert = $mwert + ($mw - $anteil);
                }
                $mwertalle = $tempMwert;
            }
            $this->view->netto = $basket->getPreisNetto();
            $this->view->versand = $versand;
            $this->view->mwert = $mwert;
            $basket->setPreisSteuer($mwert);
            unset($mwertalle['0']);
            $this->view->mwertalle = $mwertalle;
            $this->view->brutto = $basket->getPreisBrutto();
            $basket->setAdditional($this->view->productnetto, $this->view->productbrutto, $this->view->paymentwertbrutto, $this->view->versandbrutto, $mwertalle);
        }

        $this->view->shippingtype_extra_label = $basket->getShippingtypeExtraLabel();

        $this->view->weight_brutto = $gewicht;

        if ($this->_getParam('mode', false) && $this->_getParam('mode', false) == "offer") {
            $this->redirectSpeak('/basket/offer');

            return;
        }
    }

    public function simplereviewAction()
    {

        $tmp = array();

        if (Zend_Registry::get('locale')->getLanguage() . '_' . Zend_Registry::get('locale')->getRegion() != 'de_DE') {

            $tmp['1'] = 'International';
            $tmp['9037'] = '9037&nbsp;Airport Seating Division';
            $tmp['9035'] = '9035&nbsp;Asien/Pazifik und S&uuml;dafrika ';
            $tmp['9021'] = '9021&nbsp;Belgium';
            $tmp['9023'] = '9023&nbsp;Eastern Europe and Russia';
            $tmp['9013'] = '9013&nbsp;Export Director';
            $tmp['9017'] = '9017&nbsp;France';
            $tmp['9020'] = '9020&nbsp;Great Britain and Ireland';
            $tmp['9025'] = '9025&nbsp;Middle East';
            $tmp['9015'] = '9015&nbsp;Netherlands';
            $tmp['9014'] = '9014&nbsp;Other countries';
            $tmp['9010'] = '9010&nbsp;Sales support international';
            $tmp['9024'] = '9024&nbsp;Scandinavia';
            $tmp['9022'] = '9022&nbsp;Spain and South America';
            $tmp['9026'] = '9026&nbsp;Switzerland and South Tyrol';
            $tmp['9016'] = '9016&nbsp;USA';
            $tmp['8170'] = '8170&nbsp;Sitag AG';
            $tmp['2'] = 'Germany';
            $tmp['9390.2'] = '9390&nbsp;Kusch+Co DK Sachsen GmbH, D&ouml;beln';
            $tmp['9470'] = '9470&nbsp;Office Gro&szlig;-Gerau';
            $tmp['9410'] = '9410&nbsp;Office Munich';
            $tmp['9330'] = '9330&nbsp;Office Pohle';
            $tmp['9432'] = '9432&nbsp;Sales Arndt, Gerd';
            $tmp['9433'] = '9433&nbsp;Sales Becker, J&ouml;rg';
            $tmp['9474'] = '9474&nbsp;Sales Becker, Petra';
            $tmp['9416'] = '9416&nbsp;Sales Bitter, Christine';
            $tmp['9300'] = '9300&nbsp;Sales Director Germany';
            $tmp['9331'] = '9331&nbsp;Sales Fries, Hans J&uuml;rgen ';
            $tmp['9434'] = '9434&nbsp;Sales Garthoff, Kai';
            $tmp['9451'] = '9451&nbsp;Sales Hering, Monika';
            $tmp['9352'] = '9352&nbsp;Sales Hesse, Heiko';
            $tmp['9392'] = '9392&nbsp;Sales Wolf, Jan';
            $tmp['9471'] = '9471&nbsp;Sales Kalisch, Olaf';
            $tmp['9412'] = '9412&nbsp;Sales Lang, Uwe';
            $tmp['9311'] = '9311&nbsp;Sales Mehlig, Tobias';
            $tmp['9435'] = '9435&nbsp;Sales M&uuml;nch, Lothar';
            $tmp['9312'] = '9312&nbsp;Sales P&ouml;llmann, Thomas ';
            $tmp['9453'] = '9453&nbsp;Sales Reichelt, Jens';
            $tmp['9414'] = '9414&nbsp;Sales Rother, Fathia';
            $tmp['9333'] = '9333&nbsp;Sales Schmidt, Hans Joachim ';
            $tmp['9332'] = '9332&nbsp;Sales Schwarz, Wilhelm';
            $tmp['9334'] = '9334&nbsp;Sales Frommelt, Marcus';
            $tmp['9415'] = '9415&nbsp;Sales Zeidler, Simone Gesine ';
            $tmp['9000'] = '9000&nbsp;Sales support Germany';
            $tmp['9310'] = '9310&nbsp;Showroom Berlin';
            $tmp['9160'] = '9160&nbsp;Showroom Cologne';
            $tmp['9390.1'] = '9390&nbsp;Thomas Lange GmbH & Co KG, Hamburg';
            $tmp['3'] = 'Hallenberg';
            $tmp['9112'] = '9112&nbsp;Business Development';
            $tmp['9150'] = '9150&nbsp;Infocenter';
            $tmp['9110'] = '9110&nbsp;Marketing';
            $tmp['9120'] = '9120&nbsp;Exhibition Germany';
            $tmp['9121'] = '9121&nbsp;Exhibition Orgatec';
            $tmp['9122'] = '9122&nbsp;Exhibition Medica';
            $tmp['9123'] = '9123&nbsp;Exhibition inter airport';
            $tmp['9124'] = '9124&nbsp;Exhibition Altenpflege';
            $tmp['9125'] = '9125&nbsp;Exhibition EVVC';
            $tmp['9130'] = '9130&nbsp;Exhibition International';
            $tmp['9131'] = '9131&nbsp;Exhibition Interieur';
            $tmp['9137'] = '9137&nbsp;Exhibition Mailand';
            $tmp['9132'] = '9132&nbsp;Exhibition Passenger Terminal EXPO';
            $tmp['9133'] = '9133&nbsp;Exhibition Stockholm Furniture Fair';
            $tmp['9134'] = '9134&nbsp;Exhibition ACI Veranstaltungen';
            $tmp['9135'] = '9135&nbsp;Exhibition Health Care Mechelen';
            $tmp['9136'] = '9136&nbsp;Exhibition EXPO60+';
            $tmp['8970'] = '8970&nbsp;Sample stock Hallenberg';
            $tmp['8140'] = '8140&nbsp;Human Resoure Department';
            $tmp['8930'] = '8930&nbsp;Project team Hallenberg';
        } else {

            $tmp['1'] = 'Ausland';
            $tmp['9037'] = '9037&nbsp;Airport Seating Division';
            $tmp['9035'] = '9035&nbsp;Asia/Pacific and South Africa ';
            $tmp['9021'] = '9021&nbsp;Belgien';
            $tmp['9017'] = '9017&nbsp;Frankreich';
            $tmp['9020'] = '9020&nbsp;Gro&szlig;britannien und Irland';
            $tmp['9025'] = '9025&nbsp;Mittlerer Osten';
            $tmp['9015'] = '9015&nbsp;Niederlande';
            $tmp['9023'] = '9023&nbsp;Osteuropa und Russland ';
            $tmp['9013'] = '9013&nbsp;Ressortleitung Vertrieb International';
            $tmp['9014'] = '9014&nbsp;restliche Welt';
            $tmp['9026'] = '9026&nbsp;Schweiz und S&uuml;dtirol';
            $tmp['9024'] = '9024&nbsp;Skandinavien';
            $tmp['9022'] = '9022&nbsp;Spanien und S&uuml;damerika';
            $tmp['9016'] = '9016&nbsp;USA';
            $tmp['9010'] = '9010&nbsp;Vertriebsinnendienst International';
            $tmp['8170'] = '8170&nbsp;Sitag AG';
            $tmp['2'] = 'Inland';
            $tmp['9470'] = '9470&nbsp;B&uuml;ro Gro&szlig;-Gerau';
            $tmp['9410'] = '9410&nbsp;B&uuml;ro M&uuml;nchen';
            $tmp['9330'] = '9330&nbsp;B&uuml;ro Pohle';
            $tmp['9390.2'] = '9390&nbsp;Kusch+Co DK Sachsen GmbH, D&ouml;beln';
            $tmp['9300'] = '9300&nbsp;Ressortleitung Vertrieb National';
            $tmp['9310'] = '9310&nbsp;Showroom Berlin';
            $tmp['9160'] = '9160&nbsp;Showroom K&ouml;ln';
            $tmp["9390.1"] = '9390&nbsp;Thomas Lange GmbH & Co KG, Hamburg';
            $tmp['9432'] = '9432&nbsp;Vertrieb Arndt, Gerd';
            $tmp['9433'] = '9433&nbsp;Vertrieb Becker, J&ouml;rg';
            $tmp['9474'] = '9474&nbsp;Vertrieb Becker, Petra';
            $tmp['9416'] = '9416&nbsp;Vertrieb Bitter, Christine';
            $tmp['9331'] = '9331&nbsp;Vertrieb Fries, Hans J&uuml;rgen ';
            $tmp['9434'] = '9434&nbsp;Vertreib Garthoff, Kai';
            $tmp['9451'] = '9451&nbsp;Vertrieb Hering, Monika';
            $tmp['9352'] = '9352&nbsp;Vertrieb Hesse, Heiko';
            $tmp['9392'] = '9392&nbsp;Vertrieb Wolf, Jan';
            $tmp['9471'] = '9471&nbsp;Vertrieb Kalisch, Olaf';
            $tmp['9412'] = '9412&nbsp;Vertrieb Lang, Uwe';
            $tmp['9311'] = '9311&nbsp;Vertrieb Mehlig, Tobias';
            $tmp['9435'] = '9435&nbsp;Vertrieb M&uuml;nch, Lothar';
            $tmp['9312'] = '9312&nbsp;Vertrieb P&ouml;llmann, Thomas ';
            $tmp['9453'] = '9453&nbsp;Vertrieb Reichelt, Jens';
            $tmp['9414'] = '9414&nbsp;Vertrieb Rother, Fathia';
            $tmp['9333'] = '9333&nbsp;Vertrieb Schmidt, Hans Joachim ';
            $tmp['9334'] = '9334&nbsp;Vertrieb Frommelt, Marcus';
            $tmp['9332'] = '9332&nbsp;Vertrieb Schwarz, Wilhelm';
            $tmp['9415'] = '9415&nbsp;Vertrieb Zeidler, Simone Gesine ';
            $tmp['9000'] = '9000&nbsp;Vertriebsinnendienst National';
            $tmp['3'] = 'Hallenberg';
            $tmp['9112'] = '9112&nbsp;Business Development';
            $tmp['9150'] = '9150&nbsp;Infocenter';
            $tmp['9110'] = '9110&nbsp;Marketing';
            $tmp['9120'] = '9120&nbsp;Messen Inland';
            $tmp['9121'] = '9121&nbsp;Messe Orgatec';
            $tmp['9122'] = '9122&nbsp;Messe Medica';
            $tmp['9123'] = '9123&nbsp;Messe inter airport';
            $tmp['9124'] = '9124&nbsp;Messe Altenpflege';
            $tmp['9125'] = '9125&nbsp;Messe EVVC';
            $tmp['9130'] = '9130&nbsp;Messen Ausland';
            $tmp['9131'] = '9131&nbsp;Messe Interieur';
            $tmp['9137'] = '9137&nbsp;Messe Mailand';
            $tmp['9132'] = '9132&nbsp;Messe Passenger Terminal EXPO';
            $tmp['9133'] = '9133&nbsp;Messe Stockholm Furniture Fair';
            $tmp['9134'] = '9134&nbsp;Messe ACI Veranstaltungen';
            $tmp['9135'] = '9135&nbsp;Messe Health Care Mechelen';
            $tmp['9136'] = '9136&nbsp;Messe EXPO60+';
            $tmp['8970'] = '8970&nbsp;Musterlager Hallenberg';
            $tmp['8140'] = '8140&nbsp;Personalwesen';
            $tmp['8930'] = '8930&nbsp;Projektteam Hallenberg';
        }

        $this->view->kostenstellen = $tmp;


        $this->view->countrys = Zend_Locale::getCountryTranslationList();

        if (!Zend_Auth::getInstance()->hasIdentity()) {
            $this->forwardSpeak('login', 'user', 'default');
            return;
        }

        $basket = new TP_Basket();
        if ($this->_getParam('basketfield1')) {
            $basket->setBasketField1($this->_getParam('basketfield1'));
        }
        if ($this->_getParam('basketfield2')) {
            $basket->setBasketField2($this->_getParam('basketfield2'));
        }
        $this->view->basketfield1 = $basket->getBasketField1();
        $this->view->basketfield2 = $basket->getBasketField2();


        $errors = array();
        if ($this->_request->isPost() && !$this->_getParam("self_lastname", false)) {
            $errors['self_lastname'] = "No Value";
        }

        if ($this->_request->isPost() && !$this->_getParam("self_street", false)) {
            $errors['self_street'] = "No Value";
        }

        if ($this->_request->isPost() && !$this->_getParam("self_department", false)) {
            $errors['self_department'] = "No Value";
        }
        if ($this->_request->isPost() && !$this->_getParam("self_zip", false)) {
            $errors['self_zip'] = "No Value";
        }
        if ($this->_request->isPost() && !$this->_getParam("self_city", false)) {
            $errors['self_city'] = "No Value";
        }
        if ($this->_request->isPost() && !$this->_getParam("ustid", false)) {
            $errors['ustid'] = "No Value";
        }
        if ($this->_request->isPost() && !$this->_getParam("self_position", false)) {
            $errors['self_position'] = "No Value";
        }
        if ($this->_request->isPost() && !$this->_getParam("self_email", false)) {
            $errors['self_email'] = "No Value";
        }


        if (empty($errors) && $this->_request->isPost() && $this->_getParam("self_lastname", false)) {

            $this->user->ustid = $this->_getParam("ustid");
            $this->user->self_position = $this->_getParam("self_position");
            $this->user->self_anrede = $this->_getParam("self_anrede");
            $this->user->liefer_lastname = $this->_getParam("self_lastname");
            $this->user->liefer_firstname = $this->_getParam("self_firstname");
            $this->user->self_department = $this->_getParam("self_department");
            $this->user->self_street = $this->_getParam("self_street");
            $this->user->street = $this->_getParam("street");
            $this->user->phone_alternative = $this->_getParam("phone_alternative");
            $this->user->self_zip = $this->_getParam("self_zip");
            $this->user->self_city = $this->_getParam("self_city");
            $this->user->self_country = $this->_getParam("self_country");
            $this->user->self_phone = $this->_getParam("self_phone");
            $this->user->email = $this->_getParam("self_email");
            $this->user->save();

            $this->redirectSpeak("/basket/finish");
        } else {
            if ($this->_request->isPost()) {
                $this->user->ustid = $this->_getParam("ustid");
                $this->user->self_position = $this->_getParam("self_position");
                $this->user->self_anrede = $this->_getParam("self_anrede");
                $this->user->liefer_lastname = $this->_getParam("self_lastname");
                $this->user->liefer_firstname = $this->_getParam("self_firstname");
                $this->user->self_department = $this->_getParam("self_department");
                $this->user->self_street = $this->_getParam("self_street");
                $this->user->street = $this->_getParam("street");
                $this->user->phone_alternative = $this->_getParam("phone_alternative");
                $this->user->self_zip = $this->_getParam("self_zip");
                $this->user->self_city = $this->_getParam("self_city");
                $this->user->self_country = $this->_getParam("self_country");
                $this->user->self_phone = $this->_getParam("self_phone");
                $this->user->email = $this->_getParam("self_email");
                $this->user->save();
            }
        }

        $this->view->formErrors = $errors;
    }


    public function reviewAction()
    {

        $mode = new Zend_Session_Namespace('adminmode');
        $basket = new TP_Basket();
        if ($this->_getParam('basketfield1')) {
            $basket->setBasketField1($this->_getParam('basketfield1'));
        }
        if ($this->_getParam('basketfield2')) {
            $basket->setBasketField2($this->_getParam('basketfield2'));
        }
        if ($this->_getParam('iban')) {
            $basket->setIban($this->_getParam('iban'));
        }
        if ($this->_getParam('bic')) {
            $basket->setBic($this->_getParam('bic'));
        }
        if (!Zend_Auth::getInstance()->hasIdentity()) {
            $this->forwardSpeak('login', 'user', 'default', array(
                'mode' => 'basket'
            ));
            return;
        }
        if ($mode->over_ride_contact == 'new') {
            $this->forwardSpeak('register', 'user', 'default', array(
                'mode' => 'basket'
            ));
            return;
        }
        $this->view->basketInfo = $basket->getInfo();
        $this->view->addresses = Doctrine_Query::create()->from('ContactAddress a')->where('type = 2 AND contact_id = ? AND display = 1', array(
            $this->user->id
        ))->limit(100)->execute();
        $this->view->addresses_invoice = Doctrine_Query::create()->from('ContactAddress a')->where('type = 1 AND contact_id = ? AND display = 1', array(
            $this->user->id
        ))->limit(100)->execute();
        $this->view->addresses_sender = Doctrine_Query::create()->from('ContactAddress a')->where('type = 3 AND contact_id = ? AND display = 1', array(
            $this->user->id
        ))->limit(100)->execute();
        $this->view->addresses_delivery = $this->view->addresses;

        if ($basket->getDelivery() == 0) {
            if ($this->user->standart_delivery == 0) {
                $cs = Doctrine_Query::create()->select()->from('ContactAddress s')->where('s.contact_id = ? AND type = 2 AND display = 1', array($this->user->id))->limit(1)->fetchOne();

                if ($cs) {
                    $this->user->standart_delivery = $cs->id;
                    $this->user->save();
                } else {
                    $contact = $this->user;
                    $contactaddress = new ContactAddress();
                    $contactaddress->anrede = $contact->self_anrede;
                    $contactaddress->company = $contact->self_department;
                    $contactaddress->country = $contact->self_country;
                    $contactaddress->firstname = $contact->self_firstname;
                    $contactaddress->lastname = $contact->self_lastname;
                    $contactaddress->zip = $contact->self_zip;
                    $contactaddress->city = $contact->self_city;
                    $contactaddress->house_number = $contact->self_house_number;
                    $contactaddress->street = $contact->self_street;
                    $contactaddress->email = $contact->self_email;
                    $contactaddress->phone = $contact->self_phone;

                    $contactaddress->contact_id = $contact->id;
                    $contactaddress->install_id = $contact->install_id;
                    $contactaddress->display = true;
                    $contactaddress->type = 2;

                    $contactaddress->save();

                    $this->user->standart_delivery = $cs->id;
                    $this->user->save();
                    $cs = $contactaddress;
                }
                $basket->setDelivery($cs->uuid);
            } else {
                $cs = Doctrine_Query::create()->select()->from('ContactAddress s')->where('s.contact_id = ? AND display = 1 AND type = 2 AND s.id = ?', array($this->user->id, $this->user->standart_delivery))->fetchOne();

                if ($cs) {
                    $basket->setDelivery($cs->uuid);
                } else {
                    $cs = Doctrine_Query::create()->select()->from('ContactAddress s')->where('s.contact_id = ? AND display = 1 AND type = 2', array($this->user->id))->limit(1)->fetchOne();

                    if ($cs) {
                        $this->user->standart_delivery = $cs->id;
                        $this->user->save();
                    } else {
                        $contact = $this->user;
                        $contactaddress = new ContactAddress();
                        $contactaddress->anrede = $contact->self_anrede;
                        $contactaddress->company = $contact->self_department;
                        $contactaddress->country = $contact->self_country;
                        $contactaddress->firstname = $contact->self_firstname;
                        $contactaddress->lastname = $contact->self_lastname;
                        $contactaddress->zip = $contact->self_zip;
                        $contactaddress->city = $contact->self_city;
                        $contactaddress->house_number = $contact->self_house_number;
                        $contactaddress->street = $contact->self_street;
                        $contactaddress->email = $contact->self_email;
                        $contactaddress->phone = $contact->self_phone;

                        $contactaddress->contact_id = $contact->id;
                        $contactaddress->install_id = $contact->install_id;
                        $contactaddress->display = true;
                        $contactaddress->type = 2;

                        $contactaddress->save();

                        $this->user->standart_delivery = $cs->id;
                        $this->user->save();
                        $cs = $contactaddress;
                    }
                    $basket->setDelivery($cs->uuid);
                }
            }
        } else {

            $cs = Doctrine_Query::create()->select()->from('ContactAddress s')->where('s.contact_id = ? AND display = 1 AND s.uuid = ?', array($this->user->id, $basket->getDelivery()))->fetchOne();

            if ($cs) {
                $basket->setDelivery($cs->uuid);
            } else {

                $cs = Doctrine_Query::create()->select()->from('ContactAddress s')->where('s.contact_id = ? AND display = 1 AND type = 2', array($this->user->id))->limit(1)->fetchOne();

                if ($cs) {
                    $this->user->standart_delivery = $cs->id;
                    $this->user->save();
                } else {
                    $contact = $this->user;
                    $contactaddress = new ContactAddress();
                    $contactaddress->anrede = $contact->self_anrede;
                    $contactaddress->company = $contact->self_department;
                    $contactaddress->firstname = $contact->self_firstname;
                    $contactaddress->country = $contact->self_country;
                    $contactaddress->lastname = $contact->self_lastname;
                    $contactaddress->zip = $contact->self_zip;
                    $contactaddress->city = $contact->self_city;
                    $contactaddress->house_number = $contact->self_house_number;
                    $contactaddress->street = $contact->self_street;
                    $contactaddress->email = $contact->self_email;
                    $contactaddress->phone = $contact->self_phone;

                    $contactaddress->contact_id = $contact->id;
                    $contactaddress->install_id = $contact->install_id;
                    $contactaddress->display = true;
                    $contactaddress->type = 2;

                    $contactaddress->save();

                    $this->user->standart_delivery = $cs->id;
                    $this->user->save();
                    $cs = $contactaddress;
                }
                $basket->setDelivery($cs->uuid);
            }
        }


        if ($basket->getInvoice() == 0) {

            if ($this->user->standart_invoice == 0) {

                $cs = Doctrine_Query::create()->select()->from('ContactAddress s')->where('s.contact_id = ? AND display = 1 AND type = 1', array($this->user->id))->limit(1)->fetchOne();
                if ($cs) {
                    $this->user->standart_invoice = $cs->id;
                    $this->user->save();
                } else {
                    $contact = $this->user;
                    $contactaddress = new ContactAddress();
                    $contactaddress->anrede = $contact->self_anrede;
                    $contactaddress->company = $contact->self_department;
                    $contactaddress->firstname = $contact->self_firstname;
                    $contactaddress->lastname = $contact->self_lastname;
                    $contactaddress->country = $contact->self_country;
                    $contactaddress->zip = $contact->self_zip;
                    $contactaddress->city = $contact->self_city;
                    $contactaddress->house_number = $contact->self_house_number;
                    $contactaddress->street = $contact->self_street;
                    $contactaddress->email = $contact->self_email;
                    $contactaddress->phone = $contact->self_phone;

                    $contactaddress->contact_id = $contact->id;
                    $contactaddress->install_id = $contact->install_id;
                    $contactaddress->display = true;
                    $contactaddress->type = 1;

                    $contactaddress->save();

                    $this->user->standart_delivery = $cs->id;
                    $this->user->save();
                    $cs = $contactaddress;
                }
                $basket->setInvoice($cs->uuid);
            } else {
                $cs = Doctrine_Query::create()->select()->from('ContactAddress s')->where('s.contact_id = ? AND display = 1 AND s.id = ?', array($this->user->id, $this->user->standart_invoice))->fetchOne();

                if ($cs) {
                    $basket->setInvoice($cs->uuid);
                } else {

                    $cs = Doctrine_Query::create()->select()->from('ContactAddress s')->where('s.contact_id = ? AND display = 1 AND type = 1', array($this->user->id))->limit(1)->fetchOne();
                    if ($cs) {
                        $this->user->standart_invoice = $cs->id;
                        $this->user->save();
                    } else {
                        $contact = $this->user;
                        $contactaddress = new ContactAddress();
                        $contactaddress->anrede = $contact->self_anrede;
                        $contactaddress->company = $contact->self_department;
                        $contactaddress->firstname = $contact->self_firstname;
                        $contactaddress->country = $contact->self_country;
                        $contactaddress->lastname = $contact->self_lastname;
                        $contactaddress->zip = $contact->self_zip;
                        $contactaddress->city = $contact->self_city;
                        $contactaddress->house_number = $contact->self_house_number;
                        $contactaddress->street = $contact->self_street;
                        $contactaddress->email = $contact->self_email;
                        $contactaddress->phone = $contact->self_phone;

                        $contactaddress->contact_id = $contact->id;
                        $contactaddress->install_id = $contact->install_id;
                        $contactaddress->display = true;
                        $contactaddress->type = 1;

                        $contactaddress->save();

                        $this->user->standart_delivery = $cs->id;
                        $this->user->save();
                        $cs = $contactaddress;
                    }
                    $basket->setInvoice($cs->uuid);
                }
            }
        } else {

            $cs = Doctrine_Query::create()->select()->from('ContactAddress s')->where('s.contact_id = ? AND display = 1 AND s.uuid = ?', array($this->user->id, $basket->getInvoice()))->fetchOne();

            if ($cs) {
                $basket->setInvoice($cs->uuid);
            } else {
                $cs = Doctrine_Query::create()->select()->from('ContactAddress s')->where('s.contact_id = ? AND display = 1 AND type = 1', array($this->user->id))->limit(1)->fetchOne();

                if ($cs) {
                    $this->user->standart_invoice = $cs->id;
                    $this->user->save();
                } else {
                    $contact = $this->user;
                    $contactaddress = new ContactAddress();
                    $contactaddress->anrede = $contact->self_anrede;
                    $contactaddress->company = $contact->self_department;
                    $contactaddress->firstname = $contact->self_firstname;
                    $contactaddress->lastname = $contact->self_lastname;
                    $contactaddress->country = $contact->self_country;
                    $contactaddress->zip = $contact->self_zip;
                    $contactaddress->city = $contact->self_city;
                    $contactaddress->house_number = $contact->self_house_number;
                    $contactaddress->street = $contact->self_street;
                    $contactaddress->email = $contact->self_email;
                    $contactaddress->phone = $contact->self_phone;

                    $contactaddress->contact_id = $contact->id;
                    $contactaddress->install_id = $contact->install_id;
                    $contactaddress->display = true;
                    $contactaddress->type = 1;

                    $contactaddress->save();

                    $this->user->standart_invoice = $cs->id;
                    $this->user->save();
                    $cs = $contactaddress;
                }
                $basket->setInvoice($cs->uuid);
            }
        }
        $this->view->sender = $basket->getSender();
        $this->view->invoice = $basket->getInvoice();
        $this->view->delivery = $basket->getDelivery();

        $this->view->sender_address = Doctrine_Query::create()->select()->from('ContactAddress s')->where('s.contact_id = ? AND display = 1 AND s.uuid = ?', array($this->user->id, $basket->getSender()))->fetchOne();
        $this->view->invoice_address = Doctrine_Query::create()->select()->from('ContactAddress s')->where('s.contact_id = ? AND display = 1 AND s.uuid = ?', array($this->user->id, $basket->getInvoice()))->fetchOne();
        $this->view->delivery_address = Doctrine_Query::create()->select()->from('ContactAddress s')->where('s.contact_id = ? AND display = 1 AND s.uuid = ?', array($this->user->id, $basket->getDelivery()))->fetchOne();

        if ($basket->getSender() == 0 || $this->view->sender_address == null) {

            $this->view->sender_address = Doctrine_Query::create()->select()->from('ContactAddress s')->where('s.contact_id = ? AND display = 1 AND s.type = ?', array($this->user->id, 3))->fetchOne();
            if ($this->view->sender_address == null) {
                $this->view->sender_address = $this->shop->getOrderSenderSavedAsObject();
            } else {
                $basket->setSender($this->view->sender_address->uuid);
                $this->view->sender = $basket->getSender();
            }
        }
        $this->view->sender_address_shop = $this->shop->getOrderSenderSavedAsObject();
        if ($this->_getParam('basketfield1')) {
            $basket->setBasketField1($this->_getParam('basketfield1'));
        }
        if ($this->_getParam('basketfield2')) {
            $basket->setBasketField2($this->_getParam('basketfield2'));
        }

        if ($this->getRequest()->getParam('sender') != '' && $this->getRequest()->getParam('sender') != 's') {
            $basket->setSenderIsSame(false);
            $basket->setSender($this->getRequest()->getParam('sender'));
        } elseif ($this->getRequest()->getParam('sender') != '' && $this->getRequest()->getParam('sender') == 's') {
            $basket->setSenderIsSame(true);
            $basket->setSender(0);
        }

        if ($this->getRequest()->getParam('delivery') != '' && $this->getRequest()->getParam('delivery') == 's') {
            if ($this->getRequest()->getParam('invoice', false)) {
                $basket->setDelivery($this->getRequest()->getParam('invoice', false));
                $basket->setInvoice($this->getRequest()->getParam('invoice', false));
            } else {
                $basket->setDelivery($basket->getInvoice());
                $basket->setInvoice($basket->getInvoice());
            }

            $basket->setDeliveryIsSame(true);
            //$basket->setSender(0);
            $this->redirectSpeak('/basket/finish');
        } elseif (
            $this->getRequest()->getParam('delivery') != '' && $this->getRequest()->getParam('delivery') != 's' &&
            $this->getRequest()->getParam('sender') != '' && $this->getRequest()->getParam('invoice') != ''
        ) {

            $basket->setDeliveryIsSame(false);
            $basket->setDelivery($this->getRequest()->getParam('delivery'));
            $basket->setInvoice($this->getRequest()->getParam('invoice'));
            $basket->setSender($this->getRequest()->getParam('sender'));
            $this->redirectSpeak('/basket/finish');
        } elseif ($this->getRequest()->getParam('delivery') != '' && $this->getRequest()->getParam('delivery') != 's') {
            $basket->setDeliveryIsSame(false);
            $basket->setDelivery($this->getRequest()->getParam('delivery'));
            $this->redirectSpeak('/basket/finish');
        }

        $this->view->senderIsSame = $basket->getSenderIsSame();
        $this->view->deliveryIsSame = $basket->getDeliveryIsSame();

        if ($this->view->delivery_address) {
            $basket->setDeliveryPLZ($this->view->delivery_address->zip);
        }

        if (file_exists($this->_configPath . '/user/address.ini')) {

            $config = new Zend_Config_Ini($this->_configPath . '/user/address.ini', 'add');
            if ($config->global) {
                $form = new EasyBib_Form($config->user);
            } else {
                $form = new Zend_Form($config->user);
            }

            $form->removeElement('update');
            $form->removeElement('del');

            $countrys = TP_Country::getCountrysAsArray();

            if (isset($form->country)) {
                if (!$form->country->getMultiOptions()) {
                    $form->country->addMultiOptions($countrys);
                }
                $form->country->setValue("DE");
            }

            if (!$this->shop->display_sender && isset($form->type) && $form->type instanceof Zend_Form_Element_MultiCheckbox) {
                $form->type->removeMultiOption('3');
            }


            if (!$this->shop->display_delivery && isset($form->type) && $form->type instanceof Zend_Form_Element_MultiCheckbox) {
                $form->type->removeMultiOption('2');
            }

            if ($this->_request->isPost()) {

                $formData = $this->getRequest()->getParams();

                if ($form->isValid($formData)) {

                    $contactaddress = new ContactAddress();

                    $contactaddress->anrede = 1;
                    $contactaddress->email = "";
                    $contactaddress->mobil_phone = "";
                    $contactaddress->company = "";
                    if (isset($formData['anrede'])) {
                        $contactaddress->anrede = $formData['anrede'];
                    }
                    if (isset($formData['mobil_phone'])) {
                        $contactaddress->mobil_phone = $formData['mobil_phone'];
                    }
                    if (isset($formData['email'])) {
                        $contactaddress->email = $formData['email'];
                    }
                    if (isset($formData['country'])) {
                        $contactaddress->country = $formData['country'];
                    }
                    if (isset($formData['company'])) {
                        $contactaddress->company = $formData['company'];
                    }
                    if (isset($formData['company2'])) {
                        $contactaddress->company2 = $formData['company2'];
                    }
                    if (isset($formData['kostenstellung'])) {
                        $contactaddress->kostenstellung = $formData['kostenstellung'];
                    }
                    $contactaddress->firstname = $formData['firstname'];
                    $contactaddress->lastname = $formData['lastname'];
                    $contactaddress->zip = $formData['zip'];
                    $contactaddress->city = $formData['city'];
                    $contactaddress->house_number = $formData['house_number'];
                    $contactaddress->street = $formData['street'];
                    $contactaddress->phone = $formData['phone'];
                    $contactaddress->display = true;
                    if ($mode->over_ride_contact != false && $mode->over_ride_contact != 'new') {
                        $contactaddress->contact_id = $mode->over_ride_contact;
                    } else {
                        $contactaddress->contact_id = $this->user->id;
                    }
                    $contactaddress->install_id = $this->user->install_id;
                    $contactaddress->display = true;

                    if (!$this->_getParam('uuid')) {
                        if (in_array('1', $formData['type'])) {
                            $cs = $contactaddress->copy();
                            $cs->uuid = "";
                            $cs->type = 1;
                            $cs->save();
                            $basket->setInvoice($cs->uuid);
                        }
                        if (in_array('2', $formData['type'])) {
                            $cs = $contactaddress->copy();
                            $cs->uuid = "";
                            $cs->type = 2;
                            $cs->save();
                            $basket->setDelivery($cs->uuid);
                        }
                        if (in_array('3', $formData['type'])) {
                            $cs = $contactaddress->copy();
                            $cs->uuid = "";
                            $cs->type = 3;
                            $cs->save();
                            $basket->setSender($cs->uuid);
                        }
                    } else {
                        $contactaddress->save();
                    }

                    $this->view->priorityMessenger('Adresse erfolgreich gespeichert', 'success');
                    $this->redirectSpeak('/basket/review');
                }
            }

            if ($config->global) {
                $editform = new EasyBib_Form($config->user);
            } else {
                $editform = new Zend_Form($config->user);
            }
            $editform->setAttrib('id', 'updateaddress');
            $editform->setName('updateaddress');
            $editform->removeElement('submit');
            $editform->removeElement('type');
            $editform->removeElement('del');
            $countrys = TP_Country::getCountrysAsArray();

            if (isset($form->country)) {
                if (!$editform->country->getMultiOptions()) {
                    $editform->country->addMultiOptions($countrys);
                }
                $editform->country->setValue("DE");
                if (isset($editform->anrede)) {
                    $editform->anrede->setAttrib('id', 'update_anrede');
                }
                $editform->country->setAttrib('id', 'update_country');
            }
            $this->view->editform = $editform;
        } else {
            $config = new Zend_Config_Ini($this->_configPath . '/user/registercontact.ini', 'register');
            $form = new Zend_Form();
            $form->addAttribs(array(
                'class' => 'niceform',
                'id' => 'userreg'
            ));
            $form->addSubForm(new Zend_Form_SubForm($config->user->liefer), 'liefer');
            $form->addElement('submit', 'submit', $config->user->liefersubmit->elements->submit->options);
            if ($this->_request->isPost()) {
                $formData = $this->_request->getPost();
                if ($form->isValid($formData) && $this->_getParam('name') != 'Benutzername') {
                    $contactaddress = new ContactAddress();
                    if (isset($formData['liefer']['anrede'])) {
                        $contactaddress->anrede = $formData['liefer']['anrede'];
                    }
                    $contactaddress->firstname = $formData['liefer']['firstname'];
                    $contactaddress->lastname = $formData['liefer']['lastname'];
                    $contactaddress->zip = $formData['liefer']['zip'];
                    $contactaddress->city = $formData['liefer']['city'];
                    $contactaddress->house_number = $formData['liefer']['house_number'];
                    $contactaddress->street = $formData['liefer']['street'];
                    $contactaddress->email = $formData['liefer']['email'];
                    $contactaddress->phone = $formData['liefer']['phone'];
                    $contactaddress->company = $formData['liefer']['company'];
                    if ($mode->over_ride_contact != false && $mode->over_ride_contact != 'new') {
                        $contactaddress->contact_id = $mode->over_ride_contact;
                    } else {
                        $contactaddress->contact_id = $this->user->id;
                    }
                    $contactaddress->install_id = $this->user->install_id;
                    $contactaddress->display = true;
                    $contactaddress->type = 2;
                    $contactaddress->save();
                    $basket->setDeliveryIsSame(false);
                    $basket->setDelivery($contactaddress->uuid);
                    $this->redirectSpeak('/basket/finish');
                } else {
                    $form->populate($formData);
                }
            }
        }

        $this->view->form = $form;
    }

    public function finishAction()
    {


        if (trim($this->shop->custom_agb) != "") {
            $this->view->agb_text = $this->shop->custom_agb;
        } else {
            $this->view->agb_text = $this->install->agb;
        }

        if (Zend_Auth::getInstance()->hasIdentity()) {
            if ($this->getRequest()->getParam('error') != "") {
                $this->view->priorityMessenger($this->getRequest()->getParam('error'), 'error');
            }
            $basket = new TP_Basket();

            if ($basket->getDelivery() != "") {
                $delivery_address = Doctrine_Query::create()->select()->from('ContactAddress s')->where('s.contact_id = ? AND s.uuid = ?', array($this->user->id, $basket->getDelivery()))->fetchOne();
                $basket->setDeliveryPLZ($delivery_address->zip);
                $shippingtype = Doctrine_Query::create()->select()->from('Shippingtype s')->where('s.id = ?', array($basket->getShippingtype()))->fetchOne();

                if ($shippingtype->xml != "") {
                    $versand = $shippingtype->calcVersand($basket->getPreisNetto(), $basket->getWeight(), $delivery_address->country, $this->user);
                    $pos_article_versand = false;
                    $articles = $basket->getAllArtikel();
                    foreach ($articles as $uuid => $art) {
                        /** @var Article $article */
                        $article = Doctrine_Query::create()->from('Article c')->where('c.uuid = ?', array(
                            $art->getArticleId()
                        ))->fetchOne();

                        if ($article['versand'] == 'Fix') {
                            if (!$pos_article_versand || $pos_article_versand != $article->id) {
                                $versand = $versand + $article['versandwert'];
                                $pos_article_versand = $article->id;
                            }
                        } elseif ($article['versand'] == 'Position') {
                        }
                    }

                    if ($versand != $basket->getVersandkosten() && $this->install->id != 17) {
                        $mwert = $basket->getPreisSteuer();

                        $mwert = $mwert - ($basket->getVersandkosten() / 100 * str_replace('%', '', $shippingtype['mwert']));

                        $mwertalle = $basket->getMWert();
                        $mwertalle[str_replace('%', '', $shippingtype['mwert'])] = $mwertalle[str_replace('%', '', $shippingtype['mwert'])] - ($basket->getVersandkosten() / 100 * str_replace('%', '', $shippingtype['mwert']));

                        $mwert = $mwert + ($versand / 100 * str_replace('%', '', $shippingtype['mwert']));
                        $mwertalle[str_replace('%', '', $shippingtype['mwert'])] = $mwertalle[str_replace('%', '', $shippingtype['mwert'])] + ($versand / 100 * str_replace('%', '', $shippingtype['mwert']));

                        $basket->setPreisBrutto(($basket->getPreisNetto() + $versand + $mwert + $basket->getZahlkosten() - ($basket->getGutscheinAbzug() * 1)));
                        $basket->setVersandkosten($versand);
                        $basket->setPreisSteuer($mwert);
                        $basket->setMWert($mwertalle);
                        $basket->setVersandBrutto($versand + ($versand / 100 * str_replace('%', '', $shippingtype['mwert'])));
                        $this->view->priorityMessenger('Versandpreis hat sich geändert', 'info');
                    }
                }
            }

            if ($basket->getGutschein() != "") {
                $gutscheincode = Doctrine_Query::create()->from('CreditSystemDetail c')->where('c.shop_id = ? AND c.uuid = ? AND c.contact_id = ?', array(
                    $this->shop->id,
                    $basket->getGutschein(),
                    $this->user->id
                ))->fetchOne();

                if ($gutscheincode && $gutscheincode->used == true && $gutscheincode->CreditSystem->more == false) {
                    $this->view->priorityMessenger('Gutschein nicht mehr gültig', 'error');
                    $this->redirect('/basket');
                    die();
                } elseif ($gutscheincode && $gutscheincode->CreditSystem->more == true) {
                    $gutscheincode = Doctrine_Query::create()->from('CreditSystemDetail c')->where('c.contact_id = ? AND c.uuid = ?', array(
                        $this->user->id,
                        $basket->getGutschein()
                    ))->fetchOne();

                    if ($gutscheincode && $this->install->id != 34) {
                        $this->view->priorityMessenger('Gutschein nicht mehr gültig', 'error');
                        $this->redirect('/basket');
                        die();
                    }
                }
            }

            if ($this->install->only_de) {
                $deliveryAddress = Doctrine_Query::create()->from('ContactAddress a')->where('uuid = ?', array(
                    $basket->getDelivery()
                ))->fetchOne();

                if (strtolower($deliveryAddress->country) != "de") {
                    $this->view->priorityMessenger('Lieferung nur innerhalb Deutschlands möglich', 'error');
                    $this->redirectSpeak('/basket/review');
                }
            }

            if ($this->_getParam('basketfield1')) {
                $basket->setBasketField1($this->_getParam('basketfield1'));
            }
            if ($this->_getParam('basketfield2')) {
                $basket->setBasketField2($this->_getParam('basketfield2'));
            }

            if (($this->getRequest()->getParam('company', 1) && $this->getRequest()->getParam('payment', 1) && $this->getRequest()->getParam('person', 1) && $this->getRequest()->getParam('revocation', 1) && $this->getRequest()->getParam('agb') == '1' && $this->getRequest()->getParam('privacy', 1)) || $this->getRequest()->getParam('hash') == 'Ja' || $this->getRequest()->getParam('Data') != '' ||
                ($this->getRequest()->getParam('token') != "" && $this->getRequest()->getParam('token') == $basket->getToken())
            ) {

                $user = Zend_Auth::getInstance()->getIdentity();
                $mode = new Zend_Session_Namespace('adminmode');
                if ($mode->over_ride_contact != false && $mode->over_ride_contact != 'new') {
                    $user = Doctrine_Query::create()->from('Contact m')->where('id = ?')->fetchOne(array(
                        $mode->over_ride_contact
                    ));
                } else {
                    $user = Doctrine_Query::create()->from('Contact m')->where('id = ?')->fetchOne(array(
                        $user['id']
                    ));
                }

                if ($this->_getParam('basketfield1')) {
                    $basket->setBasketField1($this->_getParam('basketfield1'));
                }
                if ($this->_getParam('basketfield2')) {
                    $basket->setBasketField2($this->_getParam('basketfield2'));
                }
                if ($basket->getAllCount() == 0) {
                    $this->redirectSpeak('/');
                    return;
                }

                TP_Queue::process('paymenttype', 'global', $basket);

                TP_Payment::process($basket);

                $order = new Orders();
                $order->Shop = $this->shop;
                $order->Account = $user->Account;
                $order->created = date('Y-m-d H:i:s');
                $order->updated = date('Y-m-d H:i:s');
                $order->Install = $user->Install;
                $order->enable = 1;

                $m = TP_Mongo::getInstance();
                $obj = $m->Shop->findOne(array('uid' => $this->shop->id));

                if ($obj !== NULL && $obj['ownNumber']) {

                    $templates = array('template' => $obj['numberPattern']);
                    $twig = new \Twig\Environment(new \Twig\Loader\ArrayLoader($templates));
                    $order->alias = $twig->render('template', array('number'=> $obj['numberStart']));
                    $obj['numberStart'] +=1;
                    $m->Shop->updateOne(array('uid' => $this->shop->id), [ '$set' =>  $obj]);

                }else{
                    $obj = $m->Instance->findOne(array('appId' => "1"));
                    $install = $this->shop->Install;

                    if ($obj == null) {
                        $obj = [];
                        $obj['appId'] = "1";
                        $obj['numberStart'] = $install->order_number;

                        if ($install->order_number_date) {
                            $obj['numberPattern'] = '{{ "now"|date("Ym") }}-{{number}}';
                        } else {
                            $obj['numberPattern'] = '{{number}}';
                        }

                        $m->Instance->updateOne(array('appId' => "1"), [ '$set' =>  $obj]);
                    } else {
                        if (!isset($obj['numberStart']) || $obj['numberStart'] == '') {
                            $obj['numberStart'] = $install->order_number;
                        }
                        if (!isset($obj['numberPattern']) || $obj['numberPattern'] == '') {
                            if ($install->order_number_date) {
                                $obj['numberPattern'] = '{{ "now"|date("Ym") }}-{{number}}';
                            } else {
                                $obj['numberPattern'] = '{{number}}';
                            }
                        }

                        $m->Instance->updateOne(array('appId' => "1"), [ '$set' =>  $obj]);
                    }
                    $templates = array('template' => $obj['numberPattern']);
                    $twig = new \Twig\Environment(new \Twig\Loader\ArrayLoader($templates));
                    $order->alias = $twig->render('template', array('number' => $obj['numberStart']));
                    $obj['numberStart'] += 1;
                    $m->Instance->updateOne(array('appId' => "1"), [ '$set' =>  $obj]);
                }



                $order->Contact = $user;

                $order->gutschein = $basket->getGutschein();
                $order->gutscheinabzug = $basket->getGutscheinAbzug();
                $order->gutscheinabzugtyp = $basket->getGutscheinAbzugTyp();

                $order->basketfield1 = $basket->getBasketField1();
                $order->basketfield2 = $basket->getBasketField2();
                $order->delivery_same = $basket->getDeliveryIsSame();
                $order->sender_same = $basket->getSenderIsSame();
                $order->info = json_encode($basket->getInfo());
                if (Zend_Registry::isRegistered('birch_Server')) {
                    $order->file1 = Zend_Registry::get('birch_Server');
                }

                if ($this->_getParam("createNewDeliveryAddress", false)) {
                    $contactaddress = new ContactAddress();
                    $contactaddress->anrede = $user->self_anrede;
                    $contactaddress->company = $user->self_department;
                    $contactaddress->country = $user->self_country;
                    $contactaddress->firstname = $user->liefer_firstname;
                    $contactaddress->lastname = $user->liefer_lastname;
                    $contactaddress->zip = $user->self_zip;
                    $contactaddress->city = $user->self_city;
                    $contactaddress->house_number = $user->self_house_number;
                    $contactaddress->street = $user->self_street;
                    $contactaddress->email = $user->email;
                    $contactaddress->phone = $user->self_phone;
                    $contactaddress->position = $user->self_position;

                    $contactaddress->zusatz1 = $user->street;
                    $contactaddress->zusatz2 = $user->phone_alternative;

                    $contactaddress->ustid = $user->ustid;

                    $contactaddress->contact_id = $user->id;
                    $contactaddress->install_id = $user->install_id;
                    $contactaddress->display = true;
                    $contactaddress->type = 2;

                    $contactaddress->save();

                    $basket->setDelivery($contactaddress->uuid);
                }

                $order->mongoLoaded = true;
                if ($basket->getDelivery() != "") {
                    $contactaddress = Doctrine_Query::create()->from('ContactAddress c')->where('c.uuid = ?', array(
                        $basket->getDelivery()
                    ))->fetchOne();
                    $order->delivery_address = $contactaddress->id;
                    if ($contactaddress) {
                        $order->setDeliveryAddressSaved($contactaddress->getOrderSaveArray());
                    }
                }
                if ($basket->getInvoice() != "") {
                    $contactaddress = Doctrine_Query::create()->from('ContactAddress c')->where('c.uuid = ?', array(
                        $basket->getInvoice()
                    ))->fetchOne();
                    $order->invoice_address = $contactaddress->id;

                    if ($contactaddress) {
                        $order->setInvoiceAddressSaved($contactaddress->getOrderSaveArray());
                    }
                }
                if ($basket->getSender() != "") {
                    $contactaddress = Doctrine_Query::create()->from('ContactAddress c')->where('c.uuid = ?', array(
                        $basket->getSender()
                    ))->fetchOne();
                    $order->sender_address = $contactaddress->id;

                    if ($contactaddress) {
                        $order->setSenderAddressSaved($contactaddress->getOrderSaveArray());
                    }
                }
                if (($basket->getDelivery() === "s" || $basket->getDelivery() === 0 || $basket->getDelivery() === "" || $basket->getDelivery() === "0") && $basket->getInvoice() != "") {
                    $order->setDeliveryAddressSaved($order->getInvoiceAddressSaved());
                }
                if (($basket->getInvoice() === "s" || $basket->getInvoice() === 0 || $basket->getInvoice() === "" || $basket->getInvoice() === "0") && $basket->getDelivery() != "") {
                    $order->setInvoiceAddressSaved($order->getDeliveryAddressSaved());
                }
                if ($basket->getSender() === "s" || $basket->getSender() === 0 || $basket->getSender() == "" || $basket->getSender() === "0") {
                    $order->setSenderAddressSaved($this->shop->getOrderSenderSaveArray());
                }

                $withTax = TP_Country::getTaxForCountry($basket->getDeliveryCountry(), $order->getDeliveryAddressSaved()['ustid']);

                $order->status = 10;

                $order->use_account_as_invoice = $user->Account->use_account_as_invoice;

                $order->preis = ($basket->getPreisNetto() + $basket->getZahlkosten() + $basket->getVersandkosten());
                $order->preissteuer = $basket->getPreisSteuer();
                $order->preisbrutto = $basket->getPreisBrutto();
                $order->paymenttype_id = $basket->getPaymenttype();
                $order->shippingtype_id = $basket->getShippingtype();
                $order->shippingtype_extra_label = $basket->getShippingtypeExtraLabel();
                $order->zahlkosten = $basket->getZahlkosten();
                $order->versandkosten = $basket->getVersandkosten();
                $order->lang = strtolower(Zend_Registry::get('locale')->getLanguage());
                $order->mwertalle = Zend_Json::encode($basket->getMWert());

                if (!$withTax) {
                    $order->preissteuer = 0;
                    $order->preisbrutto = $order->preis;
                    $order->mwertalle = Zend_Json::encode([]);
                }

                if ($this->shop->uid == '0001-557e6b9d-531afcb3-9700-4e8b7d8a') {

                    $budget = json_decode($user->self_information, true);

                    $budget['budget_all'] = $budget['budget_all'] + $order->preis;
                    $budget['budget_mon'] = $budget['budget_mon'] + $order->preis;

                    $user->self_information = json_encode($budget);
                    $user->save();
                }

                if ($basket->getGutschein() != "") {
                    $gutscheincode = Doctrine_Query::create()->from('CreditSystemDetail c')->where('c.shop_id = ? and c.uuid = ?', array(
                        $this->shop->id,
                        $basket->getGutschein()
                    ))->fetchOne();

                    if(!$gutscheincode) {
                        $gutscheincode = Doctrine_Query::create()->from('CreditSystem a')->where('a.shop_id = ? AND a.pre_code = ? AND (a.product_id = 0 OR a.product_id IS NULL) AND (a.articlegroup_id = 0 OR a.articlegroup_id IS NULL) AND a.f <= ? AND a.t >= ?', array(
                            $this->shop->id,
                            $basket->getGutschein(), date('Y-m-d'), date('Y-m-d')
                        ))->fetchOne();

                    }elseif($gutscheincode) {
                        $gutscheincode = $gutscheincode->CreditSystem;
                    }


                    if ($gutscheincode) {
                        $gcDetail = new CreditSystemDetail();
                        $gcDetail->install_id = $this->install->id;
                        $gcDetail->creditsystem_id = $gutscheincode->id;
                        $gcDetail->uuid = $basket->getGutschein();
                        $gcDetail->shop_id = $this->shop->id;
                        $gcDetail->used = true;
                        $gcDetail->contact_id = $user->id;
                        $gcDetail->save();
                    }
                }
                $order->save();
                $order->setPaymentRef($basket->getPaymentRef());
                $order->setPaymentToken($basket->getToken());
                $order->setPackageExported(!$this->shop->isCreatePackageAfterBuy());
                $order->setSendDataToShipping((bool)$this->_getParam('sendDataToShipping', 1));
                $order->setWithTax($withTax);
                $order->setGutscheinAbzugNetto($basket->getGutscheinAbzugNetto());

                $order->saveMongo();


                $basePath = APPLICATION_PATH . '/../market/basket/' . $order->id . '/';

                if (!file_exists($basePath)) {
                    mkdir($basePath, 0777, true);
                }

                $adapter = new Zend_File_Transfer_Adapter_Http();

                $adapter->setDestination($basePath);

                $adapter->addFilter('Rename', array('target' => $basePath . $adapter->getFileName(null, false), 'overwrite' => true));

                if ($adapter->receive() && $adapter->isValid()) {
                    $order->file1 = $adapter->getFileName(null, false);
                    $order->save();
                }



                $posId = 1;

                /** @var TP_Basket_Item $artikel */
                foreach ($basket->getAllArtikel() as $artikel) {
                    $article = Doctrine_Query::create()->from('Article c')->where('c.uuid = ?', array(
                        $artikel->getArticleId()
                    ))->fetchOne();

                    $art = new Orderspos();
                    $art->Shop = $this->shop;
                    $art->Account = $user->Account;
                    $art->Install = $user->Install;
                    $art->createdd = date('Y-m-d');
                    $art->updatedd = date('Y-m-d');
                    $art->createdt = date('H:i');
                    $art->updatedt = date('H:i');
                    $art->orders_id = $order->id;
                    $art->pos = $posId;
                    $posId++;
                    $art->layouter_mode = 0;
                    $layouter_mode = false;

                    $art->status = $article->init_status;


                    $art->weight = $artikel->getWeight();

                    if (($article->typ == 8 || $article->typ == 6) &&
                        ($article->upload_article || $article->upload_multi || $article->upload_center ||
                            $article->upload_post || $article->upload_email || $article->upload_weblayouter ||
                            $article->upload_templateprint || $article->upload_steplayouter || $article->upload_steplayouter2 || $article->upload_collecting_orders  || $article->isUploadProvided())
                    ) {

                        $status = $artikel->getUploadMode();
                        switch ($status) {
                            case "center":
                                $art->status = $article->upload_center_status;
                                break;
                            case "weblayouter":
                                $art->status = $article->upload_weblayouter_status;
                                break;
                            case "steplayouter":
                                if ($art->layouter_mode == 2) {
                                    $art->status = $article->upload_templateprint_status;
                                }
                                if ($art->layouter_mode == 4) {
                                    $art->status = $article->upload_steplayouter2_status;
                                }
                                break;
                            case "steplayouter2":
                                if ($art->layouter_mode == 2) {
                                    $art->status = $article->upload_templateprint_status;
                                }
                                if ($art->layouter_mode == 4) {
                                    $art->status = $article->upload_steplayouter2_status;
                                }
                                break;
                            case "post":
                                $art->status = $article->upload_post_status;
                                break;
                            case "email":
                            case "mail":
                                $art->status = $article->upload_email_status;
                                break;
                            case "fromLatestOrder":
                                $art->status = $article->getUploadFromLatestOrderInitalStatus();
                                break;
                            case "provided":
                                $art->status = $article->getUploadProvidedInitalStatus();
                                $article->copyProvidedFileToBasket($art);
                                break;
                            case "collecting_orders":
                                $art->status = $article->upload_collecting_orders_status;
                                break;
                            case "article":
                                $art->status = $article->upload_article_status;
                                break;
                        }
                    }

                    if ($article->typ == 6 && $artikel->getLayouterId() != "") {
                        $articleSession = new TP_Layoutersession();

                        $articleSess = $articleSession->getLayouterArticle($artikel->getLayouterId());
                        $art->layouter_mode = 4;
                        if ($articleSess->getLayouterModus() == 2) {
                            $articleSess->copyToBasketStepLayouter($order, $art, $articleSess->getArticleId());
                            $art->layouter_mode = 3;
                            $art->a9_designer_data = $articleSess->getDesignerXML();
                        } elseif ($articleSess->getLayouterModus() == 8) {
                            $art->layouter_mode = 8;
                        } elseif ($articleSess->getLayouterModus() == 11) {
                            $art->layouter_mode = 11;
                            $art->status = $article->getPluginSettings('pitchprint', 'uploadPitchprintInitalStatus');
                        } elseif ($articleSess->getTemplatePrintId() != "") {
                            $art->layouter_mode = 2;
                            $articleSess->copyToBasket($order, $art, $articleSess->getTemplatePrintId());
                        }
                        if ($articleSess->getLayouterModus() == 5) {
                            $art->layouter_mode = 5;
                            $contentCheck = json_decode(file_get_contents("http://localhost/apps/psc/component/configurationlayouter/store/check/" . $artikel->getLayouterId()), true);


                            if ($contentCheck['found'] == true) {
                                if (isset($contentCheck['data']['products'])) {
                                    foreach ($contentCheck['data']['products'] as $art) {

                                        $articleSet = Doctrine_Query::create()
                                            ->from('Article c')
                                            ->where('c.id = ?', array($art['uid']))
                                            ->fetchOne();
                                        if ($articleSet) {
                                            if ($articleSet->stock) {
                                                $articleSet->stock_count = $articleSet->stock_count - $artikel->getAuflage();
                                                $articleSet->save();

                                                if ($articleSet->set_config != "") {

                                                    $setConfig = array();

                                                    foreach ($articleSet->getSetProducts() as $setProduct) {


                                                        $anzahl = $artikel->getAuflage();
                                                        if ($setProduct->stock_count < $anzahl) {
                                                            $anzahl = $setProduct->stock_count;
                                                            $setProduct->stock_count = 0;
                                                            $setProduct->save();
                                                            $setConfig[$setProduct->id] = $anzahl;
                                                        } else {
                                                            $setProduct->stock_count = $setProduct->stock_count - $anzahl;
                                                            $setProduct->save();
                                                        }

                                                        $sb = new Stockbooking();
                                                        $sb->uid = TP_Util::uuid();
                                                        $sb->contact_id = $user->id;
                                                        $sb->delivery = $order->alias;
                                                        $sb->description = "Bestellung";
                                                        $sb->amount = $anzahl * -1;
                                                        $sb->created = $order->created;
                                                        $sb->product_id = $setProduct->id;
                                                        $sb->save();



                                                        if ($setProduct->stock_count <= $setProduct->stock_count_min) {
                                                            TP_Queue::process('articlestockmin', 'global', $setProduct);
                                                        }
                                                    }

                                                    $art->calc_xml = json_encode($setConfig);
                                                }

                                                $sb = new Stockbooking();
                                                $sb->uid = TP_Util::uuid();
                                                $sb->contact_id = $user->id;
                                                $sb->delivery = $order->alias;
                                                $sb->description = "Bestellung";
                                                $sb->amount = $artikel->getAuflage() * -1;
                                                $sb->created = $order->created;
                                                $sb->product_id = $articleSet->id;
                                                $sb->save();



                                                if ($articleSet->stock_count <= $articleSet->stock_count_min) {
                                                    TP_Queue::process('articlestockmin', 'global', $articleSet);
                                                }
                                                if ($articleSet->stock_max_buy > 0 && $artikel->getCount() > $articleSet->stock_max_buy) {
                                                    TP_Queue::process('maxstockbuy', 'global', array(
                                                        'article' => $articleSet,
                                                        'count' => $artikel->getAuflage(),
                                                        'order' => $order,
                                                        'contact' => $user
                                                    ));
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    $orgid = $article->id;
                    if (($article->typ == 6 || $article->typ == 8 || $article->typ == 9) && $artikel->getLayouterId() != "" && !$article->not_edit) {

                        Zend_Registry::set('target_shop', $this->shop->uid);
                        $uuid = $article->uuid;
                        if ($article->a6_org_article != 0) {
                            $orgid = $article->a6_org_article;
                        } else {
                            $orgid = $article->id;
                        }
                        $article = $article->copy();

                        $article->uuid = "";
                        $article->a6_org_article = $orgid;
                        $article->contact_id = $user->id;
                        $article->private = true;
                        $article->url = $article->url . '_'.time();
                        $article->file = "";
                        $article->file1 = "";
                        $article->render_new_preview_image = true;
                        $article->render_new_preview_gallery = true;
                        $article->render_new_preview_pdf = true;
                        if ($artikel->getRef() != "") {
                            $article->title = $article->title . ' ' . $artikel->getRef();
                        }
                        $article->save();

                        $articles = Zend_Registry::get('articles');

                        $articleObj = new $articles[$article->typ]();

                        $layouter_mode = true;

                        $art->render_print = true;

                        if (method_exists($articleObj, 'copyPreDispatch')) {
                            $articleObj->copyPreDispatch($article, $artikel->getLayouterId(), $art);
                        }
                    } elseif (($article->typ == 6 || $article->typ == 8) && $article->not_edit) {

                        $art->render_print = true;
                        $articles = Zend_Registry::get('articles');
                        $articleObj = new $articles[$article->typ]();
                    }

                    if (file_exists(APPLICATION_PATH . '/../market/templateprint/basket/' . $art->orders_id . '/' . $art->pos . '/product.zip')) {
                        $path = str_split($article->id);
                        if (!file_exists(APPLICATION_PATH . '/../market/templateprint/orginal/' . implode('/', $path) . '')) {
                            $an = '';
                            foreach ($path as $pt) {
                                $an .= $pt . '/';
                                if (!file_exists(APPLICATION_PATH . '/../market/templateprint/orginal/' . $an)) {
                                    mkdir(APPLICATION_PATH . '/../market/templateprint/orginal/' . $an);
                                }
                            }
                        }

                        if (file_exists(APPLICATION_PATH . '/../market/templateprint/basket/' . $art->orders_id . '/' . $art->pos . '/preview.pdf')) {
                            copy(APPLICATION_PATH . '/../market/templateprint/basket/' . $art->orders_id . '/' . $art->pos . '/preview.pdf', APPLICATION_PATH . '/../market/templateprint/orginal/' . implode('/', $path) . '/preview.pdf');
                        }
                        if (file_exists(APPLICATION_PATH . '/../market/templateprint/basket/' . $art->orders_id . '/' . $art->pos . '/product.zip')) {
                            copy(APPLICATION_PATH . '/../market/templateprint/basket/' . $art->orders_id . '/' . $art->pos . '/product.zip', APPLICATION_PATH . '/../market/templateprint/orginal/' . implode('/', $path) . '/product.zip');
                        }
                        if (file_exists(APPLICATION_PATH . '/../market/templateprint/basket/' . $art->orders_id . '/' . $art->pos . '/final.pdf')) {
                            copy(APPLICATION_PATH . '/../market/templateprint/basket/' . $art->orders_id . '/' . $art->pos . '/final.pdf', APPLICATION_PATH . '/../market/templateprint/orginal/' . implode('/', $path) . '/final.pdf');
                        }
                    }

                    $art->calc_xml = $article->a1_xml;

                    if ($article->stock) {
                        $article->stock_count = $article->stock_count - $artikel->getAuflage();
                        $article->save();

                        if ($article->set_config != "") {

                            $setConfig = array();

                            foreach ($article->getSetProducts() as $setProduct) {

                                if ($setProduct->stock) {
                                    $anzahl = $artikel->getAuflage();
                                    if ($setProduct->stock_count < $anzahl) {
                                        $anzahl = $setProduct->stock_count;
                                        $setProduct->stock_count = 0;
                                        $setProduct->save();
                                        $setConfig[$setProduct->id] = $anzahl;
                                    } else {
                                        $setProduct->stock_count = $setProduct->stock_count - $anzahl;
                                        $setProduct->save();
                                    }

                                    $sb = new Stockbooking();
                                    $sb->uid = TP_Util::uuid();
                                    $sb->contact_id = $user->id;
                                    $sb->delivery = $order->alias;
                                    $sb->description = "Bestellung";
                                    $sb->amount = $anzahl * -1;
                                    $sb->created = $order->created;
                                    $sb->product_id = $setProduct->id;
                                    $sb->save();


                                    if ($setProduct->stock_count <= $setProduct->stock_count_min) {
                                        TP_Queue::process('articlestockmin', 'global', $setProduct);
                                    }
                                }
                            }

                            $art->calc_xml = json_encode($setConfig);
                        }

                        $sb = new Stockbooking();
                        $sb->uid = TP_Util::uuid();
                        $sb->contact_id = $user->id;
                        $sb->delivery = $order->alias;
                        $sb->description = "Bestellung";
                        $sb->amount = $artikel->getAuflage() * -1;
                        $sb->created = $order->created;
                        $sb->product_id = $article->id;
                        $sb->save();



                        if ($article->stock_count <= $article->stock_count_min) {
                            TP_Queue::process('articlestockmin', 'global', $article);
                            $dbMongo = TP_Mongo::getInstance();
                            $dbMongo->Job->insertOne(array(
                                "shop" =>  $this->shop->id,
                                "event" => "product_stock_min",
                                "data" => ["product" => $article->uuid],
                                "created" => new MongoDB\BSON\UTCDateTime(),
                                "updated" => new MongoDB\BSON\UTCDateTime()
                            ));
                        }
                        if ($article->stock_max_buy > 0 && $artikel->getCount() > $article->stock_max_buy) {
                            TP_Queue::process('maxstockbuy', 'global', array(
                                'article' => $article,
                                'count' => $artikel->getAuflage(),
                                'order' => $order,
                                'contact' => $user
                            ));
                            $dbMongo = TP_Mongo::getInstance();
                            $dbMongo->Job->insertOne(array(
                                "shop" =>  $this->shop->id,
                                "event" => "product_stock_buy_max",
                                "data" => ["product" => $article->uuid],
                                "created" => new MongoDB\BSON\UTCDateTime(),
                                "updated" => new MongoDB\BSON\UTCDateTime()
                            ));
                        }
                    }

                    $motive = $artikel->getMotive();
                    if (!empty($motive)) {
                        Doctrine_Query::create()->update('Motiv')->set('used', 'used + 1')->where('id IN (' . implode(',', array_keys($artikel->getMotive())) . ')')->execute();
                    }
                    $article->used = $article->used + 1;
                    $article->save();

                    

                    $art->count = $artikel->getCount();
                    $art->article_id = $article->id;
                    $art->priceone = $artikel->getNetto();
                    $art->priceonesteuer = $artikel->getSteuer();
                    $art->priceonebrutto = $artikel->getBrutto();
                    $art->priceall = $artikel->getNetto() * $artikel->getCount();
                    $art->priceallsteuer = $artikel->getSteuer() * $artikel->getCount();
                    $art->priceallbrutto = $artikel->getBrutto() * $artikel->getCount();

                    if (!$withTax) {
                        $art->priceonesteuer = 0;
                        $art->priceonebrutto = $art->priceonesteuer;
                        $art->priceallsteuer = 0;
                        $art->priceallbrutto = $art->priceall;
                    }

                    $art->data = serialize($artikel);
                    $art->resale_price = $artikel->getResalePrice();
                    $art->ref = $artikel->getRef();
                    $art->kst = $artikel->getKst();
                    $art->hasgutschein = intval($artikel->getHasGutschein());
                    $art->typ = $article->typ;
                    $art->shipping_type = intval($artikel->getShippingtype());
                    $art->shipping_price = $artikel->getShippingPrice();
                    $art->calc_values = Zend_Json::encode($artikel->getCalcValues());
                    $art->save();

                    $art->setLayouterId($artikel->getLayouterId());
                    $art->setTemplatePrintId($artikel->getTemplatePrintId());
                    $art->setTemplatePrintContactId($artikel->getTemplatePrintContactId());
                    $art->setDeliveryData($artikel->getDeliverys());
                    $art->setCalcReferences($artikel->getCalcReferences());
                    $art->setUploadMode($artikel->getUploadMode());
                    $art->setAdditionalInfos($artikel->getAdditionalInfos());
                    $art->saveMongo();

                    $articlebuy = new Articlebuy();
                    $articlebuy->contact_id = $user->id;
                    $articlebuy->article_id = $article->id;
                    $articlebuy->save();

                    $hasUploads = false;
                    foreach ($artikel->getFiles() as $file) {
                        $upload = new Upload();
                        $upload->Orderspos = $art;
                        $upload->name = $file['name'];
                        $upload->path = $file['value'];
                        $upload->typ = $file['what'];
                        $upload->chunktitle = $file['chunktitle'];
                        $upload->save();
                        $hasUploads = true;
                    }


                    if ($art->status == 0) {
                        $art->status = $article->init_status;
                        $art->save();
                    }
                    if ($article->typ == 2) {
                        if ($article->init_status == "" || $article->init_status == 0) {
                            $art->status = 10;
                        } else {
                            $art->status = $article->init_status;
                        }
                        $art->save();
                    }
                    if ($article->confirm) {
                        if ($article->confirmone) {
                            $art->status = 90;
                            $order->status = 90;
                        } else {
                            $art->status = 100;
                            $order->status = 90;
                        }
                        $order->save();
                        $art->save();

                        $contacts = Doctrine_Query::create()->from('ArticleConfirmContact c')->where('c.article_id = ?', array(
                            $orgid
                        ))->execute();

                        $dbMongo = TP_Mongo::getInstance();

                        foreach ($contacts as $contact) {

                            $ordposconfirm = new OrdersposConfirmContact();
                            $ordposconfirm->orderspos_id = $art->id;
                            $ordposconfirm->contact_id = $contact->contact_id;
                            $ordposconfirm->save();

                            TP_Queue::process('orderpos_approval_send', 'global', array('pos' => $art, 'article' => $article, 'contact' => $contact->Contact));

                            $dbMongo->Job->insertOne(array(
                                "shop" =>  $this->shop->id,
                                "event" => "position_contact_approval_request",
                                "data" => ["contact" => $contact->Contact->uuid, "position" => $art->uuid, "order" => $order->uuid],
                                "created" => new MongoDB\BSON\UTCDateTime(),
                                "updated" => new MongoDB\BSON\UTCDateTime()
                            ));

                        }
                    }

                    TP_Queue::process('orderposstatuschange', 'global', $art);
                    $dbMongo = TP_Mongo::getInstance();

                    $dbMongo->Job->insertOne(array(
                        "shop" =>  $this->shop->id,
                        "status" => $art->status,
                        "event" => "position_status_change",
                        "data" => ["status" => $art->status, "position" => $art->uuid],
                        "created" => new MongoDB\BSON\UTCDateTime(),
                        "updated" => new MongoDB\BSON\UTCDateTime()
                    ));
                }
                TP_Queue::process('ordercreated', 'global', $order);
                $dbMongo = TP_Mongo::getInstance();
                $dbMongo->Job->insertOne(array(
                    "shop" =>  $this->shop->id,
                    "event" => "order_create",
                    "data" => ["order" => $order->uuid],
                    "created" => new MongoDB\BSON\UTCDateTime(),
                    "updated" => new MongoDB\BSON\UTCDateTime()
                ));

                if ($order->use_account_as_invoice) {
                    TP_Queue::process('ordercreatedaccount', 'global', $order);
                }
                $basket->clear();
                $this->view->priorityMessenger('Order created', 'success');

                $conv = new Zend_Session_Namespace('conversion_tracking');

                $conv->unlock();

                if (!$conv->conv) {
                    $conv->conv = 0;
                }

                $conv->conv = $order->preisbrutto;

                if ($this->shop->getRedirectAfterBuy() != "") {
                    $templates = array('template' => $this->shop->getRedirectAfterBuy());
                    $twig = new \Twig\Environment(new \Twig\Loader\ArrayLoader($templates));

                    $this->redirectSpeak($twig->render('template', array('order' => $order, 'pos' => $art)));
                }

                if ($this->shop->redirect_to_uploadcenter_after_basket_finish) {
                    $this->redirectSpeak('/uploadcenter?id=' . $order->id);
                    return;
                }

                if (file_exists($this->_templatePath . '/basket/done.phtml') || file_exists(APPLICATION_PATH . '/design/vorlagen/' . $this->shop->layout . '/templates/basket/done.phtml')) {
                    $this->redirectSpeak('/basket/done/' . $order->id);
                    return;
                }
                $this->redirectSpeak('/uploadcenter?id=' . $order->id);
            } else {
                $basket = new TP_Basket();
                if ($this->_getParam('basketfield1')) {
                    $basket->setBasketField1($this->_getParam('basketfield1'));
                }
                if ($this->_getParam('basketfield2')) {
                    $basket->setBasketField1($this->_getParam('basketfield2'));
                }
                $this->view->agb = true;

                if ($this->getRequest()->isPost()) {
                    if ($this->_getParam('template_mode', false) == "simple") {
                        $this->redirectSpeak("/basket/simple");
                        return;
                    }

                    if (!$this->_getParam('agb', 1)) {
                        $this->view->erroragb = true;
                        $this->view->priorityMessenger('Bitte AGB bestätigen', 'error');
                    }

                    if (!$this->_getParam('revocation', 1)) {
                        $this->view->errorrevocation = true;
                        $this->view->priorityMessenger('Bitte die Widerrufsbelehrung bestätigen', 'error');
                    }

                    if (!$this->_getParam('privacy', 1)) {
                        $this->view->errorprivacy = true;
                        $this->view->priorityMessenger('Bitte die Datenschutzerklärung bestätigen', 'error');
                    }

                    if (!$this->_getParam('payment', 1)) {
                        $this->view->errorpayment = true;
                        $this->view->priorityMessenger('Bitte die Zahlungskonditionen akzeptieren', 'error');
                    }

                    if (!$this->_getParam('company', 1)) {
                        $this->view->errorcompany = true;
                        $this->view->priorityMessenger('Bitte die Bestellung als Unternehmer bestätigen', 'error');
                    }

                    if (!$this->_getParam('person', 1)) {
                        $this->view->errorperson = true;
                        $this->view->priorityMessenger('Bitte die Auftragserteilung bestätigen', 'error');
                    }
                }
            }
        } else {
            $this->forwardSpeak('login', 'user', 'default', array(
                'mode' => 'basket'
            ));
            return;
        }

        $this->view->liefer_custom = $basket->getDeliveryIsSame();

        $articles = $basket->getAllArtikel();
        $temp = array();
        foreach ($articles as $uuid => $art) {
            $article = Doctrine_Query::create()->from('Article c')->where('c.uuid = ?', array(
                $art->getArticleId()
            ))->fetchOne();

            if ($article->typ != '9') {
                $field = 'a1_xml';
                $options = array();

                if ($article->a6_org_article != 0) {
                    $options = $article->OrgArticle->getOptArray($art, false);
                } else {
                    $options = $article->getOptArray($art, false);
                }

                array_push($temp, array(
                    'uuid' => $uuid,
                    'article' => $article,
                    'basketarticle' => $art,
                    'options' => $options
                ));
            }
        }

        $this->view->basket_articles = $basket->getAllArtikel();
        $this->view->basketfield1 = $basket->getBasketField1();
        $this->view->basketfield2 = $basket->getBasketField2();
        $this->view->basketInfo = $basket->getInfo();

        $this->view->basket_articles_complete = $temp;

        $this->view->basket_gutschein = $basket->getGutscheinAbzug();
        $this->view->basket_gutschein_enable = $basket->getGutschein();

        $this->view->gutschein = false;

        if ($basket->getGutschein() != "") {
            $gutscheincode = Doctrine_Query::create()->from('CreditSystemDetail c')->leftJoin('c.CreditSystem a')->where('a.shop_id = ? AND c.uuid = ? AND (a.product_id = 0 OR a.product_id IS NULL) AND (a.articlegroup_id = 0 OR a.articlegroup_id IS NULL) AND a.f <= ? AND a.t >= ?', array(
                $this->shop->id,
                $basket->getGutschein(), date('Y-m-d'), date('Y-m-d')
            ))->fetchOne();

            if(!$gutscheincode) {
                $gutscheincode = Doctrine_Query::create()->from('CreditSystem a')->where('a.shop_id = ? AND a.pre_code = ? AND (a.product_id = 0 OR a.product_id IS NULL) AND (a.articlegroup_id = 0 OR a.articlegroup_id IS NULL) AND a.f <= ? AND a.t >= ?', array(
                    $this->shop->id,
                    $basket->getGutschein(), date('Y-m-d'), date('Y-m-d')
                ))->fetchOne();

            }elseif($gutscheincode) {
                $gutscheincode = $gutscheincode->CreditSystem;
            }

            $this->view->gutschein = $gutscheincode;
        }

        $this->view->preis = $basket->getPreisNetto();
        $this->view->preiszu = ($basket->getVersandkosten() + $basket->getZahlkosten());
        $this->view->preissteuer = $basket->getPreisSteuer();
        $this->view->preisbrutto = $basket->getPreisBrutto();

        if (!$basket->getDeliveryIsSame()) {
            $this->view->liefer = Doctrine_Query::create()->select()->from('ContactAddress c')->where('c.uuid = ?', array($basket->getDelivery()))->fetchOne();
        }

        $this->view->paymenttype_id = $basket->getPaymenttype();
        $this->view->shippingtype_id = $basket->getShippingtype();

        $this->view->shippingtype_extra_label = $basket->getShippingtypeExtraLabel();

        $spt = Doctrine_Query::create()->from('Shippingtype c')->where('c.id = ?', array(
            $basket->getShippingtype()
        ))->fetchOne();

        $pmt = Doctrine_Query::create()->from('Paymenttype c')->where('c.id = ?', array(
            $basket->getPaymenttype()
        ))->fetchOne();

        $this->view->paymenttype = new Paymenttype();
        $this->view->shippingtype = new Shippingtype();
        if ($spt) {
            $this->view->shippingtype = $spt;
        }
        if ($pmt) {
            $this->view->paymenttype = $pmt;
        }

        if (trim($this->shop->custom_agb) != "") {
            $this->view->display_agb = $this->shop->custom_agb;
        } else {
            $this->view->display_agb = $this->install->agb;
        }

        if ($basket->getSender() != "") {
            $this->view->sender_address = Doctrine_Query::create()->select()->from('ContactAddress s')->where('s.contact_id = ? AND s.uuid = ?', array($this->user->id, $basket->getSender()))->fetchOne();
        }
        if ($basket->getInvoice() != "") {
            $this->view->invoice_address = Doctrine_Query::create()->select()->from('ContactAddress s')->where('s.contact_id = ? AND s.uuid = ?', array($this->user->id, $basket->getInvoice()))->fetchOne();
        }
        if ($basket->getDelivery() != "") {
            $this->view->delivery_address = Doctrine_Query::create()->select()->from('ContactAddress s')->where('s.contact_id = ? AND s.uuid = ?', array($this->user->id, $basket->getDelivery()))->fetchOne();
            $basket->setDeliveryCountry($this->view->delivery_address->country);
            $basket->setDeliveryUstId($this->view->delivery_address->ustid);
        } else {
            $basket->setDeliveryPLZ($this->view->invoice_address->country);
            $basket->setDeliveryCountry($this->view->invoice_address->country);
            $basket->setDeliveryUstId($this->view->invoice_address->ustid);
            $this->view->delivery_address = $this->view->invoice_address;
        }
        $basket->setWithTax(TP_Country::getTaxForCountry($basket->getDeliveryCountry(), $basket->getDeliveryUstId()));

        if (is_int($basket->getSender()) && $basket->getSender() == 0 || $basket->getSender() === '0') {
            $this->view->sender_address = $this->shop->getOrderSenderSavedAsObject();
        }
        $this->view->withTax = $basket->isWithTax();
    }

    public function anfrageAction()
    {

        $this->getHelper('layout')->disableLayout();
        $this->_helper->viewRenderer->setNoRender();

        $basket = new TP_Basket();

        foreach ($basket->getAllArtikel() as $artikel) {

            if ($artikel->getSpecial()) {

                $user = Zend_Auth::getInstance()->getIdentity();
                $user = Doctrine_Query::create()->from('Contact m')->where('id = ?')->fetchOne(array(
                    $user['id']
                ));

                $article = Doctrine_Query::create()->from('Article c')->where('c.uuid = ?', array(
                    $artikel->getArticleId()
                ))->fetchOne();

                $order = new Orders();
                $order->Shop = $this->shop;
                $order->Account = $user->Account;
                $order->created = date('Y-m-d H:i:s');
                $order->updated = date('Y-m-d H:i:s');
                $order->Install = $user->Install;
                $order->enable = 1;

                $install = $this->shop->Install;

                if ($install->order_number_date) {
                    $order->alias = date('Ym-') . $install->order_number;
                } else {
                    $order->alias = $install->order_number;
                }

                $install->order_number = $install->order_number + 1;
                $install->save();

                $order->Contact = $user;
                $order->gutschein = $basket->getGutschein();
                $order->gutscheinabzug = $basket->getGutscheinAbzug();
                $order->basketfield1 = $basket->getBasketField1();
                $order->basketfield2 = $basket->getBasketField2();
                $order->delivery_same = $basket->getDeliveryIsSame();
                if (!$basket->getDeliveryIsSame()) {
                    $contactaddress = Doctrine_Query::create()->from('ContactAddress c')->where('c.uuid = ?', array(
                        $basket->getDelivery()
                    ))->fetchOne();
                    $order->delivery_address = $contactaddress->id;
                }
                $order->status = 20;
                $order->preis = $basket->getPreisNetto();
                $order->preissteuer = $basket->getPreisSteuer();
                $order->preisbrutto = $basket->getPreisBrutto();
                $order->paymenttype_id = $basket->getPaymenttype();
                $order->shippingtype_id = $basket->getShippingtype();

                $order->zahlkosten = $basket->getZahlkosten();
                $order->versandkosten = $basket->getVersandkosten();

                if ($basket->getGutschein() != "") {
                    $gutscheincode = Doctrine_Query::create()->from('CreditSystemDetail c')->where('c.shop_id = ? AND c.used = 0 AND c.uuid = ?', array(
                        $this->shop->id,
                        $basket->getGutschein()
                    ))->fetchOne();
                    if ($gutscheincode != false) {
                        $gutscheincode->used = true;
                        $gutscheincode->save();
                    }
                }
                $order->save();

                $art = new Orderspos();

                $layouter_mode = false;
                if ($article->typ == 6 || ($article->typ == 8 && $artikel->getLayouterId() != "")) {

                    $uuid = $article->uuid;
                    if ($article->a6_org_article != 0) {
                        $orgid = $article->a6_org_article;
                    } else {
                        $orgid = $article->id;
                    }
                    $article = $article->copy();
                    $article->uuid = "";
                    $article->a6_org_article = $orgid;
                    $article->url = $article->url . '_' . time();
                    $article->contact_id = $user->id;
                    $article->private = true;
                    $article->render_new_preview_image = true;
                    $article->render_new_preview_gallery = true;
                    $article->render_new_preview_pdf = true;
                    $article->save();

                    $rel = new ContactArticle();
                    $rel->article_id = $article->id;
                    $rel->contact_id = $user->id;
                    $rel->save();

                    $articles = Zend_Registry::get('articles');

                    $articleObj = new $articles[$article->typ]();

                    $layouter_mode = true;

                    $art->render_print = true;

                    if (method_exists($articleObj, 'copyPreDispatch')) {
                        $articleObj->copyPreDispatch($article, $artikel->getLayouterId(), $art);
                    }
                }

                if ($article->stock) {
                    $article->stock_count = $article->stock_count - 1;
                    $article->save();
                    if ($article->stock_count <= $article->stock_count_min) {
                        TP_Queue::process('articlestockmin', 'global', $article);
                    }
                }

                $motive = $artikel->getMotive();
                if (!empty($motive)) {
                    Doctrine_Query::create()->update('Motiv')->set('used', 'used + 1')->where('id IN (' . implode(',', array_keys($artikel->getMotive())) . ')')->execute();
                }
                $article->used = $article->used + 1;
                $article->save();

                $art->Shop = $this->shop;
                $art->Account = $user->Account;
                $art->Install = $user->Install;
                $art->status = 20;
                $art->layouter_mode = true;
                $art->calc_xml = $article->a1_xml;

                $art->createdd = date('Y-m-d');
                $art->updatedd = date('Y-m-d');
                $art->createdt = date('H:i');
                $art->updatedt = date('H:i');
                $art->orders_id = $order->id;
                $art->count = $artikel->getCount();
                $art->article_id = $article->id;
                $art->priceone = $artikel->getNetto();
                $art->shipping_type = $artikel->getShippingtype();
                $art->shipping_price = $artikel->getShippingPrice();
                $art->shipping_price_mwert = $artikel->getShippingPriceMWert();
                $art->ref = $artikel->getRef();
                $art->priceonesteuer = $artikel->getSteuer();
                $art->priceonebrutto = $artikel->getBrutto();
                $art->priceall = $artikel->getNetto() * $artikel->getCount();
                $art->priceallsteuer = $artikel->getSteuer() * $artikel->getCount();
                $art->priceallbrutto = $artikel->getBrutto() * $artikel->getCount();
                $art->data = serialize($artikel);
                $art->resale_price = $artikel->getResalePrice();
                $art->typ = $article->typ;
                $art->save();

                foreach ($artikel->getFiles() as $file) {
                    $upload = new Upload();
                    $upload->Orderspos = $art;
                    $upload->name = $file['name'];
                    $upload->path = $file['value'];
                    $upload->chunktitle = $file['chunktitle'];
                    $upload->save();
                }

                $this->view->priorityMessenger('Anfrage gesendet', 'success');

                $basket->clear();

                $this->redirectSpeak('/');
                return;
            }
        }
    }
}
