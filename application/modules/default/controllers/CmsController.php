<?php

/**
 * Cms Controller
 *
 * Stellt das Contentmanagement fürs Frontend da
 *
 * PHP versions 4 and 5
 *
 * @category   Modul
 * @package    Default
 * @subpackage Controller
 * @author     Thomas petersomn <tpeterson@printshopcreator.de>
 * @copyright  1997-2009 Thomas Peterson
 * @license    Not Public
 * @version    SVN: $Id: CmsController.php 7266 2012-02-02 20:52:29Z boonkerz $
 * @link       http://www.printshopcreator.de
 */

/**
 * Cms Controller
 *
 * Stellt das Contentmanagement fürs Frontend da
 *
 * PHP versions 4 and 5
 *
 * @category   Modules
 * @package    Default
 * @subpackage Controller
 * @author     Thomas petersomn <tpeterson@printshopcreator.de>
 * @copyright  1997-2009 Thomas Peterson
 * @license    Not Public
 * @version    Release: $Id: CmsController.php 7266 2012-02-02 20:52:29Z boonkerz $
 * @link       http://www.printshopcreator.de
 */
class CmsController extends TP_Controller_Action
{

    public function preDispatch() {
        //self::preDispatch();
    }

    /**
     * Cms Seiten werden immer nach dieser Methode gemappt.
     * Es erfolgen Weiterleitungen nach dem eingestellten
     * Modul.
     *
     * @param string $page
     * @return void
     */
    public function indexAction($page = 'Overview') {

        $this->view->actmenu[] = $this->_request->getParam('page');

        $page = Doctrine_Query::create()
            ->from('Cms c')
            ->where('c.url = ? AND c.shop_id = ? AND
                (c.language = ? OR language = \'all\')')
            ->fetchOne(array($this->_request->getParam('page'),
            $this->shop->id, Zend_Registry::get('locale')));

        if ($page === false && $this->_request->getParam('page', false) && $this->shop->market) {
            $page = Doctrine_Query::create()
                ->from('Cms c')
                ->where('c.url = ? AND c.shop_id = ? AND
                    (c.language = ? OR language = \'all\')')
                ->fetchOne(array($this->_request->getParam('page'), $this->install->defaultmarket,
                Zend_Registry::get('locale')));
        }

        if ($page === false) {
            $page = Doctrine_Query::create()->from('Cms c')
                ->where('c.url = ? AND c.shop_id = ? AND c.language = ?')
                ->fetchOne(array('index', $this->shop->id,
                Zend_Registry::get('locale')));
        }

        if ($page === false) {
            $page = Doctrine_Query::create()->from('Cms c')
                ->where('c.url = ? AND c.shop_id = ?')
                ->fetchOne(array('index', $this->shop->id));
        }

        if ($page == false) {
            $this->_forward('notfound', 'error');
            return;
        }

        if ($page->display_only_logged_in && !Zend_Auth::getInstance()->hasIdentity()) {
            $this->_redirect('/user/login');
        }

        if ($page->modul == 'Overview') {
            if ($page->parameter != '') {
                $this->_forward('index', 'overview', 'default', array('id' => $page->parameter));
            } else {
                $this->_forward('index', 'overview', 'default');
            }
        }
        if ($page->modul == 'Login') {
            $this->_forward('login', 'user', 'default');
        }
        if ($page->modul == 'News') {
            $this->_forward('index', 'news', 'default');
        }
        if ($page->modul == 'Start') {
            if ($this->shop->defaultparam != '') {
                $this->_forward($this->shop->defaultparam, 'cms', 'default');
            } else {
                $this->_forward('index', 'cms', 'default');
            }
        }

        $nav = $this->view->navigation()->findOneBy('id', $page->pos . '_' . $page->url);

        if (!$nav) {
            $this->view->navigation()->findOneBy('label', 'Home')->addPage(array(
                'label' => $page->menu,
                'id' => $page->pos . '_' . $page->url,
                'active' => true,
                'uri' => '/cms/' . $page->url));
        }
        /* $activeNav = $this->view->navigation()->findOneBy( 'id', $page->pos . '_' . $page->url);
          if ( $activeNav != null )
          {
          $activeNav->active = true;
          $activeNav->setClass( "active" );
          }
         */

        if ($this->getRequest()->isPost() && !$this->_getParam('contact_newsletter', false) && $this->_getParam('_action', false) != "newsletter") {

            $captcha = $element = new Zend_Form_Element_Captcha('cp', array(
                'label' => "",
                'captcha' => array(
                    'captcha' => 'Image',
                    'font' => APPLICATION_PATH . '/fonts/verdana.ttf',
                    'imgDir' => PUBLIC_PATH . '/temp/thumb/',
                    'imgUrl' => '/temp/thumb/',
                    'wordLen' => '4'
                ),
            ));

            $valid = $captcha->getCaptcha()->isValid($this->_getParam('cp'));

            if ($this->getRequest()->isPost() && !$this->view->liveedit && (($this->_getParam('cp', false) && $valid))) {

                $this->_setParam('page', $page->id);
                TP_Queue::process('cmspost', 'global', $this->getRequest()->getParams());

                $this->view->priorityMessenger('Nachricht versendet.');

                $files = new Zend_Session_Namespace('Basket');

                if (isset($files->files[$page->id])) {
                    unset($files->files[$page->id]);
                }
            } else {
                $this->view->priorityMessenger('Captcha stimmt nicht überein', 'error');
            }
        }

        $this->view->enable_upload = false;
        if (strpos('t' . $page->parameter, 'enable_upload')) {
            $this->view->enable_upload = true;
            $this->view->page_id = $page->id;

            $files = new Zend_Session_Namespace('Basket');

            if (isset($files->files[$page->id])) {
                $this->view->files = $files->files[$page->id];
            }
        }

        $this->setHeadData($page);

        $this->view->page = $page;

        $this->_helper->layout->setLayout('default');
        $this->view->noLayout = false;
        if ($this->shop->private && !Zend_Auth::getInstance()->hasIdentity()) {
            $this->_helper->layout->setLayout('private');
        }
        if($this->view->page->template != "") {
            $this->render($this->view->page->template);
        }
        if ($this->_getParam('help')) {
            $this->_helper->layout->setLayout('simple');
            $this->view->noLayout = true;
        }
        if ($this->_getParam('layout')) {
            $this->_helper->layout->setLayout($this->_getParam('layout'));
            $this->view->noLayout = true;
        }
    }

}
