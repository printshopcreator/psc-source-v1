<?php

class ErrorController extends TP_Controller_Action
{

    public function checkRedirects() {

        if(file_exists(APPLICATION_PATH .'/design/redirects.csv')) {
            $redirects = array();

            if (($handle = fopen(APPLICATION_PATH .'/design/redirects.csv', "r")) !== FALSE) {
                while (($data = fgetcsv($handle, 1000, ";")) !== FALSE) {
                    $redirects[$data[0]] = $data[1];
                }
                fclose($handle);
            }

            if (isset($redirects[$_SERVER['REQUEST_URI']])) {
                header('location: ' . $redirects[$_SERVER['REQUEST_URI']]);
                die();
            }
        }
    }

    /**
     * ErrorAction
     *
     * @todo Dokumentation der ErrorAction
     */
    public function errorAction() {
        $this->checkRedirects();
        // Ensure the default view suffix is used so we always return good
        // content
        //$this->_helper->layout()-> disableLayout();

        // Grab the error object from the request
        $errors = $this->_getParam('error_handler');

        switch ($errors['type']) {
            case Zend_Controller_Plugin_ErrorHandler::EXCEPTION_NO_CONTROLLER:
            case Zend_Controller_Plugin_ErrorHandler::EXCEPTION_NO_ACTION:
            case Zend_Controller_Plugin_ErrorHandler::EXCEPTION_OTHER:
                // 404 error -- controller or action not found
                //$this->getResponse()->setHttpResponseCode(404);
                $this->view->message = 'Page not found';
                $this->view->code = 404;
                if ($errors['type'] == Zend_Controller_Plugin_ErrorHandler::EXCEPTION_NO_CONTROLLER) {
                    $this->view->info = sprintf(
                        'Unable to find controller "%s" in module "%s"',
                        $errors->request->getControllerName(),
                        $errors->request->getModuleName()
                    );
                }
                if ($errors['type'] == Zend_Controller_Plugin_ErrorHandler::EXCEPTION_NO_ACTION) {
                    $this->view->info = sprintf(
                        'Unable to find action "%s" in controller "%s" in module "%s"',
                        $errors->request->getActionName(),
                        $errors->request->getControllerName(),
                        $errors->request->getModuleName()
                    );
                }
                if ($errors['type'] == Zend_Controller_Plugin_ErrorHandler::EXCEPTION_OTHER) {
                    $this->view->info = sprintf(
                        'Unable to find action "%s" in controller "%s" in module "%s or ACL error"',
                        $errors->request->getActionName(),
                        $errors->request->getControllerName(),
                        $errors->request->getModuleName()
                    );
                }
                break;

            case Zend_Controller_Plugin_ErrorHandler::EXCEPTION_NO_ACTION:
            default:
                // application error
                //$this->getResponse()->setHttpResponseCode(500);
                $this->view->message = 'Application error';
                $this->view->code = 500;
                $this->view->exception = $errors['exception'];

                break;
        }
        Zend_Registry::get('log')->debug('Exception: ' . $errors['exception']. $this->view->info. $this->view->message);

        $shop = Zend_Registry::get('shop');
        //TP_Mail::sendTextMail( 'queue', $errors->request->getModuleName().$errors->request->getControllerName().$errors->request->getActionName().$errors['exception'].$_SERVER['REQUEST_URI'], '', 'ERROR CODE: '. $this->view->code .' ' .$shop['id']. ' '. $shop['name'] , 'boonkerz@yahoo.de', 'PrintshopCreator Server', 'support@printshopcreator.de' );

        $this->view->title = 'Error!';
        $this->view->heading = 'Error!';

        // pass the environment to the view script so we can conditionally
        // display more/less information
        $this->view->env = APPLICATION_ENV;
        // pass the actual exception object to the view
        $this->view->exception = $errors['exception'];

        // pass the request to the view
        $this->view->request = $errors['request'];

        $params = $this->getRequest()->getParams();
        unset($params['error_handler']);

        #Use XMPPHP_Log::LEVEL_VERBOSE to get more logging for error reports
        #If this doesn't work, are you running 64-bit PHP with < 5.2.6?
        /*$conn = new XMPPHP_XMPP('www.thomas-peterson.de', 5222, 'admin', '38412914', 'xmpphp', 'www.thomas-peterson.de', $printlog=false, $loglevel=XMPPHP_Log::LEVEL_INFO);
    
       try {
            $conn->connect();
            $conn->processUntil('session_start');
            $conn->presence();
            $conn->message('bot@thomas-peterson.de', 'psc log ' . $this->shop->Domain[0]->name . ' '.$this->view->code.' ' . $this->view->info . ' ' . print_r($params, true));
            $conn->disconnect();
        } catch(XMPPHP_Exception $e) {
            //die($e->getMessage());
        }*/
    }

    /**
     * Keine Zugriffsrechte
     *
     * @return void
     */

    public function noaccessAction() {
        $this->checkRedirects();
    }

    public function notfoundAction() {
        $this->checkRedirects();
        $this->getResponse()->setHttpResponseCode(404);
    }
}
