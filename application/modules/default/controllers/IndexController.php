<?php

class IndexController extends TP_Controller_Action
{

    /**
     * IndexAction
     *
     * @todo Startcontroller der Webseite
     *
     * @return void
     */
    public function init() {
        parent::init();
        $contextSwitch = $this->_helper->contextSwitch();
        $contextSwitch->addActionContext('sendtellafriend', 'json')->addActionContext('sendcms', 'json')->addActionContext('sendfeedback', 'json')->setAutoJsonSerialization(true)->initContext();
    }

    public function sitemapAction() {
        $this->_helper->viewRenderer->setNoRender();
        $this->getHelper('layout')->disableLayout();

        if ($this->shop->sitemap != "") {

            $dbMongo = TP_Mongo::getInstance();
            $obj = $dbMongo->Media->findOne(array('_id' => new \MongoDB\BSON\ObjectId($this->shop->sitemap)));
            readfile("/data/www/new/web" . $obj['url']);
        }else{
            die("");
        }
    }

    public function aboutAction() {

    }

    public function goAction() {

    }

    public function robotsAction() {
        $this->_helper->viewRenderer->setNoRender();
        $this->getHelper('layout')->disableLayout();

        $this->getResponse()
            ->setHeader('Content-type', 'text/plain');

        if ($this->shop->robots != "") {

            $dbMongo = TP_Mongo::getInstance();
            $obj = $dbMongo->Media->findOne(array('_id' => new \MongoDB\BSON\ObjectId($this->shop->robots)));
            readfile("/data/www/new/web" . $obj['url']);
        } else {
            die("");
        }
    }

    public function videositemapAction() {
        $this->_helper->viewRenderer->setNoRender();
        $this->getHelper('layout')->disableLayout();

        $this->getResponse()
            ->setHeader('Content-type', 'application/xhtml+xml');

        if ($this->shop->xmlvideositemap != "") {

            $xml = Doctrine_Query::create()
                ->from('Image as i')
                ->where('i.id = ?')->fetchOne(array($this->shop->xmlvideositemap));

            readfile($xml->path);
        } else {
            //readfile('xmlsitemap.xml');
        }
    }

    public function agbAction() {
        $this->view->agb = $this->shop->getConditionsText();
    }

    public function revocationAction() {
        $this->view->revocation = $this->shop->getCancellationTermsText();
    }

    public function impressAction() {
        if($_SERVER['REQUEST_URI'] == '/impress') {
            $dbMongo = TP_Mongo::getInstance();
            $route = $dbMongo->Route->findOne(array('url' => '/impress'));
            if ($route) {
                return $this->redirect($route->parameter->getArrayCopy()['target']);
            }
        }
        $this->view->impress = $this->shop->getImprintText();
    }

    public function privacyAction() {
        if($_SERVER['REQUEST_URI'] == '/privacy') {
           $dbMongo = TP_Mongo::getInstance();
            $route = $dbMongo->Route->findOne(array('url' => '/privacy'));
            if ($route) {
                return $this->redirect($route->parameter->getArrayCopy()['target']);
            }
        }
        $this->view->privacy = $this->shop->getPrivacyPolicyText();
    }

    public function contactAction() {
        $this->view->form = new TP_Forms_Shop_Contact($this->shop);
        if ($this->getRequest()->isPost()) {
            $params = $this->getRequest()->getParams();
            if ($this->view->form->isValid($params)) {
                TP_Queue::process('contactpost', 'global', $this->getRequest()->getParams());
                $this->view->priorityMessenger('Nachricht erfolgreich versendet.', 'success');
            } else {
                $this->view->form->buildBootstrapErrorDecorators();
                $this->view->priorityMessenger('Bitte kontrollieren Sie Ihre Eingaben.', 'error');
                $this->view->form->populate($params);
            }

        }
        $this->view->market_shop = false;
        if (Zend_Registry::isRegistered('market_shop')) {
            $this->view->market_shop = Zend_Registry::get('market_shop');
        }
    }

    public function redirectAction() {
        $this->_helper->viewRenderer->setNoRender();
        $this->getHelper('layout')->disableLayout();
        $mode = $this->getRequest()->getParam('mode');
        $target = $this->getRequest()->getParam('target');

        if($mode == "redirect_301") {
            return $this->_redirect($target, ['prepandBase' => true, 'code' => 301]);
        }
        die("ERROR");
    }

    public function indexAction() {

        $this->view->showStart = true;
        if ($this->shop->defaultfunc == 'Cms') {
            if ($this->shop->defaultparam != '') {
                $this->_redirect('/cms/' . $this->shop->defaultparam);
            } else {
                $this->_forward('index', 'cms', 'default');
            }
        }

        if ($this->shop->defaultfunc == 'Page') {
            $this->_redirect('/cms/' . $this->shop->defaultparam);
        }

        if ($this->shop->defaultfunc == 'Redirect') {
            $this->_redirect($this->shop->defaultparam);
        }

        if ($this->shop->defaultfunc == 'PersArticle') {
            if($this->_getParam('help_mode', false)) {
                $this->_redirect('/article/mypersarticle?help_mode=1');
            }
            $this->_redirect('/article/mypersarticle');
        }

        if ($this->shop->defaultfunc == 'Overview') {
            $this->_helper->viewRenderer->setNoRender();
            if ($this->shop->defaultparam != "") {
                //$this->_redirect('/overview/'.  $this->shop->defaultparam);
                $this->_forward('index', 'overview', 'default', array('id' => $this->shop->defaultparam, 'redirected' => true));
            } else {
                //$this->_redirect('/overview');
                $this->_forward('index', 'overview', 'default', array('redirected' => true));
            }
            return;
        }
        if ($this->shop->defaultfunc == 'OverviewWithRedirect') {
            $this->_helper->viewRenderer->setNoRender();

            if ($this->shop->defaultparam != "") {
                $this->_redirect('/overview/' . $this->shop->defaultparam, array('code' => 301));
            } else {
                $this->_redirect('/overview', array('code' => 301));
            }
            return;
        }
        if ($this->shop->defaultfunc == 'Login') {
            $this->_redirect('/user');
        }

        if ($this->shop->defaultfunc == 'Article') {
            $this->_forward('show', 'article', 'default', array('id' => $this->shop->defaultparam));
        }

        $this->view->articlegroup = new ArticleGroup();
        $this->view->articlegroup->url = 'all';

        $this->render(TP_Shop_Settings::getSetting('index', 'index_index_layout'));
    }

    public function violationAction() {

        $config = new Zend_Config_Ini($this->_configPath . '/index/violation.ini', 'violation');

        $v = new Zend_Config_Ini(APPLICATION_PATH . '/configs/systemdefaults.ini', 'violation');

        $form = new Zend_Form($config->violation);

        if ($this->getRequest()->getParam('uuid') != "") {
            $form->addElement('hidden', 'uuid', array('value' => $this->getRequest()->getParam('uuid')));
        }
        $form->addElement('hidden', 'type', array('value' => $this->getRequest()->getParam('type')));

        $type = $this->getRequest()->getParam('type');

        $form->what->addMultiOptions($v->$type->toArray());

        $form->setAction('/index/violation/' . $type . '/' . $this->getRequest()->getParam('uuid'));
        $what = $v->$type->toArray();
        if (isset($form->cp)) {
            $form->cp->setOptions(array(
                'label' => "",
                'captcha' => array(
                    'captcha' => 'Image',
                    'font' => PUBLIC_PATH . '/layouter/fonts/verdana.ttf',
                    'imgDir' => PUBLIC_PATH . '/temp/thumb/',
                    'imgUrl' => '/temp/thumb/',
                    'wordLen' => '4'
                ),
            ));
        }
        $this->view->form = $form;

        $this->view->element = "";

        if ($type == 'product') {
            $element = Doctrine_Query::create()
                ->from('Article c')
                ->where('c.uuid = ?', array($this->getRequest()->getParam('uuid')))
                ->fetchOne();

            $this->view->element = $element->title;
        } elseif ($type == 'motiv') {
            $element = Doctrine_Query::create()
                ->from('Motiv c')
                ->where('c.uuid = ?', array($this->getRequest()->getParam('uuid')))
                ->fetchOne();
            $this->view->element = $element->title;
        } elseif ($type == 'themenshop') {
            $element = Doctrine_Query::create()
                ->from('Shop c')
                ->where('c.uid = ?', array($this->getRequest()->getParam('uuid')))
                ->fetchOne();
            $this->view->element = $element->title;
        }

        $formData = $this->_request->getPost();

        if ($this->_request->isPost() && $form->isValid($formData)) {

            $this->view->priorityMessenger('Verstoß erfolgreich gesendet', 'success');
            if ($type == 'all') {
                $data = array_merge($formData, array('whattext' => $what[$formData['what']]));
            } else {
                $data = array_merge($formData, array('whattext' => $what[$formData['what']], 'element' => $element->toArray(false)));
            }
            TP_Queue::process('violationsend', 'global', $data);
        } else {
            if (Zend_Auth::getInstance()->hasIdentity()) {
                $user = Zend_Auth::getInstance()->getIdentity();
                $this->view->form->email->setValue($user['self_email']);
                $this->view->form->name->setValue($user['self_firstname'] . ' ' . $user['self_lastname']);
            }
        }
    }

    public function sendfeedbackAction() {

        TP_Queue::process('feedbackform', 'global', $this->getRequest()->getParams());

        die(Zend_Json::encode(array('success' => true)));
    }

    public function sendtellafriendAction() {

        $captcha = $element = new Zend_Form_Element_Captcha('cp', array(
            'label' => "",
            'captcha' => array(
                'captcha' => 'Image',
                'font' => PUBLIC_PATH . '/layouter/fonts/verdana.ttf',
                'imgDir' => PUBLIC_PATH . '/temp/thumb/',
                'imgUrl' => '/temp/thumb/',
                'wordLen' => '4'
            ),
        ));

        $valid = $captcha->getCaptcha()->isValid($this->_getParam('cp'));

        if ($valid) {
            TP_Queue::process('tellafriend', 'global', $this->getRequest()->getParams());
        }
        die(Zend_Json::encode(array('success' => $valid, 'captcha' => $captcha->render())));
    }

    public function sendcmsAction() {

        $captcha = $element = new Zend_Form_Element_Captcha('cp', array(
            'label' => "",
            'captcha' => array(
                'captcha' => 'Image',
                'font' => PUBLIC_PATH . '/layouter/fonts/verdana.ttf',
                'imgDir' => PUBLIC_PATH . '/temp/thumb/',
                'imgUrl' => '/temp/thumb/',
                'wordLen' => '4'
            ),
        ));

        $valid = $captcha->getCaptcha()->isValid($this->_getParam('cp'));

        if ($valid) {
            TP_Queue::process('cmspost', 'global', $this->getRequest()->getParams());
        }
        die(Zend_Json::encode(array('success' => $valid, 'captcha' => $captcha->render())));
    }

}
