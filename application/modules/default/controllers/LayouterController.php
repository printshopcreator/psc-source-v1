<?php

/**
 * Layouter Controller
 *
 * Stellt den Layouter fürs Frontend bereit
 *
 * PHP versions 5
 *
 * @category   Modul
 * @package    Default
 * @subpackage Controller
 * @author     Thomas petersomn <tpeterson@printshopcreator.de>
 * @copyright  1997-2009 Thomas Peterson
 * @license    Not Public
 * @version    SVN: $Id: LayouterController.php 6946 2011-11-06 21:49:45Z boonkerz $
 * @link       http://www.printshopcreator.de
 */

/**
 * Layouter Controller
 *
 * Stellt den Layouter fürs Frontend bereit
 *
 * PHP versions 5
 *
 * @category   Modules
 * @package    Default
 * @subpackage Controller
 * @author     Thomas petersomn <tpeterson@printshopcreator.de>
 * @copyright  1997-2009 Thomas Peterson
 * @license    Not Public
 * @version    Release: $Id: LayouterController.php 6946 2011-11-06 21:49:45Z boonkerz $
 * @link       http://www.printshopcreator.de
 */
class LayouterController extends TP_Controller_Action
{

    public function init() {
        parent::init();
        $this->getHelper('layout')->disableLayout();
    }

    public function indexAction() {
        $this->view->resale = false;
        if ($this->_getParam('mode') == 'resale') {
            $this->view->resale = true;
        }

        $this->view->orginal = $this->_getParam('orginal', "");
        $this->view->copy = $this->_getParam('copy', 0);

        $this->view->uuid = $this->_getParam('uuid');

        $this->view->autofill = $this->_getParam('autofill', false);

        $this->view->layouterversion = $this->install->layouter_version . $this->shop->layouter_version;

        if ($this->_getParam('list') == true) {
            $this->view->list = true;
        }

        if ($this->view->copy == 0 && Zend_Auth::getInstance()->hasIdentity()) {
            $article = Doctrine_Query::create()
                ->from('Article c')
                ->where('c.uuid = ?', array($this->view->uuid))
                ->fetchOne();

            $user = Zend_Auth::getInstance()->getIdentity();
            $mode = new Zend_Session_Namespace('adminmode');
            if ($mode->over_ride_contact != false && $mode->over_ride_contact != 'new') {
                $user = Doctrine_Query::create()
                    ->from('Contact m')
                    ->where('id = ?')->fetchOne(array($mode->over_ride_contact));
            } else {
                $user = Doctrine_Query::create()
                    ->from('Contact m')
                    ->where('id = ?')->fetchOne(array($user['id']));
            }

            if ($article->contact_id != $user->id && $this->view->admin != true) {
                $this->view->copy = 3;
            }
        }
    }

    public function shirtAction() {
        $this->view->resale = false;
        if ($this->_getParam('mode') == 'resale') {
            $this->view->resale = true;
        }

        $this->view->orginal = $this->_getParam('orginal', "");
        $this->view->copy = $this->_getParam('copy', 0);

        $this->view->uuid = $this->_getParam('uuid');

        if ($this->_getParam('list') == true) {
            $this->view->list = true;
        }

        if ($this->view->copy == 0 && Zend_Auth::getInstance()->hasIdentity()) {
            $article = Doctrine_Query::create()
                ->from('Article c')
                ->where('c.uuid = ?', array($this->view->uuid))
                ->fetchOne();

            $this->view->article = $article;

            $user = Zend_Auth::getInstance()->getIdentity();
            $mode = new Zend_Session_Namespace('adminmode');
            if ($mode->over_ride_contact != false && $mode->over_ride_contact != 'new') {
                $user = Doctrine_Query::create()
                    ->from('Contact m')
                    ->where('id = ?')->fetchOne(array($mode->over_ride_contact));
            } else {
                $user = Doctrine_Query::create()
                    ->from('Contact m')
                    ->where('id = ?')->fetchOne(array($user['id']));
            }

            if ($article->contact_id != $user->id && $this->view->admin != true) {
                $this->view->copy = 3;
            }
        } else {
            if ($this->view->orginal != "") {
                $article = Doctrine_Query::create()
                    ->from('Article c')
                    ->where('c.uuid = ?', array($this->view->orginal))
                    ->fetchOne();
            } else {
                $article = Doctrine_Query::create()
                    ->from('Article c')
                    ->where('c.uuid = ?', array($this->view->uuid))
                    ->fetchOne();
            }
            $this->view->article = $article;
        }
    }

}
