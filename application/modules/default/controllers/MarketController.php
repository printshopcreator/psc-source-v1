<?php

class MarketController extends TP_Controller_Action
{
    public function init() {
        parent::init();
        $this->view->showShop = true;
    }

    /**
     * IndexAction
     *
     * @todo Dokumentation der IndexAction
     *
     * @return void
     */
    public function rateAction() {
        $this->getHelper('layout')->disableLayout();
        $this->_helper->viewRenderer->setNoRender();

        $shop = Doctrine_Query::create()->from('Shop c')->where('c.uid = ?', array($this->_getParam('uuid')))->fetchOne();

        $ratings = $this->getRequest()->getCookie('rateings');
        if ($ratings) {
            $ratings = json_decode($ratings, true);
        } else {
            $ratings = array();
        }

        if (isset($ratings[$this->_getParam('uuid')]) == false && $shop && intval($this->_getParam('vote')) >= 1 && intval($this->_getParam('vote')) <= 5) {
            $shop->rate = $shop->rate + intval($this->_getParam('vote'));
            $shop->rate_count++;
            $shop->save();
            $ratings[$this->_getParam('uuid')] = 1;
            $cookie = new Zend_Http_Cookie('rateings',
                json_encode($ratings),
                $this->shop->Domain[0]->name);
            setcookie("rateings", json_encode($ratings));

            TP_Queue::process('ratesend', 'global', array('data' => $shop->toArray(), 'vote' => intval($this->_getParam('vote'))));
        }

    }

    public function indexAction() {
        $filter = new TP_Themesfilter();

        $this->view->mode = 1;
        $this->view->mode2 = 2;

        if (count($filter->getShopFilter()) > 0) {
            $shops = Doctrine_Query::create()
                ->from('Shop c')
                ->where('c.market = 1 AND c.display_market = 1 AND c.install_id = ?
                            AND c.id IN (SELECT d.shop_id
                            FROM ShopThemeShop as d WHERE
                            d.theme_id = ' . implode(' OR d.theme_id =',
                    array_keys($filter->getShopFilter())) . '
                            GROUP BY d.shop_id)',
                array($this->shop->Install->id));

        } else {
            $shops = Doctrine_Query::create()
                ->from('Shop c')
                ->where('c.market = 1 AND c.display_market = 1 AND c.install_id = ?', array($this->shop->Install->id));
        }

        $shops->select('c.*, (c.rate/c.rate_count) as ratesum');

        $this->view->sort = "nothing";
        $this->view->dir = "nothing";

        if ($this->_getParam('dir')) {
            switch ($this->_getParam('dir')) {

                case 'asc':
                    $dir = 'asc';
                    $this->view->dir = "asc";
                    break;
                case 'desc':
                default:
                    $dir = 'desc';
                    $this->view->dir = "desc";
                    break;
            }

        } else {
            $dir = 'DESC';
            $this->view->dir = "desc";
        }

        if ($this->_getParam('sort')) {
            switch ($this->_getParam('sort')) {
                case 'date' :
                    $shops->orderBy('c.created ' . $dir);
                    $this->view->sort = "date";
                    break;
                case 'rate' :
                    $shops->orderBy('ratesum ' . $dir);
                    $this->view->sort = "rate";
                    break;
                case 'visits' :
                    $shops->orderBy('c.views ' . $dir);
                    $this->view->sort = "visits";
                    break;
                case 'name' :
                    if (!$this->_getParam('dir', false)) {
                        $dir = 'ASC';
                        $this->view->dir = "asc";
                    }
                    $shops->orderBy('name ' . $dir);
                    $this->view->sort = "name";
                    break;
                case 'author' :
                    if (!$this->_getParam('dir', false)) {
                        $dir = 'ASC';
                        $this->view->dir = "asc";
                    }
                    $this->view->sort = "author";
                    $shops->orderBy('c.name ' . $dir);
                    break;

            }
        }

        $pager = new Doctrine_Pager($shops, $this->_getParam('page', 1), $this->shop->display_market_count);

        $this->view->shops = $pager->execute();

        $pagerLayout = new TP_Doctrine_Pager_Arrows(
            $pager,
            new Doctrine_Pager_Range_Sliding(array('chunk' => 5)),
            '/market/page/{%page_number}/sort/' . $this->_getParam('sort', "") . '/' . $dir
        );

        $pagerLayout->setTemplate('<a href="{%url}"><span>{%page}</span></a> ');
        $pagerLayout->setSelectedTemplate('<a href="{%url}"><span><b>{%page}</b></span></a> ');

        $this->view->paginator = $pagerLayout;
    }

    public function myshopAction() {

        $this->view->mode = 2;
        $this->view->mode2 = 1;
        if (Zend_Auth::getInstance()->hasIdentity() == true) {

            $shops = Doctrine_Query::create()
                ->from('Shop c')
                ->leftJoin('c.ShopContact as a')
                ->where('a.admin = 1 AND a.contact_id = ?',
                array($this->user->id));

            $this->view->sort = "nothing";
            $this->view->dir = "nothing";

            $shops->select('c.*, (c.rate/c.rate_count) as ratesum');

            if ($this->_getParam('dir')) {
                switch ($this->_getParam('dir')) {

                    case 'asc':
                        $dir = 'asc';
                        $this->view->dir = "asc";
                        break;
                    case 'desc':
                    default:
                        $dir = 'desc';
                        $this->view->dir = "desc";
                        break;
                }

            } else {
                $dir = 'DESC';
                $this->view->dir = "desc";
            }

            if ($this->_getParam('sort')) {
                switch ($this->_getParam('sort')) {
                    case 'date' :
                        $shops->orderBy('c.created ' . $dir);
                        $this->view->sort = "date";
                        break;
                    case 'visits' :
                        $shops->orderBy('c.views ' . $dir);
                        $this->view->sort = "visits";
                        break;
                    case 'rate' :
                        $shops->orderBy('ratesum ' . $dir);
                        $this->view->sort = "rate";
                        break;
                    case 'name' :
                        if (!$this->_getParam('dir', false)) {
                            $dir = 'ASC';
                            $this->view->dir = "asc";
                        }
                        $shops->orderBy('name ' . $dir);
                        $this->view->sort = "name";
                        break;
                    case 'author' :
                        if (!$this->_getParam('dir', false)) {
                            $dir = 'ASC';
                            $this->view->dir = "asc";
                        }
                        $this->view->sort = "author";
                        $shops->orderBy('c.name ' . $dir);
                        break;

                }
            }

            $pager = new Doctrine_Pager($shops, $this->_getParam('page', 1), $this->shop->display_market_count);

            $this->view->myshops = $pager->execute();

            $pagerLayout = new TP_Doctrine_Pager_Arrows(
                $pager,
                new Doctrine_Pager_Range_Sliding(array('chunk' => 5)),
                '/market/myshop/{%page_number}/sort/' . $this->_getParam('sort', "") . '/' . $dir
            );

            $pagerLayout->setTemplate('<a href="{%url}"><span>{%page}</span></a> ');
            $pagerLayout->setSelectedTemplate('<a href="{%url}"><span><b>{%page}</b></span></a> ');

            $this->view->paginator = $pagerLayout;

        }
    }

}
