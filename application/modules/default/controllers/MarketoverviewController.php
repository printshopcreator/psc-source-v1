<?php

/**
 * Overview Controller
 *
 * Stellt das Overviewsystem fürs Frontend da
 * Artikelgruppen ansicht
 *
 * PHP versions 4 and 5
 *
 * @category   Modul
 * @package    Default
 * @subpackage Controller
 * @author     Thomas petersomn <tpeterson@printshopcreator.de>
 * @copyright  1997-2009 Thomas Peterson
 * @license    Not Public
 * @version    SVN: $Id: OverviewController.php 7095 2012-01-04 09:20:35Z boonkerz $
 * @link       http://www.printshopcreator.de
 */

/**
 * Overview Controller
 *
 * Stellt das Overviewsystem fürs Frontend da
 * Artikelgruppen ansicht
 *
 * PHP versions 4 and 5
 *
 * @category   Modules
 * @package    Default
 * @subpackage Controller
 * @author     Thomas petersomn <tpeterson@printshopcreator.de>
 * @copyright  1997-2009 Thomas Peterson
 * @license    Not Public
 * @version    Release: $Id: OverviewController.php 7095 2012-01-04 09:20:35Z boonkerz $
 * @link       http://www.printshopcreator.de
 */
class MarketoverviewController extends TP_Controller_Action
{

    public function init() {
        parent::init();
        if (!$this->getRequest()->getParam('redirected', false)) {
            $this->view->showMarketProduct = true;
        }

        $articlegroupsd = Doctrine_Query::create()->from('ArticleGroup c')->where('c.shop_id = ? AND c.notinmenu = 0 AND c.enable = 1 AND c.parent=0 AND (language = ? OR language = \'all\')', array(
            $this->install->defaultmarket,
            Zend_Registry::get('locale')))->orderBy('c.pos ASC')->useQueryCache(true)->useResultCache(true)->execute();

        if ($this->_request->getParam('id', false) == 'all') {
            $this->_redirect('/marketoverview/' . $articlegroupsd[0]->url);
        }

        $this->view->navigation()->findOneBy('id', 'Home')->addPage(array(
            'label' => 'MarketArticlegroups',
            'id' => 'MarketArticlegroups',
            'uri' => '/marketoverview'));

        $this->view->navigation()->findOneBy('id', 'Home')->addPage(array(
            'label' => 'MarketArticlegroupsArticle',
            'id' => 'MarketArticlegroupsArticle',
            'uri' => '/marketoverview/'));

        $page = $this->view->navigation()->findOneBy('id', 'MarketArticlegroups');
        $pageart = $this->view->navigation()->findOneBy('id', 'MarketArticlegroupsArticle');

        foreach ($articlegroupsd as $articlegroup) {
            $this->getMarketArticleGroups($page, $pageart, $articlegroup);
        }
    }

    protected function getMarketArticleGroups($page, $pageart, $articlegroup) {
        $page->addPage(array(
            'label' => $articlegroup->title,
            'id' => $articlegroup->id,
            'uri' => '/marketoverview/' . $articlegroup->url));
        $pageart->addPage(array(
            'label' => $articlegroup->title,
            'id' => 'A' . $articlegroup->id,
            'uri' => '/marketoverview/' . $articlegroup->url));
        $articlegroupsd = Doctrine_Query::create()->from('ArticleGroup c')->where('c.shop_id = ? AND c.enable = 1 AND c.parent=?', array(
            $this->install->defaultmarket,
            $articlegroup->id))->orderBy('c.pos ASC')->useQueryCache(true)->useResultCache(true)->execute();
        foreach ($articlegroupsd as $articlegroups) {
            $this->getMarketArticleGroups($this->view->navigation()->findOneBy('id', $articlegroup->id), $this->view->navigation()->findOneBy('id', 'A' . $articlegroup->id), $articlegroups);
        }
    }

    public function indexAction() {

        $defaultNamespace = new Zend_Session_Namespace('settings');
        if (isset($defaultNamespace->overview_mode) && $this->_getParam('mode') == "") {
            $this->getRequest()->setParam('mode', $defaultNamespace->overview_mode);
        }

        $group = $this->_request->getParam('id');
        $this->view->group = $group;
        $this->view->inworkCount = 0;
        $articleSession = new TP_Layoutersession();
        $this->view->inworkCount = count($articleSession->getAllLayouterArticle());

        if (!isset($group) || $group == '' || $group == 'articlegroups') {

            $this->view->id = 0;

            $articlegroups = Doctrine_Query::create()
                ->from('ArticleGroup c')
                ->where('c.shop_id = ? AND c.parent = 0 AND c.enable = 1', array($this->install->defaultmarket))
                ->orderBy('c.pos ASC')
                ->useQueryCache(true)
                ->execute();

            $this->view->articlegroup = new ArticleGroup();
            $this->view->articlegroup->url = 'all';
            $articlegroupselect = " AND c.articlegroup_id = '" . $this->view->id . "'";
        } elseif (isset($group) && $group == 'all') {
            $this->view->articlegroup = new ArticleGroup();
            $this->view->articlegroup->url = 'all';

            $articlegroupselect = "";
            $articlegroups = array();
        } else {
            $this->view->articlegroup = Doctrine_Query::create()
                ->from('ArticleGroup c')
                ->where('c.shop_id = ? AND c.url = ? AND c.enable = 1', array($this->install->defaultmarket, $group))
                ->orderBy('c.pos ASC')
                ->useQueryCache(true)
                ->fetchOne();
            $this->setHeadData($this->view->articlegroup);
            $this->view->id = $this->view->articlegroup->id;

            $articlegroups = Doctrine_Query::create()
                ->from('ArticleGroup c')
                ->where('c.shop_id = ? AND c.parent = ? AND c.enable = 1', array($this->install->defaultmarket, $this->view->id))
                ->orderBy('c.pos ASC')
                ->useQueryCache(true)
                ->execute();

            $articlegroupselect = " AND c.articlegroup_id = '" . $this->view->id . "'";

            $this->view->navigation()->findOneBy('id', $this->view->id)->setActive(true);
        }

        if ($this->view->articlegroup == false) {
            $this->_forward('notfound', 'error');
            return;
        }

        $this->view->articlegroupss = $articlegroups;

        $this->view->mode = 5;
        $this->view->mode2 = 3;
        $this->view->mode3 = 1;
        $this->view->mode4 = 2;
        $this->view->mode5 = 4;
        $this->view->mode6 = 6;
        $this->view->mode7 = 7;
        $this->view->mode8 = 8;
        $this->view->mode9 = 9;
        $select = " AND ((a.typ = 9 AND a.not_edit != 1) OR (a.a6_org_article = 0 AND a.typ = 6 AND a.not_edit != 1) OR ( a.typ = 8 AND a.upload_weblayouter ))";

        $filter = new TP_Themesfilter ();

        if (count($filter->getArticleFilter()) > 0) {

            $articles = Doctrine_Query::create()->from('Article a')
                ->leftJoin('a.ArticleGroupArticle c')->where('a.shop_id = ? AND a.private = 0
                                AND a.enable = 1' . $select . ' AND a.id IN (SELECT d.article_id
    FROM ArticleThemeArticle as d WHERE
    d.theme_id = ' . implode(' OR d.theme_id =', array_keys($filter->getArticleFilter())) . '
    GROUP BY d.article_id)' . $articlegroupselect, array($this->install->defaultmarket))->orderBy('a.pos ASC');

        } else {

            $articles = Doctrine_Query::create()->from('Article a')
                ->leftJoin('a.ArticleGroupArticle c')
                ->where('a.shop_id = ?' . $select . ' AND a.private = 0 AND a.enable = 1' . $articlegroupselect, array($this->install->defaultmarket))->orderBy('a.pos ASC');
        }

        if ($this->view->tag != "") {
            $m = TP_Mongo::getInstance();
            $obj = $m->articles->find(array('tags.url' => $this->view->tag));
            $tmp = array();
            foreach ($obj as $row) {
                $tmp[] = $row['_id'];
            }
            $articles->WhereIn('a.uuid', $tmp);
        }

        $this->view->sort = "nothing";
        $this->view->dir = "nothing";

        if ($this->_getParam('mode', false) != "customs") {
            $articles->leftJoin('a.OrgArticle l');
            $articles->select('a.*, (a.rate/a.rate_count) as ratesum, l.*, (a.a6_resale_price+a.motive_price+coalesce(l.a4_abpreis,0)) as sum');
        } else {
            $articles->select('a.*, (a.rate/a.rate_count) as ratesum, (a.a4_abpreis+a.motive_price) as sum');
        }

        if ($this->shop->product_sort_dir != "" && !$this->_getParam('dir', false)) {
            $this->_setParam('dir', $this->shop->product_sort_dir);
        }

        if ($this->_getParam('dir')) {
            switch ($this->_getParam('dir')) {

                case 'asc':
                    $dir = 'asc';
                    $this->view->dir = "asc";
                    break;
                case 'desc':
                default:
                    $dir = 'desc';
                    $this->view->dir = "desc";
                    break;
            }
        } else {
            $dir = 'desc';
            $this->view->dir = "desc";
        }

        if ($this->shop->product_sort != "" && !$this->_getParam('sort', false)) {
            $this->_setParam('sort', $this->shop->product_sort);
        }

        if ($this->_getParam('sort')) {
            switch ($this->_getParam('sort')) {
                case 'date' :
                    $articles->orderBy('a.created ' . $dir);
                    $this->view->sort = "date";
                    break;
                case 'buy' :
                    $articles->orderBy('a.used ' . $dir);
                    $this->view->sort = "buy";
                    break;
                case 'price' :

                    $this->view->sort = "price";
                    $articles->orderBy('sum ' . $dir);

                    break;
                case 'visits' :
                    $articles->orderBy('a.visits ' . $dir);
                    $this->view->sort = "visits";
                    break;
                case 'rate' :
                    $articles->orderBy('ratesum ' . $dir);
                    $this->view->sort = "rate";
                    break;
                case 'name' :
                    if (!$this->_getParam('dir', false)) {
                        $dir = 'asc';
                        $this->view->dir = "asc";
                    }
                    $articles->orderBy('title ' . $dir);
                    $this->view->sort = "name";
                    break;
                case 'pos' :
                    if (!$this->_getParam('pos', false)) {
                        $dir = 'asc';
                        $this->view->dir = "asc";
                    }
                    $articles->orderBy('a.pos ' . $dir);
                    $this->view->sort = "pos";
                    break;
                case 'author' :
                    if (!$this->_getParam('dir', false)) {
                        $dir = 'asc';
                        $this->view->dir = "asc";
                    }
                    $this->view->sort = "author";
                    $articles->leftJoin('a.Contact f');
                    $articles->orderBy('f.article_count ' . $dir);
                    break;
            }
        } else {
            if ($this->shop->id != 4 && $this->shop->id != 10) {
                $articles->orderBy('a.created ' . $dir);
                $this->view->sort = "date";
            }
        }
        $pager = new Doctrine_Pager($articles, $this->_getParam('page', 1), $this->shop->display_article_count);

        $this->view->articles = $pager->execute();

        $pagerLayout = new TP_Doctrine_Pager_Arrows($pager, new Doctrine_Pager_Range_Sliding(array('chunk' => 30)), '/overview/' . $this->view->articlegroup->url . '/mode/' . $this->_getParam('mode', "") . '/page/{%page_number}/sort/' . $this->_getParam('sort', "") . '/' . $dir . '/' . $this->view->tag);

        $pagerLayout->setTemplate('<a href="{%url}"><span>{%page}</span></a> ');
        $pagerLayout->setSelectedTemplate('<a href="{%url}"><span><b>{%page}</b></span></a> ');

        $this->view->paginator = $pagerLayout;
    }

}
