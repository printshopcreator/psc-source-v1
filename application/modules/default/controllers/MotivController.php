<?php

class MotivController extends TP_Controller_Action
{

    protected $_extAllow = 'ppt,inx,idml,indd,qxd,ps,cdr,cdx,jpg,gif,pdf,png,jpeg,tiff,ai,eps,ttf,svg';

    public function init() {
        parent::init();

        $contextSwitch = $this->_helper->contextSwitch();
        $contextSwitch->addActionContext('resaleuploadbeginboostrap', 'json')->addActionContext('getmotiv', 'json')->addActionContext('resaleuploadlistbootstrap', 'json')->addActionContext('addautofillmotiv', 'json')->addActionContext('resaleuploaddelete', 'json')->addActionContext('resaleuploadlist', 'json')->addActionContext('resaleuploadbegin', 'json')->setAutoJsonSerialization(true)->initContext();

        $this->view->showMotiv = true;

    }

    public function resaleAction() {

    }

    public function deleteAction() {

        if (!Zend_Auth::getInstance()->hasIdentity()) {
            $this->_forward('login', 'user', null, array('mode' => 'deletemotiv'));
        }

        $motiv = Doctrine_Query::create()->from('Motiv c')->where('c.uuid = ?', array($this->_getParam('uuid')))->fetchOne();

        if (!$motiv) {
            $this->_forward('notfound', 'error');
            return;
        }

        if ($motiv->contact_id != $this->user->id && $this->highRole->level < 40) {
            $this->_forward('noaccess', 'error');
            return;
        }

        $this->view->motiv = $motiv;

        $obj = TP_Mongo::getInstance();
        $obj = $obj->articles->find(array('motive.uuid' => $motiv->uuid));

        if ($this->getRequest()->isPost()) {

            if ($obj->count() == 0) {
                unlink(APPLICATION_PATH . '/../' . $motiv->file_orginal);
                unlink(APPLICATION_PATH . '/../' . $motiv->file_work);
                unlink(APPLICATION_PATH . '/../' . $motiv->file_thumb);
                unlink(APPLICATION_PATH . '/../' . $motiv->file_mid);
                $motiv->delete();
                $this->view->priorityMessenger('Motiv successfull deleted', 'success');

                $this->_redirect('/motiv/mymotiv');
                return;
            } else {
                if ($this->highRole->level < 40) {
                    //$motiv->enable = 0;
                } else {
                    //$motiv->delete = 1;
                    //$motiv->save();
                }
            }
        }
    }

    public function getmotivAction() {
        $this->view->clearVars();

        $motiv = Doctrine_Query::create()->from('Motiv c')->where('c.uuid = ?', array($this->_getParam('id')))->fetchOne();
        if ($motiv) {

            require_once(APPLICATION_PATH . '/helpers/Image.php');
            $img = new TP_View_Helper_Image();
            $mode = $this->_getParam('mode', 'motivelist');
            $this->view->image_html = $img->thumbnailMotiv($motiv->title, $mode, $motiv->file_mid);
            $this->view->title = $motiv->title;
            $this->view->shop_id = $motiv->shop_id;
            $this->view->mymotiv = false;
            if (Zend_Auth::getInstance()->hasIdentity() == true && $this->user->id == $motiv->contact_id) {
                $this->view->mymotiv = true;
            }

            $this->view->info = $motiv->info;

            if (Zend_Auth::getInstance()->hasIdentity() == true) {

                $myfav = Doctrine_Query::create()
                    ->from('ContactMotiv c')
                    ->where('c.contact_id = ?',
                    array($this->user->id))
                    ->execute()->toKeyValueArray('motiv_id', 'motiv_id');
            } else {
                $motivBasket = new TP_FavMotiv();

                $myfav = $motivBasket->getMotive();
            }

            $this->view->myfav = false;

            if (in_array($motiv->id, $myfav)) {
                $this->view->myfav = true;
            }

            $this->view->rate = $motiv->getRate();
            $this->view->price = $motiv->price1;
            $this->view->success = true;
            $currency = new Zend_Currency(Zend_Registry::get('locale')->getLanguage() . '_' . Zend_Registry::get('locale')->getRegion());

            $this->view->price_html = $currency->toCurrency($motiv->price1 * 1);
            $this->view->author = $motiv->copyright;

            return;
        }

        $this->view->success = false;
        $this->view->message = 'Not found';
    }

    public function addautofillmotivAction() {
        $this->view->clearVars();

        $motivBasket = new TP_FavMotiv();

        $motivBasket->addAutoFillMotiv($this->_getParam('article_uuid', 0), $this->_getParam('motiv_uuid', 0));

        if (Zend_Auth::getInstance()->hasIdentity()) {

            if ($this->_getParam('motiv_uuid', "") != "") {

                $motiv = Doctrine_Query::create()
                    ->from('Motiv c')
                    ->where('c.uuid = ?', array($this->_getParam('motiv_uuid', "")))
                    ->fetchOne();

                if ($motiv) {
                    $found = Doctrine_Query::create()
                        ->from('ContactMotiv c')
                        ->where('c.contact_id = ? AND c.motiv_id = ?', array($this->user->id, $motiv->id))
                        ->fetchOne();

                    if ($found == false) {
                        $contactmotiv = new ContactMotiv();
                        $contactmotiv->contact_id = $this->user->id;
                        $contactmotiv->motiv_id = $motiv->id;
                        $contactmotiv->save();
                    }
                }

            }
        } else {

            if ($this->_getParam('motiv_uuid', "") != "") {

                $motiv = Doctrine_Query::create()
                    ->from('Motiv c')
                    ->where('c.uuid = ?', array($this->_getParam('motiv_uuid', "")))
                    ->fetchOne();

                if ($motiv) {

                    $found = $motivBasket->addMotiv($motiv->id);

                }

            }
        }

        $this->view->success = true;

    }

    public function resaleuploadAction() {

        if (!Zend_Auth::getInstance()->hasIdentity()) {
            $this->_redirect('/user/login?mode=motiv_upload');
        }

        $this->view->form = new TP_Forms_Resale_MotivStep1 ();

        if ($this->getRequest()->isPost()) {
            if ($this->view->form->isValid($this->getRequest()->getParams())) {
                $this->view->form->storeInSession();
                $this->view->priorityMessenger('Motive successfull uploaded', 'success');
                //$this->_redirect ( '/' );
            }
        }
    }

    public function resaleuploaddeleteAction() {

        $this->view->motive = array();
        $images = new Zend_Session_Namespace('buymotive');

        $images->unlock();

        if ($this->_getParam('mode', '') == 'product') {
            if (!$images->product_motive) {
                $images->product_motive = array();
            }

            unset($images->product_motive[$this->_getParam('key')]);
        } else {
            if (!$images->motive) {
                $images->motive = array();
            }
            unset($images->motive[$this->_getParam('key')]);
        }
    }

    public function resaleuploadlistAction() {
        $this->view->clearVars();
        $this->view->motive = array();
        $images = new Zend_Session_Namespace('buymotive');

        $images->unlock();

        if (!$images->product_motive) {
            $images->product_motive = array();
        }

        if (!$images->motive) {
            $images->motive = array();
        }

        $basket = new TP_ResaleWizard();
        $resaleMotive = $basket->getResaleMotive();

        require_once(APPLICATION_PATH . '/helpers/Image.php');
        $img = new TP_View_Helper_Image();

        foreach ($images->product_motive as $key => $row) {

            $motiv = Doctrine_Query::create()->from('Motiv c')->where('c.uuid = ?', array($key))->fetchOne();

            $this->view->motive[$key] = array('md5' => md5($key), 'mode' => 'product', 'name' => (isset($resaleMotive['motivnames'][$key])) ? $resaleMotive['motivnames'][$key] : $motiv->title, 'thumb' => $img->image()->thumbnailMotiv($motiv->title, 'motivelist', $motiv->file_thumb, false));
        }

        foreach ($images->motive as $key => $motiv) {
            $this->view->motive[$key] = array('md5' => md5($key), 'mode' => 'upload', 'name' => (isset($resaleMotive['motivnames'][$key])) ? $resaleMotive['motivnames'][$key] : $motiv['orgfilename'], 'thumb' => $img->image()->thumbnailMotivBuy($key, $this->_getParam('mode'), $motiv['file'], null, null, false));
        }
    }

    public function resaleuploadlistbootstrapAction() {
        $this->view->clearVars();
        $this->view->motive = array();
        $this->view->files = array();
        $images = new Zend_Session_Namespace('buymotive');

        $images->unlock();

        if (!$images->product_motive) {
            $images->product_motive = array();
        }

        if (!$images->motive) {
            $images->motive = array();
        }

        $basket = new TP_ResaleWizard();
        $resaleMotive = $basket->getResaleMotive();

        require_once(APPLICATION_PATH . '/helpers/Image.php');
        $img = new TP_View_Helper_Image();

        foreach ($images->product_motive as $key => $row) {

            $motiv = Doctrine_Query::create()->from('Motiv c')->where('c.uuid = ?', array($key))->fetchOne();
            $this->view->files[] = array('delete_url' => '/motiv/resaleuploaddelete/format/json?mode=product&key=' . $key, 'key' => $key, 'md5' => md5($key), 'mode' => 'product', 'name' => (isset($resaleMotive['motivnames'][$key])) ? $resaleMotive['motivnames'][$key] : $motiv->title, 'thumbnail_url' => $img->image()->thumbnailMotiv($motiv->title, 'motivelist', $motiv->file_thumb, true), 'thumb' => $img->image()->thumbnailMotiv($motiv->title, 'motivelist', $motiv->file_thumb, false));

            $this->view->motive[] = array('delete_url' => '/motiv/resaleuploaddelete/format/json?mode=product&key=' . $key, 'key' => $key, 'md5' => md5($key), 'mode' => 'product', 'name' => (isset($resaleMotive['motivnames'][$key])) ? $resaleMotive['motivnames'][$key] : $motiv->title, 'thumbnail_url' => $img->image()->thumbnailMotiv($motiv->title, 'motivelist', $motiv->file_thumb, true), 'thumb' => $img->image()->thumbnailMotiv($motiv->title, 'motivelist', $motiv->file_thumb, false));
        }

        foreach ($images->motive as $key => $motiv) {
            $this->view->files[] = array('delete_url' => '/motiv/resaleuploaddelete/format/json?mode=upload&key=' . $key, 'key' => $key, 'md5' => md5($key), 'mode' => 'upload', 'name' => (isset($resaleMotive['motivnames'][$key])) ? $resaleMotive['motivnames'][$key] : $motiv['orgfilename'], 'thumbnail_url' => $img->image()->thumbnailMotivBuy($key, $this->_getParam('mode'), $motiv['file'], true, null, false), 'thumb' => $img->image()->thumbnailMotivBuy($key, $this->_getParam('mode'), $motiv['file'], null, null, false));

            $this->view->motive[] = array('delete_url' => '/motiv/resaleuploaddelete/format/json?mode=upload&key=' . $key, 'key' => $key, 'md5' => md5($key), 'mode' => 'upload', 'name' => (isset($resaleMotive['motivnames'][$key])) ? $resaleMotive['motivnames'][$key] : $motiv['orgfilename'], 'thumbnail_url' => $img->image()->thumbnailMotivBuy($key, $this->_getParam('mode'), $motiv['file'], true, null, false), 'thumb' => $img->image()->thumbnailMotivBuy($key, $this->_getParam('mode'), $motiv['file'], null, null, false));
        }
    }

    public function resaleuploadbeginAction() {
        $this->view->clearVars();
        $this->getHelper('layout')->disableLayout();
        $this->_helper->viewRenderer->setNoRender();

        $adapter = new Zend_File_Transfer_Adapter_Http();
        $adapter->addValidator('Extension', false, $this->_extAllow);

        if (!file_exists(APPLICATION_PATH . '/../market/guestm/' . Zend_Session::getId())) {
            mkdir(APPLICATION_PATH . '/../market/guestm/' . Zend_Session::getId(), 0777, true);
        }

        $adapter->setDestination(APPLICATION_PATH . '/../market/guestm/' . Zend_Session::getId());

        $id = sha1(mt_rand() . microtime());

        $orginalFilename = $adapter->getFileName(null, false);

        $umlaute = Array("/ä/", "/ö/", "/ü/", "/Ä/", "/Ö/", "/Ü/", "/ß/");
        $replace = Array("ae", "oe", "ue", "Ae", "Oe", "Ue", "ss");
        $filename = preg_replace($umlaute, $replace, $adapter->getFileName(null, false));
        $filter = new Zend_Filter_Alnum();
        $filename = $filter->filter($filename);

        $adapter->addFilter('Rename', APPLICATION_PATH . '/../market/guestm/' . Zend_Session::getId() . '/' . $id . '_' . $filename);

        if (!$adapter->receive() || !$adapter->isValid()) {
            Zend_Registry::get('log')->debug(print_r($adapter->getMessages(), true));
            $error = $adapter->getMessages();
            if (isset($error['fileExtensionFalse'])) {
                $this->view->error = false;
                $this->view->message = 'Falsche Extension';
            } else {
                $this->view->error = false;
                $this->view->message = 'Upload Error';
            }
            return;
        }

        $this->view->error = false;
        $this->view->message = 'Upload Finish';

        $images = new Zend_Session_Namespace('buymotive');

        $images->unlock();

        if (!$images->motive) {
            $images->motive = array();
        }

        $uuid = TP_Util::uuid();

        $images->motive[$uuid] = array(
            'file' => APPLICATION_PATH . '/../market/guestm/' . Zend_Session::getId() . '/' . $id . '_' . $filename,
            'orgfilename' => $orginalFilename
        );

    }

    public function resaleuploadbeginboostrapAction() {
        $this->view->clearVars();
        $this->getHelper('layout')->disableLayout();
        $this->_helper->viewRenderer->setNoRender();

        $adapter = new Zend_File_Transfer_Adapter_Http();
        $adapter->addValidator('Extension', false, $this->_extAllow);

        if (!file_exists(APPLICATION_PATH . '/../market/guestm/' . Zend_Session::getId())) {
            mkdir(APPLICATION_PATH . '/../market/guestm/' . Zend_Session::getId(), 0777, true);
        }

        $adapter->setDestination(APPLICATION_PATH . '/../market/guestm/' . Zend_Session::getId());

        $id = sha1(mt_rand() . microtime());

        $orginalFilename = $adapter->getFileName(null, false);

        $umlaute = Array("/ä/", "/ö/", "/ü/", "/Ä/", "/Ö/", "/Ü/", "/ß/");
        $replace = Array("ae", "oe", "ue", "Ae", "Oe", "Ue", "ss");
        $filename = preg_replace($umlaute, $replace, $adapter->getFileName(null, false));
        $filter = new Zend_Filter_Alnum();
        $filename = $filter->filter($filename);

        $adapter->addFilter('Rename', APPLICATION_PATH . '/../market/guestm/' . Zend_Session::getId() . '/' . $id . '_' . $filename);

        if (!$adapter->receive() || !$adapter->isValid()) {
            Zend_Registry::get('log')->debug(print_r($adapter->getMessages(), true));
            $error = $adapter->getMessages();
            if (isset($error['fileExtensionFalse'])) {
                $this->view->error = false;
                $this->view->message = 'Falsche Extension';
            } else {
                $this->view->error = false;
                $this->view->message = 'Upload Error';
            }
            return;
        }

        $this->view->error = false;
        $this->view->message = 'Upload Finish';

        $images = new Zend_Session_Namespace('buymotive');

        $images->unlock();

        if (!$images->motive) {
            $images->motive = array();
        }

        $uuid = TP_Util::uuid();

        $images->motive[$uuid] = array(
            'file' => APPLICATION_PATH . '/../market/guestm/' . Zend_Session::getId() . '/' . $id . '_' . $filename,
            'orgfilename' => $orginalFilename
        );

        $this->view->clearVars();
        $this->view->motive = array();
        $this->view->files = array();
        $images = new Zend_Session_Namespace('buymotive');

        $images->unlock();

        if (!$images->product_motive) {
            $images->product_motive = array();
        }

        if (!$images->motive) {
            $images->motive = array();
        }

        $basket = new TP_ResaleWizard();
        $resaleMotive = $basket->getResaleMotive();

        require_once(APPLICATION_PATH . '/helpers/Image.php');
        $img = new TP_View_Helper_Image();

        foreach ($images->product_motive as $key => $row) {

            $motiv = Doctrine_Query::create()->from('Motiv c')->where('c.uuid = ?', array($key))->fetchOne();
            $this->view->files[] = array('delete_url' => '/motiv/resaleuploaddelete/format/json?mode=product&key=' . $key, 'key' => $key, 'md5' => md5($key), 'mode' => 'product', 'name' => (isset($resaleMotive['motivnames'][$key])) ? $resaleMotive['motivnames'][$key] : $motiv->title, 'thumbnail_url' => $img->image()->thumbnailMotiv($motiv->title, 'motivelist', $motiv->file_thumb, true), 'thumb' => $img->image()->thumbnailMotiv($motiv->title, 'motivelist', $motiv->file_thumb, false));

            $this->view->motive[] = array('delete_url' => '/motiv/resaleuploaddelete/format/json?mode=product&key=' . $key, 'key' => $key, 'md5' => md5($key), 'mode' => 'product', 'name' => (isset($resaleMotive['motivnames'][$key])) ? $resaleMotive['motivnames'][$key] : $motiv->title, 'thumbnail_url' => $img->image()->thumbnailMotiv($motiv->title, 'motivelist', $motiv->file_thumb, true), 'thumb' => $img->image()->thumbnailMotiv($motiv->title, 'motivelist', $motiv->file_thumb, false));
        }

        foreach ($images->motive as $key => $motiv) {
            $this->view->files[] = array('delete_url' => '/motiv/resaleuploaddelete/format/json?mode=upload&key=' . $key, 'key' => $key, 'md5' => md5($key), 'mode' => 'upload', 'name' => (isset($resaleMotive['motivnames'][$key])) ? $resaleMotive['motivnames'][$key] : $motiv['orgfilename'], 'thumbnail_url' => $img->image()->thumbnailMotivBuy($key, $this->_getParam('mode'), $motiv['file'], true, null, false), 'thumb' => $img->image()->thumbnailMotivBuy($key, $this->_getParam('mode'), $motiv['file'], null, null, false));

            $this->view->motive[] = array('delete_url' => '/motiv/resaleuploaddelete/format/json?mode=upload&key=' . $key, 'key' => $key, 'md5' => md5($key), 'mode' => 'upload', 'name' => (isset($resaleMotive['motivnames'][$key])) ? $resaleMotive['motivnames'][$key] : $motiv['orgfilename'], 'thumbnail_url' => $img->image()->thumbnailMotivBuy($key, $this->_getParam('mode'), $motiv['file'], true, null, false), 'thumb' => $img->image()->thumbnailMotivBuy($key, $this->_getParam('mode'), $motiv['file'], null, null, false));
        }


    }

    public function clearmyAction() {
        if (Zend_Auth::getInstance()->hasIdentity() == true) {
            Doctrine_Query::create()
                ->delete('ContactMotiv c')
                ->where('c.contact_id = ?', array($this->user->id))
                ->execute();

            $this->view->priorityMessenger('Merkliste geleert', 'success');
        }

        $this->_redirect('/motiv/myfav');
    }

    public function getShop() {

        return $this->shop;
    }

    public function rateAction() {
        $this->getHelper('layout')->disableLayout();
        $this->_helper->viewRenderer->setNoRender();

        $motiv = Doctrine_Query::create()->from('Motiv c')->where('c.uuid = ?', array($this->_getParam('uuid')))->fetchOne();

        $ratings = $this->getRequest()->getCookie('rateings');
        if ($ratings) {
            $ratings = json_decode($ratings, true);
        } else {
            $ratings = array();
        }
        if (Zend_Auth::getInstance()->hasIdentity() && isset($ratings[$this->_getParam('uuid')]) == false && $motiv && intval($this->_getParam('vote')) >= 1 && intval($this->_getParam('vote')) <= 5) {
            $motiv->rate = $motiv->rate + intval($this->_getParam('vote'));
            $motiv->rate_count++;
            $motiv->save();
            $ratings[$this->_getParam('uuid')] = 1;
            $cookie = new Zend_Http_Cookie('rateings',
                json_encode($ratings),
                $this->shop->Domain[0]->name);
            setcookie("rateings", json_encode($ratings));

            TP_Queue::process('ratesend', 'global', array('data' => $motiv->toArray(), 'vote' => intval($this->_getParam('vote'))));
        }
    }

    /**
     * IndexAction
     *
     * @todo Dokumentation der IndexAction
     *
     * @return void
     */
    public function indexAction() {

        $this->view->myfav = array();

        $this->view->mode = 1;
        $this->view->mode2 = 2;
        $this->view->mode3 = 3;

        if (Zend_Auth::getInstance()->hasIdentity() == true) {

            $this->view->myfav = Doctrine_Query::create()
                ->from('ContactMotiv c')
                ->where('c.contact_id = ?',
                array($this->user->id))
                ->execute()->toKeyValueArray('motiv_id', 'motiv_id');
        } else {
            $motivBasket = new TP_FavMotiv();

            $this->view->myfav = $motivBasket->getMotive();
        }

        $filter = new TP_Themesfilter();

        if (count($filter->getMotivFilter()) > 0) {
            if ($this->shop->pmb) {
                $motive = Doctrine_Query::create()
                    ->from('Motiv c')
                    ->where('c.install_id = ? AND c.resale_market = 1 AND c.status >= 50
                            AND c.id IN (SELECT d.motiv_id
                            FROM MotivThemeMarketMotiv as d WHERE
                            d.theme_id = ' . implode(' OR d.theme_id =',
                        array_keys($filter->getMotivFilter())) . '
GROUP BY d.motiv_id)',
                    array($this->shop->install_id));
            } else {
                $motive = Doctrine_Query::create()
                    ->from('Motiv c')
                    ->where('c.shop_id = ? AND c.resale_shop = 1 AND c.status >= 50
                            AND c.id IN (SELECT d.motiv_id
                            FROM MotivThemeMotiv as d WHERE
                            d.theme_id = ' . implode(' OR d.theme_id =',
                        array_keys($filter->getMotivFilter())) . '
GROUP BY d.motiv_id)',
                    array($this->shop->id));
            }
        } else {
            if ($this->shop->pmb) {
                $motive = Doctrine_Query::create()
                    ->from('Motiv c')
                    ->where('c.install_id = ? AND c.resale_market = 1 AND c.status >= 50', array($this->shop->install_id));
            } else {
                $motive = Doctrine_Query::create()
                    ->from('Motiv c')
                    ->where('c.shop_id = ? AND c.resale_shop = 1 AND c.status >= 50', array($this->shop->id));
            }
        }
        if ($this->view->tag != "") {
            $m = TP_Mongo::getInstance();
            $obj = $m->motive->find(array('tags.url' => $this->view->tag));
            $tmp = array();
            foreach ($obj as $row) {
                $tmp[] = $row['_id'];
            }
            $motive->WhereIn('c.uuid', $tmp);
        }
        $motive->select('c.*, (c.rate/c.rate_count) as ratesum');

        $this->view->sort = "nothing";
        $this->view->dir = "nothing";

        if ($this->_getParam('dir')) {
            switch ($this->_getParam('dir')) {

                case 'asc':
                    $dir = 'asc';
                    $this->view->dir = "asc";
                    break;
                case 'desc':
                default:
                    $dir = 'desc';
                    $this->view->dir = "desc";
                    break;
            }
        } else {
            $dir = 'DESC';
            $this->view->dir = "desc";
        }

        if ($this->_getParam('sort')) {
            switch ($this->_getParam('sort')) {
                case 'date' :
                    $motive->orderBy('c.created ' . $dir);
                    $this->view->sort = "date";
                    break;
                case 'buy' :
                    $motive->orderBy('c.used ' . $dir);
                    $this->view->sort = "buy";
                    break;
                case 'price' :
                    $this->view->sort = "price";
                    $motive->orderBy('c.price3 ' . $dir);
                    break;
                case 'visits' :
                    $motive->orderBy('c.visits ' . $dir);
                    $this->view->sort = "visits";
                    break;
                case 'rate' :
                    $motive->orderBy('ratesum ' . $dir);
                    $this->view->sort = "rate";
                    break;
                case 'name' :
                    if (!$this->_getParam('dir', false)) {
                        $dir = 'ASC';
                        $this->view->dir = "asc";
                    }
                    $motive->orderBy('title ' . $dir);
                    $this->view->sort = "name";
                    break;
                case 'author' :
                    if (!$this->_getParam('dir', false)) {
                        $dir = 'ASC';
                        $this->view->dir = "asc";
                    }
                    $this->view->sort = "author";
                    $motive->leftJoin('c.Contact f');
                    $motive->orderBy('f.motiv_count ' . $dir);
                    break;
            }
        }

        $pager = new Doctrine_Pager($motive, $this->_getParam('page', 1), $this->shop->display_motiv_count);

        $this->view->motive = $pager->execute();

        $pagerLayout = new TP_Doctrine_Pager_Arrows(
            $pager,
            new Doctrine_Pager_Range_Sliding(array('chunk' => 5)),
            '/motiv/page/{%page_number}/sort/' . $this->_getParam('sort', "") . '/' . $dir . '/' . $this->view->tag
        );

        $pagerLayout->setTemplate('<a href="{%url}"><span>{%page}</span></a> ');
        $pagerLayout->setSelectedTemplate('<a href="{%url}"><span><b>{%page}</b></span></a> ');

        $this->view->paginator = $pagerLayout;
    }

    public function mymotivAction() {
        $this->view->mymotive = array();
        $this->view->myfav = array();
        $this->view->mode = 3;
        $this->view->mode2 = 1;
        $this->view->mode3 = 2;
        if (Zend_Auth::getInstance()->hasIdentity() == true) {

            $this->view->myfav = Doctrine_Query::create()
                ->from('ContactMotiv c')
                ->where('c.contact_id = ?',
                array($this->user->id))
                ->execute()->toKeyValueArray('motiv_id', 'motiv_id');

            $motive = Doctrine_Query::create()
                ->from('Motiv c')
                ->where('c.shop_id = ? AND c.contact_id = ?',
                array($this->shop->id, $this->user->id));

            $this->view->sort = "nothing";
            $this->view->dir = "nothing";

            $motive->select('c.*, (c.rate/c.rate_count) as ratesum');

            if ($this->_getParam('dir')) {
                switch ($this->_getParam('dir')) {

                    case 'asc':
                        $dir = 'asc';
                        $this->view->dir = "asc";
                        break;
                    case 'desc':
                    default:
                        $dir = 'desc';
                        $this->view->dir = "desc";
                        break;
                }
            } else {
                $dir = 'DESC';
                $this->view->dir = "desc";
            }

            if ($this->_getParam('sort')) {
                switch ($this->_getParam('sort')) {
                    case 'date' :
                        $motive->orderBy('c.created ' . $dir);
                        $this->view->sort = "date";
                        break;
                    case 'buy' :
                        $motive->orderBy('c.used ' . $dir);
                        $this->view->sort = "buy";
                        break;
                    case 'price' :
                        $this->view->sort = "price";
                        $motive->orderBy('c.price3 ' . $dir);
                        break;
                    case 'visits' :
                        $motive->orderBy('c.visits ' . $dir);
                        $this->view->sort = "visits";
                        break;
                    case 'rate' :
                        $motive->orderBy('ratesum ' . $dir);
                        $this->view->sort = "rate";
                        break;
                    case 'name' :
                        if (!$this->_getParam('dir', false)) {
                            $dir = 'ASC';
                            $this->view->dir = "asc";
                        }
                        $motive->orderBy('title ' . $dir);
                        $this->view->sort = "name";
                        break;
                    case 'author' :
                        if (!$this->_getParam('dir', false)) {
                            $dir = 'ASC';
                            $this->view->dir = "asc";
                        }
                        $this->view->sort = "author";
                        $motive->leftJoin('c.Contact f');
                        $motive->orderBy('f.motiv_count ' . $dir);
                        break;
                }
            }

            $pager = new Doctrine_Pager($motive, $this->_getParam('page', 1), $this->shop->display_motiv_count);

            $this->view->mymotive = $pager->execute();

            $pagerLayout = new TP_Doctrine_Pager_Arrows(
                $pager,
                new Doctrine_Pager_Range_Sliding(array('chunk' => 5)),
                '/motiv/mymotiv/{%page_number}/sort/' . $this->_getParam('sort', "") . '/' . $dir
            );

            $pagerLayout->setTemplate('<a href="{%url}"><span>{%page}</span></a> ');
            $pagerLayout->setSelectedTemplate('<a href="{%url}"><span><b>{%page}</b></span></a> ');

            $this->view->paginator = $pagerLayout;
        }
    }

    public function myfavAction() {
        $this->view->mymotive = array();
        $this->view->myfav = array();
        $this->view->mode = 2;
        $this->view->mode2 = 1;
        $this->view->mode3 = 3;
        if (Zend_Auth::getInstance()->hasIdentity() == true) {

            $this->view->myfav = Doctrine_Query::create()
                ->from('ContactMotiv c')
                ->leftJoin('c.Motiv as d')
                ->where('d.shop_id = ? AND c.contact_id = ?',
                array($this->shop->id, $this->user->id))
                ->execute()->toKeyValueArray('motiv_id', 'motiv_id');

            $motive = Doctrine_Query::create()
                ->from('ContactMotiv c')
                ->leftJoin('c.Motiv as d')
                ->where('d.shop_id = ? AND c.contact_id = ?',
                array($this->shop->id, $this->user->id));

            $this->view->sort = "nothing";
            $this->view->dir = "nothing";

            $motive->select('c.*,d.*, (d.rate/d.rate_count) as ratesum');

            if ($this->_getParam('dir')) {
                switch ($this->_getParam('dir')) {

                    case 'asc':
                        $dir = 'asc';
                        $this->view->dir = "asc";
                        break;
                    case 'desc':
                    default:
                        $dir = 'desc';
                        $this->view->dir = "desc";
                        break;
                }
            } else {
                $dir = 'DESC';
                $this->view->dir = "desc";
            }

            if ($this->_getParam('sort')) {
                switch ($this->_getParam('sort')) {
                    case 'date' :
                        $motive->orderBy('d.created ' . $dir);
                        $this->view->sort = "date";
                        break;
                    case 'buy' :
                        $motive->orderBy('d.used ' . $dir);
                        $this->view->sort = "buy";
                        break;
                    case 'price' :
                        $this->view->sort = "price";
                        $motive->orderBy('d.price3 ' . $dir);
                        break;
                    case 'rate' :
                        $motive->orderBy('ratesum ' . $dir);
                        $this->view->sort = "rate";
                        break;
                    case 'visits' :
                        $motive->orderBy('d.visits ' . $dir);
                        $this->view->sort = "visits";
                        break;
                    case 'name' :
                        if (!$this->_getParam('dir', false)) {
                            $dir = 'ASC';
                            $this->view->dir = "asc";
                        }
                        $motive->orderBy('title ' . $dir);
                        $this->view->sort = "name";
                        break;
                    case 'author' :
                        if (!$this->_getParam('dir', false)) {
                            $dir = 'ASC';
                            $this->view->dir = "asc";
                        }
                        $this->view->sort = "author";
                        $motive->leftJoin('c.Contact f');
                        $motive->orderBy('f.motiv_count ' . $dir);
                        break;
                }
            }

            $pager = new Doctrine_Pager($motive, $this->_getParam('page', 1), $this->shop->display_motiv_count);

            $this->view->mymotive = $pager->execute();

            $pagerLayout = new TP_Doctrine_Pager_Arrows(
                $pager,
                new Doctrine_Pager_Range_Sliding(array('chunk' => 5)),
                '/motiv/myfav/{%page_number}/sort/' . $this->_getParam('sort', "") . '/' . $dir
            );

            $pagerLayout->setTemplate('<a href="{%url}"><span>{%page}</span></a> ');
            $pagerLayout->setSelectedTemplate('<a href="{%url}"><span><b>{%page}</b></span></a> ');

            $this->view->paginator = $pagerLayout;
        } else {
            $motivBasket = new TP_FavMotiv();

            $this->view->myfav = $motivBasket->getMotive();

            $this->view->mymotive = Doctrine_Query::create()
                ->from('Motiv c')
                ->where('c.id = ' . implode(' OR c.id = ', array_values($motivBasket->getMotive())))
                ->execute();
        }
    }

    public function addfavAction() {

        if (Zend_Auth::getInstance()->hasIdentity()) {

            if ($this->_getParam('motiv', "") != "") {

                $motiv = Doctrine_Query::create()
                    ->from('Motiv c')
                    ->where('c.uuid = ?', array($this->_getParam('motiv', "")))
                    ->fetchOne();

                if ($motiv == false) {
                    $this->view->priorityMessenger('Motiv not found', 'error');
                    if ($this->_getParam('con', "") != "" && $this->_getParam('act', "") != "") {
                        $this->_redirect('/' . $this->_getParam('con', "") . '/' . $this->_getParam('act', ""));
                    } elseif ($this->_getParam('con', "") != "") {
                        $this->_redirect('/' . $this->_getParam('con', ""));
                    } else {
                        $this->_redirect('/motiv');
                    }
                }

                $found = Doctrine_Query::create()
                    ->from('ContactMotiv c')
                    ->where('c.contact_id = ? AND c.motiv_id = ?', array($this->user->id, $motiv->id))
                    ->fetchOne();

                if ($found != false) {
                    $this->view->priorityMessenger('Motiv is found', 'error');
                    if ($this->_getParam('con', "") != "" && $this->_getParam('act', "") != "") {
                        $this->_redirect('/' . $this->_getParam('con', "") . '/' . $this->_getParam('act', ""));
                    } elseif ($this->_getParam('con', "") != "") {
                        $this->_redirect('/' . $this->_getParam('con', ""));
                    } else {
                        $this->_redirect('/motiv');
                    }
                }

                $contactmotiv = new ContactMotiv();
                $contactmotiv->contact_id = $this->user->id;
                $contactmotiv->motiv_id = $motiv->id;
                $contactmotiv->save();

                $this->view->priorityMessenger('Motivfav added', 'success');

                if ($this->_getParam('con', "") != "" && $this->_getParam('act', "") != "") {
                    $this->_redirect('/' . $this->_getParam('con', "") . '/' . $this->_getParam('act', ""));
                } elseif ($this->_getParam('con', "") != "") {
                    $this->_redirect('/' . $this->_getParam('con', ""));
                } else {
                    $this->_redirect('/motiv');
                }
            }
        } else {

            if ($this->_getParam('motiv', "") != "") {

                $motiv = Doctrine_Query::create()
                    ->from('Motiv c')
                    ->where('c.uuid = ?', array($this->_getParam('motiv', "")))
                    ->fetchOne();

                if ($motiv == false) {
                    $this->view->priorityMessenger('Motiv not found', 'error');
                    if ($this->_getParam('con', "") != "" && $this->_getParam('act', "") != "") {
                        $this->_redirect('/' . $this->_getParam('con', "") . '/' . $this->_getParam('act', ""));
                    } elseif ($this->_getParam('con', "") != "") {
                        $this->_redirect('/' . $this->_getParam('con', ""));
                    } else {
                        $this->_redirect('/motiv');
                    }
                }

                $motivBasket = new TP_FavMotiv();
                $found = $motivBasket->addMotiv($motiv->id);

                if ($found == false) {
                    $this->view->priorityMessenger('Motiv is found', 'error');
                    if ($this->_getParam('con', "") != "" && $this->_getParam('act', "") != "") {
                        $this->_redirect('/' . $this->_getParam('con', "") . '/' . $this->_getParam('act', ""));
                    } elseif ($this->_getParam('con', "") != "") {
                        $this->_redirect('/' . $this->_getParam('con', ""));
                    } else {
                        $this->_redirect('/motiv');
                    }
                }

                $this->view->priorityMessenger('Motivfav added', 'success');

                if ($this->_getParam('con', "") != "" && $this->_getParam('act', "") != "") {
                    $this->_redirect('/' . $this->_getParam('con', "") . '/' . $this->_getParam('act', ""));
                } elseif ($this->_getParam('con', "") != "") {
                    $this->_redirect('/' . $this->_getParam('con', ""));
                } else {
                    $this->_redirect('/motiv');
                }
            }
        }
    }

    public function delfavAction() {

        if (Zend_Auth::getInstance()->hasIdentity()) {

            if ($this->_getParam('motiv', "") != "") {

                $motiv = Doctrine_Query::create()
                    ->from('Motiv c')
                    ->where('c.uuid = ?', array($this->_getParam('motiv', "")))
                    ->fetchOne();

                if ($motiv == false) {
                    $this->view->priorityMessenger('Motiv not found', 'error');
                    if ($this->_getParam('con', "") != "" && $this->_getParam('act', "") != "") {
                        $this->_redirect('/' . $this->_getParam('con', "") . '/' . $this->_getParam('act', ""));
                    } elseif ($this->_getParam('con', "") != "") {
                        $this->_redirect('/' . $this->_getParam('con', ""));
                    } else {
                        $this->_redirect('/motiv');
                    }
                }

                Doctrine_Query::create()
                    ->from('ContactMotiv c')
                    ->where('c.contact_id = ? AND c.motiv_id = ?', array($this->user->id, $motiv->id))
                    ->delete()->execute();

                $this->view->priorityMessenger('Motivfav deleted', 'success');

                if ($this->_getParam('con', "") != "" && $this->_getParam('act', "") != "") {
                    $this->_redirect('/' . $this->_getParam('con', "") . '/' . $this->_getParam('act', ""));
                } elseif ($this->_getParam('con', "") != "") {
                    $this->_redirect('/' . $this->_getParam('con', ""));
                } else {
                    $this->_redirect('/motiv');
                }
            }
        } else {

            if ($this->_getParam('motiv', "") != "") {

                $motiv = Doctrine_Query::create()
                    ->from('Motiv c')
                    ->where('c.uuid = ?', array($this->_getParam('motiv', "")))
                    ->fetchOne();

                if ($motiv == false) {
                    $this->view->priorityMessenger('Motiv not found', 'error');
                    if ($this->_getParam('con', "") != "" && $this->_getParam('act', "") != "") {
                        $this->_redirect('/' . $this->_getParam('con', "") . '/' . $this->_getParam('act', ""));
                    } elseif ($this->_getParam('con', "") != "") {
                        $this->_redirect('/' . $this->_getParam('con', ""));
                    } else {
                        $this->_redirect('/motiv');
                    }
                }

                $motivBasket = new TP_FavMotiv();
                $found = $motivBasket->delMotiv($motiv->id);

                $this->view->priorityMessenger('Motivfav deleted', 'success');

                if ($this->_getParam('con', "") != "" && $this->_getParam('act', "") != "") {
                    $this->_redirect('/' . $this->_getParam('con', "") . '/' . $this->_getParam('act', ""));
                } elseif ($this->_getParam('con', "") != "") {
                    $this->_redirect('/' . $this->_getParam('con', ""));
                } else {
                    $this->_redirect('/motiv');
                }
            }
        }
    }

    public function showAction() {

        if ($this->_getParam('article') != '') {
            $article = Doctrine_Query::create()
                ->from('Article c')
                ->where('c.shop_id = ?  AND c.enable = 1 AND c.url = ?', array($this->shop->id, $this->_getParam('article')))
                ->fetchOne();
        } elseif ($this->_getParam('id') != '') {
            $article = Doctrine_Query::create()
                ->from('Article c')
                ->where('c.shop_id = ?  AND c.enable = 1 AND c.id = ?', array($this->shop->id, intval($this->_getParam('id'))))
                ->fetchOne();
        } elseif ($this->_getParam('uuid') != '') {
            $article = Doctrine_Query::create()
                ->from('Article c')
                ->where('c.shop_id = ?  AND c.enable = 1 AND c.uuid = ?', array($this->shop->id, $this->_getParam('uuid')))
                ->fetchOne();
        }

        if ($article == false) {
            $this->_forward('notfound', 'error');
            return;
        }

        $this->view->article = $article;

        $this->view->form = new Zend_Form();
        $this->view->form->setAttrib('enctype', 'application/x-www-form-urlencoded');
        $this->view->form->setAttrib('action', '/article/show/' . $article->url);

        if ($this->_getParam('articlegroup', '') != "") {
            $activeNav = $this->view->navigation()->findOneById('ArticlegroupsArticle')->findByUri('/overview/' . $this->_getParam('articlegroup', ''));

            $activeNav->active = false;
            $activeNav->setClass();

            $activeNav->addPage(array(
                'label' => $article->title,
                'id' => $article->id . '_' . $article->url,
                'uri' => '/article/show/' . $article->url . '/' . $this->_getParam('articlegroup', ''),
                'active' => false
            ));

            $activeNav = $this->view->navigation()->findOneById('ArticlegroupsArticle')->findById($article->id . '_' . $article->url);

            if ($activeNav != null) {
                $activeNav->active = true;
                $activeNav->setClass("active");
            }

            $this->view->form->setAttrib('action', '/article/show/' . $article->url . '/' . $this->_getParam('articlegroup', ''));
        }
        if ($article->private == true) {

            if (Zend_Auth::getInstance()->hasIdentity() == false) {
                $this->_forward('index', 'index', 'default');
            } else {

                $ident = Zend_Auth::getInstance()->getIdentity();
                $success = false;
                foreach ($article->ContactArticle as $contactArticle) {
                    if ($contactArticle->contact_id == $ident['id']) {
                        $success = true;
                    }
                }

                if ($success == false) {
                    $this->_forward('index', 'index', 'default');
                }
            }
        }

        $articlesObjs = Zend_Registry::get('articles');

        $articleObj = new $articlesObjs[$article->typ]();
        unset($articlesObjs);

        $this->view->articleObj = $articleObj;

        if (!$this->_request->isPost()) {
            if (Zend_Auth::getInstance()->hasIdentity() == true) {
                $idend = Zend_Auth::getInstance()->getIdentity();
                $this->getRequest()->setParams($idend);
            }
        }

        $basket = new TP_Basket();

        $articleFiles = $basket->getTempProduct();
        if ($articleFiles->getSpecial() == true || $this->getRequest()->getParam('special')) {
            $this->view->special = true;
        }

        $this->view->files = $articleFiles->getFiles();

        if ($this->_getParam('load') == 1) {
            $this->getRequest()->setParams($articleFiles->getOptions());
        }

        $this->view->netto = $articleObj->createFrontend($this, $this->translate, $article);
        $this->view->steuer = ($this->view->netto / 100 * $article->mwert);
        $this->view->brutto = $this->view->netto + ($this->view->netto / 100 * $article->mwert);

        if ($this->_request->isPost() || $this->_getParam('load') == 1) {
            if ($this->_getParam('load') == 1) {
                $formData = $this->_request->getParams();
            } else {
                $formData = $this->_request->getPost();
            }

            if ($this->view->form->isValid($formData)) {

                $sessart = $basket->getTempProduct();
                $sessart->setArticleId($article->uuid);
                $sessart->setArticle($article->toArray());
                $sessart->setOptions($formData);
                $sessart->setNetto($this->view->netto);
                $sessart->setSteuer($this->view->steuer);
                $sessart->setBrutto($this->view->brutto);

                if ($this->getRequest()->getParam('special')) {
                    $sessart->setSpecial(true);
                }
                $basket->setTempProductClass($sessart);
            } else {
                if ($this->view->form) {
                    $this->view->form->populate($formData);
                }
            }
        } else {
            $sessart = $basket->getTempProduct();
            $sessart->setArticleId($article->uuid);
            $sessart->setArticle($article->toArray());
            $sessart->setNetto($this->view->netto);
            $sessart->setSteuer($this->view->steuer);
            $sessart->setBrutto($this->view->brutto);
            $basket->setTempProductClass($sessart);
        }
        $this->view->articleTemplate = $this->view->render($article['typ'] . '.phtml');

        $this->_helper->layout->setLayout('default');
    }

    public function buyAction() {
        $basket = new TP_Basket();

        $article = Doctrine_Query::create()
            ->from('Article c')
            ->where('c.shop_id = ? AND c.id = ?', array($this->shop->id, intval($this->_getParam('id'))))
            ->fetchOne();
        $sessart = $basket->getTempProduct();
        $sessart->setCount(intval($this->_getParam('count')));

        $articlesObjs = Zend_Registry::get('articles');
        $articleObj = new $articlesObjs[$article->typ]();
        unset($articlesObjs);

        if (method_exists($articleObj, 'buyPreDispatch')) {
            $articleObj->buyPreDispatch($article);
        }

        if ($sessart->getArticleId() == "") {

            if ($sessart->getArticle() == NULL) {
                $sessart->setArticleId($article->uuid);
                $sessart->setArticle($article->toArray());
            }
            $basket->setTempProductClass($sessart);
        }

        if ($basket->addProduct()
        ) {
            ;
        }
        $this->_redirect('/basket/index');
    }

    public function loadAction() {

        if ($this->_getParam('key') != "") {

            $basket = new TP_Basket();
            $article = $basket->getBasketArtikel($this->_getParam('key'));
            $article->setInLoad($this->_getParam('key'));
            $basket->setTempProductClass($article);
            $art = $article->getArticle();
            $this->_forward('show', 'article', 'default', array('id' => $art['id'], 'load' => 1));
        }
    }

}