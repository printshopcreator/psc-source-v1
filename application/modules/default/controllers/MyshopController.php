<?php

/**
 * Myshop Controller
 *
 * Stellt das Myshop fürs Frontend da
 *
 * PHP versions 4 and 5
 *
 * @category   Modul
 * @package    Default
 * @subpackage Controller
 * @author     Thomas petersomn <tpeterson@printshopcreator.de>
 * @copyright  1997-2009 Thomas Peterson
 * @license    Not Public
 * @version    SVN: $Id: UploadcenterController.php 7070 2011-12-14 14:22:09Z boonkerz $
 * @link       http://www.printshopcreator.de
 */

/**
 * Myshop Controller
 *
 * Stellt das Myshop fürs Frontend da
 *
 * PHP versions 4 and 5
 *
 * @category   Modules
 * @package    Default
 * @subpackage Controller
 * @author     Thomas petersomn <tpeterson@printshopcreator.de>
 * @copyright  1997-2009 Thomas Peterson
 * @license    Not Public
 * @version    Release: $Id: UploadcenterController.php 7070 2011-12-14 14:22:09Z boonkerz $
 * @link       http://www.printshopcreator.de
 */

class MyshopController extends TP_Controller_Action
{

    protected $myShops;

    protected $currentShop;

    public function init() {
        parent::init();

        $this->myShops = array();

        if (!Zend_Auth::getInstance()->hasIdentity()) {
            $this->_redirect('/user/login');
        }

        $settings = new TP_Shopverwaltung();

        $selector = array();

        foreach ($this->user->ShopContact as $shop) {
            if ($shop->admin == true) {
                if (!$settings->getCurrentShop()) {
                    $settings->setCurrentShop($shop->shop_id);
                }
                if ($settings->getCurrentShop() == $shop->shop_id) {
                    $this->view->current_shop = $shop->Shop;
                    $this->currentShop = $shop->Shop;
                }
                $this->myShops[$shop->shop_id] = $shop->Shop;
                $selector[$shop->shop_id] = $shop->Shop->name;
            }
        }

        if(empty($selector)) {
            $this->_redirect('/user/login');
        }

        $this->view->myshops = $this->myShops;

        $this->view->myshop_selector = new Zend_Form_Element_Select('myshop_selector');
        $this->view->myshop_selector->setLabel('Shopauswahl')
            ->addMultiOptions($selector)
            ->setValue($settings->getCurrentShop());

        $this->view->mode = 4;
    }

    /**
     * Meine Shops Overview
     *
     * @return void
     */
    public function indexAction() {

    }

    public function domainsAction() {
        $domains = Doctrine_Query::create()->from('Domain c')->where('c.shop_id = ?', array($this->currentShop->id))->execute();
        $tempdomains = array();
        foreach ($domains as $domain) {
            $tmpar = $domain->toArray();

            if (strpos($tmpar['name'], $this->install->marketdomain)) {
                $tmpar['delete'] = false;
                $tmpar['edit'] = false;
            } else {
                $tmpar['delete'] = true;
                $tmpar['edit'] = true;
            }
            $tempdomains[] = $tmpar;
        }

        $this->view->domains = $tempdomains;

    }

    public function configAction() {

        if($this->highRole->level < 26) {
            $this->_redirect('/myshop/articles');
        }

        $form = new TP_Forms_Setting_Shop($this->currentShop);
        $this->view->form = $form;

        if ($this->getRequest()->isPost()) {
            if ($this->view->form->isValid($this->getRequest()->getParams())) {
                $this->view->form->storeInSession();
                $this->view->priorityMessenger('Shopeinstellungen erfolgreich gespeichert', 'success');
                $this->_redirect('/myshop/config');
            } else {
                $this->view->priorityMessenger('Shopeinstellungen nicht gespeichert. Bitte überprüfen Sie die angaben.', 'error');
            }
        }
    }

    public function configaboutusAction() {
        $form = new TP_Forms_Setting_Aboutus($this->currentShop);
        $this->view->form = $form;

        if ($this->getRequest()->isPost()) {
            if ($this->view->form->isValid($this->getRequest()->getParams())) {
                $this->view->form->storeInSession();
                $this->view->priorityMessenger('Über uns erfolgreich gespeichert', 'success');
                $this->_redirect('/myshop/configaboutus');
            } else {
                $this->view->priorityMessenger('Über uns nicht gespeichert. Bitte überprüfen Sie die angaben.', 'error');
            }
        }
    }

    public function editslideAction() {

        $setting = new Shopsetting();
        $setting->shop_id = $this->currentShop->id;
        $setting->keyid = 'index_slides';
        $setting->value = Zend_Json::encode(array('link' => '', 'text' => '', 'image' => '', 'title' => ''));

        if ($this->_getParam('id', false)) {
            $setting = Doctrine_Query::create()->from('Shopsetting c')->where('c.shop_id = ? AND c.id = ?', array($this->currentShop->id, $this->_getParam('id', false)))->fetchOne();
        }
        $form = new TP_Forms_Setting_Slide($this->currentShop, $setting);
        $this->view->form = $form;

        if ($this->getRequest()->isPost()) {
            if ($this->view->form->isValid($this->getRequest()->getParams())) {
                $this->view->form->storeInSession();
                $this->view->priorityMessenger('Slide erfolgreich gespeichert', 'success');
                $this->_redirect('/myshop/configstart');
            }
        }
    }

    public function configstartAction() {
        $form = new TP_Forms_Setting_Startseite($this->currentShop);
        $this->view->form = $form;
        $this->view->showSlider = false;
        if ($this->view->form->mode->getValue() == 'sliderproducts' || $this->view->form->mode->getValue() == 'sliderproductsnosidenav') {
            $this->view->showSlider = true;
        }

        $slides = Doctrine_Query::create()->from('Shopsetting c')->where("c.keyid = 'index_slides' AND c.shop_id = ?", array($this->currentShop->id))->execute();
        $tempslides = array();
        foreach ($slides as $slide) {
            $tempslides[] = array_merge($slide->toArray(), Zend_Json::decode($slide->value));
        }
        $this->view->slides = $tempslides;

        if ($this->getRequest()->isPost()) {
            if ($this->view->form->isValid($this->getRequest()->getParams())) {
                $this->view->form->storeInSession();
                $this->view->priorityMessenger('Startseiteneinstellungen erfolgreich gespeichert', 'success');
                $this->_redirect('/myshop/configstart');
            } else {
                $this->view->priorityMessenger('Startseiteneinstellungen nicht gespeichert. Bitte überprüfen Sie die angaben.', 'error');
            }
        }
    }

    public function designAction() {
        $designs = array();

        if (file_exists(APPLICATION_PATH . '/design/clients/' . $this->install->DefaultMarket->uid . '/subshopdesigns/public')) {
            foreach (new DirectoryIterator(APPLICATION_PATH . '/design/clients/' . $this->install->DefaultMarket->uid . '/subshopdesigns/public') as $file) {
                if ($file == '.' || $file == '..' || $file == 'config' || $file == '.svn' || $file == 'datapacks' || $file == '.DS_Store' || $file == '._.DS_Store') {
                    continue;
                }

                if (file_exists(APPLICATION_PATH . '/design/clients/' . $this->install->DefaultMarket->uid . '/subshopdesigns/public/' . $file->getFileName() . '/config/info.ini')) {
                    $config = new Zend_Config_Ini(APPLICATION_PATH . '/design/clients/' . $this->install->DefaultMarket->uid . '/subshopdesigns/public/' . $file->getFileName() . '/config/info.ini');

                    if ($this->_getParam('id', false) == $file->getFileName()) {
                        $this->currentShop->layout = '../' . $this->install->DefaultMarket->uid . '/subshopdesigns/public/' . $file->getFileName();
                        $this->currentShop->customtemplates = 0;
                        $this->currentShop->save();
                        $this->view->priorityMessenger('Design erfolgreich geändert', 'success');
                    }

                    $selected = false;
                    if ($this->currentShop->layout == '../' . $this->install->DefaultMarket->uid . '/subshopdesigns/public/' . $file->getFileName()) {
                        $selected = true;
                    }

                    array_push($designs, array(
                        'id' => $file->getFileName(),
                        'version' => $config->version,
                        'selected' => $selected,
                        'thumbnail' => '/styles/' . $this->install->DefaultMarket->uid . '/design/subshopdesigns/public/' . $file->getFileName() . '/screenshot.png',
                        'author' => $config->author,
                        'title' => $config->title));
                }
            }
        }
        if ($this->currentShop->customtemplates == 0 && file_exists(APPLICATION_PATH . '/design/clients/' . $this->install->DefaultMarket->uid . '/' . $this->currentShop->layout . '/config/config.ini')) {
            $config = new Zend_Config_Ini(APPLICATION_PATH . '/design/clients/' . $this->install->DefaultMarket->uid . '/' . $this->currentShop->layout . '/config/config.ini');

            $config = $this->checkform($config);

            $form = new Zend_Form($config);
            $form->addAttribs(array(
                'class' => 'niceform',
                'id' => 'design_settings'));

            $form->addElement('submit', 'save', array('label' => 'Speichern'));
        } elseif ($this->currentShop->customtemplates == 0 && file_exists(APPLICATION_PATH . '/design/clients/' . $this->currentShop->uid . '/' . $this->currentShop->layout . '/config/config.ini')) {
            $config = new Zend_Config_Ini(APPLICATION_PATH . '/design/clients/' . $this->currentShop->uid . '/' . $this->currentShop->layout . '/config/config.ini');

            $config = $this->checkform($config);

            $form = new Zend_Form($config);
            $form->addAttribs(array(
                'class' => 'niceform',
                'id' => 'design_settings'));

            $form->addElement('submit', 'save', array('label' => 'Speichern'));
        } elseif (file_exists(APPLICATION_PATH . '/design/vorlagen/' . $this->currentShop->layout . '/config/config.ini')) {

            if (file_exists(APPLICATION_PATH . '/design/clients/' . $this->install->DefaultMarket->uid . '/subshopdesigns/public/' . $this->currentShop->layout . '/config/config.ini')) {
                $config = new Zend_Config_Ini(APPLICATION_PATH . '/design/clients/' . $this->install->DefaultMarket->uid . '/subshopdesigns/public/' . $this->currentShop->layout . '/config/config.ini');
            } else {
                $config = new Zend_Config_Ini(APPLICATION_PATH . '/design/vorlagen/' . $this->currentShop->layout . '/config/config.ini');
            }
            $config = $this->checkform($config);

            $form = new Zend_Form($config);
            $form->addAttribs(array(
                'class' => 'niceform',
                'id' => 'design_settings'));

            $form->addElement('submit', 'save', array('label' => 'Speichern'));
        } else {

            $form = new Zend_Form();
        }

        if ($this->getRequest()->isPost()) {
            if ($form->isValid($this->getRequest()->getParams())) {
                if ($this->currentShop->layout_settings != "") {
                    $conf = Zend_Json::decode($this->currentShop->layout_settings);
                } else {
                    $conf = array();
                }
                $tmp = array_merge($conf[$this->currentShop->layout], $form->getValues());
                $tmp['custom_css'] = $conf[$this->currentShop->layout]['custom_css'];

                if (isset($form->custom_css) && $form->custom_css->isUploaded()) {
                    $tmp['custom_css'] = file_get_contents($form->custom_css->getFilename());
                }
                if (isset($form->custom_css_delete) && $form->custom_css_delete->isChecked()) {
                    $tmp['custom_css'] = "";
                }
                if (isset($form->display_sourceproducts)) {
                    $this->currentShop->template_display_products_custom_layouter = $form->display_sourceproducts->isChecked();
                }

                $conf[$this->currentShop->layout] = $tmp;
                $this->currentShop->layout_settings = Zend_Json::encode($conf);
                $this->currentShop->save();
                Zend_Registry::set('shop', $this->currentShop->toArray());
                $this->view->priorityMessenger('Shopeinstellungen erfolgreich gespeichert', 'success');
            } else {
                $form->populate($this->getRequest()->getParams());
            }
        } else {
            if ($this->currentShop->layout_settings != "") {
                $params = Zend_Json::decode($this->currentShop->layout_settings);
                if (isset($params[$this->currentShop->layout])) {
                    $form->populate($params[$this->currentShop->layout]);
                }
            }
        }
        $this->view->form = $form;
        $this->view->designs = $designs;
    }

    public function checkform($config) {
        $temp = array('elements' => array());
        foreach ($config->elements as $key => $conf) {
            if (isset($conf->display)) {
                foreach ($conf->display as $keys => $val) {
                    if ($this->currentShop[$keys] == $val) {
                        $temp['elements'][$key] = $conf->toArray();
                    }
                }

            } else {
                $temp['elements'][$key] = $conf->toArray();
            }
        }
        return new Zend_Config($temp);
    }

    public function articlesAction() {
        if ($this->highRole->level >= 25) {
            $articles = Doctrine_Query::create()->from('Article c')->where('c.shop_id = ?', array($this->currentShop->id))->orderBy('c.pos ASC')->execute();

        } else {
            $articles = Doctrine_Query::create()->from('Article c')->where('c.contact_id = ? AND c.shop_id = ?', array($this->user->id, $this->currentShop->id))->orderBy('c.pos ASC')->execute();

        }
        $this->view->articles = $articles;
    }

    public function editarticleAction() {
        $motiv = new Article();
        $motiv->install_id = $this->install->id;
        $motiv->shop_id = $this->currentShop->id;

        if ($this->_getParam('id', false)) {
            if ($this->highRole->level >=25) {
                $motiv = Doctrine_Query::create()->from('Article c')->where('c.uuid = ?', array($this->_getParam('id', false)))->fetchOne();
            } else {
                $motiv = Doctrine_Query::create()->from('Article c')->where('c.contact_id = ? AND c.uuid = ?', array($this->user->id, $this->_getParam('id', false)))->fetchOne();
            }
        }
        $this->view->form = new TP_Forms_Setting_Article($motiv);
        if ($this->getRequest()->isPost()) {
            if ($this->view->form->isValid($this->getRequest()->getParams())) {
                $this->view->form->storeInSession();
                $this->view->priorityMessenger('Produkt erfolgreich gespeichert', 'success');
                $this->_redirect('/myshop/editarticle/' . $motiv->uuid);
            }
        }
    }

    public function copyarticleAction() {

        if ($this->_getParam('id', false)) {
            if ($this->highRole->level >=25) {
                $article = Doctrine_Query::create()->from('Article c')->where('c.uuid = ?', array($this->_getParam('id', false)))->fetchOne();
            } else {
                $article = Doctrine_Query::create()->from('Article c')->where('c.contact_id = ? AND c.uuid = ?', array($this->user->id, $this->_getParam('id', false)))->fetchOne();
            }
        }

        if($article) {

            $articleGroups = array();
            foreach($article->ArticleGroupArticle as $group) {
                $articleGroups[] = $group->articlegroup_id;
            }


            $article = $article->copy();
            $article->uuid = "";
            $article->used = 0;
            $article->template_admin = 0;
            $article->template_system_operator = 0;
            $article->title = $article->title . ' COPY';
            $article->save();

            foreach($articleGroups as $group) {
                $articleGroup = new ArticleGroupArticle();
                $articleGroup->article_id = $article->id;
                $articleGroup->articlegroup_id = $group;
                $articleGroup->save();
            }

            $articles = Zend_Registry::get('articles');

            $articleObj = new $articles[$article->typ]();

            if (method_exists($articleObj, 'copyPreDispatch')) {
                $articleObj->copyPreDispatch($article);
            }

            TP_Queue::process('myshop_article_copy', 'global', $article);
            $this->_redirect('/myshop/editarticle/' . $article->uuid);
        }

        $this->_redirect('/');
    }

    public function ordersAction() {

    }

    public function motiveAction() {
        $onlyDisplayInShop = "";
        if($this->_getParam("onlyshop")) {
            $onlyDisplayInShop = " AND c.resale_shop = 1";
        }
        $motive = Doctrine_Query::create()->from('Motiv c')->where('c.deleted = 0 AND c.shop_id = ?'.$onlyDisplayInShop, array($this->currentShop->id))->execute();
        $this->view->motive = $motive;
    }

    public function editmotivAction() {
        $motiv = new Motiv();
        $motiv->install_id = $this->install->id;
        $motiv->shop_id = $this->currentShop->id;



        if ($this->_getParam('id', false)) {
            if ($this->highRole->level >=25) {
                $motiv = Doctrine_Query::create()->from('Motiv c')->where('c.uuid = ?', array($this->_getParam('id', false)))->fetchOne();

            } else {
                $motiv = Doctrine_Query::create()->from('Motiv c')->where('c.contact_id = ? AND c.uuid = ?', array($this->user->id, $this->_getParam('id', false)))->fetchOne();

            }
        }
        $this->view->form = new TP_Forms_Setting_Motiv($motiv, $this->highRole, $this->shop);
        if ($this->getRequest()->isPost()) {
            if ($this->view->form->isValid($this->getRequest()->getParams())) {
                $this->view->form->storeInSession();
                $this->view->priorityMessenger('Motiv erfolgreich gespeichert', 'success');
                $this->_redirect('/myshop/editmotiv/' . $motiv->uuid);
            }
        }
    }

    public function articlegroupsAction() {
        $articleGroups = Doctrine_Query::create()->from('ArticleGroup c')->where('c.shop_id = ?', array($this->currentShop->id))->orderBy('c.pos ASC')->execute();
        $this->view->articlegroups = $articleGroups;
    }

    public function editarticlegroupAction() {
        $articleGroup = new ArticleGroup();
        $articleGroup->install_id = $this->install->id;
        $articleGroup->shop_id = $this->currentShop->id;
        $articleGroup->language = 'all';

        if ($this->_getParam('id', false)) {
            $articleGroup = Doctrine_Query::create()->from('ArticleGroup c')->where('c.shop_id = ? AND c.id = ?', array($this->currentShop->id, $this->_getParam('id', false)))->fetchOne();
        }
        $articleGroups = Doctrine_Query::create()->from('ArticleGroup c')->where('c.shop_id = ?', array($this->currentShop->id))->orderBy('c.pos ASC')->execute();

        $this->view->form = new TP_Forms_Setting_Articlegroup($articleGroup, $articleGroups);
        if ($this->getRequest()->isPost()) {
            if ($this->view->form->isValid($this->getRequest()->getParams())) {
                $this->view->form->storeInSession();
                $this->view->priorityMessenger('Produktgruppe erfolgreich gespeichert', 'success');
                $this->_redirect('/myshop/editarticlegroup/' . $articleGroup->id);
            }
        }
    }

    public function delarticlegroupAction() {
        $articleGroup = new ArticleGroup();
        $articleGroup->install_id = $this->install->id;
        $articleGroup->shop_id = $this->currentShop->id;

        if ($this->_getParam('id', false)) {
            $articleGroup = Doctrine_Query::create()->from('ArticleGroup c')->where('c.shop_id = ? AND c.id = ?', array($this->currentShop->id, $this->_getParam('id', false)))->fetchOne();
        }
        $this->view->articlegroup = $articleGroup;
        $this->view->form = new TP_Forms_Setting_Delarticlegroup($articleGroup);
        if ($this->getRequest()->isPost()) {
            if ($this->view->form->isValid($this->getRequest()->getParams())) {
                $this->view->form->storeInSession();
                $this->view->priorityMessenger('Produktgruppe erfolgreich gelöscht', 'success');
                $this->_redirect('/myshop/articlegroups');
            }
        }
    }

    public function delmotivAction() {

        if (!Zend_Auth::getInstance()->hasIdentity()) {
            $this->_forward('login', 'user', null, array('mode' => 'deletemotiv'));
        }

        $motiv = Doctrine_Query::create()->from('Motiv c')->where('c.uuid = ?', array($this->_getParam('uuid')))->fetchOne();

        if (!$motiv) {
            $this->_forward('notfound', 'error');
            return;
        }

        if ($motiv->contact_id != $this->user->id && $this->highRole->level < 40) {
            $this->_forward('noaccess', 'error');
            return;
        }

        $this->view->motiv = $motiv;

        $obj = TP_Mongo::getInstance();
        $obj = $obj->articles->find(array('motive.uuid' => $motiv->uuid));

        $this->view->motiv_in_use = $obj->count();

        if ($this->getRequest()->isPost()) {

            if ($obj->count() == 0) {
                unlink(APPLICATION_PATH . '/../' . $motiv->file_orginal);
                unlink(APPLICATION_PATH . '/../' . $motiv->file_work);
                unlink(APPLICATION_PATH . '/../' . $motiv->file_thumb);
                unlink(APPLICATION_PATH . '/../' . $motiv->file_mid);
                $motiv->delete();
                $this->view->priorityMessenger('Motiv successfull deleted', 'success');

                $this->_redirect('/myshop/motive');
                return;
            } else {
                if ($this->highRole->level < 40) {
                    //$motiv->enable = 0;
                } else {
                    //$motiv->delete = 1;
                    //$motiv->save();
                }
            }
        }
    }

    public function delslideAction() {
        $setting = new Shopsetting();
        $setting->shop_id = $this->currentShop->id;
        $setting->keyid = 'index_slides';
        $setting->value = Zend_Json::encode(array('link' => '', 'text' => '', 'image' => '', 'title' => ''));

        if ($this->_getParam('id', false)) {
            $setting = Doctrine_Query::create()->from('Shopsetting c')->where('c.shop_id = ? AND c.id = ?', array($this->currentShop->id, $this->_getParam('id', false)))->fetchOne();
        }
        $form = new TP_Forms_Setting_Delslide($this->currentShop, $setting);
        $this->view->form = $form;

        if ($this->getRequest()->isPost()) {
            if ($this->view->form->isValid($this->getRequest()->getParams())) {
                $this->view->form->storeInSession();
                $this->view->priorityMessenger('Slide erfolgreich gelöscht', 'success');
                $this->_redirect('/myshop/configstart');
            }
        }
    }

    public function editdomainAction() {
        $domain = new Domain();
        $domain->shop_id = $this->currentShop->id;

        if ($this->_getParam('id', false)) {
            $domain = Doctrine_Query::create()->from('Domain c')->where('c.shop_id = ? AND c.id = ?', array($this->currentShop->id, $this->_getParam('id', false)))->fetchOne();
        }

        $this->view->form = new TP_Forms_Setting_Domain($domain);
        if ($this->getRequest()->isPost()) {
            if ($this->view->form->isValid($this->getRequest()->getParams())) {
                $this->view->form->storeInSession();
                $this->view->priorityMessenger('Domain erfolgreich gespeichert', 'success');
                $this->_redirect('/myshop/editdomain/' . $domain->id);
            }
        }
    }

    public function deldomainAction() {
        $domain = new Domain();
        $domain->shop_id = $this->currentShop->id;

        $domains = Doctrine_Query::create()->from('Domain c')->where('c.shop_id = ?', array($this->currentShop->id))->execute();
        if ($domains->count() == 1) {
            $this->view->priorityMessenger('Domain kann nicht gelöscht werden', 'error');
            $this->_redirect('/myshop/domains');
        }

        if ($this->_getParam('id', false)) {
            $domain = Doctrine_Query::create()->from('Domain c')->where('c.shop_id = ? AND c.id = ?', array($this->currentShop->id, $this->_getParam('id', false)))->fetchOne();
        }

        $this->view->domain = $domain;
        $this->view->form = new TP_Forms_Setting_Deldomain($domain);
        if ($this->getRequest()->isPost()) {
            if ($this->view->form->isValid($this->getRequest()->getParams())) {
                $this->view->form->storeInSession();
                $this->view->priorityMessenger('Domain erfolgreich gelöscht', 'success');
                $this->_redirect('/myshop/domains');
            }
        }
    }

    public function editthememotivAction() {
        $theme = new MotivTheme();
        $theme->install_id = $this->install->id;
        $theme->shop_id = $this->currentShop->id;

        if ($this->_getParam('id', false)) {
            $theme = Doctrine_Query::create()->from('MotivTheme c')->where('c.shop_id = ? AND c.id = ?', array($this->currentShop->id, $this->_getParam('id', false)))->fetchOne();
        }
        $themes = Doctrine_Query::create()->from('MotivTheme c')->where('c.shop_id = ?', array($this->currentShop->id))->execute();

        $this->view->form = new TP_Forms_Setting_Thememotiv($theme, $themes);
        if ($this->getRequest()->isPost()) {
            if ($this->view->form->isValid($this->getRequest()->getParams())) {
                $this->view->form->storeInSession();
                $this->view->priorityMessenger('Motivthema erfolgreich gespeichert', 'success');
                $this->_redirect('/myshop/editthememotiv/' . $theme->id);
            }
        }
    }

    public function delthememotivAction() {
        $theme = new MotivTheme();
        $theme->install_id = $this->install->id;
        $theme->shop_id = $this->currentShop->id;

        if ($this->_getParam('id', false)) {
            $theme = Doctrine_Query::create()->from('MotivTheme c')->where('c.shop_id = ? AND c.id = ?', array($this->currentShop->id, $this->_getParam('id', false)))->fetchOne();
        }
        $this->view->theme = $theme;
        $this->view->form = new TP_Forms_Setting_Delthememotiv($theme);
        if ($this->getRequest()->isPost()) {
            if ($this->view->form->isValid($this->getRequest()->getParams())) {
                $this->view->form->storeInSession();
                $this->view->priorityMessenger('Motivthema erfolgreich gelöscht', 'success');
                $this->_redirect('/myshop/themesmotive');
            }
        }
    }

    public function editthemearticleAction() {
        $theme = new ArticleTheme();
        $theme->install_id = $this->install->id;
        $theme->shop_id = $this->currentShop->id;

        if ($this->_getParam('id', false)) {
            $theme = Doctrine_Query::create()->from('ArticleTheme c')->where('c.shop_id = ? AND c.id = ?', array($this->currentShop->id, $this->_getParam('id', false)))->fetchOne();
        }
        $themes = Doctrine_Query::create()->from('ArticleTheme c')->where('c.shop_id = ?', array($this->currentShop->id))->execute();

        $this->view->form = new TP_Forms_Setting_Themearticle($theme, $themes);
        if ($this->getRequest()->isPost()) {
            if ($this->view->form->isValid($this->getRequest()->getParams())) {
                $this->view->form->storeInSession();
                $this->view->priorityMessenger('Produktthema erfolgreich gespeichert', 'success');
                $this->_redirect('/myshop/editthemearticle/' . $theme->id);
            }
        }
    }

    public function delthemearticleAction() {
        $theme = new ArticleTheme();
        $theme->install_id = $this->install->id;
        $theme->shop_id = $this->currentShop->id;

        if ($this->_getParam('id', false)) {
            $theme = Doctrine_Query::create()->from('ArticleTheme c')->where('c.shop_id = ? AND c.id = ?', array($this->currentShop->id, $this->_getParam('id', false)))->fetchOne();
        }
        $this->view->theme = $theme;
        $this->view->form = new TP_Forms_Setting_Delthemearticle($theme);
        if ($this->getRequest()->isPost()) {
            if ($this->view->form->isValid($this->getRequest()->getParams())) {
                $this->view->form->storeInSession();
                $this->view->priorityMessenger('Produktthema erfolgreich gelöscht', 'success');
                $this->_redirect('/myshop/themesarticles');
            }
        }
    }

    public function themesmotiveAction() {
        $themes = Doctrine_Query::create()->from('MotivTheme c')->where('c.shop_id = ?', array($this->currentShop->id))->execute();
        $this->view->themes = $themes;
    }

    public function themesarticlesAction() {
        $themes = Doctrine_Query::create()->from('ArticleTheme c')->where('c.shop_id = ?', array($this->currentShop->id))->execute();
        $this->view->themes = $themes;
    }

    /**
     * Setzt den aktuellen Shop zur verwaltung
     *
     * @return void
     */
    public function switchshopAction() {

        $shop_id = $this->_getParam('id');

        if (!isset($this->myShops[$shop_id])) {
            $this->view->priorityMessenger('Shop ist nicht in deiner Liste', 'error');
            $this->_redirect('/myshop/index');
        }

        $settings = new TP_Shopverwaltung();
        $settings->setCurrentShop($shop_id);

        $this->_redirect('/myshop/config');
    }

}
