<?php

/**
 * News Controller
 *
 * Stellt das Newssytem fürs Frontend da
 *
 * PHP versions 4 and 5
 *
 * @category   Modul
 * @package    Default
 * @subpackage Controller
 * @author     Thomas petersomn <tpeterson@printshopcreator.de>
 * @copyright  1997-2009 Thomas Peterson
 * @license    Not Public
 * @version    SVN: $Id: NewsController.php 5679 2011-01-26 13:42:46Z boonkerz $
 * @link       http://www.printshopcreator.de
 */

/**
 * News Controller
 *
 * Stellt das Newssystem fürs Frontend da
 *
 * PHP versions 4 and 5
 *
 * @category   Modules
 * @package    Default
 * @subpackage Controller
 * @author     Thomas petersomn <tpeterson@printshopcreator.de>
 * @copyright  1997-2009 Thomas Peterson
 * @license    Not Public
 * @version    Release: $Id: NewsController.php 5679 2011-01-26 13:42:46Z boonkerz $
 * @link       http://www.printshopcreator.de
 */

class NewsController extends TP_Controller_Action
{
    /**
     * Default Action für die Newsanzeige
     *
     * @return void
     */
    public function indexAction() {

        $news = Doctrine_Query::create()
            ->from('News c')
            ->where('c.shop_id = ? AND c.active = 1 AND (c.language = ?
                OR c.language = \'all\')',
            array($this->shop->id, Zend_Registry::get('locale')))
            ->orderBy('c.sort_date DESC')
            ->execute();

        $this->view->news = $news;

        $this->_helper->layout->setLayout('default');
    }

    public function showAction() {

        $news = Doctrine_Query::create()
            ->from('News c')
            ->where('c.shop_id = ? AND c.active = 1 AND c.url = ?',
            array($this->shop->id, $this->_getParam('url')))
            ->fetchOne();
        
        $this->view->news = $news;

        $this->_helper->layout->setLayout('default');
    }
}