<?php


/**
 * Overview Controller
 *
 * Stellt das Overviewsystem fürs Frontend da
 * Artikelgruppen ansicht
 *
 * PHP versions 4 and 5
 *
 * @category   Modul
 * @package    Default
 * @subpackage Controller
 * @author     Thomas petersomn <tpeterson@printshopcreator.de>
 * @copyright  1997-2009 Thomas Peterson
 * @license    Not Public
 * @version    SVN: $Id: OverviewController.php 7095 2012-01-04 09:20:35Z boonkerz $
 * @link       http://www.printshopcreator.de
 */

/**
 * Overview Controller
 *
 * Stellt das Overviewsystem fürs Frontend da
 * Artikelgruppen ansicht
 *
 * PHP versions 4 and 5
 *
 * @category   Modules
 * @package    Default
 * @subpackage Controller
 * @author     Thomas petersomn <tpeterson@printshopcreator.de>
 * @copyright  1997-2009 Thomas Peterson
 * @license    Not Public
 * @version    Release: $Id: OverviewController.php 7095 2012-01-04 09:20:35Z boonkerz $
 * @link       http://www.printshopcreator.de
 */
class OverviewController extends TP_Controller_Action
{

    public function init() {
        parent::init();
        if (!$this->getRequest()->getParam('redirected', false)) {
            $this->view->showProduct = true;
        }

    }

    public function indexAction() {

        $allowIdsGroups = [1];

        if (Zend_Auth::getInstance()->hasIdentity()) {
            $user = Zend_Auth::getInstance()->getIdentity();
            $user = Doctrine_Query::create()->from('Contact m')->useQueryCache(true)->where('id = ?')->fetchOne(array(
                $user['id']));
            if ($user != false) {

                foreach($user->ContactArticlegroup as $group) {
                    $allowIdsGroups[] = $group->productgroup_id;
                }

                foreach($user->Account->AccountArticlegroup as $group) {
                    $allowIdsGroups[] = $group->productgroup_id;
                }
            }
        }

        $this->view->stock_count_error = false;

        if($this->_getParam("buy", false)) {


            $article = $this->_getParam('article_id');

            $article = Doctrine_Query::create()->from('Article c')->where('c.id = ?', array(intval($article)))->fetchOne();

            if($article && ((intval($this->_getParam('count')) <= $article->stock_count) || $article->set_config != "" || !$article->stock)) {
                $basket = new TP_Basket();
                $sessart = $basket->getTempProduct($article->uuid);

                $sessart->setArticleId($article->uuid);
                $sessart->setArticleType($article->typ);
                $sessart->setArticle($article->toArray());
                $sessart->setNetto($article->preis);
                $sessart->setCount(intval($this->_getParam('count')));
                $sessart->setSteuer($article->preis / 100 * $article->mwert);
                $sessart->setBrutto(($article->preis / 100 * $article->mwert) + $article->preis);
                $basket->setTempProductClass($sessart);
                $basket->addProduct(false, $article->uuid);
            }elseif(intval($this->_getParam('count')) > $article->stock_count){
                $this->view->stock_count_error = true;
                $this->view->stock_count_error_count = $article->stock_count;
            }

        }

        if($this->_getParam("buyjson", false)) {


            $article = $this->_getParam('uuid');

            $article = Doctrine_Query::create()->from('Article c')->where('c.uuid = ?', array(($article)))->fetchOne();

            if($article) {
                $basket = new TP_Basket();
                $sessart = $basket->getTempProduct($article->uuid);

                $sessart->setArticleId($article->uuid);
                $sessart->setArticleType($article->typ);
                $sessart->setArticle($article->toArray());
                $sessart->setNetto($sessart->getNetto());
                $sessart->setCount(intval($this->_getParam('count')));
                $sessart->setSteuer($sessart->getNetto() / 100 * $article->mwert);
                $sessart->setBrutto(($sessart->getNetto() / 100 * $article->mwert) + $sessart->getNetto());
                $basket->setTempProductClass($sessart);
                $basket->addProduct(false, $article->uuid);

                die(json_encode(array("success" => true, 'netto' => $sessart->getNetto(), 'count' => $basket->getAllCount())));
            }elseif(intval($this->_getParam('count')) > $article->stock_count){
                $this->view->stock_count_error = true;
                $this->view->stock_count_error_count = $article->stock_count;
            }

        }

        if($this->_getParam("buy_delete", false)) {


            $article = $this->_getParam('article_id');

            $article = Doctrine_Query::create()->from('Article c')->where('c.id = ?', array(intval($article)))->fetchOne();

            if($article) {
                $basket = new TP_Basket();
                $key = $basket->getBasketArtikelId($article->uuid, true);

                $basket->removeProduct($key);
            }

        }

        $defaultNamespace = new Zend_Session_Namespace('settings');

        if (isset($defaultNamespace->overview_mode) && $this->_getParam('mode') == "") {
            $this->getRequest()->setParam('mode', $defaultNamespace->overview_mode);
        }

        $group = $this->_request->getParam('id');
        $this->view->group = $group;
        $this->view->inworkCount = 0;
        $articleSession = new TP_Layoutersession();
        $this->view->inworkCount = count($articleSession->getAllLayouterArticle());

        if (!isset($group) || $group == '' || $group == 'articlegroups') {

            $this->view->id = 0;

            $articlegroups = Doctrine_Query::create()
                ->from('ArticleGroup c')
                ->where('c.shop_id = ? AND (c.private = 0 OR (c.private = 1 AND c.id IN ('.implode(",", $allowIdsGroups).'))) AND (c.parent = 0 OR c.parent IS NULL) AND c.enable = 1', array($this->shop->id))
                ->orderBy('c.pos ASC')
                ->useQueryCache(true)
                ->execute();

            $this->view->articlegroup = new ArticleGroup();
            $this->view->articlegroup->url = 'all';
            $defaultNamespace->article_overview_url = 'all';
            $articlegroupselect = " AND c.articlegroup_id = '" . $this->view->id . "'";
        } elseif (isset($group) && $group == 'all') {
            $this->view->articlegroup = new ArticleGroup();
            $this->view->articlegroup->url = 'all';

            $articlegroupselect = "";
            $articlegroups = array();
        } else {
            $this->view->articlegroup = Doctrine_Query::create()
                ->from('ArticleGroup c')
                ->where('c.shop_id = ? AND c.url = ? AND c.enable = 1', array($this->shop->id, $group))
                ->orderBy('c.pos ASC')
                ->useQueryCache(true)
                ->fetchOne();

            if ($this->view->articlegroup == false) {
                $this->_forward('notfound', 'error');
                return;
            }
            $defaultNamespace->article_overview_url = $this->view->articlegroup->url;
            $this->setHeadData($this->view->articlegroup);
            $this->view->id = $this->view->articlegroup->id;

            $articlegroups = Doctrine_Query::create()
                ->from('ArticleGroup c')
                ->where('c.shop_id = ? AND (c.private = 0 OR (c.private = 1 AND c.id IN ('.implode(",", $allowIdsGroups).'))) AND c.parent = ? AND c.enable = 1', array($this->shop->id, $this->view->id))
                ->orderBy('c.pos ASC')
                ->useQueryCache(true)
                ->execute();

            $articlegroupselect = " AND c.articlegroup_id = '" . $this->view->id . "'";
        }

        if ($this->view->articlegroup == false) {
            $this->_forward('notfound', 'error');
            return;
        }

        $allowIdsContacts = [1];

        if (Zend_Auth::getInstance()->hasIdentity()) {
            $user = Zend_Auth::getInstance()->getIdentity();
            $user = Doctrine_Query::create()->from('Contact m')->useQueryCache(true)->where('id = ?')->fetchOne(array(
                $user['id']));
            if ($user != false) {

                foreach($user->ContactArticle as $ca) {
                    $allowIdsContacts[] = $ca->article_id;
                }

                foreach($user->Account->AccountArticle as $ca) {
                    $allowIdsContacts[] = $ca->article_id;
                }
            }
        }

        $temp = [];

        foreach($articlegroups as $row) {
            $articles = Doctrine_Query::create()->from('Article a')
                ->leftJoin('a.ArticleGroupArticle c')->where('c.articlegroup_id = ? AND a.display_not_in_overview = 0 AND (a.private = 0 OR (a.private = 1 AND a.id IN ('.implode(",", $allowIdsContacts).')))', [$row->id])->execute()->count();

            if($articles > 0 || $this->checkSubGroups($row, $allowIdsGroups, $allowIdsContacts)) {
                $temp[] = $row;
            }
        }

        $this->view->articlegroupss = $temp;

        $select = "";
        $this->view->mode = 1;

        if ($this->_getParam('mode')) {
            $defaultNamespace = new Zend_Session_Namespace('settings');
            $defaultNamespace->overview_mode = $this->_getParam('mode');

            switch ($this->_getParam('mode')) {
                case 'all' :
                    $this->view->mode = 1;
                    $this->view->mode2 = 3;
                    $this->view->mode3 = 2;
                    $this->view->mode4 = 4;
                    $this->view->mode5 = 5;
                    $this->view->mode6 = 6;
                    $this->view->mode7 = 7;
                    $this->view->mode8 = 8;
                    $this->view->mode9 = 9;
                    $select = "";
                    break;
                case 'article' :
                    $this->view->mode = 2;
                    $this->view->mode2 = 3;
                    $this->view->mode3 = 1;
                    $this->view->mode4 = 4;
                    $this->view->mode5 = 5;
                    $this->view->mode6 = 6;
                    $this->view->mode7 = 7;
                    $this->view->mode8 = 8;
                    $this->view->mode9 = 9;
                    $select = " AND ((a.a6_org_article != 0) OR (a.typ != 6))";
                    break;
                case 'templates' :
                    $this->view->mode = 4;
                    $this->view->mode2 = 3;
                    $this->view->mode3 = 1;
                    $this->view->mode4 = 2;
                    $this->view->mode5 = 5;
                    $this->view->mode6 = 6;
                    $this->view->mode7 = 7;
                    $this->view->mode8 = 8;
                    $this->view->mode9 = 9;
                    $select = " AND a.resale_design = 1";
                    break;
                case 'articles' :
                    $this->view->mode = 3;
                    $this->view->mode2 = 4;
                    $this->view->mode3 = 1;
                    $this->view->mode4 = 2;
                    $this->view->mode5 = 5;
                    $this->view->mode6 = 6;
                    $this->view->mode7 = 7;
                    $this->view->mode8 = 8;
                    $this->view->mode9 = 9;
                    $select = " AND ((a.resale = 0 AND a.a6_org_article != 0 AND a.resale_design = 0) OR (a.typ != 6 AND a.typ != 9) OR ((a.typ = 6 OR a.typ = 9) AND a.not_edit = 1))";
                    break;
                case 'customs' :
                    $this->view->mode = 5;
                    $this->view->mode2 = 3;
                    $this->view->mode3 = 1;
                    $this->view->mode4 = 2;
                    $this->view->mode5 = 4;
                    $this->view->mode6 = 6;
                    $this->view->mode7 = 7;
                    $this->view->mode8 = 8;
                    $this->view->mode9 = 9;
                    $select = " AND ((a.typ = 9 AND a.not_edit != 1) OR (a.a6_org_article = 0 AND a.typ = 6 AND a.not_edit != 1) OR ( a.typ = 8 AND a.upload_weblayouter ))";
                    break;
                case 'calc' :
                    $this->view->mode = 8;
                    $this->view->mode2 = 3;
                    $this->view->mode3 = 1;
                    $this->view->mode4 = 2;
                    $this->view->mode5 = 4;
                    $this->view->mode6 = 6;
                    $this->view->mode7 = 7;
                    $this->view->mode8 = 5;
                    $this->view->mode9 = 9;
                    $select = " AND (a.typ = 1 OR a.typ = 8) ";
                    break;
                case 'stock' :
                    $this->view->mode = 9;
                    $this->view->mode2 = 3;
                    $this->view->mode3 = 1;
                    $this->view->mode4 = 2;
                    $this->view->mode5 = 4;
                    $this->view->mode6 = 6;
                    $this->view->mode7 = 7;
                    $this->view->mode8 = 8;
                    $this->view->mode9 = 5;
                    $select = " AND a.stock = 1";
                    break;
            }
        } else {
            $defaultNamespace = new Zend_Session_Namespace('settings');
            $defaultNamespace->overview_mode = "";
        }

        $filter = new TP_Themesfilter ();

        if($filter->getArticleUrl() && $this->view->articlegroup->id != $filter->getArticleUrl()) {
            if (count($filter->getArticleFilter()) > 0) {
                foreach ($filter->getArticleFilter() as $key => $value) {
                    $tp = $this->view->navigation()->findOneBy('id', 'AT' . $key);
                    if($tp) {
                        $tp->active = false;
                        $tp->setOptions(array('uri' => '/themes/product/filter/add/' . $key));
                    }
                }
            }
            $filter->clearArticleFilter();
        }

        $filter->setArticleUrl($this->view->articlegroup->id);

        $this->view->hasArticleFilter = $filter->hasArticleFilter();

        if (count($filter->getArticleFilter()) > 0) {

            if ($this->shop->pmb) {
                $articles = Doctrine_Query::create()->from('Article a')
                    ->leftJoin('a.ArticleGroupMarketArticle c')->where('a.shop_id = ? AND a.private = 0
                                AND a.enable = 1 AND a.display_not_in_overview = 0' . $select . ' AND a.id IN (SELECT d.article_id
    FROM ArticleThemeMarketArticle as d WHERE
    d.theme_id = ' . implode(' OR d.theme_id =', array_keys($filter->getArticleFilter())) . '
    GROUP BY d.article_id)' . $articlegroupselect, array($this->shop->id))->orderBy('a.pos ASC');
            } else {
                $articles = Doctrine_Query::create()->from('Article a')
                    ->leftJoin('a.ArticleGroupArticle c')->where('a.shop_id = ? AND a.private = 0
                                AND a.display_not_in_overview = 0 AND a.enable = 1' . $select . ' AND a.id IN (SELECT d.article_id
    FROM ArticleThemeArticle as d WHERE
    d.theme_id = ' . implode(' OR d.theme_id =', array_keys($filter->getArticleFilter())) . '
    GROUP BY d.article_id)' . $articlegroupselect, array($this->shop->id))->orderBy('a.pos ASC');
            }
        } else {
            if ($this->shop->pmb) {
                $articles = Doctrine_Query::create()->from('Article a')
                    ->leftJoin('a.ArticleGroupMarketArticle c')
                    ->where('a.shop_id = ?' . $select . ' AND a.display_not_in_overview = 0 AND a.private = 0 AND a.enable = 1' . $articlegroupselect, array($this->shop->id))->orderBy('a.pos ASC');
            } else {
                $articles = Doctrine_Query::create()->from('Article a')
                    ->leftJoin('a.ArticleGroupArticle c')
                    ->leftJoin('a.ContactArticle ca')
                    ->leftJoin('a.AccountArticle aa')
                    ->where('a.shop_id = ?' . $select . ' AND a.display_not_in_overview = 0 AND (a.private = 0 OR (a.private = 1 AND ca.contact_id = ?) OR (a.private = 1 AND aa.account_id = ?)) AND a.enable = 1' . $articlegroupselect, array($this->shop->id, $this->user->id, $this->user->account_id))->orderBy('a.pos ASC');
            }
        }

        if ($this->view->tag != "") {
            $m = TP_Mongo::getInstance();
            $obj = $m->articles->find(array('tags.url' => $this->view->tag));
            $tmp = array();
            foreach ($obj as $row) {
                $tmp[] = $row['_id'];
            }
            $articles->WhereIn('a.uuid', $tmp);
        }

        $this->view->sort = "nothing";
        $this->view->dir = "nothing";

        if ($this->_getParam('mode', false) != "customs") {
            $articles->leftJoin('a.OrgArticle l');
            $articles->select('a.*, (a.rate/a.rate_count) as ratesum, l.*, (a.a6_resale_price+a.motive_price+coalesce(l.a4_abpreis,0)) as sum');
        } else {
            $articles->select('a.*, (a.rate/a.rate_count) as ratesum, (a.a4_abpreis+a.motive_price) as sum');
        }

        if ($this->shop->product_sort_dir != "" && !$this->_getParam('dir', false)) {
            $this->_setParam('dir', $this->shop->product_sort_dir);
        }

        if ($this->_getParam('dir')) {
            switch ($this->_getParam('dir')) {

                case 'asc':
                    $dir = 'asc';
                    $this->view->dir = "asc";
                    break;
                case 'desc':
                default:
                    $dir = 'desc';
                    $this->view->dir = "desc";
                    break;
            }
        } else {
            $dir = 'asc';
            $this->view->dir = "asc";
        }

        if ($this->shop->product_sort != "" && !$this->_getParam('sort', false)) {
            $this->_setParam('sort', $this->shop->product_sort);
        }

        if ($this->_getParam('sort')) {
            switch ($this->_getParam('sort')) {
                case 'date' :
                    $articles->orderBy('a.created ' . $dir);
                    $this->view->sort = "date";
                    break;
                case 'buy' :
                    $articles->orderBy('a.used ' . $dir);
                    $this->view->sort = "buy";
                    break;
                case 'price' :

                    $this->view->sort = "price";
                    $articles->orderBy('sum ' . $dir);

                    break;
                case 'visits' :
                    $articles->orderBy('a.visits ' . $dir);
                    $this->view->sort = "visits";
                    break;
                case 'rate' :
                    $articles->orderBy('ratesum ' . $dir);
                    $this->view->sort = "rate";
                    break;
                case 'name' :
                    if (!$this->_getParam('dir', false)) {
                        $dir = 'asc';
                        $this->view->dir = "asc";
                    }
                    $articles->orderBy('title ' . $dir);
                    $this->view->sort = "name";
                    break;
                case 'pos' :
                    if (!$this->_getParam('pos', false)) {
                        $dir = 'asc';
                        $this->view->dir = "asc";
                    }
                    $articles->orderBy('a.pos ' . $dir);
                    $this->view->sort = "pos";
                    break;
                case 'postitle' :
                    if (!$this->_getParam('pos', false)) {
                        $dir = 'asc';
                        $this->view->dir = "asc";
                    }
                    $articles->orderBy('a.pos, a.title ' . $dir);
                    $this->view->sort = "pos";
                    break;
                case 'author' :
                    if (!$this->_getParam('dir', false)) {
                        $dir = 'asc';
                        $this->view->dir = "asc";
                    }
                    $this->view->sort = "author";
                    $articles->leftJoin('a.Contact f');
                    $articles->orderBy('f.article_count ' . $dir);
                    break;
            }
        } else {
            if ($this->shop->id != 4 && $this->shop->id != 10) {
                $articles->orderBy('a.pos ' . $dir);
                $this->view->sort = "pos";
            }
        }
        $pager = new Doctrine_Pager($articles, $this->_getParam('page', 1), $this->shop->display_article_count);

        $this->view->articles = $pager->execute();

        if($this->install->id == 7) {

            if ($this->shop->pmb) {
                $articlesTemp = Doctrine_Query::create()->from('Article a')
                    ->leftJoin('a.ArticleGroupMarketArticle c')
                    ->where('a.shop_id = ?' . $select . ' AND a.display_not_in_overview = 0 AND a.private = 0 AND a.enable = 1' . $articlegroupselect, array($this->shop->id))->orderBy('a.pos ASC')->execute();
            } else {
                $articlesTemp = Doctrine_Query::create()->from('Article a')
                    ->leftJoin('a.ArticleGroupArticle c')
                    ->where('a.shop_id = ?' . $select . ' AND a.display_not_in_overview = 0 AND a.private = 0 AND a.enable = 1' . $articlegroupselect, array($this->shop->id))->orderBy('a.pos ASC')->execute();
            }

            $artTemp = array();
            foreach($articlesTemp as $art) {
                foreach($art->ArticleThemeArticle as $theme)
                $artTemp[] = 'AT' . $theme->theme_id;
            }
            foreach($articlesTemp as $art) {
                foreach($art->ArticleThemeMarketArticle as $theme)
                    $artTemp[] = 'AT' . $theme->theme_id;
            }
            $tp = $this->view->navigation()->findOneBy('id', 'Articlethemes');
            foreach($tp->getPages() as $tpp) {

                $visible = false;
                foreach($tpp->getPages() as $tppp) {
                    if(!in_array($tppp->id, $artTemp)) {
                        $tppp->visible = false;
                    }else{
                        $visible = true;
                    }
                }
                $tpp->visible = $visible;

            }
        }

        $pagerLayout = new TP_Doctrine_Pager_Arrows($pager, new Doctrine_Pager_Range_Sliding(array('chunk' => 30)), '/overview/' . $this->view->articlegroup->url . '/mode/' . $this->_getParam('mode', "") . '/page/{%page_number}/sort/' . $this->_getParam('sort', "") . '/' . $dir . '/' . $this->view->tag);

        $pagerLayout->setTemplate('<a href="{%url}"><span>{%page}</span></a> ');
        $pagerLayout->setSelectedTemplate('<a href="{%url}"><span><b>{%page}</b></span></a> ');

        $this->view->paginator = $pagerLayout;

        if ($this->getRequest()->isPost() && !$this->_getParam('buy', false) && !$this->_getParam('buy_delete', false)) {
            TP_Queue::process('overviewpost', 'global',
                array_merge(array('page' => $this->view->articlegroup->id), $this->getRequest()->getParams()));

            $this->view->priorityMessenger('Nachricht versendet.');

            $files = new Zend_Session_Namespace('Basket');

            if (isset($files->files[$this->view->articlegroup->id])) {
                unset($files->files[$this->view->articlegroup->id]);
            }
        }
        $this->view->enable_upload = false;
        if ($this->view->articlegroup->id != 0 && $this->view->articlegroup->id != "" && strpos('t' . $this->view->articlegroup->parameter, 'enable_upload')) {
            $this->view->enable_upload = true;
            $this->view->page_id = $this->view->articlegroup->id;

            $files = new Zend_Session_Namespace('Basket');

            if (isset($files->files[$this->view->articlegroup->id])) {
                $this->view->files = $files->files[$this->view->articlegroup->id];
            }
        }
    }

    private function checkSubGroups($row, array $allowIdsGroups, array $allowIdsContacts)
    {
        $rows = Doctrine_Query::create()
            ->from('ArticleGroup a')
            ->where('a.enable = 1 AND a.parent = ? AND (a.private = 0 OR (a.private = 1 AND a.id IN ('.implode(",", $allowIdsGroups).')))', [$row->id])
            ->orderBy('a.pos ASC')->execute();

        foreach($rows as $row) {

            $articles = Doctrine_Query::create()->from('Article a')
                ->leftJoin('a.ArticleGroupArticle c')->where('c.articlegroup_id = ? AND a.display_not_in_overview = 0 AND (a.private = 0 OR (a.private = 1 AND a.id IN ('.implode(",", $allowIdsContacts).')))', [$row->id])->execute()->count();

            if($articles > 0 || $this->checkSubGroups($row, $allowIdsGroups, $allowIdsContacts)) {
                return true;
            }

        }

        return false;

    }

}
