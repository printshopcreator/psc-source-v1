<?php

/**
 * Preflight Controller
 *
 * Stellt das Preflight fürs Frontend da
 *
 * PHP versions 4 and 5
 *
 * @category   Modul
 * @package    Default
 * @subpackage Controller
 * @author     Thomas petersomn <tpeterson@printshopcreator.de>
 * @copyright  1997-2009 Thomas Peterson
 * @license    Not Public
 * @version    SVN: $Id: PreflightController.php 7319 2012-02-14 15:36:44Z boonkerz $
 * @link       http://www.printshopcreator.de
 */

/**
 * Preflight Controller
 *
 * Stellt das Preflight fürs Frontend da
 *
 * PHP versions 4 and 5
 *
 * @category   Modules
 * @package    Default
 * @subpackage Controller
 * @author     Thomas petersomn <tpeterson@printshopcreator.de>
 * @copyright  1997-2009 Thomas Peterson
 * @license    Not Public
 * @version    Release: $Id: PreflightController.php 7319 2012-02-14 15:36:44Z boonkerz $
 * @link       http://www.printshopcreator.de
 */
class PreflightController extends TP_Controller_Action
{

    protected $_extAllow = 'ppt,inx,idml,indd,qxd,ps,cdr,cdx,jpg,gif,pdf,xls,docx,png,doc,jpeg,tif,tiff,ai,eps,psd,phtml,zip,jrxml,ttf,otf,plt,csv,docx,xlsx,pptx';

    protected function FileSize($file, $setup = null) {
        $FZ = ($file && @is_file($file)) ? filesize($file) : NULL;
        $FS = array("B", "kB", "MB", "GB", "TB", "PB", "EB", "ZB", "YB");

        if (!$setup && $setup !== 0) {
            return number_format($FZ / pow(1024, $I = floor(log($FZ, 1024))), ($i >= 1) ? 2 : 0) . ' ' . $FS[$I];
        } elseif ($setup == 'INT') {
            return number_format($FZ);
        }
        else {
            return number_format($FZ / pow(1024, $setup), ($setup >= 1) ? 2 : 0) . ' ' . $FS[$setup];
        }
    }

    public function init() {
        parent::init();

        $contextSwitch = $this->_helper->contextSwitch();
        $contextSwitch
            ->addActionContext('checkpreflight', 'json')
            ->addActionContext('checkpreflightcenter', 'json')
            ->addActionContext('getuploads', 'json')
            ->addActionContext('getfile', 'json')
            ->addActionContext('uploadbootstrap', 'json')
            ->addActionContext('getfiles', 'json')
            ->addActionContext('getfilecenter', 'json')
            ->addActionContext('getfilescenter', 'json')
            ->addActionContext('upload', 'json')
            ->addActionContext('uploadcenter', 'json')
            ->setAutoJsonSerialization(true)->initContext();
    }

    public function uploadbootstrapAction() {
        $this->view->clearVars();

        $adapter = new Zend_File_Transfer_Adapter_Http();

        $adapter->addValidator('Extension', false, $this->_extAllow);

        $id = TP_Util::uuid();

        if (!file_exists('uploads/' . $this->shop->uid . '/article')) {
            mkdir('uploads/' . $this->shop->uid . '/article', 0777, true);
        }

        $adapter->setDestination('uploads/' . $this->shop->uid . '/article');

        $orginalFilename = $adapter->getFileName(null, false);

        $adapter->addFilter('Rename', 'uploads/' . $this->shop->uid . '/article/' . $id . '_' . $adapter->getFileName(null, false));

        $this->view->motive = array();
        $this->view->error = false;
        $this->view->message = 'Upload Finish';

        if (!$adapter->receive()) {
            $error = $adapter->getMessages();
            if (isset($error['fileExtensionFalse'])) {
                $arguments = array('error' => true, 'message' => $error['fileExtensionFalse']);
                $this->view->error = false;
                $this->view->message = $error['fileExtensionFalse'];
            } else {
                $arguments = array('error' => true, 'message' => 'Upload Error');
                $this->view->error = false;
                $this->view->message = 'Upload Error';
            }
            return;
        }

        TP_Queue::process('upload_in_process', 'article', 'uploads/' . $this->shop->uid . '/article/' . $adapter->getFileName(null, false));

        $basket = new TP_Basket();

        $article = $basket->getTempProduct();
        $article->setFiles($id, $adapter->getFileName(null, false), $orginalFilename, '', $this->_getParam('id', ''));
        $basket->setTempProductClass($article);

        $this->view->motive[] = array('delete_url' => 'javascript:deleteUploadPreflight(\'' . $this->_getParam('id', '') . '\',\'' . $id . '\');', 'id' => $this->_getParam('id', ''), 'uuid' => $id,
            'name' => $orginalFilename, 'size' => filesize('uploads/' . $this->shop->uid . '/article/' . $adapter->getFileName(null, false)));

        $this->getResponse()->setHeader('Content-type', 'text/plain');
    }

    public function checkpreflightAction() {

        $article = Doctrine_Query::create()->from('Article c')->where('c.shop_id = ? AND c.enable = 1 AND c.url = ?', array($this->shop->id, $this->_getParam('ajax_calc_id')))->fetchOne();

        if ($article->a1_xml != '') {
            if ($article->a6_org_article != '0' || $article->a6_org_article > 0) {
                $org = Doctrine_Core::getTable('Article')->find($article->a6_org_article);

                $xml = str_replace('\\', '', $org->a1_xml);
            } else {

                $xml = str_replace('\\', '', $article->a1_xml);
            }
        } else {
            $xml = file_get_contents(APPLICATION_PATH . '/articles/' . $this->frontend, null);
        }

        $xml = new SimpleXMLElement($xml, null, false);

        $basket = new TP_Basket();
        $row = $basket->getTempProduct()->getFile($this->_getParam('uuid'));
        $params = $this->_getAllParams();
        $calc_values = $basket->getTempProduct()->getCalcValues();

        $file = 'uploads/' . $this->shop->uid . '/article/' . $row['value'];

        $checksMessages = array();

        foreach ($xml as $article) {
            if((string)$article->name != $this->_getParam('kalk_artikel') || !$this->_getParam('kalk_artikel', false)) {
                continue;
            }
            foreach ($article->uploads->children() as $upload) {
                if ((string)$upload['id'] == $this->_getParam('type')) {
                    foreach ($upload->children() as $row) {

                        if (isset($row['select']) && $params[(string)$row['select']] != (string)$row['value']) {
                            continue;
                        }

                        if (isset($row['select1']) && $params[(string)$row['select1']] != (string)$row['value1']) {
                            continue;
                        }

                        $check = Doctrine_Query::create()->from('Preflight c')->where('c.nr = ?', array((string)$row['id']))->fetchOne();
                        $isPdf = false;
                        if ($check->check_filetype_pdf) {
                            $handle = fopen($file, "r");
                            $stringCont = fgets($handle, 8);
                            fclose($handle);
                            if ($stringCont != false && strpos($stringCont, 'PDF') == false) {
                                $checksMessages[] = array(
                                    'message' => $check->check_filetype_error_message,
                                    'nr' => $check->nr
                                );
                            } else {
                                $isPdf = true;
                            }
                        }

                        if ($isPdf && ((int)$check->check_site_count > 0 || $check->check_site_count_rel != "")) {

                            try {
                                $p = new pdflib();
                                $indoc = $p->open_pdi_document($file, "");
                                $anzahlSeiten = $p->pcos_get_number($indoc, "length:pages");
                            } catch (Exception $e) {
                                Zend_Registry::get('log')->debug($e->getMessage());
                            }
                            if ((int)$check->check_site_count > 0) {

                                if ((int)$check->check_site_count != $anzahlSeiten) {
                                    $checksMessages[] = array(
                                        'message' => str_replace("{value}", $check->check_site_count, $check->check_site_count_error_message),
                                        'nr' => $check->nr
                                    );
                                }
                            } elseif ($check->check_site_count_rel != "") {


                                if (strpos($check->check_site_count_rel, 'CV') && $calc_values[str_replace('$CV', '', $check->check_site_count_rel)] != $anzahlSeiten) {
                                    $checksMessages[] = array(
                                        'message' => str_replace("{value}", $calc_values[str_replace('$CV', '', $check->check_site_count_rel)], $check->check_site_count_error_message),
                                        'nr' => $check->nr
                                    );
                                } elseif (strpos($check->check_site_count_rel, '+')) {
                                    $sum = explode("+", $check->check_site_count_rel);
                                    $seitenCount = 0;
                                    foreach($sum as $sumKey) {
                                        $seitenCount += $this->_getParam($sumKey);
                                    }
                                    if($seitenCount != $anzahlSeiten) {
                                        $checksMessages[] = array(
                                            'message' => str_replace("{value}", $seitenCount, $check->check_site_count_error_message),
                                            'nr' => $check->nr
                                        );
                                    }

                                } elseif (strpos($check->check_site_count_rel, '*')) {
                                    $sum = explode("*", $check->check_site_count_rel);
                                    $seitenCount = 0;
                                    foreach($sum as $sumKey) {
                                        if($seitenCount == 0) {
                                            $seitenCount = $this->_getParam($sumKey);
                                        }else{
                                            $seitenCount = $seitenCount * $this->_getParam($sumKey);
                                        }
                                    }
                                    if($seitenCount != $anzahlSeiten) {
                                        $checksMessages[] = array(
                                            'message' => str_replace("{value}", $seitenCount, $check->check_site_count_error_message),
                                            'nr' => $check->nr
                                        );
                                    }

                                } elseif ($this->_getParam($check->check_site_count_rel) != $anzahlSeiten && !strpos($check->check_site_count_rel, 'CV')) {
                                    $checksMessages[] = array(
                                        'message' => str_replace("{{value_pdf}}", $anzahlSeiten, str_replace("{value}", $this->_getParam($check->check_site_count_rel), $check->check_site_count_error_message)),
                                        'nr' => $check->nr
                                    );
                                }
                            }
                        }

                        $checks = array();
                        if ($check->check_format_a0) {
                            $checks['a0'] = array(
                                'width_from' => (1189 - 3) * 2.8353,
                                'width_to' => (1189 + 3) * 2.8353,
                                'height_from' => (841 - 3) * 2.8353,
                                'height_to' => (841 + 3) * 2.8353
                            );
                        }
                        if ($check->check_format_a1) {
                            $checks['a1'] = array(
                                'width_from' => (841 - 3) * 2.8353,
                                'width_to' => (841 + 3) * 2.8353,
                                'height_from' => (594 - 3) * 2.8353,
                                'height_to' => (594 + 3) * 2.8353
                            );
                        }
                        if ($check->check_format_a2) {
                            $checks['a2'] = array(
                                'width_from' => (594 - 3) * 2.8353,
                                'width_to' => (594 + 3) * 2.8353,
                                'height_from' => (420 - 3) * 2.8353,
                                'height_to' => (420 + 3) * 2.8353
                            );
                        }
                        if ($check->check_format_a3) {
                            $checks['a3'] = array(
                                'width_from' => (420 - 3) * 2.8353,
                                'width_to' => (420 + 3) * 2.8353,
                                'height_from' => (297 - 3) * 2.8353,
                                'height_to' => (297 + 3) * 2.8353
                            );
                        }
                        if ($check->check_format_a4) {
                            $checks['a4'] = array(
                                'width_from' => (297 - 3) * 2.8353,
                                'width_to' => (297 + 3) * 2.8353,
                                'height_from' => (210 - 3) * 2.8353,
                                'height_to' => (210 + 3) * 2.8353
                            );
                        }
                        if ($check->check_format_a5) {
                            $checks['a5'] = array(
                                'width_from' => (210 - 3) * 2.8353,
                                'width_to' => (210 + 3) * 2.8353,
                                'height_from' => (148 - 3) * 2.8353,
                                'height_to' => (148 + 3) * 2.8353
                            );
                        }
                        if ($check->check_format_a6) {
                            $checks['a6'] = array(
                                'width_from' => (148 - 3) * 2.8353,
                                'width_to' => (148 + 3) * 2.8353,
                                'height_from' => (105 - 3) * 2.8353,
                                'height_to' => (105 + 3) * 2.8353
                            );
                        }
                        if ($check->check_format_a7) {
                            $checks['a6'] = array(
                                'width_from' => (105 - 3) * 2.8353,
                                'width_to' => (105 + 3) * 2.8353,
                                'height_from' => (74 - 3) * 2.8353,
                                'height_to' => (74 + 3) * 2.8353
                            );
                        }
                        if ($check->check_format_a8) {
                            $checks['a6'] = array(
                                'width_from' => (74 - 3) * 2.8353,
                                'width_to' => (74 + 3) * 2.8353,
                                'height_from' => (52 - 3) * 2.8353,
                                'height_to' => (52 + 3) * 2.8353
                            );
                        }
                        if ($check->check_format_a9) {
                            $checks['a6'] = array(
                                'width_from' => (52 - 3) * 2.8353,
                                'width_to' => (52 + 3) * 2.8353,
                                'height_from' => (37 - 3) * 2.8353,
                                'height_to' => (37 + 3) * 2.8353
                            );
                        }
                        if ($check->check_format_a10) {
                            $checks['a6'] = array(
                                'width_from' => (37 - 3) * 2.8353,
                                'width_to' => (37 + 3) * 2.8353,
                                'height_from' => (26 - 3) * 2.8353,
                                'height_to' => (26 + 3) * 2.8353
                            );
                        }
                        if ($check->check_format_custom_width_from && $check->check_format_custom_width_from != 0 && $check->check_format_custom_width_from != "") {
                            $checks['custom'] = array(
                                'width_from' => round((int)$check->check_format_custom_width_from * 2.8353),
                                'width_to' => round((int)$check->check_format_custom_width_to * 2.8353),
                                'height_from' => round((int)$check->check_format_custom_height_from * 2.8353),
                                'height_to' => round((int)$check->check_format_custom_height_to * 2.8353)
                            );
                        }

                        $boxes = array();
                        if ($check->check_format_box_media) {
                            $boxes[] = "MediaBox";
                        }
                        if ($check->check_format_box_trim) {
                            $boxes[] = "TrimBox";
                        }
                        if ($check->check_format_box_bleed) {
                            $boxes[] = "BleedBox";
                        }
                        if ($check->check_format_box_crop) {
                            $boxes[] = "Cropbox";
                        }
                        if ($check->check_format_box_art) {
                            $boxes[] = "ArtBox";
                        }

                        if ($isPdf && $check->check_fonts_embedded) {
                            $p = new pdflib();
                            $indoc = $p->open_pdi_document($file, "");

                            $fontcount = $p->pcos_get_number($indoc, "length:fonts");

                            for ($i = 0; $i < $fontcount; $i++) {

                                if($p->pcos_get_number($indoc, "fonts[" . $i . "]/embedded") == 0) {
                                    $checksMessages[] = array(
                                        'message' => $check->check_fonts_embedded_error_message,
                                        'nr' => $check->nr
                                    );
                                    break;
                                }

                            }
                        }

                        if ($isPdf && count($boxes) > 0 && count($checks) > 0) {

                            $isFormat = false;

                            $p = new pdflib();
                            $indoc = $p->open_pdi_document($file, "");
                            $anzahlSeiten = $p->pcos_get_number($indoc, "length:pages");

                            $width = round($p->pcos_get_number($indoc, "pages[" . 0 . "]/width") / 2.8353);
                            $height = round($p->pcos_get_number($indoc, "pages[" . 0 . "]/height") / 2.8353);

                            foreach ($checks as $ck) {

                                if (!$isFormat &&
                                    round($p->pcos_get_number($indoc, "pages[" . 0 . "]/width")) >= $ck['width_from'] &&
                                    round($p->pcos_get_number($indoc, "pages[" . 0 . "]/width")) <= $ck['width_to'] &&
                                    round($p->pcos_get_number($indoc, "pages[" . 0 . "]/height")) >= $ck['height_from'] &&
                                    round($p->pcos_get_number($indoc, "pages[" . 0 . "]/height")) <= $ck['height_to']
                                ) {

                                    $isFormat = true;
                                }

                                foreach ($boxes as $key => $box) {
                                    try {
                                        if (!$isFormat && ((
                                            round($p->pcos_get_number($indoc, "pages[" . 0 . "]/" . $box . "[2]")) >= $ck['width_from'] &&
                                            round($p->pcos_get_number($indoc, "pages[" . 0 . "]/" . $box . "[2]")) <= $ck['width_to'] &&
                                            round($p->pcos_get_number($indoc, "pages[" . 0 . "]/" . $box . "[3]")) >= $ck['height_from'] &&
                                            round($p->pcos_get_number($indoc, "pages[" . 0 . "]/" . $box . "[3]")) <= $ck['height_to']) ||
                                            (round($p->pcos_get_number($indoc, "pages[" . 0 . "]/" . $box . "[2]")-$p->pcos_get_number($indoc, "pages[" . 0 . "]/" . $box . "[0]")) >= $ck['width_from'] &&
                                            round($p->pcos_get_number($indoc, "pages[" . 0 . "]/" . $box . "[2]")-$p->pcos_get_number($indoc, "pages[" . 0 . "]/" . $box . "[0]")) <= $ck['width_to'] &&
                                            round($p->pcos_get_number($indoc, "pages[" . 0 . "]/" . $box . "[3]")-$p->pcos_get_number($indoc, "pages[" . 0 . "]/" . $box . "[1]")) >= $ck['height_from'] &&
                                            round($p->pcos_get_number($indoc, "pages[" . 0 . "]/" . $box . "[3]")-$p->pcos_get_number($indoc, "pages[" . 0 . "]/" . $box . "[1]")) <= $ck['height_to']))
                                        ) {

                                            $isFormat = true;
                                        }
                                    } catch (Exception $e) {

                                    }
                                }
                            }

                            if (!$isFormat && $check->check_format_orientation_quer && $p->pcos_get_number($indoc, "pages[" . 0 . "]/width") < $p->pcos_get_number($indoc, "pages[" . 0 . "]/height")) {
                                $isFormat = false;
                            }

                            if (!$isFormat && $check->check_format_orientation_hoch && $p->pcos_get_number($indoc, "pages[" . 0 . "]/width") > $p->pcos_get_number($indoc, "pages[" . 0 . "]/height")) {
                                $isFormat = false;
                            }

                            if (!$isFormat) {
                                $checksMessages[] = array(
                                    'message' => str_replace("{upload_width}", $width, str_replace("{upload_height}", $height, $check->check_format_error_message)),
                                    'nr' => $check->nr
                                );
                            }
                        }
                    }
                }
            }
        }

        $this->view->clearVars();
        $this->view->success = true;
        $sessart = $basket->getTempProduct();
        $sessart->setPreflightCheckStatus(50);
        if (count($checksMessages) > 0) {
            $this->view->success = false;
            $this->view->errors = $checksMessages;
            $sessart->setPreflightCheckStatus(70);
        }
    }

    public function checkpreflightcenterAction() {

        $row = Doctrine_Query::create()
            ->from('Upload m')
            ->where('uuid = ?', array($this->_request->getParam('uuid')))
            ->fetchOne();
        $file = 'uploads/' . $this->shop->uid . '/article/' . $row['path'];

        $calc_values = Zend_Json::decode($row->Orderspos->calc_values);
        $pro = unserialize($row->Orderspos->data);
        $proa = $pro->getArticle();

        $params = $pro->getOptions();
        $article = Doctrine_Query::create()->from('Article c')->where('c.enable = 1 AND c.id = ?', array(intval($proa['id'])))->fetchOne();

        if ($article['a1_xml'] != '') {
            if ($article['a6_org_article'] != '0' || $article['a6_org_article'] > 0) {
                $org = Doctrine_Core::getTable('Article')->find($article['a6_org_article']);

                $xml = str_replace('\\', '', $org->a1_xml);
            } else {

                $xml = str_replace('\\', '', $article['a1_xml']);
            }
        } else {
            $xml = file_get_contents(APPLICATION_PATH . '/articles/' . $this->frontend, null);
        }

        $xml = new SimpleXMLElement($xml, null, false);

        $checksMessages = array();

        foreach ($xml as $article) {
            if ($params['kalk_artikel'] == (string)$article->name) {
                foreach ($article->uploads->children() as $upload) {
                    if ((string)$upload['id'] == $this->_getParam('type')) {
                        foreach ($upload->children() as $rows) {

                            if (isset($rows['select']) && $params[(string)$rows['select']] != (string)$rows['value']) {
                                continue;
                            }

                            if (isset($rows['select1']) && $params[(string)$rows['select1']] != (string)$rows['value1']) {
                                continue;
                            }


                            $check = Doctrine_Query::create()->from('Preflight c')->where('c.nr = ?', array((string)$rows['id']))->fetchOne();
                            $isPdf = false;
                            if ($check->check_filetype_pdf) {
                                $handle = fopen($file, "r");
                                $stringCont = fgets($handle, 8);
                                fclose($handle);
                                if ($stringCont != false && strpos($stringCont, 'PDF') == false) {
                                    $checksMessages[] = array(
                                        'message' => $check->check_filetype_error_message,
                                        'nr' => $check->nr
                                    );
                                } else {
                                    $isPdf = true;
                                }
                            }

                            if ($isPdf && ((int)$check->check_site_count || $check->check_site_count_rel)) {

                                $p = new pdflib();
                                $indoc = $p->open_pdi_document($file, "");
                                $anzahlSeiten = $p->pcos_get_number($indoc, "length:pages");

                                if ((int)$check->check_site_count > 0) {

                                    if ((int)$check->check_site_count != $anzahlSeiten) {
                                        $checksMessages[] = array(
                                            'message' => str_replace("{value}", $check->check_site_count, $check->check_site_count_error_message),
                                            'nr' => $check->nr
                                        );
                                    }
                                } elseif ($check->check_site_count_rel != "") {
                                    if (strpos($check->check_site_count_rel, 'CV') && $calc_values[str_replace('$CV', '', $check->check_site_count_rel)] != $anzahlSeiten) {
                                        $checksMessages[] = array(
                                            'message' => str_replace("{value}", $calc_values[str_replace('$CV', '', $check->check_site_count_rel)], $check->check_site_count_error_message),
                                            'nr' => $check->nr
                                        );
                                    } elseif ($params[$check->check_site_count_rel] != $anzahlSeiten && !strpos($check->check_site_count_rel, 'CV')) {
                                        $checksMessages[] = array(
                                            'message' => str_replace("{value}", $params[$check->check_site_count_rel], $check->check_site_count_error_message),
                                            'nr' => $check->nr
                                        );
                                    }
                                }
                            }

                            $checks = array();
                            if ($check->check_format_a0) {
                                $checks['a0'] = array(
                                    'width_from' => (1189 - 3) * 2.8353,
                                    'width_to' => (1189 + 3) * 2.8353,
                                    'height_from' => (841 - 3) * 2.8353,
                                    'height_to' => (841 + 3) * 2.8353
                                );
                            }
                            if ($check->check_format_a1) {
                                $checks['a1'] = array(
                                    'width_from' => (841 - 3) * 2.8353,
                                    'width_to' => (841 + 3) * 2.8353,
                                    'height_from' => (594 - 3) * 2.8353,
                                    'height_to' => (594 + 3) * 2.8353
                                );
                            }
                            if ($check->check_format_a2) {
                                $checks['a2'] = array(
                                    'width_from' => (594 - 3) * 2.8353,
                                    'width_to' => (594 + 3) * 2.8353,
                                    'height_from' => (420 - 3) * 2.8353,
                                    'height_to' => (420 + 3) * 2.8353
                                );
                            }
                            if ($check->check_format_a3) {
                                $checks['a3'] = array(
                                    'width_from' => (420 - 3) * 2.8353,
                                    'width_to' => (420 + 3) * 2.8353,
                                    'height_from' => (297 - 3) * 2.8353,
                                    'height_to' => (297 + 3) * 2.8353
                                );
                            }
                            if ($check->check_format_a4) {
                                $checks['a4'] = array(
                                    'width_from' => (297 - 3) * 2.8353,
                                    'width_to' => (297 + 3) * 2.8353,
                                    'height_from' => (210 - 3) * 2.8353,
                                    'height_to' => (210 + 3) * 2.8353
                                );
                            }
                            if ($check->check_format_a5) {
                                $checks['a5'] = array(
                                    'width_from' => (210 - 3) * 2.8353,
                                    'width_to' => (210 + 3) * 2.8353,
                                    'height_from' => (148 - 3) * 2.8353,
                                    'height_to' => (148 + 3) * 2.8353
                                );
                            }
                            if ($check->check_format_a6) {
                                $checks['a6'] = array(
                                    'width_from' => (148 - 3) * 2.8353,
                                    'width_to' => (148 + 3) * 2.8353,
                                    'height_from' => (105 - 3) * 2.8353,
                                    'height_to' => (105 + 3) * 2.8353
                                );
                            }
                            if ($check->check_format_a7) {
                                $checks['a6'] = array(
                                    'width_from' => (105 - 3) * 2.8353,
                                    'width_to' => (105 + 3) * 2.8353,
                                    'height_from' => (74 - 3) * 2.8353,
                                    'height_to' => (74 + 3) * 2.8353
                                );
                            }
                            if ($check->check_format_a8) {
                                $checks['a6'] = array(
                                    'width_from' => (74 - 3) * 2.8353,
                                    'width_to' => (74 + 3) * 2.8353,
                                    'height_from' => (52 - 3) * 2.8353,
                                    'height_to' => (52 + 3) * 2.8353
                                );
                            }
                            if ($check->check_format_a9) {
                                $checks['a6'] = array(
                                    'width_from' => (52 - 3) * 2.8353,
                                    'width_to' => (52 + 3) * 2.8353,
                                    'height_from' => (37 - 3) * 2.8353,
                                    'height_to' => (37 + 3) * 2.8353
                                );
                            }
                            if ($check->check_format_a10) {
                                $checks['a6'] = array(
                                    'width_from' => (37 - 3) * 2.8353,
                                    'width_to' => (37 + 3) * 2.8353,
                                    'height_from' => (26 - 3) * 2.8353,
                                    'height_to' => (26 + 3) * 2.8353
                                );
                            }
                            if ($check->check_format_custom_width_from && $check->check_format_custom_width_from != 0 && $check->check_format_custom_width_from != "") {
                                $checks['custom'] = array(
                                    'width_from' => round((int)$check->check_format_custom_width_from * 2.8353),
                                    'width_to' => round((int)$check->check_format_custom_width_to * 2.8353),
                                    'height_from' => round((int)$check->check_format_custom_height_from * 2.8353),
                                    'height_to' => round((int)$check->check_format_custom_height_to * 2.8353)
                                );
                            }

                            $boxes = array();
                            if ($check->check_format_box_media) {
                                $boxes[] = "MediaBox";
                            }
                            if ($check->check_format_box_trim) {
                                $boxes[] = "TrimBox";
                            }
                            if ($check->check_format_box_bleed) {
                                $boxes[] = "BleedBox";
                            }
                            if ($check->check_format_box_crop) {
                                $boxes[] = "Cropbox";
                            }
                            if ($check->check_format_box_art) {
                                $boxes[] = "ArtBox";
                            }

                            if ($isPdf && $check->check_fonts_embedded) {
                                $p = new pdflib();
                                $indoc = $p->open_pdi_document($file, "");

                                $fontcount = $p->pcos_get_number($indoc, "length:fonts");

                                for ($i = 0; $i < $fontcount; $i++) {

                                    if($p->pcos_get_number($indoc, "fonts[" . $i . "]/embedded") == 0) {
                                        $checksMessages[] = array(
                                            'message' => $check->check_fonts_embedded_error_message,
                                            'nr' => $check->nr
                                        );
                                        break;
                                    }

                                }
                            }

                            if ($isPdf && count($boxes) > 0 && count($checks) > 0) {

                                $isFormat = false;

                                $p = new pdflib();
                                $indoc = $p->open_pdi_document($file, "");
                                $anzahlSeiten = $p->pcos_get_number($indoc, "length:pages");

                                $width = round($p->pcos_get_number($indoc, "pages[" . 0 . "]/width") / 2.8353);
                                $height = round($p->pcos_get_number($indoc, "pages[" . 0 . "]/height") / 2.8353);

                                foreach ($checks as $ck) {

                                    if (!$isFormat &&
                                        round($p->pcos_get_number($indoc, "pages[" . 0 . "]/width")) >= $ck['width_from'] &&
                                        round($p->pcos_get_number($indoc, "pages[" . 0 . "]/width")) <= $ck['width_to'] &&
                                        round($p->pcos_get_number($indoc, "pages[" . 0 . "]/height")) >= $ck['height_from'] &&
                                        round($p->pcos_get_number($indoc, "pages[" . 0 . "]/height")) <= $ck['height_to']
                                    ) {

                                        $isFormat = true;
                                    }

                                    foreach ($boxes as $key => $box) {
                                        try {
                                            if (!$isFormat && ((
                                                        round($p->pcos_get_number($indoc, "pages[" . 0 . "]/" . $box . "[2]")) >= $ck['width_from'] &&
                                                        round($p->pcos_get_number($indoc, "pages[" . 0 . "]/" . $box . "[2]")) <= $ck['width_to'] &&
                                                        round($p->pcos_get_number($indoc, "pages[" . 0 . "]/" . $box . "[3]")) >= $ck['height_from'] &&
                                                        round($p->pcos_get_number($indoc, "pages[" . 0 . "]/" . $box . "[3]")) <= $ck['height_to']) ||
                                                    (round($p->pcos_get_number($indoc, "pages[" . 0 . "]/" . $box . "[2]")-$p->pcos_get_number($indoc, "pages[" . 0 . "]/" . $box . "[0]")) >= $ck['width_from'] &&
                                                        round($p->pcos_get_number($indoc, "pages[" . 0 . "]/" . $box . "[2]")-$p->pcos_get_number($indoc, "pages[" . 0 . "]/" . $box . "[0]")) <= $ck['width_to'] &&
                                                        round($p->pcos_get_number($indoc, "pages[" . 0 . "]/" . $box . "[3]")-$p->pcos_get_number($indoc, "pages[" . 0 . "]/" . $box . "[1]")) >= $ck['height_from'] &&
                                                        round($p->pcos_get_number($indoc, "pages[" . 0 . "]/" . $box . "[3]")-$p->pcos_get_number($indoc, "pages[" . 0 . "]/" . $box . "[1]")) <= $ck['height_to']))
                                            ) {

                                                $isFormat = true;
                                            }
                                        } catch (Exception $e) {

                                        }
                                    }
                                }

                                if (!$isFormat && $check->check_format_orientation_quer && $p->pcos_get_number($indoc, "pages[" . 0 . "]/width") < $p->pcos_get_number($indoc, "pages[" . 0 . "]/height")) {
                                    $isFormat = false;
                                }

                                if (!$isFormat && $check->check_format_orientation_hoch && $p->pcos_get_number($indoc, "pages[" . 0 . "]/width") > $p->pcos_get_number($indoc, "pages[" . 0 . "]/height")) {
                                    $isFormat = false;
                                }

                                if (!$isFormat) {
                                    $checksMessages[] = array(
                                        'message' => str_replace("{upload_width}", $width, str_replace("{upload_height}", $height, $check->check_format_error_message)),
                                        'nr' => $check->nr
                                    );
                                }
                            }
                        }
                    }
                }
            }
        }

        $this->view->clearVars();
        $this->view->success = true;
        $row->preflight_status = 84;
        if (count($checksMessages) > 0) {
            $this->view->success = false;
            $this->view->errors = $checksMessages;
            $row->preflight_status = 80;
            $row->preflight_errors = json_encode($checksMessages);
        }
        $row->save();
    }

    public function uploadAction() {
        $this->getHelper('layout')->disableLayout();
        $this->_helper->viewRenderer->setNoRender();

        $adapter = new Zend_File_Transfer_Adapter_Http();

        $adapter->addValidator('Extension', false, $this->_extAllow);

        $id = TP_Util::uuid();

        if (!file_exists('uploads/' . $this->shop->uid . '/article')) {
            mkdir('uploads/' . $this->shop->uid . '/article', 0777, true);
        }

        $adapter->setDestination('uploads/' . $this->shop->uid . '/article');

        $orginalFilename = $adapter->getFileName(null, false);

        $adapter->addFilter('Rename', 'uploads/' . $this->shop->uid . '/article/' . $id . '_' . $adapter->getFileName(null, false));

        if (!$adapter->receive()) {
            $error = $adapter->getMessages();
            if (isset($error['fileExtensionFalse'])) {
                $arguments = array('error' => true, 'message' => $error['fileExtensionFalse']);
            } else {
                $arguments = array('error' => true, 'message' => 'Upload Error');
            }
            die(Zend_Json::encode($arguments));
        }

        TP_Queue::process('upload_in_process', 'article', 'uploads/' . $this->shop->uid . '/article/' . $adapter->getFileName(null, false));

        $basket = new TP_Basket();

        $article = $basket->getTempProduct();
        $article->setFiles($id, $adapter->getFileName(null, false), $orginalFilename, '', $this->_getParam('id', ''));
        $basket->setTempProductClass($article);

        $arguments = array('error' => false, 'message' => 'Upload Finish', 'id' => $id);

        die(Zend_Json::encode($arguments));
    }

    public function uploadcenterAction() {
        Zend_Registry::get('log')->debug('UPLOAD');
        $this->getHelper('layout')->disableLayout();
        $this->_helper->viewRenderer->setNoRender();

        $row = Doctrine_Query::create()
            ->from('Orderspos m')
            ->where('id = ?', array(intval($this->_request->getParam('id'))))
            ->fetchOne();

        // Redirect wenn der User nicht mit der Bestellung verknüpft ist
        if ($row->Orders->contact_id != $this->user->id) {
            Zend_Registry::get('log')->debug($row->Orders->contact_id . '/' . $this->user->id);
            $arguments = array('error' => true, 'message' => 'Not Allowed');
            die(Zend_Json::encode($arguments));
        }

        $adapter = new Zend_File_Transfer_Adapter_Http();
        $adapter->addValidator('Extension', false, $this->_extAllow);

        $id = $row->Orders->alias . '_' . $row->id;

        $adapter->setDestination('uploads/' . $this->shop->uid . '/article');

        $orginalFilename = $adapter->getFileName(null, false);

        $adapter->addFilter('Rename', 'uploads/' . $this->shop->uid . '/article/' . $id . '_' . $adapter->getFileName(null, false));

        if (file_exists('uploads/' . $this->shop->uid . '/article/' . $id . '_' . $adapter->getFileName(null, false))) {

            $id .= time();
            $adapter->addFilter('Rename', 'uploads/' . $this->shop->uid . '/article/' . $id . '_' . $adapter->getFileName(null, false));
        } else {
            $adapter->addFilter('Rename', 'uploads/' . $this->shop->uid . '/article/' . $id . '_' . $adapter->getFileName(null, false));
        }

        if (!$adapter->receive() || !$adapter->isValid()) {
            $error = $adapter->getMessages();
            Zend_Registry::get('log')->debug(var_dump($adapter->getMessages(), true));
            if (isset($error['fileExtensionFalse'])) {
                $arguments = array('error' => true, 'message' => "Falsche Extension");
            } else {
                $arguments = array('error' => true, 'message' => 'Upload Error');
            }
            die(Zend_Json::encode($arguments));
        }

        //TP_Queue::process('upload_in_process', 'article', 'uploads/'.$this->shop->uid.'/article/'.$adapter->getFileName(null,false));

        $upload = new Upload();

        $upload->name = $orginalFilename;
        $upload->path = $adapter->getFileName(null, false);
        $upload->orderpos_id = intval($this->_request->getParam('id'));
        $upload->typ = $this->_request->getParam('type');

        $upload->save();

        $arguments = array('error' => false, 'message' => 'Upload Finish', 'id' => $upload->uuid);

        die(Zend_Json::encode($arguments));
    }

    public function getfileAction() {

        $basket = new TP_Basket();

        $noImg = $this->_getParam('no_image', 0);

        $row = $basket->getTempProduct()->getFile($this->_getParam('uuid'));
        $row['key'] = $this->_getParam('uuid');

        $this->view->clearVars();
        $this->view->key = $row['key'];

        if ($noImg == 0) {
            require_once(APPLICATION_PATH . '/helpers/Image.php');
            $img = new TP_View_Helper_Image();
            $this->view->img = $img->image()->thumbnailImage($row['key'], 'articlelist', $row, null, null, false);
        }
        $this->view->name = $row['name'];
        $this->view->shop = $this->shop->uid;
        $this->view->value = $row['value'];
        $this->view->size = $this->FileSize('uploads/' . $this->shop->uid . '/article/' . $row['value']);

        $this->view->success = true;
    }

    public function getfilesAction() {

        $basket = new TP_Basket();

        $rows = $basket->getTempProduct()->getFiles();

        $content = array();
        foreach ($rows as $key => $row) {
            $row['key'] = $key;
            $content[] = array(
                'key' => $key,
                'chunk' => $row['chunk'],
                'what' => $row['what']
            );

        }

        $this->view->data = $content;
        $this->view->success = true;
    }

    /**
     * Gibt aktuelle Dateien zum Artikel zurück
     *
     * @return void
     */
    public function getfilecenterAction() {

        $mode = $this->_getParam('mode', 'articlelist');

        $row = Doctrine_Query::create()
            ->from('Upload m')
            ->where('uuid = ?', array($this->_getParam('uuid')))
            ->fetchOne();
        if (isset($row['uuid']) && $row['uuid'] == "") {
            $this->view->message = "File not found";
            $this->view->success = true;
            return;
        }
        $content = array();
        require_once(APPLICATION_PATH . '/helpers/Image.php');
        $img = new TP_View_Helper_Image();

        $image = array('key' => $row->id, 'value' => $row->path);
        $this->view->clearVars();
        $this->view->key = $row->uuid;
        $this->view->img = $img->image()->thumbnailImage($row->uuid, $mode, $image, null, null, false);
        $this->view->name = $row->name;
        $this->view->shop = $this->shop->uid;
        $this->view->value = $row->path;

        $this->view->success = true;
    }

    /**
     * Gibt aktuelle Dateien zum Artikel zurück
     *
     * @return void
     */
    public function getfilescenterAction() {

        $mode = $this->_getParam('mode', 'articlelist');

        $rows = Doctrine_Query::create()
            ->from('Upload m')
            ->where('orderpos_id = ? AND typ = ?', array(intval($this->_request->getParam('id')), $this->_getParam('type')))
            ->execute();

        $content = array();
        foreach ($rows as $row) {
            $content[] = array(
                'key' => $row->uuid
            );
        }
        $this->view->clearVars();
        $this->view->success = true;
        $this->view->data = $content;
    }

    public function getuploadsAction() {

        if ($this->shop->pmb) {
            if ($this->_getParam('article') != '') {
                $article = Doctrine_Query::create()->from('Article c')->where('c.enable = 1 AND c.shop_id = ? AND c.url = ?', array($this->shop->id, $this->_getParam('article')))->fetchOne();
            } elseif ($this->_getParam('id') != '') {
                $article = Doctrine_Query::create()->from('Article c')->where('c.enable = 1 AND c.shop_id = ? AND c.id = ?', array($this->shop->id, intval($this->_getParam('id'))))->fetchOne();
            } elseif ($this->_getParam('uuid') != '') {
                $article = Doctrine_Query::create()->from('Article c')->where('c.enable = 1 AND c.shop_id = ? AND c.uuid = ?', array($this->shop->id, $this->_getParam('uuid')))->fetchOne();
            }
        } else {
            if ($this->_getParam('article') != '') {
                $article = Doctrine_Query::create()->from('Article c')->where('c.enable = 1 AND c.shop_id = ? AND c.url = ?', array($this->shop->id, $this->_getParam('article')))->fetchOne();
            } elseif ($this->_getParam('id') != '') {
                $article = Doctrine_Query::create()->from('Article c')->where('c.enable = 1 AND c.shop_id = ? AND c.id = ?', array($this->shop->id, intval($this->_getParam('id'))))->fetchOne();
            } elseif ($this->_getParam('uuid') != '') {
                $article = Doctrine_Query::create()->from('Article c')->where('c.enable = 1 AND c.shop_id = ? AND c.uuid = ?', array($this->shop->id, $this->_getParam('uuid')))->fetchOne();
            }
        }

        if ($article->a1_xml != '') {
            if ($article->a6_org_article != '0' || $article->a6_org_article > 0) {
                $org = Doctrine_Core::getTable('Article')->find($article->a6_org_article);

                $xml = str_replace('\\', '', $org->a1_xml);
            } else {

                $xml = str_replace('\\', '', $article->a1_xml);
            }
        } else {
            $xml = file_get_contents(APPLICATION_PATH . '/articles/' . $this->frontend, null);
        }

        $xml = new SimpleXMLElement($xml, null, false);

        $uploads = array();
        $params = $this->_getAllParams();
        foreach ($xml as $article) {

            $up = array();
            foreach ($article->uploads->children() as $upload) {
                if (isset($upload['select']) && $params[(string)$upload['select']] != (string)$upload['value']) {
                    continue;
                }
                $up[] = array(
                    'id' => (string)$upload['id'],
                    'name' => (string)$upload['name'],
                    'description' => (string)$upload['description']
                );
            }

            $uploads[(string)$article->name] = $up;
        }
        $this->view->clearVars();
        $this->view->uploads = $uploads[$this->_getParam('which')];
        $this->view->success = true;
    }

}
