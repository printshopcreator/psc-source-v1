<?php

/**
 * Resale Controller
 *
 * Stellt das Overviewsystem fürs Frontend da
 * Artikelgruppen ansicht
 *
 * PHP versions 4 and 5
 *
 * @category   Modul
 * @package    Default
 * @subpackage Controller
 * @author     Thomas petersomn <tpeterson@printshopcreator.de>
 * @copyright  1997-2009 Thomas Peterson
 * @license    Not Public
 * @version    SVN: $Id: ResaleController.php 7301 2012-02-12 20:58:24Z boonkerz $
 * @link       http://www.printshopcreator.de
 */

/**
 * Resale Controller
 *
 * Stellt das Overviewsystem fürs Frontend da
 * Artikelgruppen ansicht
 *
 * PHP versions 4 and 5
 *
 * @category   Modules
 * @package    Default
 * @subpackage Controller
 * @author     Thomas petersomn <tpeterson@printshopcreator.de>
 * @copyright  1997-2009 Thomas Peterson
 * @license    Not Public
 * @version    Release: $Id: ResaleController.php 7301 2012-02-12 20:58:24Z boonkerz $
 * @link       http://www.printshopcreator.de
 */
class ResaleController extends TP_Controller_Action
{

    public function beginAction() {

    }

    public function init() {

        parent::init();
        $contextSwitch = $this->_helper->contextSwitch();
        $contextSwitch->addActionContext('tags', 'json')->addActionContext('savearticlegroup', 'json')->addActionContext('savearticletheme', 'json')->addActionContext('savemotivtheme', 'json')->addActionContext('getcontacts', 'json')->addActionContext('themes', 'json')->addActionContext('themesmarket', 'json')->addActionContext('articlegroups', 'json')->setAutoJsonSerialization(true)->initContext();
    }

    public function savearticlegroupAction() {

        $baseShop = Zend_Registry::get('shop');
        $baseInstall = Zend_Registry::get('install');

        $wizard = new TP_ResaleWizard();
        $shopid = $wizard->getArticleShop();

        foreach ($this->user->ShopContact as $shop) {
            if ($shopid == $shop->shop_id && $shop->admin == true) {

                $data = new Articlegroup();
                $data->shop_id = intval($shopid);
                $data->install_id = $baseInstall['id'];
                $data->enable = true;
                $filter = new TP_Filter_Badwords();
                $data->language = 'all';
                $data->title = $filter->filter($this->_getParam('title'));
                $data->parent = intval($this->_getParam('parent_id'));
                $data->save();
                break;
            }
        }

        $this->view->success = true;
    }

    public function tagsAction() {
        require_once('MongoMapReduce.php');

        $shop = Zend_Registry::get('shop');

        $mongodb = new Mongo();

        $map = '
                function()	{
                    if(this.install_id == ' . $shop['install_id'] . ') {
                        if(this.tags) {
                            this.tags.forEach(function(x)   {
                                if(x.name.indexOf("' . $this->_getParam('tag') . '") >= 0) {
                                    emit(x.name, {count: 1});
                                }
                            });
                        }
                    }
                }
        ';

        $reduce = '
                function(k, v)  {
                var total = 0;
                for (var i = 0; i < v.length; i++)  {
                    total += v[i].count;
                }
                return {count: total};
            }';

        $map_reduce = new MongoMapReduce($map, $reduce);
        $collection_name = "articles";
        $response = $map_reduce->invoke($mongodb->printshopcreator, $collection_name);
        $tmp = array();
        if ($response->valid()) {
            $count_data = $response->getCountsData();
            foreach ($response->getResultSet() as $tag) {
                $tmp[] = $tag['_id'];
            }
        } else {
            $tmp = $response->getRawResponse();
        }

        $this->_helper->json->sendJson($tmp);
    }

    public function getcontactsAction() {
        $wizard = new TP_ResaleWizard ();
        $rows = array();
        if ($this->_getParam('id') == 'all') {

            $rows = Doctrine_Query::create()->from('Contact c')->select('c.id, c.self_firstname,c.self_lastname')->leftJoin('c.ShopContact s')->where('s.shop_id = ?', array($wizard->getArticleShop()))->fetchArray();
        } elseif ($this->_getParam('id', false)) {
            $rows = Doctrine_Query::create()->from('Contact c')->select('c.id, c.self_firstname,c.self_lastname')->leftJoin('c.ShopContact s')->where('s.shop_id = ? AND c.account_id = ?', array($wizard->getArticleShop(), intval($this->_getParam('id', false))))->fetchArray();
        }

        $this->_helper->json->sendJson($rows);
    }

    public function articlegroupsAction() {

        $wizard = new TP_ResaleWizard ();

        $id = $this->_getParam('id', 0);

        $baseShop = Zend_Registry::get('shop');
        $baseInstall = Zend_Registry::get('install');

        $temparttheme = array();
        $articlethemes = Doctrine_Query::create()->select('a.title as title, a.id as id')->from('ArticleGroup as a')->where('a.shop_id = ? AND a.parent=? AND a.install_id = ?', array($wizard->getArticleShop(), $id, $baseShop ['install_id']))->execute();
        $i = 0;
        $temparttheme ['rows'] = array();
        $temparttheme ['sub'] = true;
        if ($this->_getParam('depth', false) && $this->_getParam('depth', false) >= 2) {

        } else {
            $temparttheme ['rows'] [] = array('id' => 'new', 'title' => 'Hinzufügen');
        }
        if (count($articlethemes) == 0) {
            $temparttheme ['sub'] = false;
        }
        foreach ($articlethemes as $articletheme) {
            if ($this->hasArticleGroup($articletheme->id)) {
                $temparttheme ['rows'] [] = array('id' => $articletheme->id, 'title' => $articletheme->title . ' >');
            } else {
                $temparttheme ['rows'] [] = array('id' => $articletheme->id, 'title' => $articletheme->title);
            }
        }

        $this->_helper->json->sendJson($temparttheme);
    }

    public function themesmarketAction() {

        $wizard = new TP_ResaleWizard ();

        $id = $this->_getParam('id', 0);

        $baseShop = Zend_Registry::get('shop');
        $baseInstall = Zend_Registry::get('install');

        $temparttheme = array();
        $articlethemes = Doctrine_Query::create()->select('a.title as title, a.id as id')->from('ArticleTheme as a')->where('a.shop_id = ? AND a.parent_id=? AND a.install_id = ?', array($baseInstall ['defaultmarket'], $id, $baseShop ['install_id']))->orderBy('a.title ASC')->execute();
        $i = 0;
        $temparttheme ['rows'] = array();
        $temparttheme ['sub'] = true;
        if (count($articlethemes) == 0) {
            $temparttheme ['sub'] = false;
        }
        foreach ($articlethemes as $articletheme) {
            if ($this->hasArticleTheme($articletheme->id)) {
                $temparttheme ['rows'] [] = array('id' => $articletheme->id, 'title' => $articletheme->title . ' >');
            } else {
                $temparttheme ['rows'] [] = array('id' => $articletheme->id, 'title' => $articletheme->title);
            }
        }

        $this->_helper->json->sendJson($temparttheme);
    }

    public function themesmarketmotivAction() {

        $id = $this->_getParam('id', 0);

        $baseShop = Zend_Registry::get('shop');
        $baseInstall = Zend_Registry::get('install');

        $temparttheme = array();
        $articlethemes = Doctrine_Query::create()->select('a.title as title, a.id as id')->from('MotivTheme as a')->where('a.shop_id = ? AND a.parent_id=? AND a.install_id = ?', array($baseInstall ['defaultmarket'], $id, $baseShop ['install_id']))->orderBy('a.title ASC')->execute();
        $i = 0;
        $temparttheme ['rows'] = array();
        $temparttheme ['sub'] = true;
        if (count($articlethemes) == 0) {
            $temparttheme ['sub'] = false;
        }
        foreach ($articlethemes as $articletheme) {
            if ($this->hasMotivTheme($articletheme->id)) {
                $temparttheme ['rows'] [] = array('id' => $articletheme->id, 'title' => $articletheme->title . ' >');
            } else {
                $temparttheme ['rows'] [] = array('id' => $articletheme->id, 'title' => $articletheme->title);
            }
        }

        $this->_helper->json->sendJson($temparttheme);
    }

    public function themesmotivAction() {

        $id = $this->_getParam('id', 0);

        $baseShop = Zend_Registry::get('shop');
        $baseInstall = Zend_Registry::get('install');

        $basket = new TP_ResaleWizard();
        $resaleMotive = $basket->getResaleMotive();
        $shopid = $resaleMotive['shop'];

        $temparttheme = array();
        $articlethemes = Doctrine_Query::create()->select('a.title as title, a.id as id')->from('MotivTheme as a')->where('a.shop_id = ? AND a.parent_id=? AND a.install_id = ?', array($shopid, $id, $baseShop ['install_id']))->orderBy('a.title ASC')->execute();
        $i = 0;
        $temparttheme ['rows'] = array();
        if ($this->_getParam('depth', false) && $this->_getParam('depth', false) >= 2) {

        } else {
            foreach ($this->user->ShopContact as $shop) {
                if ($shopid == $shop->shop_id && $shop->admin == true) {
                    $temparttheme ['rows'] [] = array('id' => 'new', 'title' => 'Hinzufügen');
                    break;
                }
            }
        }
        $temparttheme ['sub'] = true;
        if (count($articlethemes) == 0) {
            $temparttheme ['sub'] = false;
        }
        foreach ($articlethemes as $articletheme) {
            if ($this->hasMotivTheme($articletheme->id)) {
                $temparttheme ['rows'] [] = array('id' => $articletheme->id, 'title' => $articletheme->title . ' >');
            } else {
                $temparttheme ['rows'] [] = array('id' => $articletheme->id, 'title' => $articletheme->title);
            }
        }

        $this->_helper->json->sendJson($temparttheme);
    }

    public function savemotivthemeAction() {

        $baseShop = Zend_Registry::get('shop');
        $baseInstall = Zend_Registry::get('install');

        $wizard = new TP_ResaleWizard();
        $resaleMotive = $wizard->getResaleMotive();
        $shopid = $resaleMotive['shop'];

        foreach ($this->user->ShopContact as $shop) {
            if ($shopid == $shop->shop_id && $shop->admin == true) {

                $data = new MotivTheme();
                $data->shop_id = intval($shopid);
                $data->install_id = $baseInstall['id'];
                $filter = new TP_Filter_Badwords();
                $data->title = $filter->filter($this->_getParam('title'));
                $data->parent_id = intval($this->_getParam('parent_id'));
                $data->save();
                break;
            }
        }

        $this->view->success = true;
    }

    public function savearticlethemeAction() {

        $baseShop = Zend_Registry::get('shop');
        $baseInstall = Zend_Registry::get('install');

        $wizard = new TP_ResaleWizard();
        $shopid = $wizard->getArticleShop();

        foreach ($this->user->ShopContact as $shop) {
            if ($shopid == $shop->shop_id && $shop->admin == true) {

                $data = new ArticleTheme();
                $data->shop_id = intval($shopid);
                $data->install_id = $baseInstall['id'];
                $filter = new TP_Filter_Badwords();
                $data->title = $filter->filter($this->_getParam('title'));
                $data->parent_id = intval($this->_getParam('parent_id'));
                $data->save();
                break;
            }
        }

        $this->view->success = true;
    }

    public function shopthemesAction() {

        $wizard = new TP_ResaleWizard ();

        $id = $this->_getParam('id', 0);

        $baseShop = Zend_Registry::get('shop');
        $baseInstall = Zend_Registry::get('install');

        $temparttheme = array();
        $articlethemes = Doctrine_Query::create()->select('a.title as title, a.id as id')->from('ShopTheme as a')->where('a.shop_id = ? AND a.parent_id=? AND a.install_id = ?', array($baseInstall ['defaultmarket'], $id, $baseShop ['install_id']))->orderBy('a.title ASC')->execute();
        $i = 0;
        $temparttheme ['rows'] = array();
        $temparttheme ['sub'] = true;
        if (count($articlethemes) == 0) {
            $temparttheme ['sub'] = false;
        }
        foreach ($articlethemes as $articletheme) {
            if ($this->hasShopTheme($articletheme->id)) {
                $temparttheme ['rows'] [] = array('id' => $articletheme->id, 'title' => $articletheme->title . ' >');
            } else {
                $temparttheme ['rows'] [] = array('id' => $articletheme->id, 'title' => $articletheme->title);
            }
        }

        $this->_helper->json->sendJson($temparttheme);
    }

    public function themesAction() {

        $wizard = new TP_ResaleWizard ();

        $id = $this->_getParam('id', 0);

        $baseShop = Zend_Registry::get('shop');
        $baseInstall = Zend_Registry::get('install');

        $temparttheme = array();
        $articlethemes = Doctrine_Query::create()->select('a.title as title, a.id as id')->from('ArticleTheme as a')->where('a.shop_id = ? AND a.parent_id=? AND a.install_id = ?', array($wizard->getArticleShop(), $id, $baseShop ['install_id']))->orderBy('a.title ASC')->execute();
        $i = 0;
        $temparttheme ['rows'] = array();
        if ($this->_getParam('depth', false) && $this->_getParam('depth', false) >= 2) {

        } else {
            foreach ($this->user->ShopContact as $shop) {
                if ($wizard->getArticleShop() == $shop->shop_id && $shop->admin == true) {
                    $temparttheme ['rows'] [] = array('id' => 'new', 'title' => 'Hinzufügen');
                    break;
                }
            }
        }
        $temparttheme ['sub'] = true;
        if (count($articlethemes) == 0) {
            $temparttheme ['sub'] = false;
        }
        foreach ($articlethemes as $articletheme) {
            if ($this->hasArticleTheme($articletheme->id)) {
                $temparttheme ['rows'] [] = array('id' => $articletheme->id, 'title' => $articletheme->title . ' >');
            } else {
                $temparttheme ['rows'] [] = array('id' => $articletheme->id, 'title' => $articletheme->title);
            }
        }

        $this->_helper->json->sendJson($temparttheme);
    }

    public function indexAction() {

        $this->view->layouter = "";
        $this->view->uuid = "";

        if ($this->_getParam('uuid', false)) {
            $basket = new TP_ResaleWizard ();
            $basket->setArticleId($this->_getParam('uuid'));
            $this->view->uuid = $this->_getParam('uuid');
        }
        if ($this->_getParam('layouterid', false)) {
            $basket = new TP_ResaleWizard ();
            $basket->setLayouterId($this->_getParam('layouterid'));
            $session = new TP_Layoutersession();
            $session = $session->getLayouterArticle($this->_getParam('layouterid'));
            $basket->setArticleStep1($session->getTitle(), "", "", "");

            $this->view->layouter = $this->_getParam('layouterid');
        } else {
            $basket = new TP_ResaleWizard ();
            $basket->setLayouterId("");
        }
        $this->view->hasShop = false;
        if (Zend_Auth::getInstance()->hasIdentity() && $this->highRole->level >= 30) {
            $this->view->hasShop = true;
        } elseif (!Zend_Auth::getInstance()->hasIdentity()) {
            $this->view->hasShop = true;
        }
    }

    public function finishAction() {

        if (!Zend_Auth::getInstance()->hasIdentity()) {
            $this->_forward('login', 'user');
        }

        if ($this->_getParam('mode', false)) {
            $this->view->mode = $this->_getParam('mode');
        }

        $wizard = new TP_ResaleWizard ();

        $article = Doctrine_Query::create()->from('Article c')->where('c.uuid = ?', array($wizard->getArticleId()))->fetchOne();

        $this->view->article = $article;

        $this->view->shopdetail = Doctrine_Query::create()->from('Shop c')->where('c.id = ?', array($wizard->getArticleShop()))->fetchOne();
    }

    public function layouterindexAction() {

        $this->view->layouter = "";
        $this->view->uuid = "";

        if ($this->_getParam('uuid', false)) {
            $basket = new TP_ResaleWizard ();
            $basket->setArticleId($this->_getParam('uuid'));
            $this->view->uuid = $this->_getParam('uuid');
        }
        if ($this->_getParam('layouterid', false)) {
            $basket = new TP_ResaleWizard ();
            $basket->setLayouterId($this->_getParam('layouterid'));
            $this->view->layouter = $this->_getParam('layouterid');
        } else {
            $basket = new TP_ResaleWizard ();
            $basket->setLayouterId("");
        }
        $this->view->hasShop = false;
        if (Zend_Auth::getInstance()->hasIdentity() && $this->highRole->level >= 30) {
            $this->view->hasShop = true;
        } elseif (!Zend_Auth::getInstance()->hasIdentity()) {
            $this->view->hasShop = true;
        }
    }

    public function motivstep1Action() {

        if (!Zend_Auth::getInstance()->hasIdentity()) {
            $this->_redirect('/user/login?mode=motiv_upload');
        }

        if ($this->_getParam('uuid', false)) {
            $basket = new TP_ResaleWizard ();
            $basket->setArticleId($this->_getParam('uuid'));
            $this->view->uuid = $this->_getParam('uuid');
        }

        if ($this->getRequest()->isPost() && $this->_getParam('finish')) {

            $this->view->form = new TP_Forms_Resale_MotivStep2 ();
            $this->view->form->finishUpload($this->_getAllParams());

            $this->_redirect('/motiv/mymotiv');
        }

        if ($this->getRequest()->isPost() && $this->_getParam('next')) {
            $wizard = new TP_ResaleWizard();

            $motiveNames = array();

            $images = new Zend_Session_Namespace('buymotive');

            $images->unlock();

            if (!$images->product_motive) {
                $images->product_motive = array();
            }

            if (!$images->motive) {
                $images->motive = array();
            }

            foreach ($images->product_motive as $key => $row) {
                $motiveNames[$key] = $this->_getParam('title_' . $key);
            }

            foreach ($images->motive as $key => $row) {
                $motiveNames[$key] = $this->_getParam('title_' . $key);
            }

            $wizard->setResaleMotive($motiveNames);
            $this->_redirect('/resale/motivstep2');
        }
    }

    public function motivstep2Action() {
        if (!Zend_Auth::getInstance()->hasIdentity()) {
            $this->_redirect('/user/login?mode=motiv_upload');
        }

        if ($this->_getParam('uuid', false)) {
            $basket = new TP_ResaleWizard ();
            $basket->setArticleId($this->_getParam('uuid'));
            $this->view->uuid = $this->_getParam('uuid');
        }

        $images = new Zend_Session_Namespace('buymotive');

        $images->unlock();

        if (!$images->product_motive) {
            $images->product_motive = array();
        }

        if (!$images->motive) {
            $images->motive = array();
        }

        if (count($images->motive) == 0 && count($images->product_motive) == 0) {
            $this->view->priorityMessenger('Bitte ein Motiv hochladen', 'error');
            $this->_redirect('/resale/motivstep1');
        }

        $this->view->motive = $images->motive;
        $this->view->product_motive = array();
        foreach ($images->product_motive as $key => $row) {
            $motiv = Doctrine_Query::create()->from('Motiv c')->where('c.uuid = ?', array($key))->fetchOne();
            $this->view->product_motive[$key] = $motiv;
        }

        $basket = new TP_ResaleWizard ();
        $motivebasket = $basket->getResaleMotive();

        if ($motivebasket['motivtheme1']) {
            $articletheme = Doctrine_Query::create()->select('a.title as title, a.id as id')->from('Motivtheme as a')->where('a.id = ?', array($motivebasket['motivtheme1']))->fetchOne();
            $text = ' ';
            if ($articletheme->parent_id != 0) {
                $text .= $this->getMotivThemeText($articletheme->parent_id, $text);
            }
            $text .= $articletheme->title;
            $this->view->motivtheme1name = $text;
        }

        if ($motivebasket['motivtheme2']) {
            $articletheme = Doctrine_Query::create()->select('a.title as title, a.id as id')->from('Motivtheme as a')->where('a.id = ?', array($motivebasket['motivtheme2']))->fetchOne();
            $text = ' ';
            if ($articletheme->parent_id != 0) {
                $text .= $this->getMotivThemeText($articletheme->parent_id, $text);
            }
            $text .= $articletheme->title;
            $this->view->motivtheme2name = $text;
        }

        if ($motivebasket['motivtheme3']) {
            $articletheme = Doctrine_Query::create()->select('a.title as title, a.id as id')->from('Motivtheme as a')->where('a.id = ?', array($motivebasket['motivtheme3']))->fetchOne();
            $text = ' ';
            if ($articletheme->parent_id != 0) {
                $text .= $this->getMotivThemeText($articletheme->parent_id, $text);
            }
            $text .= $articletheme->title;
            $this->view->motivtheme3name = $text;
        }

        if ($motivebasket['markettheme1']) {
            $articletheme = Doctrine_Query::create()->select('a.title as title, a.id as id')->from('Motivtheme as a')->where('a.id = ?', array($motivebasket['markettheme1']))->fetchOne();
            $text = ' ';
            if ($articletheme->parent_id != 0) {
                $text .= $this->getMotivThemeText($articletheme->parent_id, $text);
            }
            $text .= $articletheme->title;
            $this->view->markettheme1name = $text;
        }

        if ($motivebasket['markettheme2']) {
            $articletheme = Doctrine_Query::create()->select('a.title as title, a.id as id')->from('Motivtheme as a')->where('a.id = ?', array($motivebasket['markettheme2']))->fetchOne();
            $text = ' ';
            if ($articletheme->parent_id != 0) {
                $text .= $this->getMotivThemeText($articletheme->parent_id, $text);
            }
            $text .= $articletheme->title;
            $this->view->markettheme2name = $text;
        }

        if ($motivebasket['markettheme3']) {
            $articletheme = Doctrine_Query::create()->select('a.title as title, a.id as id')->from('Motivtheme as a')->where('a.id = ?', array($motivebasket['markettheme3']))->fetchOne();
            $text = ' ';
            if ($articletheme->parent_id != 0) {
                $text .= $this->getMotivThemeText($articletheme->parent_id, $text);
            }
            $text .= $articletheme->title;
            $this->view->markettheme3name = $text;
        }

        $this->view->form = new TP_Forms_Resale_MotivStep1 ( $this->highRole);

        if ($this->getRequest()->isPost()) {
            if ($this->view->form->isValid($this->getRequest()->getParams())) {
                if (file_exists($this->_templatePath . '/resale/motivstep21.phtml')) {
                    $this->view->form->storeInSession(false);
                    $this->_redirect('/resale/motivstep21');
                } else {
                    $this->view->form->storeInSession();
                    $this->_redirect('/resale/motivstep3');
                }
            }
        }

    }

    public function motivstep21Action() {
        if (!Zend_Auth::getInstance()->hasIdentity()) {
            $this->_redirect('/user/login?mode=motiv_upload');
        }

        if ($this->_getParam('uuid', false)) {
            $basket = new TP_ResaleWizard ();
            $basket->setArticleId($this->_getParam('uuid'));
            $this->view->uuid = $this->_getParam('uuid');
        }

        $images = new Zend_Session_Namespace('buymotive');

        $images->unlock();

        if (!$images->product_motive) {
            $images->product_motive = array();
        }

        if (!$images->motive) {
            $images->motive = array();
        }

        if (count($images->motive) == 0 && count($images->product_motive) == 0) {
            $this->view->priorityMessenger('Bitte ein Motiv hochladen', 'error');
            $this->_redirect('/resale/motivstep1');
        }

        $basket = new TP_ResaleWizard ();
        $motivebasket = $basket->getResaleMotive();
        if ($motivebasket['motivtheme1']) {
            $articletheme = Doctrine_Query::create()->select('a.title as title, a.id as id')->from('Motivtheme as a')->where('a.id = ?', array($motivebasket['motivtheme1']))->fetchOne();
            $text = ' ';
            if ($articletheme->parent_id != 0) {
                $text .= $this->getMotivThemeText($articletheme->parent_id, $text);
            }
            $text .= $articletheme->title;
            $this->view->motivtheme1name = $text;
        }

        if ($motivebasket['motivtheme2']) {
            $articletheme = Doctrine_Query::create()->select('a.title as title, a.id as id')->from('Motivtheme as a')->where('a.id = ?', array($motivebasket['motivtheme2']))->fetchOne();
            $text = ' ';
            if ($articletheme->parent_id != 0) {
                $text .= $this->getMotivThemeText($articletheme->parent_id, $text);
            }
            $text .= $articletheme->title;
            $this->view->motivtheme2name = $text;
        }

        if ($motivebasket['motivtheme3']) {
            $articletheme = Doctrine_Query::create()->select('a.title as title, a.id as id')->from('Motivtheme as a')->where('a.id = ?', array($motivebasket['motivtheme3']))->fetchOne();
            $text = ' ';
            if ($articletheme->parent_id != 0) {
                $text .= $this->getMotivThemeText($articletheme->parent_id, $text);
            }
            $text .= $articletheme->title;
            $this->view->motivtheme3name = $text;
        }

        if ($motivebasket['markettheme1']) {
            $articletheme = Doctrine_Query::create()->select('a.title as title, a.id as id')->from('Motivtheme as a')->where('a.id = ?', array($motivebasket['markettheme1']))->fetchOne();
            $text = ' ';
            if ($articletheme->parent_id != 0) {
                $text .= $this->getMotivThemeText($articletheme->parent_id, $text);
            }
            $text .= $articletheme->title;
            $this->view->markettheme1name = $text;
        }

        if ($motivebasket['markettheme2']) {
            $articletheme = Doctrine_Query::create()->select('a.title as title, a.id as id')->from('Motivtheme as a')->where('a.id = ?', array($motivebasket['markettheme2']))->fetchOne();
            $text = ' ';
            if ($articletheme->parent_id != 0) {
                $text .= $this->getMotivThemeText($articletheme->parent_id, $text);
            }
            $text .= $articletheme->title;
            $this->view->markettheme2name = $text;
        }

        if ($motivebasket['markettheme3']) {
            $articletheme = Doctrine_Query::create()->select('a.title as title, a.id as id')->from('Motivtheme as a')->where('a.id = ?', array($motivebasket['markettheme3']))->fetchOne();
            $text = ' ';
            if ($articletheme->parent_id != 0) {
                $text .= $this->getMotivThemeText($articletheme->parent_id, $text);
            }
            $text .= $articletheme->title;
            $this->view->markettheme3name = $text;
        }

        $this->view->form = new TP_Forms_Resale_MotivStep21 ();

        if ($this->getRequest()->isPost()) {
            if ($this->view->form->isValid($this->getRequest()->getParams())) {
                $this->view->form->storeInSession();
                $this->_redirect('/resale/motivstep3');
            }
        }

    }

    public function motivstep3Action() {

        if (!Zend_Auth::getInstance()->hasIdentity()) {
            $this->_forward('login', 'user');
        }

        if ($this->_getParam('uuid', false)) {
            $basket = new TP_ResaleWizard ();
            $basket->setArticleId($this->_getParam('uuid'));
            $this->view->uuid = $this->_getParam('uuid');
        }

        $this->view->form = new TP_Forms_Resale_MotivStep2 ();
        $this->view->error = false;
        if ($this->getRequest()->isPost()) {
            if ($this->view->form->isValid($this->getRequest()->getParams())) {
                $this->view->form->storeInSession();
                $this->view->priorityMessenger('Motive erfolgreich geändert', 'success');
                $this->_redirect('/resale/finish?mode=motive');
            } else {
                $this->view->error = true;
            }
        }

    }

    public function shopstep0Action() {

        if (!Zend_Auth::getInstance()->hasIdentity()) {
            $this->_redirect('/user/login?mode=resaleshop');
        }

        if ($this->_getParam('uuid', false)) {
            $basket = new TP_ResaleWizard ();
            $basket->setArticleId($this->_getParam('uuid'));
        }

        $this->view->form = new TP_Forms_Resale_ShopStep0($this->highRole);

        if ($this->getRequest()->isPost()) {
            if ($this->view->form->isValid($this->getRequest()->getParams())) {
                $this->view->form->storeInSession();
                $this->_redirect('/resale/shopstep1');
            }
        }
    }

    public function shopstep1Action() {

        if (!Zend_Auth::getInstance()->hasIdentity()) {
            $this->_forward('login', 'user');
        }

        if ($this->_getParam('uuid', false)) {
            $basket = new TP_ResaleWizard ();
            $basket->setArticleId($this->_getParam('uuid'));
        }

        $this->view->form = new TP_Forms_Resale_ShopStep1 ();
        $wizard = new TP_ResaleWizard ();
        $this->view->shoptype = $wizard->getType();

        if ($wizard->getShopTheme1()) {
            $articletheme = Doctrine_Query::create()->select('a.title as title, a.id as id')->from('ShopTheme as a')->where('a.id = ?', array($wizard->getShopTheme1()))->fetchOne();
            $text = ' ';
            if ($articletheme->parent_id != 0) {
                $text .= $this->getShopThemeText($articletheme->parent_id, $text);
            }
            $text .= $articletheme->title;
            $this->view->shoptheme1name = $text;
        }

        if ($wizard->getShopTheme2()) {
            $articletheme = Doctrine_Query::create()->select('a.title as title, a.id as id')->from('ShopTheme as a')->where('a.id = ?', array($wizard->getShopTheme2()))->fetchOne();
            $text = ' ';
            if ($articletheme->parent_id != 0) {
                $text .= $this->getShopThemeText($articletheme->parent_id, $text);
            }
            $text .= $articletheme->title;
            $this->view->shoptheme2name = $text;
        } else {
            $this->view->shoptheme2name = '';
        }

        if ($wizard->getShopTheme3()) {
            $articletheme = Doctrine_Query::create()->select('a.title as title, a.id as id')->from('ShopTheme as a')->where('a.id = ?', array($wizard->getShopTheme3()))->fetchOne();
            $text = ' ';
            if ($articletheme->parent_id != 0) {
                $text .= $this->getShopThemeText($articletheme->parent_id, $text);
            }
            $text .= $articletheme->title;
            $this->view->shoptheme3name = $text;
        } else {
            $this->view->shoptheme3name = '';
        }

        if ($this->getRequest()->isPost()) {
            if ($this->view->form->isValid($this->getRequest()->getParams())) {

                if ($wizard->getType() != 2 && $wizard->getType() != 3 && $wizard->getType() != 4 && $this->view->form->logo->getValue()) {
                    $file = new Image ();
                    $file->id = TP_Util::uuid();
                    $file->path = $this->view->form->logo->getFileName(null, true);
                    $file->name = $this->view->form->logo->getValue();

                    $finfo = new finfo(FILEINFO_MIME_TYPE);
                    $finfo = $finfo->file($file->path);
                    $file->imagetype = $finfo;
                    $file->save();
                }

                $this->view->form->storeInSession($file);
                $this->_redirect('/resale/shopstep3');
            }
        }
    }

    public function shopstep2Action() {

        if (!Zend_Auth::getInstance()->hasIdentity()) {
            $this->_forward('login', 'user');
        }

        $temp = array();
        $options = array();
        /*
          foreach (new DirectoryIterator(APPLICATION_PATH . '/design/vorlagen') as $file) {
          if($file == '.' || $file == '..' || $file == 'config' || $file == '.svn' || $file == 'datapacks' || $file == '.DS_Store') continue;
          array_push($temp, array('name' => $file->getFileName(), 'label' => $file->getFileName()));
          $options[$file->getFileName()] = $file->getFileName();
          } */
        $temp [] = array('label' => 'Standard', 'name' => 'design2');
        $options ['design2'] = 'design2';

        $this->view->form = new TP_Forms_Resale_ShopStep2($options);

        $this->view->designs = $temp;

        if ($this->getRequest()->isPost()) {
            if ($this->view->form->isValid($this->getRequest()->getParams())) {
                $this->view->form->storeInSession();
                $this->_redirect('/resale/shopstep3');
            }
        }
    }

    public function shopstep3Action() {

        if (!Zend_Auth::getInstance()->hasIdentity()) {
            $this->_forward('login', 'user');
        }

        $this->view->form = new TP_Forms_Resale_ShopStep3 ();

        $wizard = new TP_ResaleWizard ();

        $temp = array();

        $this->view->designs = $temp;

        if ($this->getRequest()->isPost()) {
            if ($this->view->form->isValid($this->getRequest()->getParams())) {
                $this->view->form->storeInSession();
                $wizard = new TP_ResaleWizard ();
                $value = $wizard->getShopName();
                $install = Zend_Registry::get('install');
                $filter = new Zend_Filter_Alnum ();
                $value = $filter->filter($value) . $install ['marketdomain'];

                $count = Doctrine_Query::create()->from('Domain m')->where('m.name = ?', array($value))->Count();
                if ($count) {
                    $this->view->priorityMessenger('Shopname nicht mehr möglich, da Shop mit diesem Namen bereits existiert', 'error');
                    $this->_redirect('/resale/shopstep1');
                }

                $wizard->createShop();
                $this->view->priorityMessenger('Themenshop erfolgreich eingerichtet', 'success');
                if ($wizard->hasArticleId()) {
                    $this->_redirect('/resale/articlestep1');
                } else {
                    $wizard->clearWizard();
                    $this->_redirect('/');
                }
            } else {
                $this->view->error = true;
                var_dump($this->view->form->getErrors());
            }
        }
    }

    public function articlestep1Action() {
        if($this->install->id == 7) $this->_redirect('/');


        if (!Zend_Auth::getInstance()->hasIdentity()) {
            $this->_redirect('/user/login?mode=resalearticle');
        }

        $user = Zend_Auth::getInstance()->getIdentity();
        $user = Doctrine_Query::create()
            ->from('Contact m')
            ->useQueryCache(true)
            ->where('id = ?')->fetchOne(array($user['id']));

        $install = Zend_Registry::get('install');
        $hasShop = false;
        foreach ($user->Shops as $shop) {
            if ($shop->id != $install['defaultmarket'] || $this->highRole->level >= 40) {
                $hasShop = true;
            }
        }

        if ($this->_getParam('uuid', false)) {
            $basket = new TP_ResaleWizard ();
            $basket->setArticleId($this->_getParam('uuid'));
        }

        $wizard = new TP_ResaleWizard ();

        if ($this->_getParam('uuid') != "") {
            $wizard->setArticleId($this->_getParam('uuid'));
        }

        if ($hasShop == false) {
            $this->view->priorityMessenger('Sie sind noch kein Themenshopbetreiber. Bitte erstellen Sie erst ihren eigenen Shop.', 'error');
            $this->_redirect('/resale/shopstep1');
        }

        $article = Doctrine_Query::create()->from('Article c')->where('c.uuid = ?', array($wizard->getArticleId()))->fetchOne();

        $this->view->article = $article;
        $this->view->layouterPreviewId = $wizard->getLayouterId();

        $articlesObjs = Zend_Registry::get('articles');

        $articleObj = new $articlesObjs [$article->typ] ();
        unset($articlesObjs);

        $this->view->articleObj = $articleObj;

        if ($article == false || $article->resale == false) {
            $this->view->priorityMessenger('Article not for resale', 'error');
            $this->_redirect('/');
        }

        if ($this->getRequest()->isPost()) {
            if ($this->_getParam('shop', false)) {
                $wizard->setArticleShop($this->_getParam('shop'));
            }
            $this->view->form = new TP_Forms_Resale_ArticleStep1(null, $this->highRole);
            if ($this->view->form->isValid($this->getRequest()->getParams())) {
                $this->view->form->storeInSession();
                $this->_redirect('/resale/articlestep2');
            } else {
                $this->view->form->storeInSession();
            }
        } else {

            $this->view->form = new TP_Forms_Resale_ArticleStep1($article, $this->highRole);
        }
    }

    public function articlestep2Action() {

        if (!Zend_Auth::getInstance()->hasIdentity()) {
            $this->_forward('login', 'user');
        }

        $wizard = new TP_ResaleWizard ();

        $baseShop = Zend_Registry::get('shop');
        $baseInstall = Zend_Registry::get('install');

        $tempartgroup = array();
        $articlegroups = Doctrine_Query::create()->select('a.title as title, a.id as id')->from('ArticleGroup as a')->where('a.shop_id = ? AND a.parent = 0 AND a.install_id = ?', array($wizard->getArticleShop(), $baseShop ['install_id']))->execute();
        $i = 0;
        foreach ($articlegroups as $articlegroup) {
            if ($this->hasArticleGroup($articlegroup->id)) {
                $tempartgroup [$articlegroup->id] = $articlegroup->title . ' >';
            } else {
                $tempartgroup [$articlegroup->id] = $articlegroup->title;
            }
        }

        $this->view->articlegroup = $tempartgroup;

        $temparttheme = array();
        $articlethemes = Doctrine_Query::create()->select('a.title as title, a.id as id')->from('ArticleTheme as a')->where('a.shop_id = ? AND a.parent_id=0 AND a.install_id = ?', array($wizard->getArticleShop(), $baseShop ['install_id']))->execute();
        $i = 0;
        foreach ($articlethemes as $articletheme) {
            if ($this->hasArticleTheme($articletheme->id)) {
                $temparttheme [$articletheme->id] = $articletheme->title . ' >';
            } else {
                $temparttheme [$articletheme->id] = $articletheme->title;
            }
        }

        $this->view->theme = $temparttheme;

        if ($wizard->getArticleTheme1()) {
            $articletheme = Doctrine_Query::create()->select('a.title as title, a.id as id')->from('ArticleTheme as a')->where('a.id = ?', array($wizard->getArticleTheme1()))->fetchOne();
            $text = ' ';
            if ($articletheme->parent_id != 0) {
                $text .= $this->getArticleThemeText($articletheme->parent_id, $text);
            }
            $text .= $articletheme->title;
            $this->view->articletheme1name = $text;
        }

        if ($wizard->getArticleTheme2()) {
            $articletheme = Doctrine_Query::create()->select('a.title as title, a.id as id')->from('ArticleTheme as a')->where('a.id = ?', array($wizard->getArticleTheme2()))->fetchOne();
            $text = ' ';
            if ($articletheme->parent_id != 0) {
                $text .= $this->getArticleThemeText($articletheme->parent_id, $text);
            }
            $text .= $articletheme->title;
            $this->view->articletheme2name = $text;
        } else {
            $this->view->articletheme2name = '';
        }

        if ($wizard->getArticleTheme3()) {
            $articletheme = Doctrine_Query::create()->select('a.title as title, a.id as id')->from('ArticleTheme as a')->where('a.id = ?', array($wizard->getArticleTheme3()))->fetchOne();
            $text = ' ';
            if ($articletheme->parent_id != 0) {
                $text .= $this->getArticleThemeText($articletheme->parent_id, $text);
            }
            $text .= $articletheme->title;
            $this->view->articletheme3name = $text;
        } else {
            $this->view->articletheme3name = '';
        }

        if ($wizard->getArticleGroup1()) {
            $articletheme = Doctrine_Query::create()->select('a.title as title, a.id as id')->from('ArticleGroup as a')->where('a.id = ?', array($wizard->getArticleGroup1()))->fetchOne();
            $text = ' ';
            if ($articletheme->parent != 0) {
                $text .= $this->getArticleGroupText($articletheme->parent, $text);
            }
            $text .= $articletheme->title;
            $this->view->articlegroup1name = $text;
        }

        if ($wizard->getArticleGroup2()) {
            $articletheme = Doctrine_Query::create()->select('a.title as title, a.id as id')->from('ArticleGroup as a')->where('a.id = ?', array($wizard->getArticleGroup2()))->fetchOne();
            $text = ' ';
            if ($articletheme->parent != 0) {
                $text .= $this->getArticleGroupText($articletheme->parent, $text);
            }
            $text .= $articletheme->title;
            $this->view->articlegroup2name = $text;
        } else {
            $this->view->articlegroup2name = '';
        }

        if ($wizard->getArticleGroup3()) {
            $articletheme = Doctrine_Query::create()->select('a.title as title, a.id as id')->from('ArticleGroup as a')->where('a.id = ?', array($wizard->getArticleGroup3()))->fetchOne();
            $text = ' ';
            if ($articletheme->parent != 0) {
                $text .= $this->getArticleGroupText($articletheme->parent, $text);
            }
            $text .= $articletheme->title;
            $this->view->articlegroup3name = $text;
        } else {
            $this->view->articlegroup3name = '';
        }

        if ($this->getRequest()->isPost()) {
            $this->view->form = new TP_Forms_Resale_ArticleStep2 ();
            if ($this->view->form->isValid($this->getRequest()->getParams())) {
                $this->view->form->storeInSession();
                $this->_redirect('/resale/articlestep3');
            } else {
                $this->view->form->storeInSession();
            }
        } else {

            $this->view->form = new TP_Forms_Resale_ArticleStep2 ();
        }
    }

    protected function getArticleThemeText($id, $text) {
        $articlethemes = Doctrine_Query::create()->select('a.title as title, a.id as id')->from('ArticleTheme as a')->where('a.id=?', array($id))->fetchOne();
        if ($articlethemes->parent_id != 0) {
            $text .= $this->getArticleThemeText($articlethemes->parent_id, $text);
        }
        $text .= ' ' . $articlethemes->title . ' >';
        return $text;
    }

    protected function getMotivThemeText($id, $text) {
        $articlethemes = Doctrine_Query::create()->select('a.title as title, a.id as id')->from('MotivTheme as a')->where('a.id=?', array($id))->fetchOne();
        if ($articlethemes->parent_id != 0) {
            $text .= $this->getMotivThemeText($articlethemes->parent_id, $text);
        }
        $text .= ' ' . $articlethemes->title . ' >';
        return $text;
    }

    protected function getShopThemeText($id, $text) {
        $articlethemes = Doctrine_Query::create()->select('a.title as title, a.id as id')->from('ShopTheme as a')->where('a.id=?', array($id))->fetchOne();
        if ($articlethemes->parent_id != 0) {
            $text .= $this->getShopThemeText($articlethemes->parent_id, $text);
        }
        $text .= ' ' . $articlethemes->title . ' >';
        return $text;
    }

    protected function getArticleGroupText($id, $text) {
        $articlethemes = Doctrine_Query::create()->select('a.title as title, a.id as id')->from('ArticleGroup as a')->where('a.id=?', array($id))->fetchOne();
        if ($articlethemes->parent != 0) {
            $text .= $this->getArticleGroupText($articlethemes->parent, $text);
        }
        $text .= ' ' . $articlethemes->title . ' >';
        return $text;
    }

    protected function hasArticleGroup($id) {
        $articlegroups = Doctrine_Query::create()->select('a.title as title, a.id as id')->from('ArticleGroup as a')->where('a.parent = ?', array($id))->execute();
        if (count($articlegroups) > 0) {
            return true;
        }
        return false;
    }

    protected function hasArticleTheme($id) {
        $articlethemes = Doctrine_Query::create()->select('a.title as title, a.id as id')->from('ArticleTheme as a')->where('a.parent_id=?', array($id))->execute();
        if (count($articlethemes) > 0) {
            return true;
        }
        return false;
    }

    protected function hasShopTheme($id) {
        $articlethemes = Doctrine_Query::create()->select('a.title as title, a.id as id')->from('ShopTheme as a')->where('a.parent_id=?', array($id))->execute();
        if (count($articlethemes) > 0) {
            return true;
        }
        return false;
    }

    protected function hasMotivTheme($id) {
        $articlethemes = Doctrine_Query::create()->select('a.title as title, a.id as id')->from('MotivTheme as a')->where('a.parent_id=?', array($id))->execute();
        if (count($articlethemes) > 0) {
            return true;
        }
        return false;
    }

    public function articlestep3Action() {

        if (!Zend_Auth::getInstance()->hasIdentity()) {
            $this->_forward('login', 'user');
        }

        $wizard = new TP_ResaleWizard ();

        if ($wizard->getArticleThemeMarket1()) {
            $articletheme = Doctrine_Query::create()->select('a.title as title, a.id as id')->from('ArticleTheme as a')->where('a.id = ?', array($wizard->getArticleThemeMarket1()))->fetchOne();
            $text = ' ';
            if ($articletheme->parent_id != 0) {
                $text .= $this->getArticleThemeText($articletheme->parent_id, $text);
            }
            $text .= $articletheme->title;
            $this->view->articlemarkettheme1name = $text;
        }

        if ($wizard->getArticleThemeMarket2()) {
            $articletheme = Doctrine_Query::create()->select('a.title as title, a.id as id')->from('ArticleTheme as a')->where('a.id = ?', array($wizard->getArticleThemeMarket2()))->fetchOne();
            $text = ' ';
            if ($articletheme->parent_id != 0) {
                $text .= $this->getArticleThemeText($articletheme->parent_id, $text);
            }
            $text .= $articletheme->title;
            $this->view->articlemarkettheme2name = $text;
        } else {
            $this->view->articlemarkettheme2name = '';
        }

        if ($wizard->getArticleThemeMarket3()) {
            $articletheme = Doctrine_Query::create()->select('a.title as title, a.id as id')->from('ArticleTheme as a')->where('a.id = ?', array($wizard->getArticleThemeMarket3()))->fetchOne();
            $text = ' ';
            if ($articletheme->parent_id != 0) {
                $text .= $this->getArticleThemeText($articletheme->parent_id, $text);
            }
            $text .= $articletheme->title;
            $this->view->articlemarkettheme3name = $text;
        } else {
            $this->view->articlemarkettheme3name = '';
        }

        if ($this->getRequest()->isPost()) {
            $this->view->form = new TP_Forms_Resale_ArticleStep3 ();
            if ($this->view->form->isValid($this->getRequest()->getParams())) {
                $this->view->form->storeInSession();
                if ($this->_getParam('nextstep', false) == 5) {
                    $this->_redirect('/resale/articlestep5');
                    return;
                }
                $this->_redirect('/resale/articlestep4');
            } else {
                $this->view->form->storeInSession();
            }
        } else {

            $this->view->form = new TP_Forms_Resale_ArticleStep3 ();
        }
    }

    protected function getArticleTheme($id) {
        $temparttheme = array();
        $articlethemes = Doctrine_Query::create()->select('a.title as title, a.id as id')->from('ArticleTheme as a')->where('a.parent_id=?', array($id))->execute();
        foreach ($articlethemes as $articletheme) {
            $temparttheme [$articletheme->title] = $articletheme->id;

            $subitems = $this->getArticleTheme($articletheme->id);
            if (count($subitems) > 0) {
                $temparttheme [$articletheme->title] = $subitems;
            }
        }
        return $temparttheme;
    }

    public function articlestep4Action() {

        if (!Zend_Auth::getInstance()->hasIdentity()) {
            $this->_forward('login', 'user');
        }

        $wizard = new TP_ResaleWizard ();
        $this->view->private = false;
        $this->view->private = $wizard->getArticlePrivate();

        if ($this->getRequest()->isPost()) {
            $this->view->form = new TP_Forms_Resale_ArticleStep4 ();
            if ($this->view->form->isValid($this->getRequest()->getParams())) {
                $this->view->form->storeInSession();
                $this->_redirect('/resale/articlestep5');
            } else {
                $this->view->form->storeInSession();
            }
        } else {

            $this->view->form = new TP_Forms_Resale_ArticleStep4 ();
        }
    }

    public function articlestep5Action() {

        if (!Zend_Auth::getInstance()->hasIdentity()) {
            $this->_forward('login', 'user');
        }

        $this->view->form = new TP_Forms_Resale_ArticleStep5 ();

        $temp = array();
        $wizard = new TP_ResaleWizard ();
        $this->view->designs = $temp;

        if ($this->getRequest()->isPost()) {
            if ($this->view->form->isValid($this->getRequest()->getParams())) {
                $this->view->form->storeInSession();
                $wizard->createArticle();
                $uri = $wizard->getArticleShop();
                $uuid = $wizard->getArticleId();
                $shopid = $wizard->getArticleShop();
                $wizard->clearWizard();
                $wizard->setArticleId($uuid);
                $wizard->setShopId($shopid);
                $wizard->setArticleShop($uri);
                $this->view->priorityMessenger('Artikel erfolgreich eingestellt', 'success');

                $article = Doctrine_Query::create()->from('Article c')->where('c.uuid = ?', array($wizard->getArticleId()))->fetchOne();

                $dom = new DOMDocument ();
                $dom->load(APPLICATION_PATH . '/../market/products/' . $article->a6_directory . '/pageobjects.xml');

                $xpath = new DOMXPath($dom);
                $motiveCalc = $xpath->query('//root/item[contains(objType,"Image")]/image_uuid');

                $user = false;
                if (Zend_Auth::getInstance()->hasIdentity()) {
                    $user = Zend_Auth::getInstance()->getIdentity();
                }

                $images = new Zend_Session_Namespace('buymotive');
                $images->unlock();

                if (!$images->product_motive) {
                    $images->product_motive = array();
                }

                foreach ($motiveCalc as $motivCalc) {

                    $row = Doctrine_Query::create()->from('Motiv c')->where('c.uuid = ?')->fetchOne(array($motiveCalc->item($i)->nodeValue));

                    if ($row->contact_id == $user ['id'] && $row->resale_shop != true) {
                        $images->product_motive[$row->uuid] = $row->uuid;
                    }
                }

                $this->_redirect('/resale/finish?mode=product');
            } else {
                $this->view->error = true;
            }
        }
    }

}