<?php

class SearchController extends TP_Controller_Action
{

    /**
     * SearchAction
     *
     * @todo Dokumentation der IndexAction
     *
     * @return void
     */
    public function indexAction() {

        $this->view->items = array();

        $filters = array(
            'q' => 'Alnum'
        );

        $validators = array(
            'q' => 'Alnum'
        );

        $input = new Zend_Filter_Input($filters, $validators, $this->_getAllParams());

        if ($input->isValid('q') &&
            $this->_getParam('q') != ""
        ) {

            $this->view->q = $this->_getParam('q');

            $query = explode(' ', $this->_getParam('q'));

            $searchQuery = $this->_getParam('q');

            $articles = Doctrine_Query::create()
                ->select()
                ->from('Article c')
                ->leftJoin('c.OrgArticle l')
                ->select('c.*, (c.rate/c.rate_count) as ratesum, l.*, (l.a4_abpreis+c.a6_resale_price) as sum')
                ->where("c.shop_id = ?
                    AND c.enable = 1 AND c.display_not_in_overview = 0
                    AND (c.meta_keywords LIKE '%" . $searchQuery . "%' OR c.title LIKE '%" . $searchQuery . "%' OR c.kostenstelle LIKE '%" . $searchQuery . "%' OR c.article_nr_intern LIKE '%" . $searchQuery . "%' OR c.info LIKE '%" . $searchQuery . "%' OR c.einleitung LIKE '%" . $searchQuery . "%')"
                    , array($this->shop->id))
                ->orderBy('c.pos ASC')
                ->execute();


            $temp = array();
            $temp[] = 0;

            if(Zend_Auth::getInstance()->hasIdentity()) {
                $identity = Zend_Auth::getInstance()->getIdentity();

                $articles_contact = Doctrine_Query::create()->from('ContactArticle c')->where('c.contact_id = ?', array($identity ['id']))->execute()->toArray();

                foreach ($articles_contact as $art) {
                    $temp[] = $art['article_id'];
                }

                $temp = $this->getAccountArticle($temp, $identity ['account_id']);
            }

            $article = array();

            foreach($articles as $art) {
                if(!$art->private || in_array($art->id, $temp)) {
                    $article[] = $art;
                }
            }
            $this->view->article = $article;


            /* CMS */


            $cms = Doctrine_Query::create()
                ->select()
                ->from('Cms c')
                ->where("c.shop_id = ?
                AND (c.title LIKE '%".$searchQuery."%' OR text1 LIKE '%".$searchQuery."%')"
                , array($this->shop->id))
                ->execute();


            $temp = array();
            $temp[] = 0;

            if(Zend_Auth::getInstance()->hasIdentity()) {
                $identity = Zend_Auth::getInstance()->getIdentity();

                $articles_contact = Doctrine_Query::create()->from('ContactCms c')->where('c.contact_id = ?', array($identity ['id']))->execute()->toArray();

                foreach ($articles_contact as $art) {
                    $temp[] = $art['cms_id'];
                }
            }

            $tempCms = array();

            foreach($cms as $art) {
                if(!$art->private || in_array($art->id, $temp)) {
                    $tempCms[] = $art;
                }
            }

            $this->view->cms = $tempCms;

        }
    }

    protected function getAccountArticle($articles, $account_id, $deep = 1) {
        if($deep == 4) return $articles;
        $account_article = Doctrine_Query::create()->from('AccountArticle c')->where('c.account_id = ?', array($account_id))->execute()->toArray();
        $account = Doctrine_Query::create()->from('Account a')->where('a.id = ?', array($account_id))->fetchOne();

        foreach ($account_article as $art) {
            if (!in_array($art['article_id'], $articles)) {
                $articles[] = $art['article_id'];
            }
        }

        if($account['filiale_id'] != "" && $account['filiale_id'] != 0) {
            $articles = $this->getAccountArticle($articles, $account['filiale_id'], $deep+1);
        }

        return $articles;
    }

}
