<?php

class ThemesController extends TP_Controller_Action
{

    public function indexAction() {

    }

    public function addfilterarticleAction() {

        $theme = $this->_getParam('theme');

        $theme = Doctrine_Query::create()
            ->select()
            ->from('ArticleTheme as a')
            ->where('a.url = ?', array($theme))
            ->fetchOne();

        $filter = new TP_Themesfilter();
        $filter->setArticleFilterId($theme->id, $theme->title);
        if (strpos('t' . $_SERVER['HTTP_REFERER'], 'article') == false && strpos('t' . $_SERVER['HTTP_REFERER'], 'overview') == false) {
            return $this->_redirect('/overview');
        }
        $this->_redirect($_SERVER['HTTP_REFERER']);
    }

    public function delfilterarticleAction() {

        $theme = $this->_getParam('theme');

        $filter = new TP_Themesfilter();
        if ($theme == 'all') {
            $filter->delAllArticleFilter();
        } else {
            $filter->delArticleFilterId($theme);
        }

        $this->_redirect($_SERVER['HTTP_REFERER']);
    }

    public function addfiltermotivAction() {

        $theme = $this->_getParam('theme');

        $theme = Doctrine_Query::create()
            ->select()
            ->from('MotivTheme as a')
            ->where('a.url = ?', array($theme))
            ->fetchOne();

        $filter = new TP_Themesfilter();
        $filter->setMotivFilterId($theme->id, $theme->title);

        $this->_redirect('/motiv');
    }

    public function delfiltermotivAction() {

        $theme = $this->_getParam('theme');

        $filter = new TP_Themesfilter();
        if ($theme == 'all') {
            $filter->delAllMotivFilter();
        } else {
            $filter->delMotivFilterId($theme);
        }

        $this->_redirect('/motiv');
    }

    public function addfiltershopAction() {

        $theme = $this->_getParam('theme');

        $theme = Doctrine_Query::create()
            ->select()
            ->from('ShopTheme as a')
            ->where('a.url = ?', array($theme))
            ->fetchOne();

        $filter = new TP_Themesfilter();
        $filter->setShopFilterId($theme->id, $theme->title);

        $this->_forward('index', 'market');
    }

    public function delfiltershopAction() {

        $theme = $this->_getParam('theme');

        $filter = new TP_Themesfilter();
        if ($theme == 'all') {
            $filter->delAllShopFilter();
        } else {
            $filter->delShopFilterId($theme);
        }

        $this->_redirect('/market');
    }
}
