<?php

/**
 * Uploadcenter Controller
 *
 * Stellt das Uploadcenter fürs Frontend da
 *
 * PHP versions 4 and 5
 *
 * @category   Modul
 * @package    Default
 * @subpackage Controller
 * @author     Thomas petersomn <tpeterson@printshopcreator.de>
 * @copyright  1997-2009 Thomas Peterson
 * @license    Not Public
 * @version    SVN: $Id: UploadcenterController.php 7070 2011-12-14 14:22:09Z boonkerz $
 * @link       http://www.printshopcreator.de
 */

/**
 * Uploadcenter Controller
 *
 * Stellt das Uploadcenter fürs Frontend da
 *
 * PHP versions 4 and 5
 *
 * @category   Modules
 * @package    Default
 * @subpackage Controller
 * @author     Thomas petersomn <tpeterson@printshopcreator.de>
 * @copyright  1997-2009 Thomas Peterson
 * @license    Not Public
 * @version    Release: $Id: UploadcenterController.php 7070 2011-12-14 14:22:09Z boonkerz $
 * @link       http://www.printshopcreator.de
 */

class UploadcenterController extends TP_Controller_Action
{

    /**
     * Zeigt den Auftrag an und ermöglicht das hochladen von Files
     *
     * @return void
     */
    public function indexAction() {
        $this->view->mode = 3;

        $mode = new Zend_Session_Namespace('adminmode');

        $row = Doctrine_Query::create()
            ->from('Orders m')
            ->where('id = ?', array(intval($this->_request->getParam('id'))))
            ->fetchOne();

        if($this->user->isShowOtherOrders()) {
            if (!$row ||
                (($row->contact_id != $this->user->id) && !$this->user->isShowOtherOrders()) ||
                ($this->user->isShowOtherOrders() && $this->user->getShowOtherOrdersAccountFilter() == 2 && $row->account_id != $this->user->account_id) &&
                ($this->user->isShowOtherOrders() && $this->user->getShowOtherOrdersAccountFilter() == 3 && $row->account_id != $this->user->getShowOtherOrdersAccount())) {
                $this->_redirect('/user/login?mode=orders');
            }
        }else {
            if (!$row ||
                ($row->contact_id != $this->user->id && ($mode->over_ride_contact == false || ($mode->over_ride_contact != false && $mode->over_ride_contact != 'new' && $row->contact_id != $mode->over_ride_contact)))
            ) {
                $this->_redirect('/user/login?mode=orders');
            }
        }
        $this->view->order = $row;
        $this->view->articles = array();

        foreach ($row->Orderspos as $row) {

            if($this->_getParam('pos', false)) {
                if($row->id != $this->_getParam('pos', false)) {
                    continue;
                }
            }

            $pro = unserialize($row->data);
            $proa = $pro->getArticle();

            $params = $pro->getOptions();
            $prof = Doctrine_Query::create()
                ->from('Upload m')
                ->where('orderpos_id = ?', array(intval($row->id)))->execute();

            $article = Doctrine_Query::create()->from('Article c')->where('c.id = ?', array(intval($proa['id'])))->fetchOne();

            if ($article['a1_xml'] != '') {
                if ($article['a6_org_article'] != '0' || $article['a6_org_article'] > 0) {
                    $org = Doctrine_Core::getTable('Article')->find($article['a6_org_article']);

                    $xml = str_replace('\\', '', $org->a1_xml);
                } else {

                    $xml = str_replace('\\', '', $article['a1_xml']);
                }
            } else {
                $articlesObjs = Zend_Registry::get('articles');

                $articleObj = new $articlesObjs [$article['typ']] ();
                unset($articlesObjs);
                $xml = file_get_contents(APPLICATION_PATH . '/articles/' . $articleObj->frontend, null);
            }

            try{
                $xml = new SimpleXMLElement($xml, null, false);
            }catch(Exception $e) {
                $xml = array();
            }
            $uploads = array();

            foreach ($xml as $article_xml) {

                $up = array();
                foreach ($article_xml->uploads->children() as $upload) {
                    if (isset($upload['select']) && $params[(string)$upload['select']] != (string)$upload['value']) {
                        continue;
                    }
                    $up[] = array(
                        'id' => (string)$upload['id'],
                        'name' => (string)$upload['name'],
                        'description' => (string)$upload['description']
                    );
                }

                $uploads[(string)$article_xml->name] = $up;

            }

            if (count($uploads) > 1 && isset($uploads[$params['kalk_artikel']])) {
                array_push($this->view->articles, array('article' => $proa,
                    'orderpos' => $row, 'files' => $prof, 'article_org' => $article, 'preflight_uploads' => $uploads[$params['kalk_artikel']]));
            } elseif (count($uploads) == 1) {
                array_push($this->view->articles, array('article' => $proa,
                    'orderpos' => $row, 'files' => $prof, 'article_org' => $article, 'preflight_uploads' => array_shift($uploads)));
            } else {
                array_push($this->view->articles, array('article' => $proa,
                    'orderpos' => $row, 'files' => $prof, 'article_org' => $article));
            }
        }

        $translateStatus = Zend_Registry::get('Zend_Translate');


        $this->view->stati = array();
        if ($this->install->production_status != "") {
            foreach (explode(",", $this->install->production_status) as $key) {
                $this->view->stati[$key] = $translateStatus->translate($key);
            }
        }

        $config = new Zend_Config_Ini(APPLICATION_PATH . '/configs/systemdefaults.ini', 'status');

        foreach ($config->toArray() as $key => $value) {
            $this->view->stati[$key] = $translateStatus->translate("status".$key);
        }

        if ($this->install['offline_custom2'] != "") {
            try {

                $maschienen = new SimpleXMLElement($this->install['offline_custom2'], null, false);

                foreach ($maschienen as $maschine) {
                    $this->view->stati[(int)$maschine['id']] = $translateStatus->translate((string)$maschine);
                }
            } catch (Exception $e) {

            }
        }

    }

    /**
     * Setzt den Upload auf fertig
     *
     * @return void
     */
    public function finishAction() {
        $this->view->mode = 3;

        if($this->_getParam("pos", false)) {
            $row = Doctrine_Query::create()
                ->from('Orders m')
                ->where('id = ?', array(intval($this->_request->getParam('id'))))
                ->limit(intval($this->_request->getParam('limit')))
                ->offset(intval($this->_request->getParam('start')))
                ->fetchOne();

            $row->loadMongo();
            // Redirect wenn der User nicht mit der Bestellung verknüpft ist
            if ($row->contact_id != $this->user->id) {
                $this->_forward('noaccess', 'error');
            }

            $pos = Doctrine_Query::create()
                ->from('Orderspos m')
                ->where('id = ?', array(intval($this->_request->getParam('pos'))))
                ->limit(intval($this->_request->getParam('limit')))
                ->offset(intval($this->_request->getParam('start')))
                ->fetchOne();

            if($this->_getParam('status', false)) {

				if($this->_getParam('status', false) == 520 && $this->_getParam('comment', false) == '') {
					$this->view->priorityMessenger('Bitte einen Kommentar abgeben.', 'error');
					$this->_redirect('/uploadcenter?id='.intval($this->_request->getParam('id')).'&pos='.intval($this->_request->getParam('pos')));
					return;					
				}
				
				$pos->status = $this->_getParam('status', false);
                $pos->save();

                $upload = Doctrine_Query::create()
                    ->from('Upload m')
                    ->where('m.orderpos_id = ?', array($pos->id))
                    ->orderBy('m.created DESC')
                    ->fetchOne();

				

                if($this->_getParam('comment', false)) {
                    $upload->preflight_errors = $this->_getParam('comment', false) . '                     ' . $upload->preflight_errors;
                    $upload->save();
                }

                if($pos->status == 530) {

                    $upload->export = true;
                    $upload->save();
                }
                $row->setPackageExported(false);
                $row->saveMongo();

                $dbMongo = TP_Mongo::getInstance();
                $dbMongo->Job->insertOne(array(
                    "shop" =>  $this->shop->id,
                    "event" => "position_status_change",
                    "data" => [ "position" => $pos->uuid, 'status' => $pos->status ],
                    "created" => new MongoDB\BSON\UTCDateTime(),
                    "updated" => new MongoDB\BSON\UTCDateTime()
                ));


                TP_Queue::process('orderposuploadcomplete', 'global', $pos);
                TP_Queue::process('orderposstatuschange', 'global', $pos);

            }else{
                if ($pos->status < 50) {
                    
                    if(count($pos->Upload) == 0) {
                        $this->view->priorityMessenger('Bitte eine Datei hochladen', 'error');
                        $this->_redirect('/uploadcenter?id='.intval($this->_request->getParam('id')).'&pos='.intval($this->_request->getParam('pos')));
                        return;
                    }

                    foreach ($row->Orderspos as $orderpos) {
                        $orderpos->uploadfinish = true;
                        $orderpos->save();
                    }

                    $pos->status = 50;
                    $pos->save();

                    $row->setPackageExported(false);
                    $row->saveMongo();

                    $dbMongo = TP_Mongo::getInstance();
                    $dbMongo->Job->insertOne(array(
                        "shop" =>  $this->shop->id,
                        "event" => "position_status_change",
                        "data" => [ "position" => $pos->uuid, 'status' => 50 ],
                        "created" => new MongoDB\BSON\UTCDateTime(),
                        "updated" => new MongoDB\BSON\UTCDateTime()
                    ));

                    TP_Queue::process('orderposuploadcomplete', 'global', $pos);
                    TP_Queue::process('orderposstatuschange', 'global', $pos);
                }
            }



            $this->_redirect('/user/myorders');
            return;
        }

        $row = Doctrine_Query::create()
            ->from('Orders m')
            ->where('id = ?', array(intval($this->_request->getParam('id'))))
            ->limit(intval($this->_request->getParam('limit')))
            ->offset(intval($this->_request->getParam('start')))
            ->fetchOne();

        // Redirect wenn der User nicht mit der Bestellung verknüpft ist
        if ($row->contact_id != $this->user->id) {
            $this->_forward('noaccess', 'error');
        }
        if ($row->status < 50) {
            $row->status = 50;
            $row->save();

            $row->setPackageExported(false);
            $row->saveMongo();

            foreach ($row->Orderspos as $orderpos) {
                $orderpos->uploadfinish = true;
                $orderpos->save();
            }

            $dbMongo = TP_Mongo::getInstance();
            $dbMongo->Job->insertOne(array(
                "shop" =>  $this->shop->id,
                "event" => "order_status_change",
                "data" => [ "order" => $row->uuid, 'status' => 50 ],
                "created" => new MongoDB\BSON\UTCDateTime(),
                "updated" => new MongoDB\BSON\UTCDateTime()
            ));

            TP_Queue::process('orderstatuschange', 'global', $row);
        }
        $this->_redirect('/user/myorders');

    }

    /**
     * Setzt den Upload auf fertig
     *
     * @return void
     */
    public function approvedAction() {
        $this->view->mode = 3;
        $row = Doctrine_Query::create()
            ->from('Orders m')
            ->where('id = ?', array(intval($this->_request->getParam('id'))))
            ->limit(intval($this->_request->getParam('limit')))
            ->offset(intval($this->_request->getParam('start')))
            ->fetchOne();

        // Redirect wenn der User nicht mit der Bestellung verknüpft ist
        if ($row->contact_id != $this->user->id) {
            $this->_forward('noaccess', 'error');
        }


        $produktionszeit = 0;

        foreach ($row->Orderspos as $orderpos) {
            $pro = unserialize($orderpos->data);
            $proo = $pro->getOptions();

            $countDays = 10;
            if($proo['produktion'] == "24std") {
                $countDays = 1;
            }
            if($proo['produktion'] == "exp") {
                $countDays = 5;
            }


            if ($countDays > $produktionszeit) {
                $produktionszeit = $countDays;
            }

            $obj = new TP_Delivery(date('Y-m-d H:i:s'));
            $result = $obj->getDeliveryDate($countDays);
            $orderpos->delivery_date = $result->format('Y-m-d H:i:s');
            $orderpos->status = 540;
            $orderpos->save();

        }

        if ($produktionszeit > 0) {
            $obj = new TP_Delivery(date('Y-m-d H:i:s'));
            $result = $obj->getDeliveryDate($produktionszeit);
            $row->delivery_date = $result->format('Y-m-d H:i:s');
            $row->save();
        }

        $row->status = 540;
        $row->save();

        TP_Queue::process('orderstatuschange', 'global', $row);



        $this->_redirect('/user/myorders');

    }

}
