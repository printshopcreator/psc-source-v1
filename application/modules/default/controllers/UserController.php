<?php

/**
 * User Controller
 *
 * Stellt das Usersystem fürs Frontend da
 *
 * PHP versions 4 and 5
 *
 * @category   Modul
 * @package    Default
 * @subpackage Controller
 * @author     Thomas petersomn <tpeterson@printshopcreator.de>
 * @copyright  1997-2009 Thomas Peterson
 * @license    Not Public
 * @version    SVN: $Id: UserController.php 7378 2012-03-10 16:45:56Z boonkerz $
 * @link       http://www.printshopcreator.de
 */

/**
 * User Controller
 *
 * Stellt das Usersystem fürs Frontend da
 *
 * PHP versions 4 and 5
 *
 * @category   Modules
 * @package    Default
 * @subpackage Controller
 * @author     Thomas petersomn <tpeterson@printshopcreator.de>
 * @copyright  1997-2009 Thomas Peterson
 * @license    Not Public
 * @version    Release: $Id: UserController.php 7378 2012-03-10 16:45:56Z boonkerz $
 * @link       http://www.printshopcreator.de
 */
class UserController extends TP_Controller_Action
{

    public function init() {
        parent::init();

        $contextSwitch = $this->_helper->contextSwitch();
        $contextSwitch
            ->addActionContext('jsonlogin', 'json')
            ->addActionContext('urllogin', 'json')
            ->addActionContext('addressupdate', 'json')
            ->addActionContext('addressload', 'json')
            ->addActionContext('addressget', 'json')
            ->addActionContext('accountgetall', 'json')
            ->addActionContext('addressadd', 'json')
            ->addActionContext('contactadd', 'json')
            ->addActionContext('contactupdate', 'json')
            ->addActionContext('contactcopy', 'json')
            ->addActionContext('contactinvite', 'json')
            ->addActionContext('collectiondeactivatecontact', 'json')
            ->addActionContext('collectionsendmail', 'json')
            ->addActionContext('contactaddempty', 'json')
            ->setAutoJsonSerialization(true)->initContext('json');

    }

    public function logoutAction() {

        $this->getHelper('layout')->disableLayout();
        $this->_helper->viewRenderer->setNoRender();
        Zend_Auth::getInstance()->clearIdentity();

        $basket = new TP_Basket();
        $basket->clear();

        $mode = new Zend_Session_Namespace('adminmode');
        $mode->liveedit = false;
        $mode->over_ride_contact = false;
        $this->view->liveedit = false;
        $this->view->priorityMessenger('Logout Success', 'success');

        if($this->shop->redirect_logout != "") {
            $this->redirectSpeak($this->shop->redirect_logout);
        }

        $this->redirectSpeak('/');
    }

    public function collectiondeactivatecontactAction() {
        $this->view->clearVars();
        $this->view->userUuid = $this->user->uuid;

        if(!$this->_getParam('uuid', false)) {
            $this->view->success = false;
            $this->view->errorCode = "404";

            return;
        }

        $contact = Doctrine_Query::create()->select()
            ->from('Contact c')
            ->where('c.uuid = ?', array($this->_getParam('uuid')))
            ->fetchOne();

        if(!$contact) {
            $this->view->success = false;
            $this->view->errorMessage = "404";

            return;
        }

        $contact->collecting_orders = false;
        $contact->save();

        $this->view->success = true;
    }

    public function addressgetAction() {
        $this->view->clearVars();
        $this->view->userId = $this->user->id;

	    if($this->_getParam('mode', false) == 2) {
            if($this->_getParam('term', false)) {
                $term = "%".$this->_getParam('term', false)."%";

                $addresses_delivery = Doctrine_Query::create()->from('ContactAddress a')->where('a.type = 2 AND a.contact_id = ?
            AND (company LIKE ? OR city LIKE ? OR zip LIKE ? OR firstname LIKE ? OR lastname LIKE ? OR street LIKE ? OR company2 LIKE ? OR zusatz1 LIKE ?)', array(
                    $this->user->id, $term,$term,$term,$term,$term,$term,$term,$term))->orderBy('a.pos ASC')->execute();

            }else {

                $addresses_delivery = Doctrine_Query::create()->from('ContactAddress a')->where('a.type = 2 AND a.contact_id = ?', array(
                    $this->user->id))->orderBy('a.pos ASC')->execute();
            }

            $data = array();

            foreach($addresses_delivery as $ad) {
                $temp = $ad->toArray();
                $temp['homepage'] = $ad->getHomepage();
                $temp['kundenNr'] = $ad->getKundenNr();
                $temp['anredeText'] = $ad->getAnrede();
                $data[] = $temp;
            }

            $this->view->data = $data;
            return;
        }

        if($this->_getParam('mode', false) == 1) {
            if($this->_getParam('term', false)) {
                $term = "%".$this->_getParam('term', false)."%";

                $addresses_delivery = Doctrine_Query::create()->from('ContactAddress a')->where('a.type = 1 AND a.contact_id = ?
            AND (company LIKE ? OR city LIKE ? OR zip LIKE ? OR firstname LIKE ? OR lastname LIKE ? OR street LIKE ? OR company2 LIKE ? OR zusatz1 LIKE ?)', array(
                    $this->user->id, $term,$term,$term,$term,$term,$term,$term,$term))->orderBy('a.pos ASC')->execute();

            }else {

                $addresses_delivery = Doctrine_Query::create()->from('ContactAddress a')->where('a.type = 1 AND a.contact_id = ?', array(
                    $this->user->id))->orderBy('a.pos ASC')->execute();
            }

            $data = array();

            foreach($addresses_delivery as $ad) {
                $temp = $ad->toArray();
                $temp['kundenNr'] = $ad->getKundenNr();
                $temp['homepage'] = $ad->getHomepage();
                $temp['anredeText'] = $ad->getAnrede();
                $data[] = $temp;
            }

            $this->view->data = $data;
            return;
        }

        if($this->_getParam('mode', false) == 3) {
            if($this->_getParam('term', false)) {
                $term = "%".$this->_getParam('term', false)."%";

                $addresses_delivery = Doctrine_Query::create()->from('ContactAddress a')->where('a.type = 3 AND a.contact_id = ?
            AND (company LIKE ? OR city LIKE ? OR zip LIKE ? OR firstname LIKE ? OR lastname LIKE ? OR street LIKE ? OR company2 LIKE ? OR zusatz1 LIKE ?)', array(
                    $this->user->id, $term,$term,$term,$term,$term,$term,$term,$term))->orderBy('a.pos ASC')->execute();

            }else {

                $addresses_delivery = Doctrine_Query::create()->from('ContactAddress a')->where('a.type = 3 AND a.contact_id = ?', array(
                    $this->user->id))->orderBy('a.pos ASC')->execute();
            }

            $data = array();

            foreach($addresses_delivery as $ad) {
                $temp = $ad->toArray();
                $temp['kundenNr'] = $ad->getKundenNr();
                $temp['homepage'] = $ad->getHomepage();
                $temp['anredeText'] = $ad->getAnrede();
                $data[] = $temp;
            }

            $this->view->data = $data;
            return;
        }

        if($this->_getParam('term', false)) {
            $term = "%".$this->_getParam('term', false)."%";

            $this->view->addresses_delivery = Doctrine_Query::create()->from('ContactAddress a')->where('a.type = 2 AND a.contact_id = ?
            AND (company LIKE ? OR city LIKE ? OR zip LIKE ? OR firstname LIKE ? OR lastname LIKE ? OR street LIKE ? OR company2 LIKE ? OR zusatz1 LIKE ?)', array(
                $this->user->id, $term,$term,$term,$term,$term,$term,$term,$term))->orderBy('a.pos ASC')->execute()->toArray();
            $this->view->addresses_invoice = Doctrine_Query::create()->from('ContactAddress a')->where('a.type = 1 AND a.contact_id = ?
            AND (company LIKE ? OR city LIKE ? OR zip LIKE ? OR firstname LIKE ? OR lastname LIKE ? OR street LIKE ? OR company2 LIKE ? OR zusatz1 LIKE ?)', array(
                $this->user->id, $term,$term,$term,$term,$term,$term,$term,$term))->orderBy('a.pos ASC')->execute()->toArray();
            $this->view->addresses_sender = Doctrine_Query::create()->from('ContactAddress a')->where('a.type = 3 AND a.contact_id = ?
            AND (company LIKE ? OR city LIKE ? OR zip LIKE ? OR firstname LIKE ? OR lastname LIKE ? OR street LIKE ? OR company2 LIKE ? OR zusatz1 LIKE ?)', array(
                $this->user->id, $term,$term,$term,$term,$term,$term,$term,$term))->orderBy('a.pos ASC')->execute()->toArray();

            return;
        }

        $this->view->addresses_delivery = Doctrine_Query::create()->from('ContactAddress a')->where('a.type = 2 AND a.contact_id = ?', array(
            $this->user->id))->orderBy('a.pos ASC')->execute()->toArray();
        $this->view->addresses_invoice = Doctrine_Query::create()->from('ContactAddress a')->where('a.type = 1 AND a.contact_id = ?', array(
            $this->user->id))->orderBy('a.pos ASC')->execute()->toArray();
        $this->view->addresses_sender = Doctrine_Query::create()->from('ContactAddress a')->where('a.type = 3 AND a.contact_id = ?', array(
            $this->user->id))->orderBy('a.pos ASC')->execute()->toArray();

    }

    public function contactinviteAction()
    {
        $this->view->clearVars();

        /** @var Contact $contact */
        $contact = Doctrine_Query::create()->from('Contact a')->where('a.uuid = ?', array(
            $this->_getParam('uuid')))->fetchOne();

        $contact->name = $this->_getParam('email');
        $contact->self_email = $this->_getParam('email');

        $contact->save();

        TP_Queue::process('collecting_orders_contact_add', 'global', $contact);

        $this->view->success = true;
    }

    public function contactcopyAction() {
        $this->view->clearVars();

        /** @var Contact $contactOrg */
        $contactOrg = Doctrine_Query::create()->from('Contact a')->where('a.uuid = ?', array(
            $this->_getParam('uuid')))->fetchOne();

        /** @var Contact $contact */
        $contact = $contactOrg->copy();
        $mongoCopy = $contactOrg->getArray();
        $contact->self_firstname = $contact->self_firstname;
        $contact->self_lastname = $contact->self_lastname;
        $contact->uuid = TP_Util::uuid();
        if($this->shop->getContactOwnNumber()) {

            $templates = array('template' => $this->shop->getContactNumberPattern());
            $twig = new \Twig\Environment(new \Twig\Loader\ArrayLoader($templates));

            $contact->setKundenNr($twig->render('template', array('number'=> $this->shop->getContactNumberStart())));
            $this->shop->setContactNumberStart($this->shop->getContactNumberStart()+1);
            $this->shop->saveMongo();
        }

        $contact->save();
        $contact->self_email = "copy_" . $contact->id . "_" . $contact->self_email;
        $contact->name = "copy_" . $contact->id . "_" . $contact->self_email;
        $contact->save();
        $contact->setCustom1($mongoCopy['custom1']);
        $contact->setCustom2($mongoCopy['custom3']);
        $contact->setCustom3($mongoCopy['custom3']);
        $contact->setCustom4($mongoCopy['custom4']);
        $contact->setCustom5($mongoCopy['custom5']);
        $contact->setCustom6($mongoCopy['custom6']);
        $contact->setCustom7($mongoCopy['custom7']);
        $contact->setCustom8($mongoCopy['custom8']);
        $contact->setCustom9($mongoCopy['custom9']);
        $contact->setCustom10($mongoCopy['custom10']);
        $contact->setCustom11($mongoCopy['custom11']);
        $contact->setCustom12($mongoCopy['custom13']);
        $contact->setCustom13($mongoCopy['custom13']);
        $contact->setCustom14($mongoCopy['custom14']);
        $contact->setCustom15($mongoCopy['custom15']);
        $contact->setCustom16($mongoCopy['custom16']);
        $contact->setCustom17($mongoCopy['custom17']);
        $contact->setCustom18($mongoCopy['custom18']);
        $contact->setCustom19($mongoCopy['custom19']);
        $contact->setCustom20($mongoCopy['custom20']);
        $contact->setCustom21($mongoCopy['custom21']);
        $contact->setCustom22($mongoCopy['custom22']);
        $contact->setCustom23($mongoCopy['custom23']);
        $contact->setCustom24($mongoCopy['custom24']);
        $contact->setCalcValue1($mongoCopy['calcValue1']);
        $contact->setCalcValue2($mongoCopy['calcValue2']);
        $contact->setShowOtherOrders($mongoCopy['showOtherOrders']);
        $contact->setShowOtherOrdersAccountFilter($mongoCopy['showOtherOrdersAccountFilter']);
        $contact->setShowOtherOrdersAccount($mongoCopy['showOtherOrdersAccount']);
        $contact->saveMongo();

        $contactaddress = new ContactAddress ();
        $contactaddress->anrede = "";
        $contactaddress->firstname = $contact->self_firstname;
        $contactaddress->lastname = $contact->self_lastname;
        $contactaddress->zip = $contact->self_zip;
        $contactaddress->city = $contact->self_city;
        $contactaddress->house_number = $contact->self_house_number;
        $contactaddress->street = $contact->self_street;
        $contactaddress->email = $contact->self_email;
        $contactaddress->phone = "";
        $contactaddress->mobil_phone = "";
        $contactaddress->company = "";
        $contactaddress->country = "";

        $contactaddress->contact_id = $contact->id;
        $contactaddress->install_id = $contact->install_id;
        $contactaddress->display = true;
        $contactaddress->type = 1;

        $contactaddress->save();
        $contactaddress->setKundenNr($contact->getKundenNr());
        $contactaddress->saveMongo();

        $shopcontact = new ShopContact ();
        $shopcontact->Contact = $contact;
        $shopcontact->Shop = $this->shop;
        $shopcontact->login = 1;
        $shopcontact->admin = 0;
        $shopcontact->save();

        $roles = new Zend_Config_Ini(APPLICATION_PATH . '/configs/roles.ini', APPLICATION_ENV);

        foreach ($roles->contact as $row) {
            $rolecontact = new ContactRole ();
            $rolecontact->Contact = $contact;
            $rolecontact->role_id = $row;
            $rolecontact->save();
        }

        $this->view->contact_id = $contact->id;
        $this->view->contact_uuid = $contact->uuid;
        $this->view->success = true;
    }

    public function contactaddemptyAction() {
        $this->view->clearVars();
        $this->view->userId = $this->user->id;

        $contact = new Contact ();
        if($this->shop->getContactOwnNumber()) {

            $templates = array('template' => $this->shop->getContactNumberPattern());
            $twig = new \Twig\Environment(new \Twig\Loader\ArrayLoader($templates));

            $contact->setKundenNr($twig->render('template', array('number'=> $this->shop->getContactNumberStart())));
            $this->shop->setContactNumberStart($this->shop->getContactNumberStart()+1);
            $this->shop->saveMongo();
        }

        $Buchstaben = array("a", "b", "c", "d", "e", "f", "g", "h", "k", "m", "n", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z");
        $Zahlen = array("2", "3", "4", "5", "6", "7", "8", "9");

        $Laenge = 6;


        for ($i = 0, $Passwort = ""; strlen($Passwort) < $Laenge; $i++) {
            if (rand(0, 2) == 0 && isset($Buchstaben)) {
                $Passwort .= $Buchstaben [rand(0, count($Buchstaben))];
            } elseif (rand(0, 2) == 1 && isset($Zahlen)) {
                if (isset($Zahlen [rand(0, count($Zahlen))])) {
                    $Passwort .= $Zahlen [rand(0, count($Zahlen))];
                }
            }
        }

        $contact->created = date('Y-m-d');
        $contact->updated = date('Y-m-d');
        $locale = Zend_Registry::get('locale');
        $contact->language = $locale->getLanguage() . '_' . $locale->getRegion();
        $contact->hash = $Passwort;
        $contact->password = password_hash($Passwort, PASSWORD_DEFAULT);
        $contact->enable = true;
        $contact->collecting_orders = true;
        $contact->Install = $this->shop->Install;
        $contact->account_id = $this->_getParam('account_id');

        $contact->liefer = 0;
        $contact->liefer_firstname = "";
        $contact->liefer_lastname = "";
        $contact->liefer_street = "";
        $contact->liefer_house_number = "";
        $contact->liefer_city = "";
        $contact->liefer_zip = "";
        $contact->liefer_phone = "";
        $contact->department = "";
        $contact->vonwo = "";
        $contact->passwordresetid = "";
        $contact->passwordresetdate = date('Y-m-d H:i:s');
        $contact->locked_master = 0;
        $contact->locked = 0;
        $contact->shop_name = "";
        $contact->shop_kto = "";
        $contact->shop_blz = "";
        $contact->shop_iban = "";
        $contact->shop_bic = "";
        $contact->shop_bank_name = "";
        $contact->bank_name = "";
        $contact->bank_kto = "";
        $contact->bank_blz = "";
        $contact->bank_iban = "";
        $contact->bank_bic = "";
        $contact->firstname = "";
        $contact->lastname = "";
        $contact->function = "";
        $contact->department = "";
        $contact->street = "";
        $contact->house_number = "";
        $contact->destrict = "";
        $contact->zip = "";
        $contact->city = "";
        $contact->state = "";
        $contact->country = "";
        $contact->email = "";
        $contact->web = "";
        $contact->foto = "";
        $contact->phone = "";
        $contact->bank_bank_name = "";
        $contact->self_durchwahl = "";
        $contact->self_abteilung = "";
        $contact->self_phone_lv = "";
        $contact->self_phone_vorwahl = "";
        $contact->self_phone_durchwahl = "";
        $contact->self_mobile_lv = "";
        $contact->self_mobile_vorwahl = "";
        $contact->self_mobile_durchwahl = "";
        $contact->self_fax_lv = "";
        $contact->self_fax_vorwahl = "";
        $contact->self_fax_phone = "";
        $contact->ustid = "";
        $contact->self_fax_durchwahl = "";
        $contact->self_phone_alternative_durchwahl = "";
        $contact->self_title = "";
        $contact->self_position = "";
        $contact->is_sek = 0;
        $contact->show_calc = 0;
        $contact->test_calc = 0;
        $contact->standart_delivery = 0;
        $contact->standart_sender = 0;
        $contact->standart_invoice = 0;


        $contact->save();
        $contact->saveMongo();

        $contactaddress = new ContactAddress ();
        $contactaddress->anrede = "";
        $contactaddress->firstname = "";
        $contactaddress->lastname = "";
        $contactaddress->zip = "";
        $contactaddress->city = "";
        $contactaddress->house_number = "";
        $contactaddress->street = "";
        $contactaddress->email = "";
        $contactaddress->phone = "";
        $contactaddress->mobil_phone = "";
        $contactaddress->company = "";
        $contactaddress->country = "";

        $contactaddress->contact_id = $contact->id;
        $contactaddress->install_id = $contact->install_id;
        $contactaddress->display = true;
        $contactaddress->type = 1;

        $contactaddress->save();
        $contactaddress->setKundenNr($contact->getKundenNr());
        $contactaddress->saveMongo();

        $shopcontact = new ShopContact ();
        $shopcontact->Contact = $contact;
        $shopcontact->Shop = $this->shop;
        $shopcontact->login = 1;
        $shopcontact->admin = 0;
        $shopcontact->save();

        $roles = new Zend_Config_Ini(APPLICATION_PATH . '/configs/roles.ini', APPLICATION_ENV);

        foreach ($roles->contact as $row) {
            $rolecontact = new ContactRole ();
            $rolecontact->Contact = $contact;
            $rolecontact->role_id = $row;
            $rolecontact->save();
        }

        $this->view->contact_id = $contact->id;
        $this->view->contact_uuid = $contact->uuid;
        $this->view->success = true;
    }

    public function accountgetallAction() {
        $this->view->clearVars();
        $this->view->userId = $this->user->id;

        if($this->_getParam('sort', false)) {
            $accounts = Doctrine_Query::create()->from('ShopAccount a')->where('a.shop_id = ?', array(
                $this->shop->id))->orderBy($this->_getParam('sort', false).' '.$this->_getParam('dir', false))->execute();
        }else{
            $accounts = Doctrine_Query::create()->from('ShopAccount a')->where('a.shop_id = ?', array(
                $this->shop->id))->execute();
        }


        $temp = array();

        foreach($accounts as $account) {
            if($account->Account->locked == 0) {
                $temp[] = array(
                    'id' => $account->account_id,
                    'label' => $account->Account->company,
                    'appendix' => $account->Account->appendix,
                    'hotelName' => $account->Account->hotel_name,
                    'subHotelName' => $account->Account->sub_hotel_name,
                    'locked' => $account->Account->locked

                );
            }
        }


        $this->view->accounts = $temp;
        if (Zend_Auth::getInstance()->hasIdentity()) {
            $this->view->myAccount = $this->user->account_id;
        }else{
            $this->view->myAccount = 0;
        }
        $this->view->success = true;
    }

    public function addressloadAction() {
        $this->view->clearVars();
        $this->view->success = true;
        $address = Doctrine_Query::create()->from('ContactAddress a')->where('a.uuid = ?', array(
            $this->_getParam('uuid')))->fetchOne();
        
        $this->view->address = array_merge($address->toArray(), ['homepage' => $address->getHomepage()]);
    }

    public function addressupdateAction() {

        if (file_exists($this->_configPath . '/user/address.ini')) {

            $config = new Zend_Config_Ini($this->_configPath . '/user/address.ini', 'add');
            if ($config->global) {
                $editform = new EasyBib_Form($config->user);
            } else {
                $editform = new Zend_Form($config->user);
            }
            $editform->setAttrib('id', 'updateaddress');
            $editform->setName('updateaddress');
            $editform->removeElement('submit');
            $editform->removeElement('type');
            $editform->removeElement('del');

            if (isset($editform->country)) {
                $editform->country->addMultiOptions(TP_Country::getCountrysAsArray());
            }
            $this->view->editform = $editform;

            $contactaddress = Doctrine_Query::create()->from('ContactAddress a')->where('a.uuid = ?', array(
                $this->_getParam('uuid')))->fetchOne();

            if ($this->_request->isPost() && $contactaddress) {

                $formData = $this->getRequest()->getParams();
                if ($editform->isValid($formData)) {

                    if (isset($formData['anrede'])) {
                        $contactaddress->anrede = $formData['anrede'];
                    }
                    if (isset($formData['mobil_phone'])) {
                        $contactaddress->mobil_phone = $formData['mobil_phone'];
                    }
                    if (isset($formData['email'])) {
                        $contactaddress->email = $formData['email'];
                    }
                    if (isset($formData['homepage'])) {
                        $contactaddress->setHomepage($formData['homepage']);
                    }
                    if (isset($formData['country'])) {
                        $contactaddress->country = $formData['country'];
                    }
                    if (isset($formData['company'])) {
                        $contactaddress->company = $formData['company'];
                    }
                    if (isset($formData['company2'])) {
                        $contactaddress->company2 = $formData['company2'];
                    }
                    if (isset($formData['kostenstellung'])) {
                        $contactaddress->kostenstellung = $formData['kostenstellung'];
                    }
                    if (isset($formData['abteilung'])) {
                        $contactaddress->abteilung = $formData['abteilung'];
                    }
                    if (isset($formData['fax'])) {
                        $contactaddress->fax = $formData['fax'];
                    }
                    if (isset($formData['ustid'])) {
                        $contactaddress->ustid = $formData['ustid'];
                    }
                    $contactaddress->firstname = $formData['firstname'];
                    $contactaddress->lastname = $formData['lastname'];
                    $contactaddress->zip = $formData['zip'];
                    $contactaddress->city = $formData['city'];
                    $contactaddress->house_number = $formData['house_number'];
                    $contactaddress->street = $formData['street'];
                    $contactaddress->phone = $formData['phone'];
                    $contactaddress->save();
                    $contactaddress->saveMongo();
                    $this->view->clearVars();
                    $this->view->success = true;
                    $this->view->address = $contactaddress->toArray();

                } else {
                    $this->view->clearVars();
                    $this->view->errors = $editform->getErrors();
                    $this->view->messages = $editform->getMessages();
                    $this->view->success = false;
                }
            }
        }
    }

    public function contactupdateAction() {
        $this->user->loadData();
        $this->user->setCustom1($this->_getParam('custom1'));
        $this->user->setCustom2($this->_getParam('custom2'));
        $this->user->setCustom3($this->_getParam('custom3'));

        Zend_Auth::getInstance()->getStorage()->write($this->user->toArray(true));
        $this->user->save();
        $this->user->saveMongo();

        $basket = new TP_Basket ();
        $sessart = $basket->getTempProduct();

        $sessart->setKst($this->_getParam('custom1') . ' ' . $this->_getParam('custom2'));

        $this->view->clearVars();
        $this->view->success = true;
    }

    public function collectionsendmailAction() {
        $this->view->clearVars();
        $this->view->success = false;

        $contact = Doctrine_Query::create()->from('Contact a')->where('a.uuid = ?', array(
            $this->_getParam('uuid')))->fetchOne();

        TP_Queue::process('collecting_orders_contact_add', 'global', $contact);

        $this->view->success = true;
    }

    public function contactaddAction() {
        $this->view->clearVars();
        $this->view->success = false;

        $config = new Zend_Config_Ini($this->_configPath . '/user/addcontact.ini', 'register');

        if ($config->global) {
            $addcontactform = new EasyBib_Form($config->global);
        } else {
            $addcontactform = new Zend_Form ();
            $addcontactform->addAttribs(array('class' => 'niceform', 'id' => 'userreg'));
        }

        $subform = new Zend_Form_SubForm($config->user->login);

        $addcontactform->addSubForm($subform, 'login');

        $addcontactform->login->self_email->addPrefixPath('TP_Validate', 'TP/Validate/', 'Validate');

        $rech = new Zend_Form_SubForm();
        $rech->addPrefixPath("TP_Form", "TP/Form");
        $rech->setConfig($config->user->rech);

        $addcontactform->addSubForm($rech, 'rech');

        if (isset($formData ['rech']) && isset($formData ['rech'] ['lieferja']) && $formData ['rech'] ['lieferja'] == 1) {
            $addcontactform->addSubForm(new Zend_Form_SubForm($config->user->liefer), 'liefer');
        }

        $addcontactform->addElement($config->user->submit->elements->submit->type, 'submit', $config->user->submit->elements->submit->options);

        if ($this->_request->isPost()) {

            $formData = $this->getRequest()->getParams();

            if ($addcontactform->isValid($formData) || isset($formData ['self_firstname'])) {

                $contact = new Contact ();
                if($this->shop->getContactOwnNumber()) {

                    $templates = array('template' => $this->shop->getContactNumberPattern());
                    $twig = new \Twig\Environment(new \Twig\Loader\ArrayLoader($templates));

                    $template = $twig->loadTemplate('template', $this->shop->getContactNumberPattern());

                    $contact->setKundenNr($template->render(array('number'=> $this->shop->getContactNumberStart())));
                    $this->shop->setContactNumberStart($this->shop->getContactNumberStart()+1);
                    $this->shop->saveMongo();
                }
                $contact->name = $formData ['login'] ['self_email'];
                if (isset($formData ['rech'] ['self_anrede'])) {
                    $contact->self_anrede = $formData ['rech'] ['self_anrede'];
                }

                $contact->self_firstname = $formData ['rech'] ['self_firstname'];
                $contact->self_lastname = $formData ['rech'] ['self_lastname'];
                $contact->self_email = $formData ['login'] ['self_email'];
                $contact->self_street = $formData ['rech'] ['self_street'];
                $contact->self_house_number = $formData ['rech'] ['self_house_number'];
                $contact->self_city = $formData ['rech'] ['self_city'];
                $contact->self_zip = $formData ['rech'] ['self_zip'];
                if (isset($formData ['rech'] ['self_phone'])) {
                    $contact->self_phone = $formData ['rech'] ['self_phone'];
                }
                if (isset($formData ['rech'] ['self_fax_phone'])) {
                    $contact->self_fax_phone = $formData ['rech'] ['self_fax_phone'];
                }
                if (isset($formData ['rech'] ['self_mobile_durchwahl'])) {
                    $contact->self_mobile_durchwahl = $formData ['rech'] ['self_mobile_durchwahl'];
                }

                if (isset($formData ['self_firstname'])) {
                    $contact->self_firstname = $formData['self_firstname'];
                }
                if (isset($formData ['self_lastname'])) {
                    $contact->self_lastname = $formData['self_lastname'];
                }
                if (isset($formData ['self_email'])) {
                    $contact->self_email = $formData['self_email'];
                    $contact->name = $formData ['self_email'];
                }
                if (isset($formData ['self_street'])) {
                    $contact->self_street = $formData['self_street'];
                }
                if (isset($formData ['self_house_number'])) {
                    $contact->self_house_number = $formData['self_house_number'];
                }
                if (isset($formData ['self_city'])) {
                    $contact->self_city = $formData['self_city'];
                }
                if (isset($formData ['self_zip'])) {
                    $contact->self_zip = $formData['self_zip'];
                }
                if (isset($formData ['self_anrede'])) {
                    $contact->self_anrede = $formData['self_anrede'];
                }
                if (isset($formData ['self_country'])) {
                    $contact->self_country = $formData['self_country'];
                }
                if (isset($formData ['self_department'])) {
                    $contact->self_department = $formData ['self_department'];
                }
                if (isset($formData ['self_abteilung'])) {
                    $contact->self_abteilung = $formData ['self_abteilung'];
                }
                if (isset($formData ['self_title'])) {
                    $contact->self_title = $formData ['self_title'];
                }
                if (isset($formData ['self_position'])) {
                    $contact->self_position = $formData ['self_position'];
                }
                if (isset($formData ['self_function'])) {
                    $contact->self_function = $formData ['self_function'];
                }
                if (isset($formData ['self_web'])) {
                    $contact->self_web = $formData ['self_web'];
                }
                if (isset($formData ['self_title'])) {
                    $contact->self_title = $formData ['self_title'];
                }


                if (isset($formData ['rech'] ['rech-self_phone']) && isset($formData ['rech'] ['rech-self_phone']['lv'])) {
                    $contact->self_phone_lv = $formData ['rech'] ['rech-self_phone']['lv'];
                    $contact->self_phone_vorwahl = $formData ['rech'] ['rech-self_phone']['vorwahl'];
                    $contact->self_phone = $formData ['rech'] ['rech-self_phone']['phone'];
                    $contact->self_phone_durchwahl = $formData ['rech'] ['rech-self_phone']['durchwahl'];
                }

                if (isset($formData ['rech'] ['rech-self_mobile']) && isset($formData ['rech'] ['rech-self_mobile']['lv'])) {
                    $contact->self_mobile_lv = $formData ['rech'] ['rech-self_mobile']['lv'];
                    $contact->self_mobile_vorwahl = $formData ['rech'] ['rech-self_mobile']['vorwahl'];
                    $contact->self_mobile_durchwahl = $formData ['rech'] ['rech-self_mobile']['durchwahl'];
                }

                if (isset($formData ['rech'] ['rech-self_fax']) && isset($formData ['rech'] ['rech-self_fax']['lv'])) {
                    $contact->self_fax_lv = $formData ['rech'] ['rech-self_fax']['lv'];
                    $contact->self_fax_vorwahl = $formData ['rech'] ['rech-self_fax']['vorwahl'];
                    $contact->self_fax_phone = $formData ['rech'] ['rech-self_fax']['phone'];
                    $contact->self_fax_durchwahl = $formData ['rech'] ['rech-self_fax']['durchwahl'];
                }

                if (isset($formData ['rech'] ['self_phone']) && isset($formData ['rech'] ['self_phone']['lv'])) {
                    $contact->self_phone_lv = $formData ['rech'] ['self_phone']['lv'];
                    $contact->self_phone_vorwahl = $formData ['rech'] ['self_phone']['vorwahl'];
                    $contact->self_phone = $formData ['rech'] ['self_phone']['phone'];
                    $contact->self_phone_durchwahl = $formData ['rech'] ['self_phone']['durchwahl'];
                }

                if (isset($formData ['rech'] ['self_mobile']) && isset($formData ['rech'] ['self_mobile']['lv'])) {
                    $contact->self_mobile_lv = $formData ['rech'] ['self_mobile']['lv'];
                    $contact->self_mobile_vorwahl = $formData ['rech'] ['self_mobile']['vorwahl'];
                    $contact->self_mobile_durchwahl = $formData ['rech'] ['self_mobile']['durchwahl'];
                }

                if (isset($formData ['rech'] ['self_fax']) && isset($formData ['rech'] ['self_fax']['lv'])) {
                    $contact->self_fax_lv = $formData ['rech'] ['self_fax']['lv'];
                    $contact->self_fax_vorwahl = $formData ['rech'] ['self_fax']['vorwahl'];
                    $contact->self_fax_phone = $formData ['rech'] ['self_fax']['phone'];
                    $contact->self_fax_durchwahl = $formData ['rech'] ['self_fax']['durchwahl'];
                }

                if($this->shop->install_id==16) {

                    if (isset($formData ['rech'] ['self_phone']) && isset($formData ['rech'] ['self_phone']['lv'])) {
                        $contact->self_phone_lv = $formData ['rech'] ['self_phone']['lv'];
                        $contact->self_phone_vorwahl = $formData ['rech'] ['self_phone']['vorwahl'];
                        $contact->self_phone = $formData ['rech'] ['self_phone']['phone'];
                        $contact->self_phone_durchwahl = $formData ['rech'] ['self_phone']['durchwahl'];
                    }

                    if (isset($formData ['rech'] ['self_mobile']) && isset($formData ['rech'] ['self_mobile']['lv'])) {
                        $contact->self_mobile_lv = $formData ['rech'] ['self_mobile']['lv'];
                        $contact->self_mobile_vorwahl = $formData ['rech'] ['self_mobile']['vorwahl'];
                        $contact->self_mobile_durchwahl = $formData ['rech'] ['self_mobile']['durchwahl'];
                    }

                    if (isset($formData ['rech'] ['self_fax']) && isset($formData ['rech'] ['self_fax']['lv'])) {
                        $contact->self_fax_lv = $formData ['rech'] ['self_fax']['lv'];
                        $contact->self_fax_vorwahl = $formData ['rech'] ['self_fax']['vorwahl'];
                        $contact->self_fax_phone = $formData ['rech'] ['self_fax']['phone'];
                        $contact->self_fax_durchwahl = $formData ['rech'] ['self_fax']['durchwahl'];
                    }

                    $fields = array(
                        'department_2' => 'department_2',
                        'street_2' => 'street_2',
                        'house_number_2' => 'house_number_2',
                        'country_1' => 'country_1',
                        'self_country' => 'country_1',
                        'lkz_2' => 'lkz_2',
                        'lkz_1' => 'lkz_1',
                        'country_2' => 'country_2',
                        'land_2' => 'country_2',
                        'zip_2' => 'zip_2',
                        'city_2' => 'city_2',
                        'email_2' => 'email_2',
                        'phone_lv_2' => 'phone_lv_2',
                        'phone_vorwahl_2' => 'phone_vorwahl_2',
                        'phone_2' => 'phone_2',
                        'phone_durchwahl_2' => 'phone_durchwahl_2',
                        'mobile_lv_2' => 'mobile_lv_2',
                        'mobile_vorwahl_2' => 'mobile_vorwahl_2',
                        'mobile_2' => 'mobile_2',
                        'fax_lv_2' => 'fax_lv_2',
                        'fax_vorwahl_2' => 'fax_vorwahl_2',
                        'fax_2' => 'fax_2',
                        'fax_durchwahl_2' => 'fax_durchwahl_2',
                        'department_3' => 'department_3',
                        'street_3' => 'street_3',
                        'house_number_3' => 'house_number_3',
                        'country_3' => 'land_3',
                        'zip_3' => 'zip_3',
                        'city_3' => 'city_3',
                        'email_3' => 'email_3',
                        'phone_lv_3' => 'phone_lv_3',
                        'lkz_3' => 'lkz_3',
                        'country_3' => 'country_3',
                        'phone_vorwahl_3' => 'phone_vorwahl_3',
                        'phone_3' => 'phone_3',
                        'phone_durchwahl_3' => 'phone_durchwahl_3',
                        'mobile_lv_3' => 'mobile_lv_3',
                        'mobile_vorwahl_3' => 'mobile_vorwahl_3',
                        'mobile_3' => 'mobile_3',
                        'fax_lv_3' => 'fax_lv_3',
                        'fax_vorwahl_3' => 'fax_vorwahl_3',
                        'fax_3' => 'fax_3',
                        'fax_durchwahl_3' => 'fax_durchwahl_3',
                        'internet_2' => 'internet_2',
                        'internet_3' => 'internet_3'
                    );

                    $infos = array();

                    foreach($fields as $key => $value) {

                        if (isset($formData [$key])) {
                            $infos[$value] = $formData [$key];
                        }
                        if (isset($formData['rech'] [$key])) {
                            $infos[$value] = $formData [$key];
                        }
                        if (isset($formData['rech'] [$key])) {
                            $infos[$value] = $formData [$key];
                        }

                    }

                    if (isset($formData ['rech'] ['phone_2']) && isset($formData ['rech'] ['phone_2']['lv'])) {
                        $infos["phone_lv_2"] = $formData ['rech'] ['phone_2']['lv'];
                        $infos["phone_vorwahl_2"] = $formData ['rech'] ['phone_2']['vorwahl'];
                        $infos["phone_2"] = $formData ['rech'] ['phone_2']['phone'];
                        $infos["phone_durchwahl_2"] = $formData ['rech'] ['phone_2']['durchwahl'];
                    }
                    if (isset($formData ['rech'] ['mobile_2']) && isset($formData ['rech'] ['mobile_2']['lv'])) {
                        $infos["mobile_lv_2"] = $formData ['rech'] ['mobile_2']['lv'];
                        $infos["mobile_vorwahl_2"] = $formData ['rech'] ['mobile_2']['vorwahl'];
                        $infos["mobile_2"] = $formData ['rech'] ['mobile_2']['phone'];
                    }
                    if (isset($formData ['rech'] ['fax_2']) && isset($formData ['rech'] ['fax_2']['lv'])) {
                        $infos["fax_lv_2"] = $formData ['rech'] ['fax_2']['lv'];
                        $infos["fax_vorwahl_2"] = $formData ['rech'] ['fax_2']['vorwahl'];
                        $infos["fax_2"] = $formData ['rech'] ['fax_2']['phone'];
                        $infos["fax_durchwahl_2"] = $formData ['rech'] ['fax_2']['durchwahl'];
                    }
                    if (isset($formData ['rech'] ['phone_3']) && isset($formData ['rech'] ['phone_3']['lv'])) {
                        $infos["phone_lv_3"] = $formData ['rech'] ['phone_3']['lv'];
                        $infos["phone_vorwahl_3"] = $formData ['rech'] ['phone_3']['vorwahl'];
                        $infos["phone_3"] = $formData ['rech'] ['phone_3']['phone'];
                        $infos["phone_durchwahl_3"] = $formData ['rech'] ['phone_3']['durchwahl'];
                    }
                    if (isset($formData ['rech'] ['mobile_3']) && isset($formData ['rech'] ['mobile_3']['lv'])) {
                        $infos["mobile_lv_3"] = $formData ['rech'] ['mobile_3']['lv'];
                        $infos["mobile_vorwahl_3"] = $formData ['rech'] ['mobile_3']['vorwahl'];
                        $infos["mobile_3"] = $formData ['rech'] ['mobile_3']['phone'];
                    }
                    if (isset($formData ['rech'] ['fax_3']) && isset($formData ['rech'] ['fax_3']['lv'])) {
                        $infos["fax_lv_3"] = $formData ['rech'] ['fax_3']['lv'];
                        $infos["fax_vorwahl_3"] = $formData ['rech'] ['fax_3']['vorwahl'];
                        $infos["fax_3"] = $formData ['rech'] ['fax_3']['phone'];
                        $infos["fax_durchwahl_3"] = $formData ['rech'] ['fax_3']['durchwahl'];
                    }

                    $contact->self_information = json_encode($infos);

                }

                if (isset($formData ['rech'] ['self_department'])) {
                    $contact->self_department = $formData ['rech'] ['self_department'];
                }
                if (isset($formData ['rech'] ['self_country'])) {
                    $contact->self_country = $formData ['rech'] ['self_country'];
                }
                $contact->vonwo = $formData ['login'] ['vonwo'];

                if (isset($formData ['login']) && isset($formData ['login'] ['newsletter']) && $formData ['login'] ['newsletter'] == 1) {
                    $contact->newsletter = 1;
                } else {
                    $contact->newsletter = 0;
                }

                if (isset($formData ['rech']) && $formData ['rech'] ['lieferja'] == 1) {

                    $contact->liefer = 1;
                    $contact->liefer_firstname = $formData ['liefer'] ['self_firstname'];
                    $contact->liefer_lastname = $formData ['liefer'] ['self_lastname'];
                    $contact->liefer_street = $formData ['liefer'] ['self_street'];
                    $contact->liefer_house_number = $formData ['liefer'] ['self_house_number'];
                    $contact->liefer_city = $formData ['liefer'] ['self_city'];
                    $contact->liefer_zip = $formData ['liefer'] ['self_zip'];
                    $contact->liefer_phone = $formData ['liefer'] ['self_phone'];
                    $contact->department = $formData ['liefer'] ['self_department'];

                } else {

                    $contact->liefer = 0;
                }

                $Buchstaben = array("a", "b", "c", "d", "e", "f", "g", "h", "k", "m", "n", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z");
                $Zahlen = array("2", "3", "4", "5", "6", "7", "8", "9");

                $Laenge = 6;

                for ($i = 0, $Passwort = ""; strlen($Passwort) < $Laenge; $i++) {
                    if (rand(0, 2) == 0 && isset($Buchstaben)) {
                        $Passwort .= $Buchstaben [rand(0, count($Buchstaben))];
                    } elseif (rand(0, 2) == 1 && isset($Zahlen)) {
                        if (isset($Zahlen [rand(0, count($Zahlen))])) {
                            $Passwort .= $Zahlen [rand(0, count($Zahlen))];
                        }
                    }
                }
                if (isset($formData ['password'])) {
                    $Passwort = $formData ['password'];
                }

                $contact->created = date('Y-m-d');
                $contact->updated = date('Y-m-d');
                $locale = Zend_Registry::get('locale');
                $contact->language = $locale->getLanguage() . '_' . $locale->getRegion();
                $contact->hash = $Passwort;
                $contact->password = password_hash($Passwort, PASSWORD_DEFAULT);
                $contact->enable = true;
                $contact->collecting_orders = true;
                $contact->Install = $this->shop->Install;
                $contact->account_id = $this->shop->default_account;
                $contact->save();
                $contact->saveMongo();

                $contactaddress = new ContactAddress ();
                $contactaddress->anrede = $contact->self_anrede;
                $contactaddress->firstname = $contact->self_firstname;
                $contactaddress->lastname = $contact->self_lastname;
                $contactaddress->zip = $contact->self_zip;
                $contactaddress->city = $contact->self_city;
                $contactaddress->house_number = $contact->self_house_number;
                $contactaddress->street = $contact->self_street;
                $contactaddress->email = $contact->self_email;
                $contactaddress->phone = $contact->self_phone;
                if (isset($formData ['rech'] ['self_mobile_durchwahl'])) {
                    $contactaddress->mobil_phone = $formData ['rech'] ['self_mobile_durchwahl'];
                }

                $contactaddress->country = $contact->self_country;
                if (isset($formData ['rech'] ['self_fax_phone'])) {
                    $contactaddress->fax = $formData ['rech'] ['self_fax_phone'];
                }
                $contactaddress->contact_id = $contact->id;
                $contactaddress->install_id = $contact->install_id;
                $contactaddress->display = true;
                $contactaddress->type = 1;

                $contactaddress->save();
                $contactaddress->setKundenNr($contact->getKundenNr());
                $contactaddress->saveMongo();

                $shopcontact = new ShopContact ();
                $shopcontact->Contact = $contact;
                $shopcontact->Shop = $this->shop;
                $shopcontact->save();

                $roles = new Zend_Config_Ini(APPLICATION_PATH . '/configs/roles.ini', APPLICATION_ENV);

                foreach ($roles->contact as $row) {
                    $rolecontact = new ContactRole ();
                    $rolecontact->Contact = $contact;
                    $rolecontact->role_id = $row;
                    $rolecontact->save();
                }

                if (isset($formData ['private_products']) && is_array($formData ['private_products'])) {
                    foreach($formData ['private_products'] as $ppKey => $pp) {
                        $ppObj = new ContactArticle();
                        $ppObj->article_id = $pp;
                        $ppObj->contact_id = $contact->id;
                        $ppObj->save();
                    }
                }

                TP_Queue::process('collecting_orders_contact_add', 'global', $contact);

                $this->view->success = true;
            } else {
                $this->view->errors = $addcontactform->getErrors();
                $this->view->messages = $addcontactform->getMessages();

            }
        }
    }

    public function addressaddAction() {

        $this->view->clearVars();

        $basket = new TP_Basket();

        if (file_exists($this->_configPath . '/user/address.ini')) {

            $config = new Zend_Config_Ini($this->_configPath . '/user/address.ini', 'add');
            $form = new Zend_Form($config->user);

            $form->removeElement('update');
            $form->removeElement('del');

            if (isset($form->country)) {
                $form->country->addMultiOptions(TP_Country::getCountrysAsArray());
                $form->country->setValue("DE");
            }

            if ($this->_request->isPost()) {

                $formData = $this->getRequest()->getParams();

                if ($form->isValid($formData)) {

                    $contactaddress = new ContactAddress();

                    $contactaddress->anrede = 1;
                    $contactaddress->email = "";
                    $contactaddress->mobil_phone = "";
                    $contactaddress->company = "";
                    if (isset($formData['anrede'])) {
                        $contactaddress->anrede = $formData['anrede'];
                    }
                    if (isset($formData['mobil_phone'])) {
                        $contactaddress->mobil_phone = $formData['mobil_phone'];
                    }
                    if (isset($formData['email'])) {
                        $contactaddress->email = $formData['email'];
                    }
                    if (isset($formData['country'])) {
                        $contactaddress->country = $formData['country'];
                    }
                    if (isset($formData['homepage'])) {
                        $contactaddress->setHomepage($formData['homepage']);
                    }
                    if (isset($formData['company'])) {
                        $contactaddress->company = $formData['company'];
                    }
                    if (isset($formData['company2'])) {
                        $contactaddress->company2 = $formData['company2'];
                    }
                    if (isset($formData['kostenstellung'])) {
                        $contactaddress->kostenstellung = $formData['kostenstellung'];
                    }
                    if (isset($formData['abteilung'])) {
                        $contactaddress->abteilung = $formData['abteilung'];
                    }
                    if (isset($formData['fax'])) {
                        $contactaddress->fax = $formData['fax'];
                    }
                    $contactaddress->firstname = $formData['firstname'];
                    $contactaddress->lastname = $formData['lastname'];
                    $contactaddress->zip = $formData['zip'];
                    $contactaddress->city = $formData['city'];
                    $contactaddress->house_number = $formData['house_number'];
                    $contactaddress->street = $formData['street'];
                    $contactaddress->phone = $formData['phone'];


                    if ($mode->over_ride_contact != false && $mode->over_ride_contact != 'new') {
                        $contactaddress->contact_id = $mode->over_ride_contact;
                    } else {
                        $contactaddress->contact_id = $this->user->id;
                    }
                    $contactaddress->install_id = $this->shop->install_id;
                    $contactaddress->display = true;

                    if (!$this->_getParam('uuid')) {
                        if (in_array('1', $formData['type']) || (!is_array($formData['type']) && $formData['type'] == 1)) {
                            $cs = $contactaddress->copy();
                            $cs->uuid = "";
                            $cs->type = 1;
                            $cs->save();
                            if (isset($formData['homepage'])) {
                                $cs->setHomepage($formData['homepage']);
                            }
                            $cs->setKundenNr($this->user->getKundenNr());
                            $cs->saveMongo();
                            $basket->setInvoice($cs->uuid);
                            $this->view->invoice = array('uuid' => $cs->uuid, 'str' => $cs->getAddressStringWithBreak(), 'anrede' => $cs->getAnrede(), 'company' => $cs->getCompany(), 'firstname' => $cs->getFirstname(), 'lastname' => $cs->getLastname(), 'street' => $cs->getStreet(), 'housenumber' => $cs->getHouseNumber(), 'zip' => $cs->getZip(), 'city' => $cs->getCity(), 'country' => $cs->getCountry());
                        }
                        if (in_array('2', $formData['type']) || (!is_array($formData['type']) && $formData['type'] == 2)) {
                            $cs = $contactaddress->copy();
                            $cs->uuid = "";
                            $cs->type = 2;
                            $cs->save();
                            if (isset($formData['homepage'])) {
                                $cs->setHomepage($formData['homepage']);
                            }
                            $cs->setKundenNr($this->user->getKundenNr());
                            $cs->saveMongo();
                            $basket->setDelivery($cs->uuid);
                            $this->view->delivery = array('uuid' => $cs->uuid, 'str' => $cs->getAddressStringWithBreak(), 'anrede' => $cs->getAnrede(), 'company' => $cs->getCompany(), 'firstname' => $cs->getFirstname(), 'lastname' => $cs->getLastname(), 'street' => $cs->getStreet(), 'housenumber' => $cs->getHouseNumber(), 'zip' => $cs->getZip(), 'city' => $cs->getCity(), 'country' => $cs->getCountry());
                        }
                        if (in_array('3', $formData['type']) || (!is_array($formData['type']) && $formData['type'] == 3)) {
                            $cs = $contactaddress->copy();
                            $cs->uuid = "";
                            $cs->type = 3;
                            $cs->save();
                            if (isset($formData['homepage'])) {
                                 $cs->setHomepage($formData['homepage']);
                            }
                            $cs->setKundenNr($this->user->getKundenNr());
                            $cs->saveMongo();
                            $basket->setSender($cs->uuid);
                            $this->view->sender = array('uuid' => $cs->uuid, 'str' => $cs->getAddressStringWithBreak(), 'anrede' => $cs->getAnrede(), 'company' => $cs->getCompany(), 'firstname' => $cs->getFirstname(), 'lastname' => $cs->getLastname(), 'street' => $cs->getStreet(), 'housenumber' => $cs->getHouseNumber(), 'zip' => $cs->getZip(), 'city' => $cs->getCity(), 'country' => $cs->getCountry());
                        }
                        $this->view->success = true;
                    } else {
                        $contactaddress->save();
                        if (isset($formData['homepage'])) {
                            $contactaddress->setHomepage($formData['homepage']);
                        }
                        $contactaddress->setKundenNr($this->user->getKundenNr());
                        $contactaddress->saveMongo();
                    }
                } else {
                    $this->view->errors = $form->getErrors();
                    $this->view->messages = $form->getMessages();
                    $this->view->success = false;
                }

            }

        } else {
            $config = new Zend_Config_Ini($this->_configPath . '/user/registercontact.ini', 'register');
            $form = new Zend_Form();
            $form->addAttribs(array(
                'class' => 'niceform',
                'id' => 'userreg'));
            $form->addSubForm(new Zend_Form_SubForm($config->user->liefer), 'liefer');
            $form->addElement('submit', 'submit', $config->user->liefersubmit->elements->submit->options);
            if ($this->_request->isPost()) {
                $formData = $this->_request->getPost();
                if ($form->isValid($formData) && $this->_getParam('name') != 'Benutzername') {
                    $contactaddress = new ContactAddress();
                    if (isset($formData['liefer']['anrede'])) {
                        $contactaddress->anrede = $formData['liefer']['anrede'];
                    }
                    $contactaddress->firstname = $formData['liefer']['firstname'];
                    $contactaddress->lastname = $formData['liefer']['lastname'];
                    $contactaddress->zip = $formData['liefer']['zip'];
                    $contactaddress->city = $formData['liefer']['city'];
                    $contactaddress->house_number = $formData['liefer']['house_number'];
                    $contactaddress->street = $formData['liefer']['street'];
                    $contactaddress->email = $formData['liefer']['email'];
                    $contactaddress->phone = $formData['liefer']['phone'];
                    $contactaddress->company = $formData['liefer']['company'];
                    $contactaddress->fax = $formData['liefer']['fax'];
                    if ($mode->over_ride_contact != false && $mode->over_ride_contact != 'new') {
                        $contactaddress->contact_id = $mode->over_ride_contact;
                    } else {
                        $contactaddress->contact_id = $this->user->id;
                    }
                    $contactaddress->install_id = $this->user->install_id;
                    $contactaddress->display = true;
                    $contactaddress->type = 2;
                    $contactaddress->save();
                } else {
                    $this->view->errors = $form->getErrorMessages();
                }
            }
        }

    }

    public function deladdressAction() {
        $this->view->mode = 6;

        if (!Zend_Auth::getInstance()->hasIdentity()) {
            $this->forwardSpeak('login', 'user');
        }

        if ($this->_getParam('uuid')) {
            $contactaddress = Doctrine_Query::create()->select()
                ->from('ContactAddress s')
                ->where('s.uuid = ? AND s.contact_id = ? AND s.display = 1', array($this->_getParam('uuid'), $this->user->id))
                ->fetchOne();

            if (!$contactaddress) {
                $this->redirectSpeak('/user/myaddress');
            }

            $config = new Zend_Config_Ini($this->_configPath . '/user/address.ini', 'add');
            $form = new Zend_Form($config->user);

            if (isset($form->country)) {
                $form->country->addMultiOptions(TP_Country::getCountrysAsArray());
                $form->country->setValue("DE");
            }

            if ($this->_request->isPost()) {

                $user = Doctrine_Query::create()->select()
                    ->from('ContactAddress s')
                    ->where('s.uuid = ? AND s.contact_id = ?', array($this->_getParam('uuid'), $this->user->id))
                    ->fetchOne();

                if ($user) {
                    $user->display = 0;
                    $user->save();
                    $this->view->priorityMessenger('Adresse erfolgreich gelöscht', 'success');
                    $this->redirectSpeak('/user/myaddress');
                }
            } else {
                $form->populate($contactaddress->toArray());
            }
            $form->removeElement('type');
            $form->addElement('hidden', 'uuid', array('value' => $this->_getParam('uuid')));
            $form->removeElement('submit');
            $form->removeElement('update');
            $this->view->form = $form;
        }
    }

    public function setaddressasdefaultAction() {
        $this->view->mode = 6;

        if (!Zend_Auth::getInstance()->hasIdentity()) {
            $this->forwardSpeak('login', 'user');
        }

        $address = Doctrine_Query::create()->select()
            ->from('ContactAddress s')
            ->where('s.uuid = ? AND s.contact_id = ?', array($this->_getParam('uuid'), $this->user->id))
            ->fetchOne();
        if ($address) {

            if ($this->_getParam('type') == 'invoice') {

                $this->user->standart_invoice = $address->id;
                $this->user->save();
                $this->view->priorityMessenger('Erfolgreich als Rechnungsadresse gesetzt', 'success');
            } elseif ($this->_getParam('type') == 'delivery') {
                $this->user->standart_delivery = $address->id;
                $this->user->save();
                $this->view->priorityMessenger('Erfolgreich als Lieferadresse gesetzt', 'success');
            } elseif ($this->_getParam('type') == 'sender') {
                $this->user->standart_sender = $address->id;
                $this->user->save();
                $this->view->priorityMessenger('Erfolgreich als Absendeadresse gesetzt', 'success');
            }
        }

        $this->redirectSpeak('/user/myaddress');
    }

    public function addaddressAction() {
        $this->view->mode = 6;

        if (!Zend_Auth::getInstance()->hasIdentity()) {
            $this->forwardSpeak('login', 'user');
        }

        $countrys = TP_Country::getCountrysAsArray();

        $config = new Zend_Config_Ini($this->_configPath . '/user/address.ini', 'add');

        $form = new Zend_Form($config->user);
        if ($this->_getParam('mode', false)) {
            $form->removeElement('type');
        }
        if (isset($form->country)) {
            if(!$form->country->getMultiOptions()) {
                $form->country->addMultiOptions($countrys);
            }
            $form->country->setValue("DE");
        }

        if ($this->_request->isPost()) {

            $formData = $this->getRequest()->getParams();

            if ($form->isValid($formData) && ((isset($formData['type']) || $this->_getParam('mode', false)) || $this->_getParam('uuid'))) {

                if ($this->_getParam('uuid')) {
                    $contactaddress = Doctrine_Query::create()->select()
                        ->from('ContactAddress s')
                        ->where('s.uuid = ? AND s.contact_id = ?', array($this->_getParam('uuid'), $this->user->id))
                        ->fetchOne();

                    if (!$contactaddress) {
                        $contactaddress = new ContactAddress();
                    }
                } else {
                    $contactaddress = new ContactAddress();
                }

                $contactaddress->anrede = 1;
                $contactaddress->email = "";
                $contactaddress->mobil_phone = "";
                $contactaddress->company = "";
                if (isset($formData['anrede'])) {
                    $contactaddress->anrede = $formData['anrede'];
                }
                if (isset($formData['mobil_phone'])) {
                    $contactaddress->mobil_phone = $formData['mobil_phone'];
                }
                if (isset($formData['email'])) {
                    $contactaddress->email = $formData['email'];
                }
                if (isset($formData['country'])) {
                    $contactaddress->country = $formData['country'];
                }
                if (isset($formData['company'])) {
                    $contactaddress->company = $formData['company'];
                }
                if (isset($formData['company2'])) {
                    $contactaddress->company2 = $formData['company2'];
                }
                if (isset($formData['fax'])) {
                    $contactaddress->fax = $formData['fax'];
                }
                if (isset($formData['ustid'])) {
                    $contactaddress->ustid = $formData['ustid'];
                }
                if (isset($formData['homepage'])) {
                    $contactaddress->setHomepage($formData['homepage']);
                }
                $contactaddress->firstname = $formData['firstname'];
                $contactaddress->lastname = $formData['lastname'];
                $contactaddress->zip = $formData['zip'];
                $contactaddress->city = $formData['city'];
                $contactaddress->house_number = $formData['house_number'];
                $contactaddress->street = $formData['street'];
                $contactaddress->phone = $formData['phone'];

                if ($mode->over_ride_contact != false && $mode->over_ride_contact != 'new') {
                    $contactaddress->contact_id = $mode->over_ride_contact;
                } else {
                    $contactaddress->contact_id = $this->user->id;
                }
                $contactaddress->install_id = $this->user->install_id;
                $contactaddress->display = true;

                if (!$this->_getParam('uuid')) {
                    if ($this->_getParam('mode', false)) {
                        $cs = $contactaddress->copy();
                        $cs->uuid = "";
                        $cs->type = intval($this->_getParam('mode', false));
                        $cs->save();
                    } else {
                        if (in_array('1', $formData['type'])) {
                            $cs = $contactaddress->copy();
                            $cs->uuid = "";
                            $cs->type = 1;
                            $cs->save();
                        }
                        if (in_array('2', $formData['type'])) {
                            $cs = $contactaddress->copy();
                            $cs->uuid = "";
                            $cs->type = 2;
                            $cs->save();
                        }
                        if (in_array('3', $formData['type'])) {
                            $cs = $contactaddress->copy();
                            $cs->uuid = "";
                            $cs->type = 3;
                            $cs->save();
                        }
                    }
                } else {
                    $contactaddress->save();
                    $contactaddress->saveMongo();
                }

                $this->view->priorityMessenger('Adresse erfolgreich gespeichert', 'success');
                $this->redirectSpeak('/user/myaddress');
            } else {
                $form->populate($formData);
            }
        }

        if ($this->_getParam('uuid')) {

            $user = Doctrine_Query::create()->select()
                ->from('ContactAddress s')
                ->where('s.uuid = ? AND s.contact_id = ?', array($this->_getParam('uuid'), $this->user->id))
                ->fetchOne();
            $user->loadData();
            if ($user) {
                $form->removeElement('type');
                $form->addElement('hidden', 'uuid', array('value' => $this->_getParam('uuid')));
                $form->populate($user->getOrderSaveArray());
                $form->removeElement('submit');
            }
            $form->removeElement('del');
            $this->view->form = $form;
            $this->renderScript('user/editaddress.phtml');
        } else {
            $form->removeElement('update');
            $form->removeElement('del');
            $this->view->form = $form;
        }
    }

    public function myaddressAction() {
        $this->view->mode = 6;

        if (!Zend_Auth::getInstance()->hasIdentity()) {
            $this->forwardSpeak('login', 'user');
        }

        $this->view->delivery_addresses = Doctrine_Query::create()->select()->from('ContactAddress s')->where('s.contact_id = ? AND s.type = 2 AND s.display = 1', array($this->user->id))->execute();

        $this->view->sender_addresses = Doctrine_Query::create()->select()->from('ContactAddress s')->where('s.contact_id = ? AND s.type = 3 AND s.display = 1', array($this->user->id))->execute();

        $this->view->invoice_addresses = Doctrine_Query::create()->select()->from('ContactAddress s')->where('s.contact_id = ? AND s.type = 1 AND s.display = 1', array($this->user->id))->execute();
    }

    public function myfavarticleAction() {

        if (!Zend_Auth::getInstance()->hasIdentity()) {
            $this->forwardSpeak('login', 'user');
        }

        $this->view->mode = 5;

        $this->view->article = Doctrine_Query::create()->select()->from('Article a')->leftJoin('a.ContactFavArticle c')->where('c.contact_uuid = ?', array($this->user->id))->execute();
    }

    /**
     * Reset Password
     *
     * @return void
     */
    public function resetpasswordAction() {
        $config = new Zend_Config_Ini($this->_configPath . '/user/resetpassword.ini', 'reset');

        if ($config->global) {
            $form = new EasyBib_Form($config->user->reset);
            $form->addAttribs(array('class' => $config->global->class, 'id' => 'userreset'));
        } else {
            $form = new Zend_Form($config->user->reset);
            $form->addAttribs(array('class' => 'niceform', 'id' => 'userreset'));
        }

        if (isset($form->cp)) {
            $form->cp->setOptions(array(
                'captcha' => array(
                    'captcha' => 'Image',
                    'font' => APPLICATION_PATH . '/fonts/verdana.ttf',
                    'imgDir' => PUBLIC_PATH . '/temp/thumb/',
                    'imgUrl' => '/temp/thumb/',
                    'wordLen' => '4'
                ),
            ));
        }

        if ($this->getRequest()->isXmlHttpRequest() && $form->isValid($this->getRequest()->getPost())) {
            TP_Queue::process('contactreset', 'global', $this->_getParam('self_email'));

            if (Zend_Registry::get('queueerror')) {
                echo json_encode(array('success' => true));
            } else {
                echo json_encode(array('success' => false));
            }
            die();
        }

        if ($this->getRequest()->isPost() && $form->isValid($this->getRequest()->getPost())) {


            $contact = Doctrine_Query::create()->from('Contact m')->leftJoin('m.Shops as s')->where('s.id = ? AND m.self_email = ? AND m.enable = 1 AND m.virtual = 0', array(
                $this->shop->id, $this->_getParam('self_email')))->fetchOne();

            if($contact) {
                $dbMongo = TP_Mongo::getInstance();
                $dbMongo->Job->insertOne(array(
                    "shop" => $this->shop->id,
                    "event" => "contact_password_reset_start",
                    "data" => ["contact" => $contact->uuid],
                    "created" => new MongoDB\BSON\UTCDateTime(),
                    "updated" => new MongoDB\BSON\UTCDateTime()
                ));

                $this->view->priorityMessenger('Resetmail successfull', 'success');
                $this->redirectSpeak('/user/resetpassworddone');
            }else{
                $this->view->priorityMessenger('Benutzer nicht gefunden', 'error');
                $this->redirectSpeak('/user/resetpassword');
            }

        }

        $this->view->form = $form;

        if ($this->shop->private && !Zend_Auth::getInstance()->hasIdentity()) {
            $this->_helper->layout->setLayout('private');
        } else {
            $this->_helper->layout->setLayout('default');
        }
    }

    public function resetpassworddoneAction() {
        if ($this->shop->private && !Zend_Auth::getInstance()->hasIdentity()) {
            $this->_helper->layout->setLayout('private');
        } else {
            $this->_helper->layout->setLayout('default');
        }
    }

    /**
     * Reset Password finish
     *
     * @return void
     */
    public function resetpasswordfinishAction() {

        if (!$this->_getParam('uid') || $this->_getParam('uid') == '') {
            $this->forwardSpeak('index', 'index', 'default');
        }

        $contact = Doctrine_Query::create()->from('Contact m')->where('m.passwordresetid = ? AND m.enable = 1', array($this->_getParam('uid')))->fetchOne();

        if (!$contact) {
            $this->view->priorityMessenger('Sie hatten diesen Link bereits aufgerufen!', 'error');
            return $this->forwardSpeak('index', 'index', 'default');
        }

        $Buchstaben = array("a", "b", "c", "d", "e", "f", "g", "h", "k", "m", "n", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z");
        $Zahlen = array("2", "3", "4", "5", "6", "7", "8", "9");

        $Laenge = 6;

        for ($i = 0, $Passwort = ""; strlen($Passwort) < $Laenge; $i++) {
            if (rand(0, 2) == 0 && isset($Buchstaben)) {
                $Passwort .= $Buchstaben [rand(0, (count($Buchstaben) - 1))];
            } elseif (rand(0, 2) == 1 && isset($Zahlen)) {
                $Passwort .= $Zahlen [rand(0, (count($Zahlen) - 1))];
            }
        }
        Zend_Registry::set('password', $Passwort);
        $contact->password = password_hash($Passwort, PASSWORD_DEFAULT);
        $contact->passwordresetid = "test";
        $contact->save();

        TP_Queue::process('contactresetfinish', 'global', $contact);


        $dbMongo = TP_Mongo::getInstance();
        $dbMongo->Job->insertOne(array(
            "shop" => $this->shop->id,
            "event" => "contact_password_reset_finish",
            "data" => ["contact" => $contact->uuid],
            "created" => new MongoDB\BSON\UTCDateTime(),
            "updated" => new MongoDB\BSON\UTCDateTime()
        ));



        $this->view->priorityMessenger('Resetmail successfull', 'success');

        if ($this->shop->private && !Zend_Auth::getInstance()->hasIdentity()) {
            $this->_helper->layout->setLayout('private');
        } else {
            $this->_helper->layout->setLayout('default');
        }
    }

    public function mydataAction() {
        if (!Zend_Auth::getInstance()->hasIdentity()) {
            $this->forwardSpeak('login', 'user');
        }

        $this->view->mode = 7;
        $config = new Zend_Config_Ini($this->_configPath . '/user/mydata.ini', 'mydata');

        $countrys = TP_Country::getCountrysAsArray();

        $formData = $this->_request->getPost();

        $this->user->loadData();
        $identity = $this->user;
        $form = new EasyBib_Form($config->user);
//        $subform = new Zend_Form_SubForm($config->user->login);


 //       $form->addSubForm(new Zend_Form_SubForm($config->user->rech), 'rech');
        if (isset($form->self_country)) {
            if(!$form->self_country->getMultiOptions()) {
                $form->self_country->addMultiOptions($countrys);
            }
        }

        if ($this->_request->isPost()) {

            if ($form->isValid($formData)) {

                if (isset($formData['self_anrede'])) {
                    $this->user->self_anrede = $formData['self_anrede'];
                }
                if(isset($formData['self_firstname'])) {
                    $this->user->self_firstname = $formData['self_firstname'];
                }
                if(isset($formData['self_lastname'])) {
                    $this->user->self_lastname = $formData['self_lastname'];
                }
                if (isset($formData['self_fax_phone'])) {
                    $this->user->self_fax_phone = $formData['self_fax_phone'];
                }
                if (isset($formData['self_mobile_durchwahl'])) {
                    $this->user->self_mobile_durchwahl = $formData['self_mobile_durchwahl'];
                }
                if (isset($formData['self_phone_mobile'])) {
                    $this->user->self_phone_mobile = $formData['self_phone_mobile'];
                }
                if (isset($formData['self_phone'])) {
                    $this->user->self_phone = $formData['self_phone'];
                }
                if (isset($formData['self_department'])) {
                    $this->user->self_department = $formData['self_department'];
                }
                if (isset($formData['self_country'])) {
                    $this->user->self_country = $formData['self_country'];
                }
                if (isset($formData['self_abteilung'])) {
                    $this->user->self_abteilung = $formData['self_abteilung'];
                }
                if (isset($formData['self_kostenstellung'])) {
                    $this->user->self_kostenstellung = $formData['self_kostenstellung'];
                }
				if (isset($formData['ustid'])) {
                    $this->user->ustid = $formData['ustid'];
                }	
                if (isset($formData['self_birthday'])) {
                    $this->user->self_birthday = $formData['self_birthday'];
                }
                if (isset($formData['self_web'])) {
                    $this->user->self_web = $formData['self_web'];
                }
                if (isset($formData['custom1'])) {
                    $this->user->setCustom1($formData['custom1']);
                }
                if (isset($formData['custom2'])) {
                    $this->user->setCustom2($formData['custom2']);
                }
                if (isset($formData['custom3'])) {
                    $this->user->setCustom3($formData['custom3']);
                }
                if (isset($formData['custom4'])) {
                    $this->user->setCustom4($formData['custom4']);
                }
                if (isset($formData['custom5'])) {
                    $this->user->setCustom5($formData['custom5']);
                }
                if (isset($formData['custom6'])) {
                    $this->user->setCustom6($formData['custom6']);
                }
                if (isset($formData['custom7'])) {
                    $this->user->setCustom7($formData['custom7']);
                }
                if (isset($formData['custom8'])) {
                    $this->user->setCustom8($formData['custom8']);
                }
                if (isset($formData['custom9'])) {
                    $this->user->setCustom9($formData['custom9']);
                }
                if (isset($formData['custom10'])) {
                    $this->user->setCustom10($formData['custom10']);
                }
                if (isset($formData['custom11'])) {
                    $this->user->setCustom11($formData['custom11']);
                }
                if (isset($formData['custom12'])) {
                    $this->user->setCustom12($formData['custom12']);
                }
                if (isset($formData['custom13'])) {
                    $this->user->setCustom13($formData['custom13']);
                }
                if (isset($formData['custom14'])) {
                    $this->user->setCustom14($formData['custom14']);
                }
                if (isset($formData['custom15'])) {
                    $this->user->setCustom15($formData['custom15']);
                }
                if (isset($formData['custom16'])) {
                    $this->user->setCustom16($formData['custom16']);
                }
                if (isset($formData['custom17'])) {
                    $this->user->setCustom17($formData['custom17']);
                }
                if (isset($formData['custom18'])) {
                    $this->user->setCustom18($formData['custom18']);
                }
                if (isset($formData['custom19'])) {
                    $this->user->setCustom19($formData['custom19']);
                }
                if (isset($formData['custom20'])) {
                    $this->user->setCustom20($formData['custom20']);
                }
                if (isset($formData['custom21'])) {
                    $this->user->setCustom21($formData['custom21']);
                }
                if (isset($formData['custom22'])) {
                    $this->user->setCustom22($formData['custom22']);
                }
                if (isset($formData['custom23'])) {
                    $this->user->setCustom23($formData['custom23']);
                }
                if (isset($formData['custom24'])) {
                    $this->user->setCustom24($formData['custom24']);
                }
                if (isset($formData ['self_phone_lv'])) {
                    $this->user->self_phone_lv = $formData ['self_phone_lv'];
                }
                if (isset($formData ['self_phone_vorwahl'])) {
                    $this->user->self_phone_vorwahl = $formData ['self_phone_vorwahl'];
                }
                if (isset($formData ['self_phone'])) {
                    $this->user->self_phone = $formData ['self_phone'];
                }
                if (isset($formData ['self_phone_durchwahl'])) {
                    $this->user->self_phone_durchwahl = $formData ['self_phone_durchwahl'];
                }

                if (isset($formData ['self_fax_lv'])) {
                    $this->user->self_fax_lv = $formData ['self_fax_lv'];
                }
                if (isset($formData ['self_fax_vorwahl'])) {
                    $this->user->self_fax_vorwahl = $formData ['self_fax_vorwahl'];
                }
                if (isset($formData ['self_fax_phone'])) {
                    $this->user->self_fax_phone = $formData ['self_fax_phone'];
                }
                if (isset($formData ['self_fax_durchwahl'])) {
                    $this->user->self_fax_durchwahl = $formData ['self_fax_durchwahl'];
                }

                if (isset($formData ['self_mobile_lv'])) {
                    $this->user->self_mobile_lv = $formData ['self_mobile_lv'];
                }
                if (isset($formData ['self_mobile_vorwahl'])) {
                    $this->user->self_mobile_vorwahl = $formData ['self_mobile_vorwahl'];
                }
                if (isset($formData ['self_phone_mobile'])) {
                    $this->user->self_phone_mobile = $formData ['self_phone_mobile'];
                }
                if (isset($formData ['self_mobile_durchwahl'])) {
                    $this->user->self_mobile_durchwahl = $formData ['self_mobile_durchwahl'];
                }
                $this->user->saveMongo(); 

                Zend_Auth::getInstance()->getStorage()->write($this->user->toArray(true));

                $this->user->save();

                $this->view->priorityMessenger('Benutzereinstellungen erfolgreich geändert', 'success');
            } else {
                $this->view->priorityMessenger('Benutzereinstellungen nicht geändert. Bitte überprüfen Sie Ihre Eingaben', 'error');
            }

            $form->populate($formData);
        } else {
            $formData = $identity->toArray();

            $formData['custom1'] = $identity->getCustom1();
            $formData['custom2'] = $identity->getCustom2();
            $formData['custom3'] = $identity->getCustom3();
            $formData['custom4'] = $identity->getCustom4();
            $formData['custom5'] = $identity->getCustom5();
            $formData['custom6'] = $identity->getCustom6();
            $formData['custom7'] = $identity->getCustom7();
            $formData['custom8'] = $identity->getCustom8();
            $formData['custom9'] = $identity->getCustom9();
            $formData['custom10'] = $identity->getCustom10();
            $formData['custom11'] = $identity->getCustom11();
            $formData['custom12'] = $identity->getCustom12();
            $formData['custom13'] = $identity->getCustom13();
            $formData['custom14'] = $identity->getCustom14();
            $formData['custom15'] = $identity->getCustom15();
            $formData['custom16'] = $identity->getCustom16();
            $formData['custom17'] = $identity->getCustom17();
            $formData['custom18'] = $identity->getCustom18();
            $formData['custom19'] = $identity->getCustom19();
            $formData['custom20'] = $identity->getCustom20();
            $formData['custom21'] = $identity->getCustom21();
            $formData['custom22'] = $identity->getCustom22();
            $formData['custom23'] = $identity->getCustom23();
            $formData['custom24'] = $identity->getCustom24();

            $form->populate($formData);
        }

        $this->view->form = $form;
 
    }
    /**
     * Meine Einstellungen Aktion
     *
     * @return void
     *
     */
    public function mysettingsAction() {

        if (!Zend_Auth::getInstance()->hasIdentity()) {
            $this->forwardSpeak('login', 'user');
        }

        $this->view->mode = 2;
        $config = new Zend_Config_Ini($this->_configPath . '/user/updatecontact.ini', 'register');

        $vonwo = new Zend_Config_Ini(APPLICATION_PATH . '/configs/' . '/systemdefaults.ini', 'vonwo');

        $countrys = TP_Country::getCountrysAsArray();

        $formData = $this->_request->getPost();

        $this->user->loadData();
        $identity = $this->user;
        if ($config->global) {
            $form = new EasyBib_Form($config->global);
        } else {
            $form = new Zend_Form ();
            $form->addAttribs(array('class' => 'niceform', 'id' => 'userreg'));
        }
        $subform = new Zend_Form_SubForm($config->user->login);

        if (isset($config->user->vonwo)) {
            $subform->addElement('select', 'vonwo', array('label' => 'Wie haben Sie uns gefunden?', 'multiOptions' => $vonwo->toArray()));
        }

        $form->addSubForm($subform, 'login');

        if (isset($config->user->bank)) {
            $form->addSubForm(new Zend_Form_SubForm($config->user->bank), 'bank');
        }

        if (isset($form->login->password_re)) {
            $form->login->password_re->addPrefixPath('TP_Validate', 'TP/Validate/', 'Validate');
        }

        $form->addSubForm(new Zend_Form_SubForm($config->user->rech), 'rech');
        if (isset($form->rech->self_country)) {
            if(!$form->rech->self_country->getMultiOptions()) {
                $form->rech->self_country->addMultiOptions($countrys);
            }
        }
        if ($this->user->shop_kto != "" && $this->shop->market) {
            $form->addSubForm(new Zend_Form_SubForm($config->user->bank), 'bank');
        }
        if (isset($config->user->person)) {
            $form->addSubForm(new Zend_Form_SubForm($config->user->person), 'person');
        }
        if (isset($config->user->settings)) {
            $paymenttypeids = array(0);
            $shippingtypeids = array(0);

            if (Zend_Auth::getInstance()->hasIdentity()) {
                $user = Zend_Auth::getInstance()->getIdentity();
                $user = Doctrine_Query::create()->from('Contact m')->where('id = ?')->fetchOne(array(
                    $user['id']
                ));

                foreach ($user->ContactPaymenttype as $Paymenttype) {
                    array_push($paymenttypeids, $Paymenttype->paymenttype_id);
                }

                foreach ($user->ContactShippingtype as $shippingtype) {
                    array_push($shippingtypeids, $shippingtype->shippingtype_id);
                }

                foreach ($user->Account->AccountPaymenttype as $Paymenttype) {
                    array_push($paymenttypeids, $Paymenttype->paymenttype_id);
                }

                foreach ($user->Account->AccountShippingtype as $shippingtype) {
                    array_push($shippingtypeids, $shippingtype->shippingtype_id);
                }
            }

            foreach ($this->shop->ShopShippingtype as $shippingtype) {
                if ($shippingtype->shop_id == $this->shop->id) {
                    array_push($shippingtypeids, $shippingtype->shippingtype_id);
                }
            }
            $shippingtypes = Doctrine_Query::create()->from('Shippingtype c')->where('c.shop_id = ? AND c.enable = 1 AND (c.private = 0 OR (c.private = 1 AND c.id in (' . implode(',', $shippingtypeids) . '))) AND weight_from = 0 AND weight_to = 0 AND price_from = 0 AND price_to = 0', array(
                $this->shop->id
            ))->orderBy('pos ASC')->fetchArray();

            $payments = Doctrine_Query::create()->from('Paymenttype c')->where('c.shop_id = ? AND c.enable = 1 AND (c.private = 0 OR (c.private = 1 AND c.id in (' . implode(',', $paymenttypeids) . ')))', array(
                $this->shop->id
            ))->orderBy('pos ASC')->fetchArray();

            $form->addSubForm(new Zend_Form_SubForm($config->user->settings), 'settings');
            if (isset($form->settings->defaultPayment)) {
                if(!$form->settings->defaultPayment->getMultiOptions()) {
                    $temp = [];
                    foreach($payments as $payment) {
                        $temp[$payment['id']] = $payment['title'];
                    }
                    $form->settings->defaultPayment->addMultiOptions($temp);
                }
            }
            if (isset($form->settings->defaultShipment)) {
                if(!$form->settings->defaultShipment->getMultiOptions()) {
                    $temp = [];
                    foreach($shippingtypes as $shippingtype) {
                        $temp[$shippingtype['id']] = $shippingtype['title'];
                    }
                    $form->settings->defaultShipment->addMultiOptions($temp);
                }
            }
        }


        $form->addElement($config->user->submit->elements->submit->type, 'submit', $config->user->submit->elements->submit->options);

        if ($this->_request->isPost()) {

            if ($form->isValid($formData)) {
                if(isset($formData ['settings'] ['defaultPayment'])) {
                    $this->user->setDefaultPayment($formData ['settings'] ['defaultPayment']);
                }
                if(isset($formData ['settings'] ['language'])) {
                    $this->user->language = $formData ['settings'] ['language'];
                }
                if(isset($formData ['settings'] ['defaultShipment'])) {
                    $this->user->setDefaultShipment($formData ['settings'] ['defaultShipment']);
                }
 
                if ($formData ['login'] ['password'] != "" && $formData ['login'] ['password_re'] != "") {
                    $this->user->password = $formData ['login'] ['password'];
                }
                if (isset($formData ['rech'] ['self_anrede'])) {
                    $this->user->self_anrede = $formData ['rech'] ['self_anrede'];
                }
                if(isset($formData ['rech'] ['self_firstname'])) {
                    $this->user->self_firstname = $formData ['rech'] ['self_firstname'];
                }
                if(isset($formData ['rech'] ['self_lastname'])) {
                    $this->user->self_lastname = $formData ['rech'] ['self_lastname'];
                }
                if(isset($formData ['person'] ['self_anrede'])) {
                    $this->user->self_anrede = $formData ['person'] ['self_anrede'];
                }
                if(isset($formData ['person'] ['self_firstname'])) {
                    $this->user->self_firstname = $formData ['person'] ['self_firstname'];
                }
                if(isset($formData ['person'] ['self_lastname'])) {
                    $this->user->self_lastname = $formData ['person'] ['self_lastname'];
                }
                if(isset($formData ['login'] ['self_email'])) {
                    $this->user->self_email = $formData ['login'] ['self_email'];
                }
                if(isset($formData ['person'] ['self_street'])) {
                    $this->user->self_street = $formData ['rech'] ['self_street'];
                }
                if(isset($formData ['person'] ['self_house_number'])) {
                    $this->user->self_house_number = $formData ['rech'] ['self_house_number'];
                }
                if(isset($formData ['person'] ['self_city'])) {
                    $this->user->self_city = $formData ['rech'] ['self_city'];
                }
                if(isset($formData ['person'] ['self_zip'])) {
                    $this->user->self_zip = $formData ['rech'] ['self_zip'];
                }
                if (isset($formData ['rech'] ['ustid'])) {
                    $this->user->ustid = $formData ['rech'] ['ustid'];
                }
                if (isset($formData ['rech'] ['self_phone_lv'])) {
                    $this->user->self_phone_lv = $formData ['rech'] ['self_phone_lv'];
                }
                if (isset($formData ['rech'] ['self_phone_vorwahl'])) {
                    $this->user->self_phone_vorwahl = $formData ['rech'] ['self_phone_vorwahl'];
                }
                if (isset($formData ['rech'] ['self_phone'])) {
                    $this->user->self_phone = $formData ['rech'] ['self_phone'];
                }
                if (isset($formData ['rech'] ['self_phone_durchwahl'])) {
                    $this->user->self_phone_durchwahl = $formData ['rech'] ['self_phone_durchwahl'];
                }

                if (isset($formData ['rech'] ['self_fax_lv'])) {
                    $this->user->self_fax_lv = $formData ['rech'] ['self_fax_lv'];
                }
                if (isset($formData ['rech'] ['self_fax_vorwahl'])) {
                    $this->user->self_fax_vorwahl = $formData ['rech'] ['self_fax_vorwahl'];
                }
                if (isset($formData ['rech'] ['self_fax_phone'])) {
                    $this->user->self_fax_phone = $formData ['rech'] ['self_fax_phone'];
                }
                if (isset($formData ['rech'] ['self_fax_durchwahl'])) {
                    $this->user->self_fax_durchwahl = $formData ['rech'] ['self_fax_durchwahl'];
                }

                if (isset($formData ['rech'] ['self_mobile_lv'])) {
                    $this->user->self_mobile_lv = $formData ['rech'] ['self_mobile_lv'];
                }
                if (isset($formData ['rech'] ['self_mobile_vorwahl'])) {
                    $this->user->self_mobile_vorwahl = $formData ['rech'] ['self_mobile_vorwahl'];
                }
                if (isset($formData ['rech'] ['self_phone_mobile'])) {
                    $this->user->self_phone_mobile = $formData ['rech'] ['self_phone_mobile'];
                }
                if (isset($formData ['rech'] ['self_mobile_durchwahl'])) {
                    $this->user->self_mobile_durchwahl = $formData ['rech'] ['self_mobile_durchwahl'];
                }
                if (isset($formData ['rech'] ['self_department'])) {
                    $this->user->self_department = $formData ['rech'] ['self_department'];
                }
                if (isset($formData ['rech'] ['self_country'])) {
                    $this->user->self_country = $formData ['rech'] ['self_country'];
                }
                if (isset($formData ['rech'] ['self_abteilung'])) {
                    $this->user->self_abteilung = $formData ['rech'] ['self_abteilung'];
                }
                if (isset($formData ['rech'] ['self_kostenstellung'])) {
                    $this->user->self_kostenstellung = $formData ['rech'] ['self_kostenstellung'];
                }
					
                if (isset($formData ['rech'] ['self_birthday'])) {
                    $this->user->self_birthday = $formData ['rech'] ['self_birthday'];
                }
                if (isset($formData ['bank'] ['bank_iban'])) {
                    $this->user->bank_iban = $formData ['bank'] ['bank_iban'];
                }
                if (isset($formData ['bank'] ['bank_bic'])) {
                    $this->user->bank_bic = $formData ['bank'] ['bank_bic'];
                }
                if (count($this->user->ContactAddress) == 0) {

                    $contactaddress = new ContactAddress ();
                    $contactaddress->anrede = $this->user->self_anrede;
                    $contactaddress->firstname = $this->user->self_firstname;
                    $contactaddress->lastname = $this->user->self_lastname;
                    $contactaddress->zip = $this->user->self_zip;
                    $contactaddress->city = $this->user->self_city;
                    $contactaddress->house_number = $this->user->self_house_number;
                    $contactaddress->street = $this->user->self_street;
                    $contactaddress->email = $this->user->self_email;
                    $contactaddress->phone = $this->user->self_phone;
                    $contactaddress->company = $this->user->self_department;
                    if (isset($formData ['rech'] ['self_mobile_durchwahl'])) {
                        $contactaddress->mobil_phone = $formData ['rech'] ['self_mobile_durchwahl'];
                    }
                    if (isset($formData ['rech'] ['self_phone_mobile'])) {
                        $contactaddress->mobil_phone = $formData ['rech'] ['self_phone_mobile'];
                    }
                    $contactaddress->contact_id = $this->user->id;
                    $contactaddress->install_id = $this->user->install_id;
                    $contactaddress->display = true;
                    $contactaddress->type = 1;
                    if (isset($formData ['rech'] ['self_country'])) {
                        $contactaddress->country = $formData ['rech'] ['self_country'];
                    }
                    if (isset($formData ['rech'] ['self_fax_phone'])) {
                        $contactaddress->fax = $formData ['rech'] ['self_fax_phone'];
                    }
                    if (isset($formData ['rech'] ['self_abteilung'])) {
                        $contactaddress->abteilung = $formData ['rech'] ['self_abteilung'];
                    }
                    if (isset($formData ['rech'] ['self_kostenstellung'])) {
                        $contactaddress->kostenstellung = $formData ['rech'] ['self_kostenstellung'];
                    }
                    $contactaddress->save();
                }

                $this->user->vonwo = $formData ['login'] ['vonwo'];

                $newsletter = $this->user->newsletter;

                if (isset($formData ['login']) && isset($formData ['login'] ['newsletter']) && $formData ['login'] ['newsletter'] == 1) {
                    $this->user->newsletter = 1;
                    $this->user->self_newsletter = 1;
                } else {
                    $this->user->newsletter = 0;
                    $this->user->self_newsletter = 0;
                }

                if (isset($formData ['rech']) && isset($formData ['rech'] ['newsletter']) && $formData ['rech'] ['newsletter'] == 1) {
                    $this->user->newsletter = 1;
                    $this->user->self_newsletter = 1;
                } else {
                    $this->user->newsletter = 0;
                    $this->user->self_newsletter = 0;
                }
                if (isset($formData ['rech']) && isset($formData ['rech'] ['self_newsletter']) && $formData ['rech'] ['self_newsletter'] == 1) {
                    $this->user->newsletter = 1;
                    $this->user->self_newsletter = 1;
                } else {
                    $this->user->newsletter = 0;
                    $this->user->self_newsletter = 0;
                }

                if($newsletter != $this->user->newsletter) {
                    TP_Queue::process('newsletter_change_status', 'global', $this->user);
                }

                if (isset($formData ['bank'])) {
                    $this->user->shop_kto = $formData ['bank']['shop_kto'];
                    $this->user->shop_blz = $formData ['bank']['shop_blz'];
                    $this->user->shop_bic = $formData ['bank']['shop_bic'];
                    $this->user->shop_iban = $formData ['bank']['shop_iban'];
                    $this->user->shop_bank_name = $formData ['bank']['shop_bank_name'];
                }
                if (isset($formData ['rech'] ['custom1'])) {
                    $this->user->setCustom1($formData ['rech'] ['custom1']);
                }
                if (isset($formData ['rech'] ['custom2'])) {
                    $this->user->setCustom2($formData ['rech'] ['custom2']);
                }
                if (isset($formData ['rech'] ['custom3'])) {
                    $this->user->setCustom3($formData ['rech'] ['custom3']);
                }
                if (isset($formData ['rech'] ['custom4'])) {
                    $this->user->setCustom4($formData ['rech'] ['custom4']);
                }
                if (isset($formData ['rech'] ['custom5'])) {
                    $this->user->setCustom5($formData ['rech'] ['custom5']);
                }
                if (isset($formData ['rech'] ['custom6'])) {
                    $this->user->setCustom6($formData ['rech'] ['custom6']);
                }
                if (isset($formData ['rech'] ['custom7'])) {
                    $this->user->setCustom7($formData ['rech'] ['custom7']);
                }
                if (isset($formData ['rech'] ['custom8'])) {
                    $this->user->setCustom8($formData ['rech'] ['custom8']);
                }
                if (isset($formData ['rech'] ['custom9'])) {
                    $this->user->setCustom9($formData ['rech'] ['custom9']);
                }
                if (isset($formData ['rech'] ['custom10'])) {
                    $this->user->setCustom10($formData ['rech'] ['custom10']);
                }
                if (isset($formData ['rech'] ['custom11'])) {
                    $this->user->setCustom11($formData ['rech'] ['custom11']);
                }
                if (isset($formData ['rech'] ['custom12'])) {
                    $this->user->setCustom12($formData ['rech'] ['custom12']);
                }
                if (isset($formData ['rech'] ['custom13'])) {
                    $this->user->setCustom13($formData ['rech'] ['custom13']);
                }
                if (isset($formData ['rech'] ['custom14'])) {
                    $this->user->setCustom14($formData ['rech'] ['custom14']);
                }
                if (isset($formData ['rech'] ['custom15'])) {
                    $this->user->setCustom15($formData ['rech'] ['custom15']);
                }
                if (isset($formData ['rech'] ['custom16'])) {
                    $this->user->setCustom16($formData ['rech'] ['custom16']);
                }
                if (isset($formData ['rech'] ['custom17'])) {
                    $this->user->setCustom17($formData ['rech'] ['custom17']);
                }
                if (isset($formData ['rech'] ['custom18'])) {
                    $this->user->setCustom18($formData ['rech'] ['custom18']);
                }
                if (isset($formData ['rech'] ['custom19'])) {
                    $this->user->setCustom19($formData ['rech'] ['custom19']);
                }
                if (isset($formData ['rech'] ['custom20'])) {
                    $this->user->setCustom20($formData ['rech'] ['custom20']);
                }
                if (isset($formData ['rech'] ['custom21'])) {
                    $this->user->setCustom21($formData ['rech'] ['custom21']);
                }
                if (isset($formData ['rech'] ['custom22'])) {
                    $this->user->setCustom22($formData ['rech'] ['custom22']);
                }
                if (isset($formData ['rech'] ['custom23'])) {
                    $this->user->setCustom23($formData ['rech'] ['custom23']);
                }
                if (isset($formData ['rech'] ['custom24'])) {
                    $this->user->setCustom24($formData ['rech'] ['custom24']);
                }
                $this->user->saveMongo(); 

                Zend_Auth::getInstance()->getStorage()->write($this->user->toArray(true));

                $this->user->save();

                $this->view->priorityMessenger('Benutzereinstellungen erfolgreich geändert', 'success');
            } else {
                $this->view->priorityMessenger('Benutzereinstellungen nicht geändert. Bitte überprüfen Sie Ihre Eingaben', 'error');
            }

            $form->populate($formData);
        } else {
            $formData ['rech'] = $identity->toArray();

            $formData ['bank'] = $identity;
            $formData ['settings']['defaultPayment'] = $identity->getDefaultPayment();
            $formData ['settings']['defaultShipment'] = $identity->getDefaultShipment();
            $formData ['settings']['language'] = $identity->language;
            $basket = TP_Basket::getBasket();
            $basket->setPaymenttype($identity->getDefaultPayment());
            $basket->setShippingtype($identity->getDefaultShipment());
            $formData ['rech'] ['custom1'] = $identity->getCustom1();
            $formData ['rech'] ['custom2'] = $identity->getCustom2();
            $formData ['rech'] ['custom3'] = $identity->getCustom3();
            $formData ['rech'] ['custom4'] = $identity->getCustom4();
            $formData ['rech'] ['custom5'] = $identity->getCustom5();
            $formData ['rech'] ['custom6'] = $identity->getCustom6();
            $formData ['rech'] ['custom7'] = $identity->getCustom7();
            $formData ['rech'] ['custom8'] = $identity->getCustom8();
            $formData ['rech'] ['custom9'] = $identity->getCustom9();
            $formData ['rech'] ['custom10'] = $identity->getCustom10();
            $formData ['rech'] ['custom11'] = $identity->getCustom11();
            $formData ['rech'] ['custom12'] = $identity->getCustom12();
            $formData ['rech'] ['custom13'] = $identity->getCustom13();
            $formData ['rech'] ['custom14'] = $identity->getCustom14();
            $formData ['rech'] ['custom15'] = $identity->getCustom15();
            $formData ['rech'] ['custom16'] = $identity->getCustom16();
            $formData ['rech'] ['custom17'] = $identity->getCustom17();
            $formData ['rech'] ['custom18'] = $identity->getCustom18();
            $formData ['rech'] ['custom19'] = $identity->getCustom19();
            $formData ['rech'] ['custom20'] = $identity->getCustom20();
            $formData ['rech'] ['custom21'] = $identity->getCustom21();
            $formData ['rech'] ['custom22'] = $identity->getCustom22();
            $formData ['rech'] ['custom23'] = $identity->getCustom23();
            $formData ['rech'] ['custom24'] = $identity->getCustom24();
            $formData ['login'] ['self_email'] = $identity ['self_email'];

            $formData ['login'] ['newsletter'] = $identity ['newsletter'];

            $formData ['rech'] ['self_newsletter'] = $identity ['newsletter'];
            $formData ['rech'] ['newsletter'] = $identity ['newsletter'];
            $formData ['login'] ['vonwo'] = $identity ['vonwo'];

            $form->populate($formData);
        }

        $this->view->form = $form;
    }

    /**
     * Meine Aufträge
     *
     * @return void
     */
    public function myorderdetailAction() {
        if (!Zend_Auth::getInstance()->hasIdentity()) {
            $this->forwardSpeak('login', 'user', null, array('mode' => 'orders'));
        }

        $this->view->mode = 3;

        $order = Doctrine_Query::create()->from('Orders o')->where('o.uuid = ?', array($this->_getParam('uuid', false)))->fetchOne();

        $this->view->order = $order;

    }
    /**
     * Meine Aufträge
     *
     * @return void
     */
    public function myordersAction() {

        if (!Zend_Auth::getInstance()->hasIdentity()) {
            $this->forwardSpeak('login', 'user', null, array('mode' => 'orders'));
        }

        $this->view->mode = 3;
        $temp = array();
        $user = Zend_Auth::getInstance()->getIdentity();

        if($this->_request->getParam('delete', false)) {
            $order = Doctrine_Query::create()
                ->from('Orders m')
                ->where('m.contact_id = ? AND m.uuid = ?', array(intval($user ['id']), $this->_request->getParam('delete')))
                ->fetchOne();

            foreach($order->Orderspos as $pos) {
                foreach($pos->Upload as $upload) {
                    $upload->delete();
                }
                $pos->delete();
            }

            $order->delete();
        }


        $mode = new Zend_Session_Namespace('adminmode');

        if($this->user->isShowOtherOrders()) {
            if($this->user->getShowOtherOrdersAccountFilter() == 1) {
                $rows = Doctrine_Query::create()->from('Orders m');
            }elseif($this->user->getShowOtherOrdersAccountFilter() == 2) {
                $rows = Doctrine_Query::create()->from('Orders m');
                $rows->where('account_id = ?', array(intval($this->user->account_id)));
            }elseif($this->user->getShowOtherOrdersAccountFilter() == 3) {
                $rows = Doctrine_Query::create()->from('Orders m');
                $rows->where('account_id = ?', array(intval($this->user->getShowOtherOrdersAccount())));
            }
        }else{
            if ($mode->over_ride_contact != false && $mode->over_ride_contact != 'new') {
                $rows = Doctrine_Query::create()->from('Orders m');
                $rows->where('contact_id = ?', array(intval($mode->over_ride_contact)));
            } else {
                $rows = Doctrine_Query::create()->from('Orders m');
                $rows->where('contact_id = ?', array(intval($user ['id'])));
            }
        }

        require_once(APPLICATION_PATH . '/helpers/Designsettings.php');
        $designSettings = new TP_View_Helper_Designsettings();
        $displayCound = 45;
        if($designSettings->get('displayMyOrderCount')) {
            $displayCound = intval($designSettings->get('displayMyOrderCount'));
        }

        $rows->addWhere('shop_id = ?', array($this->shop->id));
        if($this->_getParam("term", false)) {
            $rows->leftJoin("m.Orderspos pos");
            $rows->leftJoin("pos.Article art");
            $rows->addWhere('(pos.ref LIKE ? OR art.title LIKE ? OR lang_data LIKE ?)', array('%'.$this->_getParam("term", false).'%', '%'.$this->_getParam("term", false).'%', '%'.$this->_getParam("term", false).'%'));
        }
        $rows->orderBy('created DESC');
        $rows->limit($displayCound);
        $rows = $rows->execute();

        $configStatus = new Zend_Config_Ini(APPLICATION_PATH . '/configs/' . '/systemdefaults.ini', 'status');

        $translateStatus = Zend_Registry::get('Zend_Translate');

        $this->view->stati = array();
        if ($this->install->production_status != "") {
            foreach (explode(",", $this->install->production_status) as $key) {
                $this->view->stati[$key] = $translateStatus->translate($key);
            }
        }

        $config = new Zend_Config_Ini(APPLICATION_PATH . '/configs/systemdefaults.ini', 'status');

        foreach ($config->toArray() as $key => $value) {
            $this->view->stati[$key] = $translateStatus->translate("status".$key);
        }

        if ($this->install['offline_custom2'] != "") {
            try {

                $maschienen = new SimpleXMLElement($this->install['offline_custom2'], null, false);

                foreach ($maschienen as $maschine) {
                    $this->view->stati[(int)$maschine['id']] = $translateStatus->translate((string)$maschine);
                }
            } catch (Exception $e) {

            }
        }


        $i = 0;

        foreach ($rows as $row) {

            if ($row->status) {
                $tempStatus = array();

                foreach ($configStatus->toArray() as $value) {
                    array_push($tempStatus, array('name' => $value, 'label' => $translateStatus->translate($value)));
                }

                //$row->status = $tempStatus[($row->status-1)]['name'];
            }

            $uploadFinish = 0;
            $uploadWork = 0;

            if($i < 11) {
                foreach ($row->Orderspos as $orderpos) {
                    if ($orderpos->uploadfinish == 1) {
                        $uploadFinish++;
                    } else {
                        $uploadWork++;
                    }
                }
            }
            $i++;
            array_push($temp, array(
                'basketfield1' => $row->basketfield1,
                'basketfield2' => $row->basketfield2,
                'versandkosten' => $row->versandkosten,
                'package' => $row->package,
                'uuid' => $row->uuid,
                'id' => $row->id,
                'account' => $row->Account->company,
                'contact' => $row->Contact->self_firstname . ' ' . $row->Contact->self_lastname,
                'email' => $row->Contact->self_email,
                'status' => $row->status,
                'alias' => $row->alias,
                'preisbrutto' => $row->preisbrutto,
                'preisnetto' => $row->preis,
                'uploadfinish' => $uploadFinish,
                'uploadwork' => $uploadWork,
                'shipping' => $row->Shippingtype,
                'payment' => $row->Paymenttype->title,
                'pos' => $row->Orderspos,
                'created' => $row->created,
                'senderAddress' => $row->getSenderAddress(),
                'invoiceAddress' => $row->getInvoiceAddress(),
                'deliveryAddress' => $row->getDeliveryAddress()));
        }

        $this->view->orders = $temp;

        $paginator = new Zend_Paginator(new Zend_Paginator_Adapter_Array($temp));

        $paginator->setItemCountPerPage(20)->setCurrentPageNumber($this->_getParam('page', 1));

        $this->view->paginator = $paginator;
    }

    /**
     * Meine Aufträge
     *
     * @return void
     */
    public function myapprovalAction() {
        if (!Zend_Auth::getInstance()->hasIdentity()) {
            $this->redirectSpeak('/user/login?mode=approvaloverview');
            return;
        }
        $this->view->mode = 10;
        $temp = array();
        $user = Zend_Auth::getInstance()->getIdentity();

        $rows = Doctrine_Query::create()->from('Orderspos p')->leftJoin('p.Article a')->leftJoin('p.OrdersposConfirmContact c')->leftJoin('p.Orders o');
        $rows->where('c.contact_id = ? AND p.status >= 90 AND p.status <= 120 AND p.status != 110 AND p.status != 120', array(intval($user ['id'])));

        $rows->orderBy('o.created DESC');

        $rows = $rows->execute();

        $configStatus = new Zend_Config_Ini(APPLICATION_PATH . '/configs/' . '/systemdefaults.ini', 'status');

        $translateStatus = Zend_Registry::get('Zend_Translate');

        foreach ($rows as $row) {

            if (!isset($temp [$row->Orders->id])) {
                $temp [$row->Orders->id] = array();
                $temp [$row->Orders->id] ['created'] = $row->Orders->created;
                $temp [$row->Orders->id] ['username'] = $row->Orders->Contact->self_email;
                $temp [$row->Orders->id] ['contact'] = $row->Orders->Contact;
                $temp [$row->Orders->id] ['alias'] = $row->Orders->alias;
                $temp [$row->Orders->id] ['payment'] = $row->Orders->Paymenttype->title;
                $temp [$row->Orders->id] ['rows'] = array();
            }

            $temp [$row->Orders->id] ['rows'] [] = $row;
        }

        $this->view->orders = $temp;
    }

    /**
     * Meine Übersicht
     *
     * @return void
     */
    public function myoverviewAction() {
        if (!Zend_Auth::getInstance()->hasIdentity()) {
            $this->forwardSpeak('login', 'user', null, array('mode' => 'myoverview'));
        }
        $rows = Doctrine_Query::create()->from('Orders m');
        $rows->where('contact_id = ?', array(intval($this->user->id)));
        $rows->orderBy('created DESC')->limit(10);

        $this->view->lastOrders = $rows->execute();

        $this->view->mode = 1;
    }

    /**
     * Nachrichtensystem des Users
     *
     * @return void
     */
    public function messageAction() {

        $this->forwardSpeak('myorders');
    }

    /**
     * Neubestellung von Aufträgen
     *
     * @return void
     */
    public function reorderAction() {

        $this->forwardSpeak('myorders');
    }

    /**
     * Defaul User Aktion
     *
     * @return void
     */
    public function indexAction() {

        if (intval($this->_request->getParam('logout')) == 1) {
            Zend_Auth::getInstance()->clearIdentity();

            $basket = new TP_Basket();
            $basket->clear();

            $mode = new Zend_Session_Namespace('adminmode');
            $mode->liveedit = false;
            $mode->over_ride_contact = false;
            $this->view->liveedit = false;
            $this->view->priorityMessenger('Logout Success', 'success');

            if($this->shop->redirect_logout != "") {
                $this->redirectSpeak($this->shop->redirect_logout);
            }

            $this->redirectSpeak('/');
        }
        $this->forwardSpeak('login', 'user', 'default');
    }

    /**
     * User werden verifiziert
     *
     * @return void
     */
    public function verifyAction() {

        
        $config = new Zend_Config_Ini($this->_configPath . '/user/verify.ini', 'verify');

        $form = new Zend_Form($config->user->verify);
        $form->addAttribs(array('class' => 'niceform'));

        if ($this->_request->getParam('mode') == 'basket') {
            $form->addElement('hidden', 'mode', array('value' => 'basket'));
        }

        if ($this->_request->isPost()) {
            $formData = $this->_request->getPost();
            if ($this->_getParam('name') != 'Benutzername' && $this->_getParam('name') != '' && $form->isValid($formData)) {

                $user = Doctrine_Query::create()->from('Contact as c')->where('c.name = ? AND c.password = ? AND c.hash = ?', array($this->_getParam('name'), $this->_getParam('password'), $this->_getParam('hash')))->fetchOne();

                if ($user != false) {
                    $user->enable = true;
                    $user->save();

                    TP_Queue::process('contactverify', 'global', $user);

                    $_authAdapter = new TP_Plugin_AuthAdapter ();
                    $_authAdapter->setIdentity($this->_getParam('name'))->setCredential($this->_getParam('password'));
                    $result = Zend_Auth::getInstance()->authenticate($_authAdapter);

                    if ($result->isValid()) {
                        if ($this->_getParam('mode') == 'basket') {
                            $this->view->priorityMessenger('Login successfull', 'success');
                            $this->forwardSpeak('index', 'basket', 'default');
                            return;
                        } else {
                            $this->forwardSpeak('index', 'index', 'default');
                        }
                    } else {
                        $form->populate($formData);
                    }
                }
                $form->populate($formData);
            } else {
                $form->populate($formData);
            }
        }

        $this->view->form = $form;

        $this->_helper->layout->setLayout('default');
    }

    /**
     * gast Funktion
     *
     * @return void
     */
    public function guestAction() {
        $config = new Zend_Config_Ini($this->_configPath . '/user/registerguest.ini', 'register');

        $vonwo = new Zend_Config_Ini(APPLICATION_PATH . '/configs/systemdefaults.ini', 'vonwo');

        $roles = new Zend_Config_Ini(APPLICATION_PATH . '/configs/roles.ini', APPLICATION_ENV);

        $countrys = TP_Country::getCountrysAsArray();

        $formData = $this->_request->getPost();

        if ($config->global) {
            $form = new EasyBib_Form($config->global);
        } else {
            $form = new Zend_Form ();
            $form->addAttribs(array('class' => 'niceform', 'id' => 'userreg'));
        }

        $form->addSubForm(new Zend_Form_SubForm($config->user->rech), 'rech');
        if (isset($form->rech->self_country)) {
            if(!$form->rech->self_country->getMultiOptions()) {
                $form->rech->self_country->addMultiOptions($countrys);
            }
            if($form->rech->self_country->getValue() == '') {
                $form->rech->self_country->setValue("DE");
            }
        }
        $form->rech->self_email->addPrefixPath('TP_Validate', 'TP/Validate/', 'Validate');

        if (isset($form->rech->cp)) {
            $form->rech->cp->setOptions(array(
                'captcha' => array(
                    'captcha' => 'Image',
                    'font' => APPLICATION_PATH . '/fonts/verdana.ttf',
                    'imgDir' => PUBLIC_PATH . '/temp/thumb/',
                    'imgUrl' => '/temp/thumb/',
                    'wordLen' => '4'
                ),
            ));
        }

        $form->addElement($config->user->submit->elements->submit->type, 'submit', $config->user->submit->elements->submit->options);

        if ($this->_request->isPost()) {
            if ($form->isValid($formData)) {
                $contact = new Contact ();
                $contact->self_email = $formData ['rech'] ['self_email'];
                if (isset($formData ['rech'] ['self_anrede'])) {
                    $contact->self_anrede = $formData ['rech'] ['self_anrede'];
                }

                $contact->self_firstname = $formData ['rech'] ['self_firstname'];
                $contact->self_lastname = $formData ['rech'] ['self_lastname'];
                if(isset($formData ['person'] ['self_anrede'])) {
                    $contact->self_anrede = $formData ['rech'] ['self_anrede'];
                }
                if(isset($formData ['person'] ['self_firstname'])) {
                    $contact->self_firstname = $formData ['rech'] ['self_firstname'];
                }
                if(isset($formData ['person'] ['self_lastname'])) {
                    $contact->self_lastname = $formData ['rech'] ['self_lastname'];
                }
                $contact->name = "guest";
                $contact->self_street = $formData ['rech'] ['self_street'];
                $contact->self_house_number = $formData ['rech'] ['self_house_number'];
                $contact->self_city = $formData ['rech'] ['self_city'];
                $contact->self_zip = $formData ['rech'] ['self_zip'];
                if (isset($formData ['rech'] ['ustid'])) {
                    $contact->ustid = $formData ['rech'] ['ustid'];
                }
                if (isset($formData ['rech'] ['self_phone'])) {
                    $contact->self_phone = $formData ['rech'] ['self_phone'];
                }
                if (isset($formData ['rech'] ['self_fax_phone'])) {
                    $contact->self_fax_phone = $formData ['rech'] ['self_fax_phone'];
                }
                if (isset($formData ['rech'] ['self_mobile_durchwahl'])) {
                    $contact->self_mobile_durchwahl = $formData ['rech'] ['self_mobile_durchwahl'];
                }
                if (isset($formData ['rech'] ['self_phone_mobile'])) {
                    $contact->self_phone_mobile = $formData ['rech'] ['self_phone_mobile'];
                }
                if (isset($formData ['rech'] ['self_department'])) {
                    $contact->self_department = $formData ['rech'] ['self_department'];
                }
                if (isset($formData ['rech'] ['self_department2'])) {
                    $contact->self_department2 = $formData ['rech'] ['self_department2'];
                }
                if (isset($formData ['rech'] ['self_abteilung'])) {
                    $contact->self_abteilung = $formData ['rech'] ['self_abteilung'];
                }
                if (isset($formData ['rech'] ['self_kostenstellung'])) {
                    $contact->self_kostenstellung = $formData ['rech'] ['self_kostenstellung'];
                }
                if (isset($formData ['rech'] ['self_birthday'])) {
                    $contact->self_birthday = $formData ['rech'] ['self_birthday'];
                }
                if (isset($formData ['rech'] ['self_phone_alternative'])) {
                    $contact->self_phone_alternative = $formData ['rech'] ['self_phone_alternative'];
                }
                if (isset($formData ['rech'] ['self_country'])) {
                    $contact->self_country = $formData ['rech'] ['self_country'];
                }

                if (isset($formData ['bank'] ['bank_iban'])) {
                    $contact->bank_iban = $formData ['bank'] ['bank_iban'];
                }
                if (isset($formData ['bank'] ['bank_bic'])) {
                    $contact->bank_bic = $formData ['bank'] ['bank_bic'];
                }
                if (isset($formData ['rech']) && isset($formData ['rech'] ['newsletter']) && $formData ['rech'] ['newsletter'] == 1) {
                    $contact->newsletter = 1;
                    $contact->self_newsletter = 1;
                } else {
                    $contact->newsletter = 0;
                    $contact->self_newsletter = 0;
                }

                if (isset($formData ['rech']) && isset($formData ['rech'] ['self_newsletter']) && $formData ['rech'] ['self_newsletter'] == 1) {
                    $contact->newsletter = 1;
                    $contact->self_newsletter = 1;
                } else {
                    $contact->newsletter = 0;
                    $contact->self_newsletter = 0;
                }

                $contact->created = date('Y-m-d');
                $contact->updated = date('Y-m-d');
                $locale = Zend_Registry::get('locale');
                $contact->language = $locale->getLanguage() . '_' . $locale->getRegion();
                $contact->hash = "test";
                $contact->enable = true;
                $contact->Install = $this->shop->Install;
                $contact->account_id = $this->shop->default_account;
                $contact->virtual = true;

                $shopcontact = new ShopContact ();
                $shopcontact->Contact = $contact;
                $shopcontact->Shop = $this->shop;
                $shopcontact->save();

                foreach ($roles->contact as $row) {
                    $rolecontact = new ContactRole ();
                    $rolecontact->Contact = $contact;
                    $rolecontact->role_id = $row;
                    $rolecontact->save();
                }

                $dbMongo = TP_Mongo::getInstance();
                $dbMongo->Job->insertOne(array(
                    "shop" =>  $this->shop->id,
                    "event" => "guest_create",
                    "data" => [ "contact" => $contact->uuid ],
                    "created" => new MongoDB\BSON\UTCDateTime(),
                    "updated" => new MongoDB\BSON\UTCDateTime()
                ));
                $_authAdapter = new TP_Plugin_AuthAdapter ();
                $_authAdapter->setIdLogin($contact->id);
                $result = Zend_Auth::getInstance()->authenticate($_authAdapter);
                $result = $result->isValid();
                if ($result) {


                    $layouterSess = new TP_Layoutersession();

                    foreach ($layouterSess->getLayouterSessionForLogin() as $item) {

                        $rowLay = new LayouterSession();
                        $rowLay->contact_id = $contact->id;
                        $rowLay->setArticleId($item->getArticleId());
                        $rowLay->setOrgArticleId($item->getOrgArticleId());

                        $rowLay->setConfigXml($item->getConfigXml());
                        $rowLay->setExtendPreviewXslFo($item->getExtendPreviewXslFo());
                        $rowLay->setPageObjects($item->getPageObjects());
                        $rowLay->setPreviewXslFo($item->getPreviewXslFo());
                        $rowLay->setPagesXml($item->getPagesXml());
                        $rowLay->setPageTemplates($item->getPageTemplates());
                        $rowLay->setXslFo($item->getXslFo());

                        $rowLay->setDesignerXML($item->getDesignerXML());
                        $rowLay->setLayouterModus($item->getLayouterModus());
                        if ($item->getTemplatePrintId() != "") {
                            $rowLay->copyFromGuest($item->getTemplatePrintId());
                        }

                        $rowLay->setTitle($item->getTitle());

                        $rowLay->save();
                    }

                    $layouterSess->clearSession();

                    if ($this->_request->getParam('mode') == 'basket') {
                        $this->redirectSpeak('/basket/review');
                    } else {
                        if ($this->shop->redirect_register != "") {
                            $this->redirectSpeak($this->shop->redirect_register);
                            return;
                        }

                        if ($this->shop->private_product) {
                            $this->redirectSpeak('/user/myoverview');
                        } else {
                            if ($this->_getParam('externalValidation', false)) {
                                $this->redirectSpeak('/?help_mode=1');
                            }
                            $this->redirectSpeak('/');
                        }
                    }
                }
            }else{
                $form->populate($formData);
            }
        }

        $this->view->form = $form;
    }

    /**
     * Contactregister Funktion
     *
     * @return void
     */
    public function registerAction() {

        if (!$this->shop->registration && ($this->_getParam('externalValidation', false) != 'YWNjb3I=')) {
            $this->view->priorityMessenger('Register not available', 'error');
            $this->redirectSpeak('/');
        }

        if(!file_exists('temp/thumb')) {
            mkdir('temp/thumb', 0777);
        }

        $config = new Zend_Config_Ini($this->_configPath . '/user/registercontact.ini', 'register');

        $vonwo = new Zend_Config_Ini(APPLICATION_PATH . '/configs/systemdefaults.ini', 'vonwo');

        $roles = new Zend_Config_Ini(APPLICATION_PATH . '/configs/roles.ini', APPLICATION_ENV);

        $countrys = TP_Country::getCountrysAsArray();

        $formData = $this->_request->getPost();

        if ($config->global) {
            $form = new EasyBib_Form($config->global);
        } else {
            $form = new Zend_Form ();
            $form->addAttribs(array('class' => 'niceform', 'id' => 'userreg'));
        }

        $subform = new Zend_Form_SubForm($config->user->login);
        if (isset($config->user->vonwo)) {
            $subform->addElement('select', 'vonwo', array('label' => 'Wie haben Sie uns gefunden?', 'multiOptions' => $vonwo->toArray()));
        }
        $form->addSubForm($subform, 'login');

        if (isset($config->user->bank)) {
            $form->addSubForm(new Zend_Form_SubForm($config->user->bank), 'bank');
        }

        if ($this->shop->useemailaslogin != '1') {
            $form->login->name->addPrefixPath('TP_Validate', 'TP/Validate/', 'Validate');
        }

        if (isset($form->login->password_re)) {
            $form->login->password_re->addPrefixPath('TP_Validate', 'TP/Validate/', 'Validate');
        }
        if (isset($form->login->self_email_re)) {
            $form->login->self_email_re->addPrefixPath('TP_Validate', 'TP/Validate/', 'Validate');
        }
        $form->login->self_email->addPrefixPath('TP_Validate', 'TP/Validate/', 'Validate');

        $form->addSubForm(new Zend_Form_SubForm($config->user->rech), 'rech');
        if (isset($form->rech->self_country)) {
            if(!$form->rech->self_country->getMultiOptions()) {
                $form->rech->self_country->addMultiOptions($countrys);
            }
            if($form->rech->self_country->getValue() == '') {
                $form->rech->self_country->setValue("DE");
            }
        }
        if (isset($formData ['rech']) && isset($formData ['rech'] ['lieferja']) && $formData ['rech'] ['lieferja'] == 1) {
            $form->addSubForm(new Zend_Form_SubForm($config->user->liefer), 'liefer');
        }

        if (isset($config->user->person)) {
            $form->addSubForm(new Zend_Form_SubForm($config->user->person), 'person');
        }

        if (isset($form->rech->cp)) {
            $form->rech->cp->setOptions(array(
                'captcha' => array(
                    'captcha' => 'Image',
                    'font' => APPLICATION_PATH . '/fonts/verdana.ttf',
                    'imgDir' => PUBLIC_PATH . '/temp/thumb/',
                    'imgUrl' => '/temp/thumb/',
                    'wordLen' => '4'
                ),
            ));
        }

        if (isset($config->user->captcha) && ($this->install->id == 7 || $this->install->id == 9 || $this->install->id == 2 || $this->shop->uid == '0001-54bb35a3-52722492-1144-4b2a493d'
                || $this->shop->uid == '0001-578b5c52-52b8666e-115d-f0ee2bd2' || $this->shop->uid == '45678uhggfrt67')) {
            $form->addSubForm(new Zend_Form_SubForm($config->user->captcha), 'captcha');
            if (isset($form->captcha->cp)) {
                $form->captcha->cp->setOptions(array(
                    'captcha' => array(
                        'captcha' => 'Image',
                        'font' => APPLICATION_PATH . '/fonts/verdana.ttf',
                        'imgDir' => PUBLIC_PATH . '/temp/thumb/',
                        'imgUrl' => '/temp/thumb/',
                        'wordLen' => '4'
                    ),
                ));
            }
        }
        $form->addElement($config->user->submit->elements->submit->type, 'submit', $config->user->submit->elements->submit->options);
        $form->addElement('hidden', 'quote', array('value' => ""));
        if ($this->_request->getParam('mode')) {
            $form->addElement('hidden', 'mode', array('value' => $this->_request->getParam('mode')));
            $form->addElement('hidden', 'uuid', array('value' => $this->_request->getParam('uuid')));
            $this->view->mode = $this->_request->getParam('mode');
            $this->view->uuid = $this->_request->getParam('uuid');
        }

        if ($this->_request->isPost()) {
            if (($this->_getParam('externalValidation', false) || (isset($_SESSION ['lieferja']) && (!isset($formData ['rech'] ['lieferja']) || (isset($formData ['rech'] ['lieferja']) && $_SESSION ['lieferja'] == $formData ['rech'] ['lieferja']))))
                && $form->isValid($formData) && ($this->_getParam('quote', false) || ($this->_getParam('quote', false) == ""))) {

                $contact = new Contact ();

                if($this->shop->getContactOwnNumber()) {

                    $templates = array('template' => $this->shop->getContactNumberPattern());
                    $twig = new \Twig\Environment(new \Twig\Loader\ArrayLoader($templates));

                    $contact->setKundenNr($twig->render('template', array('number'=> $this->shop->getContactNumberStart())));
                    $this->shop->setContactNumberStart($this->shop->getContactNumberStart()+1);
                    $this->shop->saveMongo();
                }

                if ($this->shop->useemailaslogin == true) {
                    $contact->name = $formData ['login'] ['self_email'];
                } else {
                    $contact->self_email = $formData ['login'] ['self_email'];
                }
                $contact->password = password_hash($formData ['login'] ['password'], PASSWORD_DEFAULT);
                if (isset($formData ['rech'] ['self_anrede'])) {
                    $contact->self_anrede = $formData ['rech'] ['self_anrede'];
                }

                $contact->self_firstname = $formData ['rech'] ['self_firstname'];
                $contact->self_lastname = $formData ['rech'] ['self_lastname'];
                if(isset($formData ['person'] ['self_anrede'])) {
                    $contact->self_anrede = $formData ['rech'] ['self_anrede'];
                }
                if(isset($formData ['person'] ['self_firstname'])) {
                    $contact->self_firstname = $formData ['rech'] ['self_firstname'];
                }
                if(isset($formData ['person'] ['self_lastname'])) {
                    $contact->self_lastname = $formData ['rech'] ['self_lastname'];
                }
                $contact->self_email = $formData ['login'] ['self_email'];
                $contact->self_street = $formData ['rech'] ['self_street'];
                $contact->self_house_number = $formData ['rech'] ['self_house_number'];
                $contact->self_city = $formData ['rech'] ['self_city'];
                $contact->self_zip = $formData ['rech'] ['self_zip'];
                if (isset($formData ['rech'] ['ustid'])) {
                    $contact->ustid = $formData ['rech'] ['ustid'];
                }
                if (isset($formData ['rech'] ['self_phone_lv'])) {
                    $contact->self_phone_lv = $formData ['rech'] ['self_phone_lv'];
                }
                if (isset($formData ['rech'] ['self_phone_vorwahl'])) {
                    $contact->self_phone_vorwahl = $formData ['rech'] ['self_phone_vorwahl'];
                }
                if (isset($formData ['rech'] ['self_phone'])) {
                    $contact->self_phone = $formData ['rech'] ['self_phone'];
                }
                if (isset($formData ['rech'] ['self_phone_durchwahl'])) {
                    $contact->self_phone_durchwahl = $formData ['rech'] ['self_phone_durchwahl'];
                }

                if (isset($formData ['rech'] ['self_fax_lv'])) {
                    $contact->self_fax_lv = $formData ['rech'] ['self_fax_lv'];
                }
                if (isset($formData ['rech'] ['self_fax_vorwahl'])) {
                    $contact->self_fax_vorwahl = $formData ['rech'] ['self_fax_vorwahl'];
                }
                if (isset($formData ['rech'] ['self_fax_phone'])) {
                    $contact->self_fax_phone = $formData ['rech'] ['self_fax_phone'];
                }
                if (isset($formData ['rech'] ['self_fax_durchwahl'])) {
                    $contact->self_fax_durchwahl = $formData ['rech'] ['self_fax_durchwahl'];
                }

                if (isset($formData ['rech'] ['self_mobile_lv'])) {
                    $contact->self_mobile_lv = $formData ['rech'] ['self_mobile_lv'];
                }
                if (isset($formData ['rech'] ['self_mobile_vorwahl'])) {
                    $contact->self_mobile_vorwahl = $formData ['rech'] ['self_mobile_vorwahl'];
                }
                if (isset($formData ['rech'] ['self_phone_mobile'])) {
                    $contact->self_phone_mobile = $formData ['rech'] ['self_phone_mobile'];
                }
                if (isset($formData ['rech'] ['self_mobile_durchwahl'])) {
                    $contact->self_mobile_durchwahl = $formData ['rech'] ['self_mobile_durchwahl'];
                }



                if (isset($formData ['rech'] ['self_department'])) {
                    $contact->self_department = $formData ['rech'] ['self_department'];
                }
                if (isset($formData ['rech'] ['self_department2'])) {
                    $contact->self_department2 = $formData ['rech'] ['self_department2'];
                }
                if (isset($formData ['rech'] ['self_abteilung'])) {
                    $contact->self_abteilung = $formData ['rech'] ['self_abteilung'];
                }
                if (isset($formData ['rech'] ['self_kostenstellung'])) {
                    $contact->self_kostenstellung = $formData ['rech'] ['self_kostenstellung'];
                }
                if (isset($formData ['rech'] ['self_birthday'])) {
                    $contact->self_birthday = $formData ['rech'] ['self_birthday'];
                }
                if (isset($formData ['rech'] ['self_phone_alternative'])) {
                    $contact->self_phone_alternative = $formData ['rech'] ['self_phone_alternative'];
                }
                if (isset($formData ['rech'] ['self_country'])) {
                    $contact->self_country = $formData ['rech'] ['self_country'];
                }

                if (isset($formData ['bank'] ['bank_iban'])) {
                    $contact->bank_iban = $formData ['bank'] ['bank_iban'];
                }
                if (isset($formData ['bank'] ['bank_bic'])) {
                    $contact->bank_bic = $formData ['bank'] ['bank_bic'];
                }


                $contact->vonwo = $formData ['login'] ['vonwo'];

                if (isset($formData ['login']) && isset($formData ['login'] ['newsletter']) && $formData ['login'] ['newsletter'] == 1) {
                    $contact->newsletter = 1;
                    $contact->self_newsletter = 1;
                } else {
                    $contact->newsletter = 0;
                    $contact->self_newsletter = 0;
                }

                if (isset($formData ['login']) && isset($formData ['login'] ['collecting_orders']) && $formData ['login'] ['collecting_orders'] == 1) 				{
                    $contact->collecting_orders = 1;
                } else {
                    $contact->collecting_orders = 0;
                }
                if (isset($formData ['login']) && isset($formData ['login'] ['is_sek']) && $formData ['login'] ['is_sek'] == 1) 				{
                    $contact->is_sek = 1;
                } else {
                    $contact->is_sek = 0;
                }

                if (isset($formData ['rech']) && isset($formData ['rech'] ['newsletter']) && $formData ['rech'] ['newsletter'] == 1) {
                    $contact->newsletter = 1;
                    $contact->self_newsletter = 1;
                } else {
                    $contact->newsletter = 0;
                    $contact->self_newsletter = 0;
                }

                if (isset($formData ['rech']) && isset($formData ['rech'] ['self_newsletter']) && $formData ['rech'] ['self_newsletter'] == 1) {
                    $contact->newsletter = 1;
                    $contact->self_newsletter = 1;
                } else {
                    $contact->newsletter = 0;
                    $contact->self_newsletter = 0;
                }

                if (isset($formData ['rech']) && $formData ['rech'] ['lieferja'] == 1) {

                    $contact->liefer = 1;
                    $contact->liefer_firstname = $formData ['liefer'] ['self_firstname'];
                    $contact->liefer_lastname = $formData ['liefer'] ['self_lastname'];
                    $contact->liefer_street = $formData ['liefer'] ['self_street'];
                    $contact->liefer_house_number = $formData ['liefer'] ['self_house_number'];
                    $contact->liefer_city = $formData ['liefer'] ['self_city'];
                    $contact->liefer_zip = $formData ['liefer'] ['self_zip'];
                    $contact->liefer_phone = $formData ['liefer'] ['self_phone'];
                    $contact->department = $formData ['liefer'] ['self_department'];

                } else {

                    $contact->liefer = 0;
                }

                $Buchstaben = array("a", "b", "c", "d", "e", "f", "g", "h", "k", "m", "n", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z");
                $Zahlen = array("2", "3", "4", "5", "6", "7", "8", "9");

                $Laenge = 6;


                for ($i = 0, $Passwort = ""; strlen($Passwort) < $Laenge; $i++) {
                    if (rand(0, 2) == 0 && isset($Buchstaben)) {
                        $Passwort .= $Buchstaben [rand(0, count($Buchstaben))];
                    } elseif (rand(0, 2) == 1 && isset($Zahlen)) {
                        if (isset($Zahlen [rand(0, count($Zahlen))])) {
                            $Passwort .= $Zahlen [rand(0, count($Zahlen))];
                        }
                    }
                }

                $contact->created = date('Y-m-d');
                $contact->updated = date('Y-m-d');
                $locale = Zend_Registry::get('locale');
                $contact->language = $locale->getLanguage() . '_' . $locale->getRegion();
                $contact->hash = $Passwort;
                $contact->enable = false;
                $contact->Install = $this->shop->Install;
                $contact->account_id = $this->shop->default_account;

                if($this->_getParam('hn', false)) {
                    $account = Doctrine_Query::create()->from('Account a')->where('a.company = ? OR a.company = ?', array(strtoupper($this->_getParam('hn', false)), "H".strtoupper($this->_getParam('hn', false))))->fetchOne();
                    if($account) {
                        $contact->account_id = $account->id;
                    }
                }

                $contact->save();
                if (isset($formData ['rech'] ['custom1'])) {
                    $contact->setCustom1($formData ['rech'] ['custom1']);
                }
                if (isset($formData ['rech'] ['custom2'])) {
                    $contact->setCustom2($formData ['rech'] ['custom2']);
                }
                if (isset($formData ['rech'] ['custom3'])) {
                    $contact->setCustom3($formData ['rech'] ['custom3']);
                }
                if (isset($formData ['rech'] ['custom4'])) {
                    $contact->setCustom4($formData ['rech'] ['custom4']);
                }
                if (isset($formData ['rech'] ['custom5'])) {
                    $contact->setCustom4($formData ['rech'] ['custom5']);
                }
                if (isset($formData ['rech'] ['custom6'])) {
                    $contact->setCustom4($formData ['rech'] ['custom6']);
                }
                if (isset($formData ['rech'] ['custom7'])) {
                    $contact->setCustom4($formData ['rech'] ['custom7']);
                }
                if (isset($formData ['rech'] ['custom8'])) {
                    $contact->setCustom4($formData ['rech'] ['custom8']);
                }
                if (isset($formData ['rech'] ['custom9'])) {
                    $contact->setCustom4($formData ['rech'] ['custom9']);
                }
                if (isset($formData ['rech'] ['custom10'])) {
                    $contact->setCustom4($formData ['rech'] ['custom10']);
                }
                if (isset($formData ['rech'] ['custom11'])) {
                    $contact->setCustom4($formData ['rech'] ['custom11']);
                }
                if (isset($formData ['rech'] ['custom12'])) {
                    $contact->setCustom4($formData ['rech'] ['custom12']);
                }
                if (isset($formData ['rech'] ['custom13'])) {
                    $contact->setCustom4($formData ['rech'] ['custom13']);
                }
                if (isset($formData ['rech'] ['custom14'])) {
                    $contact->setCustom4($formData ['rech'] ['custom14']);
                }
                if (isset($formData ['rech'] ['custom15'])) {
                    $contact->setCustom4($formData ['rech'] ['custom15']);
                }
                if (isset($formData ['rech'] ['custom16'])) {
                    $contact->setCustom4($formData ['rech'] ['custom16']);
                }
                if (isset($formData ['rech'] ['custom17'])) {
                    $contact->setCustom4($formData ['rech'] ['custom17']);
                }
                if (isset($formData ['rech'] ['custom18'])) {
                    $contact->setCustom4($formData ['rech'] ['custom18']);
                }
                if (isset($formData ['rech'] ['custom19'])) {
                    $contact->setCustom4($formData ['rech'] ['custom19']);
                }
                if (isset($formData ['rech'] ['custom20'])) {
                    $contact->setCustom4($formData ['rech'] ['custom20']);
                }
                if (isset($formData ['rech'] ['custom21'])) {
                    $contact->setCustom4($formData ['rech'] ['custom21']);
                }
                if (isset($formData ['rech'] ['custom22'])) {
                    $contact->setCustom4($formData ['rech'] ['custom22']);
                }
                if (isset($formData ['rech'] ['custom23'])) {
                    $contact->setCustom4($formData ['rech'] ['custom23']);
                }
                if (isset($formData ['rech'] ['custom24'])) {
                    $contact->setCustom4($formData ['rech'] ['custom24']);
                }
 
                $contact->saveMongo();

                $contactaddress = new ContactAddress ();
                $contactaddress->anrede = $contact->self_anrede;
                $contactaddress->firstname = $contact->self_firstname;
                $contactaddress->lastname = $contact->self_lastname;
                $contactaddress->zip = $contact->self_zip;
                $contactaddress->city = $contact->self_city;
                $contactaddress->house_number = $contact->self_house_number;
                $contactaddress->street = $contact->self_street;
                $contactaddress->email = $contact->self_email;
                $contactaddress->phone = $contact->self_phone;
                if (isset($formData ['rech'] ['self_mobile_durchwahl'])) {
                    $contactaddress->mobil_phone = $formData ['rech'] ['self_mobile_durchwahl'];
                }
                if (isset($formData ['rech'] ['self_department'])) {
                    $contactaddress->company = $formData ['rech'] ['self_department'];
                }
                if (isset($formData ['rech'] ['self_department2'])) {
                    $contactaddress->company2 = $formData ['rech'] ['self_department2'];
                }
                if (isset($formData ['rech'] ['self_abteilung'])) {
                    $contactaddress->abteilung = $formData ['rech'] ['self_abteilung'];
                }
                if (isset($formData ['rech'] ['self_kostenstellung'])) {
                    $contactaddress->kostenstellung = $formData ['rech'] ['self_kostenstellung'];
                }
                $contactaddress->country = $contact->self_country;
                if (isset($formData ['rech'] ['self_fax_phone'])) {
                    $contactaddress->fax = $formData ['rech'] ['self_fax_phone'];
                }
                if (isset($formData ['rech'] ['self_phone_alternative'])) {
                    $contactaddress->fax = $formData ['rech'] ['self_phone_alternative'];
                }
                $contactaddress->contact_id = $contact->id;
                $contactaddress->install_id = $contact->install_id;
                $contactaddress->display = true;
                $contactaddress->type = 1;

                $contactaddress->save();
                $contactaddress->setKundenNr($contact->getKundenNr());
                $contactaddress->saveMongo();

                $shopcontact = new ShopContact ();
                $shopcontact->Contact = $contact;
                $shopcontact->Shop = $this->shop;
                $shopcontact->save();

                foreach ($roles->contact as $row) {
                    $rolecontact = new ContactRole ();
                    $rolecontact->Contact = $contact;
                    $rolecontact->role_id = $row;
                    $rolecontact->save();
                }

                TP_Queue::process('contactregister', 'global', $contact);

                $dbMongo = TP_Mongo::getInstance();
                $dbMongo->Job->insertOne(array(
                    "shop" =>  $this->shop->id,
                    "event" => "contact_create",
                    "data" => [ "contact" => $contact->uuid ],
                    "created" => new MongoDB\BSON\UTCDateTime(),
                    "updated" => new MongoDB\BSON\UTCDateTime()
                ));

                if ($this->shop->noverify == true) {
                    $contact->enable = true;
                    $contact->save();

                    $_authAdapter = new TP_Plugin_AuthAdapter ();
                    $mode = new Zend_Session_Namespace('adminmode');
                    if ($mode->over_ride_contact == 'new') {
                        $mode->over_ride_contact = $contact->id;
                        $result = true;
                    } else {
                        if ($this->shop->useemailaslogin == true) {
                            $_authAdapter->setIdentity($contact->self_email)->setCredential($formData ['login'] ['password']);
                        } else {
                            $_authAdapter->setIdentity($contact->name)->setCredential($formData ['login'] ['password']);
                        }
                        $result = Zend_Auth::getInstance()->authenticate($_authAdapter);
                        $result = $result->isValid();
                    }
                    if ($result) {

                        $mode = new Zend_Session_Namespace('adminmode');
                        if ($mode->over_ride_contact != false && $mode->over_ride_contact != 'new') {
                            $contact = Doctrine_Query::create()->from('Contact as c')->where('c.id = ?', array($mode->over_ride_contact))->fetchOne();
                        } else {
                            $contact = Zend_Auth::getInstance()->getIdentity();
                            $contact = Doctrine_Query::create()->from('Contact as c')->where('c.id = ?', array($contact ['id']))->fetchOne();
                        }

                        $stepuploads = new TP_Uploadsession();
                        $stepuploads->copyToDatabase();

                        $layouterSess = new TP_Layoutersession();

                        foreach ($layouterSess->getLayouterSessionForLogin() as $item) {

                            $rowLay = new LayouterSession();
                            $rowLay->contact_id = $contact->id;
                            $rowLay->setArticleId($item->getArticleId());
                            $rowLay->setOrgArticleId($item->getOrgArticleId());

                            $rowLay->setConfigXml($item->getConfigXml());
                            $rowLay->setExtendPreviewXslFo($item->getExtendPreviewXslFo());
                            $rowLay->setPageObjects($item->getPageObjects());
                            $rowLay->setPreviewXslFo($item->getPreviewXslFo());
                            $rowLay->setPagesXml($item->getPagesXml());
                            $rowLay->setPageTemplates($item->getPageTemplates());
                            $rowLay->setXslFo($item->getXslFo());

                            $rowLay->setDesignerXML($item->getDesignerXML());
                            $rowLay->setLayouterModus($item->getLayouterModus());
                            if ($item->getTemplatePrintId() != "") {
                                $rowLay->copyFromGuest($item->getTemplatePrintId());
                            }

                            $rowLay->setTitle($item->getTitle());

                            $rowLay->save();
                        }

                        $layouterSess->clearSession();

                        $images = new Zend_Session_Namespace('amfmotive');

                        $images->unlock();

                        if (!$images->motive) {
                            $images->motive = array();
                        }

                        $guestMotive = $images->motive;

                        if ($this->shop->market) {
                            $shopMarket = Doctrine_Query::create()->from('Shop c')->where('c.id = ?', array($this->Install->defaultmarket))->fetchOne();
                        } else {
                            $shopMarket = $this->shop;
                        }

                        foreach ($guestMotive as $motivg) {

                            if (!file_exists(APPLICATION_PATH . '/../market/motive/' . $contact->uuid)) {
                                mkdir(APPLICATION_PATH . '/../market/motive/' . $contact->uuid);
                            }

                            copy(APPLICATION_PATH . '/../market/guest/' . $motivg ['file_orginal'], APPLICATION_PATH . '/../market/motive/' . $contact->uuid . '/' . $motivg ['org']);
                            copy(APPLICATION_PATH . '/../market/guest/' . $motivg ['file_work'], APPLICATION_PATH . '/../market/motive/' . $contact->uuid . '/work_' . $motivg ['org']);
                            copy(APPLICATION_PATH . '/../market/guest/' . $motivg ['file_mid'], APPLICATION_PATH . '/../market/motive/' . $contact->uuid . '/mid_' . $motivg ['org']);
                            copy(APPLICATION_PATH . '/../market/guest/' . $motivg ['file_thumb'], APPLICATION_PATH . '/../market/motive/' . $contact->uuid . '/thumb_' . $motivg ['org']);

                            $motive = new Motiv ();
                            $motive->uuid = $motivg ['uuid'];
                            $motive->account_id = $contact->account_id;
                            $motive->shop_id = $this->shop->id;
                            $motive->contact_id = $contact->id;
                            //$motive->deleted = false;
                            $motive->install_id = $contact->Install->id;
                            $motive->title = $motivg ['title'];
                            $motive->dpi_org = $motivg ['dpi_org'];
                            $motive->dpi_con = $motivg ['dpi_con'];
                            $motive->orgfilename = $motivg ['orgfilename'];
                            $motive->file_orginal = $contact->uuid . '/' . $motivg ['org'];
                            $motive->file_work = $contact->uuid . '/work_' . $motivg ['org'];
                            $motive->file_mid = $contact->uuid . '/mid_' . $motivg ['org'];
                            $motive->file_thumb = $contact->uuid . '/thumb_' . $motivg ['org'];

                            $motive->status = $shopMarket['default_motiv_status'];
                            $motive->width = $motivg ['width'];
                            $motive->height = $motivg ['height'];
                            $motive->mid_width = $motivg ['mid_width'];
                            $motive->mid_height = $motivg ['mid_height'];
                            $motive->typ = $motivg ['typ'];
                            $motive->copyright = $motivg ['copyright'];
                            $motive->save();
                        }

                        $images->motive = array();

                        $motivBasket = new TP_FavMotiv ();

                        foreach ($motivBasket->getMotive() as $key => $row) {
                            if ($key == 0) {
                                continue;
                            }

                            $count = Doctrine_Query::create()->from('ContactMotiv c')->where('c.motiv_id = ?', array($key))->count();

                            if ($count > 0) {
                                continue;
                            }

                            $count = Doctrine_Query::create()->from('Motiv c')->where('c.id = ?', array($key))->count();

                            if ($count > 0) {
                                $contactmotiv = new ContactMotiv ();
                                $contactmotiv->contact_id = $contact->id;
                                $contactmotiv->motiv_id = $key;
                                $contactmotiv->save();
                            }
                        }

                        foreach ($motivBasket->getMotiveUUID() as $key => $row) {
                            if ($key == 0) {
                                continue;
                            }

                            $motiv = Doctrine_Query::create()->from('Motiv c')->where('c.uuid = ?', array($key))->fetchOne();
                            if ($count > 0) {
                                continue;
                            }

                            $count = Doctrine_Query::create()->from('ContactMotiv c')->where('c.motiv_id = ?', array($motiv->id))->count();

                            if ($count > 0) {
                                continue;
                            }

                            $contactmotiv = new ContactMotiv ();
                            $contactmotiv->contact_id = $contact->id;
                            $contactmotiv->motiv_id = $motiv->id;
                            $contactmotiv->save();
                        }

                        $motivBasket->clearSession();

                        if ($this->_getParam('mode') == 'resaleshop' || $this->_getParam('mode') == 'resalearticle') {
                            $this->view->priorityMessenger('Login successfull', 'success');

                            /*
                              $resale = new TP_ResaleWizard();
                              $layouterid = $resale->getLayouterId();
                              $articleid = $resale->getArticleId();

                              $article = Doctrine_Query::create()
                              ->from('Article c')
                              ->where('c.enable = 1 AND c.uuid = ?', array($articleid))
                              ->fetchOne();

                              if($article == false) {
                              throw new Zend_Amf_Exception;
                              }

                              $uuid = $article->uuid;
                              if($article->a6_org_article != 0) {
                              $orgid = $article->a6_org_article;
                              }else{
                              $orgid = $article->id;
                              }
                              $article = $article->copy();
                              $article->uuid = "";
                              $article->a6_org_article = $orgid;
                              $article->contact_id = $contact->id;
                              $article->private = true;
                              $article->save();

                              $rel = new ContactArticle();
                              $rel->article_id = $article->id;
                              $rel->contact_id = $contact->id;
                              $rel->save();

                              $articles = Zend_Registry::get('articles');

                              $articleObj = new $articles[$article->typ]();

                              if(method_exists($articleObj, 'copyPreDispatch')) $articleObj->copyPreDispatch($article, $layouterid);
                             */
                            $this->view->priorityMessenger('Register successfull', 'success');

                            if ($this->_getParam('mode') == 'resaleshop') {
                                $this->redirectSpeak('/resale/shopstep0/' . $article->uuid);
                            }
                            if ($this->_getParam('mode') == 'resalearticle') {
                                $this->redirectSpeak('/resale/articlestep1/' . $article->uuid);
                            }

                            $this->redirectSpeak('/resale/index');
                            return;
                        }

                        if ($this->_getParam('mode') == 'approval') {
                            $this->view->priorityMessenger('Login successfull', 'success');

                            $this->redirectSpeak('/article/approval/' . $this->_getParam('uuid'));
                            return;
                        }
                        if ($this->_getParam('mode') == 'product_show') {
                            $this->view->priorityMessenger('Login successfull', 'success');

                            $this->redirectSpeak($this->_getParam('uuid'));
                            return;
                        }

                        if ($this->_getParam('mode') == 'motiv_upload') {
                            $this->view->priorityMessenger('Login successfull', 'success');

                            $this->redirectSpeak('/resale/motivstep1');
                            return;
                        }

                        if ($this->_getParam('mode') == 'anfrage') {
                            $this->view->priorityMessenger('Login successfull', 'success');

                            $this->redirectSpeak('/basket/anfrage');
                            return;
                        }

                        if ($this->_request->getParam('mode') == 'basket') {
                            $this->view->priorityMessenger('Register successfull', 'success');
                            $this->redirectSpeak('/basket/review');
                        } else {
                            $this->view->priorityMessenger('Register successfull', 'success');
                            if($this->shop->redirect_register != "") {
                                $this->redirectSpeak($this->shop->redirect_register);
                                return;
                            }

                            if ($this->shop->private_product) {
                                $this->redirectSpeak('/user/myoverview');
                            } else {
                                if ($this->_getParam('externalValidation', false)) {
                                    $this->redirectSpeak('/?help_mode=1');
                                }
                                $this->redirectSpeak('/');
                            }
                        }
                    }
                } else {
                    $dbMongo = TP_Mongo::getInstance();
                    $dbMongo->Job->insertOne(array(
                        "shop" => $this->shop->id,
                        "event" => "contact_password_reset_start",
                        "data" => ["contact" => $contact->uuid],
                        "created" => new MongoDB\BSON\UTCDateTime(),
                        "updated" => new MongoDB\BSON\UTCDateTime()
                    ));

                    $this->view->priorityMessenger('Resetmail successfull', 'success');
                    $this->forwardSpeak('verify');
                    $this->view->finish = true;
                }
            } else {

                $form->populate($formData);
            }
            if (isset($formData ['rech'] ['lieferja'])) {
                $_SESSION ['lieferja'] = $formData ['rech'] ['lieferja'];
            }
        } else {
            $_SESSION ['lieferja'] = 0;
        }

        if (!isset($_SESSION ['lieferja'])) {
            $_SESSION ['lieferja'] = 0;
        }

        $this->view->form = $form;

        if ($this->shop->private && !Zend_Auth::getInstance()->hasIdentity()) {
            $this->_helper->layout->setLayout('private');
        } else {
            $this->_helper->layout->setLayout('default');
        }
    }

    public function xmlloginAction() {

        if ($this->_request->getParam('contact', false)) {
            if('nsWXSoLmx8TNEjdE8fbn' != $this->_request->getParam('apikey')) {
                die("Not allowed");
            }

            $_authAdapter = new TP_Plugin_AuthAdapter (); // put this in a constructor?
            $_authAdapter->setApiLogin(true);
            $_authAdapter->setIdentity($this->_getParam('contact', false));
            $result = Zend_Auth::getInstance()->authenticate($_authAdapter);

            if ($result->isValid()) {
                return $this->redirectSpeak('/');
            }

            var_dump($result->getMessages());
            die();
        }

        if ($this->_request->isPost()) {
            $formData = $this->_request->getPost();
            $xml = simplexml_load_string(file_get_contents("php://input"));
            $basepath = 'http://' . $_SERVER["SERVER_NAME"];

            if ((isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on')) {
                $basepath = 'https://' . $_SERVER["SERVER_NAME"];
            }

            $contact = Doctrine_Query::create()->from('Contact as c')->where('c.self_email = ? AND c.enable = 1 AND c.install_id = ?', array((string)$xml->Request->PunchOutSetupRequest->Extrinsic[1], $this->install->id))->fetchOne();

            if($contact) {
                echo '<?xml version="1.0"?>' .
                    '<!DOCTYPE cXML SYSTEM "http://xml.cxml.org/schemas/cXML/1.1.007/cXML.dtd">' .
                    '<cXML xml:lang="en" payloadID="7213656@Supplier.com" timestamp="2002-01-01T08:46:00-07:00">' .
                    '<Response>' .
                    '<Status code="200" text="success"/>' .
                    '<PunchOutSetupResponse>' .
                    '<StartPage>' .
                    '<URL>'.$basepath.'/user/xmllogin?contact='.$contact->id.'&apikey=nsWXSoLmx8TNEjdE8fbn</URL>' .
                    '</StartPage>' .
                    '</PunchOutSetupResponse>' .
                    '</Response>' .
                    '</cXML>';
            }else{
                echo "failure";
            }
        }

        die();
    }

    public function urlloginAction()
    {
        if ($this->_request->getParam('username', false) && $this->_request->getParam('hash', false)) {
            $formData = $this->_request->getPost();
            $_authAdapter = new TP_Plugin_AuthAdapter (); // put this in a constructor?
            $_authAdapter->setIdentity($this->_getParam('username'))->setCredential($this->_getParam('hash'));
            $result = Zend_Auth::getInstance()->authenticate($_authAdapter);

            if ($result->isValid()) {

                $this->view->priorityMessenger('Login successfull', 'success');

                if ($this->shop->redirect_login != "") {
                    return $this->redirectSpeak($this->shop->redirect_login);
                }

                return $this->redirect("/");
            }
        }
    }

    public function jsonloginAction() {
        if ($this->_request->isPost()) {
            $formData = $this->_request->getPost();
            $_authAdapter = new TP_Plugin_AuthAdapter (); // put this in a constructor?
            $_authAdapter->setIdentity($this->_getParam('name'))->setCredential($this->_getParam('password'));
            $result = Zend_Auth::getInstance()->authenticate($_authAdapter);

            if ($result->isValid()) {

                $contact = $result->getIdentity();
                $contact = Doctrine_Query::create()->from('Contact as c')->where('c.id = ?', array($contact ['id']))->fetchOne();

                TP_Queue::process('contactlogin', 'global', $contact);

                $layouterSess = new TP_Layoutersession();

                foreach ($layouterSess->getLayouterSessionForLogin() as $item) {

                    $rowLay = new LayouterSession();
                    $rowLay->contact_id = $contact->id;
                    $rowLay->setArticleId($item->getArticleId());
                    $rowLay->setOrgArticleId($item->getOrgArticleId());

                    $rowLay->setConfigXml($item->getConfigXml());
                    $rowLay->setExtendPreviewXslFo($item->getExtendPreviewXslFo());
                    $rowLay->setPageObjects($item->getPageObjects());
                    $rowLay->setPreviewXslFo($item->getPreviewXslFo());
                    $rowLay->setPagesXml($item->getPagesXml());
                    $rowLay->setPageTemplates($item->getPageTemplates());
                    $rowLay->setXslFo($item->getXslFo());

                    $rowLay->setDesignerXML($item->getDesignerXML());
                    $rowLay->setLayouterModus($item->getLayouterModus());
                    if ($item->getTemplatePrintId() != "") {
                        $rowLay->copyFromGuest($item->getTemplatePrintId());
                    }

                    $rowLay->setTitle($item->getTitle());

                    $rowLay->save();
                }

                $layouterSess->clearSession();

                $images = new Zend_Session_Namespace('amfmotive');

                $images->unlock();

                if (!$images->motive) {
                    $images->motive = array();
                }

                $guestMotive = $images->motive;

                if ($this->shop->market) {
                    $shopMarket = Doctrine_Query::create()->from('Shop c')->where('c.id = ?', array($this->Install->defaultmarket))->fetchOne();
                } else {
                    $shopMarket = $this->shop;
                }

                foreach ($guestMotive as $motivg) {

                    if (!file_exists(APPLICATION_PATH . '/../market/motive/' . $contact->uuid)) {
                        mkdir(APPLICATION_PATH . '/../market/motive/' . $contact->uuid);
                    }

                    copy(APPLICATION_PATH . '/../market/guest/' . $motivg ['file_orginal'], APPLICATION_PATH . '/../market/motive/' . $contact->uuid . '/' . $motivg ['org']);
                    copy(APPLICATION_PATH . '/../market/guest/' . $motivg ['file_work'], APPLICATION_PATH . '/../market/motive/' . $contact->uuid . '/work_' . $motivg ['org']);
                    copy(APPLICATION_PATH . '/../market/guest/' . $motivg ['file_mid'], APPLICATION_PATH . '/../market/motive/' . $contact->uuid . '/mid_' . $motivg ['org']);
                    copy(APPLICATION_PATH . '/../market/guest/' . $motivg ['file_thumb'], APPLICATION_PATH . '/../market/motive/' . $contact->uuid . '/thumb_' . $motivg ['org']);

                    $motive = new Motiv ();
                    $motive->uuid = $motivg ['uuid'];
                    $motive->account_id = $contact->account_id;
                    $motive->shop_id = $this->shop->id;
                    $motive->contact_id = $contact->id;
                    //$motive->deleted = false;
                    $motive->install_id = $contact->Install->id;
                    $motive->title = $motivg ['title'];
                    $motive->dpi_org = $motivg ['dpi_org'];
                    $motive->dpi_con = $motivg ['dpi_con'];
                    $motive->orgfilename = $motivg ['orgfilename'];
                    $motive->file_orginal = $contact->uuid . '/' . $motivg ['org'];
                    $motive->file_work = $contact->uuid . '/work_' . $motivg ['org'];
                    $motive->file_mid = $contact->uuid . '/mid_' . $motivg ['org'];
                    $motive->file_thumb = $contact->uuid . '/thumb_' . $motivg ['org'];

                    $motive->status = $shopMarket['default_motiv_status'];
                    $motive->width = $motivg ['width'];
                    $motive->height = $motivg ['height'];
                    $motive->mid_width = $motivg ['mid_width'];
                    $motive->mid_height = $motivg ['mid_height'];
                    $motive->typ = $motivg ['typ'];
                    $motive->copyright = $motivg ['copyright'];
                    $motive->save();
                }

                $images->motive = array();

                $motivBasket = new TP_FavMotiv ();

                foreach ($motivBasket->getMotive() as $key => $row) {
                    if ($key == 0) {
                        continue;
                    }

                    $count = Doctrine_Query::create()->from('ContactMotiv c')->where('c.motiv_id = ?', array($key))->count();

                    if ($count > 0) {
                        continue;
                    }

                    $count = Doctrine_Query::create()->from('Motiv c')->where('c.id = ?', array($key))->count();

                    if ($count > 0) {
                        $contactmotiv = new ContactMotiv ();
                        $contactmotiv->contact_id = $contact->id;
                        $contactmotiv->motiv_id = $key;
                        $contactmotiv->save();
                    }
                }

                foreach ($motivBasket->getMotiveUUID() as $key => $row) {
                    if ($key == 0) {
                        continue;
                    }

                    $motiv = Doctrine_Query::create()->from('Motiv c')->where('c.uuid = ?', array($key))->fetchOne();
                    if ($count > 0) {
                        continue;
                    }

                    $count = Doctrine_Query::create()->from('ContactMotiv c')->where('c.motiv_id = ?', array($motiv->id))->count();

                    if ($count > 0) {
                        continue;
                    }

                    $contactmotiv = new ContactMotiv ();
                    $contactmotiv->contact_id = $contact->id;
                    $contactmotiv->motiv_id = $motiv->id;
                    $contactmotiv->save();
                }

                $motivBasket->clearSession();
                $this->view->success = true;
                $this->view->code = 0;
                $this->view->message = 'Login successfull';

                $this->view->priorityMessenger('Login successfull', 'success');

            } else {
                $this->view->success = false;
                $this->view->code = 200;
                $this->view->message = 'Username or Password wrong';
            }
        }
    }

    /**
     * Loginaktion
     *
     * @return void
     */
    public function loginAction() {

        $config = new Zend_Config_Ini($this->_configPath . '/user/login.ini', 'login');
        if ($config->global) {
            $form = new EasyBib_Form($config->user->login);
            $form->addAttribs(array('class' => $config->global->class, 'id' => 'login'));
        } else {
            $form = new Zend_Form($config->user->login);
            $form->addAttribs(array('class' => 'niceform', 'id' => 'login'));

        }

        if (Zend_Auth::getInstance()->hasIdentity()) {
            return $this->redirectSpeak("/");
        }
        
        if ($this->_request->getParam('mode')) {
            $form->addElement('hidden', 'mode', array('value' => $this->_getParam('mode')));
            $form->mode->setValue($this->_getParam('mode'));
            $form->addElement('hidden', 'uuid', array('value' => $this->_getParam('uuid')));
            $form->uuid->setValue($this->_getParam('uuid'));
            $this->view->mode = $this->_getParam('mode');
            $this->view->uuid = $this->_getParam('uuid');
        }

        if($this->_request->getParam('HOOK_URL', false)) {
            $sap = new Zend_Session_Namespace('sap');
            $sap->unlock();

            if (!$sap->return_url) {
                $sap->return_url = $this->_request->getParam('HOOK_URL', false);
            }

            $sap->lock();
        }

        if ($this->_request->isPost()) {
            $formData = $this->_request->getPost();
            if ($form->isValid($formData) && $this->_getParam('name') != 'Benutzername') {
                $_authAdapter = new TP_Plugin_AuthAdapter (); // put this in a constructor?
                $_authAdapter->setIdentity($this->_getParam('name'))->setCredential($this->_getParam('password'));
                $result = Zend_Auth::getInstance()->authenticate($_authAdapter);
                if ($result->isValid()) {
                    $contact = $result->getIdentity();
                    $contact = Doctrine_Query::create()->from('Contact as c')->where('c.id = ?', array($contact ['id']))->fetchOne();

                    TP_Queue::process('contactlogin', 'global', $contact);
                    
                    $basket = TP_Basket::getBasket();
                    if($contact->getDefaultPayment() !== null) {
                        $paymentType = Doctrine_Query::create()->from('Paymenttype as c')->leftJoin('c.Shop as s')->where('c.id = ? and s.id = ?', array((int)$contact->getDefaultPayment(), $this->shop->id))->fetchOne();
                        if($paymentType) {
                            $basket->setPaymenttype((int)$contact->getDefaultPayment());
                        }
                    }
                    if($contact->getDefaultShipment() !== null) {
                        $shippingType = Doctrine_Query::create()->from('Shippingtype as c')->leftJoin('c.Shop as s')->where('c.id = ? and s.id = ?', array((int)$contact->getDefaultShipment(), $this->shop->id))->fetchOne();
                        if($shippingType) {
                            $basket->setShippingtype((int)$contact->getDefaultShipment());
                        }
                    }
                    $stepuploads = new TP_Uploadsession();
                    $stepuploads->copyToDatabase();

                    $layouterSess = new TP_Layoutersession();

                    foreach ($layouterSess->getLayouterSessionForLogin() as $item) {

                        $rowLay = new LayouterSession();
                        $rowLay->contact_id = $contact->id;
                        $rowLay->setArticleId($item->getArticleId());
                        $rowLay->setOrgArticleId($item->getOrgArticleId());

                        $rowLay->setConfigXml($item->getConfigXml());
                        $rowLay->setExtendPreviewXslFo($item->getExtendPreviewXslFo());
                        $rowLay->setPageObjects($item->getPageObjects());
                        $rowLay->setPreviewXslFo($item->getPreviewXslFo());
                        $rowLay->setPagesXml($item->getPagesXml());
                        $rowLay->setPageTemplates($item->getPageTemplates());
                        $rowLay->setXslFo($item->getXslFo());
                        $rowLay->setDesignerXML($item->getDesignerXML());
                        $rowLay->setLayouterModus($item->getLayouterModus());

                        if ($item->getTemplatePrintId() != "") {
                            $rowLay->copyFromGuest($item->getTemplatePrintId());
                        }

                        $rowLay->setTitle($item->getTitle());

                        $rowLay->save();
                    }

                    $layouterSess->clearSession();

                    $images = new Zend_Session_Namespace('amfmotive');

                    $images->unlock();

                    if (!$images->motive) {
                        $images->motive = array();
                    }

                    $guestMotive = $images->motive;

                    if ($this->shop->market) {
                        $shopMarket = Doctrine_Query::create()->from('Shop c')->where('c.id = ?', array($this->Install->defaultmarket))->fetchOne();
                    } else {
                        $shopMarket = $this->shop;
                    }

                    foreach ($guestMotive as $motivg) {

                        if (!file_exists(APPLICATION_PATH . '/../market/motive/' . $contact->uuid)) {
                            mkdir(APPLICATION_PATH . '/../market/motive/' . $contact->uuid);
                        }

                        copy(APPLICATION_PATH . '/../market/guest/' . $motivg ['file_orginal'], APPLICATION_PATH . '/../market/motive/' . $contact->uuid . '/' . $motivg ['org']);
                        copy(APPLICATION_PATH . '/../market/guest/' . $motivg ['file_work'], APPLICATION_PATH . '/../market/motive/' . $contact->uuid . '/work_' . $motivg ['org']);
                        copy(APPLICATION_PATH . '/../market/guest/' . $motivg ['file_mid'], APPLICATION_PATH . '/../market/motive/' . $contact->uuid . '/mid_' . $motivg ['org']);
                        copy(APPLICATION_PATH . '/../market/guest/' . $motivg ['file_thumb'], APPLICATION_PATH . '/../market/motive/' . $contact->uuid . '/thumb_' . $motivg ['org']);

                        $motive = new Motiv ();
                        $motive->uuid = $motivg ['uuid'];
                        $motive->account_id = $contact->account_id;
                        $motive->shop_id = $this->shop->id;
                        $motive->contact_id = $contact->id;
                        //$motive->deleted = false;
                        $motive->install_id = $contact->Install->id;
                        $motive->title = $motivg ['title'];
                        $motive->dpi_org = $motivg ['dpi_org'];
                        $motive->dpi_con = $motivg ['dpi_con'];
                        $motive->orgfilename = $motivg ['orgfilename'];
                        $motive->file_orginal = $contact->uuid . '/' . $motivg ['org'];
                        $motive->file_work = $contact->uuid . '/work_' . $motivg ['org'];
                        $motive->file_mid = $contact->uuid . '/mid_' . $motivg ['org'];
                        $motive->file_thumb = $contact->uuid . '/thumb_' . $motivg ['org'];

                        $motive->status = $shopMarket['default_motiv_status'];
                        $motive->width = $motivg ['width'];
                        $motive->height = $motivg ['height'];
                        $motive->mid_width = $motivg ['mid_width'];
                        $motive->mid_height = $motivg ['mid_height'];
                        $motive->typ = $motivg ['typ'];
                        $motive->copyright = $motivg ['copyright'];
                        $motive->save();
                    }

                    $images->motive = array();

                    $motivBasket = new TP_FavMotiv ();

                    foreach ($motivBasket->getMotive() as $key => $row) {
                        if ($key == 0) {
                            continue;
                        }

                        $count = Doctrine_Query::create()->from('ContactMotiv c')->where('c.motiv_id = ?', array($key))->count();

                        if ($count > 0) {
                            continue;
                        }

                        $count = Doctrine_Query::create()->from('Motiv c')->where('c.id = ?', array($key))->count();

                        if ($count > 0) {
                            $contactmotiv = new ContactMotiv ();
                            $contactmotiv->contact_id = $contact->id;
                            $contactmotiv->motiv_id = $key;
                            $contactmotiv->save();
                        }
                    }

                    foreach ($motivBasket->getMotiveUUID() as $key => $row) {
                        if ($key == 0) {
                            continue;
                        }

                        $motiv = Doctrine_Query::create()->from('Motiv c')->where('c.uuid = ?', array($key))->fetchOne();
                        if ($count > 0) {
                            continue;
                        }

                        $count = Doctrine_Query::create()->from('ContactMotiv c')->where('c.motiv_id = ?', array($motiv->id))->count();

                        if ($count > 0) {
                            continue;
                        }

                        $contactmotiv = new ContactMotiv ();
                        $contactmotiv->contact_id = $contact->id;
                        $contactmotiv->motiv_id = $motiv->id;
                        $contactmotiv->save();
                    }

                    $motivBasket->clearSession();

                    if ($this->_getParam('mode') == 'basket') {
                        $this->view->priorityMessenger('Login successfull', 'success');

                        $this->redirectSpeak('/basket/review');
                        return;
                    }

                    if ($this->_getParam('mode') == 'approvaloverview') {
                        $this->view->priorityMessenger('Login successfull', 'success');

                        $this->redirectSpeak('/user/myapproval');
                        return;
                    }

                    if ($this->_getParam('mode') == 'approval') {
                        $this->view->priorityMessenger('Login successfull', 'success');

                        $this->redirectSpeak('/article/approval/' . $this->_getParam('uuid'));
                        return;
                    }

                    if ($this->_getParam('mode') == 'product_show') {
                        $this->view->priorityMessenger('Login successfull', 'success');

                        $this->redirectSpeak($this->_getParam('uuid'));
                        return;
                    }

                    if ($this->_getParam('mode') == 'anfrage') {
                        $this->view->priorityMessenger('Login successfull', 'success');

                        $this->redirectSpeak('/basket/anfrage');
                        return;
                    }

                    if ($this->_getParam('mode') == 'orders') {
                        $this->view->priorityMessenger('Login successfull', 'success');

                        $this->redirectSpeak('/user/myorders');
                        return;
                    }

                    if ($this->_getParam('mode') == 'motiv_upload') {
                        $this->view->priorityMessenger('Login successfull', 'success');

                        $this->redirectSpeak('/resale/motivstep1');
                        return;
                    }

                    if ($this->_getParam('mode') == 'resaleshop' || $this->_getParam('mode') == 'resalearticle') {
                        $this->view->priorityMessenger('Login successfull', 'success');

                        /* $resale = new TP_ResaleWizard();
                          $layouterid = $resale->getLayouterId();
                          $articleid = $resale->getArticleId();

                          $article = Doctrine_Query::create()
                          ->from('Article c')
                          ->where('c.enable = 1 AND c.uuid = ?', array($articleid))
                          ->fetchOne();

                          if($article == false) {
                          throw new Zend_Amf_Exception;
                          }

                          $uuid = $article->uuid;
                          if($article->a6_org_article != 0) {
                          $orgid = $article->a6_org_article;
                          }else{
                          $orgid = $article->id;
                          }
                          $article = $article->copy();
                          $article->uuid = "";
                          $article->a6_org_article = $orgid;
                          $article->contact_id = $contact->id;
                          $article->private = true;
                          $article->save();

                          $rel = new ContactArticle();
                          $rel->article_id = $article->id;
                          $rel->contact_id = $contact->id;
                          $rel->save();

                          $articles = Zend_Registry::get('articles');

                          $articleObj = new $articles[$article->typ]();

                          if(method_exists($articleObj, 'copyPreDispatch')) $articleObj->copyPreDispatch($article, $layouterid);
                         */

                        if ($this->_getParam('mode') == 'resaleshop') {
                            $this->redirectSpeak('/resale/shopstep0/' . $article->uuid);
                        }
                        if ($this->_getParam('mode') == 'resalearticle') {
                            $this->redirectSpeak('/resale/articlestep1/' . $article->uuid);
                        }

                        $this->redirectSpeak('/resale/index');
                        return;
                    }

                    if ($this->_getParam('redirect') == 'myorders') {
                        $this->view->priorityMessenger('Login successfull', 'success');

                        $this->redirectSpeak('/user/myorders');
                        return;
                    }

                    if ($this->shop->redirect_login != "") {
                        $this->redirectSpeak($this->shop->redirect_login);
                    }

                    $this->view->priorityMessenger('Login successfull', 'success');
                    $this->forwardSpeak('myoverview', 'user', 'default');
                } else {

                    $row = Doctrine_Query::create()->from('Contact m')->where('m.name = ?', array($this->_getParam('name')))->fetchOne();
                    if ($row != false && $row->enable == 0) {
                        $this->forwardSpeak('verify');
                    }
                    $form->populate($formData);
                    $this->view->priorityMessenger('Login gescheitert', 'error');
                }
            } else {
                $form->populate($formData);
                $this->view->priorityMessenger('Login gescheitert', 'error');
            }
        }

        $this->view->form = $form;

        if ($this->shop->private && !Zend_Auth::getInstance()->hasIdentity()) {
            $this->_helper->layout->setLayout('private');
        } else {
            $this->_helper->layout->setLayout('default');
        }

    }

    /**
     * LayouterLoginaktion
     *
     * @return void
     */
    public function layouterloginAction() {

        $config = new Zend_Config_Ini($this->_configPath . '/user/layouterlogin.ini', 'login');
        $form = new Zend_Form($config->user->login);

        if (Zend_Auth::getInstance()->hasIdentity()) {

            $result = '<html><?xml version="1.0" encoding="UTF-8"?>
					<response>
					<parameter name="sessionId">' . Zend_Session::getId() . '</parameter>
					<parameter name="errorId">0</parameter>
					<parameter name="errorMessage" />
					</response></html>';

            die($result);
        }

        if ($this->_request->isPost()) {

            $formData = $this->_request->getPost();
            if ($form->isValid($formData) && $this->_getParam('name') != 'Benutzername') {
                $_authAdapter = new TP_Plugin_AuthAdapter (); // put this in a constructor?
                $_authAdapter->setIdentity($this->_getParam('name'))->setCredential($this->_getParam('password'));
                $result = Zend_Auth::getInstance()->authenticate($_authAdapter);

                if ($result->isValid()) {

                    $contact = Doctrine_Query::create()->from('Contact as c')->where('c.name = ? AND c.password = ?', array($this->_getParam('name'), $this->_getParam('password')))->fetchOne();

                    TP_Queue::process('contactlayouterlogin', 'global', $contact);

                    $this->getHelper('layout')->disableLayout();
                    $this->_helper->viewRenderer->setNoRender();

                    $result = '<html><?xml version="1.0" encoding="UTF-8"?>
								<response>
								<parameter name="sessionId">' . Zend_Session::getId() . '</parameter>
								<parameter name="errorId">0</parameter>
								<parameter name="errorMessage" />
								</response></html>';

                    die($result);
                } else {

                    $row = Doctrine_Query::create()->from('Contact m')->where('m.name = ?', array($this->_getParam('name')))->fetchOne();
                    if ($row->enable == 0) {
                        $this->forwardSpeak('verify');
                    }

                    $form->populate($formData);
                }
            } else {
                $form->populate($formData);
            }
        }

        $this->view->form = $form;

        $this->_helper->layout->setLayout('simple');
    }

}
