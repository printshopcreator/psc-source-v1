<?php
/**
 * Eine Seite (Page) besteht aus folgenden Einzelteilen:
 *
 * ResponseType/ContentType (html, txt, xml, json ...)
 * ContentCharSet/Encoding (iso-8859-1, utf-8, ...)
 * ContentLanguage (de-DE, en-US, en-EN, ...)
 *
 * Zend stellt ein Response Object, eine LayoutKomponente, diverse Placeholder
 * und View Helper.
 *
 * Aufgabe des PageModels ist die Logik welcher hinter der Entscheidungsfindung
 * liegt zu kapseln (Welches Layout, welcher Response Typ, welcher Content [lang,
 * type] usw.
 *
 */
class PageModel
{
    /**
     * Konstanten für "Content-Type" im Header bereich.
     */
    const XHTML1_1 = 'application/xhtml+xml';
    const XHTML1 = 'text/html';
    const HTML = 'text/html';
    const JSON = 'application/json';
    const XML = 'application/xml';
    const RSS = 'application/rss+xml';
    const RDF = 'application/rdf+xml';
    const ATOM = 'application/atom+xml';

    /**
     * Da der View Helper DocType keinen Doctype für XHTML 1.1 mitliefert müßen
     * wir ihn selbst definieren.
     *
     * Eigentlich hätte man den View Helper erweitern können, da dieser Helper
     * aber etwas komisch aufgebaut ist, müßte man ihn komplett "kopieren" da
     * die definierten Konstanten festgeschrieben sind und im Constructor diese
     * feste Anzahl an die interne registry übergeben wird.
     *
     * @todo Erweitern des Doctype View Helpers um eine XHTML 1.1 Konstante
     *
     * @see Zend_View_Helper_Doctype
     */
    const XHTML_1_1_DocType = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">';

    protected $defaultConfig = array(
        'doctype' => 'HTML4_LOOSE',
        'contentType' => self::HTML,
        'charset' => 'utf-8',
        'language' => 'de-de'
    );
    protected $config = null;

    public function __construct($config = null) {
        if (is_array($config)) {
            $this->setConfig($config);
        } elseif ($config instanceof Zend_Config) {
            $this->setConfig($config->toArray());
        } elseif (null !== $config) {
            throw new Exception('Given config is of wrong DataType');
        } else {
            $this->setConfig(array());
        }
    }

    /**
     * @return unknown
     */
    public function getConfig() {
        if (null === $this->config) {
            $this->setConfig(array());
        }

        return $this->config;
    }

    /**
     * @param unknown_type $config
     */
    public function setConfig($config) {
        $this->config = array_merge($this->defaultConfig, $config);
    }

    public function getLanguage() {
        $config = $this->getConfig();
        return $config['language'];
    }

    public function getCharset() {
        $config = $this->getConfig();
        return $config['charset'];
    }

    public function getContentType() {
        $config = $this->getConfig();
        return $config['contentType'];
    }

    public function getDoctype() {
        $config = $this->getConfig();
        return $config['doctype'];
    }
}

?>