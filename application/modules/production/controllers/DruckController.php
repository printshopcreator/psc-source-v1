<?php

class Production_DruckController extends TP_Production_Controller_Action
{
    protected $_extAllow = 'ppt,inx,idml,indd,qxd,ps,cdr,cdx,jpg,gif,pdf,xls,png,doc,jpeg,tif,tiff,ai,eps,psd,phtml,zip,jrxml,ttf,otf,csv,docx,xlsx';
    /**
     * IndexAction
     *
     * @todo Dokumentation der IndexAction
     *
     * @return void
     */
    public function indexAction() {

        if ($this->getRequest()->isPost()) {

            $username = $this->getRequest()->getParam('username');
            $password = $this->getRequest()->getParam('password');

            $_authAdapter = new TP_Plugin_AuthAdapter (); // put this in a constructor?
            $_authAdapter->setIdentity($this->_getParam('username'))->setCredential($this->_getParam('password'));
            $result = Zend_Auth::getInstance()->authenticate($_authAdapter);
            if ($result->isValid()) {
                $this->view->priorityMessenger('Login erfolgreich', 'success');
                $this->_redirect('/production/druck/portal');
                return;
            } else {
                $this->view->priorityMessenger('Benutzername oder Password falsch', 'error');
            }

        }

    }

    public function portalAction() {
        if (!Zend_Auth::getInstance()->hasIdentity() || $this->highRole->level <= 30) {
            $this->_redirect('/production/druck');
        }

        $translate = Zend_Registry::get('translate');

        $this->view->order = false;

        $this->view->stati = array();
        if ($this->install->production_status != "") {
            foreach (explode(",", $this->install->production_status) as $key) {
                $this->view->stati[$key] = TP_Status::getStatusText($key, TP_Status::$InternalName);
            }
        } else {
            $config = new Zend_Config_Ini(APPLICATION_PATH . '/configs/systemdefaults.ini', 'status');

            foreach ($config->toArray() as $key => $value) {
                $this->view->stati[$key] = $translate->translate($key);
            }
        }

        if ($this->install['offline_custom2'] != "") {
            try {

                $maschienen = new SimpleXMLElement($this->install['offline_custom2'], null, false);

                foreach ($maschienen as $maschine) {
                    $this->view->stati[(int)$maschine['id']] = $translate->translate((string)$maschine);
                }
            } catch (Exception $e) {

            }
        }

        if($this->user->self_information != "") {
            $tempStati = array();
            foreach (explode(",", $this->user->self_information) as $key) {
                $tempStati[$key] = $this->view->stati[$key];
            }
            $this->view->stati = $tempStati;
        }

        if ($this->getRequest()->isPost()) {

            $this->view->search_field = $this->_getParam('search_field');

            $row = Doctrine_Query::create()
                ->select('m.id')
                ->from('Orders m')
                ->leftJoin('m.Contact as c')
                ->addWhere('m.alias = ? AND m.install_id = ?', array($this->_getParam('search_field'), $this->install->id));
            $order = $row->fetchOne();

            $this->view->status = TP_Status::getStatusText($order->status, TP_Status::$InternalName);

            if ($this->user->production_status != "" && $this->user->production_status != 0) {

                $orgstatus = $order->status;

                if ($this->user->production_status > $orgstatus) {
                    $order->status = $this->user->production_status;

                    $order->save();

                    $produktionszeit = 0;
                    foreach ($order->Orderspos as $pos) {
                        $pro = unserialize($pos->data);
                        $proo = $pro->getOptions();

                        $countDays = 10;
                        if($proo['produktion'] == "24std") {
                            $countDays = 1;
                        }
                        if($proo['produktion'] == "exp") {
                            $countDays = 5;
                        }


                        if ($countDays > $produktionszeit) {
                            $produktionszeit = $countDays;
                        }

                        $obj = new TP_Delivery(date('Y-m-d H:i:s'));
                        $result = $obj->getDeliveryDate($countDays);
                        $pos->delivery_date = $result->format('Y-m-d H:i:s');
                        $pos->save();
                    }

                    if ($produktionszeit > 0) {
                        $obj = new TP_Delivery(date('Y-m-d H:i:s'));
                        $result = $obj->getDeliveryDate($produktionszeit);
                        $order->delivery_date = $result->format('Y-m-d H:i:s');
                        $order->save();
                    }

                    reset($order->Orderspos);


                    TP_Queue::process('orderstatuschange', 'global', $order);
                    $this->view->priorityMessenger('Status erfolgreich geändert', 'success');
                    $this->view->status = TP_Status::getStatusText($order->status, TP_Status::$InternalName);

                }

            }

            $this->view->order = $order;

        } elseif ($this->_getParam('uuid', false)) {

            $row = Doctrine_Query::create()
                ->select('m.id')
                ->from('Orders m')
                ->leftJoin('m.Contact as c')
                ->leftJoin('m.Shop as s')
                ->addWhere('m.uuid = ? AND m.install_id = ?', array($this->_getParam('uuid'), $this->install->id));
            $order = $row->fetchOne();

            $orgstatus = $order->status;
            $order->status = $this->_getParam('status');

            $order->save();

            if ($this->_getParam('status') == 540) {

                $produktionszeit = 0;
                foreach ($order->Orderspos as $pos) {
                    $pro = unserialize($pos->data);
                    $proo = $pro->getOptions();

                    if (isset($proo['produktionszeit']) && $proo['produktionszeit'] > $produktionszeit) {
                        $produktionszeit = $proo['produktionszeit'];
                    }
                    if (isset($proo['produktionszeit'])) {
                        $obj = new TP_Delivery(date('Y-m-d H:i:s'));
                        $result = $obj->getDeliveryDate($proo['produktionszeit']);
                        $pos->delivery_date = $result->format('Y-m-d H:i:s');
                        $pos->save();
                    }
                }

                if ($produktionszeit > 0) {
                    $obj = new TP_Delivery(date('Y-m-d H:i:s'));
                    $result = $obj->getDeliveryDate($produktionszeit);
                    $order->delivery_date = $result->format('Y-m-d H:i:s');
                    $order->save();
                }

                reset($order->Orderspos);

            }
            TP_Queue::process('orderstatuschange', 'global', $order);
            $this->view->priorityMessenger('Status erfolgreich geändert', 'success');
            $this->view->status = TP_Status::getStatusText($order->status, TP_Status::$InternalName);
            $this->view->order = $order;
        }

        $row = Doctrine_Query::create()
            ->select('m.id')
            ->from('Orders m')
            ->leftJoin('m.Contact as c')
            ->orderBy("m.created DESC");

        if ($this->getRequest()->isPost()) {

            $this->view->search_field = $this->_getParam('search_field');

            $row->addWhere('m.alias = ? AND m.install_id = ?', array( $this->_getParam('search_field'), $this->install->id));
        }else{
            $row->addWhere('m.status IN (540,550,560) AND m.install_id = ?', array($this->install->id));
        }

        $orders = $row->execute();

        $this->view->orders = $orders;
    }

    public function uploadAction() {
        $row = Doctrine_Query::create()
            ->from('Orderspos m')
            ->where('id = ?', array(intval($this->_request->getParam('id'))))
            ->fetchOne();

        $adapter = new Zend_File_Transfer_Adapter_Http();
        $adapter->addValidator('Extension', false, $this->_extAllow);

        $id = $row->Orders->alias . '_' . $row->id;

        Zend_Registry::get('log')->debug('uploads/' . $this->shop->uid . '/article');
        if(!file_exists('uploads/' . $this->shop->uid . '/article')) {
            mkdir('uploads/' . $this->shop->uid . '/article');
        }
        $adapter->setDestination('uploads/' . $this->shop->uid . '/article');

        $orginalFilename = $adapter->getFileName(null, false);

        $adapter->addFilter('Rename', 'uploads/' . $this->shop->uid . '/article/' . $id . '_' . $adapter->getFileName(null, false));

        if (file_exists('uploads/' . $this->shop->uid . '/article/' . $id . '_' . $adapter->getFileName(null, false))) {

            $id .= time();
            $adapter->addFilter('Rename', 'uploads/' . $this->shop->uid . '/article/' . $id . '_' . $adapter->getFileName(null, false));
        } else {
            $adapter->addFilter('Rename', 'uploads/' . $this->shop->uid . '/article/' . $id . '_' . $adapter->getFileName(null, false));
        }

        if (!$adapter->receive() || !$adapter->isValid()) {
            $error = $adapter->getMessages();

            if (isset($error['fileExtensionFalse'])) {
                $arguments = array('error' => true, 'message' => "Falsche Extension");
            } else {
                $arguments = array('error' => true, 'message' => 'Upload Error');
            }
            die(Zend_Json::encode($arguments));
        }


        $upload = new Upload();

        $upload->name = $orginalFilename;
        $upload->path = $adapter->getFileName(null, false);
        $upload->orderpos_id = intval($this->_request->getParam('id'));
        $upload->typ = $this->_request->getParam('type');
        $upload->preflight_errors = $this->_request->getParam('comment');

        $upload->save();

        $this->_redirect("/production/druck/portal");

    }
}
