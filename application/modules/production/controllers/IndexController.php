<?php

class Production_IndexController extends TP_Production_Controller_Action
{
    /**
     * IndexAction
     *
     * @todo Dokumentation der IndexAction
     *
     * @return void
     */
    public function indexAction() {

        if ($this->getRequest()->isPost()) {

            $username = $this->getRequest()->getParam('username');
            $password = $this->getRequest()->getParam('password');

            $_authAdapter = new TP_Plugin_AuthAdapter (); // put this in a constructor?
            $_authAdapter->setIdentity($this->_getParam('username'))->setCredential($this->_getParam('password'));
            $result = Zend_Auth::getInstance()->authenticate($_authAdapter);
            if ($result->isValid()) {
                $this->view->priorityMessenger('Login erfolgreich', 'success');
                $this->_redirect('/production/index/portal');
                return;
            } else {
                $this->view->priorityMessenger('Benutzername oder Password falsch', 'error');
            }

        }

    }

    public function portalAction() {
        if (!Zend_Auth::getInstance()->hasIdentity() || $this->highRole->level <= 30) {
            $this->_redirect('/production');
        }

        $translate = Zend_Registry::get('translate');

        $this->view->order = false;

        $this->view->stati = array();
        if ($this->install->production_status != "") {
            foreach (explode(",", $this->install->production_status) as $key) {
                $this->view->stati[$key] = TP_Status::getStatusText($key, TP_Status::$InternalName);
            }
        } else {
            $config = new Zend_Config_Ini(APPLICATION_PATH . '/configs/systemdefaults.ini', 'status');

            foreach ($config->toArray() as $key => $value) {
                $this->view->stati[$key] = $translate->translate($key);
            }
        }

        if ($this->install['offline_custom2'] != "") {
            try {

                $maschienen = new SimpleXMLElement($this->install['offline_custom2'], null, false);

                foreach ($maschienen as $maschine) {
                    $this->view->stati[(int)$maschine['id']] = $translate->translate((string)$maschine);
                }
            } catch (Exception $e) {

            }
        }

        if ($this->getRequest()->isPost() && $this->_getParam('search_field', false)) {

            $this->view->search_field = $this->_getParam('search_field');

            $row = Doctrine_Query::create()
                ->select('m.id')
                ->from('Orders m')
                ->leftJoin('m.Contact as c')
                ->addWhere('m.alias = ? AND m.install_id = ?', array($this->_getParam('search_field'), $this->install->id));
            $order = $row->fetchOne();

            $this->view->status = TP_Status::getStatusText($order->status, TP_Status::$InternalName);

            if ($this->user->production_status != "" && $this->user->production_status != 0) {

                $orgstatus = $order->status;

                if (true || $this->user->production_status > $orgstatus) {
                    $order->status = $this->user->production_status;

                    $order->save();

                    $produktionszeit = 0;
                    foreach ($order->Orderspos as $pos) {
                        $pro = unserialize($pos->data);
                        $proo = $pro->getOptions();

                        if (isset($proo['produktionszeit']) && $proo['produktionszeit'] > $produktionszeit) {
                            $produktionszeit = $proo['produktionszeit'];
                        }
                        if (isset($proo['produktionszeit'])) {
                            $obj = new TP_Delivery(date('Y-m-d'));
                            $result = $obj->getDeliveryDate($proo['produktionszeit']);
                            $pos->delivery_date = $result->format('Y-m-d');
                            $pos->save();
                        }
                    }

                    if ($produktionszeit > 0) {
                        $obj = new TP_Delivery(date('Y-m-d'));
                        $result = $obj->getDeliveryDate($produktionszeit);
                        $order->delivery_date = $result->format('Y-m-d');
                        $order->save();
                    }

                    reset($order->Orderspos);

                    $dbMongo = TP_Mongo::getInstance();
                    $dbMongo->Job->insertOne(array(
                        "shop" =>  $this->shop->id,
                        "event" => "order_status_change",
                        "status" => $this->_getParam('status'),
                        "data" => [ "order" => $order->uuid, "status" => $order->status ],
                        "created" => new MongoDB\BSON\UTCDateTime(),
                        "updated" => new MongoDB\BSON\UTCDateTime()
                    ));
                    TP_Queue::process('orderstatuschange', 'global', $order);
                    $this->view->priorityMessenger('Status erfolgreich geändert', 'success');
                    $this->view->status = TP_Status::getStatusText($order->status, TP_Status::$InternalName);

                }

            }

            $this->view->order = $order;

        } elseif ($this->_getParam('uuid', false)) {

            $row = Doctrine_Query::create()
                ->select('m.id')
                ->from('Orders m')
                ->leftJoin('m.Contact as c')
                ->leftJoin('m.Shop as s')
                ->addWhere('m.uuid = ? AND m.install_id = ?', array($this->_getParam('uuid'), $this->install->id));
            $order = $row->fetchOne();

            $orgstatus = $order->status;
            $order->status = $this->_getParam('status');

            $order->save();

            if ($orgstatus < 140 && $this->_getParam('status') >= 140 && $this->_getParam('status') != 170) {

                $produktionszeit = 0;
                foreach ($order->Orderspos as $pos) {
                    $pro = unserialize($pos->data);
                    $proo = $pro->getOptions();

                    if (isset($proo['produktionszeit']) && $proo['produktionszeit'] > $produktionszeit) {
                        $produktionszeit = $proo['produktionszeit'];
                    }
                    if (isset($proo['produktionszeit'])) {
                        $obj = new TP_Delivery(date('Y-m-d'));
                        $result = $obj->getDeliveryDate($proo['produktionszeit']);
                        $pos->delivery_date = $result->format('Y-m-d');
                        $pos->save();
                    }
                }

                if ($produktionszeit > 0) {
                    $obj = new TP_Delivery(date('Y-m-d'));
                    $result = $obj->getDeliveryDate($produktionszeit);
                    $order->delivery_date = $result->format('Y-m-d');
                    $order->save();
                }

                reset($order->Orderspos);

            }
            TP_Queue::process('orderstatuschange', 'global', $order);
            $dbMongo = TP_Mongo::getInstance();
            $dbMongo->Job->insertOne(array(
                "shop" =>  $this->shop->id,
                "event" => "order_status_change",
                "status" => $this->_getParam('status'),
                "data" => [ "order" => $order->uuid, "status" => $this->_getParam('status') ],
                "created" => new MongoDB\BSON\UTCDateTime(),
                "updated" => new MongoDB\BSON\UTCDateTime()
            ));
            $this->view->priorityMessenger('Status erfolgreich geändert', 'success');
            $this->view->status = TP_Status::getStatusText($order->status, TP_Status::$InternalName);
            $this->view->order = $order;

        }else{
            if($this->install->id != 1) {
                $row = Doctrine_Query::create()
                    ->select('m.id')
                    ->from('Orders m')
                    ->leftJoin('m.Contact as c')
                    ->addWhere('m.status NOT IN (200,210) AND m.install_id = ?', array($this->install->id));
                $orders = $row->execute();

                $this->view->orders = $orders;
            }
        }
    }
}
