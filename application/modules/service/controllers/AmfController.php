<?php

class Service_AmfController extends Zend_Controller_Action
{

    public function init() {
        $this->getHelper('layout')->disableLayout();
        $this->_helper->viewRenderer->setNoRender();
    }

    public function indexAction() {

        require_once("ZendAmfServiceBrowser.php");
        $server = new Zend_Amf_Server();
        $server->setClass('TP_AmfServer');
        $server->setClass('ZendAmfServiceBrowser');
        $server->setClassMap('TP_Amf_Contracts_FileVO', 'TP_Amf_Contracts_FileVO');
        $server->setProduction(false);

        ZendAmfServiceBrowser::$ZEND_AMF_SERVER = $server;

        try {
            echo($server->handle());
        } catch (Exception $e) {
            Zend_Registry::get('log')->log("Error AMF Server" . $e->getMessage(), Zend_Log::DEBUG);
        }

    }
}


