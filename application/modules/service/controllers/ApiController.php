<?php

class Service_ApiController extends Zend_Controller_Action
{

    public function init() {
        $this->getHelper('layout')->disableLayout();
        $this->_helper->viewRenderer->setNoRender();
    }

    public function csloginAction() {

        $shop = Zend_Registry::get("shop");

        $install = Zend_Registry::get("install");

        $shared = "f6k0vl";

        if( $_GET['lokal'] ){

            // Kusch intern, wird nicht benötigt und nicht gespeichert.
            $_GET['benutzer_ID'] = '12346';

            // extern = Händer, intern = Intern, admin = Administratoren
            $_GET['benutzer_gruppe'] = 'intern';
            $_GET['benutzer_email'] = 'tgross9812@psw.net';
            $_GET['vorname'] = 'Peter1';
            $_GET['name'] = 'm%2Bs+AG';
            $_GET['kostenstelle'] = '9415';
            $_GET['kundennummer'] = '111111';
        }

        $setstring = md5($_GET['benutzer_ID'].'$'.$_GET['benutzer_email'].'$'.$_GET['benutzer_gruppe'].'$'.$_GET['vorname'].'$'.$_GET['name'].'$'.$_GET['kostenstelle'].'$'.$_GET['kundennummer'].'$'.$shared);

        if( $_GET['lokal'] ){
            $_GET['h'] = $setstring;
        }

        if( ($setstring != $_GET['h']) || !isset($_GET['h']) ){
            die("Aufruf nicht gestattet.");
        }
        ini_set('display_errors', 0);
        $Buchstaben = array("a", "b", "c", "d", "e", "f", "g", "h", "k", "m", "n", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z");
        $Zahlen = array("2", "3", "4", "5", "6", "7", "8", "9");

        $Laenge = 6;
        $Passwort = "";
        for ($i = 0, $Passwort = ""; strlen($Passwort) < $Laenge; $i++) {
            if (rand(0, 2) == 0 && isset($Buchstaben)) {
                $Passwort .= $Buchstaben [rand(0, (count($Buchstaben) - 1))];
            } elseif (rand(0, 2) == 1 && isset($Zahlen)) {
                $Passwort .= $Zahlen [rand(0, (count($Zahlen) - 1))];
            }
        }
        ini_set('display_errors', 1);


        $contact = Doctrine_Query::create()->from('Contact as c')->leftJoin("c.Shops s")->where('c.self_email = ? AND s.id = ?', array($_GET['benutzer_email'], $shop['id']))->fetchOne();

        if($contact) {
            $_authAdapter = new TP_Plugin_AuthAdapter ();
            $_authAdapter->setIdentity($contact->self_email)->setCredential($contact->password);
            $result = Zend_Auth::getInstance()->authenticate($_authAdapter);

            if($result = $result->isValid()) {
                $this->_redirect("/");
                die();
            }
        }

        $contact = new Contact ();
        $contact->name = $_GET['benutzer_email'];
        $contact->password = password_hash($Passwort, PASSWORD_DEFAULT);

        $contact->self_country = "DE";
        $contact->self_firstname = $_GET['vorname'];
        $contact->self_lastname = $_GET['name'];
        $contact->liefer_firstname = $_GET['vorname'];
        $contact->liefer_lastname = $_GET['name'];
        $contact->self_email = $_GET['benutzer_email'];
        $contact->email = $_GET['benutzer_email'];
        $contact->newsletter = 0;
        $contact->liefer = 0;

        $contact->created = date('Y-m-d');
        $contact->updated = date('Y-m-d');
        $locale = Zend_Registry::get('locale');
        $contact->language = $locale->getLanguage() . '_' . $locale->getRegion();
        $contact->hash = $Passwort;
        $contact->enable = false;
        $contact->self_position = $_GET['kundennummer'];
        $contact->ustid = $_GET['kostenstelle'];



        $contact->install_id = $install['id'];
        $contact->enable = true;

        switch( $_GET['benutzer_gruppe'] ){

            case 'admin':
                $usergroup = 1046;
                break;

            case 'intern':
                $usergroup = 1045;
                break;

            // Händler
            case 'extern':
                $usergroup = 1043;
                break;

            default:
                $usergroup = 1043;
                break;
        }


        $contact->account_id = $usergroup;

        $contact->save();

        $shopcontact = new ShopContact ();
        $shopcontact->contact_id = $contact->id;
        $shopcontact->shop_id = $shop['id'];
        $shopcontact->save();

        $roles = new Zend_Config_Ini(APPLICATION_PATH . '/configs/roles.ini', APPLICATION_ENV);
        foreach ($roles->contact as $row) {
            $rolecontact = new ContactRole ();
            $rolecontact->contact_id = $contact->id;
            $rolecontact->role_id = $row;
            $rolecontact->save();
        }

        $_authAdapter = new TP_Plugin_AuthAdapter ();
        $_authAdapter->setIdentity($contact->self_email)->setCredential($contact->password);
        $result = Zend_Auth::getInstance()->authenticate($_authAdapter);

        if($result = $result->isValid()) {
            $this->_redirect("/");
        }
        echo "success";

    }

    public function changestatusAction() {

        $username = $this->_getParam('username');
        $password = $this->_getParam('password');
        $order = $this->_getParam('order');
        $status = $this->_getParam('status');

        if (!isset($order) || $order == ""
            || !isset($status) || $status == ""
            || !isset($username) || $username == ""
            || !isset($password) || $password == ""
        ) {

            die('FAIL');

        }

        if ($username != 'api' || $password != 'apikey') {
            die('NO AUTH');
        }

        $data = Doctrine_Query::create()
            ->from('Orders a')
            ->where('a.alias = ?')->fetchOne(array($order));

        if ($data == false) {
            die('NOT FOUND');
        }

        $data->status = $status;
        $data->save();

        echo 'success';

    }

    public function computopAction() {

        $Data = $this->_getParam("Data");
        $Len = $this->_getParam("Len");

        // decrypt the data string

        require_once(APPLICATION_PATH . '/queues/Computop/function.inc.php');
        $myPayGate = new ctBlowfish;
        $plaintext = $myPayGate->ctDecrypt($Data, $Len, 'A)f4_c7QL*d6x9(G');

        // prepare notify log

        Zend_Registry::get('log')->debug($plaintext);

        echo "ok";

    }

    public function giropayAction() {



        echo "ok";

    }

    public function heidelpayAction() {

        $Data = $this->_getAllParams();

        // prepare notify log

        Zend_Registry::get('log')->debug(print_r($Data, true));
        if ($Data['POST_VALIDATION'] != 'ACK') {
            die("http://" . $_SERVER["SERVER_NAME"] . "/basket/finish/error/" . rawurlencode("Fehler: " . $this->_getParam('PROCESSING_REASON') . "" . $this->_getParam('PROCESSING_RETURN')));
        }

        die("http://" . $_SERVER["SERVER_NAME"] . "/basket/finish/hash/Ja/token/" . $this->_getParam('token'));

    }

    public function directposAction() {

        $Data = $this->_getAllParams();

        // prepare notify log

        Zend_Registry::get('log')->debug(implode("/", $Data));
        if ($this->_getParam('directPosErrorCode') == 0) {
            die("redirecturls=http://" . $_SERVER["SERVER_NAME"] . "/basket/finish/hash/Ja/token/" . $this->_getParam('sessionid'));
        } else {
            die("redirecturlf=http://" . $_SERVER["SERVER_NAME"] . "/basket/finish/error/" . rawurlencode("Fehler: " . $this->_getParam('directPosErrorMessage')));
        }

    }

    public function xmlrpcAction() {

        $server = new Zend_XmlRpc_Server();
        $server->setClass('TP_Xmlrpc');

        echo $server->handle();

    }

    public function senddeliveryAction() {

        $this->view->clearVars();
        $adapter = new Zend_File_Transfer_Adapter_Http();
        $adapter->addValidator('Extension', false, "xls,xlsx");

        if (!$adapter->receive() || !$adapter->isValid()) {
            $error = $adapter->getMessages();
            if (isset($error['fileExtensionFalse'])) {
                $arguments = array('error' => true, 'message' => $error['fileExtensionFalse']);
            } else {
                $arguments = array('error' => true, 'message' => 'Upload Error');
            }
            Zend_Registry::get('log')->debug('UPLOAD CMS DEBUG' . print_r($error, true));
            die(Zend_Json::encode($arguments));
        } elseif ($adapter->receive() && $adapter->isValid()) {
            $basket = new TP_Basket();
            $tempProdukt = $basket->getTempProduct();
            $arguments = $tempProdukt->getDeliverys();
            $arguments['success'] = true;

            require_once('PHPExcel/PHPExcel.php');

            try {
                $objPHPExcel = PHPExcel_IOFactory::load($adapter->getFileName());

                $basket = new TP_Basket();
                $tempProdukt = $basket->getTempProduct();

                $worksheet = $objPHPExcel->getActiveSheet();

                $data = array();
                $stripFirstLine = false;
                foreach ($worksheet->getRowIterator() as $row) {
                    $rowIndex = $row->getRowIndex();
                    $uuid = TP_Util::uuid();
                    $cell = $worksheet->getCell('A' . $rowIndex);
                    $cellB = $worksheet->getCell('B' . $rowIndex);
                    $cellC = $worksheet->getCell('C' . $rowIndex);
                    $cellE = $worksheet->getCell('E' . $rowIndex);
                    $cellI = $worksheet->getCell('I' . $rowIndex);

                    if(strtolower($this->checkIfRichText($cell->getValue())) == "projekt:") {
                        $arguments['projectname'] = $this->checkIfRichText($cellE->getValue());
                        $arguments['projectdate'] = $this->checkIfRichText($cellI->getValue());
                    }

                    if(strtolower($this->checkIfRichText($cell->getValue())) == "pos."
                        || strtolower($this->checkIfRichText($cell->getValue())) == "projekt:"
                        || ((strtolower($this->checkIfRichText($cellB->getValue())) == ""
                        || strtolower($this->checkIfRichText($cellC->getValue())) == "") && strtolower($this->checkIfRichText($cellB->getValue())) != "")) {
                        continue;
                    }
                    if(((strtolower($this->checkIfRichText($cellB->getValue())) == ""
                            || strtolower($this->checkIfRichText($cellC->getValue())) == "") && strtolower($this->checkIfRichText($cellB->getValue())) == "")) {
                        break;
                    }
                    $temp = array();
                    $temp[] = $this->checkIfRichText($cell->getValue());
                    $cell = $worksheet->getCell('B' . $rowIndex);
                    $temp[] = $this->checkIfRichText($cell->getValue());
                    $cell = $worksheet->getCell('C' . $rowIndex);
                    $temp[] = $this->checkIfRichText($cell->getValue());
                    $cell = $worksheet->getCell('D' . $rowIndex);
                    $temp[] = $this->checkIfRichText($cell->getValue());
                    $cell = $worksheet->getCell('E' . $rowIndex);
                    $temp[] = $this->checkIfRichText($cell->getValue());
                    $cell = $worksheet->getCell('F' . $rowIndex);
                    $temp[] = str_replace(array("DE","AT","FR","Ö"), array("D","A","F","A"), $this->checkIfRichText($cell->getValue()));
                    $cell = $worksheet->getCell('G' . $rowIndex);
                    $temp[] = $this->checkIfRichText($cell->getValue());
                    $cell = $worksheet->getCell('H' . $rowIndex);
                    $temp[] = $this->checkIfRichText($cell->getValue());
                    $cell = $worksheet->getCell('I' . $rowIndex);
                    $temp[] = $this->checkIfRichText($cell->getValue());
                    $cell = $worksheet->getCell('J' . $rowIndex);
                    $temp[] = date('d.m.y',PHPExcel_Shared_Date::ExcelToPHP($cell->getValue()));
                    $cell = $worksheet->getCell('K' . $rowIndex);
                    $temp[] = $this->checkIfRichText($cell->getValue());
                    $data[$uuid] = $temp;
                }

                $arguments['data'] = $data;

                $tempProdukt->setDeliverys($arguments);

            }catch(Exception $e) {
                $arguments = array('success' => false);

                die(Zend_Json::encode($arguments));
            }



            Zend_Registry::get('log')->debug('UPLOAD SEND DELIVERY'.$adapter->getFileName());

            die(Zend_Json::encode($arguments));
        }

        $arguments = array('success' => false);

        die(Zend_Json::encode($arguments));
    }

    public function loaddeliveryAction() {

        $this->view->clearVars();

        $basket = new TP_Basket();
        $tempProdukt = $basket->getTempProduct();
        $args = $tempProdukt->getDeliverys();

        $data = array();
        foreach($args['data'] as $uuid => $delivery) {
            $temp = array();

            $temp['DT_RowId'] = $uuid;
            $temp['pos'] = $delivery[0];
            $temp['version'] = $delivery[1];
            $temp['auflage'] = $delivery[2];
            $temp['liefername'] = $delivery[3];
            $temp['lieferstreet'] = $delivery[4];
            $temp['land'] = strtoupper($delivery[5]);
            $temp['plz'] = $delivery[6];
            $temp['lieferort'] = $delivery[7];
            $temp['buendelung'] = $delivery[8];
            $temp['liefertermin'] = $delivery[9];
            $temp['hinweiss'] = $delivery[10];

            $temp['edit'] = '<a class="edit btn btn-small btn-warning" title="Bearbeiten" href=""><i class="icon-pencil"></i></a>';
            $temp['del'] = '<a class="delete btn btn-small btn-danger" title="Löschen" href=""><i class="icon-remove"></i></a>';
            $data[] = $temp;
        }

        $response = array(
            "id" => -1,
              "error" => "",
              "fieldErrors" => array(),
              "data" => array(),
            'iTotalRecords' => count($data),
            'iTotalDisplayRecords' => count($data),
            'aaData' => $data
        );

        die(Zend_Json::encode($response));
    }

    public function loaddeliverysimpleAction() {

        $this->view->clearVars();

        $basket = new TP_Basket();
        $tempProdukt = $basket->getTempProduct();
        $args = $tempProdukt->getDeliverys();

        $data = array();
        foreach($args['data'] as $uuid => $delivery) {
            $temp = array();

            $temp['DT_RowId'] = $uuid;
            $temp['pos'] = $delivery[0];
            $temp['version'] = $delivery[1];
            $temp['auflage'] = $delivery[2];
            $temp['buendelung'] = $delivery[8];
            $temp['liefertermin'] = $delivery[9];
            $temp['hinweiss'] = $delivery[10];

            $temp['edit'] = '<a class="edit btn btn-small btn-warning" title="Bearbeiten" href=""><i class="icon-pencil"></i></a>';
            $temp['del'] = '';
            $data[] = $temp;
        }

        $response = array(
            "id" => -1,
            "error" => "",
            "fieldErrors" => array(),
            "data" => array(),
            'iTotalRecords' => count($data),
            'iTotalDisplayRecords' => count($data),
            'aaData' => $data
        );

        die(Zend_Json::encode($response));
    }

    public function savemailingAction() {

        $this->getHelper('layout')->disableLayout();
        $this->_helper->viewRenderer->setNoRender();

        $basket = new TP_Basket();
        $tempProdukt = $basket->getTempProduct();
        $args = $tempProdukt->getDeliverys();

        $args['versand_teile'] = $this->_getParam("versand_teile", false);
        $args['versand_huelle'] = $this->_getParam("versand_huelle", false);
        if($this->_getParam("versand_huelle", false)) {
            $args['versand_huelle_druck'] = $this->_getParam("versand_huelle_druck", false);
        }else{
            $args['versand_huelle_druck'] = "unbedruckt";
        }

        $args['versand_huelle_color'] = $this->_getParam("versand_huelle_color", false);
        $args['versand_anschreib'] = $this->_getParam("versand_anschreib", false);
        $args['versand_anschreib_druck'] = $this->_getParam("versand_anschreib_druck", false);
        $args['versand_anschreib_color'] = $this->_getParam("versand_anschreib_color", false);

        $tempProdukt->setDeliverys($args);

        $this->getHelper('Json')->sendJson(array('success' => true));
    }

    public function savemailingportoAction() {

        $this->getHelper('layout')->disableLayout();
        $this->_helper->viewRenderer->setNoRender();

        $basket = new TP_Basket();
        $tempProdukt = $basket->getTempProduct();
        $args = $tempProdukt->getDeliverys();

        $args['versand_national'] = $this->_getParam("versand_national", false);
        $args['versand_innational'] = $this->_getParam("versand_innational", false);
        $args['versand_date'] = $this->_getParam("versand_date", false);

        $tempProdukt->setDeliverys($args);

        $this->getHelper('Json')->sendJson(array('success' => true));
    }

    public function savedeliveryAction() {

        $this->getHelper('layout')->disableLayout();
        $this->_helper->viewRenderer->setNoRender();

        $basket = new TP_Basket();
        $tempProdukt = $basket->getTempProduct();
        $args = $tempProdukt->getDeliverys();

        $data = $args['data'];

        $response = array(
            'success' => true
        );



        if(!$this->_getParam('DT_RowId', false)) {
            $temp = array();

            $temp[0] = $this->_getParam('pos', false);
            $temp[1] = $this->_getParam('version', false);
            $temp[2] = $this->_getParam('auflage', false);
            if($this->_getParam('plz', false)) {
                $temp[3] = $this->_getParam('liefername', false);
                $temp[4] = $this->_getParam('lieferstreet', false);
                $temp[5] = str_replace(array("DE","AT","FR","Ö"), array("D","A","F","A"), strtoupper($this->_getParam('land', false)));
                $temp[6] = $this->_getParam('plz', false);
                $temp[7] = $this->_getParam('lieferort', false);
            }

            $temp[8] = $this->_getParam('buendelung', false);
            $temp[9] = $this->_getParam('liefertermin', false);
            $temp[10] = $this->_getParam('hinweiss', false);


            $data[TP_Util::uuid()] = $temp;
        }else{
            $temp = $data[$this->_getParam('DT_RowId', false)];

            $temp[0] = $this->_getParam('pos', false);
            $temp[1] = $this->_getParam('version', false);
            $temp[2] = $this->_getParam('auflage', false);
            if($this->_getParam('plz', false)) {
                $temp[3] = $this->_getParam('liefername', false);
                $temp[4] = $this->_getParam('lieferstreet', false);
                $temp[5] = str_replace(array("DE","AT","FR","Ö"), array("D","A","F","A"), strtoupper($this->_getParam('land', false)));
                $temp[6] = $this->_getParam('plz', false);
                $temp[7] = $this->_getParam('lieferort', false);
            }
            $temp[8] = $this->_getParam('buendelung', false);
            $temp[9] = $this->_getParam('liefertermin', false);
            $temp[10] = $this->_getParam('hinweiss', false);

            $data[$this->_getParam('DT_RowId', false)] = $temp;
        }



        $args['data'] = $data;

        $tempProdukt->setDeliverys($args);

        $this->getHelper('Json')->sendJson(array('success' => true, 'data' => $args, 'temp' => $temp));

    }

    public function deletedeliveryAction() {

        $this->getHelper('layout')->disableLayout();
        $this->_helper->viewRenderer->setNoRender();

        $basket = new TP_Basket();
        $tempProdukt = $basket->getTempProduct();
        $args = $tempProdukt->getDeliverys();

        $data = $args['data'];
        unset($data[$this->_getParam('DT_RowId', false)]);
        $args['data'] = $data;
        $tempProdukt->setDeliverys($args);


        $this->getHelper('Json')->sendJson(array('success' => true, 'data' => $args));
    }

    public function savedeliveryaddressAction() {
        $this->getHelper('layout')->disableLayout();
        $this->_helper->viewRenderer->setNoRender();

        $basket = new TP_Basket();
        $tempProdukt = $basket->getTempProduct();
        $args = $tempProdukt->getDeliverys();

        $args['address'] = array(
            'company' => $this->_getParam('company'),
            'name' => $this->_getParam('name'),
            'street' => $this->_getParam('street'),
            'city' => $this->_getParam('city'),
            'zip' => $this->_getParam('zip'),
            'country' => $this->_getParam('country')
        );

        if(count($args['data']) == 0 && ($this->_getParam('versand') == 1)) {

            for($i=1; $i<=$this->_getParam('anzahlSorten');$i++) {

                $temp = array();

                $temp[0] = $i;
                $temp[1] = $i;
                $temp[2] = 0;
                $temp[3] = $args['address']['name'];
                $temp[4] = $args['address']['street'];
                $temp[5] = $args['address']['country'];
                $temp[6] = $args['address']['zip'];
                $temp[7] = $args['address']['city'];
                $temp[8] = "";
                $temp[9] = "";
                $temp[10] = "";

                $args['data'][TP_Util::uuid()] = $temp;

            }

        }

        if(count($args['data']) != $this->_getParam('anzahlSorten') && ($this->_getParam('versand') == 1)) {

            if(count($args['data']) > $this->_getParam('anzahlSorten')) {
                $args['data'] = array_slice($args['data'], 0 , $this->_getParam('anzahlSorten'));
            }

            if(count($args['data']) < $this->_getParam('anzahlSorten')) {
                for($i=count($args['data'])+1; $i<=$this->_getParam('anzahlSorten');$i++) {

                    $temp = array();

                    $temp[0] = $i;
                    $temp[1] = $i;
                    $temp[2] = 0;
                    $temp[3] = $args['address']['name'];
                    $temp[4] = $args['address']['street'];
                    $temp[5] = $args['address']['country'];
                    $temp[6] = $args['address']['zip'];
                    $temp[7] = $args['address']['city'];
                    $temp[8] = "";
                    $temp[9] = "";
                    $temp[10] = "";

                    $args['data'][TP_Util::uuid()] = $temp;

                }
            }

        }

        $tmp = array();
        foreach($args['data'] as $key => $row) {
            $row[3] = $args['address']['name'];
            $row[4] = $args['address']['street'];
            $row[5] = $args['address']['country'];
            $row[6] = $args['address']['zip'];
            $row[7] = $args['address']['city'];

            $tmp[$key] = $row;
        }

        $args['data'] = $tmp;

        $tempProdukt->setDeliverys($args);


        $this->getHelper('Json')->sendJson(array('success' => true, count($args['data'])));
    }

    public function downloaddeliveryAction() {

        $this->getHelper('layout')->disableLayout();
        $this->_helper->viewRenderer->setNoRender();



        header('Content-Type: application/octet-stream');

        // Es wird downloaded.pdf benannt
        header('Content-Disposition: attachment; filename="vorlage_frei_haus.xsl"');
        header('Content-Transfer-Encoding: binary');

        $shop = Zend_Registry::get("shop");

        try {
            require_once('PHPExcel/PHPExcel.php');
            $objPHPExcel = PHPExcel_IOFactory::load(APPLICATION_PATH . "/design/clients/" . $shop['uid'] . "/shopvorlage.xls");

            $basket = new TP_Basket();
            $tempProdukt = $basket->getTempProduct();

            $delivers = $tempProdukt->getDeliverys();


            if(isset($delivers['address'])) {

                $worksheet = $objPHPExcel->getActiveSheet();

                $data = array();
                $stripFirstLine = false;
                foreach ($worksheet->getRowIterator() as $row) {
                    $rowIndex = $row->getRowIndex();
                    $cell = $worksheet->getCell('A' . $rowIndex);
                    $cellB = $worksheet->getCell('B' . $rowIndex);
                    $cellC = $worksheet->getCell('C' . $rowIndex);
                    $cellE = $worksheet->getCell('E' . $rowIndex);
                    $cellI = $worksheet->getCell('I' . $rowIndex);

                    if(strtolower($cell->getCalculatedValue()) == "pos."
                        || strtolower($cell->getCalculatedValue()) == "projekt:") {
                        continue;
                    }

                    if(strtolower($cell->getCalculatedValue()) == "") break;

                    $worksheet->setCellValue('D'.$rowIndex, $delivers['address']['name']);
                    $worksheet->setCellValue('E'.$rowIndex, $delivers['address']['street']);
                    $worksheet->setCellValue('F'.$rowIndex, $delivers['address']['country']);
                    $worksheet->setCellValue('G'.$rowIndex, $delivers['address']['zip']);
                    $worksheet->setCellValue('H'.$rowIndex, $delivers['address']['city']);
                }

            }

            $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
            $objWriter->save('php://output');


        }catch (Exception $e) {
            var_dump($e->getMessage());
        }

        die();


    }

    public function downloaddeliveryemptyAction() {

        $this->getHelper('layout')->disableLayout();
        $this->_helper->viewRenderer->setNoRender();



        header('Content-Type: application/octet-stream');

        // Es wird downloaded.pdf benannt
        header('Content-Disposition: attachment; filename="vorlage_dienstleister.xls"');
        header('Content-Transfer-Encoding: binary');

        $shop = Zend_Registry::get("shop");

        try {
            require_once('PHPExcel/PHPExcel.php');
            $objPHPExcel = PHPExcel_IOFactory::load(APPLICATION_PATH . "/design/clients/" . $shop['uid'] . "/shopvorlage.xls");

            $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
            $objWriter->save('php://output');


        }catch (Exception $e) {
            var_dump($e->getMessage());
        }

        die();


    }

    // Fuktion readfile_chunked();
    protected function readfile_chunked($filename) {
        $chunksize = 1 * (1024 * 1024); // how many bytes per chunk
        $buffer = '';
        $handle = fopen($filename, 'rb');
        if ($handle === false) {
            return false;
        }
        while (!feof($handle)) {
            $buffer = fread($handle, $chunksize);
            print $buffer;
            ob_flush();
            flush();
        }
        return fclose($handle);
        die();
    }

    private function checkIfRichText($value) {

        if($value instanceof PHPExcel_RichText) {
            return $value->getPlainText();
        }

        return $value;
    }
}