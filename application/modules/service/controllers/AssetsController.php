<?php

class Service_AssetsController extends TP_Controller_Action
{

    public function init() {
        parent::init();
        $this->getHelper('layout')->disableLayout();
        $this->_helper->viewRenderer->setNoRender();
    }

    public function printAction() {

        if (Zend_Auth::getInstance()->hasIdentity()
            && $this->user != null
        ) {

            $filters = array(
                'alias' => 'StripTags',
                'mode' => 'Alnum'
            );

            $validators = array(
                'alias' => 'NotEmpty',
                'mode' => array('Alnum', 'NotEmpty')
            );

            $input = new Zend_Filter_Input($filters, $validators, $this->_getAllParams());

            if ($input->isValid()) {

                $data = Doctrine_Query::create()
                    ->from('Orders a')
                    ->where('a.alias = ?')->fetchOne(array($input->alias));

                if ($data != false && ($data->contact_id == $this->user->id ||
                    ($this->highRole->level >= 30 &&
                        in_array($this->user->id, $this->user->Shops->getPrimaryKeys(), true)))
                ) {

                    $file = TP_Util::writeReport($data->id, $input->mode);

                    if ($file == false || !file_exists($file)) {
                        $this->_forward('error', 'error');
                    }
                    header('Content-type: application/pdf');
                    header('Content-Disposition: attachment; filename="' . $input->mode . '-' . $input->alias . '.pdf"');
                    echo file_get_contents($file);

                } else {
                    $this->_forward('error', 'error');
                }
            } else {
                $this->_forward('error', 'error');
            }

        } else {
            $this->_forward('error', 'error');
        }

    }
}


