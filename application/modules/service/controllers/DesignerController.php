<?php

class Service_DesignerController extends TP_Controller_Action
{

    protected $_extAllow = 'ppt,inx,idml,indd,qxd,ps,cdr,cdx,jpg,gif,pdf,png,jpeg,tiff,ai,eps,ttf,svg,tif';

    public function init() {
        parent::init();
        $this->getHelper('layout')->disableLayout();
        $this->_helper->viewRenderer->setNoRender();
    }

    public function uploadinfoAction() {
        echo "clipno=" . TP_Util::uuid();
    }

    public function uploadAction() {

        if (Zend_Auth::getInstance()->hasIdentity()) {

            $user = Zend_Auth::getInstance()->getIdentity();
            $user = Doctrine_Query::create()
                ->from('Contact m')
                ->where('id = ?')->fetchOne(array($user['id']));

            $i = 0;
            $shop = Zend_Registry::get('shop');

            if ($shop['market']) {
                $shopMarket = Doctrine_Query::create()->from('Shop c')->where('c.id = ?', array($user->Install->defaultmarket))->fetchOne();
            } else {
                $shopMarket = $shop;
            }

            $adapter = new Zend_File_Transfer_Adapter_Http();
            $adapter->addValidator('Extension', false, $this->_extAllow);

            if (!file_exists(APPLICATION_PATH . '/../market/motive/' . $user->uuid)) {
                mkdir(APPLICATION_PATH . '/../market/motive/' . $user->uuid);
            }

            $adapter->setDestination(APPLICATION_PATH . '/../market/motive/' . $user->uuid . '/');

            $id = sha1(mt_rand() . microtime());

            $orginalFilename = $adapter->getFileName(null, false);

            $path_parts = pathinfo($orginalFilename);

            $umlaute = Array("/ä/", "/ö/", "/ü/", "/Ä/", "/Ö/", "/Ü/", "/ß/");
            $replace = Array("ae", "oe", "ue", "Ae", "Oe", "Ue", "ss");
            $filename = preg_replace($umlaute, $replace, $adapter->getFileName(null, false));
            $filter = new Zend_Filter_Alnum();
            $filename = $filter->filter($filename) . '.' . $path_parts['extension'];

            $adapter->addFilter('Rename', APPLICATION_PATH . '/../market/motive/' . $user->uuid . '/' . $id . '_' . $filename);

            if ($adapter->receive() && $adapter->isValid()) {

                $im = new imagick();
                $im->readImage(APPLICATION_PATH . '/../market/motive/' . $user->uuid . '/' . $id . '_' . $filename);
                $res = $im->getImageResolution();
                $res['x'] = intval($res['x']);
                if ($res['x'] != '300') {
                    $res['x'] = 300;
                    $im->setImageResolution(300, 300);
                }

                if ($im->getImageUnits() == 2) {
                    $im->setImageUnits(1);
                    $res['x'] = $res['x'] * 2.54;
                }

                $imgwork = $im->clone();
                $imgwork->writeImage(APPLICATION_PATH . '/../market/motive/' . $user->uuid . '/work_' . $id . '_' . $filename);
                $imgmid = $im->clone();
                $imgmid->thumbnailImage(500, null);
                $imgmid->writeImage(APPLICATION_PATH . '/../market/motive/' . $user->uuid . '/mid_' . $id . '_' . $filename);
                $imgthumb = $im->clone();
                $imgthumb->thumbnailImage(null, 110);
                $imgthumb->writeImage(APPLICATION_PATH . '/../market/motive/' . $user->uuid . '/thumb_' . $id . '_' . $filename);

                $shop = Zend_Registry::get('shop');

                $motive = new Motiv();
                $motive->uuid = $this->_getParam('flname');
                $motive->Account = $user->Account;
                $motive->shop_id = $shop['id'];
                $motive->Contact = $user;

                $user->motiv_count = $user->motiv_count + 1;
                $user->save();

                $motive->Install = $user->Install;
                $motive->resale_shop = false;
                $motive->resale_market = false;
                $motive->resale_download = false;

                $motive->title = $orginalFilename;
                $motive->dpi_org = $res['x'];
                $motive->dpi_con = '300';
                $motive->orgfilename = $orginalFilename;

                $motive->file_orginal = $user->uuid . '/' . $id . '_' . $filename;
                $motive->file_work = $user->uuid . '/work_' . $id . '_' . $filename;
                $motive->file_mid = $user->uuid . '/mid_' . $id . '_' . $filename;
                $motive->file_thumb = $user->uuid . '/thumb_' . $id . '_' . $filename;
                $motive->status = $shopMarket['default_motiv_status'];
                $motive->width = $im->getImageWidth();
                $motive->height = $im->getImageHeight();
                $motive->mid_width = $imgmid->getImageWidth();
                $motive->mid_height = $imgmid->getImageHeight();
                $motive->typ = $im->getFormat();
                $motive->copyright = $im->getImageProperty('exif:Copyright');
                $motive->save();

                $contactmotiv = new ContactMotiv();
                $contactmotiv->contact_id = $user->id;
                $contactmotiv->motiv_id = $motive->id;
                $contactmotiv->save();

            } else {

            }

        } else {

            if (!file_exists(APPLICATION_PATH . '/../market/guest/' . Zend_Session::getId())) {
                mkdir(APPLICATION_PATH . '/../market/guest/' . Zend_Session::getId());
            }

            $adapter = new Zend_File_Transfer_Adapter_Http();
            $adapter->addValidator('Extension', false, $this->_extAllow);

            $adapter->setDestination(APPLICATION_PATH . '/../market/guest/' . Zend_Session::getId());

            $id = sha1(mt_rand() . microtime());

            $orginalFilename = $adapter->getFileName(null, false);

            $path_parts = pathinfo($orginalFilename);

            $umlaute = Array("/ä/", "/ö/", "/ü/", "/Ä/", "/Ö/", "/Ü/", "/ß/");
            $replace = Array("ae", "oe", "ue", "Ae", "Oe", "Ue", "ss");
            $filename = preg_replace($umlaute, $replace, $adapter->getFileName(null, false));
            $filter = new Zend_Filter_Alnum();
            $filename = $filter->filter($filename) . '.' . $path_parts['extension'];

            $adapter->addFilter('Rename', APPLICATION_PATH . '/../market/guest/' . Zend_Session::getId() . '/' . $id . '_' . $filename);

            if ($adapter->receive() && $adapter->isValid()) {

                $im = new imagick();
                $im->readImage(APPLICATION_PATH . '/../market/guest/' . Zend_Session::getId() . '/' . $id . '_' . $filename);
                $res = $im->getImageResolution();
                $res['x'] = intval($res['x']);
                if ($res['x'] != '300') {
                    $res['x'] = 300;
                    $im->setImageResolution(300, 300);
                }

                if ($im->getImageUnits() == 2) {
                    $im->setImageUnits(1);
                    $res['x'] = $res['x'] * 2.54;
                }

                $imgwork = $im->clone();
                $imgwork->writeImage(APPLICATION_PATH . '/../market/guest/' . Zend_Session::getId() . '/work_' . $id . '_' . $filename);
                $imgmid = $im->clone();
                $imgmid->thumbnailImage(500, null);
                $imgmid->writeImage(APPLICATION_PATH . '/../market/guest/' . Zend_Session::getId() . '/mid_' . $id . '_' . $filename);
                $imgthumb = $im->clone();
                $imgthumb->thumbnailImage(null, 110);
                $imgthumb->writeImage(APPLICATION_PATH . '/../market/guest/' . Zend_Session::getId() . '/thumb_' . $id . '_' . $filename);

                $images = new Zend_Session_Namespace('amfmotive');

                $images->unlock();

                if (!$images->motive) {
                    $images->motive = array();
                }

                $uuid = TP_Util::uuid();
                $images->motive[$uuid] = array(
                    'file_orginal' => Zend_Session::getId() . '/' . $id . '_' . $filename,
                    'file_work' => Zend_Session::getId() . '/work_' . $id . '_' . $filename,
                    'file_mid' => Zend_Session::getId() . '/mid_' . $id . '_' . $filename,
                    'file_thumb' => Zend_Session::getId() . '/thumb_' . $id . '_' . $filename,
                    'width' => $im->getImageWidth(),
                    'height' => $im->getImageHeight(),
                    'mid_width' => $imgmid->getImageWidth(),
                    'mid_height' => $imgmid->getImageHeight(),
                    'copyright' => $im->getImageProperty('exif:Copyright'),
                    'dpi_org' => $res['x'],
                    'dpi_con' => '300',
                    'typ' => $im->getImageType(),
                    'title' => $path_parts['filename'],
                    'uuid' => $uuid,
                    'org' => $orginalFilename,
                    'orgfilename' => $orginalFilename
                );

                $motive = $images->motive[$uuid];

                $motivBasket = new TP_FavMotiv();
                $motivBasket->addMotivUUID($uuid);

                $images->lock();

            }

        }

    }

    public function fontsAction() {
        echo '<fontList>
	<font font_path="/designer/fonts/ArialLoader.swf" font_name="Arial" />
	<font font_path="/designer/fonts/ComicLoader.swf" font_name="Comic Sans MS" />
        <font font_path="/designer/fonts/GeorgiaLoader.swf" font_name="Georgia" />
        <font font_path="/designer/fonts/CollegeLoader.swf" font_name="College" />
        <font font_path="/designer/fonts/CollegeLoader.swf" font_name="College Condensed" />
        <font font_path="/designer/fonts/CollegeLoader.swf" font_name="College Semi condensed" />
        <font font_path="/designer/fonts/CollegeLoader.swf" font_name="ChopinScript" />
        <font font_path="/designer/fonts/CollegeLoader.swf" font_name="Old English Text MT" />
        <font font_path="/designer/fonts/CollegeLoader.swf" font_name="Sick Capital Vice" />
        <font font_path="/designer/fonts/CollegeLoader.swf" font_name="Square 721 Bold Extended BT" />
        </fontList>';
    }

    public function readprinttechnoloyAction() {

        $rows = Doctrine_Query::create()->from('Article a')
            ->where('a.uuid = ?', array($this->_getParam('id')))->fetchOne();

        $content = '<printing>';

        $pt = $this->shop->Install->a9_pt;
        try {

            $pt = new SimpleXMLElement($pt, null, false);
        } catch (Exception $e) {
            return;
        }

        foreach (explode(',', $rows->a9_pt) as $key) {
            $key = trim($key);
            if ($key != "") {

                $itm = $pt->xpath('//printing/itm[@id="' . $key . '"]');

                $content .= $itm[0]->asXml();
            }
        }

        echo $content . '</printing>';
    }

    public function readusermotiveAction() {

        $content = '<pro uid="1106231396">';

        if (Zend_Auth::getInstance()->hasIdentity()) {

            $user = Zend_Auth::getInstance()->getIdentity();
            $user = Doctrine_Query::create()
                ->from('Motiv m')
                ->leftJoin('m.ContactMotiv as v')
                ->where('v.contact_id = ?')->execute(array($user['id']));

            if ($user != false) {
                $guestMotive = $user->toArray(false);
            } else {
                $guestMotive = array();
            }
        } else {

            $images = new Zend_Session_Namespace('amfmotive');

            $images->unlock();

            if (!$images->motive) {
                $images->motive = array();
            }

            $motivBasket = new TP_FavMotiv();

            $motiv = Doctrine_Query::create()
                ->from('Motiv c')
                ->where('c.id = ' . implode(' OR c.id = ', array_values($motivBasket->getMotive())))->limit(10)
                ->execute();

            $guestMotive = array_merge($images->motive, $motiv->toArray(false));
        }

        foreach ($guestMotive as $motiv) {
            $content .= '<clip path="' . $motiv['uuid'] . '" clip_ext="jpg" />';
        }

        $content .= '</pro>';
        echo $content;
    }

    public function readclipcategoryAction() {
        if ($this->_getParam('id', false)) {
            $rows = Doctrine_Query::create()->select()->from('ClipartTheme m')->where('m.install_id = ? AND m.parent_id = ?', array($this->install->id, $this->_getParam('id', false)))->execute();
        } else {
            $rows = Doctrine_Query::create()->select()->from('ClipartTheme m')->where('m.install_id = ?', array($this->install->id))->execute();
        }
        $content = '<pro>';
        foreach ($rows as $row) {
            $content .= '<itm proname="' . $row->title . '" proid="' . $row->id . '" />';
        }
        echo $content . '</pro>';
    }

    public function checkonloadAction() {

        $contentFake = '<clip side="front" type="img" col_type="0" clip_cost="0" clip_position="0,0,0,0,undefined,undefined" path_str="/service/designer/loadclipartfile?id=3&part=1,/service/designer/loadclipartfile?id=3&part=2," xval="099.5" yval="086" rotate="0" scalex="0" scaley="0" fntsize="16" fntface="N" fntbld="N" fntalgn="N" fntitl="N" fntund="N"  part="2" col="0x141754,0xFFD202" colname="undefined" texture=",," textstyle="N" radius="N" spacing="N" whrem="undefined" whrange="undefined" mh="undefined" mv="undefined" />';

        header('Content-type: text/xml');
        if ($this->_getParam('design_from') == 'BASKET') {

            $basket = new TP_Basket ();

            $sessart = $basket->getTempProduct();
            $article = Doctrine_Query::create()->from('Article c')->where('c.uuid = ?', array($sessart->getArticleId()))->fetchOne();

            $sides = 0;
            if ($article->a9_front1 != "") {
                $sides += 1;
            }
            if ($article->a9_back1 != "") {
                $sides += 1;
            }
            if ($article->a9_left1 != "") {
                $sides += 1;
            }
            if ($article->a9_right1 != "") {
                $sides += 1;
            }

            $multiColor = 'N';
            if ($article->a9_front2 != "") {
                $multiColor = 'Y';
            }

            $data = $sessart->getDesignerData();

            $content = '<design farea_pcent="" barea_pcent="" larea_pcent="" rarea_pcent="" colo="' . $data['colors'] . '" prosubcatid="' . $article->uuid . '" prosize="' . $data['psize'] . '" proqty="' . $sessart->getCount() . '" proname="' . $article->title . '" proid="' . $article->uuid . '" prodir="' . $article->uuid . '" procol="cap" sides="' . $sides . '" multicolor="' . $multiColor . '" front_area="' . $article->a9_front_area . '" back_area="' . $article->a9_back_area . '" left_area="' . $article->a9_left_area . '" right_area="' . $article->a9_right_area . '" cost="' . $article->preis . '">';
            //$content .= $contentFake;
            $content .= $data['design_config'];
            echo $content . '</design>';
            return;

        } else {
            $shop = Zend_Registry::get('shop');

            if (intval($this->_getParam('pro_sku')) == 0) {
                throw new Zend_Amf_Exception;
            }
            $copy = $this->_getParam('copy');
            if (Zend_Auth::getInstance()->hasIdentity() && (!isset($copy) || $copy != 2)) {

                try {

                    $article = Doctrine_Query::create()
                        ->from('Article c')
                        ->where('c.uuid = ?', array($this->_getParam('pro_sku')))
                        ->fetchOne();

                    if ($article == false) {

                    }
                } catch (Exception $e) {
                    Zend_Registry::get('logger')->CRIT($e->getTraceAsString());
                }

                $basket = new TP_Basket ();

                $sessart = $basket->getTempProduct();

                $sides = 0;
                if ($article->a9_front1 != "") {
                    $sides += 1;
                }
                if ($article->a9_back1 != "") {
                    $sides += 1;
                }
                if ($article->a9_left1 != "") {
                    $sides += 1;
                }
                if ($article->a9_right1 != "") {
                    $sides += 1;
                }

                $multiColor = 'N';
                if ($article->a9_front2 != "") {
                    $multiColor = 'Y';
                }

                if ($article->mercendise_design != "") {
                    $data = Zend_Json::decode($article->mercendise_design);

                    $content = '<design farea_pcent="" barea_pcent="" larea_pcent="" rarea_pcent="" colo="' . $data['colors'] . '" prosubcatid="' . $article->uuid . '" prosize="' . $data['psize'] . '" proqty="' . $sessart->getCount() . '" proname="' . $article->title . '" proid="' . $article->uuid . '" prodir="' . $article->uuid . '" procol="cap" sides="' . $sides . '" multicolor="' . $multiColor . '" front_area="' . $article->a9_front_area . '" back_area="' . $article->a9_back_area . '" left_area="' . $article->a9_left_area . '" right_area="' . $article->a9_right_area . '" cost="' . $article->preis . '">';
                    //$content .= $contentFake;
                    $content .= $data['design_config'];

                    echo $content . '</design>';
                } else {
                    echo 'NODESIGN';
                }
                return;
            } else {

                if ($copy != 3) {
                    $articleSession = new TP_Layoutersession();
                    $articleSess = $articleSession->getLayouterArticle($this->_getParam('pro_sku'));

                    try {

                        $article = Doctrine_Query::create()
                            ->from('Article c')
                            ->where('c.uuid = ?', array($articleSess->getOrgArticleId()))
                            ->fetchOne();

                        if ($article == false) {

                        }
                    } catch (Exception $e) {
                        Zend_Registry::get('logger')->CRIT($e->getTraceAsString());
                    }

                    $sides = 0;
                    if ($article->a9_front1 != "") {
                        $sides += 1;
                    }
                    if ($article->a9_back1 != "") {
                        $sides += 1;
                    }
                    if ($article->a9_left1 != "") {
                        $sides += 1;
                    }
                    if ($article->a9_right1 != "") {
                        $sides += 1;
                    }

                    $multiColor = 'N';
                    if ($article->a9_front2 != "") {
                        $multiColor = 'Y';
                    }

                    $data = Zend_Json::decode($articleSess->getDesignerXML());

                    $basket = new TP_Basket ();

                    $sessart = $basket->getTempProduct();

                    $content = '<design farea_pcent="" barea_pcent="" larea_pcent="" rarea_pcent="" colo="' . $data['colors'] . '" prosubcatid="' . $article->uuid . '" prosize="' . $data['psize'] . '" proqty="' . $sessart->getCount() . '" proname="' . $article->title . '" proid="' . $article->uuid . '" prodir="' . $article->uuid . '" procol="cap" sides="' . $sides . '" multicolor="' . $multiColor . '" front_area="' . $article->a9_front_area . '" back_area="' . $article->a9_back_area . '" left_area="' . $article->a9_left_area . '" right_area="' . $article->a9_right_area . '" cost="' . $article->preis . '">';
                    //$content .= $contentFake;
                    $content .= $data['design_config'];

                    if ($data['design_config'] != "") {
                        echo $content . '</design>';
                    } else {
                        echo 'NODESIGN';
                    }
                    return;
                } else {
                    try {

                        $article = Doctrine_Query::create()
                            ->from('Article c')
                            ->where('c.uuid = ?', array($this->_getParam('pro_sku')))
                            ->fetchOne();

                        if ($article == false) {

                        }
                    } catch (Exception $e) {
                        Zend_Registry::get('logger')->CRIT($e->getTraceAsString());
                    }

                    $sides = 0;
                    if ($article->a9_front1 != "") {
                        $sides += 1;
                    }
                    if ($article->a9_back1 != "") {
                        $sides += 1;
                    }
                    if ($article->a9_left1 != "") {
                        $sides += 1;
                    }
                    if ($article->a9_right1 != "") {
                        $sides += 1;
                    }

                    $multiColor = 'N';
                    if ($article->a9_front2 != "") {
                        $multiColor = 'Y';
                    }

                    $basket = new TP_Basket ();

                    $sessart = $basket->getTempProduct();

                    if ($article->mercendise_design != "") {
                        $data = Zend_Json::decode($article->mercendise_design);

                        $content = '<design farea_pcent="" barea_pcent="" larea_pcent="" rarea_pcent="" colo="' . $data['colors'] . '" prosubcatid="' . $article->uuid . '" prosize="' . $data['psize'] . '" proqty="' . $sessart->getCount() . '" proname="' . $article->title . '" proid="' . $article->uuid . '" prodir="' . $article->uuid . '" procol="cap" sides="' . $sides . '" multicolor="' . $multiColor . '" front_area="' . $article->a9_front_area . '" back_area="' . $article->a9_back_area . '" left_area="' . $article->a9_left_area . '" right_area="' . $article->a9_right_area . '" cost="' . $article->preis . '">';
                        //$content .= $contentFake;
                        $content .= $data['design_config'];

                        echo $content . '</design>';
                    } else {
                        echo 'NODESIGN';
                    }
                    return;
                }
            }
            echo 'NODESIGN';
        }
    }

    public function readclipartAction() {

        header('Content-type: text/xml');

        $content = '<clipart txtcost="0">';

        if ($this->_getParam('pid', false)) {
            $rows = Doctrine_Query::create()->select()->from('Clipart m')->leftJoin('m.ShopClipart c')->where('c.shop_id = ? AND m.install_id = ? AND m.parent_id = ?', array($this->shop->id, $this->install->id, $this->_getParam('pid', false)))->execute();
        } else {
            $rows = Doctrine_Query::create()->select()->from('Clipart m')->leftJoin('m.ShopClipart c')->where('c.shop_id = ? AND m.install_id = ?', array($this->shop->id, $this->install->id))->execute();
        }
        foreach ($rows as $row) {

            $parts = 1;
            if ($row->clip_file2 != "") {
                $parts += 1;
            }

            $ext = 'swf';

            $image = Doctrine_Query::create()
                ->from('Image as i')
                ->where('i.id = ?', array($row->clip_file1))->fetchOne();

            $path_parts = pathinfo($image->path);

            $content .= '<clip id="' . $row->id . '" pid="' . $row->parent_id . '" name="' . $row->title . '" path="clipart/admin/1/" layers="" parts="' . $parts . '" clip_price="' . $row->clip_price . '" slice_position="' . $row->clip_slice . '" colors="' . $row->clip_colors . '" clip_ext="' . $path_parts['extension'] . '" />';

        }

        echo $content . '</clipart>';
        return;

        echo '<clipart txtcost="5.00">
            <clip id="1" pid="1" name="clip01" path="clipart/admin/1/" layers="" parts="1" clip_price="1.11" slice_position="12||12||0||0||0||0" colors="0x512D0A||undefined" clip_ext="swf" />
            <clip id="2" pid="2" name="clip02" path="clipart/admin/2/" layers="" parts="2" clip_price="2.26" slice_position="0||0||47||16||1||1" colors="0x000000||0xFF0084||0xFF0084" clip_ext="swf" />
            <clip id="7" pid="6" name="question" path="clipart/admin/8/" layers="" parts="2" clip_price="3.33" slice_position="0||0||21||9||0||0" colors="0x000000||0xFFFFFF||undefined" clip_ext="swf" />
            <clip id="8" pid="7" name="signal" path="clipart/admin/9/" layers="" parts="3" clip_price="4.50" slice_position="0||0||15||17||31||36" colors="0xD31818||0xFFFF00||0x000000" clip_ext="swf" />
            <clip id="9" pid="8" name="signal1" path="clipart/admin/10/" layers="" parts="3" clip_price="4.20" slice_position="0||0||15||17||31||36" colors="0x551B76||0xF36916||0xFFFFFF" clip_ext="swf" />
            <clip id="10" pid="9" name="shoot" path="clipart/admin/11/" layers="" parts="2" clip_price="2.22" slice_position="0||0||23||23||0||0" colors="0x0091D3||0xD31818||undefined" clip_ext="swf" />
            <clip id="11" pid="9" name="star" path="clipart/admin/12/" layers="" parts="2" clip_price="2.30" slice_position="17||16||0||0||0||0" colors="0xD31818||0xFFD202||undefined" clip_ext="swf" />
            <clip id="21" pid="1" name="ass" path="clipart/admin/20/ico.png" layers="" parts="1" clip_price="2.90" slice_position="0||0||0||0||0||0" colors="0xFFFFFF" clip_ext="swf" />
            <clip id="27" pid="12" name="LC" path="clipart/admin/28" layers="" parts="1" clip_price="2.00" slice_position="0||0||0||0||0||0" colors="||undefined" clip_ext="swf" />
            </clipart>';
    }

    public function readprocategoryAction() {

        $articlegroupsd = Doctrine_Query::create()->from('ArticleGroup c')->leftJoin('c.Article a')->where('a.typ = 9 AND c.shop_id = ? AND c.notinmenu = 0 AND c.enable = 1 AND c.parent=0 AND (language = ? OR language = \'all\')', array(
            $this->view->shop->id,
            Zend_Registry::get('locale')))->orderBy('c.pos ASC')->execute();
        $content = '<pro>';

        foreach ($articlegroupsd as $articlegroup) {

            $content .= '<itm proname="' . $articlegroup->title . '" proid="' . $articlegroup->id . '" />';
        }

        $content .= '</pro>';
        echo $content;
    }

    public function readprosubcategoryAction() {

        if ($this->_getParam('parent_id', 0) == 0) {

            if ($this->_getParam('id') == '-1') {
                $rows = Doctrine_Query::create()->from('Article a')
                    ->leftJoin('a.ArticleGroupArticle c')
                    ->where('a.shop_id = ? AND a.private = 0 AND a.enable = 1 AND a.not_edit = 0 AND a.typ = 9', array($this->shop->id))->orderBy('a.pos ASC')->execute();

            } else {
                $rows = Doctrine_Query::create()->from('Article a')
                    ->leftJoin('a.ArticleGroupArticle c')
                    ->where('a.shop_id = ? AND a.private = 0 AND a.enable = 1 AND a.not_edit = 0 AND a.typ = 9 AND c.articlegroup_id = ?', array($this->shop->id, $this->_getParam('id')))->orderBy('a.pos ASC')->execute();

            }
        }
        require_once(__DIR__ . '/../../../helpers/Text.php');
        $helper = new TP_View_Helper_Text();
        $content = '<pro>';

        foreach ($rows as $article) {

            $sides = 0;
            if ($article->a9_front1 != "") {
                $sides += 1;
            }
            if ($article->a9_back1 != "") {
                $sides += 1;
            }
            if ($article->a9_left1 != "") {
                $sides += 1;
            }
            if ($article->a9_right1 != "") {
                $sides += 1;
            }

            $multiColor = 'N';
            if ($article->a9_front2 != "") {
                $multiColor = 'Y';
            }

            $content .= '<itm proname="' . $helper->truncate_text($article->title, 16) . '" proid="' . $article->uuid . '" prodir="' . $article->uuid . '" sides="' . $sides . '" multicol="' . $multiColor . '" defaultcol="0" front_area="' . $article->a9_front_area . '" back_area="' . $article->a9_back_area . '" left_area="' . $article->a9_left_area . '" right_area="' . $article->a9_right_area . '" price="' . $article->preis . '" />';
        }

        $content .= '</pro>';
        echo $content;
    }

    public function getarticleinfoAction() {

        $article = Doctrine_Query::create()->from('Article a')
            ->where('a.uuid = ?', array($this->_getParam('sku')))->fetchOne();

        if ($article->a6_org_article > 0) {
            $article = $article->OrgArticle;
        }
        $sizes = array();
        $colors = array();

        $xml = simplexml_load_string($article->a9_sizes);

        foreach ($xml as $el) {
            $sizes[] = (string)$el['size'];
        }

        $xml = simplexml_load_string($article->a9_colors);

        foreach ($xml as $el) {
            $colors[] = (string)$el['colorvalue'];
        }

        $content = '<pro>
            <itm proname="' . $article->title . '" 
            propath="' . $article->uuid . '" 
            prodesc="' . $article->einleitung . '" 
            procolor="' . implode('*', $colors) . '" 
            sizes="' . implode(', ', $sizes) . '"></itm>
        </pro>';

        echo $content;
    }

    public function readprosubcategorylistAction() {

        $rows = Doctrine_Query::create()->from('Article a')
            ->where('a.uuid = ?', array($this->_getParam('id')))->fetchOne();

        if ($rows->a6_org_article > 0) {
            $rows = $rows->OrgArticle;
        }
        echo $rows->a9_colors;
    }

    public function readproductsizeAction() {
        $rows = Doctrine_Query::create()->from('Article a')
            ->where('a.uuid = ?', array($this->_getParam('id')))->fetchOne();

        if ($rows->a6_org_article > 0) {
            $rows = $rows->OrgArticle;
        }
        echo $rows->a9_sizes;
    }

    public function getlangAction() {
        echo '<pro><itm alias="design_label" labelname="Design" /><itm alias="print_label" labelname="Drucken" /><itm alias="products_tab_label" labelname="Produkte" /><itm alias="clips_tab_label" labelname="Icons" /><itm alias="text_tab_label" labelname="Text" /><itm alias="myart_tab_label" labelname="Motive" /><itm alias="add_new_text_label" labelname="Text einfügen" /><itm alias="loading_label" labelname="Lade..." /><itm alias="unit_label" labelname="Einheit" /><itm alias="total_price_label" labelname="Preis" /><itm alias="save_as_label" labelname="Save As" /><itm alias="popular_design_label" labelname="Top Design" /><itm alias="all_subcategory_label" labelname="Alle" /><itm alias="custom_clipart" labelname="Hochladen" /><itm alias="adding_cart_msg" labelname="Bitte warten ..." /><itm alias="size_label" labelname="Größe" /><itm alias="quantity_label" labelname="Anzahl" /><itm alias="add_to_cart_label" labelname="Bestellen" /><itm alias="save_design_label" labelname="Save design" /><itm alias="purchase_now_label" labelname="Purchase Now" /><itm alias="loading_label" labelname="Lade..." /><itm alias="white_removal_title" labelname="Your Art View" /><itm alias="remove_white_label" labelname="Remove White" /><itm alias="ok_label" labelname="Ok" /><itm alias="upload_complete_msg" labelname="Uploading Completed" /><itm alias="upload_failed_msg" labelname="Upload dialog failed to open" /><itm alias="select_cancel_msg" labelname="Select Canceled" /><itm alias="opening_msg" labelname="Opening" /><itm alias="progress_msg" labelname="Progress" /><itm alias="http_error_msg" labelname="HTTP Error while loading" /><itm alias="io_error_msg" labelname="IO Error while loading" /><itm alias="security_error_msg" labelname="Security Error while loading" /><itm alias="print_quality_error_msg" labelname="Your image printing quality may low. Please make it smaller" /><itm alias="more_color_label" labelname="More Color" /><itm alias="less_color_label" labelname="Less Color" /><itm alias="login_info_title" labelname="Save design" /><itm alias="login_info_msg" labelname="Please login/register for save your design." /><itm alias="login_info_login_btn" labelname="Login/Register" /><itm alias="login_info_cancel_btn" labelname="Cancel" /><itm alias="saved_info_msg" labelname="Your design has saved. You can browse your saved design now." /><itm alias="saved_location_btn" labelname="See My Designs" /><itm alias="saved_cancel_btn" labelname="Cancel" /><itm alias="currency_label" labelname="€" /><itm alias="currency1_label" labelname="€" /></pro>';
    }

    public function readusermotiveresizeAction() {

        $images = new Zend_Session_Namespace('amfmotive');
        $db = true;
        if (!isset($images->motive[$this->_getParam('id')])) {
            $row = Doctrine_Query::create()->from('Motiv a')->where('a.uuid = ?', array($this->_getParam('id')))->fetchOne();
        } else {
            $row = new stdClass();
            $row->file_thumb = $images->motive[$this->_getParam('id')]['file_thumb'];
            $db = false;
        }

        $shop = $this->shop->toArray();

        header("Content-Type: image/png");

        if (!file_exists(PUBLIC_PATH . '/temp/thumb/' . $shop['uid'] . '/')) {
            mkdir(PUBLIC_PATH . '/temp/thumb/' . $shop['uid'] . '/', 0777, true);
        }

        if (!file_exists(PUBLIC_PATH . '/temp/thumb/' . $shop['uid'] . '/' . md5($row->file_thumb) . '_50.png')) {
            if ($db) {
                $im = new imagick(APPLICATION_PATH . '/../market/motive/' . $row->file_thumb);
            } else {
                $im = new imagick(APPLICATION_PATH . '/../market/guest/' . $row->file_thumb);
            }
            $sizes = $im->getImageGeometry();
            if ($sizes['width'] >= $sizes['height']) {
                $im->thumbnailImage(50, null);
            } else {
                $im->thumbnailImage(null, 50);
            }
            $im->writeImage(PUBLIC_PATH . '/temp/thumb/' . $shop['uid'] . '/' . md5($row->file_thumb) . '_50.png');
            header('Location: ' . $this->view->basepath . '/temp/thumb/' . $shop['uid'] . '/' . md5($row->file_thumb) . '_50.png');
        } else {

            header('Location: ' . $this->view->basepath . '/temp/thumb/' . $shop['uid'] . '/' . md5($row->file_thumb) . '_50.png');
        }

    }

    public function readusermotivebigAction() {
        $images = new Zend_Session_Namespace('amfmotive');
        $db = true;
        if (!isset($images->motive[$this->_getParam('id')])) {
            $row = Doctrine_Query::create()->from('Motiv a')->where('a.uuid = ?', array($this->_getParam('id')))->fetchOne();
        } else {
            $row = new stdClass();
            $row->file_thumb = $images->motive[$this->_getParam('id')]['file_thumb'];
            $row->file_work = $images->motive[$this->_getParam('id')]['file_work'];
            $db = false;
        }

        $shop = $this->shop->toArray();

        header("Content-Type: image/png");

        if (!file_exists(PUBLIC_PATH . '/temp/thumb/' . $shop['uid'] . '/')) {
            mkdir(PUBLIC_PATH . '/temp/thumb/' . $shop['uid'] . '/', 0777, true);
        }

        if (!file_exists(PUBLIC_PATH . '/temp/thumb/' . $shop['uid'] . '/' . md5($row->file_work) . '_250.png')) {
            if ($db) {
                $im = new imagick(APPLICATION_PATH . '/../market/motive/' . $row->file_work);
            } else {
                $im = new imagick(APPLICATION_PATH . '/../market/guest/' . $row->file_work);
            }
            $sizes = $im->getImageGeometry();
            if ($sizes['width'] >= $sizes['height']) {
                $im->thumbnailImage(250, null);
            } else {
                $im->thumbnailImage(null, 250);
            }
            $im->writeImage(PUBLIC_PATH . '/temp/thumb/' . $shop['uid'] . '/' . md5($row->file_work) . '_250.png');
            header('Location: ' . $this->view->basepath . '/temp/thumb/' . $shop['uid'] . '/' . md5($row->file_work) . '_250.png');
        } else {

            header('Location: ' . $this->view->basepath . '/temp/thumb/' . $shop['uid'] . '/' . md5($row->file_work) . '_250.png');
        }
    }

    public function buyAction() {
        $basket = new TP_Basket ();

        $dgm = preg_replace_callback('/(str=")(.+)(" xval)/U', function ($m) {
            return $m[1] . str_replace('"', '&quot;', $m[2]) . $m[3];
        }, $this->_getParam('dgn_xml'));

        $sessart = $basket->getTempProduct();
        $sessart->setDesignerData(
            array(
                'colors' => $this->_getParam('procol'),
                'design_config' => $dgm
            )
        );

        $shop = Zend_Registry::get('shop');

        $uuid = "";
        $highRole = null;

        $copy = $this->_getParam('cp');
        if (Zend_Auth::getInstance()->hasIdentity() && (!isset($copy) || (isset($copy) && ($copy != 3 && $copy != 2)))) {

            $article = Doctrine_Query::create()
                ->from('Article c')
                ->where('c.uuid = ?', array($this->_getParam('subcatid')))
                ->fetchOne();

            if ($article == false) {
                throw new Zend_Amf_Exception;
            }

            $user = Zend_Auth::getInstance()->getIdentity();
            $mode = new Zend_Session_Namespace('adminmode');
            if ($mode->over_ride_contact != false && $mode->over_ride_contact != 'new') {
                $user = Doctrine_Query::create()
                    ->from('Contact m')
                    ->where('id = ?')->fetchOne(array($mode->over_ride_contact));
            } else {
                $user = Doctrine_Query::create()
                    ->from('Contact m')
                    ->where('id = ?')->fetchOne(array($user['id']));
            }
            if ($user != false) {
                foreach ($user->Roles as $rol) {
                    if ($highRole == null || $rol->level > $highRole->level) {
                        $highRole = $rol;
                    }
                }
            }
            if ($article->contact_id != $user->id && $highRole->level < 40) {

                $orgid = $article->id;

                $article = $article->copy();
                $article->uuid = "";
                $article->contact_id = $user->id;
                $article->private = true;
                $article->a6_org_article = $orgid;
                $article->mercendise_design = Zend_Json::encode(array(
                    'colors' => $this->_getParam('procol'),
                    'design_config' => $dgm,
                    'psize' => $this->_getParam('psize')
                ));
                $article->save();

                $rel = new ContactArticle();
                $rel->article_id = $article->id;
                $rel->contact_id = $user->id;
                $rel->save();

                $uuid = $article->uuid;
            } else {
                $article->mercendise_design = Zend_Json::encode(array(
                    'colors' => $this->_getParam('procol'),
                    'design_config' => $dgm,
                    'psize' => $this->_getParam('psize')
                ));
                $uuid = $article->uuid;
                $article->save();
            }

        } else {

            $articleSession = new TP_Layoutersession();
            $articleSess = $articleSession->getLayouterArticle($this->_getParam('uuid'));
            $articleSess->setOrgArticleId($this->_getParam('subcatid'));
            $articleSess->setDesignerXML(Zend_Json::encode(array(
                'colors' => $this->_getParam('procol'),
                'design_config' => $dgm,
                'psize' => $this->_getParam('psize')
            )));

            $article = Doctrine_Query::create()
                ->from('Article c')
                ->where('c.uuid = ?', array($articleSess->getOrgArticleId()))
                ->fetchOne();

            $articleSess->setTitle($article->title);

            $articleSess->setIsEdit();
            $uuid = $articleSess->getArticleId();
        }

        echo 'uuid=' . $uuid . '';
    }

    public function pixelsAction() {

        $w = (int)$this->_getParam('width');
        $h = (int)$this->_getParam('height');

        // create the image with desired width and height

        $img = imagecreatetruecolor($w, $h);

        // now fill the image with blank color
        // do you remember i wont pass the 0xFFFFFF pixels
        // from flash?
        imagefill($img, 0, 0, 0xFFFFFF);

        $rows = 0;
        $cols = 0;

        // now process every POST variable which
        // contains a pixel color
        for ($rows = 0; $rows < $h; $rows++) {
            // convert the string into an array of n elements
            $c_row = explode(",", $this->_getParam('px' . $rows));
            for ($cols = 0; $cols < $w; $cols++) {
                // get the single pixel color value
                $value = $c_row[$cols];
                // if value is not empty (empty values are the blank pixels)
                if ($value != "") {
                    // get the hexadecimal string (must be 6 chars length)
                    // so add the missing chars if needed
                    $hex = $value;
                    while (strlen($hex) < 6) {
                        $hex = "0" . $hex;
                    }
                    // convert value from HEX to RGB
                    $r = hexdec(substr($hex, 0, 2));
                    $g = hexdec(substr($hex, 2, 2));
                    $b = hexdec(substr($hex, 4, 2));
                    // allocate the new color
                    // N.B. teorically if a color was already allocated
                    // we dont need to allocate another time
                    // but this is only an example
                    $test = imagecolorallocate($img, $r, $g, $b);
                    // and paste that color into the image
                    // at the correct position
                    imagesetpixel($img, $cols, $rows, $test);
                }
            }
        }
        mkdir('uploads/' . $this->shop->uid . '/designer', 0777, true);
        $basket = new TP_Basket();

        if ($this->_getParam('cp') == 0) {
            $copy = $this->_getParam('cp');
            if (Zend_Auth::getInstance()->hasIdentity() && (!isset($copy) || (isset($copy) && ($copy != 3 && $copy != 2)))) {

                $article = Doctrine_Query::create()
                    ->from('Article c')
                    ->where('c.uuid = ?', array($this->_getParam('uuid')))
                    ->fetchOne();

                if ($article == false) {
                    echo 'SUCCESS';
                    return;
                }

                $user = Zend_Auth::getInstance()->getIdentity();
                $mode = new Zend_Session_Namespace('adminmode');
                if ($mode->over_ride_contact != false && $mode->over_ride_contact != 'new') {
                    $user = Doctrine_Query::create()
                        ->from('Contact m')
                        ->where('id = ?')->fetchOne(array($mode->over_ride_contact));
                } else {
                    $user = Doctrine_Query::create()
                        ->from('Contact m')
                        ->where('id = ?')->fetchOne(array($user['id']));
                }
                if ($user != false) {
                    foreach ($user->Roles as $rol) {
                        if ($highRole == null || $rol->level > $highRole->level) {
                            $highRole = $rol;
                        }
                    }
                }
                if ($highRole->level >= 40 || $article->contact_id == $user->id) {
                    $article->mercendise_thumbnail = 'uploads/' . $this->shop->uid . '/designer/' . $article->id . '.jpg';
                    imagejpeg($img, 'uploads/' . $this->shop->uid . '/designer/' . $article->id . '.jpg');
                    $article->save();
                    $sessart = $basket->getTempProduct();
                    $sessart->setDesignerFile('uploads/' . $this->shop->uid . '/designer/' . $article->id . '.jpg');
                }

            }

        } else {

            imagejpeg($img, 'uploads/' . $this->shop->uid . '/designer/' . time() . '.jpg');

            $articleSession = new TP_Layoutersession();
            $articleSess = $articleSession->getLayouterArticle($this->_getParam('uuid'));
            $articleSess->setPreviewPath('uploads/' . $this->shop->uid . '/designer/' . time() . '.jpg');

            $sessart = $basket->getTempProduct();
            $sessart->setDesignerFile('uploads/' . $this->shop->uid . '/designer/' . time() . '.jpg');
        }
        echo 'SUCCESS';
    }

    public function getresizeAction() {

        $row = Doctrine_Query::create()->from('Article a')->where('a.uuid = ?', array($this->_getParam('id')))->fetchOne();

        $colorid = 1;
        if ($this->_getParam('colorid', false)) {
            $colorid = $this->_getParam('colorid');
        }

        if ($this->_getParam('path') == '1') {
            $image = Doctrine_Query::create()
                ->from('Image as i')
                ->where('i.id = ?')->fetchOne(array($row['a9_front' . $colorid]));
        } elseif ($this->_getParam('path') == '2') {
            $image = Doctrine_Query::create()
                ->from('Image as i')
                ->where('i.id = ?')->fetchOne(array($row['a9_back' . $colorid]));
        } elseif ($this->_getParam('path') == '3') {
            $image = Doctrine_Query::create()
                ->from('Image as i')
                ->where('i.id = ?')->fetchOne(array($row['a9_left' . $colorid]));
        } elseif ($this->_getParam('path') == '4') {
            $image = Doctrine_Query::create()
                ->from('Image as i')
                ->where('i.id = ?')->fetchOne(array($row['a9_right' . $colorid]));
        }
        header("Content-Type: image/png");

        if (!file_exists(PUBLIC_PATH . '/temp/designer')) {
            mkdir(PUBLIC_PATH . '/temp/designer', 0777, true);
        }

        if (!file_exists(PUBLIC_PATH . '/temp/designer/' . md5($image->path) . '_40.png')) {
            $im = new imagick($image->path);
            $sizes = $im->getImageGeometry();
            if ($sizes['width'] >= $sizes['height']) {
                $im->thumbnailImage(40, null);
            } else {
                $im->thumbnailImage(null, 40);
            }
            $im->writeImage(PUBLIC_PATH . '/temp/designer/' . md5($image->path) . '_40.png');
            header('Location: ' . $this->view->basepath . '/temp/designer/' . md5($image->path) . '_40.png');
        } else {

            header('Location: ' . $this->view->basepath . '/temp/designer/' . md5($image->path) . '_40.png');
        }
    }

    public function getresizebigAction() {

        $row = Doctrine_Query::create()->from('Article a')->where('a.uuid = ?', array($this->_getParam('id')))->fetchOne();

        $colorid = 1;
        if ($this->_getParam('colorid', false)) {
            $colorid = $this->_getParam('colorid');
        }

        if ($this->_getParam('path') == '1') {
            $image = Doctrine_Query::create()
                ->from('Image as i')
                ->where('i.id = ?')->fetchOne(array($row['a9_front' . $colorid]));
        } elseif ($this->_getParam('path') == '2') {
            $image = Doctrine_Query::create()
                ->from('Image as i')
                ->where('i.id = ?')->fetchOne(array($row['a9_back' . $colorid]));
        } elseif ($this->_getParam('path') == '3') {
            $image = Doctrine_Query::create()
                ->from('Image as i')
                ->where('i.id = ?')->fetchOne(array($row['a9_left' . $colorid]));
        } elseif ($this->_getParam('path') == '4') {
            $image = Doctrine_Query::create()
                ->from('Image as i')
                ->where('i.id = ?')->fetchOne(array($row['a9_right' . $colorid]));
        }

        header("Content-Type: image/png");

        if (!file_exists(PUBLIC_PATH . '/temp/designer')) {
            mkdir(PUBLIC_PATH . '/temp/designer', 0777, true);
        }

        if (!file_exists(PUBLIC_PATH . '/temp/designer/' . md5($image->path) . '_50.png')) {
            $im = new imagick($image->path);
            $sizes = $im->getImageGeometry();
            if ($sizes['width'] >= $sizes['height']) {
                $im->thumbnailImage(500, null);
            } else {
                $im->thumbnailImage(null, 500);
            }
            $im->writeImage(PUBLIC_PATH . '/temp/designer/' . md5($image->path) . '_50.png');
            header('Location: ' . $this->view->basepath . '/temp/designer/' . md5($image->path) . '_50.png');
        } else {

            header('Location: ' . $this->view->basepath . '/temp/designer/' . md5($image->path) . '_50.png');
        }
    }

    public function loadclipartfileAction() {

        $row = Doctrine_Query::create()->from('Clipart a')->where('a.id = ?', array($this->_getParam('id')))->fetchOne();

        $part = 'clip_file' . $this->_getParam('part');

        $image = Doctrine_Query::create()
            ->from('Image as i')
            ->where('i.id = ?', array($row->$part))->fetchOne();

        header("Content-Type: " . $image->imagetype);

        readfile($image->path);
    }

    public function loadcliparticonAction() {

        $row = Doctrine_Query::create()->from('Clipart a')->where('a.id = ?', array($this->_getParam('id')))->fetchOne();

        $image = Doctrine_Query::create()
            ->from('Image as i')
            ->where('i.id = ?', array($row->clip_preview))->fetchOne();

        header("Content-Type: image/png");

        if (!file_exists(PUBLIC_PATH . '/temp/designer/' . md5($image->path) . '_clip.png')) {
            $im = new imagick($image->path);
            $sizes = $im->getImageGeometry();
            if ($sizes['width'] >= $sizes['height']) {
                $im->thumbnailImage(57, null);
            } else {
                $im->thumbnailImage(null, 57);
            }
            $im->writeImage(PUBLIC_PATH . '/temp/designer/' . md5($image->path) . '_clip.png');
            header('Location: ' . $this->view->basepath . '/temp/designer/' . md5($image->path) . '_clip.png');
        } else {
            header('Location: ' . $this->view->basepath . '/temp/designer/' . md5($image->path) . '_clip.png');
        }
        die();
    }

    public function getresizemiddleAction() {

        $row = Doctrine_Query::create()->from('Article a')->where('a.uuid = ?', array($this->_getParam('id')))->fetchOne();

        $colorid = 1;
        if ($row->mercendise_thumbnail != "" && file_exists($row->mercendise_thumbnail)) {
            $image = new Image();
            $image->path = $row->mercendise_thumbnail;
        } else {
            if ($this->_getParam('colorid', false)) {
                $colorid = $this->_getParam('colorid');
            }

            if ($this->_getParam('path') == '1') {
                $image = Doctrine_Query::create()
                    ->from('Image as i')
                    ->where('i.id = ?')->fetchOne(array($row['a9_front' . $colorid]));
            } elseif ($this->_getParam('path') == '2') {
                $image = Doctrine_Query::create()
                    ->from('Image as i')
                    ->where('i.id = ?')->fetchOne(array($row['a9_back' . $colorid]));
            } elseif ($this->_getParam('path') == '3') {
                $image = Doctrine_Query::create()
                    ->from('Image as i')
                    ->where('i.id = ?')->fetchOne(array($row['a9_left' . $colorid]));
            } elseif ($this->_getParam('path') == '4') {
                $image = Doctrine_Query::create()
                    ->from('Image as i')
                    ->where('i.id = ?')->fetchOne(array($row['a9_right' . $colorid]));
            }
        }
        header("Content-Type: image/png");

        if (!file_exists(PUBLIC_PATH . '/temp/designer')) {
            mkdir(PUBLIC_PATH . '/temp/designer', 0777, true);
        }

        if (!file_exists(PUBLIC_PATH . '/temp/designer/' . md5($image->path) . '_75.png')) {
            $im = new imagick($image->path);
            $sizes = $im->getImageGeometry();
            if ($sizes['width'] >= $sizes['height']) {
                $im->thumbnailImage(75, null);
            } else {
                $im->thumbnailImage(null, 75);
            }
            $im->writeImage(PUBLIC_PATH . '/temp/designer/' . md5($image->path) . '_75.png');
            header('Location: ' . $this->view->basepath . '/temp/designer/' . md5($image->path) . '_75.png');

        } else {

            header('Location: ' . $this->view->basepath . '/temp/designer/' . md5($image->path) . '_75.png');

        }
    }

}

