<?php

class Service_IndexController extends TP_Admin_Controller_Action
{

    public function init() {
        parent::init();
        $this->getHelper('layout')->disableLayout();
        $this->_helper->viewRenderer->setNoRender();
    }

    protected function contactsAction() {

        if ($this->highRole->level >= 30) {
            $rows = Doctrine_Query::create()->select()->from('Contact c')->leftJoin('c.Shops s')->where('(c.self_lastname LIKE ? OR c.self_firstname LIKE ?) AND s.id = ? AND c.id != ?', array($this->_getParam('query') . '%', $this->_getParam('query') . '%', $this->shop['id'], $this->user['id']))->orderBy('c.self_lastname ASC')->execute();

            echo "<ul>";
            foreach ($rows as $row) {
                echo "<li id=\"autocomplete_" . $row->id . "\" rel=\"" . $row->id . "\">" . $row->self_lastname . ' ' . $row->self_firstname . "</li>";
            }
            echo "</ul>";
        } else {
            $rows = Doctrine_Query::create()->select()->from('Contact c')->where('(c.self_lastname LIKE ? OR c.self_firstname LIKE ?) AND c.account_id = ? AND c.id != ?', array($this->_getParam('query') . '%', $this->_getParam('query') . '%', $this->user['account_id'], $this->user['id']))->orderBy('c.self_lastname ASC')->execute();

            echo "<ul>";
            foreach ($rows as $row) {
                echo "<li id=\"autocomplete_" . $row->id . "\" rel=\"" . $row->id . "\">" . $row->self_lastname . ' ' . $row->self_firstname . "</li>";
            }
            $this->contact($this->user['account_id']);
            echo "</ul>";
        }

        return;
    }

    protected function contact($id) {
        $rows = Doctrine_Query::create()->select()->from('Contact c')->leftJoin('c.Account a')->where('(c.self_lastname LIKE ? OR c.self_firstname LIKE ?) AND a.filiale_id = ? AND c.id != ?', array($this->_getParam('query') . '%', $this->_getParam('query') . '%', $id, $this->user['id']))->orderBy('c.self_lastname ASC')->execute();

        foreach ($rows as $row) {
            echo "<li id=\"autocomplete_" . $row->id . "\" rel=\"" . $row->id . "\">" . $row->self_lastname . ' ' . $row->self_firstname . "</li>";
            $this->contact($row->Account->id);
        }
    }

    protected function checkChildrens($table, $where, $temp, $row, $del) {

        array_push($temp, array(
            'name' => $row->id,
            'label' => $del . $row->title . ' (' . $row->id . ')'));
        $rows = Doctrine_Query::create()->from($table)->where($where, array($row->id))->execute();

        foreach ($rows as $row) {
            $temp = $this->checkChildrens($table, $where, $temp, $row, '---' . $del);
        }

        return $temp;
    }

    public function indexAction() {
        if ($this->_request->getParam('type') == 'get') {
            if ($this->_request->getParam('service') == 'articlegroup') {
                $rows = Doctrine_Query::create()->from('ArticleGroup m');
                if (intval(str_replace('S', '', $this->_request->getParam('sid'))) != 0) {
                    $rows->where('shop_id = ?', array(
                        intval(str_replace('S', '', $this->_request->getParam('sid')))));
                }
                $rows->addWhere('m.parent = 0 AND m.shop_id IN (' . implode(',', $this->shopIds) . ')');
                $rows = $rows->execute();
                $temp = array();
                array_push($temp, array(
                    'name' => '0',
                    'label' => $this->translate->translate('articlegroup_all_root')));

                foreach ($rows as $row) {
                    $temp = $this->checkChildrens('ArticleGroup m', 'm.parent = ?', $temp, $row, '');
                }
                $nodes = array(
                    'count' => count($temp),
                    'items' => $temp);
                $this->getHelper('Json')->sendJson($nodes);
            }
            if ($this->_request->getParam('service') == 'motivtheme') {
                $rows = Doctrine_Query::create()->from('MotivTheme m');
                if (intval(str_replace('S', '', $this->_request->getParam('sid'))) != 0) {
                    $rows->where('shop_id = ?', array(
                        intval(str_replace('S', '', $this->_request->getParam('sid')))));
                }
                $rows = $rows->execute();
                $temp = array();
                array_push($temp, array(
                    'name' => '0',
                    'label' => $this->translate->translate('articlegroup_all_root')));
                foreach ($rows->toArray() as $row)
                    array_push($temp, array(
                        'name' => $row['id'],
                        'label' => $row['title']));
                $nodes = array(
                    'count' => count($temp),
                    'items' => $temp);
                $this->getHelper('Json')->sendJson($nodes);
            }
            if ($this->_request->getParam('service') == 'articletheme') {
                $rows = Doctrine_Query::create()->from('ArticleTheme m');
                if (intval(str_replace('S', '', $this->_request->getParam('sid'))) != 0) {
                    $rows->where('shop_id = ?', array(
                        intval(str_replace('S', '', $this->_request->getParam('sid')))));
                }
                $rows = $rows->execute();
                $temp = array();
                array_push($temp, array(
                    'name' => '0',
                    'label' => $this->translate->translate('articlegroup_all_root')));
                foreach ($rows->toArray() as $row)
                    array_push($temp, array(
                        'name' => $row['id'],
                        'label' => $row['title']));
                $nodes = array(
                    'count' => count($temp),
                    'items' => $temp);
                $this->getHelper('Json')->sendJson($nodes);
            }
            if ($this->_request->getParam('service') == 'cliparttheme') {
                $rows = Doctrine_Query::create()->from('ClipartTheme m')
                    ->where('m.install_id = ?', array($this->install->id));
                $rows = $rows->execute();
                $temp = array();
                array_push($temp, array(
                    'name' => '0',
                    'label' => $this->translate->translate('articlegroup_all_root')));
                foreach ($rows->toArray() as $row)
                    array_push($temp, array(
                        'name' => $row['id'],
                        'label' => $row['title']));
                $nodes = array(
                    'count' => count($temp),
                    'items' => $temp);
                $this->getHelper('Json')->sendJson($nodes);
            }
            if ($this->_request->getParam('service') == 'shoptheme') {
                $rows = Doctrine_Query::create()->from('ShopTheme m');
                if (intval(str_replace('S', '', $this->_request->getParam('sid'))) != 0) {
                    $rows->where('shop_id = ?', array(
                        intval(str_replace('S', '', $this->_request->getParam('sid')))));
                }
                $rows = $rows->execute();
                $temp = array();
                array_push($temp, array(
                    'name' => '0',
                    'label' => $this->translate->translate('articlegroup_all_root')));
                foreach ($rows->toArray() as $row)
                    array_push($temp, array(
                        'name' => $row['id'],
                        'label' => $row['title']));
                $nodes = array(
                    'count' => count($temp),
                    'items' => $temp);
                $this->getHelper('Json')->sendJson($nodes);
            }
            if ($this->_request->getParam('service') == 'shops') {
                $temp = array();
                foreach ($this->user->Shops as $shop) {
                    array_push($temp, array(
                        'name' => $shop['id'],
                        'label' => $shop['name']));
                }
                $nodes = array(
                    'count' => count($temp),
                    'items' => $temp);
                $this->getHelper('Json')->sendJson($nodes);
            }
            if ($this->_request->getParam('service') == 'contactrole') {
                $temp = array();
                foreach ($this->user->ContactRole as $role) {
                    array_push($temp, array(
                        'name' => $role['role_id'],
                        'label' => $this->translate->translate($role->Role->name)));
                }
                $nodes = array(
                    'count' => count($temp),
                    'items' => $temp);
                $this->getHelper('Json')->sendJson($nodes);
            }
            if ($this->_request->getParam('service') == 'articlegroupmarket') {
                $install = Zend_Registry::get('install');
                $rows = Doctrine_Query::create()->from('ArticleGroup m')->where('m.shop_id = ?', array(
                    intval($install['defaultmarket'])));
                $rows->addWhere('m.parent = 0');
                $rows = $rows->execute();
                $temp = array();
                array_push($temp, array(
                    'name' => '0',
                    'label' => $this->translate->translate('articlegroup_all_root')));
                foreach ($rows as $row) {
                    $temp = $this->checkChildrens('ArticleGroup m', 'm.parent = ?', $temp, $row, '');
                }
                $nodes = array(
                    'count' => count($temp),
                    'items' => $temp);
                $this->getHelper('Json')->sendJson($nodes);
            }
            if ($this->_request->getParam('service') == 'shippingtype') {

                $shop = Doctrine_Query::create()->from('Shop');
                if (intval(str_replace('S', '', $this->_request->getParam('sid'))) != 0) {
                    $shop->where('id = ?', array(
                        intval(str_replace('S', '', $this->_request->getParam('sid')))));
                }
                $shop = $shop->fetchOne();
                if ($shop->market == true) {
                    $rows = Doctrine_Query::create()->from('Shippingtype m');
                    if (intval(str_replace('S', '', $this->_request->getParam('sid'))) != 0) {
                        $rows->where('shop_id = ? AND private = 1', array($shop->Install->defaultmarket));
                    }
                } else {
                    $rows = Doctrine_Query::create()->from('Shippingtype m');
                    if (intval(str_replace('S', '', $this->_request->getParam('sid'))) != 0) {
                        $rows->where('shop_id = ? AND private = 1', array(
                            intval(str_replace('S', '', $this->_request->getParam('sid')))));
                    }
                }
                $rows = $rows->execute();
                $temp = array();
                foreach ($rows->toArray() as $row)
                    array_push($temp, array(
                        'name' => $row['id'],
                        'label' => $row['title']));
                $nodes = array(
                    'count' => count($temp),
                    'items' => $temp);
                $this->getHelper('Json')->sendJson($nodes);
            }
            if ($this->_request->getParam('service') == 'paymenttype') {
                $rows = Doctrine_Query::create()->from('Paymenttype m');
                if (intval(str_replace('S', '', $this->_request->getParam('sid'))) != 0) {
                    $rows->where('shop_id = ? AND private = 1', array(
                        intval(str_replace('S', '', $this->_request->getParam('sid')))));
                }
                $rows = $rows->execute();
                $temp = array();
                foreach ($rows->toArray() as $row)
                    array_push($temp, array(
                        'name' => $row['id'],
                        'label' => $row['title']));
                $nodes = array(
                    'count' => count($temp),
                    'items' => $temp);
                $this->getHelper('Json')->sendJson($nodes);
            }
            if ($this->_request->getParam('service') == 'products') {
                $rows = Doctrine_Query::create()->from('Article m');
                if (intval(str_replace('S', '', $this->_request->getParam('sid'))) != 0) {
                    $rows->where('shop_id = ? AND private = 1', array(
                        intval(str_replace('S', '', $this->_request->getParam('sid')))));
                }
                $rows->addWhere('m.shop_id IN (' . implode(',', $this->shopIds) . ')')->orderBy("article_nr_intern, title");
                $rows = $rows->fetchArray();
                $temp = array();
                foreach ($rows as $row)
                    if ($row['a6_org_article'] != 0) {
                        array_push($temp, array(
                            'name' => $row['id'],
                            'label' => $row['article_nr_intern']. " " . $row['title'] . ' Sub'));
                    } else {
                        array_push($temp, array(
                            'name' => $row['id'],
                            'label' => $row['article_nr_intern']. " " . $row['title']));
                    }

                $nodes = array(
                    'count' => count($temp),
                    'items' => $temp);
                $this->getHelper('Json')->sendJson($nodes);
            }
            if ($this->_request->getParam('service') == 'cms') {
                $rows = Doctrine_Query::create()->from('Cms m');
                if (intval(str_replace('S', '', $this->_request->getParam('sid'))) != 0) {
                    $rows->where('shop_id = ?', array(
                        intval(str_replace('S', '', $this->_request->getParam('sid')))));
                }
                $rows->addWhere('m.shop_id IN (' . implode(',', $this->shopIds) . ')')->orderBy("title");
                $rows = $rows->fetchArray();
                $temp = array();
                foreach ($rows as $row) {
                    array_push($temp, array(
                        'name' => $row['id'],
                        'label' => $row['title']));

                }
                $nodes = array(
                    'count' => count($temp),
                    'items' => $temp);
                $this->getHelper('Json')->sendJson($nodes);
            }
            if ($this->_request->getParam('service') == 'contacts') {
                $rows = Doctrine_Query::create()->from('Contact m')->leftJoin('m.Shops as s')->addWhere('m.id != 5 AND m.enable = 1');
                if (intval(str_replace('S', '', $this->_request->getParam('sid'))) != 0) {
                    $rows->addWhere('s.shop_id = ?', array(
                        intval(str_replace('S', '', $this->_request->getParam('sid')))));
                }
                $rows->addWhere('s.id IN (' . implode(',', $this->shopIds) . ')');
                $rows = $rows->execute();
                $temp = array();
                foreach ($rows->toArray() as $row)
                    array_push($temp, array(
                        'name' => $row['id'],
                        'label' => $row['name']));
                $nodes = array(
                    'count' => count($temp),
                    'items' => $temp);
                $this->getHelper('Json')->sendJson($nodes);
            }
            if ($this->_request->getParam('service') == 'account') {
                $rows = Doctrine_Query::create()->from('Account m')->leftJoin('m.Shops as s');
                if (intval(str_replace('S', '', $this->_request->getParam('sid'))) != 0) {
                    $rows->addWhere('s.id = ?', array(
                        intval(str_replace('S', '', $this->_request->getParam('sid')))));
                }
                $rows->addWhere('s.id IN (' . implode(',', $this->shopIds) . ')')->orderBy('m.company ASC');
                $rows = $rows->execute();
                $temp = array();
                foreach ($rows->toArray() as $row)
                    array_push($temp, array(
                        'name' => $row['id'],
                        'label' => $row['company'] . " ". $row['appendix']));
                $nodes = array(
                    'count' => count($temp),
                    'items' => $temp);
                $this->getHelper('Json')->sendJson($nodes);
            }

            if ($this->_request->getParam('service') == 'confirmaccount') {
                $rows = Doctrine_Query::create()->from('Account m')->leftJoin('m.Shops as s');
                if (intval(str_replace('S', '', $this->_request->getParam('sid'))) != 0) {
                    $rows->addWhere('s.id = ?', array(
                        intval(str_replace('S', '', $this->_request->getParam('sid')))));
                }
                $rows->addWhere('s.id IN (' . implode(',', $this->shopIds) . ')');
                $rows = $rows->execute();
                $temp = array();
                foreach ($rows->toArray() as $row)
                    array_push($temp, array(
                        'name' => $row['id'],
                        'label' => $row['company']));
                $nodes = array(
                    'count' => count($temp),
                    'items' => $temp);
                $this->getHelper('Json')->sendJson($nodes);
            }
            if ($this->_request->getParam('service') == 'confirmcontact') {
                $rows = Doctrine_Query::create()->from('Contact m')->leftJoin('m.Shops as s');
                if (intval(str_replace('S', '', $this->_request->getParam('sid'))) != 0) {
                    $rows->addWhere('s.id = ?', array(
                        intval(str_replace('S', '', $this->_request->getParam('sid')))));
                }
                $rows->addWhere('m.id != 5 AND s.id IN (' . implode(',', $this->shopIds) . ')');
                if(intval(str_replace('S', '', $this->_request->getParam('sid'))) != 228) {
                    $rows = $rows->limit(100)->execute();
                }else{
                    $rows = $rows->limit(400)->execute();
                }
                $temp = array();
                foreach ($rows->toArray() as $row)
                    array_push($temp, array(
                        'name' => $row['id'],
                        'label' => $row['self_firstname'] . ' ' . $row['self_lastname']));
                $nodes = array(
                    'count' => count($temp),
                    'items' => $temp);
                $this->getHelper('Json')->sendJson($nodes);
            }
            if ($this->_request->getParam('service') == 'shops') {
                $temp = array();
                foreach ($this->user->Shops as $row)
                    array_push($temp, array(
                        'name' => $row->id,
                        'label' => $row->name));
                $nodes = array(
                    'count' => count($temp),
                    'items' => $temp);
                $this->getHelper('Json')->sendJson($nodes);
            }
            if ($this->_request->getParam('service') == 'articlethemes') {
                $rows = Doctrine_Query::create()->from('ArticleTheme m');
                if (intval(str_replace('S', '', $this->_request->getParam('sid'))) != 0) {
                    $rows->addWhere('m.shop_id = ?', array(
                        intval(str_replace('S', '', $this->_request->getParam('sid')))));
                }
                $rows->addWhere('m.parent_id = 0 AND m.shop_id IN (' . implode(',', $this->shopIds) . ')');
                $rows = $rows->execute();
                $temp = array();
                foreach ($rows as $row) {
                    $temp = $this->checkChildrens('ArticleTheme m', 'm.parent_id = ?', $temp, $row, '');
                }
                $nodes = array(
                    'count' => count($temp),
                    'items' => $temp);
                $this->getHelper('Json')->sendJson($nodes);
            }
            if ($this->_request->getParam('service') == 'articlemarketthemes') {
                $install = Zend_Registry::get('install');
                $rows = Doctrine_Query::create()->from('ArticleTheme m')->addWhere('m.shop_id = ?', array(
                    intval($install['defaultmarket'])));
                $rows->addWhere('m.parent_id = 0');
                $rows = $rows->execute();
                $temp = array();
                foreach ($rows as $row) {
                    $temp = $this->checkChildrens('ArticleTheme m', 'm.parent_id = ?', $temp, $row, '');
                }
                $nodes = array(
                    'count' => count($temp),
                    'items' => $temp);
                $this->getHelper('Json')->sendJson($nodes);
            }
            if ($this->_request->getParam('service') == 'motivthemes') {

                $rows = Doctrine_Query::create()->from('MotivTheme m')->addWhere('m.shop_id = ? AND m.shop_id IN (' . implode(',', $this->shopIds) . ')', array(
                    intval($this->_request->getParam('sid'))));
                $rows->addWhere('m.parent_id = 0');
                $rows = $rows->execute();
                $temp = array();
                foreach ($rows as $row) {
                    $temp = $this->checkChildrens('MotivTheme m', 'm.parent_id = ?', $temp, $row, '');
                }
                $nodes = array(
                    'count' => count($temp),
                    'items' => $temp);
                $this->getHelper('Json')->sendJson($nodes);
            }
            if ($this->_request->getParam('service') == 'shopthemes') {
                $rows = Doctrine_Query::create()->from('ShopTheme m')->addWhere('m.shop_id = ?', array(
                    intval($this->shop->Install->defaultmarket)));
                $rows = $rows->execute();
                $temp = array();
                foreach ($rows as $row) {
                    $temp = $this->checkChildrens('ShopTheme m', 'm.parent_id = ?', $temp, $row, '');
                }
                $nodes = array(
                    'count' => count($temp),
                    'items' => $temp);
                $this->getHelper('Json')->sendJson($nodes);
            }
            if ($this->_request->getParam('service') == 'motivmarketthemes') {

                $install = Zend_Registry::get('install');

                $rows = Doctrine_Query::create()->from('MotivTheme m')->addWhere('m.shop_id = ?', array(
                    intval($install['defaultmarket'])));
                $rows->addWhere('m.parent_id = 0');
                $rows = $rows->execute();
                $temp = array();
                foreach ($rows as $row) {
                    $temp = $this->checkChildrens('MotivTheme m', 'm.parent_id = ?', $temp, $row, '');
                }
                $nodes = array(
                    'count' => count($temp),
                    'items' => $temp);
                $this->getHelper('Json')->sendJson($nodes);
            }
            if ($this->_request->getParam('service') == 'articles') {
                $rows = Doctrine_Query::create()->select('m.id as id, m.title as title, m.a6_org_article as a6_org_article')->from('Article m');
                if (intval(str_replace('S', '', $this->_request->getParam('sid'))) != 0) {
                    $rows->where('shop_id = ? AND shop_id IN (' . implode(',', $this->shopIds) . ')', array(
                        intval(str_replace('S', '', $this->_request->getParam('sid')))));
                }
                if($this->_getParam('query', false)) {
                    $rows->andWhere("m.title LIKE ?", $this->_getParam('query'));
                }
                $rows = $rows->fetchArray();
                $temp = array();
                foreach ($rows as $row) {
                    if($row['a6_org_article'] == 0) {
                        array_push($temp, array(
                        'name' => $row['id'],
                        'label' => 'Main ' . $row['title'] .' '. $row->Shop->name));
                    }else{
                        array_push($temp, array(
                        'name' => $row['id'],
                        'label' => $row['title']));
                    }
                }
                $nodes = array(
                    'count' => count($temp),
                    'items' => $temp);
                $this->getHelper('Json')->sendJson($nodes);
            }
            if ($this->_request->getParam('service') == 'articles_main') {
                $rows = Doctrine_Query::create()->select('m.id as id, m.title as title, m.a6_org_article as a6_org_article')->from('Article m');
                if (intval(str_replace('S', '', $this->_request->getParam('sid'))) != 0) {
                    $rows->where('a6_org_article = 0 AND shop_id = ? AND shop_id IN (' . implode(',', $this->shopIds) . ')', array(
                        intval(str_replace('S', '', $this->_request->getParam('sid')))));
                }
                if($this->_getParam('query', false)) {
                    $rows->andWhere("m.title LIKE ?", $this->_getParam('query'));
                }
                $rows = $rows->fetchArray();
                $temp = array();
                foreach ($rows as $row) {
                    if($row['a6_org_article'] == 0) {
                        array_push($temp, array(
                            'name' => $row['id'],
                            'label' => 'Main ' . $row['title'] .' '. $row->Shop->name));
                    }else{
                        array_push($temp, array(
                            'name' => $row['id'],
                            'label' => $row['title']));
                    }
                }
                $nodes = array(
                    'count' => count($temp),
                    'items' => $temp);
                $this->getHelper('Json')->sendJson($nodes);
            }
            if ($this->_request->getParam('service') == 'cms') {
                $rows = Doctrine_Query::create()->from('Cms m');
                if (intval(str_replace('S', '', $this->_request->getParam('sid'))) != 0) {
                    $rows->where('shop_id = ?', array(
                        intval(str_replace('S', '', $this->_request->getParam('sid')))));
                }
                $rows = $rows->execute();
                $temp = array();
                foreach ($rows->toArray() as $row)
                    array_push($temp, array(
                        'name' => $row['id'],
                        'label' => $row['title']));
                $nodes = array(
                    'count' => count($temp),
                    'items' => $temp);
                $this->getHelper('Json')->sendJson($nodes);
            }
            if ($this->_request->getParam('service') == 'layouts') {
                $rows = Doctrine_Query::create()->from('Shop');
                if (intval(str_replace('S', '', $this->_request->getParam('sid'))) != 0) {
                    $rows->where('id = ?', array(
                        intval(str_replace('S', '', $this->_request->getParam('sid')))));
                }
                $rows = $rows->fetchOne();
                $temp = array();
                if ($rows->customtemplates == 1) {
                    foreach (new DirectoryIterator(APPLICATION_PATH . '/design/vorlagen') as $file) {
                        if ($file == '.' || $file == '..' || $file == 'config' || $file == '.svn' || $file == 'datapacks' || $file == '.DS_Store') {
                            continue;
                        }
                        array_push($temp, array(
                            'name' => $file->getFileName(),
                            'label' => $file->getFileName()));
                    }
                } else {
                    if (file_exists(APPLICATION_PATH . '/design/clients/' . $rows->uid)) {
                        foreach (new DirectoryIterator(APPLICATION_PATH . '/design/clients/' . $rows->uid) as $file) {
                            if ($file == '.' || $file == '..' || $file == 'config' || $file == '.svn' || $file == 'datapacks' || $file == '.DS_Store') {
                                continue;
                            }
                            array_push($temp, array(
                                'name' => $file->getFileName(),
                                'label' => $file->getFileName()));
                        }
                    }

                    if (file_exists(APPLICATION_PATH . '/design/clients/' . $rows->Install->DefaultMarket->uid . '/subshopdesigns/public')) {
                        foreach (new DirectoryIterator(APPLICATION_PATH . '/design/clients/' . $rows->Install->DefaultMarket->uid . '/subshopdesigns/public') as $file) {
                            if ($file == '.' || $file == '..' || $file == 'config' || $file == '.svn' || $file == 'datapacks' || $file == '.DS_Store') {
                                continue;
                            }
                            array_push($temp, array(
                                'name' => '../' . $rows->Install->DefaultMarket->uid . '/subshopdesigns/public/' . $file->getFileName(),
                                'label' => $file->getFileName()));
                        }
                        foreach (new DirectoryIterator(APPLICATION_PATH . '/design/clients/' . $rows->Install->DefaultMarket->uid . '/subshopdesigns/private') as $file) {
                            if ($file == '.' || $file == '..' || $file == 'config' || $file == '.svn' || $file == 'datapacks' || $file == '.DS_Store') {
                                continue;
                            }
                            array_push($temp, array(
                                'name' => '../' . $rows->Install->DefaultMarket->uid . '/subshopdesigns/private/' . $file->getFileName(),
                                'label' => $file->getFileName()));
                        }
                    }
                }
                $nodes = array(
                    'count' => count($temp),
                    'items' => $temp);
                $this->getHelper('Json')->sendJson($nodes);
            }
            if ($this->_request->getParam('service') == 'status') {
                $config = new Zend_Config_Ini(APPLICATION_PATH . '/configs/' . '/systemdefaults.ini', $this->_request->getParam('service'));
                $translate = Zend_Registry::get('translate');
                $temp = array();

                foreach ($config->toArray() as $key => $value) {
                    array_push($temp, array(
                        'name' => ($key),
                        'label' => $translate->translate($key)));
                }
                $install = Zend_Registry::get('install');
                if ($install['offline_custom2'] != "") {
                    try {

                        $maschienen = new SimpleXMLElement($install['offline_custom2'], null, false);

                        foreach ($maschienen as $maschine) {
                            array_push($temp, array(
                                'name' => (int)$maschine['id'], 'label' => $translate->translate((string)$maschine)
                            ));
                        }
                    } catch (Exception $e) {

                    }
                }

                $nodes = array(
                    'count' => count($temp),
                    'items' => $temp);
                $this->getHelper('Json')->sendJson($nodes);
            }
            if ($this->_request->getParam('service') == 'status_pos') {
                $config = new Zend_Config_Ini(APPLICATION_PATH . '/configs/' . '/systemdefaults.ini', $this->_request->getParam('service'));
                $translate = Zend_Registry::get('translate');
                $temp = array();

                foreach ($config->toArray() as $key => $value) {
                    array_push($temp, array(
                        'name' => ($key),
                        'label' => $translate->translate($key)));
                }
                $install = Zend_Registry::get('install');
                if ($install['offline_custom2'] != "") {
                    try {

                        $maschienen = new SimpleXMLElement($install['offline_custom2'], null, false);

                        foreach ($maschienen as $maschine) {
                            array_push($temp, array(
                                'name' => (int)$maschine['id'], 'label' => $translate->translate((string)$maschine)
                            ));
                        }
                    } catch (Exception $e) {

                    }
                }

                $nodes = array(
                    'count' => count($temp),
                    'items' => $temp);
                $this->getHelper('Json')->sendJson($nodes);
            }
            if ($this->_request->getParam('service') == 'account_typ' || $this->_request->getParam('service') == 'motivstaffel' || $this->_request->getParam('service') == 'booking_type' || $this->_request->getParam('service') == 'booking_type_art') {
                $config = new Zend_Config_Ini(APPLICATION_PATH . '/configs/' . '/systemdefaults.ini', $this->_request->getParam('service'));
                $translate = Zend_Registry::get('translate');
                $temp = array();

                foreach ($config->toArray() as $key => $value)
                    array_push($temp, array(
                        'name' => ($key),
                        'label' => $translate->translate($value)));
                $nodes = array(
                    'count' => count($config->toArray()),
                    'items' => $temp);
                $this->getHelper('Json')->sendJson($nodes);
            }
            if ($this->_request->getParam('service') == 'land') {
                $config = new Zend_Config_Ini(APPLICATION_PATH . '/configs/' . '/systemdefaults.ini', $this->_request->getParam('service'));
                $translate = Zend_Registry::get('translate');
                $temp = array();

                foreach ($config->toArray() as $key => $value)
                    array_push($temp, array(
                        'name' => ($key),
                        'label' => $translate->translate($value)));
                $nodes = array(
                    'count' => count($config->toArray()),
                    'items' => $temp);
                $this->getHelper('Json')->sendJson($nodes);
            }
            if ($this->_request->getParam('service') == 'salutation') {
                $config = new Zend_Config_Ini(APPLICATION_PATH . '/configs/' . '/systemdefaults.ini', $this->_request->getParam('service'));
                $translate = Zend_Registry::get('translate');
                $temp = array();

                foreach ($config->toArray() as $key => $value)
                    array_push($temp, array(
                        'name' => ($key),
                        'label' => $translate->translate($value)));
                $nodes = array(
                    'count' => count($config->toArray()),
                    'items' => $temp);
                $this->getHelper('Json')->sendJson($nodes);
            }
            if ($this->_request->getParam('service') == 'motivstatus') {
                $config = new Zend_Config_Ini(APPLICATION_PATH . '/configs/' . '/systemdefaults.ini', $this->_request->getParam('service'));
                $translate = Zend_Registry::get('translate');
                $temp = array();

                foreach ($config->toArray() as $key => $value)
                    array_push($temp, array(
                        'name' => ($key),
                        'label' => $translate->translate($value)));
                $nodes = array(
                    'count' => count($config->toArray()),
                    'items' => $temp);
                $this->getHelper('Json')->sendJson($nodes);
            }
            $config = new Zend_Config_Ini(APPLICATION_PATH . '/configs/' . '/systemdefaults.ini', $this->_request->getParam('service'));
            $translate = Zend_Registry::get('translate');
            $temp = array();
            foreach ($config->toArray() as $key => $value)
                array_push($temp, array(
                    'name' => $value,
                    'label' => $translate->translate($value)));
            $nodes = array(
                'count' => count($config->toArray()),
                'items' => $temp);
            $this->getHelper('Json')->sendJson($nodes);
        } elseif ($this->_request->getParam('type') == 'upload') {
            if ($this->_getParam('mode') == 'frontend') {

                if (!file_exists(PUBLIC_PATH . '/shops/' . $this->shop->uid . '/images/cms')) {
                    mkdir(PUBLIC_PATH . '/shops/' . $this->shop->uid . '/images/cms', 0777, true);
                }

                $opts = array(
                    'root' => PUBLIC_PATH . '/shops/' . $this->shop->uid . '/images/cms', // path to root directory
                    'URL' => '/shops/' . $this->shop->uid . '/images/cms/', // root directory URL
                    'rootAlias' => 'Home', // display this instead of root directory name
                    //'uploadAllow'   => array('images/*'),
                    //'uploadDeny'    => array('all'),
                    //'uploadOrder'   => 'deny,allow'
                    // 'disabled'     => array(),      // list of not allowed commands
                    // 'dotFiles'     => false,        // display dot files
                    // 'dirSize'      => true,         // count total directories sizes
                    // 'fileMode'     => 0666,         // new files mode
                    // 'dirMode'      => 0777,         // new folders mode
                    // 'mimeDetect'   => 'auto',       // files mimetypes detection method (finfo, mime_content_type, linux (file -ib), bsd (file -Ib), internal (by extensions))
                    // 'uploadAllow'  => array(),      // mimetypes which allowed to upload
                    // 'uploadDeny'   => array(),      // mimetypes which not allowed to upload
                    // 'uploadOrder'  => 'deny,allow', // order to proccess uploadAllow and uploadAllow options
                    // 'imgLib'       => 'auto',       // image manipulation library (gmagick, mogrify, gd)
                    // 'tmbDir'       => '.tmb',       // directory name for image thumbnails. Set to "" to avoid thumbnails generation
                    // 'tmbCleanProb' => 1,            // how frequiently clean thumbnails dir (0 - never, 100 - every init request)
                    // 'tmbAtOnce'    => 5,            // number of thumbnails to generate per request
                    // 'tmbSize'      => 48,           // images thumbnails size (px)
                    // 'fileURL'      => true,         // display file URL in "get info"
                    // 'dateFormat'   => 'j M Y H:i',  // file modification date format
                    // 'logger'       => null,         // object logger
                    // 'defaults'     => array(        // default permisions
                    // 	'read'   => true,
                    // 	'write'  => true,
                    // 	'rm'     => true
                    // 	),
                    // 'perms'        => array(),      // individual folders/files permisions
                    // 'debug'        => true,         // send debug to client
                    // 'archiveMimes' => array(),      // allowed archive's mimetypes to create. Leave empty for all available types.
                    // 'archivers'    => array()       // info about archivers to use. See example below. Leave empty for auto detect
                    // 'archivers' => array(
                    // 	'create' => array(
                    // 		'application/x-gzip' => array(
                    // 			'cmd' => 'tar',
                    // 			'argc' => '-czf',
                    // 			'ext'  => 'tar.gz'
                    // 			)
                    // 		),
                    // 	'extract' => array(
                    // 		'application/x-gzip' => array(
                    // 			'cmd'  => 'tar',
                    // 			'argc' => '-xzf',
                    // 			'ext'  => 'tar.gz'
                    // 			),
                    // 		'application/x-bzip2' => array(
                    // 			'cmd'  => 'tar',
                    // 			'argc' => '-xjf',
                    // 			'ext'  => 'tar.bz'
                    // 			)
                    // 		)
                    // 	)
                );

                $fm = new TP_Finder($opts);
                $fm->run();
            } elseif ($this->_request->getParam('mode') == 'list') {
                $row = Doctrine_Query::create()->from('Shop m')->where('m.id = ?')->fetchOne(array(
                    $this->_request->getParam('id')));
                $nodes = array();
                foreach (new DirectoryIterator(PUBLIC_PATH . '/shops/' . $row->uid . '/images/cms') as $file) {
                    if (strpos(strtolower($file), 'jpg') != false || strpos(strtolower($file), 'png') != false || strpos(strtolower($file), 'gif') != false) {
                        //$geo = getimagesize(PUBLIC_PATH . '/shops/' . $row->uid . '/images/cms/' . $file->getFilename());
                        $image = new imagick(PUBLIC_PATH . '/shops/' . $row->uid . '/images/cms/' . $file->getFilename());
                        $geo = $image->getImageGeometry();
                        array_push($nodes, array(
                            'label' => 'test',
                            'name' => $file->getFilename(),
                            'width' => $geo['width'],
                            'height' => $geo['height'],
                            'url' => '/shops/' . $row->uid . '/images/cms/' . $file->getFilename()));
                    }elseif(strpos(strtolower($file), 'pdf') != false) {
                        array_push($nodes, array(
                            'label' => 'test',
                            'name' => $file->getFilename(),
                            'width' => 0,
                            'height' => 0,
                            'url' => '/shops/' . $row->uid . '/images/cms/' . $file->getFilename()));
                    }
                }
                $this->getHelper('Json')->sendJson(array(
                    'images' => $nodes,
                    'success' => true));
            } elseif ($this->_request->getParam('mode') == 'uploadcms') {
                $uploadedFile = $_FILES['image'];
                $row = Doctrine_Query::create()->from('Shop m')->where('m.id = ?')->fetchOne(array(
                    $this->_request->getParam('sid')));
                $file = new Image();
                $file->generateUID();
                $path = 'uploads/' . $row->uid . '/cms/' . $file->id . '_' . $uploadedFile['name'];
                move_uploaded_file($uploadedFile['tmp_name'], $path);
                die(Zend_Json::encode(array(
                    'success' => 'true')));
            } elseif ($this->_request->getParam('mode') == 'upload') {
                $uploadedFile = $_FILES['file'];
                $file = new Image();
                $file->generateUID();
                $path = 'temp/' . $file->id . '_' . $uploadedFile['name'];
                $file->path = $path;
                $file->name = $uploadedFile['name'];
                $file->imagetype = $uploadedFile['type'];
                move_uploaded_file($uploadedFile['tmp_name'], $path);
                $tempFile = array(
                    'id' => $file->id,
                    'session_id' => session_id(),
                    'time' => Zend_Date::now()->getIso(),
                    'path' => $path,
                    'name' => $uploadedFile['name'],
                    'type' => $uploadedFile['type'],
                    'error' => $uploadedFile['error'],
                    'size' => $uploadedFile['size']);
                $file->save();
                $config = array(
                    "success" => true,
                    "status" => 'success',
                    'tempFile' => $tempFile);
                die(Zend_Json::encode($config));
            } elseif ($this->_request->getParam('mode') == 'get') {
                $image = Doctrine_Query::create()->from('Image as i')->where('i.id = ?')->fetchOne(array(
                    $this->_request->getParam('id')));
                if (strpos($image->imagetype, 'pdf') != false) {
                    $temp = 'images/admin/pdf.jpg';
                } elseif (strpos($image->imagetype, 'jpg') || strpos($image->imagetype, 'gif') || strpos($image->imagetype, 'bmp') || strpos($image->imagetype, 'png') || strpos($image->imagetype, 'jpeg') || strpos($image->imagetype, 'tiff') || strpos($image->imagetype, 'postscript')) {
                    $convert = TP_Image::getInstance();
                    $temp = $convert->createThumb('admin', $image, intval($this->_request->getParam('width')), intval($this->_request->getParam('height')));
                } else {
                    $temp = 'images/admin/download.png';
                }
                header('Content-Type: image/jpeg');
                die(file_get_contents($temp));
            }
        } elseif ($this->_request->getParam('type') == 'getTranslations') {
            $translate = Zend_Registry::get('translate');
            $this->getHelper('Json')->sendJson($translate->getMessages(Zend_Registry::get('locale')->getLanguage()));
            die();
        } elseif ($this->_request->getParam('type') == 'print') {
            $file = TP_Util::writeReport($this->_request->getParam('uid'), $this->_request->getParam('mode'));
            // Wir werden eine PDF Datei ausgeben
            if ($file == false) {
                die("ERROR PRINT");
            }

            $data = Doctrine_Query::create()
                ->from('Orders a')
                ->where('a.id = ?')->fetchOne(array(intval($this->_request->getParam('uid'))));

            header('Content-type: application/pdf');

            // Es wird downloaded.pdf benannt
            header('Content-Disposition: attachment; filename="' . $data->alias . '_' . $this->_request->getParam('mode') . '.pdf"');

            // Die originale PDF Datei heißt original.pdf
            readfile($file);
        } elseif ($this->_request->getParam('type') == 'render_print') {

            $row = Doctrine_Query::create()
                ->from('Orderspos a')
                ->where('a.uuid = ? AND a.xmlxslfofile != ""', array($this->_request->getParam('uid')))->fetchOne();

            if (!$row) {
                die();
            }

            if ($this->_getParam('get') == 'pdf') {
                header('Content-type: application/pdf');

                // Es wird downloaded.pdf benannt
                header('Content-Disposition: attachment; filename="print.pdf"');
                $path = implode('/', str_split($row->id));
                $this->readfile_chunked(APPLICATION_PATH . '/../data/renderings/' . $path . '/print.pdf');
            } else {
                header('Content-Type: application/octet-stream');

                // Es wird downloaded.pdf benannt
                header('Content-Disposition: attachment; filename="' . $row->Orders->alias . '_print.zip"');
                header('Content-Transfer-Encoding: binary');
                $path = implode('/', str_split($row->id));
                // Die originale PDF Datei heißt original.pdf
                $this->readfile_chunked(APPLICATION_PATH . '/../data/renderings/' . $path . '/print.zip');
            }
        }
    }

    public function getprintpdfAction() {
        if ($this->highRole->level >= 35) {

            $articleSession = new TP_Layoutersession();
            $articleSess = $articleSession->getLayouterArticle($this->_getParam('uuid'));

            $path = md5($this->_getParam('uuid'));

            file_put_contents(APPLICATION_PATH . '/../data/temprenderings/' . $path . '_fop.fop', $articleSess->getXslFo());
            TP_FOP::generatePdfPrint(APPLICATION_PATH . '/../data/temprenderings/' . $path . '_fop.fop', APPLICATION_PATH . '/../data/temprenderings/' . $path . '_print.pdf');

            Zend_Registry::get('log')->debug(APPLICATION_PATH . '/../data/temprenderings/' . $path . '_fop.fop');

            header('Content-type: application/pdf');

            // Es wird downloaded.pdf benannt
            header('Content-Disposition: attachment; filename="' . $path . '_print.pdf"');

            $this->readfile_chunked(APPLICATION_PATH . '/../data/temprenderings/' . $path . '_print.pdf');
            unlink(APPLICATION_PATH . '/../data/temprenderings/' . $path . '_fop.fop');
            unlink(APPLICATION_PATH . '/../data/temprenderings/' . $path . '_print.pdf');
        }
    }

    public function getprintpreviewpdfAction() {

        $articleSession = new TP_Layoutersession();
        $articleSess = $articleSession->getLayouterArticle($this->_getParam('uuid'));

        $path = md5($this->_getParam('uuid'));

        file_put_contents(APPLICATION_PATH . '/../data/temprenderings/' . $path . '_fop.fop', $articleSess->getXslFo());
        TP_FOP::generatePdfPrintPreview(APPLICATION_PATH . '/../data/temprenderings/' . $path . '_fop.fop', APPLICATION_PATH . '/../data/temprenderings/' . $path . '_print.pdf');

        header('Content-type: application/pdf');

        // Es wird downloaded.pdf benannt
        header('Content-Disposition: attachment; filename="' . $path . '_print.pdf"');

        $this->readfile_chunked(APPLICATION_PATH . '/../data/temprenderings/' . $path . '_print.pdf');
        unlink(APPLICATION_PATH . '/../data/temprenderings/' . $path . '_fop.fop');
        unlink(APPLICATION_PATH . '/../data/temprenderings/' . $path . '_print.pdf');
    }

    public function getzipAction() {
        if ($this->highRole->level >= 35 || $this->_request->getParam('uuid', false)) {

            $data = Doctrine_Query::create()
                ->from('Orders a')
                ->where('a.id = ?', array(intval($this->_request->getParam('uid'))))->andWhereIn('a.shop_id', $this->shopIds)->fetchOne();

            $package = new TP_Package();
            $package->loadOrder($data);

            if($data->Install->id == 7) {
                $package->createZip(true);
            }else{
                $package->createZip();
            }

            $package->download();

            return;
        }

        echo "Kein Zugriff";
    }

    // Fuktion readfile_chunked();
    protected function readfile_chunked($filename) {
        $chunksize = 1 * (1024 * 1024); // how many bytes per chunk
        $buffer = '';
        $handle = fopen($filename, 'rb');
        if ($handle === false) {
            return false;
        }
        while (!feof($handle)) {
            $buffer = fread($handle, $chunksize);
            print $buffer;
            ob_flush();
            flush();
        }
        return fclose($handle);
        die();
    }

}
