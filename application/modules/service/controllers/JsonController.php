<?php

class Service_JsonController extends Zend_Controller_Action
{

    protected $user;

    public function init() {
        $contextSwitch = $this->_helper->contextSwitch();
        $contextSwitch
            ->addActionContext('login', 'json')
            ->addActionContext('sync', 'json')
            ->addActionContext('download', 'json')
            ->setAutoJsonSerialization(true)->initContext();

        $this->view->clearVars();

        if ($this->_getParam('email', false)) {
            $_authAdapter = new TP_Plugin_AuthAdapter(); // put this in a constructor?
            $_authAdapter->setIdentity($this->_getParam('email'))
                ->setCredential($this->_getParam('password'));
            $result = Zend_Auth::getInstance()->authenticate($_authAdapter);
        }

        if (Zend_Auth::getInstance()->hasIdentity()) {
            $user = Zend_Auth::getInstance()->getIdentity();
            $mode = new Zend_Session_Namespace('adminmode');

            $this->user = Doctrine_Query::create()
                ->from('Contact m')
                ->where('id = ?')->fetchOne(array($user['id']));
        } else {
            $this->user = new Contact();
        }
    }

    public function loginAction() {

        $_authAdapter = new TP_Plugin_AuthAdapter(); // put this in a constructor?
        $_authAdapter->setIdentity($this->_getParam('email'))
            ->setCredential($this->_getParam('password'));
        $result = Zend_Auth::getInstance()->authenticate($_authAdapter);

        if ($result->isValid()) {
            $this->view->code = 200;
            return;
        }

        $this->view->code = 201;
    }

    public function downloadAction() {

        $package = new TP_Package($this->_getParam('uuid'));
        $package->loadOrder();

        $package->createZip();
        $package->download();

    }

    public function downloadposAction() {

        $package = new TP_Package();
        $package->loadByPos($this->_getParam('uuid'), $this->_getParam('posuuid'));

        $package->createPosZip();
        $package->downloadPos();

    }

    public function syncAction() {

        if ($this->_getParam('step', 1) == 1) {

            $this->view->sync_table = array();
            $od = array();
            $orderspos = array();
            $sp = array();
            $params = $this->_getAllParams();
            $currentOrders = array();
            $currentPositions = array();
            $currentShops = array();
            $requireOrders = array();
            $requirePositions = array();

            if ($params['sync_table']['shops']) {
                foreach ($params['sync_table']['shops'] as $shop) {
                    $currentShops[$shop['online_id']] = $shop['version'];
                }
            }

            if ($params['sync_table']['orders']) {
                foreach ($params['sync_table']['orders'] as $order) {
                    $currentOrders[$order['online_id']] = $order['version'];
                }
            }

            if ($params['sync_table']['positions']) {
                foreach ($params['sync_table']['positions'] as $pos) {
                    $currentPositions[$pos['online_id']] = $pos['version'];
                }
            }

            $shops = Doctrine_Query::create()
                ->select()
                ->from('ShopContact m')
                ->addWhere('m.contact_id = ?', array($this->user['id']))->execute();

            foreach ($shops as $shop) {

                if (!isset($currentShops[$shop->Shop['id']])) {
                    $sp[] = array(
                        'online_id' => $shop->Shop['id'],
                        'deleted' => 0,
                        'position' => 1,
                        'version' => 0,
                        'inbox' => 1,
                        'shared' => 0,
                        'name' => $shop->Shop['name']);
                }

                $limit = (40 + count($currentOrders));
                if ($limit > 200) {
                    $limit = 200;
                }

                $orders = Doctrine_Query::create()
                    ->select('m.id')
                    ->from('Orders m')
                    ->addWhere('m.shop_id = ?', array($shop->Shop['id']))->orderBy('m.created DESC')->limit($limit)->execute();

                foreach ($orders as $order) {
                    if (!isset($currentOrders[$order->uuid]) && ($order->status == 170 || $order->status == 210 || $order->status == 200)) {
                        continue;
                    } elseif (!isset($currentOrders[$order->uuid]) || $currentOrders[$order->uuid] < $order->version) {

                        $od[] = array(
                            'online_id' => $order->uuid,
                            'alias' => $order->alias,
                            'firstname' => $order->Contact->self_firstname,
                            'lastname' => $order->Contact->self_lastname,
                            'status' => $order->status,
                            'date' => strtotime($order->created),
                            'done' => 0,
                            'position' => 1,
                            'important' => 1,
                            'done_date' => time(),
                            'list_id' => $order->shop_id,
                            'deleted' => 0,
                            'version' => $order->version,
                            'note' => '',
                            'ref1' => $order->basketfield1,
                            'ref2' => $order->basketfield2
                        );

                        foreach ($order->Orderspos as $pos) {
                            if (!isset($currentPositions[$pos->id]) || $currentPositions[$pos->id] < $pos->version) {

                                $article = Doctrine_Query::create()
                                    ->from('Article c')
                                    ->where('c.id = ?', array($pos->article_id))
                                    ->fetchOne();

                                $options = $article->getOptArray($pos->data, true, true);

                                if (isset($options['upload_mode'])) {
                                    unset($options['upload_mode']);
                                }

                                if (isset($options['Typ'])) {
                                    $typ = $options['Typ'];
                                    unset($options['Typ']);
                                    $options = array_merge(array('Typ' => $typ), $options);
                                }

                                $orderspos[] = array(
                                    'online_id' => $pos->id,
                                    'order_id' => $order->uuid,
                                    'articlename' => $pos->Article->title . ' ' . $pos->Article->text_art . ' ' . $pos->Article->text_format,
                                    'status' => $pos->status,
                                    'papier' => $pos->papier,
                                    'maschine' => $pos->maschine,
                                    'proddate' => strtotime($pos->proddate),
                                    'lieferdate' => strtotime($pos->delivery_date),
                                    'options' => $options,
                                    'version' => $pos->version
                                );
                            } elseif ($currentPositions[$pos->id] > $pos->version) {
                                $requirePositions[] = $pos->id;
                            }
                        }
                    } elseif ($currentOrders[$order->uuid] > $order->version) {
                        $requireOrders[] = $order->uuid;
                    } elseif ($currentOrders[$order->uuid] == $order->version) {
                        foreach ($order->Orderspos as $pos) {
                            if (!isset($currentPositions[$pos->id]) || $currentPositions[$pos->id] < $pos->version) {

                                $article = Doctrine_Query::create()
                                    ->from('Article c')
                                    ->where('c.id = ?', array($pos->article_id))
                                    ->fetchOne();

                                $options = $article->getOptArray($pos->data, true, true);

                                if (isset($options['upload_mode'])) {
                                    unset($options['upload_mode']);
                                }

                                if (isset($options['Typ'])) {
                                    $typ = $options['Typ'];
                                    unset($options['Typ']);
                                    $options = array_merge(array('Typ' => $typ), $options);
                                }

                                $orderspos[] = array(
                                    'online_id' => $pos->id,
                                    'order_id' => $order->uuid,
                                    'articlename' => $pos->Article->title . ' ' . $pos->Article->text_art . ' ' . $pos->Article->text_format,
                                    'status' => $pos->status,
                                    'papier' => $pos->papier,
                                    'maschine' => $pos->maschine,
                                    'options' => $options,
                                    'proddate' => strtotime($pos->proddate),
                                    'lieferdate' => strtotime($pos->delivery_date),
                                    'version' => $pos->version
                                );
                            } elseif ($currentPositions[$pos->id] > $pos->version) {
                                $requirePositions[] = $pos->id;
                            }
                        }
                    }

                }
            }
            if (count($od) > 0) {
                $this->view->sync_table['new_tasks'] = $od;
            }
            if (count($orderspos) > 0) {
                $this->view->sync_table['new_positions'] = $orderspos;
            }

            $config = new Zend_Config_Ini(APPLICATION_PATH . '/configs/' . '/systemdefaults.ini', 'status');
            $translate = Zend_Registry::get('translate');
            $temp = array();
            $temp_pos = array();

            foreach ($config->toArray() as $key => $value) {

                $temp[] = array(
                    'online_id' => $key,
                    'name' => $translate->translate($key)
                );
            }

            $config_pos = new Zend_Config_Ini(APPLICATION_PATH . '/configs/' . '/systemdefaults.ini', 'status_pos');
            $translate = Zend_Registry::get('translate');

            foreach ($config_pos->toArray() as $key => $value) {

                $temp_pos[] = array(
                    'online_id' => $key,
                    'name' => $translate->translate($key)
                );
            }

            if (count($sp) > 0) {
                $this->view->sync_table['new_lists'] = $sp;
            }
            if (count($requireOrders) > 0) {
                $this->view->sync_table['required_tasks'] = $requireOrders;
            }
            if (count($requirePositions) > 0) {
                $this->view->sync_table['required_positions'] = $requirePositions;
            }

            $this->view->sync_table['maschine'] = array();
            $this->view->sync_table['maschine'][] = array('online_id' => 0, 'name' => 'Keine ausgewählt');

            $install = Zend_Registry::get('install');

            if ($install['offline_custom1'] != "") {
                try {

                    $maschienen = new SimpleXMLElement($install['offline_custom1'], null, false);

                    foreach ($maschienen as $maschine) {
                        $this->view->sync_table['maschine'][] = array(
                            'online_id' => (int)$maschine['id'], 'name' => (string)$maschine
                        );
                    }
                } catch (Exception $e) {

                }
            }

            if ($install['offline_custom2'] != "") {
                try {

                    $maschienen = new SimpleXMLElement($install['offline_custom2'], null, false);

                    foreach ($maschienen as $maschine) {
                        $temp[] = array(
                            'online_id' => (int)$maschine['id'], 'name' => $translate->translate((string)$maschine)
                        );
                    }
                } catch (Exception $e) {

                }
            }

            if ($install['offline_custom3'] != "") {
                try {

                    $maschienen = new SimpleXMLElement($install['offline_custom3'], null, false);

                    foreach ($maschienen as $maschine) {
                        $temp_pos[] = array(
                            'online_id' => (int)$maschine['id'], 'name' => $translate->translate((string)$maschine)
                        );
                    }
                } catch (Exception $e) {

                }
            }

            $this->view->sync_table['stati'] = $temp;
            $this->view->sync_table['stati_pos'] = $temp_pos;

        }

        if ($this->_getParam('step', 1) == 2) {

            $this->view->sync_table = array();
            $st = array();
            $sp = array();
            $params = $this->_getAllParams();
            $updateOrders = array();

            if (isset($params['sync_table']['required_positions'])) {
                foreach ($params['sync_table']['required_positions'] as $pos) {

                    $position = Doctrine_Query::create()
                        ->select()
                        ->from('Orderspos m')
                        ->addWhere('m.id = ?', array($pos['online_id']))->fetchOne();

                    $position->papier = $pos['papier'];
                    $position->version = $pos['version'];
                    $position->maschine = $pos['maschine'];
                    $position->status = $pos['status'];

                    if ($pos['proddate'] != "" && $pos['proddate'] != 0 && $pos['proddate'] != "-2040445456") {
                        $position->proddate = date('Y-m-d', $pos['proddate']);
                    }
                    if ($pos['lieferdate'] != "" && $pos['lieferdate'] != 0 && $pos['lieferdate'] != "-2040445456") {
                        $position->delivery_date = date('Y-m-d', $pos['lieferdate']);
                    }

                    $pro = unserialize($position->data);
                    $proo = $pro->getOptions();

                    if (isset($proo['produktionszeit']) && $pos['status'] == 160) {
                        $obj = new TP_Delivery(date('Y-m-d'));
                        $result = $obj->getDeliveryDate($proo['produktionszeit']);
                        $position->delivery_date = $result->format('Y-m-d');
                    }

                    $position->save();
                    $order = $position->Orders;
                    $order->updated = date('Y-m-d H:i:s');
                    $order->save();
                    $sp[] = array(
                        'offline_id' => $pos['id'],
                        'online_id' => $pos['online_id'],
                        'lieferdate' => strtotime($position->delivery_date)
                    );

                    TP_Queue::process('orderposstatuschange', 'global', $position);
                }
            }

            if (isset($params['sync_table']['required_tasks'])) {
                foreach ($params['sync_table']['required_tasks'] as $od) {

                    $st[] = array(
                        'offline_id' => $od['id'],
                        'online_id' => $od['online_id'],
                    );

                    $updateOrders[$od['online_id']] = $od;

                    $order = Doctrine_Query::create()
                        ->select()
                        ->from('Orders m')
                        ->addWhere('m.uuid = ?', array($od['online_id']))->fetchOne();
                    if ($order) {
                        $orgstatus = $order->status;
                        $order->status = $od['status'];
                        $order->version = $od['version'];
                        $order->save();

                        if ($orgstatus < 140 && $od['status'] >= 140 && $od['status'] != 170) {

                            $produktionszeit = 0;
                            foreach ($order->Orderspos as $pos) {
                                $pro = unserialize($pos->data);
                                $proo = $pro->getOptions();

                                if (isset($proo['produktionszeit']) && $proo['produktionszeit'] > $produktionszeit) {
                                    $produktionszeit = $proo['produktionszeit'];
                                }
                                if (isset($proo['produktionszeit'])) {
                                    $obj = new TP_Delivery(date('Y-m-d'));
                                    $result = $obj->getDeliveryDate($proo['produktionszeit']);
                                    $pos->delivery_date = $result->format('Y-m-d');
                                    $pos->save();
                                }
                            }

                            if ($produktionszeit > 0) {
                                $obj = new TP_Delivery(date('Y-m-d'));
                                $result = $obj->getDeliveryDate($produktionszeit);
                                $order->delivery_date = $result->format('Y-m-d');
                                $order->save();
                            }

                            reset($order->Orderspos);

                            $rowsbooking = Doctrine_Query::create()->select()->from('Booking b')->where('b.order_id = ?', array($order->id))->execute()->count();

                            foreach ($order->Orderspos as $pos) {

                                if ($rowsbooking == 0) {
                                    if ($pos->Article->a6_resale_price > 0) {
                                        $booking = new Booking();
                                        $booking->install_id = $order->install_id;
                                        $booking->shop_id = $order->shop_id;

                                        $booking->product_contact_creator = $pos->Article->contact_id;

                                        $booking->product_contact_creator_name = $pos->Article->Contact->self_firstname . ' ' . $pos->Article->Contact->self_lastname;
                                        $booking->product_contact_creator_street = $pos->Article->Contact->self_street . ' ' . $pos->Article->Contact->self_house_number;
                                        $booking->product_contact_creator_city = $pos->Article->Contact->self_zip . ' ' . $pos->Article->Contact->self_city;
                                        $booking->product_contact_creator_mail = $pos->Article->Contact->self_email;
                                        $booking->product_contact_creator_tel = $pos->Article->Contact->self_phone;

                                        $booking->product_contact_buyer = $order->contact_id;

                                        $booking->product_contact_buyer_name = $order->Contact->self_firstname . ' ' . $order->Contact->self_lastname;
                                        $booking->product_contact_buyer_street = $order->Contact->self_street . ' ' . $order->Contact->self_house_number;
                                        $booking->product_contact_buyer_city = $order->Contact->self_zip . ' ' . $order->Contact->self_city;
                                        $booking->product_contact_buyer_mail = $order->Contact->self_email;
                                        $booking->product_contact_buyer_tel = $order->Contact->self_phone;

                                        $booking->type = 1;

                                        $booking->shop_name = $order->Shop->name;
                                        $booking->order_id = $order->id;
                                        $booking->orderpos_id = $pos->id;
                                        $booking->product_id = $pos->article_id;

                                        $booking->product_name = $pos->Article->title;
                                        $booking->product_count = $pos->count;
                                        $booking->product_resale = $pos->Article->a6_resale_price;

                                        $booking->orderpos_netto = $pos->priceall;
                                        $booking->orderpos_mwert = $pos->priceallsteuer;
                                        $booking->orderpos_brutto = $pos->priceallbrutto;

                                        $booking->booking_typ = 1;
                                        $booking->booking_text = 'Gutschrift zu Ihrem Buchungskonto';

                                        $booking->payment_value = round(($pos->Article->a6_resale_price - ($pos->Article->a6_resale_price / 100 * $order->Shop->service_value)), 2);
                                        $booking->service_value = $order->Shop->service_value;
                                        $booking->save();
                                    }
                                    if ($pos->Article->typ == 8 || $pos->Article->typ == 6) {
                                        if (file_exists(APPLICATION_PATH . '/../market/products/' . $pos->Article->a6_directory . '/pageobjects.xml')) {
                                            $dom = new DOMDocument();

                                            $dom->load(APPLICATION_PATH . '/../market/products/' . $pos->Article->a6_directory . '/pageobjects.xml');
                                            $xpath = new DOMXPath($dom);

                                            $motiveCalc = $xpath->query('//root/item[contains(objType,"Image")]/image_uuid');

                                            $motive_calc = array();

                                            foreach ($motiveCalc as $motivCalc) {

                                                $row = Doctrine_Query::create()
                                                    ->from('Motiv c')
                                                    ->where('c.uuid = ?')
                                                    ->fetchOne(array($motivCalc->nodeValue));
                                                if (($row && $row->price1 > 0 && (!Zend_Auth::getInstance()->hasIdentity() || $row->contact_id != $order->contact_id)) && !isset($motive_calc[$row->uuid])) {

                                                    $motive_calc[$row->uuid] = $row->uuid;

                                                    $booking = new Booking();
                                                    $booking->install_id = $order->install_id;
                                                    $booking->shop_id = $order->shop_id;

                                                    $booking->product_contact_creator = $row->contact_id;

                                                    $booking->product_contact_buyer = $order->contact_id;

                                                    $booking->product_contact_creator_name = $row->Contact->self_firstname . ' ' . $row->Contact->self_lastname;
                                                    $booking->product_contact_creator_street = $row->Contact->self_street . ' ' . $row->Contact->self_house_number;
                                                    $booking->product_contact_creator_city = $row->Contact->self_zip . ' ' . $row->Contact->self_city;
                                                    $booking->product_contact_creator_mail = $row->Contact->self_email;
                                                    $booking->product_contact_creator_tel = $row->Contact->self_phone;

                                                    $booking->product_contact_buyer = $order->contact_id;

                                                    $booking->product_contact_buyer_name = $order->Contact->self_firstname . ' ' . $order->Contact->self_lastname;
                                                    $booking->product_contact_buyer_street = $order->Contact->self_street . ' ' . $order->Contact->self_house_number;
                                                    $booking->product_contact_buyer_city = $order->Contact->self_zip . ' ' . $order->Contact->self_city;
                                                    $booking->product_contact_buyer_mail = $order->Contact->self_email;
                                                    $booking->product_contact_buyer_tel = $order->Contact->self_phone;

                                                    $booking->type = 2;

                                                    $booking->shop_name = $order->Shop->name;
                                                    $booking->order_id = $order->id;
                                                    $booking->orderpos_id = $pos->id;
                                                    $booking->product_id = $row->id;

                                                    $booking->product_name = $row->title;
                                                    $booking->product_count = 1;
                                                    $booking->product_resale = $row->price1;

                                                    $booking->orderpos_netto = $pos->priceall;
                                                    $booking->orderpos_mwert = $pos->priceallsteuer;
                                                    $booking->orderpos_brutto = $pos->priceallbrutto;

                                                    $booking->booking_typ = 1;
                                                    $booking->booking_text = 'Gutschrift zu Ihrem Buchungskonto';

                                                    $booking->payment_value = round(($row->price1 - ($row->price1 / 100 * $order->Shop->service_value)), 2);
                                                    $booking->service_value = $order->Shop->service_value;
                                                    $booking->save();
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        TP_Queue::process('orderstatuschange', 'global', $order);
                    }
                }
            }

            if (count($st) > 0) {
                $this->view->sync_table['synced_tasks'] = $st;
            }

            if (count($sp) > 0) {
                $this->view->sync_table['synced_positions'] = $sp;
            }
        }

        $this->view->code = 300;
    }

}

