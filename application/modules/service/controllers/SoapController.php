<?php

class Service_SoapController extends Zend_Controller_Action
{

    public function init() {
        $this->getHelper('layout')->disableLayout();
        $this->_helper->viewRenderer->setNoRender();
        Zend_Controller_Action_HelperBroker::getStaticHelper('viewRenderer')->setNoRender(true);
    }

    public function indexAction() {
        ini_set("soap.wsdl_cache_enabled", 0);
        if (isset($_GET['wsdl'])) {
            $autodiscover = new Zend_Soap_AutoDiscover('Zend_Soap_Wsdl_Strategy_ArrayOfTypeComplex');
            $autodiscover->setClass('TP_SoapServer');
            $autodiscover->handle();
        } else {
            // zeigt auf diese aktuelle Datei
            if ((isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on')) {
                $soap = new Zend_Soap_Server('https://' . $_SERVER["SERVER_NAME"] . '/service/soap/index?wsdl=1');
            } else {
                $soap = new Zend_Soap_Server('http://' . $_SERVER["SERVER_NAME"] . '/service/soap/index?wsdl=1');
            }
            $soap->setClass('TP_SoapServer');
            $soap->handle();
        }
    }
}


