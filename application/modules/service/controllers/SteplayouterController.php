<?php

class Service_SteplayouterController extends TP_Controller_Action
{

    protected $_extAllow = 'ppt,inx,idml,indd,qxd,ps,cdr,cdx,jpg,gif,pdf,xls,png,doc,jpeg,tif,tiff,ai,eps,psd,phtml,zip,jrxml,ttf,otf,plt,csv,docx,xlsx,pptx';

    protected $_imageContainer = false;

    protected function FileSize($file, $setup = null)
    {
        $FZ = ($file && @is_file($file)) ? filesize($file) : NULL;
        $FS = array("B", "kB", "MB", "GB", "TB", "PB", "EB", "ZB", "YB");

        if (!$setup && $setup !== 0) {
            return number_format($FZ / pow(1024, $I = floor(log($FZ, 1024))), ($i >= 1) ? 2 : 0) . ' ' . $FS[$I];
        } elseif ($setup == 'INT') {
            return number_format($FZ);
        } else {
            return number_format($FZ / pow(1024, $setup), ($setup >= 1) ? 2 : 0) . ' ' . $FS[$setup];
        }
    }

    public function init()
    {
        parent::init();

        $contextSwitch = $this->_helper->contextSwitch();
        $contextSwitch
            ->addActionContext('getconfig', 'json')
            ->addActionContext('renderpreview', 'json')
            ->addActionContext('renderpdfpreview', 'json')
            ->addActionContext('renderpreviewlayer', 'json')
            ->addActionContext('store', 'json')
            ->addActionContext('savenewlayouter', 'json')
            ->addActionContext('updatenewlayouter', 'json')
            ->addActionContext('loadnewlayouter', 'json')
            ->addActionContext('upload', 'json')
            ->addActionContext('uploadmotiv', 'json')
            ->addActionContext('savecrop', 'json')
            ->addActionContext('savecropmotiv', 'json')
            ->addActionContext('getmymotive', 'json')
            ->addActionContext('getdesigntemplates', 'json')
            ->addActionContext('loadfonts', 'json')
            ->addActionContext('getimage', 'json')
            ->addActionContext('getmotiv', 'json')
            ->addActionContext('getimagemotiv', 'json')
            ->addActionContext('mymotive', 'json')
            ->addActionContext('motivthemes', 'json')
            ->addActionContext('motive', 'json')
            ->addActionContext('getthumbnail', 'json')
            ->addActionContext('getthumbnailmotiv', 'json')
            ->setAutoJsonSerialization(true)->initContext();

        $this->view->clearVars();

        $this->_imageContainer = new TP_Uploadsession();
    }

    protected function getMotivThemes($theme)
    {
        $this->view->result[] = array(
            'title' => $theme->title,
            'id' => $theme->id
        );

        $motivthemesd = Doctrine_Query::create()->from('MotivTheme c')->where('c.shop_id = ? AND c.parent_id=?', array(
            $this->shop->id,
            $theme->id))->orderBy('c.title ASC')->execute();
        foreach ($motivthemesd as $motivthemes) {
            $this->getMotivThemes($motivthemes);
        }
    }

    public function motivthemesAction()
    {
        $this->view->clearVars();

        $themes = Doctrine_Query::create()->from('MotivTheme c')->where('c.shop_id = ? AND c.parent_id = 0', array(
            $this->shop->id))->orderBy('c.title ASC')->execute();

        $this->view->success = true;
        $this->view->result = array();

        foreach ($themes as $theme) {
            $this->getMotivThemes($theme);
        }
    }

    public function motiveAction()
    {

        $uuids = "";

        if ($this->_getParam('search', false)) {

            $searchQuery = '*' . strtolower($this->_getParam('search')) . '*';

            $client = new Elastica_Client();
            $index = $client->getIndex('psc');
            $type = $index->getType('motive');

            $query = new Elastica_Query();
            $filterSI = new Elastica_Filter_Term(array('shop_id' => $this->shop->id));
            $filterEnable = new Elastica_Filter_Term(array('resale_shop' => 1));

            $filterD = new Elastica_Filter_And();
            $filterD->addField($filterSI);
            $filterD->addField($filterEnable);

            $filter = new Elastica_Filter($filterD, new Elastica_Query_MatchAll());
            $filter->addQuery(new Elastica_Query_QueryString($searchQuery));
            $query->setLimit(1000);
            $query->addFilter($filter);
            $resultSet = $type->search($query);

            $row_ids = array();
            if (count($resultSet->getResults()) > 0) {

                foreach ($resultSet->getResults() as $docinfo) {
                    array_push($row_ids, $docinfo->getId());
                }
            }

            $uuids = "AND (c.uuid = '" . implode("' OR c.uuid = '", $row_ids) . "')";
            $this->view->uuid = $uuids;
            $this->view->searchQuery = $searchQuery;
        }

        if ($this->_getParam('themeid', false)) {
            $motive = Doctrine_Query::create()
                ->from('Motiv c')
                ->where('c.shop_id = ? AND c.resale_shop = 1 AND c.status >= 50
                            AND c.id IN (SELECT d.motiv_id
                            FROM MotivThemeMotiv as d WHERE
                            d.theme_id = ? ' . $uuids . ' GROUP BY d.motiv_id)',
                    array($this->shop->id, $this->_getParam('themeid')))->offset($this->_getParam('offset', 0))->limit(18)->execute();
        } else {
            $motive = Doctrine_Query::create()
                ->from('Motiv c')
                ->where('c.shop_id = ? AND c.resale_shop = 1 AND c.status >= 50 ' . $uuids, array($this->shop->id))->offset($this->_getParam('offset', 0))->limit(18)->execute();
        }

        if ((isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on')) {
            $url = 'https://' . $_SERVER["SERVER_NAME"] . '/market/';
        } else {
            $url = 'http://' . $_SERVER["SERVER_NAME"] . '/market/';
        }

        $this->view->success = true;
        $this->view->result = array();
        foreach ($motive as $motiv) {
            if (isset($motiv['install_id'])) {
                $urlM = $url . 'motive/';
            } else {
                $urlM = $url . 'guest/';
            }
            $this->view->result[] = array(
                'uuid' => $motiv['uuid'],
                'title' => $motiv['title'],
                'thumbnail' => $motiv['file_thumb'],
                'url' => $urlM
            );

        }
    }

    public function getmotivAction() {
        $motiv = Doctrine_Query::create()
            ->from('Motiv c')
            ->where('c.uuid = ?', array($this->_getParam('uuid')))->fetchOne();

        if ((isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on')) {
            $url = 'https://' . $_SERVER["SERVER_NAME"] . '/market/';
        } else {
            $url = 'http://' . $_SERVER["SERVER_NAME"] . '/market/';
        }
        if (isset($motiv['install_id'])) {
            $urlM = $url . 'motive/';
        } else {
            $urlM = $url . 'guest/';
        }
        $this->_redirect($urlM . '/' . $motiv['file_mid']);
    }

    public function getimagemotivAction()
    {

        if ($this->_imageContainer->hasImage($this->_getParam('key'))) {
            $croping = $this->_imageContainer->getImage($this->_getParam('key'));

            $motiv = Doctrine_Query::create()
                ->from('Motiv c')
                ->where('c.uuid = ?', array($croping['motiv_id']))->fetchOne();
        } else {
            if (Zend_Auth::getInstance()->hasIdentity()) {
                $motiv = Doctrine_Query::create()
                    ->from('Motiv c')
                    ->where('c.uuid = ?', array($this->_getParam('key')))->fetchOne();
            } else {
                $images = new Zend_Session_Namespace('amfmotive');

                if (!isset($images->motive[$this->_getParam('key')])) {
                    $motiv = Doctrine_Query::create()
                        ->from('Motiv c')
                        ->where('c.uuid = ?', array($this->_getParam('key')))->fetchOne();
                } else {
                    $motiv = $images->motive[$this->_getParam('key')];
                }
            }

        }

        $this->view->motive = array();
        $this->view->motive[] = array('width' => $motiv['width'], 'height' => $motiv['height'], 'delete_url' => '', 'id' => $motiv['uuid'], 'key' => $this->_getParam('key'),
            'name' => $motiv['title']);
    }

    public function savecropmotivAction()
    {
        $source = $this->_getParam('source');
        $c = $this->_getParam('c');


        if ($this->_imageContainer->hasImage($source['key'])) {
            $croping = $this->_imageContainer->getImage($source['key']);


            $images = new Zend_Session_Namespace('amfmotive');

            if (!isset($images->motive[$croping['motiv_id']])) {
                $motiv = Doctrine_Query::create()
                    ->from('Motiv c')
                    ->where('c.uuid = ?', array($croping['motiv_id']))->fetchOne();

                $img = new imagick(APPLICATION_PATH . '/../market/motive/' . $motiv['file_work']);
            } else {
                $motiv = $images->motive[$croping['motiv_id']];

                $img = new imagick(APPLICATION_PATH . '/../market/guest/' . $motiv['file_work']);

            }

            $d = $img->getImageGeometry();
            $w = $d['width'];
            $h = $d['height'];

            $ratio = $w / $source['width'];

            $img->cropImage(intval($c['w'] * $ratio), intval($c['h'] * $ratio), intval($c['x'] * $ratio), intval($c['y'] * $ratio));

            $cropfile = 'steplayouter/uploads/crop/' . $source['key'] . '.png';
            $img->writeimage($cropfile);
            $id = $source['key'];
        } else {
            $images = new Zend_Session_Namespace('amfmotive');

            if (!isset($images->motive[$source['key']])) {
                $motiv = Doctrine_Query::create()
                    ->from('Motiv c')
                    ->where('c.uuid = ?', array($source['key']))->fetchOne();

                $img = new imagick(APPLICATION_PATH . '/../market/motive/' . $motiv['file_work']);
            } else {
                $motiv = $images->motive[$source['key']];

                $img = new imagick(APPLICATION_PATH . '/../market/guest/' . $motiv['file_work']);

            }

            $d = $img->getImageGeometry();
            $w = $d['width'];
            $h = $d['height'];

            $ratio = $w / $source['width'];

            $img->cropImage(intval($c['w'] * $ratio), intval($c['h'] * $ratio), intval($c['x'] * $ratio), intval($c['y'] * $ratio));

            $id = TP_Util::uuid();
            $cropfile = 'steplayouter/uploads/crop/' . $id . '.png';
            $img->writeimage($cropfile);

            $this->_imageContainer->addImage($cropfile, $source['key'], $id, $id);
        }

        $this->view->success = true;
        $this->view->uuid = $id;
        $this->view->elementid = $source['elementid'];
    }

    public function getthumbnailmotivAction()
    {

        if ($this->_imageContainer->hasImage($this->_getParam('key'))) {
            $croping = $this->_imageContainer->getImage($this->_getParam('key'));

            $motiv = Doctrine_Query::create()
                ->from('Motiv c')
                ->where('c.uuid = ?', array($croping['motiv_id']))->fetchOne();
        } else {
            if (Zend_Auth::getInstance()->hasIdentity()) {
                $motiv = Doctrine_Query::create()
                    ->from('Motiv c')
                    ->where('c.uuid = ?', array($this->_getParam('key')))->fetchOne();
            } else {
                $images = new Zend_Session_Namespace('amfmotive');

                if (!isset($images->motive[$this->_getParam('key')])) {
                    $motiv = Doctrine_Query::create()
                        ->from('Motiv c')
                        ->where('c.uuid = ?', array($this->_getParam('key')))->fetchOne();
                } else {
                    $motiv = $images->motive[$this->_getParam('key')];
                }
            }
        }

        if ((isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on')) {
            $url = 'https://' . $_SERVER["SERVER_NAME"] . '/market/';
        } else {
            $url = 'http://' . $_SERVER["SERVER_NAME"] . '/market/';
        }
        if (isset($motiv['install_id'])) {
            $urlM = $url . 'motive/';
        } else {
            $urlM = $url . 'guest/';
        }
        $this->view->url = $urlM . '/' . $motiv['file_mid'];
    }

    public function mymotiveAction()
    {
        $this->view->clearVars();
        if (Zend_Auth::getInstance()->hasIdentity()) {
            $user = Zend_Auth::getInstance()->getIdentity();
            $motive = Doctrine_Query::create()
                ->from('Motiv m')
                ->leftJoin("m.ContactMotiv c")
                ->where('c.contact_id = ? AND m.shop_id = ?')->execute(array($user['id'], $this->shop->id));

        } else {
            $images = new Zend_Session_Namespace('amfmotive');

            $images->unlock();

            if (!$images->motive) {
                $images->motive = array();
            }

            $motivBasket = new TP_FavMotiv();

            $motiv = Doctrine_Query::create()
                ->from('Motiv c')
                ->where('c.id = ' . implode(' OR c.id = ', array_values($motivBasket->getMotive())))
                ->execute();

            $motive = array_merge($images->motive, $motiv->toArray(false));
        }

        if ((isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on')) {
            $url = 'https://' . $_SERVER["SERVER_NAME"] . '/market/';
        } else {
            $url = 'http://' . $_SERVER["SERVER_NAME"] . '/market/';
        }

        $this->view->success = true;
        $this->view->result = array();
        foreach ($motive as $motiv) {
            if (isset($motiv['install_id'])) {
                $urlM = $url . 'motive/';
            } else {
                $urlM = $url . 'guest/';
            }
            $this->view->result[] = array(
                'uuid' => $motiv['uuid'],
                'title' => $motiv['title'],
                'thumbnail' => $motiv['file_thumb'],
                'url' => $urlM
            );

        }
    }

    public function getmymotiveAction()
    {
        $this->view->clearVars();
        if (Zend_Auth::getInstance()->hasIdentity()) {
            $user = Zend_Auth::getInstance()->getIdentity();
            $motive = Doctrine_Query::create()
                ->from('Motiv m')
                ->leftJoin("m.ContactMotiv c")
                ->where('c.contact_id = ? AND m.shop_id = ?')->orderBy("m.id DESC")->limit($this->_getParam("count", 20))->execute(array($user['id'], $this->shop->id));
        } else {
            $images = new Zend_Session_Namespace('amfmotive');

            $images->unlock();

            if (!$images->motive) {
                $images->motive = array();
            }

            $motivBasket = new TP_FavMotiv();

            $motiv = Doctrine_Query::create()
                ->from('Motiv c')
                ->where('c.id = ' . implode(' OR c.id = ', array_values(array_reverse($motivBasket->getMotive()))))
                ->execute();

            $motive = array_merge(array_reverse($images->motive), $motiv->toArray(false));
        }

        if ((isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on')) {
            $url = 'https://' . $_SERVER["SERVER_NAME"] . '/market/';
        } else {
            $url = 'http://' . $_SERVER["SERVER_NAME"] . '/market/';
        }

        $this->view->success = true;
        $this->view->result = array();
        foreach ($motive as $motiv) {
            if (isset($motiv['install_id'])) {
                $urlM = $url . 'motive/';
            } else {
                $urlM = $url . 'guest/';
            }
            $this->view->result[] = array(
                'uuid' => $motiv['uuid'],
                'title' => $motiv['title'],
                'thumbnail' => $motiv['file_thumb'],
                'url' => $urlM
            );

        }
    }

    public function getimageAction()
    {

        $image = $this->_imageContainer->getImage($this->_getParam('key'));

        $img = new imagick($image['dir']);
        $d = $img->getImageGeometry();
        $w = $d['width'];
        $h = $d['height'];

        $this->view->motive = array();
        $this->view->motive[] = array('width' => $w, 'height' => $h, 'delete_url' => 'javascript:deleteStepUpload(\'' . $image['id'] . '\',\'' . $source['key'] . '\');', 'id' => $image['id'], 'key' => $image['key'],
            'name' => $image['name'], 'size' => filesize($image['dir']));
    }

    public function getthumbnailAction()
    {
        $image = $this->_imageContainer->getImage($this->_getParam('key'));

        require_once(APPLICATION_PATH . '/helpers/Image.php');
        $img = new TP_View_Helper_Image();
        if ($image['crop']) {
            $this->view->url = $img->image()->thumbnailDesigner($image['key'], 'stepcrop', $image['crop'], true);
        } else {
            $this->view->url = $img->image()->thumbnailDesigner($image['key'], 'stepcrop', $image['dir'], true);
        }

    }

    public function savecropAction()
    {
        $source = $this->_getParam('source');
        $c = $this->_getParam('c');

        $image = $this->_imageContainer->getImage($source['key']);

        $img = new imagick($image['dir']);
        $d = $img->getImageGeometry();
        $w = $d['width'];
        $h = $d['height'];

        $ratio = $w / $source['width'];

        $img->cropImage(intval($c['w'] * $ratio), intval($c['h'] * $ratio), intval($c['x'] * $ratio), intval($c['y'] * $ratio));

        $cropfile = PUBLIC_PATH . '/steplayouter/uploads/temp/' . $source['key'] . '_crop.png';
        $img->writeimage($cropfile);
        $image['crop'] = $cropfile;
        $this->_imageContainer->updateImage($source['key'], $image);
        $this->view->success = true;
        $this->view->key = $source['key'];
        $this->view->motive = array();
        $this->view->motive[] = array('width' => $w, 'height' => $h, 'delete_url' => 'javascript:deleteStepUpload(\'' . $image['id'] . '\',\'' . $source['key'] . '\');', 'id' => $image['id'], 'key' => $source['key'],
            'name' => $image['name'], 'size' => filesize($image['dir']));
    }

    public function uploadAction()
    {
        $this->view->clearVars();

        $adapter = new Zend_File_Transfer_Adapter_Http();

        $adapter->addValidator('Extension', false, $this->_extAllow);

        $id = TP_Util::uuid();

        if (!file_exists(PUBLIC_PATH . '/steplayouter/uploads/temp')) {
            mkdir(PUBLIC_PATH . '/steplayouter/uploads/temp', 0777, true);
        }

        $adapter->setDestination(PUBLIC_PATH . '/steplayouter/uploads/temp');

        $orginalFilename = $adapter->getFileName(null, false);

        $adapter->addFilter('Rename', PUBLIC_PATH . '/steplayouter/uploads/temp/' . $id . '_' . $adapter->getFileName(null, false));

        $this->view->motive = array();
        $this->view->error = false;
        $this->view->message = 'Upload Finish';

        if (!$adapter->receive()) {
            $error = $adapter->getMessages();
            if (isset($error['fileExtensionFalse'])) {
                $arguments = array('error' => true, 'message' => $error['fileExtensionFalse']);
                $this->view->error = false;
                $this->view->message = $error['fileExtensionFalse'];
            } else {
                $arguments = array('error' => true, 'message' => 'Upload Error');
                $this->view->error = false;
                $this->view->message = 'Upload Error';
            }
            return;
        }

        $img = new imagick(PUBLIC_PATH . '/steplayouter/uploads/temp/' . $adapter->getFileName(null, false));
        $d = $img->getImageGeometry();
        $w = $d['width'];
        $h = $d['height'];

        $this->view->motive[] = array('width' => $w, 'height' => $h, 'delete_url' => 'javascript:deleteStepUpload(\'' . $this->_getParam('id', '') . '\',\'' . $id . '\');', 'id' => $this->_getParam('id', ''), 'key' => $id,
            'name' => $orginalFilename, 'size' => filesize(PUBLIC_PATH . '/steplayouter/uploads/temp/' . $adapter->getFileName(null, false)));

        $this->_imageContainer->addImage(PUBLIC_PATH . '/steplayouter/uploads/temp/' . $adapter->getFileName(null, false), $this->_getParam('id', ''), $id, $orginalFilename);
    }

    public function uploadmotivAction()
    {
        $this->view->clearVars();

        if (Zend_Auth::getInstance()->hasIdentity()) {

            $user = Zend_Auth::getInstance()->getIdentity();
            $user = Doctrine_Query::create()
                ->from('Contact m')
                ->where('id = ?')->fetchOne(array($user['id']));

            $shop = Zend_Registry::get('shop');

            if ($shop['market']) {
                $shopMarket = Doctrine_Query::create()->from('Shop c')->where('c.id = ?', array($user->Install->defaultmarket))->fetchOne();
            } else {
                $shopMarket = $shop;
            }

            $adapter = new Zend_File_Transfer_Adapter_Http();

            $adapter->addValidator('Extension', false, $this->_extAllow);

            $id = TP_Util::uuid();

            if (!file_exists(APPLICATION_PATH . '/../market/motive/' . $user->uuid)) {
                mkdir(APPLICATION_PATH . '/../market/motive/' . $user->uuid);
            }

            $adapter->setDestination(APPLICATION_PATH . '/../market/motive/' . $user->uuid);

            $orginalFilename = $adapter->getFileName(null, false);

            $adapter->addFilter('Rename', APPLICATION_PATH . '/../market/motive/' . $user->uuid . '/' . $id . '_' . $adapter->getFileName(null, false));

            if (!$adapter->receive()) {
                $error = $adapter->getMessages();
                if (isset($error['fileExtensionFalse'])) {
                    $arguments = array('error' => true, 'message' => $error['fileExtensionFalse']);
                    $this->view->error = false;
                    $this->view->message = $error['fileExtensionFalse'];
                } else {
                    $arguments = array('error' => true, 'message' => 'Upload Error');
                    $this->view->error = false;
                    $this->view->message = 'Upload Error';
                }
                return;
            }

            $im = new imagick();
            $im->readImage(APPLICATION_PATH . '/../market/motive/' . $user->uuid . '/' . $adapter->getFileName(null, false));
            $res = $im->getImageResolution();
            $res['x'] = intval($res['x']);
            if ($res['x'] != '300') {
                $res['x'] = 300;
                $im->setImageResolution(300, 300);
            }

            if ($im->getImageUnits() == 2) {
                $im->setImageUnits(1);
                $res['x'] = $res['x'] * 2.54;
            } elseif ($im->getImageUnits() == 0) {
                $im->setImageUnits(1);
            }
            $im->setimagecompressionquality(100);
            $im->setcompressionquality(100);
            $imgwork = $im->clone();
            $imgwork->writeImage(APPLICATION_PATH . '/../market/motive/' . $user->uuid . '/work_' . $adapter->getFileName(null, false));
            $imgmid = $im->clone();
            $imgmid->thumbnailImage(500, null);
            $imgmid->writeImage(APPLICATION_PATH . '/../market/motive/' . $user->uuid . '/mid_' . $adapter->getFileName(null, false));
            $imgthumb = $im->clone();
            $imgthumb->thumbnailImage(null, 110);
            $imgthumb->writeImage(APPLICATION_PATH . '/../market/motive/' . $user->uuid . '/thumb_' . $adapter->getFileName(null, false));

            $shop = Zend_Registry::get('shop');

            $motive = new Motiv();
            $motive->Account = $user->Account;
            $motive->shop_id = $shop['id'];
            $motive->Contact = $user;

            $user->motiv_count = $user->motiv_count + 1;
            $user->save();

            $motive->Install = $user->Install;
            $motive->resale_shop = false;
            $motive->resale_market = false;
            $motive->resale_download = false;

            $motive->title = $orginalFilename;
            $motive->dpi_org = $res['x'];
            $motive->dpi_con = '300';
            $motive->orgfilename = $orginalFilename;

            $motive->file_orginal = $user->uuid . '/' . $adapter->getFileName(null, false);
            $motive->file_work = $user->uuid . '/work_' . $adapter->getFileName(null, false);
            $motive->file_mid = $user->uuid . '/mid_' . $adapter->getFileName(null, false);
            $motive->file_thumb = $user->uuid . '/thumb_' . $adapter->getFileName(null, false);
            $motive->status = $shopMarket['default_motiv_status'];
            $motive->width = $im->getImageWidth();
            $motive->height = $im->getImageHeight();
            $motive->mid_width = $imgmid->getImageWidth();
            $motive->mid_height = $imgmid->getImageHeight();
            $motive->typ = $im->getFormat();
            $motive->copyright = $im->getImageProperty('exif:Copyright');
            $motive->exif = Zend_Json::encode($im->getImageProperties("exif:*"));
            $motive->save();

            $contactmotiv = new ContactMotiv();
            $contactmotiv->contact_id = $user->id;
            $contactmotiv->motiv_id = $motive->id;
            $contactmotiv->save();

            $uuid = $motive->uuid;

        } else {

            $adapter = new Zend_File_Transfer_Adapter_Http();

            $adapter->addValidator('Extension', false, $this->_extAllow);

            $id = TP_Util::uuid();

            if (!file_exists(APPLICATION_PATH . '/../market/guest/' . Zend_Session::getId())) {
                mkdir(APPLICATION_PATH . '/../market/guest/' . Zend_Session::getId());
            }

            $adapter->setDestination(APPLICATION_PATH . '/../market/guest/' . Zend_Session::getId());

            $orginalFilename = $adapter->getFileName(null, false);

            $adapter->addFilter('Rename', APPLICATION_PATH . '/../market/guest/' . Zend_Session::getId() . '/' . $id . '_' . $adapter->getFileName(null, false));

            if (!$adapter->receive()) {
                $error = $adapter->getMessages();
                if (isset($error['fileExtensionFalse'])) {
                    $arguments = array('error' => true, 'message' => $error['fileExtensionFalse']);
                    $this->view->error = false;
                    $this->view->message = $error['fileExtensionFalse'];
                } else {
                    $arguments = array('error' => true, 'message' => 'Upload Error');
                    $this->view->error = false;
                    $this->view->message = 'Upload Error';
                }
                return;
            }

            $im = new imagick();
            $im->readImage(APPLICATION_PATH . '/../market/guest/' . Zend_Session::getId() . '/' . $adapter->getFileName(null, false));
            $res = $im->getImageResolution();
            $res['x'] = intval($res['x']);
            if ($res['x'] != '300') {
                $res['x'] = 300;
                $im->setImageResolution(300, 300);
            }

            if ($im->getImageUnits() == 2) {
                $im->setImageUnits(1);
                $res['x'] = $res['x'] * 2.54;
            } elseif ($im->getImageUnits() == 0) {
                $im->setImageUnits(1);
            }
            $im->setimagecompressionquality(100);
            $im->setcompressionquality(100);
            $imgwork = $im->clone();
            $imgwork->writeImage(APPLICATION_PATH . '/../market/guest/' . Zend_Session::getId() . '/work_' . $adapter->getFileName(null, false));
            $imgmid = $im->clone();
            $imgmid->thumbnailImage(500, null);
            $imgmid->writeImage(APPLICATION_PATH . '/../market/guest/' . Zend_Session::getId() . '/mid_' . $adapter->getFileName(null, false));
            $imgthumb = $im->clone();
            $imgthumb->thumbnailImage(null, 110);
            $imgthumb->writeImage(APPLICATION_PATH . '/../market/guest/' . Zend_Session::getId() . '/thumb_' . $adapter->getFileName(null, false));

            $images = new Zend_Session_Namespace('amfmotive');

            $images->unlock();

            if (!$images->motive) {
                $images->motive = array();
            }

            $uuid = TP_Util::uuid();
            if ($uid != "") {
                $uuid = $uid;
            }
            $images->motive[$uuid] = array(
                'file_orginal' => Zend_Session::getId() . '/' . $adapter->getFileName(null, false),
                'file_work' => Zend_Session::getId() . '/work_' . $adapter->getFileName(null, false),
                'file_mid' => Zend_Session::getId() . '/mid_' . $adapter->getFileName(null, false),
                'file_thumb' => Zend_Session::getId() . '/thumb_' . $adapter->getFileName(null, false),
                'width' => $im->getImageWidth(),
                'height' => $im->getImageHeight(),
                'mid_width' => $imgmid->getImageWidth(),
                'mid_height' => $imgmid->getImageHeight(),
                'copyright' => $im->getImageProperty('exif:Copyright'),
                'dpi_org' => $res['x'],
                'dpi_con' => '300',
                'typ' => $im->getImageType(),
                'title' => $orginalFilename,
                'uuid' => $uuid,
                'org' => $adapter->getFileName(null, false),
                'orgfilename' => $orginalFilename,
                'exif' => Zend_Json::encode($im->getImageProperties("exif:*"))
            );

            $motive = $images->motive[$uuid];

            $motivBasket = new TP_FavMotiv();
            $motivBasket->addMotivUUID($uuid);

            $images->lock();
        }

        $this->view->success = true;
        $this->view->uuid = $uuid;
        $this->view->id = $this->_getParam('id');


        $userAgent = new Zend_Http_UserAgent();
        $device    = $userAgent->getDevice();
        $this->view->browser = $device->getBrowser();
        $this->view->version = $device->getBrowserVersion();

        if($device->getBrowser() == "Internet Explorer" && $device->getBrowserVersion() < 10) {
            $this->getResponse()
                ->setHeader('Content-Type', 'text/plain');
        }
    }

    public function getconfigAction()
    {
        $defaultdata = array();
        if ($this->_getParam('mode', 3) == 2) {

            $layouterSession = new TP_Layoutersession();
            $layouterArt = $layouterSession->getLayouterArticle($this->_getParam('uuid'));
            $defaultdata = json_decode($layouterArt->getDesignerXML(), true);
            $this->view->layerdata = $defaultdata['jsonLayerData'];
            $this->view->createdata = $defaultdata['jsonCreateData'];
            $defaultdata = $defaultdata['jsonSaveData'];
            $this->view->defaultdata = $defaultdata;
            $article = Doctrine_Query::create()->from('Article c')->where('c.install_id = ? AND c.enable = 1 AND c.uuid = ?', array($this->install->id, $layouterArt->getOrgArticleId()))->fetchOne();

        } else {
            $article = Doctrine_Query::create()->from('Article c')->where('c.install_id = ? AND c.enable = 1 AND c.uuid = ?', array($this->install->id, $this->_getParam('uuid')))->fetchOne();

            if ($article->upload_steplayouter_data != "") {
                $defaultdata = json_decode($article->upload_steplayouter_data, true);
                $this->view->layerdata = $defaultdata['jsonLayerData'];
                $this->view->createdata = $defaultdata['jsonCreateData'];
                $defaultdata = $defaultdata['jsonSaveData'];
                $this->view->defaultdata = $defaultdata;
            }
        }



        $this->view->success = true;

        $shop = Zend_Registry::get('shop');

        $conf = simplexml_load_string($article->upload_steplayouter_xml);

        $this->view->config = array(
            'version' => (float)$conf->version,
            'includes' => array(),
            'general' => array('buttons' => array(), 'steps' => 'header'),
            'fields' => array(
                'motive' => array()
            ),
            'layer' => array(),
            'steps' => array(),
            'fonts' => array(
                array('id' => 'Helvetica', 'name' => 'Helvetica', 'rte' => "Helvetica")
            )
        );

        foreach ($conf->rendering->layers->layer as $layer) {
            if ((int)$layer['scale'] == 0) {
                $layer['scale'] = 1;
            }
            if (!isset($layer['hidden'])) {
                $layer['hidden'] = 1;
            }
            $this->view->config['layer'][(string)$layer['id']] = array(
                'name' => (string)$layer['name'],
                'dragable' => (bool)intval($layer['dragable']),
                'scaleable' => (bool)intval($layer['scaleable']),
                'rotateable' => (bool)intval($layer['rotateable']),
                'clipbox' => (string)$layer['clipbox'],
                'rotate' => (int)$layer['rotate'],
                'scale' => (int)$layer['scale'],
                'fit' => (int)$layer['fit'],
                'hidden' => (bool)intval($layer['hidden'])
            );
        }

        $shop = Zend_Registry::get('shop');

        if ($shop['market']) {
            $shop = Zend_Registry::get('market_shop');
        }
        $searchpath = APPLICATION_PATH . '/design/clients/' . $shop['uid'] . '/fonts';

        if (file_exists($searchpath)) {

            $files = array();

            foreach (new DirectoryIterator($searchpath) as $fileInfo) {
                if (in_array(str_replace("-", " ", str_ireplace(array("-Bold-Italic","-Italic","-Black","-Bold","-Regular"), array("","","","","") , $fileInfo->getBasename('.ttf'))), $files) || $fileInfo->isDot() || $fileInfo->getExtension() == "PFB" || $fileInfo->getExtension() == "PFM" ||$fileInfo->getBasename('.php') == "settings" || strpos("t".$fileInfo->getBasename('.ttf'), "Neo Sans") || $fileInfo->getBasename('.ttf') == "verdanai" || $fileInfo->getBasename('.ttf') == "verdanab" || $fileInfo->getBasename('.ttf') == "verdanaz") {
                    continue;
                }
                $this->view->config['fonts'][] = array(
                    'id' => str_replace("-", " ", str_ireplace(array("-Bold-Italic","-Italic","-Black","-Bold","-Regular"), array("","","","","") , $fileInfo->getBasename('.ttf'))),
                    'name' => str_replace("-", " ", str_ireplace(array("-Bold-Italic","-Italic","-Black","-Bold","-Regular"), array("","","","","") , $fileInfo->getBasename('.ttf'))),
                    'rte'  => str_replace("-", " ", str_ireplace(array("-Bold-Italic","-Italic","-Black","-Bold","-Regular"), array("","","","","") , $fileInfo->getBasename('.ttf')))
                );

                $files[] = str_replace("-", " ", str_ireplace(array("-Bold-Italic","-Italic","-Black","-Bold","-Regular"), array("","","","","") , $fileInfo->getBasename('.ttf')));
            }

        }

        foreach ($conf->includes->include as $include) {
            $this->view->config['includes'][] = array('type' => (string)$include['type'], 'file' => (string)$include);
        }

        if (isset($conf->general->steps)) {
            $this->view->config['general']['steps'] = (string)$conf->general->steps;
        }
        $this->view->config['general']['size'] = array('width' => (int)$conf->general->size['width'], 'height' => (int)$conf->general->size['height']);
        $this->view->config['general']['headline'] = (string)$conf->general->headline;

        foreach ($conf->general->buttons->children() as $key => $button) {
            $btn = array('id' => $key, 'class' => '');

            if (isset($button['class'])) {
                $btn['cssClass'] = (string)$button['class'];
            }
            if (isset($button['label'])) {
                $btn['label'] = (string)$button['label'];
            }
            $this->view->config['general']['buttons'][] = $btn;
        }

        foreach ($conf->steps->children() as $step) {

            $st = array();

            $st['id'] = (int)$step['id'];
            $st['label'] = (string)$step['label'];
            $st['headline'] = (string)$step->headline;
            if ($step->step_headline) {
                $st['step_headline'] = (string)$step->step_headline;
            } else {
                $st['step_headline'] = "";
            }

            $st['description'] = (string)$step->description;
            $st['require'] = 0;
            if (isset($step['require']) && (int)$step['require'] == 1) {
                $st['require'] = 1;
            }

            if (isset($step['layout']) && (string)$step['layout']) {
                $st['layout'] = (string)$step['layout'];
                if (isset($step['columns']) && (string)$step['columns']) {
                    $st['columns'] = (string)$step['columns'];
                } else {
                    $st['columns'] = '2';
                }
            } else {
                $st['layout'] = 'normal';
                $st['columns'] = '1';
            }

            $st['elements'] = array();

            foreach ($step->elements->children() as $keyelement => $element) {

                $el = array();
                $el['id'] = (string)$element['id'];
                $el['type'] = (string)$keyelement;

                if (trim((string)$element) != "") {
                    $el['value'] = (string)$element;
                } else {
                    $el['value'] = str_replace("|", "\r\n", (string)$element['value']);
                }

                if (isset($defaultdata[$el['id']])) {
                    $el['value'] = $defaultdata[$el['id']];
                }

                if ((string)$keyelement == "formuploadmotiv" && (string)$element['id'] != "sponsord-by" && !$this->recursive_array_search((string)$element['id'], $this->view->config['fields']['motive'])) {
                    $this->view->config['fields']['motive'][] = array(
                        'value' => $el['value'],
                        'id' => (string)$element['id']
                    );
                }

                $el['data-binding'] = (string)$element['data-binding'];
                $el['label'] = (string)$element['label'];
                $el['alternatelabel'] = "";
                if (isset($element['alternate-label']) && (string)$element['alternate-label'] != "") {
                    $el['alternatelabel'] = (string)$element['alternate-label'];
                }
                if (isset($element['width']) && (string)$element['width']) {
                    $el['width'] = (string)$element['width'];
                }
                if (isset($element['render-site']) && (string)$element['render-site']) {
                    $el['rendersite'] = (string)$element['render-site'];
                }

                if (isset($element['text']) && (string)$element['text']) {
                    $el['text'] = (string)$element['text'];
                }

                if (isset($element['height']) && (string)$element['height']) {
                    $el['height'] = (string)$element['height'];
                }

                if (isset($element['base-dir']) && (string)$element['base-dir']) {
                    $el['basedir'] = (string)$element['base-dir'];
                }

                if (isset($element['column']) && (string)$element['column']) {
                    $el['column'] = (string)$element['column'];
                } else {
                    $el['column'] = '1';
                }

                if (isset($element['modal']) && (int)$element['modal']) {
                    $el['modal'] = (int)$element['modal'];
                } else {
                    $el['modal'] = '0';
                }

                if (isset($element['select-color']) && (string)$element['select-color']) {
                    $el['selectcolor'] = (int)$element['select-color'];
                } else {
                    $el['selectcolor'] = 0;
                }

                if (isset($element['mode']) && (string)$element['mode'] && isset($element['container']) && (string)$element['container']) {

                    $install = Zend_Registry::get('install');
                    if ($install['colordb'] != "") {
                        $colors = simplexml_load_string($install['colordb']);

                        $colorsTemp = array();
                        $colors = $colors->xpath('//item[@id="' . (string)$element['container'] . '"]');
                        foreach ($colors [0]->color as $color) {
                            $colorsTemp[] = array(
                                'id' => (string)$color['id'],
                                'label' => (string)$color['label']
                            );
                        }

                        $el['colordb'] = $colorsTemp;
                    }
                }

                if (isset($element['select-bold']) && (string)$element['select-bold']) {
                    $el['selectbold'] = (int)$element['select-bold'];
                } else {
                    $el['selectbold'] = 0;
                }

                if (isset($element['select-underline']) && (string)$element['select-underline']) {
                    $el['selectunderline'] = (int)$element['select-underline'];
                } else {
                    $el['selectunderline'] = 0;
                }

                if (isset($element['select-italic']) && (string)$element['select-italic']) {
                    $el['selectitalic'] = (int)$element['select-color'];
                } else {
                    $el['selectitalic'] = 0;
                }

                if (isset($element['select-font']) && (string)$element['select-font']) {
                    $el['selectfont'] = (int)$element['select-font'];
                } else {
                    $el['selectfont'] = 0;
                }
                if (isset($element['select-size']) && (string)$element['select-size']) {
                    $el['selectsize'] = (int)$element['select-size'];
                } else {
                    $el['selectsize'] = 0;
                }
                if (isset($element['select-align']) && (string)$element['select-align']) {
                    $el['selectalign'] = (int)$element['select-align'];
                } else {
                    $el['selectalign'] = 0;
                }

                if (isset($element['render-font-color']) && (string)$element['render-font-color']) {
                    $el['renderfontcolor'] = (string)$element['render-font-color'];
                } else {
                    $el['renderfontcolor'] = "#000000";
                }
                if (isset($element['render-pos-x']) && (int)$element['render-pos-x']) {
                    $el['renderposx'] = (int)$element['render-pos-x'];
                } else {
                    $el['renderposx'] = 0;
                }

                if (isset($element['render-pos-y']) && (int)$element['render-pos-y']) {
                    $el['renderposy'] = (int)$element['render-pos-y'];
                } else {
                    $el['renderposy'] = 0;
                }

                if (isset($element['render-pos-width']) && (int)$element['render-pos-width']) {
                    $el['renderposwidth'] = (int)$element['render-pos-width'];
                } else {
                    $el['renderposwidth'] = 0;
                }

                if (isset($element['render-pos-height']) && (int)$element['render-pos-height']) {
                    $el['renderposheight'] = (int)$element['render-pos-height'];
                } else {
                    $el['renderposheight'] = 0;
                }

                if (isset($element['render-pos-layer']) && (int)$element['render-pos-layer']) {
                    $el['renderposlayer'] = (int)$element['render-pos-layer'];
                } else {
                    $el['renderposlayer'] = 0;
                }
                if (isset($element['render-site']) && (int)$element['render-site']) {
                    $el['renderpossite'] = (int)$element['render-site'];
                } else {
                    $el['renderpossite'] = 0;
                }
                if (isset($element['render-font-line-height']) && (string)$element['render-font-line-height']) {
                    $el['renderfontlineheight'] = (string)$element['render-font-line-height'];
                } else {
                    $el['renderfontlineheight'] = 0;
                }
                if (isset($element['rte-background']) && (string)$element['rte-background']) {
                    $el['rtebackground'] = (string)$element['rte-background'];
                } else {
                    $el['rtebackground'] = "#ffffff";
                }
                if (isset($element['render-pos-sort']) && (int)$element['render-pos-sort']) {
                    $el['renderpossort'] = (int)$element['render-pos-sort'];
                } else {
                    $el['renderpossort'] = 0;
                }
                if (isset($element['create-rte']) && (int)$element['create-rte']) {
                    $el['createrte'] = (int)$element['create-rte'];
                } else {
                    $el['createrte'] = 0;
                }
                if (isset($element['create-text']) && (int)$element['create-text']) {
                    $el['createtext'] = (int)$element['create-text'];
                } else {
                    $el['createtext'] = 0;
                }
                if (isset($element['create-motiv-upload']) && (int)$element['create-motiv-upload']) {
                    $el['createmotivupload'] = (int)$element['create-motiv-upload'];
                } else {
                    $el['createmotivupload'] = 0;
                }
                if (isset($element['render-font-size']) && (string)$element['render-font-size']) {
                    $el['renderfontsize'] = (int)$element['render-font-size'];
                } else {
                    $el['renderfontsize'] = "15";
                }
                if (isset($element['class']) && (string)$element['class']) {
                    $el['cls'] = (string)$element['class'];
                } else {
                    $el['cls'] = "";
                }
                if (isset($element['event']) && (string)$element['event']) {
                    $el['event'] = (string)$element['event'];
                } else {
                    $el['event'] = "";
                }
                if ((isset($element['selectupload']) && (int)$element['selectupload']) ||
                    !isset($element['selectupload']) ||
                    (isset($element['selectgallery']) && (int)$element['selectgallery']) ||
                    !isset($element['selectgallery'])
                ) {
                    $el['motivElementDisabled'] = 0;
                }else{
                    $el['motivElementDisabled'] = 1;
                }
                if (isset($element['render-font-align']) && (string)$element['render-font-align']) {
                    $el['renderfontalign'] = (string)$element['render-font-align'];
                } else {
                    $el['renderfontalign'] = "left";
                }
                if (isset($element['render-font-bold']) && (string)$element['render-font-bold']) {
                    $el['renderfontbold'] = (int)$element['render-font-bold'];
                } else {
                    $el['renderfontbold'] = "0";
                }
                if (isset($element['render-font-underline']) && (string)$element['render-font-underline']) {
                    $el['renderfontunderline'] = (int)$element['render-font-underline'];
                } else {
                    $el['renderfontunderline'] = "0";
                }
                if (isset($element['help']) && (string)$element['help']) {
                    $el['help'] = (string)$element['help'];
                } else {
                    $el['help'] = "";
                }
                if (isset($element['render-font-italic']) && (string)$element['render-font-italic']) {
                    $el['renderfontitalic'] = (int)$element['render-font-italic'];
                } else {
                    $el['renderfontitalic'] = "0";
                }
                if (isset($element['render-font-family']) && (string)$element['render-font-family']) {
                    $el['renderfontfamily'] = (string)$element['render-font-family'];
                } else {
                    $el['renderfontfamily'] = "Helvetica";
                }
                if (isset($element['render-pos-layer']) && (string)$element['render-pos-layer']) {
                    $el['selectlayer'] = (int)$element['render-pos-layer'];
                } else {
                    $el['selectlayer'] = 0;
                }
                if (isset($element['selectgallery']) && (string)$element['selectgallery']) {
                    $el['selectgallery'] = (int)$element['selectgallery'];
                } else {
                    $el['selectgallery'] = 0;
                }
                if (isset($element['style']) && (string)$element['style']) {
                    $el['style'] = (string)$element['style'];
                } else {
                    $el['style'] = "";
                }
                if (isset($element->options)) {
                    $el['options'] = array();
                    foreach ($element->options->children() as $keyoption => $option) {
                        $opt = array(
                            'label' => (string)$option['label'],
                            'value' => (string)$option['id']
                        );
                        if (isset($option['thumbnail']) && (string)$option['thumbnail']) {
                            $opt['thumbnail'] = (string)$option['thumbnail'];
                        } else {
                            $opt['thumbnail'] = 0;
                        }
                        if (isset($option['motiv']) && (string)$option['motiv']) {
                            $motiv = Doctrine_Query::create()->from('Motiv c')->where('c.uuid = ?', array($option['motiv']))->fetchOne();
                            $opt['thumbnail'] = $motiv->file_thumb;
                        }
                        if (isset($option['motiv-mid']) && (string)$option['motiv-mid']) {
                            $motiv = Doctrine_Query::create()->from('Motiv c')->where('c.uuid = ?', array($option['motiv-mid']))->fetchOne();
                            $opt['thumbnail'] = $motiv->file_mid;
                        }
                        if (isset($option['style']) && (string)$option['style']) {
                            $opt['style'] = (string)$option['style'];
                        } else {
                            $opt['style'] = 0;
                        }
                        $opt['elements'] = $this->buildSubElements($option->elements->children(), $defaultdata);

                        $el['options'][] = $opt;

                    }
                }

                if ($el['id'] == 'substeps') {

                    $el['steps'] = array();
                    foreach ($element->steps->children() as $substep) {

                        $subt = array();

                        $subt['id'] = (int)$substep['id'];
                        $subt['label'] = (string)$substep['label'];
                        $subt['headline'] = (string)$substep->headline;
                        $subt['description'] = (string)$substep->description;

                        if (isset($substep['layout']) && (string)$substep['layout']) {
                            $subt['layout'] = (string)$substep['layout'];
                            if (isset($substep['columns']) && (string)$substep['columns']) {
                                $subt['columns'] = (string)$substep['columns'];
                            } else {
                                $subt['columns'] = '2';
                            }
                        } else {
                            $subt['layout'] = 'normal';
                            $subt['columns'] = '1';
                        }

                        $subt['elements'] = $this->buildSubElements($substep->elements->children(), $defaultdata);

                        $el['steps'][] = $subt;
                    }
                }

                if ($el['id'] == 'formdesigntemplates') {
                    if ($shop['market']) {
                        if ($article->a6_org_article != 0 && $article->a6_org_article != "") {
                            $articles = Doctrine_Query::create()
                                ->from('Article c')
                                ->where('c.a6_org_article = ? AND c.shop_id = ? AND c.private = 0 AND c.enable = 1 AND c.resale_design = 1', array($article->a6_org_article, $shop['id']))
                                ->execute();
                        } else {
                            $articles = Doctrine_Query::create()
                                ->from('Article c')
                                ->where('c.a6_org_article = ? AND c.shop_id = ? AND c.private = 0 AND c.enable = 1 AND c.resale_design = 1', array($article->id, $shop['id']))
                                ->execute();
                        }
                    } else {
                        if ($article->a6_org_article != 0 && $article->a6_org_article != "") {
                            $articles = Doctrine_Query::create()
                                ->from('Article c')
                                ->where('(c.a6_org_article = ? AND c.private = 0 AND c.enable = 1 AND c.resale_design = 1 AND c.display_market = 1) OR (c.id = ?)', array($article->a6_org_article, $article->a6_org_article))
                                ->execute();
                        } else {
                            $articles = Doctrine_Query::create()
                                ->from('Article c')
                                ->where('(c.a6_org_article = ? AND c.private = 0 AND c.enable = 1 AND c.resale_design = 1 AND c.display_market = 1) OR (c.id = ?)', array($article->id, $article->id))
                                ->execute();
                        }
                    }

                    $el['options'] = array();

                    require_once APPLICATION_PATH . '/helpers/Image.php';
                    $image = new TP_View_Helper_Image();

                    foreach ($articles as $art) {
                        $opt = array(
                            'title' => $art->title,
                            'uuid' => $art->uuid
                        );
                        if ($art->file == "") {
                            $opt['thumbnail'] = $image->thumbnailFop($art->title, 'articlelist', $art->getMarketFile(), true, $art->id);
                        } else {
                            $opt['thumbnail'] = $image->thumbnailImage($art->title, 'articlelist', $art->file, true);
                        }

                        $el['options'][] = $opt;

                    }
                }

                $st['elements'][] = $el;
            }

            $this->view->config['steps'][] = $st;
        }

    }

    protected function buildSubElements($elements, $defaultdata)
    {
        $temp = array();
        foreach ($elements as $subkeyelement => $subelement) {

            $subel = array();
            $subel['id'] = (string)$subelement['id'];
            $subel['type'] = (string)$subkeyelement;
            if (trim((string)$subelement) != "") {
                $subel['value'] = (string)$subelement;
            } else {
                $subel['value'] = str_replace("|", "\r\n", (string)$subelement['value']);
            }
            if (isset($defaultdata[$subel['id']])) {
                $subel['value'] = $defaultdata[$subel['id']];
            }

            if ((string)$subkeyelement == "formuploadmotiv" && (string)$subelement['id'] != "sponsord-by" && !$this->recursive_array_search((string)$subelement['id'], $this->view->config['fields']['motive'])) {
                $this->view->config['fields']['motive'][] = array(
                    'value' => $subel['value'],
                    'id' => (string)$subelement['id']
                );
            }

            $subel['data-binding'] = (string)$subelement['data-binding'];
            $subel['label'] = (string)$subelement['label'];
            $subel['alternate-label'] = "";
            if (isset($subelement['alternatelabel']) && (string)$subelement['alternate-label'] != "") {
                $subel['alternatelabel'] = (string)$subelement['alternate-label'];
            }
            if (isset($subelement['width']) && (string)$subelement['width']) {
                $subel['width'] = (string)$subelement['width'];
            }
            if (isset($subelement['help']) && (string)$subelement['help']) {
                $subel['help'] = (string)$subelement['help'];
            } else {
                $subel['help'] = "";
            }
            if (isset($subelement['render-site']) && (string)$subelement['render-site']) {
                $subel['rendersite'] = (string)$subelement['render-site'];
            }

            if (isset($subelement['height']) && (string)$subelement['height']) {
                $subel['height'] = (string)$subelement['height'];
            }

            if (isset($subelement['base-dir']) && (string)$subelement['base-dir']) {
                $subel['basedir'] = (string)$subelement['base-dir'];
            }

            if (isset($subelement['column']) && (string)$subelement['column']) {
                $subel['column'] = (string)$subelement['column'];
            } else {
                $subel['column'] = '1';
            }

            if ((isset($subelement['selectupload']) && (int)$subelement['selectupload']) ||
                !isset($subelement['selectupload']) ||
                (isset($subelement['selectgallery']) && (int)$subelement['selectgallery']) ||
                !isset($subelement['selectgallery'])
            ) {
                $subel['motivElementDisabled'] = 0;
            }else{
                $subel['motivElementDisabled'] = 1;
            }

            if (isset($subelement['select-color']) && (string)$subelement['select-color']) {
                $subel['selectcolor'] = (int)$subelement['select-color'];
            } else {
                $subel['selectcolor'] = 0;
            }

            if (isset($subelement['select-bold']) && (string)$subelement['select-bold']) {
                $subel['selectbold'] = (int)$subelement['select-bold'];
            } else {
                $subel['selectbold'] = 0;
            }

            if (isset($subelement['select-underline']) && (string)$subelement['select-underline']) {
                $subel['selectunderline'] = (int)$subelement['select-underline'];
            } else {
                $subel['selectunderline'] = 0;
            }
            if (isset($subelement['render-font-line-height']) && (string)$subelement['render-font-line-height']) {
                $subel['renderfontlineheight'] = (string)$subelement['render-font-line-height'];
            } else {
                $subel['renderfontlineheight'] = 0;
            }
            if (isset($subelement['select-italic']) && (string)$subelement['select-italic']) {
                $subel['selectitalic'] = (int)$subelement['select-color'];
            } else {
                $subel['selectitalic'] = 0;
            }
            if (isset($subelement['modal']) && (int)$subelement['modal']) {
                $subel['modal'] = (int)$subelement['modal'];
            } else {
                $subel['modal'] = '0';
            }
            if (isset($subelement['select-font']) && (string)$subelement['select-font']) {
                $subel['selectfont'] = (int)$subelement['select-font'];
            } else {
                $subel['selectfont'] = 0;
            }
            if (isset($subelement['select-size']) && (string)$subelement['select-size']) {
                $subel['selectsize'] = (int)$subelement['select-size'];
            } else {
                $subel['selectsize'] = 0;
            }
            if (isset($subelement['select-align']) && (string)$subelement['select-align']) {
                $subel['selectalign'] = (int)$subelement['select-align'];
            } else {
                $subel['selectalign'] = 0;
            }
            if (isset($subelement['style']) && (string)$subelement['style']) {
                $subel['style'] = (string)$subelement['style'];
            } else {
                $subel['style'] = "";
            }
            if (isset($subelement['mode']) && (string)$subelement['mode'] && isset($subelement['container']) && (string)$subelement['container']) {

                $install = Zend_Registry::get('install');
                if ($install['colordb'] != "") {
                    $colors = simplexml_load_string($install['colordb']);

                    $colorsTemp = array();
                    $colors = $colors->xpath('//item[@id="' . (string)$subelement['container'] . '"]');
                    foreach ($colors [0]->color as $color) {
                        $colorsTemp[] = array(
                            'id' => (string)$color['id'],
                            'label' => (string)$color['label']
                        );
                    }

                    $subel['colordb'] = $colorsTemp;
                }
            }

            if (isset($subelement['render-font-color']) && (string)$subelement['render-font-color']) {
                $subel['renderfontcolor'] = (string)$subelement['render-font-color'];
            } else {
                $subel['renderfontcolor'] = "#000000";
            }
            if (isset($subelement['render-pos-x']) && (int)$subelement['render-pos-x']) {
                $subel['renderposx'] = (int)$subelement['render-pos-x'];
            } else {
                $subel['renderposx'] = 0;
            }
            if (isset($subelement['render-pos-y']) && (int)$subelement['render-pos-y']) {
                $subel['renderposy'] = (int)$subelement['render-pos-y'];
            } else {
                $subel['renderposy'] = 0;
            }
            if (isset($subelement['render-pos-width']) && (int)$subelement['render-pos-width']) {
                $subel['renderposwidth'] = (int)$subelement['render-pos-width'];
            } else {
                $subel['renderposwidth'] = 0;
            }
            if (isset($subelement['rte-background']) && (string)$subelement['rte-background']) {
                $subel['rtebackground'] = (string)$subelement['rte-background'];
            } else {
                $subel['rtebackground'] = "#ffffff";
            }
            if (isset($subelement['render-pos-height']) && (int)$subelement['render-pos-height']) {
                $subel['renderposheight'] = (int)$subelement['render-pos-height'];
            } else {
                $subel['renderposheight'] = 0;
            }
            if (isset($subelement['render-pos-layer']) && (int)$subelement['render-pos-layer']) {
                $subel['renderposlayer'] = (int)$subelement['render-pos-layer'];
            } else {
                $subel['renderposlayer'] = 0;
            }
            if (isset($subelement['render-site']) && (int)$subelement['render-site']) {
                $subel['renderpossite'] = (int)$subelement['render-site'];
            } else {
                $subel['renderpossite'] = 0;
            }
            if (isset($subelement['render-pos-sort']) && (int)$subelement['render-pos-sort']) {
                $subel['renderpossort'] = (int)$subelement['render-pos-sort'];
            } else {
                $subel['renderpossort'] = 0;
            }
            if (isset($subelement['create-rte']) && (int)$subelement['create-rte']) {
                $subel['createrte'] = (int)$subelement['create-rte'];
            } else {
                $subel['createrte'] = 0;
            }
            if (isset($subelement['create-text']) && (int)$subelement['create-text']) {
                $subel['createtext'] = (int)$subelement['create-text'];
            } else {
                $subel['createtext'] = 0;
            }
            if (isset($subelement['create-motiv-upload']) && (int)$subelement['create-motiv-upload']) {
                $subel['createmotivupload'] = (int)$subelement['create-motiv-upload'];
            } else {
                $subel['createmotivupload'] = 0;
            }

            if (isset($subelement['render-font-size']) && (string)$subelement['render-font-size']) {
                $subel['renderfontsize'] = (int)$subelement['render-font-size'];
            } else {
                $subel['renderfontsize'] = "15";
            }
            if (isset($subelement['render-font-align']) && (string)$subelement['render-font-align']) {
                $subel['renderfontalign'] = (string)$subelement['render-font-align'];
            } else {
                $subel['renderfontalign'] = "left";
            }
            if (isset($subelement['render-font-bold']) && (string)$subelement['render-font-bold']) {
                $subel['renderfontbold'] = (int)$subelement['render-font-bold'];
            } else {
                $subel['renderfontbold'] = "0";
            }
            if (isset($subelement['render-font-underline']) && (string)$subelement['render-font-underline']) {
                $subel['renderfontunderline'] = (int)$subelement['render-font-underline'];
            } else {
                $subel['renderfontunderline'] = "0";
            }
            if (isset($subelement['render-font-italic']) && (string)$subelement['render-font-italic']) {
                $subel['renderfontitalic'] = (int)$subelement['render-font-italic'];
            } else {
                $subel['renderfontitalic'] = "0";
            }
            if (isset($subelement['render-font-family']) && (string)$subelement['render-font-family']) {
                $subel['renderfontfamily'] = (string)$subelement['render-font-family'];
            } else {
                $subel['renderfontfamily'] = "Helvetica";
            }
            if (isset($subelement['render-pos-layer']) && (string)$subelement['render-pos-layer']) {
                $subel['selectlayer'] = (int)$subelement['render-pos-layer'];
            } else {
                $subel['selectlayer'] = 0;
            }
            if (isset($subelement['selectgallery']) && (string)$subelement['selectgallery']) {
                $subel['selectgallery'] = (int)$subelement['selectgallery'];
            } else {
                $subel['selectgallery'] = 0;
            }

            if (isset($subelement->options)) {
                $subel['options'] = array();
                foreach ($subelement->options->children() as $keysuboption => $suboption) {
                    $subopt = array(
                        'label' => (string)$suboption['label'],
                        'value' => (string)$suboption['id']
                    );
                    if (isset($suboption['thumbnail']) && (string)$suboption['thumbnail']) {
                        $subopt['thumbnail'] = (string)$suboption['thumbnail'];
                    } else {
                        $subopt['thumbnail'] = 0;
                    }
                    if (isset($suboption['motiv']) && (string)$suboption['motiv']) {
                        $motiv = Doctrine_Query::create()->from('Motiv c')->where('c.uuid = ?', array($suboption['motiv']))->fetchOne();
                        $subopt['thumbnail'] = $motiv->file_thumb;
                    }
                    if (isset($suboption['style']) && (string)$suboption['style']) {
                        $subopt['style'] = (string)$suboption['style'];
                    } else {
                        $subopt['style'] = 0;
                    }
                    $subopt['elements'] = $this->buildSubElements($suboption->elements->children(), $defaultdata);
                    $subel['options'][] = $subopt;

                }
            }

            $temp[] = $subel;
        }

        return $temp;
    }

    public function getdesigntemplatesAction()
    {

        $article = Doctrine_Query::create()->from('Article c')->where('c.install_id = ? AND c.enable = 1 AND c.uuid = ?', array($this->install->id, $this->_getParam('product')))->fetchOne();

        if ($this->shop->market) {
            if ($article->a6_org_article != 0 && $article->a6_org_article != "") {
                $articles = Doctrine_Query::create()
                    ->from('Article c')
                    ->where('(c.a6_org_article = ? AND c.shop_id = ? AND c.private = 0 AND c.enable = 1 AND c.not_buy = 0 AND c.resale_design = 1) OR (c.id = ?)', array($article->a6_org_article, $this->shop->id, $article->a6_org_article))
                    ->execute();
            } else {
                $articles = Doctrine_Query::create()
                    ->from('Article c')
                    ->where('c.a6_org_article = ? AND c.shop_id = ? AND c.private = 0 AND c.enable = 1 AND c.not_buy = 0 AND c.resale_design = 1', array($article->id, $this->shop->id))
                    ->execute();
            }
        } else {
            if ($article->a6_org_article != 0 && $article->a6_org_article != "") {
                $articles = Doctrine_Query::create()
                    ->from('Article c')
                    ->where('(c.a6_org_article = ? AND c.private = 0 AND c.enable = 1 AND c.resale_design = 1 AND c.not_buy = 0 AND c.display_market = 1) OR (c.id = ?)', array($article->a6_org_article, $article->a6_org_article))
                    ->execute();
            } else {
                $articles = Doctrine_Query::create()
                    ->from('Article c')
                    ->where('(c.a6_org_article = ? AND c.private = 0 AND c.enable = 1 AND c.resale_design = 1 AND c.not_buy = 0 AND  c.display_market = 1) OR (c.id = ?)', array($article->id, $article->id))
                    ->execute();
            }
        }

        $this->view->result = array();

        require_once APPLICATION_PATH . '/helpers/Image.php';
        $image = new TP_View_Helper_Image();

        $opt = array(
            'title' => $article->title,
            'uuid' => $article->uuid
        );
        if ($article->file == "") {
            $opt['thumbnail'] = $image->thumbnailFop($article->title, 'articlelist', $article->getMarketFile(), true, $article->id);
        } else {
            $opt['thumbnail'] = $image->thumbnailImage($article->title, 'articlelist', $article->file, true);
        }

        $this->view->result[] = $opt;

        foreach ($articles as $art) {
            $opt = array(
                'title' => $art->title,
                'uuid' => $art->uuid
            );
            if ($art->file == "") {
                $opt['thumbnail'] = $image->thumbnailFop($art->title, 'articlelist', $art->getMarketFile(), true, $art->id);
            } else {
                $opt['thumbnail'] = $image->thumbnailImage($art->title, 'articlelist', $art->file, true);
            }

            $this->view->result[] = $opt;

        }

        $this->view->success = true;
    }

    public function renderpreviewlayerAction()
    {
        ini_set("max_input_vars", 6000);
        // Caching Abschalten
        header("Expires: 0");
        header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
        header("Cache-Control: no-store, no-cache, must-revalidate");
        // IE
        header('Cache-Control: pre-check=0, post-check=0', false);
        // HTTP1.0
        header("Pragma: no-cache");

        $jsonSaveData = $this->_getParam('jsonSaveData');
        $jsonCreateData = $this->_getParam('jsonCreateData');
        $jsonLayerData = $this->_getParam('jsonLayerData', false);
        $jsonUuid = $this->_getParam('jsonUuid');
        $layer = $this->_getParam('layer', 'all');
        $images_uuid = $this->_getParam('images_uuid', '');

        session_write_close();
        $refLayer = array();

        Zend_Registry::set("renderpreviewlayer", true);
        /**
         * Sicherheit
         */

        if ($this->_getParam('mode', 3) == 2) {

            $layouterSession = new TP_Layoutersession();
            $layouterArt = $layouterSession->getLayouterArticle($jsonUuid);

            $article = Doctrine_Query::create()->from('Article c')->where('c.install_id = ? AND c.enable = 1 AND c.uuid = ?', array($this->install->id, $layouterArt->getOrgArticleId()))->fetchOne();

        } else {
            $article = Doctrine_Query::create()->from('Article c')->where('c.install_id = ? AND c.enable = 1 AND c.uuid = ?', array($this->install->id, $jsonUuid))->fetchOne();
            $jsonUuid = TP_Util::uuid();
        }
        if (empty($jsonSaveData) && $article->upload_steplayouter_data != "") {
            $jsonData = json_decode($article->upload_steplayouter_data, true);
            $jsonCreateData = $jsonData['jsonCreateData'];
            $jsonSaveData = $jsonData['jsonSaveData'];
        }

        $steplayouter = new TP_Steplayouter($jsonSaveData, $article, $jsonLayerData, $jsonCreateData);
        $steplayouter->buildRenderStack($this->_getParam('width'), $this->_getParam('height'));

        if($layer == "all") {
            $images = $steplayouter->exportLayerImageNames(PUBLIC_PATH . '/steplayouter/temp/' . $jsonUuid, $this->_getParam('width'), $this->_getParam('height'), $this->_getParam('site'));
        }else{
            $images = $steplayouter->exportLayerPNGS(PUBLIC_PATH . '/steplayouter/temp/' . $images_uuid, $this->_getParam('width'), $this->_getParam('height'), $this->_getParam('site'), $layer, $images_uuid);
        }

        $site = $steplayouter->getSite($this->_getParam('site'));
        $this->view->passepartout = "";
        $this->view->render_ref_layer = array();
        if($site) {
            $this->view->render_ref_layer = $site->getRefLayers();
        }


        if ($site->getPassepartout()) {
            $this->view->passepartout = $site->getPassepartout();
        }
        $this->view->success = true;
        $this->view->images = $images;
        $this->view->images_uuid = $jsonUuid;
        $this->view->image = '/steplayouter/temp/' . $jsonUuid;

    }

    public function renderpreviewAction()
    {

        // Caching Abschalten
        header("Expires: 0");
        header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
        header("Cache-Control: no-store, no-cache, must-revalidate");
        // IE
        header('Cache-Control: pre-check=0, post-check=0', false);
        // HTTP1.0
        header("Pragma: no-cache");

        $jsonSaveData = $this->_getParam('jsonSaveData');
        $jsonCreateData = $this->_getParam('jsonCreateData');
        $jsonLayerData = $this->_getParam('jsonLayerData', false);
        $jsonUuid = $this->_getParam('jsonUuid');

        /**
         * Sicherheit
         */
        if ($this->_getParam('mode', 3) == 2) {

            $layouterSession = new TP_Layoutersession();
            $layouterArt = $layouterSession->getLayouterArticle($jsonUuid);

            $article = Doctrine_Query::create()->from('Article c')->where('c.install_id = ? AND c.enable = 1 AND c.uuid = ?', array($this->install->id, $layouterArt->getOrgArticleId()))->fetchOne();

        } else {
            $article = Doctrine_Query::create()->from('Article c')->where('c.install_id = ? AND c.enable = 1 AND c.uuid = ?', array($this->install->id, $jsonUuid))->fetchOne();
            $jsonUuid = TP_Util::uuid();
        }

        if (empty($jsonSaveData) && $article->upload_steplayouter_data != "") {
            $jsonSaveData = json_decode($article->upload_steplayouter_data, true);
        }

        $steplayouter = new TP_Steplayouter($jsonSaveData, $article, $jsonLayerData, $jsonCreateData);
        $steplayouter->buildRenderStack($this->_getParam('width'), $this->_getParam('height'));

        if ($this->_getParam('site', false) == 'all') {
            $images = $steplayouter->exportPNGS(PUBLIC_PATH . '/steplayouter/temp/' . $jsonUuid, $this->_getParam('width'), $this->_getParam('height'), $this->_getParam('site'));

            $this->view->success = true;
            $this->view->images = $images;
            $this->view->image = '/steplayouter/temp/' . $jsonUuid;

            return;
        }

        $steplayouter->exportPNG(PUBLIC_PATH . '/steplayouter/temp/' . $jsonUuid, $this->_getParam('width'), $this->_getParam('height'), $this->_getParam('site'));

        $this->view->success = true;
        $this->view->image = '/steplayouter/temp/' . $jsonUuid . '.png';

    }

    public function renderpdfpreviewAction()
    {

        // Caching Abschalten
        header("Expires: 0");
        header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
        header("Cache-Control: no-store, no-cache, must-revalidate");
        // IE
        header('Cache-Control: pre-check=0, post-check=0', false);
        // HTTP1.0
        header("Pragma: no-cache");

        $jsonSaveData = $this->_getParam('jsonSaveData');
        $jsonCreateData = $this->_getParam('jsonCreateData');
        $jsonLayerData = $this->_getParam('jsonLayerData', false);
        $jsonUuid = $this->_getParam('jsonUuid');

        /**
         * Sicherheit
         */
        if ($this->_getParam('mode', 3) == 2) {

            $layouterSession = new TP_Layoutersession();
            $layouterArt = $layouterSession->getLayouterArticle($jsonUuid);

            $article = Doctrine_Query::create()->from('Article c')->where('c.install_id = ? AND c.enable = 1 AND c.uuid = ?', array($this->install->id, $layouterArt->getOrgArticleId()))->fetchOne();

        } else {
            $article = Doctrine_Query::create()->from('Article c')->where('c.install_id = ? AND c.enable = 1 AND c.uuid = ?', array($this->install->id, $jsonUuid))->fetchOne();
            $jsonUuid = TP_Util::uuid();

            if ($article->upload_steplayouter_data != "") {
                $defaultdata = json_decode($article->upload_steplayouter_data, true);
                $this->view->layerdata = $defaultdata['jsonLayerData'];
                $this->view->createdata = $defaultdata['jsonCreateData'];
                $defaultdata = $defaultdata['jsonSaveData'];
                $this->view->defaultdata = $defaultdata;
            }
        }


        $steplayouter = new TP_Steplayouter($jsonSaveData, $article, $jsonLayerData, $jsonCreateData);
        $steplayouter->buildRenderStack($this->_getParam('width'), $this->_getParam('height'));
        $steplayouter->buildPreviewPDF('steplayouter/temp/' . $jsonUuid);

        $this->view->success = true;
        $this->view->file = '/steplayouter/temp/' . $jsonUuid . '.pdf';

    }

    public function storeAction()
    {
        $jsonSaveData = $this->_getParam('jsonSaveData');
        $jsonLayerData = $this->_getParam('jsonLayerData', false);
        $jsonCreateData = $this->_getParam('jsonCreateData', false);
        $mode = $this->_getParam('mode');

        if($mode != 1) {
            $basket = new TP_Basket();
            if ($mode == 3) {
                $articleSession = new TP_Layoutersession();
                $articleSess = $articleSession->getLayouterArticle($this->_getParam('jsonUuid'));
            } else {
                $articleSession = new TP_Layoutersession();
                $articleSess = $articleSession->getLayouterArticle($this->_getParam('jsonUuid'));
            }
            $articleSess->setDesignerXML(json_encode(array('jsonCreateData' => $jsonCreateData, 'jsonSaveData' => $jsonSaveData, 'jsonLayerData' => $jsonLayerData, 'width' => $this->_getParam('width'), 'height' => $this->_getParam('height'))));
            $articleSess->setLayouterModus(2);
            $articleSess->setTitle($this->_getParam('steplayouter_saveas_name'));
            $articleSess->setIsEdit();
            $this->view->layouter = $articleSess->getArticleId();
            unlink(PUBLIC_PATH . '/steplayouter/temp/' . $articleSess->getArticleId() . ".png");
            $this->view->article = $articleSess->getOrgArticleId();
            $this->view->debug = count($articleSession->getAllLayouterArticle());
        }else{
            $article = Doctrine_Query::create()->from('Article c')->where('c.install_id = ? AND c.enable = 1 AND c.uuid = ?', array($this->install->id, $this->_getParam('jsonUuid')))->fetchOne();
            $article->upload_steplayouter_data = json_encode(array('jsonCreateData' => $jsonCreateData, 'jsonSaveData' => $jsonSaveData, 'jsonLayerData' => $jsonLayerData, 'width' => $this->_getParam('width'), 'height' => $this->_getParam('height')));
            $article->save();
            $this->view->article = $this->_getParam('jsonUuid');
        }
        $this->view->success = true;

    }

    public function savenewlayouterAction()
    {

        $basket = new TP_Basket();
        if($this->getRequest()->getParam('modus', 4) == 5) {
            $tempBasket = $basket->getTempProduct($this->getRequest()->getParam('uuid'));
            $tempBasket->setNetto($this->getRequest()->getParam('netto', 0));
        }
        if($this->getRequest()->getParam('modus', 4) == 6) {
            $tempBasket = $basket->getTempProduct($this->getRequest()->getParam('uuid'));
            $tempBasket->setNetto($this->getRequest()->getParam('netto', 0));
        }
        $sessart = $basket->getTempProduct($this->getRequest()->getParam('uuid'));
        $articleSession = new TP_Layoutersession();
        $articleSess = $articleSession->getLayouterArticle($this->getRequest()->getParam('uuid'));
        $articleSess->setLayouterModus($this->getRequest()->getParam('modus', 4));
        $articleSess->setIsEdit();
        $sessart->setLayouterId($articleSess->getArticleId());

        $this->view->success = true;
        $this->view->layouter = $articleSess->getArticleId();
        $this->view->article = $articleSess->getOrgArticleId();

        if($this->getRequest()->getParam('modus', 4) == 8) {
            $sessart->setNetto($this->getRequest()->getParam('hplayouter_price', 0));
            $sessart->setTemplatePrintId($this->getRequest()->getParam('project_id', 4));
            $articleSess->setTemplatePrintId($this->getRequest()->getParam('project_id', 4));
            $this->_redirect('/article/show/uuid/' . $articleSess->getOrgArticleId() . '/' . $articleSess->getArticleId() . '?hplayouter_price='.$this->getRequest()->getParam('hplayouter_price', 0));

            exit;
        }

        if($this->getRequest()->getParam('modus', 4) == 9) {
            $sessart->setTemplatePrintId($this->getRequest()->getParam('external_id', 4));
            $articleSess->setTemplatePrintId($this->getRequest()->getParam('external_id', 4));
            $this->_redirect('/article/buy?count=1&load=0&uuid=' . $articleSess->getOrgArticleId());

            exit;
        }

    }

    public function updatenewlayouterAction()
    {

        $basket = new TP_Basket();
        if($this->getRequest()->getParam('modus', 4) == 5) {
            $tempBasket = $basket->getTempProduct();
            $tempBasket->setNetto($this->getRequest()->getParam('netto', 0));
        }
        if($this->getRequest()->getParam('modus', 4) == 6) {
            $tempBasket = $basket->getTempProduct();
            $tempBasket->setNetto($this->getRequest()->getParam('netto', 0));
        }

        $this->view->success = true;

    }


    public function loadnewlayouterAction()
    {
        $data = $this->_request->getPost();

        $basket = new TP_Basket();
        $articleSession = new TP_Layoutersession();
        $articleSess = $articleSession->getLayouterArticle($data['uuid']);
        $articleSess->setDesignerXML(json_encode(array('payload' => $data['payload'])));
        $articleSess->setLayouterModus($this->getRequest()->getParam('modus', 4));
        $articleSess->setIsEdit();

        $this->view->success = true;
        $this->view->layouter = $articleSess->getArticleId();
        $this->view->article = $articleSess->getOrgArticleId();

    }

    protected function recursive_array_search($needle, $haystack)
    {
        foreach ($haystack as $key => $value) {
            $current_key = $key;
            if ($needle === $value OR (is_array($value) && $this->recursive_array_search($needle, $value) !== false)) {
                return $current_key;
            }
        }
        return false;
    }

}

