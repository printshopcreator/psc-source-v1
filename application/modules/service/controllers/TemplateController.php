<?php

class Service_TemplateController extends TP_Controller_Action
{

    protected $_extAllow = 'ppt,inx,idml,indd,qxd,ps,cdr,cdx,jpg,gif,pdf,png,jpeg,tiff,ai,eps,ttf,svg';

    public function init() {
        parent::init();
        $this->getHelper('layout')->disableLayout();
        $this->_helper->viewRenderer->setNoRender();
    }

    public function storeuploadAction() {
        $uploadedFile = $_FILES['logo1'];
        $file = new Image();
        $file->generateUID();
        $path = 'temp/' . $file->id . '_' . $uploadedFile['name'];
        $file->path = $path;
        $file->name = $uploadedFile['name'];
        $file->imagetype = $uploadedFile['type'];
        move_uploaded_file($uploadedFile['tmp_name'], $path);
        $file->save();


        $contact = Doctrine_Query::create()
            ->from('Contact c')
            ->where('c.uuid = ?', array($this->_getParam("uuid")))
            ->fetchOne();

        $contact->logo1 = $file->id;
        $contact->save();

        die(json_encode(array("success" => true)));

    }

    public function getpackageAction() {
        $values = TP_Crypt::decrypt($this->_getParam('ARTID'));
        Zend_Registry::get('log')->debug(print_r($values, true));
        if ($values['COPY'] == 1) {

            $layouterid = $values['LAYOUTERID'];
            $articleSession = new TP_Layoutersession();
            $articleSess = $articleSession->getLayouterArticle($layouterid);

            if ($this->user) {
                $this->readfile_chunked(APPLICATION_PATH . '/../market/templateprint/user/' . $articleSess->getTemplatePrintId() . '/product.zip');
                Zend_Registry::get('log')->debug(APPLICATION_PATH . '/../market/templateprint/user/' . $articleSess->getTemplatePrintId() . '/product.zip');
            } else {
                $this->readfile_chunked(APPLICATION_PATH . '/../market/templateprint/guest/' . $articleSess->getTemplatePrintId() . '/product.zip');
                Zend_Registry::get('log')->debug(APPLICATION_PATH . '/../market/templateprint/guest/' . $articleSess->getTemplatePrintId() . '/product.zip');
            }
            return;
        }

        $article = Doctrine_Query::create()
            ->from('Article c')
            ->where('c.uuid = ?', array($values['UUID']))
            ->fetchOne();

        $path = str_split($article->id);

        header("Content-Transfer-Encoding: binary");
        header("Content-type: application/zip");
        $len = filesize(APPLICATION_PATH . '/../market/templateprint/orginal/' . implode('/', $path) . '/product.zip');
        header("Content-Length: $len");
        header('Content-Disposition: attachment; filename="product.zip"');
        if (file_exists(APPLICATION_PATH . '/../market/templateprint/orginal/' . implode('/', $path) . '/product.zip')) {
            $this->readfile_chunked(APPLICATION_PATH . '/../market/templateprint/orginal/' . implode('/', $path) . '/product.zip');
        }
        die();
    }

    protected function readfile_chunked($filename) {
        $chunksize = 1 * (1024 * 1024); // how many bytes per chunk
        $buffer = '';
        $handle = fopen($filename, 'rb');
        if ($handle === false) {
            return false;
        }
        while (!feof($handle)) {
            $buffer = fread($handle, $chunksize);
            print $buffer;
            ob_flush();
            flush();
        }
        return fclose($handle);
        die();
    }

    protected function escapeAND($var) {
        return str_replace("&", "&amp;", $var);
    }

    public function getuserAction() {
        //$values = TP_Crypt::decrypt($this->_getParam('ARTID'));
        if($this->_getParam("imageId", false)) {
            $imageId = str_replace("_preview", "", $this->_getParam("imageId"));
            
            if(strlen($imageId) < 25) {
                $dbMongo = TP_Mongo::getInstance();
                $obj = $dbMongo->Media->findOne(array('_id' => new \MongoDB\BSON\ObjectId($imageId)));
                header("Content-type: application/pdf");
                echo file_get_contents('.' . $obj['url']);
            }else{
                $image = Doctrine_Query::create()
                    ->from('Image as i')
                    ->where('i.id = ?')->fetchOne(array($imageId));

                if($image) {
                    header("Content-type: application/pdf");
                    $im = new imagick($image->path);
                    $im->setImageFormat('pdf');
                    echo $im->getImageBlob();
                }
            }
            die();
        }


        header('Content-type: text/xml');
        $translate = Zend_Registry::get('translate');

        if (Zend_Auth::getInstance()->hasIdentity()) {
            $user = Zend_Auth::getInstance()->getIdentity();
            $mode = new Zend_Session_Namespace('adminmode');
            if($this->_getParam('w2puserid', false) && strlen($this->_getParam('w2puserid', false)) > 10) {
                $user = Doctrine_Query::create()
                    ->from('Contact m')
                    ->where('uuid = ?')->fetchOne(array($this->_getParam('w2puserid', false)));
            }elseif ($mode->over_ride_contact != false && $mode->over_ride_contact != 'new') {
                $user = Doctrine_Query::create()
                    ->from('Contact m')
                    ->where('id = ?')->fetchOne(array($mode->over_ride_contact));
            } else {
                $user = Doctrine_Query::create()
                    ->from('Contact m')
                    ->where('id = ?')->fetchOne(array($user['id']));
            }

        } else {
            $user = new Contact();
        }

        $xml = new SimpleXMLElement('<root></root>');
        $system = $xml->addChild('system');
        $system->addChild('status', 'SUCCESS');

        $contact = $xml->addChild('contact');
        $contact->addAttribute('display', 'Person');

        $logo1 = $contact->addChild('field', $user->logo1);
        $logo1->addAttribute('datafield', 'logo1');
        $logo1->addAttribute('type', 'image');
        $logo1->addAttribute('display', "Bild 1");

        $logo2 = $contact->addChild('field', $user->self_foto);
        $logo2->addAttribute('datafield', 'logo2');
        $logo2->addAttribute('type', 'image');
        $logo2->addAttribute('display', "Bild 2");


        $cf1 = $contact->addChild('field', $this->escapeAND($user->self_firstname));
        $cf1->addAttribute('datafield', 'c1');
        $cf1->addAttribute('type', 'text');
        $cf1->addAttribute('display', "Vorname" . ' (vname)');

        $cf2 = $contact->addChild('field', $this->escapeAND($user->self_lastname));
        $cf2->addAttribute('datafield', 'c2');
        $cf2->addAttribute('type', 'text');
        $cf2->addAttribute('display', $translate->translate('contact_settings_fieldLabel_Lastname_Name') . ' (nname)');

        $ss = $contact->addChild('field', $this->escapeAND($user->self_street));
        $ss->addAttribute('datafield', 'c3');
        $ss->addAttribute('type', 'text');
        $ss->addAttribute('display', $translate->translate('contact_settings_fieldLabel_Street') . ' (str1)');

        $sh = $contact->addChild('field', $this->escapeAND($user->self_house_number));
        $sh->addAttribute('datafield', 'c4');
        $sh->addAttribute('type', 'text');
        $sh->addAttribute('display', $translate->translate('contact_settings_fieldLabel_Housenumber' . ' (strnr)'));

        $sz = $contact->addChild('field', $this->escapeAND($user->self_country));
        $sz->addAttribute('datafield', 'c5');
        $sz->addAttribute('type', 'text');
        $sz->addAttribute('display', $translate->translate('contact_settings_fieldLabel_Lkz'));

        $sz = $contact->addChild('field', $this->escapeAND($user->self_zip));
        $sz->addAttribute('datafield', 'c6');
        $sz->addAttribute('type', 'text');
        $sz->addAttribute('display', $translate->translate('contact_settings_fieldLabel_Zip'));

        $sc = $contact->addChild('field', $this->escapeAND($user->self_city));
        $sc->addAttribute('datafield', 'c7');
        $sc->addAttribute('type', 'text');
        $sc->addAttribute('display', $translate->translate('contact_settings_fieldLabel_City'));

        $se = $contact->addChild('field', $this->escapeAND($user->self_email));
        $se->addAttribute('datafield', 'c8');
        $se->addAttribute('type', 'text');
        $se->addAttribute('display', 'Email/Login');

        $mailaccount = explode("@", $user->self_email);

        $se = $contact->addChild('field', $mailaccount[0]);
        $se->addAttribute('datafield', 'c8_first');
        $se->addAttribute('type', 'text');
        $se->addAttribute('display', $translate->translate('Email/Login Name'));


        $se = $contact->addChild('field', $mailaccount[1]);
        $se->addAttribute('datafield', 'c8_last');
        $se->addAttribute('type', 'text');
        $se->addAttribute('display', $translate->translate('Email/Login Domain'));

        $se = $contact->addChild('field', $this->escapeAND($user->name));
        $se->addAttribute('datafield', 'c9');
        $se->addAttribute('type', 'text');
        $se->addAttribute('display', $translate->translate('contact_settings_fieldLabel_Name'));

        $se = $contact->addChild('field', $this->escapeAND($user->self_department));
        $se->addAttribute('datafield', 'c10');
        $se->addAttribute('type', 'text');
        $se->addAttribute('display', $translate->translate('contact_settings_fieldLabel_Department'));

        $se = $contact->addChild('field', $this->escapeAND($user->self_department2));
        $se->addAttribute('datafield', 'c10t2');
        $se->addAttribute('type', 'text');
        $se->addAttribute('display', $translate->translate('self_department2'));

        $se = $contact->addChild('field', $this->escapeAND($user->self_position));
        $se->addAttribute('datafield', 'c11');
        $se->addAttribute('type', 'text');
        $se->addAttribute('display', $translate->translate('contact_settings_fieldLabel_position'));

        $se = $contact->addChild('field', $this->escapeAND($user->self_title));
        $se->addAttribute('datafield', 'c12');
        $se->addAttribute('type', 'text');
        $se->addAttribute('display', $translate->translate('contact_settings_fieldLabel_title'));



        $sp = $contact->addChild('field', $this->escapeAND($user->self_phone_lv));
        $sp->addAttribute('datafield', 'c14lv');
        $sp->addAttribute('type', 'text');
        $sp->addAttribute('display', $translate->translate('Telefon LV') . ' (tel)');

        $sp = $contact->addChild('field', $this->escapeAND($user->self_phone_vorwahl));
        $sp->addAttribute('datafield', 'c13');
        $sp->addAttribute('type', 'text');
        $sp->addAttribute('display', $translate->translate('contact_settings_fieldLabel_Telefon_Vorwahl') . ' (tel)');

        $sp = $contact->addChild('field', $this->escapeAND($user->self_phone));
        $sp->addAttribute('datafield', 'c14');
        $sp->addAttribute('type', 'text');
        $sp->addAttribute('display', $translate->translate('contact_settings_fieldLabel_Telefon') . ' (tel)');

        $sp = $contact->addChild('field', $this->escapeAND($user->self_phone_durchwahl));
        $sp->addAttribute('datafield', 'c15');
        $sp->addAttribute('type', 'text');
        $sp->addAttribute('display', $translate->translate('contact_settings_fieldLabel_Telefon_Durchwahl') . ' (tel)');

        $sp = $contact->addChild('field', $this->escapeAND($user->self_mobile_lv));
        $sp->addAttribute('datafield', 'c17lv');
        $sp->addAttribute('type', 'text');
        $sp->addAttribute('display', $translate->translate('Mobil LV') . ' (mobil)');

        $sp = $contact->addChild('field', $this->escapeAND($user->self_mobile_vorwahl));
        $sp->addAttribute('datafield', 'c16');
        $sp->addAttribute('type', 'text');
        $sp->addAttribute('display', $translate->translate('contact_settings_fieldLabel_Mobil_Vorwahl') . ' (mobil)');

        $sp = $contact->addChild('field', $this->escapeAND($user->self_phone_mobile));
        $sp->addAttribute('datafield', 'c17');
        $sp->addAttribute('type', 'text');
        $sp->addAttribute('display', $translate->translate('contact_settings_fieldLabel_Mobil') . ' (mobil)');

        $sp = $contact->addChild('field', $this->escapeAND($user->self_mobile_durchwahl));
        $sp->addAttribute('datafield', 'c18');
        $sp->addAttribute('type', 'text');
        $sp->addAttribute('display', $translate->translate('contact_settings_fieldLabel_Mobil_Durchwahl') . ' (mobil)');

        $sp = $contact->addChild('field', $this->escapeAND($user->self_fax_lv));
        $sp->addAttribute('datafield', 'c20lv');
        $sp->addAttribute('type', 'text');
        $sp->addAttribute('display', $translate->translate('Fax LV'));

        $sp = $contact->addChild('field', $this->escapeAND($user->self_fax_vorwahl));
        $sp->addAttribute('datafield', 'c19');
        $sp->addAttribute('type', 'text');
        $sp->addAttribute('display', $translate->translate('contact_settings_fieldLabel_Fax_Vorwahl'));

        $sp = $contact->addChild('field', $this->escapeAND($user->self_fax_phone));
        $sp->addAttribute('datafield', 'c20');
        $sp->addAttribute('type', 'text');
        $sp->addAttribute('display', $translate->translate('contact_settings_fieldLabel_Fax'));

        $sp = $contact->addChild('field', $this->escapeAND($user->self_fax_durchwahl));
        $sp->addAttribute('datafield', 'c21');
        $sp->addAttribute('type', 'text');
        $sp->addAttribute('display', $translate->translate('contact_settings_fieldLabel_Fax_Durchwahl'));

        $sp = $contact->addChild('field', $this->escapeAND($user->self_phone_alternative_lv));
        $sp->addAttribute('datafield', 'c24lv');
        $sp->addAttribute('type', 'text');
        $sp->addAttribute('display', $translate->translate('Alternative LV'));

        $sp = $contact->addChild('field', $this->escapeAND($user->self_phone_alternative_type));
        $sp->addAttribute('datafield', 'c22');
        $sp->addAttribute('type', 'text');
        $sp->addAttribute('display', $translate->translate('contact_settings_fieldLabel_Alternativ_Type'));

        $sp = $contact->addChild('field', $this->escapeAND($user->self_phone_alternative));
        $sp->addAttribute('datafield', 'c23');
        $sp->addAttribute('type', 'text');
        $sp->addAttribute('display', $translate->translate('contact_settings_fieldLabel_Alternativ'));

        $sp = $contact->addChild('field', $this->escapeAND($user->self_phone_alternative_durchwahl));
        $sp->addAttribute('datafield', 'c24');
        $sp->addAttribute('type', 'text');
        $sp->addAttribute('display', $translate->translate('contact_settings_fieldLabel_Alternativ_Durchwahl'));

        $sp = $contact->addChild('field', $this->escapeAND($user->bank_name));
        $sp->addAttribute('datafield', 'c25');
        $sp->addAttribute('type', 'text');
        $sp->addAttribute('display', $translate->translate('contact_settings_fieldLabel_Alternativ_bank_name'));

        $sp = $contact->addChild('field', $this->escapeAND($user->bank_kto));
        $sp->addAttribute('datafield', 'c26');
        $sp->addAttribute('type', 'text');
        $sp->addAttribute('display', $translate->translate('contact_settings_fieldLabel_Alternativ_bank_kto'));

        $sp = $contact->addChild('field', $this->escapeAND($user->bank_blz));
        $sp->addAttribute('datafield', 'c27');
        $sp->addAttribute('type', 'text');
        $sp->addAttribute('display', $translate->translate('contact_settings_fieldLabel_Alternativ_bank_blz'));

        $sp = $contact->addChild('field', $this->escapeAND($user->bank_iban));
        $sp->addAttribute('datafield', 'c28');
        $sp->addAttribute('type', 'text');
        $sp->addAttribute('display', $translate->translate('contact_settings_fieldLabel_Alternativ_bank_iban'));

        $sp = $contact->addChild('field', $this->escapeAND($user->bank_bic));
        $sp->addAttribute('datafield', 'c29');
        $sp->addAttribute('type', 'text');
        $sp->addAttribute('display', $translate->translate('contact_settings_fieldLabel_Alternativ_bank_bic'));

        $sp = $contact->addChild('field', $this->escapeAND($user->bank_bank_name));
        $sp->addAttribute('datafield', 'c30');
        $sp->addAttribute('type', 'text');
        $sp->addAttribute('display', $translate->translate('contact_settings_fieldLabel_Alternativ_bank_bank_name'));

        $cfu = $contact->addChild('field', $this->escapeAND($user->self_function));
        $cfu->addAttribute('datafield', 'c31');
        $cfu->addAttribute('type', 'text');
        $cfu->addAttribute('display', $translate->translate('Funktion'));

        $cabt = $contact->addChild('field', $this->escapeAND($user->self_abteilung));
        $cabt->addAttribute('datafield', 'c32');
        $cabt->addAttribute('type', 'text');
        $cabt->addAttribute('display', $translate->translate('Abteilung'));

        $webt = $contact->addChild('field', $this->escapeAND($user->self_web));
        $webt->addAttribute('datafield', 'c32a');
        $webt->addAttribute('type', 'text');
        $webt->addAttribute('display', $translate->translate('Webseite'));

        $cabt = $contact->addChild('field', $this->escapeAND($user->getCustom1()));
        $cabt->addAttribute('datafield', 'v1');
        $cabt->addAttribute('type', 'text');
        $cabt->addAttribute('display', $translate->translate('v1'));

        $cabt = $contact->addChild('field', $this->escapeAND($user->getCustom2()));
        $cabt->addAttribute('datafield', 'v2');
        $cabt->addAttribute('type', 'text');
        $cabt->addAttribute('display', $translate->translate('v2'));

        $cabt = $contact->addChild('field', $this->escapeAND($user->getCustom3()));
        $cabt->addAttribute('datafield', 'v3');
        $cabt->addAttribute('type', 'text');
        $cabt->addAttribute('display', $translate->translate('v3'));

        $cabt = $contact->addChild('field', $this->escapeAND($user->getCustom4()));
        $cabt->addAttribute('datafield', 'v4');
        $cabt->addAttribute('type', 'text');
        $cabt->addAttribute('display', $translate->translate('v4'));

        $cabt = $contact->addChild('field', $this->escapeAND($user->getCustom5()));
        $cabt->addAttribute('datafield', 'v5');
        $cabt->addAttribute('type', 'text');
        $cabt->addAttribute('display', $translate->translate('v5'));

        $cabt = $contact->addChild('field', $this->escapeAND($user->getCustom6()));
        $cabt->addAttribute('datafield', 'v6');
        $cabt->addAttribute('type', 'text');
        $cabt->addAttribute('display', $translate->translate('v6'));

        $cabt = $contact->addChild('field', $this->escapeAND($user->getCustom7()));
        $cabt->addAttribute('datafield', 'v7');
        $cabt->addAttribute('type', 'text');
        $cabt->addAttribute('display', $translate->translate('v7'));

        $cabt = $contact->addChild('field', $this->escapeAND($user->getCustom8()));
        $cabt->addAttribute('datafield', 'v8');
        $cabt->addAttribute('type', 'text');
        $cabt->addAttribute('display', $translate->translate('v8'));

        $cabt = $contact->addChild('field', $this->escapeAND($user->getCustom9()));
        $cabt->addAttribute('datafield', 'v9');
        $cabt->addAttribute('type', 'text');
        $cabt->addAttribute('display', $translate->translate('v9'));

        $cabt = $contact->addChild('field', $this->escapeAND($user->getCustom10()));
        $cabt->addAttribute('datafield', 'v10');
        $cabt->addAttribute('type', 'text');
        $cabt->addAttribute('display', $translate->translate('v10'));

        $cabt = $contact->addChild('field', $this->escapeAND($user->getCustom11()));
        $cabt->addAttribute('datafield', 'v11');
        $cabt->addAttribute('type', 'text');
        $cabt->addAttribute('display', $translate->translate('v11'));

        $cabt = $contact->addChild('field', $this->escapeAND($user->getCustom12()));
        $cabt->addAttribute('datafield', 'v12');
        $cabt->addAttribute('type', 'text');
        $cabt->addAttribute('display', $translate->translate('v12'));

        $cabt = $contact->addChild('field', $this->escapeAND($user->getCustom13()));
        $cabt->addAttribute('datafield', 'v13');
        $cabt->addAttribute('type', 'text');
        $cabt->addAttribute('display', $translate->translate('v13'));

        $cabt = $contact->addChild('field', $this->escapeAND($user->getCustom14()));
        $cabt->addAttribute('datafield', 'v14');
        $cabt->addAttribute('type', 'text');
        $cabt->addAttribute('display', $translate->translate('v14'));

        $cabt = $contact->addChild('field', $this->escapeAND($user->getCustom15()));
        $cabt->addAttribute('datafield', 'v15');
        $cabt->addAttribute('type', 'text');
        $cabt->addAttribute('display', $translate->translate('v15'));

        $cabt = $contact->addChild('field', $this->escapeAND($user->getCustom16()));
        $cabt->addAttribute('datafield', 'v16');
        $cabt->addAttribute('type', 'text');
        $cabt->addAttribute('display', $translate->translate('v16'));

        $cabt = $contact->addChild('field', $this->escapeAND($user->getCustom17()));
        $cabt->addAttribute('datafield', 'v17');
        $cabt->addAttribute('type', 'text');
        $cabt->addAttribute('display', $translate->translate('v17'));

        $cabt = $contact->addChild('field', $this->escapeAND($user->getCustom18()));
        $cabt->addAttribute('datafield', 'v18');
        $cabt->addAttribute('type', 'text');
        $cabt->addAttribute('display', $translate->translate('v18'));

        $cabt = $contact->addChild('field', $this->escapeAND($user->getCustom19()));
        $cabt->addAttribute('datafield', 'v19');
        $cabt->addAttribute('type', 'text');
        $cabt->addAttribute('display', $translate->translate('v19'));

        $cabt = $contact->addChild('field', $this->escapeAND($user->getCustom20()));
        $cabt->addAttribute('datafield', 'v20');
        $cabt->addAttribute('type', 'text');
        $cabt->addAttribute('display', $translate->translate('v20'));

        $cabt = $contact->addChild('field', $this->escapeAND($user->getCustom21()));
        $cabt->addAttribute('datafield', 'v21');
        $cabt->addAttribute('type', 'text');
        $cabt->addAttribute('display', $translate->translate('v21'));

        $cabt = $contact->addChild('field', $this->escapeAND($user->getCustom22()));
        $cabt->addAttribute('datafield', 'v22');
        $cabt->addAttribute('type', 'text');
        $cabt->addAttribute('display', $translate->translate('v22'));

        $cabt = $contact->addChild('field', $this->escapeAND($user->getCustom23()));
        $cabt->addAttribute('datafield', 'v23');
        $cabt->addAttribute('type', 'text');
        $cabt->addAttribute('display', $translate->translate('v23'));

        $cabt = $contact->addChild('field', $this->escapeAND($user->getCustom24()));
        $cabt->addAttribute('datafield', 'v24');
        $cabt->addAttribute('type', 'text');
        $cabt->addAttribute('display', $translate->translate('v24'));

        try{
            $infos = json_decode(str_replace(PHP_EOL, '', $user->self_information), true);
        }catch(Exception $e) {
            $infos = array();
        }
        $fields_decode = array('department_2', 'street_2','house_number_2','country_2','country_1','zip_2','city_2','email_2','phone_lv_2','phone_vorwahl_2',
            'phone_2','phone_durchwahl_2','mobile_lv_2','mobile_vorwahl_2','mobile_2','fax_lv_2','fax_vorwahl_2',
            'fax_2','fax_durchwahl_2', 'internet_2','department_3','street_3','house_number_3','country_3','zip_3','city_3','city_4',
            'email_3','phone_lv_3','phone_vorwahl_3','phone_3','phone_durchwahl_3','mobile_lv_3','lkz_2','lkz_3','lkz_1','custom1','custom2','custom3',
            'mobile_vorwahl_3','mobile_3','fax_lv_3','fax_vorwahl_3','fax_3','fax_durchwahl_3', 'internet_3');

        if(file_exists(APPLICATION_PATH . '/design/clients/' . $this->view->shop->uid . '/templateDef.php')) {
            $fields = array();
            include_once(APPLICATION_PATH . '/design/clients/' . $this->view->shop->uid . '/templateDef.php');

            foreach($fields as $key => $row) {
                if(!is_array($row)) {
                    $fields_decode[] = $row;
                }
            }
        }

        foreach($fields_decode as $arrKey) {


            if(isset($infos[$arrKey])) {
                $cab = $contact->addChild('field', $this->escapeAND($infos[$arrKey]));
                $cab->addAttribute('datafield', $arrKey);
                $cab->addAttribute('type', 'text');
                $cab->addAttribute('display', $translate->translate($arrKey));
            }else{
                $cab = $contact->addChild('field', "");
                $cab->addAttribute('datafield', $arrKey);
                $cab->addAttribute('type', 'text');
                $cab->addAttribute('display', $translate->translate($arrKey));
            }

        }

        $account = $xml->addChild('account');
        $account->addAttribute('display', 'Filiale');
        $account->addAttribute('id', 'filiale');

        $st = $account->addChild('field', $this->escapeAND($user->Account->typ));
        $st->addAttribute('datafield', 'a0');
        $st->addAttribute('type', 'text');
        $st->addAttribute('display', $translate->translate('Typ'));

        $config = new Zend_Config_Ini(APPLICATION_PATH . '/configs/' . '/systemdefaults.ini', 'account_typ');
        $config = $config->toArray();

        $stt = $account->addChild('field', $this->escapeAND($config[$user->Account->typ]));
        $stt->addAttribute('datafield', 'a0text');
        $stt->addAttribute('type', 'text');
        $stt->addAttribute('display', $translate->translate('Typ'));

        $sf = $account->addChild('field', $this->escapeAND($user->Account->company));
        $sf->addAttribute('datafield', 'a1');
        $sf->addAttribute('type', 'text');
        $sf->addAttribute('display', $translate->translate('contact_settings_fieldLabel_Company'));

        $ss = $account->addChild('field', $this->escapeAND($user->Account->street));
        $ss->addAttribute('datafield', 'a2');
        $ss->addAttribute('type', 'text');
        $ss->addAttribute('display', $translate->translate('contact_settings_fieldLabel_Street'));

        $sh = $account->addChild('field', $this->escapeAND($user->Account->house_number));
        $sh->addAttribute('datafield', 'a3');
        $sh->addAttribute('type', 'text');
        $sh->addAttribute('display', $translate->translate('contact_settings_fieldLabel_Housenumber'));

        $sz = $account->addChild('field', $this->escapeAND($user->Account->country));
        $sz->addAttribute('datafield', 'a4');
        $sz->addAttribute('type', 'text');
        $sz->addAttribute('display', $translate->translate('contact_settings_fieldLabel_Lkz'));

        $sz = $account->addChild('field', $this->escapeAND($user->Account->zip));
        $sz->addAttribute('datafield', 'a5');
        $sz->addAttribute('type', 'text');
        $sz->addAttribute('display', $translate->translate('contact_settings_fieldLabel_Zip'));

        $sc = $account->addChild('field', $this->escapeAND($user->Account->city));
        $sc->addAttribute('datafield', 'a6');
        $sc->addAttribute('type', 'text');
        $sc->addAttribute('display', $translate->translate('contact_settings_fieldLabel_City'));

        $se = $account->addChild('field', $this->escapeAND($user->Account->email));
        $se->addAttribute('datafield', 'a7');
        $se->addAttribute('type', 'text');
        $se->addAttribute('display', $translate->translate('contact_settings_fieldLabel_Email'));

        $se = $account->addChild('field', $this->escapeAND($user->Account->appendix));
        $se->addAttribute('datafield', 'a8');
        $se->addAttribute('type', 'text');
        $se->addAttribute('display', $translate->translate('contact_settings_fieldLabel_Appendix'));

        $se = $account->addChild('field', $this->escapeAND($user->Account->homepage));
        $se->addAttribute('datafield', 'a9');
        $se->addAttribute('type', 'text');
        $se->addAttribute('display', $translate->translate('Homepage'));

        $se = $account->addChild('field', $this->escapeAND($user->Account->usid));
        $se->addAttribute('datafield', 'a10');
        $se->addAttribute('type', 'text');
        $se->addAttribute('display', $translate->translate('contact_settings_fieldLabel_USID'));

        $sp = $account->addChild('field', $this->escapeAND($user->Account->phone_lv));
        $sp->addAttribute('datafield', 'a12lv');
        $sp->addAttribute('type', 'text');
        $sp->addAttribute('display', $translate->translate('Telefon LV'));

        $sp = $account->addChild('field', $this->escapeAND($user->Account->phone_vorwahl));
        $sp->addAttribute('datafield', 'a11');
        $sp->addAttribute('type', 'text');
        $sp->addAttribute('display', $translate->translate('contact_settings_fieldLabel_Telefon_Vorwahl'));

        $sp = $account->addChild('field', $this->escapeAND($user->Account->phone));
        $sp->addAttribute('datafield', 'a12');
        $sp->addAttribute('type', 'text');
        $sp->addAttribute('display', $translate->translate('contact_settings_fieldLabel_Telefon'));

        $sp = $account->addChild('field', $this->escapeAND($user->Account->phone_durchwahl));
        $sp->addAttribute('datafield', 'a13');
        $sp->addAttribute('type', 'text');
        $sp->addAttribute('display', $translate->translate('contact_settings_fieldLabel_Telefon_Durchwahl'));

        $sp = $account->addChild('field', $this->escapeAND($user->Account->mobile_lv));
        $sp->addAttribute('datafield', 'a15lv');
        $sp->addAttribute('type', 'text');
        $sp->addAttribute('display', $translate->translate('Mobil LV'));

        $sp = $account->addChild('field', $this->escapeAND($user->Account->mobile_vorwahl));
        $sp->addAttribute('datafield', 'a14');
        $sp->addAttribute('type', 'text');
        $sp->addAttribute('display', $translate->translate('contact_settings_fieldLabel_Mobil_Vorwahl'));

        $sp = $account->addChild('field', $this->escapeAND($user->Account->mobile));
        $sp->addAttribute('datafield', 'a15');
        $sp->addAttribute('type', 'text');
        $sp->addAttribute('display', $translate->translate('contact_settings_fieldLabel_Mobil'));

        $sp = $account->addChild('field', $this->escapeAND($user->Account->mobile_durchwahl));
        $sp->addAttribute('datafield', 'a16');
        $sp->addAttribute('type', 'text');
        $sp->addAttribute('display', $translate->translate('contact_settings_fieldLabel_Mobil_Durchwahl'));

        $sp = $account->addChild('field', $this->escapeAND($user->Account->fax_lv));
        $sp->addAttribute('datafield', 'a18lv');
        $sp->addAttribute('type', 'text');
        $sp->addAttribute('display', $translate->translate('Fax LV'));

        $sp = $account->addChild('field', $this->escapeAND($user->Account->fax_vorwahl));
        $sp->addAttribute('datafield', 'a17');
        $sp->addAttribute('type', 'text');
        $sp->addAttribute('display', $translate->translate('contact_settings_fieldLabel_Fax_Vorwahl'));

        $sp = $account->addChild('field', $this->escapeAND($user->Account->fax));
        $sp->addAttribute('datafield', 'a18');
        $sp->addAttribute('type', 'text');
        $sp->addAttribute('display', $translate->translate('contact_settings_fieldLabel_Fax'));

        $sp = $account->addChild('field', $this->escapeAND($user->Account->fax_durchwahl));
        $sp->addAttribute('datafield', 'a19');
        $sp->addAttribute('type', 'text');
        $sp->addAttribute('display', $translate->translate('contact_settings_fieldLabel_Fax_Durchwahl'));

        $sp = $account->addChild('field', $this->escapeAND($user->Account->alternativ_lv));
        $sp->addAttribute('datafield', 'a21lv');
        $sp->addAttribute('type', 'text');
        $sp->addAttribute('display', $translate->translate('Alternative LV'));

        $sp = $account->addChild('field', $this->escapeAND($user->Account->alternativ_type));
        $sp->addAttribute('datafield', 'a20');
        $sp->addAttribute('type', 'text');
        $sp->addAttribute('display', $translate->translate('contact_settings_fieldLabel_Alternativ_Type'));

        $sp = $account->addChild('field', $this->escapeAND($user->Account->alternativ));
        $sp->addAttribute('datafield', 'a21');
        $sp->addAttribute('type', 'text');
        $sp->addAttribute('display', $translate->translate('contact_settings_fieldLabel_Alternativ'));

        $sp = $account->addChild('field', $this->escapeAND($user->Account->alternativ_durchwahl));
        $sp->addAttribute('datafield', 'a22');
        $sp->addAttribute('type', 'text');
        $sp->addAttribute('display', $translate->translate('contact_settings_fieldLabel_Alternativ_Durchwahl'));

        $sp = $account->addChild('field', $this->escapeAND($user->Account->bank_name));
        $sp->addAttribute('datafield', 'a23');
        $sp->addAttribute('type', 'text');
        $sp->addAttribute('display', $translate->translate('contact_settings_fieldLabel_Alternativ_bank_name'));

        $sp = $account->addChild('field', $this->escapeAND($user->Account->bank_kto));
        $sp->addAttribute('datafield', 'a24');
        $sp->addAttribute('type', 'text');
        $sp->addAttribute('display', $translate->translate('contact_settings_fieldLabel_Alternativ_bank_kto'));

        $sp = $account->addChild('field', $this->escapeAND($user->Account->bank_blz));
        $sp->addAttribute('datafield', 'a25');
        $sp->addAttribute('type', 'text');
        $sp->addAttribute('display', $translate->translate('contact_settings_fieldLabel_Alternativ_bank_blz'));

        $sp = $account->addChild('field', $this->escapeAND($user->Account->bank_iban));
        $sp->addAttribute('datafield', 'a26');
        $sp->addAttribute('type', 'text');
        $sp->addAttribute('display', $translate->translate('contact_settings_fieldLabel_Alternativ_bank_iban'));

        $sp = $account->addChild('field', $this->escapeAND($user->Account->bank_bic));
        $sp->addAttribute('datafield', 'a27');
        $sp->addAttribute('type', 'text');
        $sp->addAttribute('display', $translate->translate('contact_settings_fieldLabel_Alternativ_bank_bic'));

        $sp = $account->addChild('field', $this->escapeAND($user->Account->bank_bank_name));
        $sp->addAttribute('datafield', 'a28');
        $sp->addAttribute('type', 'text');
        $sp->addAttribute('display', $translate->translate('contact_settings_fieldLabel_Alternativ_bank_bank_name'));





        $sp = $account->addChild('field', $this->escapeAND($user->Account->gz));
        $sp->addAttribute('datafield', 'a29');
        $sp->addAttribute('type', 'text');
        $sp->addAttribute('display', $translate->translate('juristischeZugehoerigkeit'));

        $sp = $account->addChild('field', $this->escapeAND($user->Account->gf));
        $sp->addAttribute('datafield', 'a30');
        $sp->addAttribute('type', 'text');
        $sp->addAttribute('display', $translate->translate('Geschäftsführer'));

        $sp = $account->addChild('field', $this->escapeAND($user->Account->gsitz));
        $sp->addAttribute('datafield', 'a31');
        $sp->addAttribute('type', 'text');
        $sp->addAttribute('display', $translate->translate('SitzDerGesellschaft'));

        $sp = $account->addChild('field', $this->escapeAND($user->Account->handelsregister));
        $sp->addAttribute('datafield', 'a32');
        $sp->addAttribute('type', 'text');
        $sp->addAttribute('display', $translate->translate('Handelsregister'));

        $sp = $account->addChild('field', $this->escapeAND($user->Account->hrb));
        $sp->addAttribute('datafield', 'a33');
        $sp->addAttribute('type', 'text');
        $sp->addAttribute('display', $translate->translate('HRB'));

        $sp = $account->addChild('field', $this->escapeAND($user->Account->vorsitz));
        $sp->addAttribute('datafield', 'a34');
        $sp->addAttribute('type', 'text');
        $sp->addAttribute('display', $translate->translate('Aufsichtsratsvorsitzender'));


        $sp = $account->addChild('field', $this->escapeAND($user->Account->mega_code));
        $sp->addAttribute('datafield', 'a35');
        $sp->addAttribute('type', 'text');
        $sp->addAttribute('display', $translate->translate('Mega Code'));

        $sp = $account->addChild('field', $this->escapeAND($user->Account->hotel_name));
        $sp->addAttribute('datafield', 'a36');
        $sp->addAttribute('type', 'text');
        $sp->addAttribute('display', $translate->translate('Hotelname 2'));

        $sp = $account->addChild('field', $this->escapeAND($user->Account->zimmer));
        $sp->addAttribute('datafield', 'a37');
        $sp->addAttribute('type', 'text');
        $sp->addAttribute('display', $translate->translate('Zimmer'));



        $sp = $account->addChild('field', $this->escapeAND($user->Account->getAnrede1()));
        $sp->addAttribute('datafield', 'a38');
        $sp->addAttribute('type', 'text');
        $sp->addAttribute('display', $translate->translate('Anrede1'));


        $sp = $account->addChild('field', $this->escapeAND($user->Account->firstname1));
        $sp->addAttribute('datafield', 'a39');
        $sp->addAttribute('type', 'text');
        $sp->addAttribute('display', $translate->translate('Vorname1'));


        $sp = $account->addChild('field', $this->escapeAND($user->Account->lastname1));
        $sp->addAttribute('datafield', 'a40');
        $sp->addAttribute('type', 'text');
        $sp->addAttribute('display', $translate->translate('Nachname2'));


        $sp = $account->addChild('field', $this->escapeAND($user->Account->getAnrede2()));
        $sp->addAttribute('datafield', 'a41');
        $sp->addAttribute('type', 'text');
        $sp->addAttribute('display', $translate->translate('Anrede2'));


        $sp = $account->addChild('field', $this->escapeAND($user->Account->firstname2));
        $sp->addAttribute('datafield', 'a42');
        $sp->addAttribute('type', 'text');
        $sp->addAttribute('display', $translate->translate('Vorname2'));


        $sp = $account->addChild('field', $this->escapeAND($user->Account->lastname2));
        $sp->addAttribute('datafield', 'a43');
        $sp->addAttribute('type', 'text');
        $sp->addAttribute('display', $translate->translate('Nachname2'));

        $sp = $account->addChild('field', $this->escapeAND($user->Account->sub_hotel_name));
        $sp->addAttribute('datafield', 'a44');
        $sp->addAttribute('type', 'text');
        $sp->addAttribute('display', $translate->translate('Hotelname 3'));


        /*if($user->Account->filiale_id != 0) {
           $z = $xml->addChild('zentrale');
           $z->addAttribute('display', 'Zentrale');
           $z->addAttribute('id', 'zentrale');

           $ac = Zend_Auth::getInstance()->getIdentity();
           $ac = Doctrine_Query::create()
               ->from('Account m')
               ->where('id = ?')->fetchOne(array($user->Account->filiale_id));


           $zsf = $z->addChild('field', $ac->company);
           $zsf->addAttribute('datafield', 'z1');
           $zsf->addAttribute('type', 'text');
           $values->addAttribute('display', $translate->translate('contact_settings_fieldLabel_Company'));
       } */
         Zend_Registry::get('log')->debug($xml->asXML());
        echo $xml->asXML();

    }

    public function batchAction() {
        $values = TP_Crypt::decrypt($this->_getParam('ARTID'));

        header('Content-type: text/xml');
        $translate = Zend_Registry::get('translate');


        $user = Doctrine_Query::create()
            ->from('Contact m')
            ->where('uuid = ?')->fetchOne(array($values['userid']));

        $xml = new SimpleXMLElement('<root></root>');
        $system = $xml->addChild('system');
        $system->addChild('status', 'SUCCESS');

        $ds = $xml->addChild('dataset');

        $contact = $ds->addChild('contact');
        $contact->addAttribute('display', 'Person');

        $logo1 = $contact->addChild('field', $user->logo1);
        $logo1->addAttribute('datafield', 'logo1');
        $logo1->addAttribute('type', 'image');
        $logo1->addAttribute('display', "Bild 1");

        $logo2 = $contact->addChild('field', $user->self_foto);
        $logo2->addAttribute('datafield', 'logo2');
        $logo2->addAttribute('type', 'image');
        $logo2->addAttribute('display', "Bild 2");


        $cf1 = $contact->addChild('field', $this->escapeAND($user->self_firstname));
        $cf1->addAttribute('datafield', 'c1');
        $cf1->addAttribute('type', 'text');
        $cf1->addAttribute('display', $translate->translate('contact_settings_fieldLabel_Firstname'));

        $cf2 = $contact->addChild('field', $this->escapeAND($user->self_lastname));
        $cf2->addAttribute('datafield', 'c2');
        $cf2->addAttribute('type', 'text');
        $cf2->addAttribute('display', $translate->translate('contact_settings_fieldLabel_Lastname_Name'));

        $ss = $contact->addChild('field', $this->escapeAND($user->self_street));
        $ss->addAttribute('datafield', 'c3');
        $ss->addAttribute('type', 'text');
        $ss->addAttribute('display', $translate->translate('contact_settings_fieldLabel_Street'));

        $sh = $contact->addChild('field', $this->escapeAND($user->self_house_number));
        $sh->addAttribute('datafield', 'c4');
        $sh->addAttribute('type', 'text');
        $sh->addAttribute('display', $translate->translate('contact_settings_fieldLabel_Housenumber'));

        $sz = $contact->addChild('field', $this->escapeAND($user->self_country));
        $sz->addAttribute('datafield', 'c5');
        $sz->addAttribute('type', 'text');
        $sz->addAttribute('display', $translate->translate('contact_settings_fieldLabel_Lkz'));

        $sz = $contact->addChild('field', $this->escapeAND($user->self_zip));
        $sz->addAttribute('datafield', 'c6');
        $sz->addAttribute('type', 'text');
        $sz->addAttribute('display', $translate->translate('contact_settings_fieldLabel_Zip'));

        $sc = $contact->addChild('field', $this->escapeAND($user->self_city));
        $sc->addAttribute('datafield', 'c7');
        $sc->addAttribute('type', 'text');
        $sc->addAttribute('display', $translate->translate('contact_settings_fieldLabel_City'));

        $se = $contact->addChild('field', $user->self_email);
        $se->addAttribute('datafield', 'c8');
        $se->addAttribute('type', 'text');
        $se->addAttribute('display', 'Email/Login');

        $mailaccount = explode("@", $user->self_email);

        $se = $contact->addChild('field', $mailaccount[0]);
        $se->addAttribute('datafield', 'c8_first');
        $se->addAttribute('type', 'text');
        $se->addAttribute('display', $translate->translate('Email/Login Name'));


        $se = $contact->addChild('field', $mailaccount[1]);
        $se->addAttribute('datafield', 'c8_last');
        $se->addAttribute('type', 'text');
        $se->addAttribute('display', $translate->translate('Email/Login Domain'));

        $se = $contact->addChild('field', $this->escapeAND($user->name));
        $se->addAttribute('datafield', 'c9');
        $se->addAttribute('type', 'text');
        $se->addAttribute('display', $translate->translate('contact_settings_fieldLabel_Name'));

        $se = $contact->addChild('field', $this->escapeAND($user->self_department));
        $se->addAttribute('datafield', 'c10');
        $se->addAttribute('type', 'text');
        $se->addAttribute('display', $translate->translate('contact_settings_fieldLabel_Department'));

        $se = $contact->addChild('field', $this->escapeAND($user->self_department2));
        $se->addAttribute('datafield', 'c10t2');
        $se->addAttribute('type', 'text');
        $se->addAttribute('display', $translate->translate('self_department2'));

        $se = $contact->addChild('field', $this->escapeAND($user->self_position));
        $se->addAttribute('datafield', 'c11');
        $se->addAttribute('type', 'text');
        $se->addAttribute('display', $translate->translate('contact_settings_fieldLabel_position'));

        $sp = $contact->addChild('field', $this->escapeAND($user->self_phone_lv));
        $sp->addAttribute('datafield', 'c14lv');
        $sp->addAttribute('type', 'text');
        $sp->addAttribute('display', $translate->translate('Telefon LV'));

        $se = $contact->addChild('field', $this->escapeAND($user->self_title));
        $se->addAttribute('datafield', 'c12');
        $se->addAttribute('type', 'text');
        $se->addAttribute('display', $translate->translate('contact_settings_fieldLabel_title'));

        $sp = $contact->addChild('field', $this->escapeAND($user->self_phone_vorwahl));
        $sp->addAttribute('datafield', 'c13');
        $sp->addAttribute('type', 'text');
        $sp->addAttribute('display', $translate->translate('contact_settings_fieldLabel_Telefon_Vorwahl'));

        $sp = $contact->addChild('field', $this->escapeAND($user->self_phone));
        $sp->addAttribute('datafield', 'c14');
        $sp->addAttribute('type', 'text');
        $sp->addAttribute('display', $translate->translate('contact_settings_fieldLabel_Telefon'));

        $sp = $contact->addChild('field', $this->escapeAND($user->self_phone_durchwahl));
        $sp->addAttribute('datafield', 'c15');
        $sp->addAttribute('type', 'text');
        $sp->addAttribute('display', $translate->translate('contact_settings_fieldLabel_Telefon_Durchwahl'));

        $sp = $contact->addChild('field', $this->escapeAND($user->self_mobile_lv));
        $sp->addAttribute('datafield', 'c17lv');
        $sp->addAttribute('type', 'text');
        $sp->addAttribute('display', $translate->translate('Mobil LV'));

        $sp = $contact->addChild('field', $this->escapeAND($user->self_mobile_vorwahl));
        $sp->addAttribute('datafield', 'c16');
        $sp->addAttribute('type', 'text');
        $sp->addAttribute('display', $translate->translate('contact_settings_fieldLabel_Mobil_Vorwahl'));

        $sp = $contact->addChild('field', $this->escapeAND($user->self_phone_mobile));
        $sp->addAttribute('datafield', 'c17');
        $sp->addAttribute('type', 'text');
        $sp->addAttribute('display', $translate->translate('contact_settings_fieldLabel_Mobil'));

        $sp = $contact->addChild('field', $this->escapeAND($user->self_mobile_durchwahl));
        $sp->addAttribute('datafield', 'c18');
        $sp->addAttribute('type', 'text');
        $sp->addAttribute('display', $translate->translate('contact_settings_fieldLabel_Mobil_Durchwahl'));

        $sp = $contact->addChild('field', $this->escapeAND($user->self_fax_lv));
        $sp->addAttribute('datafield', 'c20lv');
        $sp->addAttribute('type', 'text');
        $sp->addAttribute('display', $translate->translate('Fax LV'));

        $sp = $contact->addChild('field', $this->escapeAND($user->self_fax_vorwahl));
        $sp->addAttribute('datafield', 'c19');
        $sp->addAttribute('type', 'text');
        $sp->addAttribute('display', $translate->translate('contact_settings_fieldLabel_Fax_Vorwahl'));

        $sp = $contact->addChild('field', $this->escapeAND($user->self_fax_phone));
        $sp->addAttribute('datafield', 'c20');
        $sp->addAttribute('type', 'text');
        $sp->addAttribute('display', $translate->translate('contact_settings_fieldLabel_Fax'));

        $sp = $contact->addChild('field', $this->escapeAND($user->self_fax_durchwahl));
        $sp->addAttribute('datafield', 'c21');
        $sp->addAttribute('type', 'text');
        $sp->addAttribute('display', $translate->translate('contact_settings_fieldLabel_Fax_Durchwahl'));

        $sp = $contact->addChild('field', $this->escapeAND($user->self_phone_alternative_type));
        $sp->addAttribute('datafield', 'c22');
        $sp->addAttribute('type', 'text');
        $sp->addAttribute('display', $translate->translate('contact_settings_fieldLabel_Alternativ_Type'));

        $sp = $contact->addChild('field', $this->escapeAND($user->self_phone_alternative));
        $sp->addAttribute('datafield', 'c23');
        $sp->addAttribute('type', 'text');
        $sp->addAttribute('display', $translate->translate('contact_settings_fieldLabel_Alternativ'));

        $sp = $contact->addChild('field', $this->escapeAND($user->self_phone_alternative_durchwahl));
        $sp->addAttribute('datafield', 'c24');
        $sp->addAttribute('type', 'text');
        $sp->addAttribute('display', $translate->translate('contact_settings_fieldLabel_Alternativ_Durchwahl'));

        $sp = $contact->addChild('field', $this->escapeAND($user->bank_name));
        $sp->addAttribute('datafield', 'c25');
        $sp->addAttribute('type', 'text');
        $sp->addAttribute('display', $translate->translate('contact_settings_fieldLabel_Alternativ_bank_name'));

        $sp = $contact->addChild('field', $this->escapeAND($user->bank_kto));
        $sp->addAttribute('datafield', 'c26');
        $sp->addAttribute('type', 'text');
        $sp->addAttribute('display', $translate->translate('contact_settings_fieldLabel_Alternativ_bank_kto'));

        $sp = $contact->addChild('field', $this->escapeAND($user->bank_blz));
        $sp->addAttribute('datafield', 'c27');
        $sp->addAttribute('type', 'text');
        $sp->addAttribute('display', $translate->translate('contact_settings_fieldLabel_Alternativ_bank_blz'));

        $sp = $contact->addChild('field', $this->escapeAND($user->bank_iban));
        $sp->addAttribute('datafield', 'c28');
        $sp->addAttribute('type', 'text');
        $sp->addAttribute('display', $translate->translate('contact_settings_fieldLabel_Alternativ_bank_iban'));

        $sp = $contact->addChild('field', $this->escapeAND($user->bank_bic));
        $sp->addAttribute('datafield', 'c29');
        $sp->addAttribute('type', 'text');
        $sp->addAttribute('display', $translate->translate('contact_settings_fieldLabel_Alternativ_bank_bic'));

        $sp = $contact->addChild('field', $this->escapeAND($user->bank_bank_name));
        $sp->addAttribute('datafield', 'c30');
        $sp->addAttribute('type', 'text');
        $sp->addAttribute('display', $translate->translate('contact_settings_fieldLabel_Alternativ_bank_bank_name'));

        $cfu = $contact->addChild('field', $this->escapeAND($user->self_function));
        $cfu->addAttribute('datafield', 'c31');
        $cfu->addAttribute('type', 'text');
        $cfu->addAttribute('display', $translate->translate('Funktion'));

        $cabt = $contact->addChild('field', $this->escapeAND($user->self_abteilung));
        $cabt->addAttribute('datafield', 'c32');
        $cabt->addAttribute('type', 'text');
        $cabt->addAttribute('display', $translate->translate('Abteilung'));

        $webt = $contact->addChild('field', $this->escapeAND($user->self_web));
        $webt->addAttribute('datafield', 'c32a');
        $webt->addAttribute('type', 'text');
        $webt->addAttribute('display', $translate->translate('Webseite'));

        $cabt = $contact->addChild('field', $this->escapeAND($user->getCustom1()));
        $cabt->addAttribute('datafield', 'v1');
        $cabt->addAttribute('type', 'text');
        $cabt->addAttribute('display', $translate->translate('v1'));

        $cabt = $contact->addChild('field', $this->escapeAND($user->getCustom2()));
        $cabt->addAttribute('datafield', 'v2');
        $cabt->addAttribute('type', 'text');
        $cabt->addAttribute('display', $translate->translate('v2'));

        $cabt = $contact->addChild('field', $this->escapeAND($user->getCustom3()));
        $cabt->addAttribute('datafield', 'v3');
        $cabt->addAttribute('type', 'text');
        $cabt->addAttribute('display', $translate->translate('v3'));

        $cabt = $contact->addChild('field', $this->escapeAND($user->getCustom4()));
        $cabt->addAttribute('datafield', 'v4');
        $cabt->addAttribute('type', 'text');
        $cabt->addAttribute('display', $translate->translate('v4'));

        $cabt = $contact->addChild('field', $this->escapeAND($user->getCustom5()));
        $cabt->addAttribute('datafield', 'v5');
        $cabt->addAttribute('type', 'text');
        $cabt->addAttribute('display', $translate->translate('v5'));

        $cabt = $contact->addChild('field', $this->escapeAND($user->getCustom6()));
        $cabt->addAttribute('datafield', 'v6');
        $cabt->addAttribute('type', 'text');
        $cabt->addAttribute('display', $translate->translate('v6'));

        $cabt = $contact->addChild('field', $this->escapeAND($user->getCustom7()));
        $cabt->addAttribute('datafield', 'v7');
        $cabt->addAttribute('type', 'text');
        $cabt->addAttribute('display', $translate->translate('v7'));

        $cabt = $contact->addChild('field', $this->escapeAND($user->getCustom8()));
        $cabt->addAttribute('datafield', 'v8');
        $cabt->addAttribute('type', 'text');
        $cabt->addAttribute('display', $translate->translate('v8'));

        $cabt = $contact->addChild('field', $this->escapeAND($user->getCustom9()));
        $cabt->addAttribute('datafield', 'v9');
        $cabt->addAttribute('type', 'text');
        $cabt->addAttribute('display', $translate->translate('v9'));

        $cabt = $contact->addChild('field', $this->escapeAND($user->getCustom10()));
        $cabt->addAttribute('datafield', 'v10');
        $cabt->addAttribute('type', 'text');
        $cabt->addAttribute('display', $translate->translate('v10'));

        $cabt = $contact->addChild('field', $this->escapeAND($user->getCustom11()));
        $cabt->addAttribute('datafield', 'v11');
        $cabt->addAttribute('type', 'text');
        $cabt->addAttribute('display', $translate->translate('v11'));

        $cabt = $contact->addChild('field', $this->escapeAND($user->getCustom12()));
        $cabt->addAttribute('datafield', 'v12');
        $cabt->addAttribute('type', 'text');
        $cabt->addAttribute('display', $translate->translate('v12'));

        $cabt = $contact->addChild('field', $this->escapeAND($user->getCustom13()));
        $cabt->addAttribute('datafield', 'v13');
        $cabt->addAttribute('type', 'text');
        $cabt->addAttribute('display', $translate->translate('v13'));

        $cabt = $contact->addChild('field', $this->escapeAND($user->getCustom14()));
        $cabt->addAttribute('datafield', 'v14');
        $cabt->addAttribute('type', 'text');
        $cabt->addAttribute('display', $translate->translate('v14'));

        $cabt = $contact->addChild('field', $this->escapeAND($user->getCustom15()));
        $cabt->addAttribute('datafield', 'v15');
        $cabt->addAttribute('type', 'text');
        $cabt->addAttribute('display', $translate->translate('v15'));

        $cabt = $contact->addChild('field', $this->escapeAND($user->getCustom16()));
        $cabt->addAttribute('datafield', 'v16');
        $cabt->addAttribute('type', 'text');
        $cabt->addAttribute('display', $translate->translate('v16'));

        $cabt = $contact->addChild('field', $this->escapeAND($user->getCustom17()));
        $cabt->addAttribute('datafield', 'v17');
        $cabt->addAttribute('type', 'text');
        $cabt->addAttribute('display', $translate->translate('v17'));

        $cabt = $contact->addChild('field', $this->escapeAND($user->getCustom18()));
        $cabt->addAttribute('datafield', 'v18');
        $cabt->addAttribute('type', 'text');
        $cabt->addAttribute('display', $translate->translate('v18'));

        $cabt = $contact->addChild('field', $this->escapeAND($user->getCustom19()));
        $cabt->addAttribute('datafield', 'v19');
        $cabt->addAttribute('type', 'text');
        $cabt->addAttribute('display', $translate->translate('v19'));

        $cabt = $contact->addChild('field', $this->escapeAND($user->getCustom20()));
        $cabt->addAttribute('datafield', 'v20');
        $cabt->addAttribute('type', 'text');
        $cabt->addAttribute('display', $translate->translate('v20'));

        $cabt = $contact->addChild('field', $this->escapeAND($user->getCustom21()));
        $cabt->addAttribute('datafield', 'v21');
        $cabt->addAttribute('type', 'text');
        $cabt->addAttribute('display', $translate->translate('v21'));

        $cabt = $contact->addChild('field', $this->escapeAND($user->getCustom22()));
        $cabt->addAttribute('datafield', 'v22');
        $cabt->addAttribute('type', 'text');
        $cabt->addAttribute('display', $translate->translate('v22'));

        $cabt = $contact->addChild('field', $this->escapeAND($user->getCustom23()));
        $cabt->addAttribute('datafield', 'v23');
        $cabt->addAttribute('type', 'text');
        $cabt->addAttribute('display', $translate->translate('v23'));

        $cabt = $contact->addChild('field', $this->escapeAND($user->getCustom24()));
        $cabt->addAttribute('datafield', 'v24');
        $cabt->addAttribute('type', 'text');
        $cabt->addAttribute('display', $translate->translate('v24'));

        try{
            $infos = json_decode(str_replace(PHP_EOL, '', $user->self_information), true);
        }catch(Exception $e) {
            $infos = array();
        }
        $fields_decode = array('department_2', 'street_2','house_number_2','country_2','country_1','zip_2','city_2','email_2','phone_lv_2','phone_vorwahl_2',
            'phone_2','phone_durchwahl_2','mobile_lv_2','mobile_vorwahl_2','mobile_2','fax_lv_2','fax_vorwahl_2',
            'fax_2','fax_durchwahl_2', 'internet_2','department_3','street_3','house_number_3','country_3','zip_3','city_3','city_4',
            'email_3','phone_lv_3','phone_vorwahl_3','phone_3','phone_durchwahl_3','mobile_lv_3','lkz_2','lkz_3','lkz_1','custom1','custom2','custom3',
            'mobile_vorwahl_3','mobile_3','fax_lv_3','fax_vorwahl_3','fax_3','fax_durchwahl_3', 'internet_3');

        if(file_exists(APPLICATION_PATH . '/design/clients/' . $this->view->shop->uid . '/templateDef.php')) {
            $fields = array();
            include_once(APPLICATION_PATH . '/design/clients/' . $this->view->shop->uid . '/templateDef.php');

            foreach($fields as $key => $row) {
                if(!is_array($row)){
                    $fields_decode[] = $row;
                }
            }
        }

        foreach($fields_decode as $arrKey) {


            if(isset($infos[$arrKey])) {
                $cab = $contact->addChild('field', $this->escapeAND($infos[$arrKey]));
                $cab->addAttribute('datafield', $arrKey);
                $cab->addAttribute('type', 'text');
                $cab->addAttribute('display', $translate->translate($arrKey));
            }else{
                $cab = $contact->addChild('field', "");
                $cab->addAttribute('datafield', $arrKey);
                $cab->addAttribute('type', 'text');
                $cab->addAttribute('display', $translate->translate($arrKey));
            }

        }

        $account = $ds->addChild('account');
        $account->addAttribute('display', 'Filiale');
        $account->addAttribute('id', 'filiale');

        if($user->Account) {
            $st = $account->addChild('field', $this->escapeAND($user->Account->typ));
            $st->addAttribute('datafield', 'a0');
            $st->addAttribute('type', 'text');
            $st->addAttribute('display', $translate->translate('Typ'));

            $config = new Zend_Config_Ini(APPLICATION_PATH . '/configs/' . '/systemdefaults.ini', 'account_typ');
            $config = $config->toArray();

            $stt = $account->addChild('field', $this->escapeAND($config[$user->Account->typ]));
            $stt->addAttribute('datafield', 'a0text');
            $stt->addAttribute('type', 'text');
            $stt->addAttribute('display', $translate->translate('Typ'));

            $sf = $account->addChild('field', $this->escapeAND($user->Account->company));
            $sf->addAttribute('datafield', 'a1');
            $sf->addAttribute('type', 'text');
            $sf->addAttribute('display', $translate->translate('contact_settings_fieldLabel_Company'));

            $ss = $account->addChild('field', $this->escapeAND($user->Account->street));
            $ss->addAttribute('datafield', 'a2');
            $ss->addAttribute('type', 'text');
            $ss->addAttribute('display', $translate->translate('contact_settings_fieldLabel_Street'));

            $sh = $account->addChild('field', $this->escapeAND($user->Account->house_number));
            $sh->addAttribute('datafield', 'a3');
            $sh->addAttribute('type', 'text');
            $sh->addAttribute('display', $translate->translate('contact_settings_fieldLabel_Housenumber'));

            $sz = $account->addChild('field', $this->escapeAND($user->Account->country));
            $sz->addAttribute('datafield', 'a4');
            $sz->addAttribute('type', 'text');
            $sz->addAttribute('display', $translate->translate('contact_settings_fieldLabel_Lkz'));

            $sz = $account->addChild('field', $this->escapeAND($user->Account->zip));
            $sz->addAttribute('datafield', 'a5');
            $sz->addAttribute('type', 'text');
            $sz->addAttribute('display', $translate->translate('contact_settings_fieldLabel_Zip'));

            $sc = $account->addChild('field', $this->escapeAND($user->Account->city));
            $sc->addAttribute('datafield', 'a6');
            $sc->addAttribute('type', 'text');
            $sc->addAttribute('display', $translate->translate('contact_settings_fieldLabel_City'));

            $se = $account->addChild('field', $this->escapeAND($user->Account->email));
            $se->addAttribute('datafield', 'a7');
            $se->addAttribute('type', 'text');
            $se->addAttribute('display', $translate->translate('contact_settings_fieldLabel_Email'));

            $se = $account->addChild('field', $this->escapeAND($user->Account->appendix));
            $se->addAttribute('datafield', 'a8');
            $se->addAttribute('type', 'text');
            $se->addAttribute('display', $translate->translate('contact_settings_fieldLabel_Appendix'));

            $se = $account->addChild('field', $this->escapeAND($user->Account->homepage));
            $se->addAttribute('datafield', 'a9');
            $se->addAttribute('type', 'text');
            $se->addAttribute('display', $translate->translate('Homepage'));

            $se = $account->addChild('field', $this->escapeAND($user->Account->usid));
            $se->addAttribute('datafield', 'a10');
            $se->addAttribute('type', 'text');
            $se->addAttribute('display', $translate->translate('contact_settings_fieldLabel_USID'));

            $sp = $account->addChild('field', $this->escapeAND($user->Account->phone_lv));
            $sp->addAttribute('datafield', 'a12lv');
            $sp->addAttribute('type', 'text');
            $sp->addAttribute('display', $translate->translate('Telefon LV'));

            $sp = $account->addChild('field', $this->escapeAND($user->Account->phone_vorwahl));
            $sp->addAttribute('datafield', 'a11');
            $sp->addAttribute('type', 'text');
            $sp->addAttribute('display', $translate->translate('contact_settings_fieldLabel_Telefon_Vorwahl'));

            $sp = $account->addChild('field', $this->escapeAND($user->Account->phone));
            $sp->addAttribute('datafield', 'a12');
            $sp->addAttribute('type', 'text');
            $sp->addAttribute('display', $translate->translate('contact_settings_fieldLabel_Telefon'));

            $sp = $account->addChild('field', $this->escapeAND($user->Account->phone_durchwahl));
            $sp->addAttribute('datafield', 'a13');
            $sp->addAttribute('type', 'text');
            $sp->addAttribute('display', $translate->translate('contact_settings_fieldLabel_Telefon_Durchwahl'));

            $sp = $account->addChild('field', $this->escapeAND($user->Account->mobile_lv));
            $sp->addAttribute('datafield', 'a15lv');
            $sp->addAttribute('type', 'text');
            $sp->addAttribute('display', $translate->translate('Mobil LV'));

            $sp = $account->addChild('field', $this->escapeAND($user->Account->mobile_vorwahl));
            $sp->addAttribute('datafield', 'a14');
            $sp->addAttribute('type', 'text');
            $sp->addAttribute('display', $translate->translate('contact_settings_fieldLabel_Mobil_Vorwahl'));

            $sp = $account->addChild('field', $this->escapeAND($user->Account->mobile));
            $sp->addAttribute('datafield', 'a15');
            $sp->addAttribute('type', 'text');
            $sp->addAttribute('display', $translate->translate('contact_settings_fieldLabel_Mobil'));

            $sp = $account->addChild('field', $this->escapeAND($user->Account->mobile_durchwahl));
            $sp->addAttribute('datafield', 'a16');
            $sp->addAttribute('type', 'text');
            $sp->addAttribute('display', $translate->translate('contact_settings_fieldLabel_Mobil_Durchwahl'));

            $sp = $account->addChild('field', $this->escapeAND($user->Account->fax_lv));
            $sp->addAttribute('datafield', 'a18lv');
            $sp->addAttribute('type', 'text');
            $sp->addAttribute('display', $translate->translate('Fax LV'));

            $sp = $account->addChild('field', $this->escapeAND($user->Account->fax_vorwahl));
            $sp->addAttribute('datafield', 'a17');
            $sp->addAttribute('type', 'text');
            $sp->addAttribute('display', $translate->translate('contact_settings_fieldLabel_Fax_Vorwahl'));

            $sp = $account->addChild('field', $this->escapeAND($user->Account->fax));
            $sp->addAttribute('datafield', 'a18');
            $sp->addAttribute('type', 'text');
            $sp->addAttribute('display', $translate->translate('contact_settings_fieldLabel_Fax'));

            $sp = $account->addChild('field', $this->escapeAND($user->Account->fax_durchwahl));
            $sp->addAttribute('datafield', 'a19');
            $sp->addAttribute('type', 'text');
            $sp->addAttribute('display', $translate->translate('contact_settings_fieldLabel_Fax_Durchwahl'));

            $sp = $account->addChild('field', $this->escapeAND($user->Account->alternativ_lv));
            $sp->addAttribute('datafield', 'a21lv');
            $sp->addAttribute('type', 'text');
            $sp->addAttribute('display', $translate->translate('Alternative LV'));

            $sp = $account->addChild('field', $this->escapeAND($user->Account->alternativ_type));
            $sp->addAttribute('datafield', 'a20');
            $sp->addAttribute('type', 'text');
            $sp->addAttribute('display', $translate->translate('contact_settings_fieldLabel_Alternativ_Type'));

            $sp = $account->addChild('field', $this->escapeAND($user->Account->alternativ));
            $sp->addAttribute('datafield', 'a21');
            $sp->addAttribute('type', 'text');
            $sp->addAttribute('display', $translate->translate('contact_settings_fieldLabel_Alternativ'));

            $sp = $account->addChild('field', $this->escapeAND($user->Account->alternativ_durchwahl));
            $sp->addAttribute('datafield', 'a22');
            $sp->addAttribute('type', 'text');
            $sp->addAttribute('display', $translate->translate('contact_settings_fieldLabel_Alternativ_Durchwahl'));

            $sp = $account->addChild('field', $this->escapeAND($user->Account->bank_name));
            $sp->addAttribute('datafield', 'a23');
            $sp->addAttribute('type', 'text');
            $sp->addAttribute('display', $translate->translate('contact_settings_fieldLabel_Alternativ_bank_name'));

            $sp = $account->addChild('field', $this->escapeAND($user->Account->bank_kto));
            $sp->addAttribute('datafield', 'a24');
            $sp->addAttribute('type', 'text');
            $sp->addAttribute('display', $translate->translate('contact_settings_fieldLabel_Alternativ_bank_kto'));

            $sp = $account->addChild('field', $this->escapeAND($user->Account->bank_blz));
            $sp->addAttribute('datafield', 'a25');
            $sp->addAttribute('type', 'text');
            $sp->addAttribute('display', $translate->translate('contact_settings_fieldLabel_Alternativ_bank_blz'));

            $sp = $account->addChild('field', $this->escapeAND($user->Account->bank_iban));
            $sp->addAttribute('datafield', 'a26');
            $sp->addAttribute('type', 'text');
            $sp->addAttribute('display', $translate->translate('contact_settings_fieldLabel_Alternativ_bank_iban'));

            $sp = $account->addChild('field', $this->escapeAND($user->Account->bank_bic));
            $sp->addAttribute('datafield', 'a27');
            $sp->addAttribute('type', 'text');
            $sp->addAttribute('display', $translate->translate('contact_settings_fieldLabel_Alternativ_bank_bic'));

            $sp = $account->addChild('field', $this->escapeAND($user->Account->bank_bank_name));
            $sp->addAttribute('datafield', 'a28');
            $sp->addAttribute('type', 'text');
            $sp->addAttribute('display', $translate->translate('contact_settings_fieldLabel_Alternativ_bank_bank_name'));





            $sp = $account->addChild('field', $this->escapeAND($user->Account->gz));
            $sp->addAttribute('datafield', 'a29');
            $sp->addAttribute('type', 'text');
            $sp->addAttribute('display', $translate->translate('juristischeZugehoerigkeit'));

            $sp = $account->addChild('field', $this->escapeAND($user->Account->gf));
            $sp->addAttribute('datafield', 'a30');
            $sp->addAttribute('type', 'text');
            $sp->addAttribute('display', $translate->translate('Geschäftsführer'));

            $sp = $account->addChild('field', $this->escapeAND($user->Account->gsitz));
            $sp->addAttribute('datafield', 'a31');
            $sp->addAttribute('type', 'text');
            $sp->addAttribute('display', $translate->translate('SitzDerGesellschaft'));

            $sp = $account->addChild('field', $this->escapeAND($user->Account->handelsregister));
            $sp->addAttribute('datafield', 'a32');
            $sp->addAttribute('type', 'text');
            $sp->addAttribute('display', $translate->translate('Handelsregister'));

            $sp = $account->addChild('field', $this->escapeAND($user->Account->hrb));
            $sp->addAttribute('datafield', 'a33');
            $sp->addAttribute('type', 'text');
            $sp->addAttribute('display', $translate->translate('HRB'));

            $sp = $account->addChild('field', $this->escapeAND($user->Account->vorsitz));
            $sp->addAttribute('datafield', 'a34');
            $sp->addAttribute('type', 'text');
            $sp->addAttribute('display', $translate->translate('Aufsichtsratsvorsitzender'));


            $sp = $account->addChild('field', $this->escapeAND($user->Account->mega_code));
            $sp->addAttribute('datafield', 'a35');
            $sp->addAttribute('type', 'text');
            $sp->addAttribute('display', $translate->translate('Mega Code'));

            $sp = $account->addChild('field', $this->escapeAND($user->Account->hotel_name));
            $sp->addAttribute('datafield', 'a36');
            $sp->addAttribute('type', 'text');
            $sp->addAttribute('display', $translate->translate('Hotelname 2'));

            $sp = $account->addChild('field', $this->escapeAND($user->Account->zimmer));
            $sp->addAttribute('datafield', 'a37');
            $sp->addAttribute('type', 'text');
            $sp->addAttribute('display', $translate->translate('Zimmer'));



            $sp = $account->addChild('field', $this->escapeAND($user->Account->getAnrede1()));
            $sp->addAttribute('datafield', 'a38');
            $sp->addAttribute('type', 'text');
            $sp->addAttribute('display', $translate->translate('Anrede1'));


            $sp = $account->addChild('field', $this->escapeAND($user->Account->firstname1));
            $sp->addAttribute('datafield', 'a39');
            $sp->addAttribute('type', 'text');
            $sp->addAttribute('display', $translate->translate('Vorname1'));


            $sp = $account->addChild('field', $this->escapeAND($user->Account->lastname1));
            $sp->addAttribute('datafield', 'a40');
            $sp->addAttribute('type', 'text');
            $sp->addAttribute('display', $translate->translate('Nachname2'));


            $sp = $account->addChild('field', $this->escapeAND($user->Account->getAnrede2()));
            $sp->addAttribute('datafield', 'a41');
            $sp->addAttribute('type', 'text');
            $sp->addAttribute('display', $translate->translate('Anrede2'));


            $sp = $account->addChild('field', $this->escapeAND($user->Account->firstname2));
            $sp->addAttribute('datafield', 'a42');
            $sp->addAttribute('type', 'text');
            $sp->addAttribute('display', $translate->translate('Vorname2'));


            $sp = $account->addChild('field', $this->escapeAND($user->Account->lastname2));
            $sp->addAttribute('datafield', 'a43');
            $sp->addAttribute('type', 'text');
            $sp->addAttribute('display', $translate->translate('Nachname2'));

            $sp = $account->addChild('field', $this->escapeAND($user->Account->sub_hotel_name));
            $sp->addAttribute('datafield', 'a44');
            $sp->addAttribute('type', 'text');
            $sp->addAttribute('display', $translate->translate('Hotelname 3'));
        }

        echo $xml->asXML();

        /*die('<?xml version="1.0" encoding="UTF-8"?>
        <root>
          <dataset>
            <contact>
              <field datafield="self_firstname">Vorname1</field>
              <field datafield="self_lastname">Nachname1</field>
              <field datafield="self_street">Straße1</field>
              <field datafield="self_house_number">Nr.1</field>
              <field datafield="self_zip">PLZ1</field>
              <field datafield="self_city">Ort1</field>
            </contact>
            <account id="account">
              <field datafield="name">Zentrale1</field>
              <field datafield="self_street">Zentralestraße1</field>
              <field datafield="self_house_number">Nr.1</field>
              <field datafield="self_zip">PLZ1</field>
              <field datafield="self_city">Ort1</field>
            </account>
          </dataset>
          <dataset>
            <contact>
              <field datafield="self_firstname">Vorname2</field>
              <field datafield="self_lastname">Nachname2</field>
              <field datafield="self_street">Straße2</field>
              <field datafield="self_house_number">Nr.2</field>
              <field datafield="self_zip">PLZ2</field>
              <field datafield="self_city">Ort2</field>
            </contact>
            <account id="account">
              <field datafield="name">Zentrale2</field>
              <field datafield="self_street">Zentralestraße2</field>
              <field datafield="self_house_number">Nr.2</field>
              <field datafield="self_zip">PLZ2</field>
              <field datafield="self_city">Ort2</field>
            </account>
          </dataset>
          <!-- weitere Datensätze -->
        </root>');
        */
    }

    public function storepackageAction() {
        $values = TP_Crypt::decrypt($this->_getParam('ARTID'));
        $article = Doctrine_Query::create()
            ->from('Article c')
            ->where('c.uuid = ?', array($values['UUID']))
            ->fetchOne();
        $uuid = $values['UUID'];

        if($values['load'] == 4) {
            $basket = new TP_Basket ();

            $sessart = $basket->getBasketArtikel($values['basketposid']);
            Zend_Registry::get('log')->debug($sessart->getRef());
            Zend_Registry::get('log')->debug(print_r($values, true));

            $adapter = new Zend_File_Transfer_Adapter_Http();
            if ($this->user) {
                if (!file_exists(APPLICATION_PATH . '/../market/templateprint/user/' . $sessart->getTemplatePrintId())) {
                    mkdir(APPLICATION_PATH . '/../market/templateprint/user/' . $sessart->getTemplatePrintId());
                }
                $adapter->setDestination(APPLICATION_PATH . '/../market/templateprint/user/' . $sessart->getTemplatePrintId());
                Zend_Registry::get('log')->debug(APPLICATION_PATH . '/../market/templateprint/user/' . $sessart->getTemplatePrintId());


            } else {
                if (!file_exists(APPLICATION_PATH . '/../market/templateprint/guest/' . $sessart->getTemplatePrintId())) {
                    mkdir(APPLICATION_PATH . '/../market/templateprint/guest/' . $sessart->getTemplatePrintId());
                }
                $adapter->setDestination(APPLICATION_PATH . '/../market/templateprint/guest/' . $sessart->getTemplatePrintId());
                Zend_Registry::get('log')->debug(APPLICATION_PATH . '/../market/templateprint/guest/' . $sessart->getTemplatePrintId());
            }

            if (!$adapter->receive()) {
                $messages = $adapter->getMessages();
            }

            if(file_exists(APPLICATION_PATH . '/../market/templateprint/user/' . $sessart->getTemplatePrintId() . '/result.zip')) {
                $zip = new ZipArchive;
                $res = $zip->open(APPLICATION_PATH . '/../market/templateprint/user/' . $sessart->getTemplatePrintId() . '/result.zip');
                $zip->extractTo(APPLICATION_PATH . '/../market/templateprint/user/' . $sessart->getTemplatePrintId());
                $zip->close();
                rename(APPLICATION_PATH . '/../market/templateprint/user/' . $sessart->getTemplatePrintId() . '/dataset0.pdf', APPLICATION_PATH . '/../market/templateprint/user/' . $sessart->getTemplatePrintId() . '/final.pdf');
            }elseif(file_exists(APPLICATION_PATH . '/../market/templateprint/guest/' . $sessart->getTemplatePrintId() . '/result.zip')) {
                $zip = new ZipArchive;
                $res = $zip->open(APPLICATION_PATH . '/../market/templateprint/guest/' . $sessart->getTemplatePrintId() . '/result.zip');
                $zip->extractTo(APPLICATION_PATH . '/../market/templateprint/guest/' . $sessart->getTemplatePrintId());
                $zip->close();
                rename(APPLICATION_PATH . '/../market/templateprint/guest/' . $sessart->getTemplatePrintId() . '/dataset0.pdf', APPLICATION_PATH . '/../market/templateprint/guest/' . $sessart->getTemplatePrintId() . '/final.pdf');
            }

            $articleSession = new TP_Layoutersession();
            $articleSess = $articleSession->getLayouterArticle($uuid, $values['LAYOUTERID']);
            $articleSess->setOrgArticleId($uuid);
            $articleSess->setTitle($values['title']);
            $articleSess->setRef($sessart->getRef());
            $articleSess->setKst($sessart->getKst());
            $articleSess->setTemplatePrintContactId($values['userid']);
            $articleSess->saveMongo();

            $sessart->setTemplatePrintId($sessart->getTemplatePrintId());
            $articleSess->setTemplatePrintId($sessart->getTemplatePrintId());

            $sessart->setLayouterId($values['LAYOUTERID']);
            $sessart->setTemplatePrintContactId($values['userid']);
            session_commit();
            return;
        }


        $saved = simplexml_load_string($this->_getParam('xml'));

        $userID = $saved->xpath('//editor/parameters/parameter[@name="w2puserid"]');
        $contact = Doctrine_Query::create()
            ->from('Contact m')
            ->where('uuid = ?')->fetchOne(array((string)$userID[0]['value']));

        Zend_Registry::get('log')->debug(print_r($values, true));
        Zend_Registry::get('log')->debug(print_r($saved, true));

        if($values['load'] == 3 || ($this->shop->isTpSaveUserData() && $values['COPY'] != 3)) {

            $self_firstname = $saved->xpath('//editor/modifications/modification[@key="self_firstname.text"]');
            $name1 = $saved->xpath('//editor/modifications/modification[@key="name.text"]');
            $name2 = $saved->xpath('//editor/modifications/modification[@key="nameblock_name.text"]');
            $self_lastname = $saved->xpath('//editor/modifications/modification[@key="self_lastname.text"]');
            $self_street = $saved->xpath('//editor/modifications/modification[@key="self_street.text"]');
            $str1 = $saved->xpath('//editor/modifications/modification[@key="str1.text"]');
            $strnr = $saved->xpath('//editor/modifications/modification[@key="strnr.text"]');
            $self_house_number = $saved->xpath('//editor/modifications/modification[@key="self_house_number.text"]');
            $self_zip = $saved->xpath('//editor/modifications/modification[@key="self_zip.text"]');
            $self_city = $saved->xpath('//editor/modifications/modification[@key="self_city.text"]');
            $self_department = $saved->xpath('//editor/modifications/modification[@key="self_department.text"]');
            $self_department2 = $saved->xpath('//editor/modifications/modification[@key="self_department2.text"]');
            $self_phone = $saved->xpath('//editor/modifications/modification[@key="self_phone.text"]');
            $email = $saved->xpath('//editor/modifications/modification[@key="email.text"]');


            $phc = $saved->xpath('//editor/modifications/modification[@key="tel.value.phonecountry"]');
            $phn = $saved->xpath('//editor/modifications/modification[@key="tel.value.phonenumber"]');
            $pha = $saved->xpath('//editor/modifications/modification[@key="tel.value.phonearea"]');
            $phe = $saved->xpath('//editor/modifications/modification[@key="tel.value.phoneextension"]');

            if(isset($phc) && !empty($phc)) {
                $contact->self_phone_lv = (string)$phc[0]['value'];
            }
            if(isset($phn) && !empty($phn)) {
                $contact->self_phone = (string)$phn[0]['value'];
            }
            if(isset($pha) && !empty($pha)) {
                $contact->self_phone_vorwahl = (string)$pha[0]['value'];
            }
            if(isset($phe) && !empty($phe)) {
                $contact->self_phone_durchwahl = (string)$phe[0]['value'];
            }



            $phc = $saved->xpath('//editor/modifications/modification[@key="phone.value.phonecountry"]');
            $phn = $saved->xpath('//editor/modifications/modification[@key="phone.value.phonenumber"]');
            $pha = $saved->xpath('//editor/modifications/modification[@key="phone.value.phonearea"]');
            $phe = $saved->xpath('//editor/modifications/modification[@key="phone.value.phoneextension"]');

            if(isset($phc) && !empty($phc)) {
                $contact->self_phone_lv = (string)$phc[0]['value'];
            }
            if(isset($phn) && !empty($phn)) {
                $contact->self_phone = (string)$phn[0]['value'];
            }
            if(isset($pha) && !empty($pha)) {
                $contact->self_phone_vorwahl = (string)$pha[0]['value'];
            }
            if(isset($phe) && !empty($phe)) {
                $contact->self_phone_durchwahl = (string)$phe[0]['value'];
            }

            $fc = $saved->xpath('//editor/modifications/modification[@key="fax.value.phonecountry"]');
            $fn = $saved->xpath('//editor/modifications/modification[@key="fax.value.phonenumber"]');
            $fa = $saved->xpath('//editor/modifications/modification[@key="fax.value.phonearea"]');
            $fe = $saved->xpath('//editor/modifications/modification[@key="fax.value.phoneextension"]');

            if(isset($fc) && !empty($fc)) {
                $contact->self_fax_lv = (string)$fc[0]['value'];
            }
            if(isset($fn) && !empty($fn)) {
                $contact->self_fax_phone = (string)$fn[0]['value'];
            }
            if(isset($fa) && !empty($fa)) {
                $contact->self_fax_vorwahl = (string)$fa[0]['value'];
            }
            if(isset($fe) && !empty($fe)) {
                $contact->self_fax_durchwahl = (string)$fe[0]['value'];
            }

            $mc = $saved->xpath('//editor/modifications/modification[@key="mobil.value.phonecountry"]');
            $ma = $saved->xpath('//editor/modifications/modification[@key="mobil.value.phonearea"]');
            $mn = $saved->xpath('//editor/modifications/modification[@key="mobil.value.phonenumber"]');
            $me = $saved->xpath('//editor/modifications/modification[@key="mobil.value.phoneextension"]');

            if(isset($mc) && !empty($mc)) {
                $contact->self_mobile_lv = (string)$mc[0]['value'];
            }
            if(isset($ma) && !empty($ma)) {
                $contact->self_mobile_vorwahl = (string)$ma[0]['value'];
            }
            if(isset($mn) && !empty($mn)) {
                $contact->self_phone_mobile = (string)$mn[0]['value'];
            }
            if(isset($me) && !empty($me)) {
                $contact->self_mobile_durchwahl = (string)$me[0]['value'];
            }

            $mc = $saved->xpath('//editor/modifications/modification[@key="mobile.value.phonecountry"]');
            $ma = $saved->xpath('//editor/modifications/modification[@key="mobile.value.phonearea"]');
            $mn = $saved->xpath('//editor/modifications/modification[@key="mobile.value.phonenumber"]');
            $me = $saved->xpath('//editor/modifications/modification[@key="mobile.value.phoneextension"]');

            if(isset($mc) && !empty($mc)) {
                $contact->self_mobile_lv = (string)$mc[0]['value'];
            }
            if(isset($ma) && !empty($ma)) {
                $contact->self_mobile_vorwahl = (string)$ma[0]['value'];
            }
            if(isset($mn) && !empty($mn)) {
                $contact->self_phone_mobile = (string)$mn[0]['value'];
            }
            if(isset($me) && !empty($me)) {
                $contact->self_mobile_durchwahl = (string)$me[0]['value'];
            }

            $nname = $saved->xpath('//editor/modifications/modification[@key="nname.text"]');
            $vname = $saved->xpath('//editor/modifications/modification[@key="vname.text"]');

            if(isset($nname) && !empty($nname)) {
                $contact->self_lastname = (string)$nname[0]['value'];
            }
            if(isset($vname) && !empty($vname)) {
                $contact->self_firstname = (string)$vname[0]['value'];
            }
            if(isset($email) && !empty($email)) {
                $contact->self_email = (string)$email[0]['value'];
            }

            if($this->shop->id == 276) {
                $email = $saved->xpath('//editor/modifications/modification[@key="email.value.email"]');
                if(isset($email) && !empty($email)) {
                    $contact->self_email = (string)$email[0]['value'] . '@deutsche-finanzagentur.de';
                }
            }
            if($this->shop->id == 272) {
                $email = $saved->xpath('//editor/modifications/modification[@key="email.value.email"]');
                if(isset($email) && !empty($email)) {
                    $contact->self_email = (string)$email[0]['value'] . '@graeper.de';
                }
            }
            if($uuid == '0001-00010103-5924428b-f4ac-a2ed0446') {
                $email = $saved->xpath('//editor/modifications/modification[@key="email.value.email"]');
                if(isset($email) && !empty($email)) {
                    $contact->self_email = (string)$email[0]['value'] . '@pa-ats.com';
                }
            }
            if($this->shop->uid == '0001-5dd94eb2-524bccb3-c27a-0101cea5') {
                $email = $saved->xpath('//editor/modifications/modification[@key="email.value.email"]');
                if(isset($email) && !empty($email)) {
                    $contact->self_email = (string)$email[0]['value'] . '@sparkasse-leerwittmund.de';
                }
            }

            if($this->shop->uid == '0001-5dcee5d0-526d5f6e-f5a6-57f856cf') {
                $email = $saved->xpath('//editor/modifications/modification[@key="email.value.email"]');
                if(isset($email) && !empty($email)) {
                    $contact->self_email = (string)$email[0]['value'] . '@it.niedersachsen.de';
                }
            }


            $pos = $saved->xpath('//editor/modifications/modification[@key="pos.text"]');
            $zusatz = $saved->xpath('//editor/modifications/modification[@key="abt.text"]');

            if(isset($pos) && !empty($pos)) {
                $contact->self_function = (string)$pos[0]['value'];
            }

            if(isset($zusatz) && !empty($zusatz)) {
                $contact->self_abteilung = (string)$zusatz[0]['value'];
            }

            $adr1 = $saved->xpath('//editor/modifications/modification[@key="adr1.text"]');
            $email = $saved->xpath('//editor/modifications/modification[@key="email.value.email"]');

            if(isset($adr1) && !empty($adr1)) {
                $adr1 = explode(" ", (string)$adr1[0]['value']);
                $contact->self_street = $adr1[0];
                $contact->self_house_number = $adr1[1];
            }

            $strasse = $saved->xpath('//editor/modifications/modification[@key="strasse.text"]');
            $ort = $saved->xpath('//editor/modifications/modification[@key="ort.text"]');

            if(isset($self_firstname) && !empty($self_firstname)) {
                $contact->self_firstname = (string)$self_firstname[0]['value'];
            }
            if(isset($self_lastname) && !empty($self_lastname)) {
                $contact->self_lastname = (string)$self_lastname[0]['value'];
            }
            if(isset($name1) && !empty($name1)) {
                $name = explode(" ", (string)$name1[0]['value']);
                if(isset($name[0])) {
                    $contact->self_firstname = $name[0];
                }
                if(isset($name[1])) {
                    $contact->self_lastname = $name[1];
                }
            }
            if(isset($name2) && !empty($name2)) {
                $name = explode(" ", (string)$name2[0]['value']);
                if(isset($name[0])) {
                    $contact->self_firstname = $name[0];
                }
                if(isset($name[1])) {
                    $contact->self_lastname = $name[1];
                }
            }
            if(isset($self_street) && !empty($self_street)) {
                $contact->self_street = (string)$self_street[0]['value'];
            }
            if(isset($str1) && !empty($str1)) {
                $contact->self_street = (string)$str1[0]['value'];
            }
            if(isset($self_house_number) && !empty($self_house_number)) {
                $contact->self_house_number = (string)$self_house_number[0]['value'];
            }
            if(isset($strnr) && !empty($strnr)) {
                $contact->self_house_number = (string)$strnr[0]['value'];
            }
            if(isset($self_zip) && !empty($self_zip)) {
                $contact->self_zip = (string)$self_zip[0]['value'];
            }
            if(isset($self_city) && !empty($self_city)) {
                $contact->self_city = (string)$self_city[0]['value'];
            }
            if(isset($self_department) && !empty($self_department)) {
                $contact->self_department = (string)$self_department[0]['value'];
            }
            if(isset($self_department2) && !empty($self_department2)) {
                $contact->self_department2 = (string)$self_department2[0]['value'];
            }
            if(isset($self_phone) && !empty($self_phone)) {
                $contact->self_phone = (string)$self_phone[0]['value'];
            }
            if(isset($strasse) && !empty($strasse)) {
                $strasse = explode(" ", (string)$strasse[0]['value']);
                if(isset($strasse[0])) {
                    $contact->self_street = $strasse[0];
                }
                if(isset($strasse[1])) {
                    $contact->self_house_number = $strasse[1];
                }
            }
            $beruf = $saved->xpath('//editor/modifications/modification[@key="beruf.text"]');

            if(isset($beruf) && !empty($beruf)) {
                $contact->self_function = (string)$beruf[0]['value'];
            }

            $vorname = $saved->xpath('//editor/modifications/modification[@key="vorname.text"]');

            if(isset($vorname) && !empty($vorname)) {
                $contact->self_firstname = (string)$vorname[0]['value'];
            }

            $nachname = $saved->xpath('//editor/modifications/modification[@key="nachname.text"]');

            if(isset($nachname) && !empty($nachname)) {
                $contact->self_lastname = (string)$nachname[0]['value'];
            }

            $phonedurchwahl = $saved->xpath('//editor/modifications/modification[@key="phonedurchwahl.text"]');

            if(isset($phonedurchwahl) && !empty($phonedurchwahl)) {
                $contact->self_phone_durchwahl = (string)$phonedurchwahl[0]['value'];
            }

            $phonedurchwahl = $saved->xpath('//editor/modifications/modification[@key="phonedurchwahl.text"]');
            $phonevw = $saved->xpath('//editor/modifications/modification[@key="phonevorwahl.text"]');
            $phonelv = $saved->xpath('//editor/modifications/modification[@key="phonelv.text"]');

            if(isset($phonedurchwahl) && !empty($phonedurchwahl)) {
                $contact->self_phone_durchwahl = (string)$phonedurchwahl[0]['value'];
            }

            if(isset($phonevw) && !empty($phonevw)) {
                $contact->self_phone_vorwahl = (string)$phonevw[0]['value'];
            }

            if(isset($phonelv) && !empty($phonelv)) {
                $contact->self_phone_lv = (string)$phonelv[0]['value'];
            }

            $alternatelv = $saved->xpath('//editor/modifications/modification[@key="alternatelv.text"]');
            $alternatevorwahl = $saved->xpath('//editor/modifications/modification[@key="alternatevorwahl.text"]');
            $alternatephone = $saved->xpath('//editor/modifications/modification[@key="alternatephone.text"]');
            $alternatedurchwahl = $saved->xpath('//editor/modifications/modification[@key="alternatedurchwahl.text"]');

            if(isset($alternatelv) && !empty($alternatelv)) {
                $contact->self_phone_alternative_lv = (string)$alternatelv[0]['value'];
            }

            if(isset($alternatevorwahl) && !empty($alternatevorwahl)) {
                $contact->self_phone_alternative_type = (string)$alternatevorwahl[0]['value'];
            }

            if(isset($alternatephone) && !empty($alternatephone)) {
                $contact->self_phone_alternative = (string)$alternatephone[0]['value'];
            }

            if(isset($alternatedurchwahl) && !empty($alternatedurchwahl)) {
                $contact->self_phone_alternative_durchwahl = (string)$alternatedurchwahl[0]['value'];
            }

            $bankinhaber = $saved->xpath('//editor/modifications/modification[@key="bankinhaber.text"]');
            $bankkto = $saved->xpath('//editor/modifications/modification[@key="bankkto.text"]');
            $bankblz = $saved->xpath('//editor/modifications/modification[@key="bankblz.text"]');
            $bankiban = $saved->xpath('//editor/modifications/modification[@key="bankiban.text"]');
            $bankbic = $saved->xpath('//editor/modifications/modification[@key="bankbic.text"]');
            $bankname = $saved->xpath('//editor/modifications/modification[@key="bankname.text"]');

            if(isset($bankinhaber) && !empty($bankinhaber)) {
                $contact->bank_name = (string)$bankinhaber[0]['value'];
            }

            if(isset($bankkto) && !empty($bankkto)) {
                $contact->bank_kto = (string)$bankkto[0]['value'];
            }

            if(isset($bankblz) && !empty($bankblz)) {
                $contact->bank_blz = (string)$bankblz[0]['value'];
            }

            if(isset($bankiban) && !empty($bankiban)) {
                $contact->bank_iban = (string)$bankiban[0]['value'];
            }

            if(isset($bankbic) && !empty($bankbic)) {
                $contact->bank_bic = (string)$bankbic[0]['value'];
            }

            if(isset($bankname) && !empty($bankname)) {
                $contact->bank_bank_name = (string)$bankinhaber[0]['value'];
            }

            if(isset($ort) && !empty($ort)) {
                $ort = explode(" ", (string)$ort[0]['value']);
                if(isset($ort[0])) {
                    $contact->self_zip = $ort[0];
                }
                if(isset($ort[1])) {
                    $contact->self_city = $ort[1];
                }
            }

            $v1 = $saved->xpath('//editor/modifications/modification[@key="v1.text"]');
            if(isset($v1) && empty($v1)) {
                $v1 = $saved->xpath('//editor/modifications/modification[@key="v1.value.email"]');
            }
            if(isset($v1) && empty($v1)) {
                $v1 = $saved->xpath('//editor/modifications/modification[@key="v1.value.text"]');
            }
            $v2 = $saved->xpath('//editor/modifications/modification[@key="v2.text"]');
            if(isset($v2) && empty($v2)) {
                $v2 = $saved->xpath('//editor/modifications/modification[@key="v2.value.email"]');
            }
            if(isset($v2) && empty($v2)) {
                $v2 = $saved->xpath('//editor/modifications/modification[@key="v2.value.text"]');
            }
            $v3 = $saved->xpath('//editor/modifications/modification[@key="v3.text"]');
            if(isset($v3) && empty($v3)) {
                $v3 = $saved->xpath('//editor/modifications/modification[@key="v3.value.email"]');
            }
            if(isset($v3) && empty($v3)) {
                $v3 = $saved->xpath('//editor/modifications/modification[@key="v3.value.text"]');
            }
            $v4 = $saved->xpath('//editor/modifications/modification[@key="v4.text"]');
            if(isset($v4) && empty($v4)) {
                $v4 = $saved->xpath('//editor/modifications/modification[@key="v4.value.email"]');
            }
            if(isset($v4) && empty($v4)) {
                $v4 = $saved->xpath('//editor/modifications/modification[@key="v4.value.text"]');
            }
            $v5 = $saved->xpath('//editor/modifications/modification[@key="v5.text"]');
            if(isset($v5) && empty($v5)) {
                $v5 = $saved->xpath('//editor/modifications/modification[@key="v5.value.email"]');
            }
            if(isset($v5) && empty($v5)) {
                $v5 = $saved->xpath('//editor/modifications/modification[@key="v5.value.text"]');
            }
            $v6 = $saved->xpath('//editor/modifications/modification[@key="v6.text"]');
            if(isset($v6) && empty($v6)) {
                $v6 = $saved->xpath('//editor/modifications/modification[@key="v6.value.email"]');
            }
            if(isset($v6) && empty($v6)) {
                $v6 = $saved->xpath('//editor/modifications/modification[@key="v6.value.text"]');
            }
            $v7 = $saved->xpath('//editor/modifications/modification[@key="v7.text"]');
            if(isset($v7) && empty($v7)) {
                $v7 = $saved->xpath('//editor/modifications/modification[@key="v7.value.email"]');
            }
            if(isset($v7) && empty($v7)) {
                $v7 = $saved->xpath('//editor/modifications/modification[@key="v7.value.text"]');
            }
            $v8 = $saved->xpath('//editor/modifications/modification[@key="v8.text"]');
            if(isset($v8) && empty($v8)) {
                $v8 = $saved->xpath('//editor/modifications/modification[@key="v8.value.email"]');
            }
            if(isset($v8) && empty($v8)) {
                $v8 = $saved->xpath('//editor/modifications/modification[@key="v8.value.text"]');
            }
            $v9 = $saved->xpath('//editor/modifications/modification[@key="v9.text"]');
            if(isset($v9) && empty($v9)) {
                $v9 = $saved->xpath('//editor/modifications/modification[@key="v9.value.email"]');
            }
            if(isset($v9) && empty($v9)) {
                $v9 = $saved->xpath('//editor/modifications/modification[@key="v9.value.text"]');
            }
            $v10 = $saved->xpath('//editor/modifications/modification[@key="v10.text"]');
            if(isset($v10) && empty($v10)) {
                $v10 = $saved->xpath('//editor/modifications/modification[@key="v10.value.email"]');
            }
            if(isset($v10) && empty($v10)) {
                $v10 = $saved->xpath('//editor/modifications/modification[@key="v10.value.text"]');
            }
            $v11 = $saved->xpath('//editor/modifications/modification[@key="v11.text"]');
            if(isset($v11) && empty($v11)) {
                $v11 = $saved->xpath('//editor/modifications/modification[@key="v11.value.email"]');
            }
            if(isset($v11) && empty($v11)) {
                $v11 = $saved->xpath('//editor/modifications/modification[@key="v11.value.text"]');
            }
            $v12 = $saved->xpath('//editor/modifications/modification[@key="v12.text"]');
            if(isset($v12) && empty($v12)) {
                $v12 = $saved->xpath('//editor/modifications/modification[@key="v12.value.email"]');
            }
            if(isset($v12) && empty($v12)) {
                $v12 = $saved->xpath('//editor/modifications/modification[@key="v12.value.text"]');
            }
            $v13 = $saved->xpath('//editor/modifications/modification[@key="v13.text"]');
            if(isset($v13) && empty($v13)) {
                $v13 = $saved->xpath('//editor/modifications/modification[@key="v13.value.email"]');
            }
            if(isset($v13) && empty($v13)) {
                $v13 = $saved->xpath('//editor/modifications/modification[@key="v13.value.text"]');
            }
            $v14 = $saved->xpath('//editor/modifications/modification[@key="v14.text"]');
            if(isset($v14) && empty($v14)) {
                $v14 = $saved->xpath('//editor/modifications/modification[@key="v14.value.email"]');
            }
            if(isset($v14) && empty($v14)) {
                $v14 = $saved->xpath('//editor/modifications/modification[@key="v14.value.text"]');
            }
            $v15 = $saved->xpath('//editor/modifications/modification[@key="v15.text"]');
            if(isset($v15) && empty($v15)) {
                $v15 = $saved->xpath('//editor/modifications/modification[@key="v15.value.email"]');
            }
            if(isset($v15) && empty($v15)) {
                $v15 = $saved->xpath('//editor/modifications/modification[@key="v15.value.text"]');
            }
            $v16 = $saved->xpath('//editor/modifications/modification[@key="v16.text"]');
            if(isset($v16) && empty($v16)) {
                $v16 = $saved->xpath('//editor/modifications/modification[@key="v16.value.email"]');
            }
            if(isset($v16) && empty($v16)) {
                $v16 = $saved->xpath('//editor/modifications/modification[@key="v16.value.text"]');
            }
            $v17 = $saved->xpath('//editor/modifications/modification[@key="v17.text"]');
            if(isset($v17) && empty($v17)) {
                $v17 = $saved->xpath('//editor/modifications/modification[@key="v17.value.email"]');
            }
            if(isset($v17) && empty($v17)) {
                $v17 = $saved->xpath('//editor/modifications/modification[@key="v17.value.text"]');
            }
            $v18 = $saved->xpath('//editor/modifications/modification[@key="v18.text"]');
            if(isset($v18) && empty($v18)) {
                $v18 = $saved->xpath('//editor/modifications/modification[@key="v18.value.email"]');
            }
            if(isset($v18) && empty($v18)) {
                $v18 = $saved->xpath('//editor/modifications/modification[@key="v18.value.text"]');
            }
            $v19 = $saved->xpath('//editor/modifications/modification[@key="v19.text"]');
            if(isset($v19) && empty($v19)) {
                $v19 = $saved->xpath('//editor/modifications/modification[@key="v19.value.email"]');
            }
            if(isset($v19) && empty($v19)) {
                $v19 = $saved->xpath('//editor/modifications/modification[@key="v19.value.text"]');
            }
            $v20 = $saved->xpath('//editor/modifications/modification[@key="v20.text"]');
            if(isset($v20) && empty($v20)) {
                $v20 = $saved->xpath('//editor/modifications/modification[@key="v20.value.email"]');
            }
            if(isset($v20) && empty($v20)) {
                $v20 = $saved->xpath('//editor/modifications/modification[@key="v20.value.text"]');
            }
            $v21 = $saved->xpath('//editor/modifications/modification[@key="v21.text"]');
            if(isset($v21) && empty($v21)) {
                $v21 = $saved->xpath('//editor/modifications/modification[@key="v21.value.email"]');
            }
            if(isset($v21) && empty($v21)) {
                $v21 = $saved->xpath('//editor/modifications/modification[@key="v21.value.text"]');
            }
            $v22 = $saved->xpath('//editor/modifications/modification[@key="v22.text"]');
            if(isset($v22) && empty($v22)) {
                $v22 = $saved->xpath('//editor/modifications/modification[@key="v22.value.email"]');
            }
            if(isset($v22) && empty($v22)) {
                $v22 = $saved->xpath('//editor/modifications/modification[@key="v22.value.text"]');
            }
            $v23 = $saved->xpath('//editor/modifications/modification[@key="v23.text"]');
            if(isset($v23) && empty($v23)) {
                $v23 = $saved->xpath('//editor/modifications/modification[@key="v23.value.email"]');
            }
            if(isset($v23) && empty($v23)) {
                $v23 = $saved->xpath('//editor/modifications/modification[@key="v23.value.text"]');
            }
            $v24 = $saved->xpath('//editor/modifications/modification[@key="v24.text"]');
            if(isset($v24) && empty($v24)) {
                $v24 = $saved->xpath('//editor/modifications/modification[@key="v24.value.email"]');
            }
            if(isset($v24) && empty($v24)) {
                $v24 = $saved->xpath('//editor/modifications/modification[@key="v24.value.text"]');
            }

            $contact->loadData();
            if(isset($v1) && !empty($v1)) {
                $contact->setCustom1((string)$v1[0]['value']);
            }
            if(isset($v2) && !empty($v2)) {
                $contact->setCustom2((string)$v2[0]['value']);
            }
            if(isset($v3) && !empty($v3)) {
                $contact->setCustom3((string)$v3[0]['value']);
            }
            if(isset($v4) && !empty($v4)) {
                $contact->setCustom4((string)$v4[0]['value']);
            }
            if(isset($v5) && !empty($v5)) {
                $contact->setCustom5((string)$v5[0]['value']);
            }
            if(isset($v6) && !empty($v6)) {
                $contact->setCustom6((string)$v6[0]['value']);
            }
            if(isset($v7) && !empty($v7)) {
                $contact->setCustom7((string)$v7[0]['value']);
            }
            if(isset($v8) && !empty($v8)) {
                $contact->setCustom8((string)$v8[0]['value']);
            }
            if(isset($v9) && !empty($v9)) {
                $contact->setCustom9((string)$v9[0]['value']);
            }
            if(isset($v10) && !empty($v10)) {
                $contact->setCustom10((string)$v10[0]['value']);
            }
            if(isset($v11) && !empty($v11)) {
                $contact->setCustom11((string)$v11[0]['value']);
            }
            if(isset($v12) && !empty($v12)) {
                $contact->setCustom12((string)$v12[0]['value']);
            }
            if(isset($v13) && !empty($v13)) {
                $contact->setCustom13((string)$v13[0]['value']);
            }
            if(isset($v14) && !empty($v14)) {
                $contact->setCustom14((string)$v14[0]['value']);
            }
            if(isset($v15) && !empty($v15)) {
                $contact->setCustom15((string)$v15[0]['value']);
            }
            if(isset($v16) && !empty($v16)) {
                $contact->setCustom16((string)$v16[0]['value']);
            }
            if(isset($v17) && !empty($v17)) {
                $contact->setCustom17((string)$v17[0]['value']);
            }
            if(isset($v18) && !empty($v18)) {
                $contact->setCustom18((string)$v18[0]['value']);
            }
            if(isset($v19) && !empty($v19)) {
                $contact->setCustom19((string)$v19[0]['value']);
            }
            if(isset($v20) && !empty($v20)) {
                $contact->setCustom20((string)$v20[0]['value']);
            }
            if(isset($v21) && !empty($v21)) {
                $contact->setCustom21((string)$v21[0]['value']);
            }
            if(isset($v22) && !empty($v22)) {
                $contact->setCustom22((string)$v22[0]['value']);
            }
            if(isset($v23) && !empty($v23)) {
                $contact->setCustom23((string)$v23[0]['value']);
            }
            if(isset($v24) && !empty($v24)) {
                $contact->setCustom24((string)$v24[0]['value']);
            }
            $contact->saveMongo();

            $fields = array(

                'phonedurchwahl' => 'self_phone_durchwahl',
                'phonevorwahl' => 'self_phone_vorwahl',
                'phonelv' => 'self_phone_lv',
                'phone' => 'self_phone',
                'faxdurchwahl' => 'self_fax_durchwahl',
                'faxvorwahl' => 'self_fax_vorwahl',
                'faxlv' => 'self_fax_lv',
                'fax' => 'self_fax_phone',
                'country' => 'self_country',
                'internet' => 'self_web',
                'mobilevorwahl' => 'self_mobile_vorwahl',
                'mobilelv' => 'self_mobile_lv',
                'mobile' => 'self_mobile_durchwahl',
                'mobil' => 'self_phone_mobile',
                'tel' => 'self_phone',
                'firmenbezeichnung' => 'self_department',
                'pos' => 'self_department',
                'pos2' => 'self_department2',
                'beruf2' => 'self_abteilung',
                'city' => 'self_city',
                'plz' => 'self_zip',
                'zip' => 'self_zip',
                'street' => 'self_street',
                'housenumber' => 'self_house_number',
                'email' => 'self_email',
                'tl' => 'self_title',
                'funkt' => 'self_function',
                'funct' => 'self_function',
                'position' => 'self_position',
                'fachg' => 'self_abteilung',
                'firstname' => 'self_firstname',
                'lastname' => 'self_lastname',


                'vorderseite/vorname' => 'self_firstname',
                'vorderseite/nachname' => 'self_lastname',
                'vorderseite/beruf' => 'self_function',
                'vorderseite/phonedurchwahl' => 'self_phone_durchwahl',
                'vorderseite/phonevorwahl' => 'self_phone_vorwahl',
                'vorderseite/phonelv' => 'self_phone_lv',
                'vorderseite/phone' => 'self_phone',
                'vorderseite/email' => 'self_email',

                'adressez1/firmenbezeichnung' => 'self_department',
                'adressez2/street' => 'self_street',
                'adressez2/housenumber' => 'self_house_number',
                'adressez2/zip' => 'self_zip',
                'adressez2/city' => 'self_city',
                'adressez2/firmenbezeichnung' => 'self_department',
                'adressez2/email' => 'self_email',

                'a1/phonedurchwahl' => 'self_phone_durchwahl',
                'a1/phonevorwahl' => 'self_phone_vorwahl',
                'a1/phonelv' => 'self_phone_lv',
                'a1/phone' => 'self_phone',
                'a1/faxdurchwahl' => 'self_fax_durchwahl',
                'a1/faxvorwahl' => 'self_fax_vorwahl',
                'a1/faxlv' => 'self_fax_lv',
                'a1/fax' => 'self_fax_phone',
                'a1/city' => 'self_city',
                'a1/street' => 'self_street',
                'a1/housenumber' => 'self_house_number',
                'a1/zip' => 'self_zip',
                'a1/country_1' => 'self_country',


                'a1/email' => 'self_email',
                'a1/internetadresse' => 'self_web',
                'a1/firmenbezeichnung' => 'self_department',


                'adressez3/phonedurchwahl' => 'self_phone_durchwahl',
                'adressez3/phonevorwahl' => 'self_phone_vorwahl',
                'adressez3/phonelv' => 'self_phone_lv',
                'adressez3/phone' => 'self_phone',
                'adressez3/faxdurchwahl' => 'self_fax_durchwahl',
                'adressez3/faxvorwahl' => 'self_fax_vorwahl',
                'adressez3/faxlv' => 'self_fax_lv',
                'adressez3/fax' => 'self_fax_phone',
                'adressez3/street' => 'self_street',
                'adressez3/housenumber' => 'self_house_number',
                'adressez3/zip' => 'self_zip',
                'adressez3/country' => 'self_country',
                'adressez3/email' => 'self_email',


                'adressez4/phonedurchwahl' => 'self_phone_durchwahl',
                'adressez4/phonevorwahl' => 'self_phone_vorwahl',
                'adressez4/phonelv' => 'self_phone_lv',
                'adressez4/phone' => 'self_phone',
                'adressez4/faxdurchwahl' => 'self_fax_durchwahl',
                'adressez4/faxvorwahl' => 'self_fax_vorwahl',
                'adressez4/faxlv' => 'self_fax_lv',
                'adressez4/fax' => 'self_fax_phone',
                'adressez4/internetadresse' => 'self_web',
                'adressez4/email' => 'self_email',

                'adressez5/internetadresse' => 'self_web',


                'vorderseitenameposition/vorname' => 'self_firstname',
                'vorderseitenameposition/nachname' => 'self_lastname',
                'vorderseitenameposition/beruf' => 'self_function',
                'adresse/street' => 'self_street',
                'adresse/housenumber' => 'self_house_number',
                'adresse/zip' => 'self_zip',
                'adresse/city' => 'self_city',
                'adresse/email' => 'self_email',

                'vorderseitetel/phonedurchwahl' => 'self_phone_durchwahl',
                'vorderseitetel/phonevorwahl' => 'self_phone_vorwahl',
                'vorderseitetel/phonelv' => 'self_phone_lv',
                'vorderseitetel/phone' => 'self_phone',
                'vorderseitetel/email' => 'self_email',

                'vorderseitefax/faxdurchwahl' => 'self_fax_durchwahl',
                'vorderseitefax/faxvorwahl' => 'self_fax_vorwahl',
                'vorderseitefax/faxlv' => 'self_fax_lv',
                'vorderseitefax/fax' => 'self_fax_phone',
                'vorderseitefax/email' => 'self_email',

            );

            foreach($fields as $key => $value) {
                $wert = $saved->xpath('//editor/modifications/modification[@key="'.str_replace("/", "_", $key).'.text"]');
                if(isset($wert) && !empty($wert)) {
                    $contact[$value] = (string)$wert[0]['value'];
                }
                $wert = $saved->xpath('//editor/modifications/modification[@key="'.str_replace("/", "_", $key).'.value.text"]');
                if(isset($wert) && !empty($wert)) {
                    $contact[$value] = (string)$wert[0]['value'];
                }
            }

            try{
                $infos = json_decode(str_replace(PHP_EOL, '', $user->self_information), true);

                $fields = array(
                    'department_2' => 'department_2',
                    'street_2' => 'street_2',
                    'street2' => 'street_2',
                    'house_number_2' => 'house_number_2',
                    'country_2' => 'country_2',
                    'country2' => 'country_2',
                    'land_2' => 'country_2',
                    'lkz_1' => 'lkz_1',
                    'zip_2' => 'zip_2',
                    'city_2' => 'city_2',
                    'email_2' => 'email_2',
                    'phone_lv_2' => 'phone_lv_2',
                    'phone_vorwahl_2' => 'phone_vorwahl_2',
                    'phone_2' => 'phone_2',
                    'phone_durchwahl_2' => 'phone_durchwahl_2',
                    'mobile_lv_2' => 'mobile_lv_2',
                    'mobile_vorwahl_2' => 'mobile_vorwahl_2',
                    'mobile_2' => 'mobile_2',
                    'fax_lv_2' => 'fax_lv_2',
                    'fax_vorwahl_2' => 'fax_vorwahl_2',
                    'fax_2' => 'fax_2',
                    'fax_durchwahl_2' => 'fax_durchwahl_2',
                    'department_3' => 'department_3',
                    'street_3' => 'street_3',
                    'house_number_3' => 'house_number_3',
                    'country_3' => 'country_3',
                    'zip_3' => 'zip_3',
                    'city_4' => 'city_4',
                    'email_3' => 'email_3',
                    'phone_lv_3' => 'phone_lv_3',
                    'phone_vorwahl_3' => 'phone_vorwahl_3',
                    'phone_3' => 'phone_3',
                    'phone_durchwahl_3' => 'phone_durchwahl_3',
                    'mobile_lv_3' => 'mobile_lv_3',
                    'mobile_vorwahl_3' => 'mobile_vorwahl_3',
                    'mobile_3' => 'mobile_3',
                    'fax_lv_3' => 'fax_lv_3',
                    'fax_vorwahl_3' => 'fax_vorwahl_3',
                    'fax_3' => 'fax_3',
                    'fax_durchwahl_3' => 'fax_durchwahl_3',
                    'internet_2' => 'internet_2',
                    'internet_3' => 'internet_3',
                    'lkz_1' => 'lkz_1',
                    'lkz_2' => 'lkz_2',
                    'lkz_3' => 'lkz_3',
                    'custom1' => 'custom1',
                    'custom2' => 'custom2',
                    'custom3' => 'custom3',
                    'country_1' => 'country_1',
                    'country_2' => 'country_2',
                    'country_3' => 'country_3',


                    'rueckseitea1z1/department' => 'department_2',
                    'rueckseitea1z2/street' => 'street_2',
                    'rueckseitea1z2/house_number' => 'house_number_2',
                    'rueckseitea1z2/country' => 'country_2',
                    'rueckseitea1z2/land' => 'country_2',
                    'rueckseitea1z2/zip' => 'zip_2',
                    'rueckseitea1z2/city' => 'city_2',
                    'rueckseitea1z5/email' => 'email_2',
                    'rueckseitea1z3/phone_lv' => 'phone_lv_2',
                    'rueckseitea1z3/phone_vorwahl' => 'phone_vorwahl_2',
                    'rueckseitea1z3/phone' => 'phone_2',
                    'rueckseitea1z3/phone_durchwahl' => 'phone_durchwahl_2',
                    'rueckseitea1z4/mobile_lv' => 'mobile_lv_2',
                    'rueckseitea1z4/mobile_vorwahl' => 'mobile_vorwahl_2',
                    'rueckseitea1z4/mobile' => 'mobile_2',
                    'rueckseitea1z3/fax_lv' => 'fax_lv_2',
                    'rueckseitea1z3/fax_vorwahl' => 'fax_vorwahl_2',
                    'rueckseitea1z3/fax' => 'fax_2',
                    'rueckseitea1z3/fax_durchwahl' => 'fax_durchwahl_2',
                    'rueckseitea1z5/internet' => 'internet_2',

                    'rueckseitea1z1/department' => 'self_department',
                    'rueckseitea1z2/street' => 'self_street',
                    'rueckseitea1z2/house_number' => 'self_house_number',
                    'rueckseitea1z2/country' => 'self_country',
                    'rueckseitea1z2/land' => 'self_country',
                    'rueckseitea1z2/zip' => 'self_zip',
                    'rueckseitea1z2/city' => 'self_city',
                    'rueckseitea1z5/email' => 'self_email',
                    'rueckseitea1z3/phone_lv' => 'self_phone_lv',
                    'rueckseitea1z3/phone_vorwahl' => 'self_phone_vorwahl',
                    'rueckseitea1z3/phone' => 'self_phone',
                    'rueckseitea1z3/phone_durchwahl' => 'self_phone_durchwahl',
                    'rueckseitea1z4/mobile_lv' => 'self_mobile_lv',
                    'rueckseitea1z4/mobile_vorwahl' => 'self_mobile_vorwahl',
                    'rueckseitea1z4/mobile' => 'self_mobile',
                    'rueckseitea1z3/fax_lv' => 'self_fax_lv',
                    'rueckseitea1z3/fax_vorwahl' => 'self_fax_vorwahl',
                    'rueckseitea1z3/fax' => 'self_fax_phone',
                    'rueckseitea1z3/fax_durchwahl' => 'self_fax_durchwahl',
                    'rueckseitea1z5/internet' => 'self_web',

                    'rueckseitea1z1/department_2' => 'department_2',
                    'rueckseitea1z2/street_2' => 'street_2',
                    'rueckseitea1z2/house_number_2' => 'house_number_2',
                    'rueckseitea1z2/country_2' => 'country_2',
                    'rueckseitea1z2/land_2' => 'country_2',
                    'rueckseitea1z2/zip_2' => 'zip_2',
                    'rueckseitea1z2/city_2' => 'city_2',
                    'rueckseitea1z5/email_2' => 'email_2',
                    'rueckseitea1z3/phone_lv_2' => 'phone_lv_2',
                    'rueckseitea1z3/phone_vorwahl_2' => 'phone_vorwahl_2',
                    'rueckseitea1z3/phone_2' => 'phone_2',
                    'rueckseitea1z3/phone_durchwahl_2' => 'phone_durchwahl_2',
                    'rueckseitea1z4/mobile_lv_2' => 'mobile_lv_2',
                    'rueckseitea1z4/mobile_vorwahl_2' => 'mobile_vorwahl_2',
                    'rueckseitea1z4/mobile_2' => 'mobile_2',
                    'rueckseitea1z3/fax_lv_2' => 'fax_lv_2',
                    'rueckseitea1z3/fax_vorwahl_2' => 'fax_vorwahl_2',
                    'rueckseitea1z3/fax_2' => 'fax_2',
                    'rueckseitea1z3/fax_durchwahl_2' => 'fax_durchwahl_2',
                    'rueckseitea1z5/internet_2' => 'internet_2',

                    'a2/department' => 'department_2',
                    'a2/street' => 'street_2',
                    'a2/house_number' => 'house_number_2',
                    'a2/country' => 'country_2',
                    'a2/zip' => 'zip_2',
                    'a2/city' => 'city_2',
                    'a2/email' => 'email_2',
                    'a2/phone_lv' => 'phone_lv_2',
                    'a2/phone_vorwahl' => 'phone_vorwahl_2',
                    'a2/phone' => 'phone_2',
                    'a2/phone_durchwahl' => 'phone_durchwahl_2',
                    'a2/lkz_1' => 'lkz_1',
                    'a2/lkz_2' => 'lkz_2',
                    'a2/lkz_3' => 'lkz_3',
                    'a2/country_1' => 'country_1',
                    'a2/country_2' => 'country_2',
                    'a2/country_3' => 'country_3',

                    'a2/mobile_lv' => 'mobile_lv_2',
                    'a2/mobile_vorwahl' => 'mobile_vorwahl_2',
                    'a2/mobile' => 'mobile_2',
                    'a2/fax_lv' => 'fax_lv_2',
                    'a2/fax_vorwahl' => 'fax_vorwahl_2',
                    'a2/fax' => 'fax_2',
                    'a2/fax_durchwahl' => 'fax_durchwahl_2',
                    'a2/internetadresse' => 'internet_2',

                    'rueckseitea2z2/department_3' => 'department_3',
                    'rueckseitea2z2/street_3' => 'street_3',
                    'rueckseitea2z2/house_number_3' => 'house_number_3',
                    'rueckseitea2z2/country_3' => 'country_3',
                    'rueckseitea2z2/zip_3' => 'zip_3',
                    'rueckseitea2z2/city_3' => 'city_3',
                    'rueckseitea2z2/city_4' => 'city_4',
                    'rueckseitea2z3/email_3' => 'email_3',
                    'rueckseitea2z3/phone_lv_3' => 'phone_lv_3',
                    'rueckseitea2z3/phone_vorwahl_3' => 'phone_vorwahl_3',
                    'rueckseitea2z3/phone_3' => 'phone_3',
                    'rueckseitea2z3/phone_durchwahl_3' => 'phone_durchwahl_3',

                    'rueckseitea2z4/mobile_lv_3' => 'mobile_lv_3',
                    'rueckseitea2z4/mobile_vorwahl_3' => 'mobile_vorwahl_3',
                    'rueckseitea2z4/mobile_3' => 'mobile_3',
                    'rueckseitea2z3/fax_lv_3' => 'fax_lv_3',
                    'rueckseitea2z3/fax_vorwahl_3' => 'fax_vorwahl_3',
                    'rueckseitea2z3/fax_3' => 'fax_3',
                    'rueckseitea2z3/fax_durchwahl_3' => 'fax_durchwahl_3',

                    'a3/department' => 'department_3',
                    'a3/street' => 'street_3',
                    'a3/house_number' => 'house_number_3',
                    'a3/country' => 'country_3',
                    'a3/zip' => 'zip_3',
                    'a3/city' => 'city_3',
                    'a3/city' => 'city_4',
                    'a3/email' => 'email_3',
                    'a3/phone_lv' => 'phone_lv_3',
                    'a3/phone_vorwahl' => 'phone_vorwahl_3',
                    'a3/phone' => 'phone_3',
                    'a3/phone_durchwahl' => 'phone_durchwahl_3',
                    'a3/lkz_1' => 'lkz_1',
                    'a3/lkz_2' => 'lkz_2',
                    'a3/lkz_3' => 'lkz_3',
                    'a3/country_1' => 'country_1',
                    'a3/country_2' => 'country_2',
                    'a3/country_3' => 'country_3',

                    'a3/mobile_lv' => 'mobile_lv_3',
                    'a3/mobile_vorwahl' => 'mobile_vorwahl_3',
                    'a3/mobile' => 'mobile_3',
                    'a3/fax_lv' => 'fax_lv_3',
                    'a3/fax_vorwahl' => 'fax_vorwahl_3',
                    'a3/fax' => 'fax_3',
                    'a3/fax_durchwahl' => 'fax_durchwahl_3',
                    'a3/internetadresse' => 'internet_3'
                );

                if(file_exists(APPLICATION_PATH . '/design/clients/' . $this->view->shop->uid . '/templateDef.php')) {
                    include_once(APPLICATION_PATH . '/design/clients/' . $this->view->shop->uid . '/templateDef.php');
                }

                foreach($fields as $key => $value) {

                    if(is_array($value)) {
                        if(isset($value['templateKey'])) {
                            $wert = $saved->xpath('//editor/modifications/modification[@key="' . str_replace("/", "_", str_replace("_", "", $value['templateKey'])) . '.'.$value['modifier'].'"]');{}
                        }else{
                            $wert = $saved->xpath('//editor/modifications/modification[@key="' . str_replace("/", "_", str_replace("_", "", $key)) . '.'.$value['modifier'].'"]');
                        }

                        if (isset($wert) && !empty($wert)) {
                            $infos[$value['column']] = (string)$wert[0]['value'];
                            if (isset($contact[$value['column']])) {
                                if(isset($value['mode']) && $value['mode'] == 'email') {
                                    $vars = explode("@", $contact[$value['column']]);
                                    $contact[$value['column']] = (string)$wert[0]['value'] . '@' . $vars[1];
                                }else {
                                    $contact[$value['column']] = (string)$wert[0]['value'];
                                    if (isset($value['append'])) {
                                        $contact[$value['column']] = $contact[$value['column']] . $value['append'];
                                    }
                                }
                            }
                        }

                    }else {
                        $wert = $saved->xpath('//editor/modifications/modification[@key="' . str_replace("/", "_", str_replace("_", "", $key)) . '.text"]');

                        if (isset($wert) && !empty($wert)) {
                            $infos[$value] = (string)$wert[0]['value'];
                            if (isset($contact[$value])) {
                                $contact[$value] = (string)$wert[0]['value'];
                            }
                        }
                        $wert = $saved->xpath('//editor/modifications/modification[@key="' . str_replace("/", "_", str_replace("_", "", $key)) . '.value.text"]');

                        if (isset($wert) && !empty($wert)) {
                            $infos[$value] = (string)$wert[0]['value'];
                            if (isset($contact[$value])) {
                                $contact[$value] = (string)$wert[0]['value'];
                            }
                        }
                    }

                }

                $pha2 = $saved->xpath('//editor/modifications/modification[@key="tel2.value.phonearea"]');
                $phn2 = $saved->xpath('//editor/modifications/modification[@key="tel2.value.phonenumber"]');
                $phe2 = $saved->xpath('//editor/modifications/modification[@key="tel2.value.phoneextension"]');

                if(isset($pha2) && !empty($pha2)) {
                    $infos['phone_vorwahl_2'] = (string)$pha2[0]['value'];
                }
                if(isset($phn2) && !empty($phn2)) {
                    $infos['phone_2'] = (string)$phn2[0]['value'];
                }
                if(isset($phe2) && !empty($phe2)) {
                    $infos['phone_durchwahl_2'] = (string)$phe2[0]['value'];
                }

                $contact->self_information = json_encode($infos);
            }catch(Exception $e) {
                Zend_Registry::get('log')->debug($e->getMessage());
            }

            $contact->save();
            if(!$this->shop->isTpSaveUserData()) {
                return;
            }
        }

        $name1 = $saved->xpath('//editor/modifications/modification[@key="name.text"]');
        $name2 = $saved->xpath('//editor/modifications/modification[@key="nameblock_name.text"]');
        $self_firstname = $saved->xpath('//editor/modifications/modification[@key="self_firstname.text"]');
        $self_lastname = $saved->xpath('//editor/modifications/modification[@key="self_lastname.text"]');

        $name3 = $saved->xpath('//editor/modifications/modification[@key="vorname.text"]');
        $name4 = $saved->xpath('//editor/modifications/modification[@key="nachname.text"]');

        $name5 = $saved->xpath('//editor/modifications/modification[@key="vornamename.text"]');

        if ($values['COPY'] == 3 && $this->user && ($article->contact_id == $this->user->id || $this->highRole->level >= 40)) {
            $path = str_split($article->id);

            if (!file_exists(APPLICATION_PATH . '/../market/templateprint/orginal/' . implode('/', $path) . '')) {
                $an = '';
                foreach ($path as $pt) {
                    $an .= $pt . '/';
                    if (!file_exists(APPLICATION_PATH . '/../market/templateprint/orginal/' . $an)) {
                        mkdir(APPLICATION_PATH . '/../market/templateprint/orginal/' . $an);
                    }
                    if (!file_exists(APPLICATION_PATH . '/../market/templateprint/orginal/' . $an)) {
                        mkdir(APPLICATION_PATH . '/../market/templateprint/orginal/' . $an);
                    }
                }
            }

            $adapter = new Zend_File_Transfer_Adapter_Http();

            $adapter->setDestination(APPLICATION_PATH . '/../market/templateprint/orginal/' . implode('/', $path));

            if (!$adapter->receive()) {
                $messages = $adapter->getMessages();
            }

            $article->render_new_preview_image = false;
            $article->save();
            unlink(APPLICATION_PATH . '/../market/templateprint/orginal/' . implode('/', $path) . '/preview.png');
            Zend_Registry::get('log')->debug(APPLICATION_PATH . '/../market/templateprint/orginal/' . implode('/', $path));

            session_commit();
        } else {
            $layouterid = $values['LAYOUTERID'];
            $basket = new TP_Basket ();
            $sessart = $basket->getTempProduct();
            Zend_Registry::get('log')->debug("here");
            $sessart->setRef($contact->self_firstname . ' ' . $contact->self_lastname);
            $sessart->setTemplatePrintContactId($contact->uuid);
            if (!empty($name1)) {
                $sessart->setRef((string)$name1[0]['value']);
            }
            if (!empty($name2)) {
                $sessart->setRef((string)$name2[0]['value']);
            }
            if (!empty($name3)) {
                $sessart->setRef((string)$name3[0]['value']);
            }
            if (!empty($name4)) {
                $sessart->setRef((string)$name4[0]['value']);
            }
            if (!empty($name5)) {
                $sessart->setRef((string)$name5[0]['value']);
            }
            if (!empty($name3) && !empty($name4)) {
                $sessart->setRef((string)$name3[0]['value'] . ' ' . (string)$name4[0]['value']);
            }
            if (!empty($self_firstname)) {
                $sessart->setRef((string)$self_firstname[0]['value']);
            }
            if (!empty($self_lastname)) {
                $sessart->setRef($sessart->getRef() . ' ' . (string)$self_lastname[0]['value']);
            }

            $adapter = new Zend_File_Transfer_Adapter_Http();


            if ($this->user) {
                if (!file_exists(APPLICATION_PATH . '/../market/templateprint/user/' . $sessart->getTemplatePrintId())) {
                    mkdir(APPLICATION_PATH . '/../market/templateprint/user/' . $sessart->getTemplatePrintId(), 0777, true);
                }
                $adapter->setDestination(APPLICATION_PATH . '/../market/templateprint/user/' . $sessart->getTemplatePrintId());
            } else {
                if (!file_exists(APPLICATION_PATH . '/../market/templateprint/guest/' . $sessart->getTemplatePrintId())) {
                    mkdir(APPLICATION_PATH . '/../market/templateprint/guest/' . $sessart->getTemplatePrintId(), 0777, true);
                }
                $adapter->setDestination(APPLICATION_PATH . '/../market/templateprint/guest/' . $sessart->getTemplatePrintId());
            }

            if (!$adapter->receive()) {
                $messages = $adapter->getMessages();
            }

            Zend_Registry::get('log')->debug(APPLICATION_PATH . '/../market/templateprint/user/' . $sessart->getTemplatePrintId());
            $articleSession = new TP_Layoutersession();
            $articleSess = $articleSession->getLayouterArticle($uuid, $layouterid);
            $articleSess->setOrgArticleId($uuid);
            $articleSess->setTemplatePrintId($sessart->getTemplatePrintId());
            $articleSess->setRef($sessart->getRef());
            $articleSess->setKst($sessart->getKst());
            $articleSess->setTemplatePrintContactId($sessart->getTemplatePrintContactId());
            $articleSess->saveMongo();

            $article = Doctrine_Query::create()
                ->from('Article c')
                ->where('c.uuid = ?', array($articleSess->getOrgArticleId()))
                ->fetchOne();

            $articleSess->setTitle($article->title);

            $articleSess->setIsEdit();

            session_commit();
            sleep(5);
        }

    }

    public function redirectAction() {
        $values = TP_Crypt::decrypt($this->_getParam('ARTID'));
        Zend_Registry::get('log')->debug(print_r($values, true));
        if(!isset($values['load'])) {
            $values['load'] = 0;
        }
        if (isset($_SERVER['HTTP_REFERER']) && strpos($_SERVER['HTTP_REFERER'], 'leave')) {
            header('location: ' . $values['SERVER'] . '/service/template/redirectfinal?load='.$values['load'].'&LEAVE=1&ARTID=' . $this->_getParam('ARTID'));
        } else {
            header('location: ' . $values['SERVER'] . '/service/template/redirectfinal?load='.$values['load'].'&ARTID=' . $this->_getParam('ARTID'));
        }
        die();
    }

    public function redirectfinalAction() {

        $values = TP_Crypt::decrypt($this->_getParam('ARTID'));

        $load = "0";
        if($this->_getParam('load', false)) {
            $load = $this->_getParam('load', false);
        }

        if ($this->_getParam('LEAVE') && (isset($values['fullscreen']) && $values['fullscreen'] == 1)) {

            if($values['COPY'] == 1) {
                return $this->_redirect("/article/show/uuid/" . $values['UUID'] . "/" . $values['LAYOUTERID']);
            }elseif($values['COPY'] == 2) {
                return $this->_redirect("/article/show/uuid/" . $values['UUID']);
            }

        }elseif($this->_getParam('LEAVE')) {
            $content = "
                <html>
                <body>
                <script>
                    window.parent.templatePrintClose();
                </script>
                </body>
                </html>
            ";

            echo $content;
            return;
        }

        $article = Doctrine_Query::create()
            ->from('Article c')
            ->where('c.uuid = ?', array($values['UUID']))
            ->fetchOne();
        $uuid = $values['UUID'];

        if ($values['COPY'] == 3 && $this->user && ($article->contact_id == $this->user->id || $this->highRole->level >= 40)) {

            if($values['fullscreen'] == 1) {

                $content = '
                    <html>
                    <body>
                    <script>
                        
                        function templatePrintRedirect(uuid, layouterid, load) {
                            if(load && load == 3) {
                                backwin();
                                loadContacts();
                                return;
                            }
                        
                            if(load) {
                                var load ="?load=1"
                            }else{
                                var load = "";
                            }
                        
                            if(layouterid) {
                                document.location = "/article/show/uuid/" + uuid + \'/\' + layouterid + load;
                            }else{
                                document.location = "/article/show/uuid/" + uuid + load;
                            }
                            
                        }
                        
                        templatePrintRedirect("'.$uuid.'", false, ' . $load . ');
                    </script>
                    </body>
                    </html>
                ';

            }else {

                $content = "
                    <html>
                    <body>
                    <script>
                        window.parent.templatePrintRedirect('$uuid', false, " . $load . ");
                    </script>
                    </body>
                    </html>
                ";
            }

        } else {

            if($values['fullscreen'] == 1) {
                $layouterid = $values['LAYOUTERID'];
                $content = '
                    <html>
                    <body>
                    <script>
                        
                        function templatePrintRedirect(uuid, layouterid, load) {
                            if(load && load == 3) {
                                backwin();
                                loadContacts();
                                return;
                            }
                        
                            if(load) {
                                var load ="?load=1"
                            }else{
                                var load = "";
                            }
                        
                            if(layouterid) {
                                document.location = "/article/show/uuid/" + uuid + \'/\' + layouterid + load;
                            }else{
                                document.location = "/article/show/uuid/" + uuid + load;
                            }
                            
                        }
                        
                         window.parent.templatePrintRedirect("'.$uuid.'", "'.$layouterid.'", ' . $load . ');
                    </script>
                    </body>
                    </html>
                ';

            }else {

                $layouterid = $values['LAYOUTERID'];
                $content = "
                    <html>
                    <body>
                    <script>
                        window.parent.templatePrintRedirect('$uuid', '$layouterid', " . $load . ");
                    </script>
                    ...
                    </body>
                    </html>
                ";
            }

        }

        echo $content;

    }
}

