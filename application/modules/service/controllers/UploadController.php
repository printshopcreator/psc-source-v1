<?php

class Service_UploadController extends TP_Controller_Action
{

    protected $_extAllow = 'ppt,inx,idml,indd,qxd,ps,cdr,cdx,jpg,gif,pdf,xls,png,doc,jpeg,tif,tiff,ai,eps,psd,phtml,zip,jrxml,ttf,otf,csv,docx,xlsx,pptx';



    public function statuschangeposAction() {

        if($this->_getParam("pos", false) && $this->_request->getParam('key') == '37fhewi7438fhaiug4fa') {
            $row = Doctrine_Query::create()
                ->from('Orders m')
                ->where('id = ?', array(intval($this->_request->getParam('id'))))
                ->limit(intval($this->_request->getParam('limit')))
                ->offset(intval($this->_request->getParam('start')))
                ->fetchOne();

            // Redirect wenn der User nicht mit der Bestellung verknüpft ist
            if ($row->contact_id != $this->user->id  && (!$this->_request->getParam('key', false) || $this->_request->getParam('key') != '37fhewi7438fhaiug4fa')) {
                $this->_forward('noaccess', 'error');
            }

            $pos = Doctrine_Query::create()
                ->from('Orderspos m')
                ->where('id = ?', array(intval($this->_request->getParam('pos'))))
                ->limit(intval($this->_request->getParam('limit')))
                ->offset(intval($this->_request->getParam('start')))
                ->fetchOne();

            if($this->_getParam('status', false)) {
                $pos->status = $this->_getParam('status', false);
                $pos->save();

                if($pos->status == 100 || $pos->status == 110) {
                    $upload = Doctrine_Query::create()
                        ->from('Upload m')
                        ->where('m.orderspos_id = ?', array($pos->id))
                        ->orderBy('m.created DESC')
                        ->fetchOne();

                    $upload->export = true;
                    $upload->save();
                }

                TP_Queue::process('orderposuploadcomplete', 'global', $pos);
                TP_Queue::process('orderposstatuschange', 'global', $pos);

            }else{
                if ($pos->status < 50) {
                    $pos->status = 50;
                    $pos->save();

                    foreach ($row->Orderspos as $orderpos) {
                        $orderpos->uploadfinish = true;
                        $orderpos->save();
                    }

                    TP_Queue::process('orderposuploadcomplete', 'global', $pos);
                    TP_Queue::process('orderposstatuschange', 'global', $pos);
                }
            }

            die("success");
            return;
        }
    }

    public function deletecmsAction() {

        $this->view->sid = intval(str_replace('S', '', $this->_request->getParam('sid')));

        $this->getHelper('layout')->disableLayout();
        $this->_helper->viewRenderer->setNoRender();

        foreach ($this->user->ShopContact as $shop) {
            if ($shop->admin != true) {
                continue;
            }
            if ($this->shop->id == $shop->shop_id) {
                $accessLocal = true;
            }
            array_push($this->shopIds, $shop->shop_id);
            if (str_replace('S', '', $this->_request->getParam('sid')) == $shop->shop_id || str_replace('S', '', $this->_request->getParam('sid')) == '') {
                $accessRemote = true;
            }
        }

        $conf = Doctrine_Query::create()
            ->from('Shop s')
            ->where('s.id = ? AND s.id IN (' . implode(',', $this->shopIds) . ')')->fetchOne(array($this->view->sid));

        if ($conf == false) {
            die();
        }

        $basePath = PUBLIC_PATH . '/shops/' . $conf['uid'] . '/images/cms/';

        unlink($basePath . $this->_request->getParam('image'));

        $arguments = array('success' => true);

        die(Zend_Json::encode($arguments));
    }

    public function uploadcmsAction() {

        $this->view->sid = intval(str_replace('S', '', $this->_request->getParam('sid')));

        $this->getHelper('layout')->disableLayout();
        $this->_helper->viewRenderer->setNoRender();

        foreach ($this->user->ShopContact as $shop) {
            if ($shop->admin != true) {
                continue;
            }
            if ($this->shop->id == $shop->shop_id) {
                $accessLocal = true;
            }
            array_push($this->shopIds, $shop->shop_id);
            if (str_replace('S', '', $this->_request->getParam('sid')) == $shop->shop_id || str_replace('S', '', $this->_request->getParam('sid')) == '') {
                $accessRemote = true;
            }
        }

        $conf = Doctrine_Query::create()
            ->from('Shop s')
            ->where('s.id = ? AND s.id IN (' . implode(',', $this->shopIds) . ')')->fetchOne(array($this->view->sid));

        if ($conf == false) {
            die();
        }

        $basePath = PUBLIC_PATH . '/shops/' . $conf['uid'] . '/images/cms/';

        if (!file_exists($basePath)) {
            mkdir($basePath, 0777, true);
            Zend_Registry::get('log')->debug('CREATE CMS PATH' . $basePath);
        }

        $adapter = new Zend_File_Transfer_Adapter_Http();
        $adapter->addValidator('Extension', false, $this->_extAllow);

        $id = sha1(mt_rand() . microtime());

        $adapter->setDestination($basePath);

        $orginalFilename = $adapter->getFileName(null, false);

        $adapter->addFilter('Rename', array('target' => $basePath . $adapter->getFileName(null, false), 'overwrite' => true));

        if (!$adapter->receive() || !$adapter->isValid()) {
            $error = $adapter->getMessages();
            if (isset($error['fileExtensionFalse'])) {
                $arguments = array('error' => true, 'message' => $error['fileExtensionFalse']);
            } else {
                $arguments = array('error' => true, 'message' => 'Upload Error');
            }
            Zend_Registry::get('log')->debug('UPLOAD CMS DEBUG' . print_r($error, true));
            die(Zend_Json::encode($arguments));
        } elseif ($adapter->receive() && $adapter->isValid()) {
            $arguments = array('success' => true);

            Zend_Registry::get('log')->debug('UPLOAD CMS DEBUG' . print_r($arguments, true));
            die(Zend_Json::encode($arguments));
        }

        Zend_Registry::get('log')->debug('UPLOAD CMS DEBUG' . print_r($arguments, true));
        $arguments = array('success' => false);

        die(Zend_Json::encode($arguments));
    }

    public function backenduploadAction() {

        $this->view->sid = intval(str_replace('S', '', $this->_request->getParam('id')));

        $this->getHelper('layout')->disableLayout();
        $this->_helper->viewRenderer->setNoRender();

        $conf = Doctrine_Query::create()
            ->from('Shop s')
            ->where('s.id = ?')->fetchOne(array($this->view->sid));

        if ($conf == false) {
            die();
        }

        $basePathTemplates = Zend_Registry::get('shop_path') . '/' . $conf['layout'] . '/';
        $basePathImages = PUBLIC_PATH . '/shops/' . $conf['uid'] . '/';
        $basePathUploads = PUBLIC_PATH . '/uploads/' . $conf['uid'] . '/';
        $basePathReports = APPLICATION_PATH . '/design/clients/' . $conf['uid'] . '/reports/';
        $basePathStyles = PUBLIC_PATH . '/styles/' . $conf['uid'] . '/design/' . $conf['layout'] . '/';
        $requestedPath = $this->getRequest()->getParam('path');
        $browsePath = preg_replace('/^\/Root\/Templates\/?/', $basePathTemplates, $requestedPath) . '/';
        $browsePath = preg_replace('/^\/Root\/Styles\/?/', $basePathStyles, $browsePath) . '/';
        $browsePath = preg_replace('/^\/Root\/Shop\/?/', $basePathImages, $browsePath) . '/';
        $browsePath = preg_replace('/^\/Root\/Reports\/?/', $basePathReports, $browsePath) . '/';
        $browsePath = preg_replace('/^\/Root\/Uploads\/?/', $basePathUploads, $browsePath) . '/';
        $browsePath = preg_replace('/^\/Root\/?/', $basePathTemplates, $browsePath) . '/';

        $adapter = new Zend_File_Transfer_Adapter_Http();
        $adapter->addValidator('Extension', false, $this->_extAllow);

        $id = sha1(mt_rand() . microtime());

        $adapter->setDestination($browsePath);

        $orginalFilename = $adapter->getFileName(null, false);

        $adapter->addFilter('Rename', $browsePath . $adapter->getFileName(null, false));

        if (!$adapter->receive() || !$adapter->isValid()) {
            $error = $adapter->getMessages();
            if (isset($error['fileExtensionFalse'])) {
                $arguments = array('error' => true, 'message' => $error['fileExtensionFalse']);
            } else {
                $arguments = array('error' => true, 'message' => 'Fehler');
            }
            die(Zend_Json::encode($arguments));
        } elseif ($adapter->receive() && $adapter->isValid()) {
            $arguments = array('success' => true);

            die(Zend_Json::encode($arguments));
        }

        $arguments = array('success' => false);

        die(Zend_Json::encode($arguments));
    }

    public function uploadcenterAction() {
        Zend_Registry::get('log')->debug('UPLOAD');
        $this->getHelper('layout')->disableLayout();
        $this->_helper->viewRenderer->setNoRender();

        $row = Doctrine_Query::create()
            ->from('Orderspos m')
            ->where('id = ?', array(intval($this->_request->getParam('id'))))
            ->fetchOne();

        // Redirect wenn der User nicht mit der Bestellung verknüpft ist
        if ($row->Orders->contact_id != $this->user->id) {
            Zend_Registry::get('log')->debug($row->Orders->contact_id . '/' . $this->user->id);
            $arguments = array('error' => true, 'message' => 'Not Allowed');
            die(Zend_Json::encode($arguments));
        }

        $adapter = new Zend_File_Transfer_Adapter_Http();
        $adapter->addValidator('Extension', false, $this->_extAllow);

        $id = $row->Orders->alias . '_' . $row->id;

        if(!file_exists('uploads/' . $this->shop->uid . '/article')) {
            mkdir('uploads/' . $this->shop->uid . '/article', 0777, true);
        }
        $adapter->setDestination('uploads/' . $this->shop->uid . '/article');

        $orginalFilename = $adapter->getFileName(null, false);

        $adapter->addFilter('Rename', 'uploads/' . $this->shop->uid . '/article/' . $id . '_' . $adapter->getFileName(null, false));

        if (file_exists('uploads/' . $this->shop->uid . '/article/' . $id . '_' . $adapter->getFileName(null, false))) {

            $id .= time();
            $adapter->addFilter('Rename', 'uploads/' . $this->shop->uid . '/article/' . $id . '_' . $adapter->getFileName(null, false));
        } else {
            $adapter->addFilter('Rename', 'uploads/' . $this->shop->uid . '/article/' . $id . '_' . $adapter->getFileName(null, false));
        }

        if (!$adapter->receive() || !$adapter->isValid()) {
            $error = $adapter->getMessages();
            Zend_Registry::get('log')->debug(var_dump($adapter->getMessages(), true));
            if (isset($error['fileExtensionFalse'])) {
                $arguments = array('error' => true, 'message' => "Falsche Extension");
            } else {
                $arguments = array('error' => true, 'message' => 'Upload Error');
            }
            die(Zend_Json::encode($arguments));
        }

        //TP_Queue::process('upload_in_process', 'article', 'uploads/'.$this->shop->uid.'/article/'.$adapter->getFileName(null,false));

        $upload = new Upload();

        $upload->name = $orginalFilename;
        $upload->path = $adapter->getFileName(null, false);
        $upload->orderpos_id = intval($this->_request->getParam('id'));

        $upload->save();

        $arguments = array('error' => false, 'message' => 'Upload Finish');

        die(Zend_Json::encode($arguments));
    }

    /*
     * Upload fürs uploadcenter bootstrapversion
     */
    public function uploadcenterbootstrapAction() {
        Zend_Registry::get('log')->debug('UPLOAD');
        $this->getHelper('layout')->disableLayout();
        $this->_helper->viewRenderer->setNoRender();
        $mode = $this->_getParam('mode', 'articlelist');
        $row = Doctrine_Query::create()
            ->from('Orderspos m')
            ->where('id = ?', array(intval($this->_request->getParam('id'))))
            ->fetchOne();

        // Redirect wenn der User nicht mit der Bestellung verknüpft ist
        if ($row->Orders->contact_id != $this->user->id) {
            Zend_Registry::get('log')->debug($row->Orders->contact_id . '/' . $this->user->id);
            $arguments = array('error' => true, 'message' => 'Not Allowed');
            die(Zend_Json::encode($arguments));
        }

        $adapter = new Zend_File_Transfer_Adapter_Http();
        $adapter->addValidator('Extension', false, $this->_extAllow);

        $id = $row->Orders->alias . '_' . $row->id;

        Zend_Registry::get('log')->debug('uploads/' . $this->shop->uid . '/article');
        if(!file_exists('uploads/' . $this->shop->uid . '/article')) {
            mkdir('uploads/' . $this->shop->uid . '/article', 0777, true);
        }
        $adapter->setDestination('uploads/' . $this->shop->uid . '/article');

        $orginalFilename = $adapter->getFileName(null, false);

        $adapter->addFilter('Rename', 'uploads/' . $this->shop->uid . '/article/' . $id . '_' . $adapter->getFileName(null, false));

        if (file_exists('uploads/' . $this->shop->uid . '/article/' . $id . '_' . $adapter->getFileName(null, false))) {

            $id .= time();
            $adapter->addFilter('Rename', 'uploads/' . $this->shop->uid . '/article/' . $id . '_' . $adapter->getFileName(null, false));
        } else {
            $adapter->addFilter('Rename', 'uploads/' . $this->shop->uid . '/article/' . $id . '_' . $adapter->getFileName(null, false));
        }

        if (!$adapter->receive() || !$adapter->isValid()) {
            $error = $adapter->getMessages();

            if (isset($error['fileExtensionFalse'])) {
                $arguments = array('error' => true, 'message' => "Falsche Extension");
            } else {
                $arguments = array('error' => true, 'message' => 'Upload Error', 'fehler' => print_r($error, true));
            }
            die(Zend_Json::encode($arguments));
        }

        //TP_Queue::process('upload_in_process', 'article', 'uploads/'.$this->shop->uid.'/article/'.$adapter->getFileName(null,false));

        $upload = new Upload();

        $upload->name = $orginalFilename;
        $upload->path = $adapter->getFileName(null, false);
        $upload->orderpos_id = intval($this->_request->getParam('id'));
        $upload->typ = $this->_request->getParam('type');

        $upload->save();

        $mode = $this->_getParam('mode', 'articlelist');
        $content = array();
        require_once(APPLICATION_PATH . '/helpers/Image.php');
        $img = new TP_View_Helper_Image();

        $image = array('key' => $upload->uuid, 'value' => $upload->path);


        $content[] = array(
            'delete_url' => '/service/upload/deletecenter?uuid=' . $upload->uuid,
            'key' => $upload->uuid,
            'md5' => md5($upload->uuid),
            'mode' => 'product',
            'name' => $upload->name,
            'id' => intval($this->_request->getParam('id')),
            'type' => $this->_request->getParam('type'),
            'error' => false,
            'thumbnail_url' => $img->image()->thumbnailImage($upload->uuid, $mode, $image, true, null, false),
            'thumb' => $img->image()->thumbnailImage($upload->uuid, $mode, $image, false, null, false));



        echo Zend_Json::encode($content);
    }

    public function uploadAction() {
        $this->getHelper('layout')->disableLayout();
        $this->_helper->viewRenderer->setNoRender();

        $adapter = new Zend_File_Transfer_Adapter_Http();

        $adapter->addValidator('Extension', false, $this->_extAllow);

        $id = TP_Util::uuid();

        if (!file_exists('uploads/' . $this->shop->uid . '/article')) {
            mkdir('uploads/' . $this->shop->uid . '/article', 0777, true);
        }

        $adapter->setDestination('uploads/' . $this->shop->uid . '/article');

        $orginalFilename = $adapter->getFileName(null, false);

        $adapter->addFilter('Rename', 'uploads/' . $this->shop->uid . '/article/' . $id . '_' . $adapter->getFileName(null, false));

        if (!$adapter->receive()) {
            $error = $adapter->getMessages();
            if (isset($error['fileExtensionFalse'])) {
                $arguments = array('error' => true, 'message' => $error['fileExtensionFalse']);
            } else {
                $arguments = array('error' => true, 'message' => 'Upload Error');
            }
            die(Zend_Json::encode($arguments));
        }

        TP_Queue::process('upload_in_process', 'article', 'uploads/' . $this->shop->uid . '/article/' . $adapter->getFileName(null, false));

        $basket = new TP_Basket();

        $article = $basket->getTempProduct();
        $article->setFiles($id, $adapter->getFileName(null, false), $orginalFilename);
        $basket->setTempProductClass($article);

        $arguments = array('error' => false, 'message' => 'Upload Finish');

        die(Zend_Json::encode($arguments));
    }

    public function checkpreflightAction() {

        $this->getHelper('layout')->disableLayout();
        $this->_helper->viewRenderer->setNoRender();

        $basket = new TP_Basket();

        $article = $basket->getTempProduct();
        $article = Doctrine_Query::create()
            ->from('Article c')
            ->where('c.uuid = ?', array($article->getArticleId()))
            ->fetchOne();

        $preflight = new TP_Preflight($article, $basket, $basket->getTempProduct());
        $preflight->validate();

        $arguments = array('preflight_valid' => $preflight->isValid(), 'preflight_errors' => $preflight->getErrors());

        die(Zend_Json::encode($arguments));
    }

    public function uploadpageAction() {
        $this->getHelper('layout')->disableLayout();
        $this->_helper->viewRenderer->setNoRender();

        $adapter = new Zend_File_Transfer_Adapter_Http();

        $adapter->addValidator('Extension', false, $this->_extAllow);

        $id = TP_Util::uuid();

        if (!file_exists('uploads/' . $this->shop->uid . '/pages')) {
            mkdir('uploads/' . $this->shop->uid . '/pages', 0777, true);
        }

        $adapter->setDestination('uploads/' . $this->shop->uid . '/pages');

        $orginalFilename = $adapter->getFileName(null, false);

        $adapter->addFilter('Rename', 'uploads/' . $this->shop->uid . '/pages/' . $id . '_' . $adapter->getFileName(null, false));

        if (!$adapter->receive()) {
            $error = $adapter->getMessages();
            if (isset($error['fileExtensionFalse'])) {
                $arguments = array('error' => true, 'message' => $error['fileExtensionFalse']);
            } else {
                $arguments = array('error' => true, 'message' => 'Upload Error');
            }
            die(Zend_Json::encode($arguments));
        }

        TP_Queue::process('upload_in_process', 'article', 'uploads/' . $this->shop->uid . '/pages/' . $adapter->getFileName(null, false));

        $files = new Zend_Session_Namespace('Basket');
        $files->files[$this->_getParam('page')][$id] = array('id' => $adapter->getFileName(null, false), 'value' => $adapter->getFileName(null, false), 'name' => $orginalFilename);

        $arguments = array('error' => false, 'message' => 'Upload Finish');

        die(Zend_Json::encode($arguments));
    }

    public function deletecenterAction() {
        $this->getHelper('layout')->disableLayout();
        $this->_helper->viewRenderer->setNoRender();

        if ($this->_getParam('uuid') == '') {
            $arguments = array('error' => false, 'message' => 'Upload Finish');

            die(Zend_Json::encode($arguments));
        }
        $row = Doctrine_Query::create()
            ->from('Upload m')
            ->where('m.uuid = ?', array($this->_request->getParam('uuid')))
            ->fetchOne();

        if ($row->Orderspos->Orders->Contact->id != $this->user->id) {
            $arguments = array('error' => false, 'message' => 'Not Allowed');

            die(Zend_Json::encode($arguments));
        }

        $row->delete();

        $arguments = array('error' => false, 'message' => 'Löschen erfolgreich');

        die(Zend_Json::encode($arguments));
    }

    public function deleteuploadpageAction() {
        $this->getHelper('layout')->disableLayout();
        $this->_helper->viewRenderer->setNoRender();

        if ($this->_getParam('uuid') == '') {
            $arguments = array('error' => true, 'message' => 'Upload Finish');

            die(Zend_Json::encode($arguments));
        }
        $files = new Zend_Session_Namespace('Basket');
        unset($files->files[$this->_getParam('page')][$this->_getParam('uuid')]);

        $arguments = array('error' => false, 'message' => 'Upload Finish');

        die(Zend_Json::encode($arguments));
    }

    public function deleteAction() {
        $this->getHelper('layout')->disableLayout();
        $this->_helper->viewRenderer->setNoRender();

        if ($this->_getParam('id') == '') {
            $arguments = array('error' => true, 'message' => 'Upload Finish');

            die(Zend_Json::encode($arguments));
        }
        $basket = new TP_Basket();

        $article = $basket->getTempProduct();
        $article->delFile($this->_getParam('id'));

        $arguments = array('error' => false, 'message' => 'Upload Finish');

        die(Zend_Json::encode($arguments));
    }

    /**
     * Gibt aktuelle Dateien zum Artikel zurück
     *
     * @return void
     */
    public function getfilesAction() {
        $this->getHelper('layout')->disableLayout();
        $this->_helper->viewRenderer->setNoRender();

        $rows = Doctrine_Query::create()
            ->from('Upload m')
            ->where('orderpos_id = ?', array(intval($this->_request->getParam('id'))))
            ->execute();
        $content = "<table>";
        require_once(APPLICATION_PATH . '/helpers/Image.php');
        foreach ($rows as $row) {
            $content .= '<tr><td><img src="/images/admin/oxygen/32x32/actions/checkmark-korganizer.png" border="0" title="Upload erfolgreich" /></td>
            <td><a href="/uploads/' . $this->shop->uid . '/article/' . $row->path . '" target="_blank">' . $row->name . '</td><td><a title="Löschen" href="javascript:deleteUploadCenter(\'' . $row->uuid . '\',\'' . $row->Orderspos->id . '\');"><img src="/scripts/jquery-uploadify/cancel.png" border="0" /></a></td></tr>';
        }
        die($content . '</table>');
    }

    /**
     * Gibt aktuelle Dateien zur Auftragsposition zurück
     *
     * @return void
     */
    public function getfilesjsonAction() {
        $this->getHelper('layout')->disableLayout();
        $this->_helper->viewRenderer->setNoRender();

        $mode = $this->_getParam('mode', 'articlelist');

        $rows = Doctrine_Query::create()
            ->from('Upload m')
            ->where('orderpos_id = ?', array(intval($this->_request->getParam('id'))))
            ->execute();

        $content = array();
        require_once(APPLICATION_PATH . '/helpers/Image.php');
        $img = new TP_View_Helper_Image();
        foreach ($rows as $row) {
            $image = array('key' => $row->id, 'value' => $row->path);
            $content[] = array(
                'key' => $row->uuid,
                'img' => $img->image()->thumbnailImage($row->uuid, $mode, $image, null, null, false),
                'name' => $row->name,
                'shop' => $this->shop->uid,
                'value' => $row->path
            );

        }

        echo Zend_Json::encode($content);
    }

    /**
     * Gibt aktuelle Dateien zur Auftragsposition zurück
     *
     * @return void
     */
    public function getfilesfromordersposAction() {
        $this->getHelper('layout')->disableLayout();
        $this->_helper->viewRenderer->setNoRender();



        $rows = Doctrine_Query::create()
            ->from('Upload m')
            ->where('orderpos_id = ? AND typ = ?', array(intval($this->_request->getParam('id')), $this->_request->getParam('type')))
            ->orderBy('updated DESC')
            ->execute();
        if(!$this->_request->getParam('type', false) || $this->_request->getParam('type', false) == 'undefined') {
            $rows = Doctrine_Query::create()
                ->from('Upload m')
                ->where('orderpos_id = ?', array(intval($this->_request->getParam('id'))))
                ->execute();
        }
        $mode = $this->_getParam('mode', 'articlelist');
        $content = array();
        require_once(APPLICATION_PATH . '/helpers/Image.php');
        $img = new TP_View_Helper_Image();
        foreach ($rows as $row) {
            $image = array('key' => $row->id, 'value' => $row->path);


            $content[] = array(
                'delete_url' => '/service/upload/deletecenter?uuid=' . $row->uuid,
                'key' => $row->uuid,
                'id'  => $row->orderpos_id,
                'download' => 'uploads/' . $row->Orderspos->Shop->uid . '/article/' . $row->path,
                'type'  => $row->typ,
                'md5' => md5($row->uuid),
                'mode' => 'product',
                'name' => $row->name,
                'comment' => $row->preflight_errors,
                'thumbnail_url' => $img->image()->thumbnailImage($row->uuid, $mode, $image, true, null, false),
                'thumb' => $img->image()->thumbnailImage($row->uuid, $mode, $image, false, null, false));

        }

        echo Zend_Json::encode(array('files' => $content));
    }



    /**
     * Gibt aktuelle Dateien zum Artikel zurück
     *
     * @return void
     */
    public function getfilespageAction() {
        $this->getHelper('layout')->disableLayout();
        $this->_helper->viewRenderer->setNoRender();

        $files = new Zend_Session_Namespace('Basket');

        $content = "<table>";
        require_once(APPLICATION_PATH . '/helpers/Image.php');
        $img = new TP_View_Helper_Image();
        foreach ($files->files[$this->_getParam('page')] as $key => $row) {
            $row['key'] = $key;
            $content .= '<tr><td><a href="javascript:deleteUpload(\'' . $row['key'] . '\');"><img src="/images/admin/oxygen/22x22/actions/dialog-cancel.png" border="0" /></a></td><td>
                ' . $img->image()->thumbnailPage($key, 'articlelist', $row, null, null, false) . '
                </td><td><a href="/uploads/' . $this->shop->uid . '/pages/' . $row['value'] . '" target="_blank">' . $row['name'] . '</td></tr>';
        }
        die($content . '</table>');
    }

    /**
     * Gibt aktuelle Dateien zum Artikel zurück
     *
     * @return void
     */
    public function getfilesproductAction() {
        $this->getHelper('layout')->disableLayout();
        $this->_helper->viewRenderer->setNoRender();

        $basket = new TP_Basket();

        $rows = $basket->getTempProduct()->getFiles();

        $content = "<table>";
        require_once(APPLICATION_PATH . '/helpers/Image.php');
        $img = new TP_View_Helper_Image();
        foreach ($rows as $key => $row) {
            $row['key'] = $key;
            $content .= '<tr><td><a href="javascript:deleteUpload(\'' . $row['key'] . '\');"><img src="/images/admin/oxygen/22x22/actions/dialog-cancel.png" border="0" /></a></td><td>
                ' . $img->image()->thumbnailImage($key, 'articlelist', $row, null, null, false) . '
                </td><td><a href="/uploads/' . $this->shop->uid . '/article/' . $row['value'] . '" target="_blank">' . $row['name'] . '</td></tr>';
        }
        die($content . '</table>');
    }

    public function getfilesproductjsonAction() {
        $this->getHelper('layout')->disableLayout();
        $this->_helper->viewRenderer->setNoRender();

        $basket = new TP_Basket();

        $rows = $basket->getTempProduct()->getFiles();

        $content = array();
        require_once(APPLICATION_PATH . '/helpers/Image.php');
        $img = new TP_View_Helper_Image();
        foreach ($rows as $key => $row) {
            $row['key'] = $key;
            $content[] = array(
                'key' => $row['key'],
                'img' => $img->image()->thumbnailImage($key, 'articlelist', $row, null, null, false),
                'name' => $row['name'],
                'shop' => $this->shop->uid,
                'value' => $row['value']
            );

        }

        echo Zend_Json::encode($content);
    }

    /**
     * Löschen eines File
     *
     * @return void
     */
    public function deleteuploadAction() {
        $this->getHelper('layout')->disableLayout();
        $this->_helper->viewRenderer->setNoRender();

        $basket = new TP_Basket();

        $basket->getTempProduct()->delFile($this->_getParam('uuid'));
        $arguments = array('error' => false, 'message' => 'Löschen erfolgreich');

        die(Zend_Json::encode($arguments));
    }

    /**
     * Upload Statusabfrage
     *
     * @return void
     */
    public function fileinfoAction() {

        TP_Queue::process('upload_in_process', 'article', $this);

        $adapter = new Zend_ProgressBar_Adapter_JsPull ();

        Zend_File_Transfer_Adapter_Http::getProgress(array('progress' => $adapter));
    }

    /**
     * Löscht ein File zum Artikel
     *
     * @return void
     */
    public function deletefileAction() {

        $this->getHelper('layout')->disableLayout();
        $this->_helper->viewRenderer->setNoRender();

        $row = Doctrine_Query::create()
            ->from('Upload m')
            ->where('id = ?', array(intval($this->_request->getParam('uid'))))
            ->fetchOne();

        if ($row->Orderspos->Orders->contact_id != $this->user->id) {
            $this->getHelper('Json')->sendJson(array('success' => false));
        }

        $row->delete();

        $this->getHelper('Json')->sendJson(array('success' => true));
    }

    /**
     * Layouter functions
     *
     */

    /**
     * Upload Prepear
     *
     * @return void
     */
    public function uploadprepareAction() {
        $this->getHelper('layout')->disableLayout();
        $this->_helper->viewRenderer->setNoRender();

        $xml = simplexml_load_file($_FILES['FILE']['tmp_name']);

        $lu = new Layouterupload();
        $lu->uploadticket_id = Zend_Session::getId();
        $lu->session_id = Zend_Session::getId();
        $lu->article_id = $xml->PRODUCT->attributes()->id;

        try {
            $lu->save();
        } catch (Exception $e) {
            Zend_Registry::get('log')->log("Error" . $e->getMessage(), Zend_Log::DEBUG);
        }

        $article = Doctrine_Query::create()
            ->from('Article c')
            ->where('c.enable = 1 AND c.layouterid = ?', array($lu->article_id))
            ->fetchOne();

        $basket = new TP_Basket();
        $sessart = $basket->getTempProduct();
        $sessart->setArticleId($article->uuid);
        $sessart->setArticle($article->toArray());

        $priceorg = (string)$xml->PRODUCT->BasePrice;
        $price = substr($priceorg, 0, (strlen($priceorg) - 2));

        $price = $price . '.' . substr($priceorg, (strlen($priceorg) - 2), strlen($priceorg));

        $sessart->setNetto($price);
        $sessart->setNetto($price);
        $sessart->setSteuer(0);
        $sessart->setBrutto($price);

        $basket->setTempProductClass($sessart);

        $result = '<html><?xml version="1.0" encoding="UTF-8"?>
					<response>
					<parameter name="ticketid">' . $lu->uploadticket_id . '</parameter>
					<parameter name="errorId">0</parameter>
					<parameter name="errorMessage" />
					<parameter name="sessionId">' . Zend_Session::getId() . '</parameter>
					</response></html>';
        print($result);
    }

    /**
     * Begin Chunk
     *
     * @return void
     */
    public function beginchunkAction() {
        $this->getHelper('layout')->disableLayout();
        $this->_helper->viewRenderer->setNoRender();

        $chunktitle = new Zend_Session_Namespace('chunktitle');

        $chunktitle->unlock();

        $chunktitle->chunktitle = $_REQUEST['chunktitle'];

        $chunktitle->lock();
        $result = '<html><?xml version="1.0" encoding="UTF-8"?>
					<response>
					<parameter name="chunkid">' . Zend_Session::getId() . '</parameter>
					<parameter name="errorId">0</parameter>
					<parameter name="errorMessage" />
					<parameter name="sessionId">' . Zend_Session::getId() . '</parameter>
					</response></html>';
        print($result);
    }

    /**
     * Transmit Chunk
     *
     * @return void
     */
    public function transmitchunkfileAction() {
        $this->getHelper('layout')->disableLayout();
        $this->_helper->viewRenderer->setNoRender();

        try {

            $adapter = new Zend_File_Transfer_Adapter_Http();

            $id = TP_Util::uuid();

            $adapter->setDestination('uploads/' . $this->shop->uid . '/article');

            $orginalFilename = $adapter->getFileName(null, false);

            $adapter->addFilter('Rename', 'uploads/' . $this->shop->uid . '/article/' . $id . '_' . $adapter->getFileName(null, false));

            if (!$adapter->receive()) {
                Zend_Registry::get('log')->log("transmitchunk ERROR", 'TEST');
            }

            $basket = new TP_Basket();
            $article = $basket->getTempProduct();

            $chunktitle = new Zend_Session_Namespace('chunktitle');

            $article->setFiles($id, $adapter->getFileName(null, false), $orginalFilename, $chunktitle->chunktitle);
            $basket->setTempProductClass($article);
        } catch (Exception $e) {
            Zend_Registry::get('log')->log("Error" . $e->getMessage(), Zend_Log::DEBUG);
        }

        $result = '<html><?xml version="1.0" encoding="UTF-8"?>
					<response>
					<parameter name="bytesReceived">20000</parameter>
					<parameter name="errorId">0</parameter>
					<parameter name="errorMessage" />
					<parameter name="sessionId">' . Zend_Session::getId() . '</parameter>
					</response></html>';
        print($result);
    }

    /**
     * finalize Chunk
     *
     * @return void
     */
    public function finalizechunkAction() {
        $this->getHelper('layout')->disableLayout();
        $this->_helper->viewRenderer->setNoRender();

        $result = '<html><?xml version="1.0" encoding="UTF-8"?>
					<response>
					<parameter name="pagesReceived">2</parameter>
					<parameter name="errorId">0</parameter>
					<parameter name="errorMessage" />
					<parameter name="sessionId">' . Zend_Session::getId() . '</parameter>
					</response></html>';
        print($result);
    }

    /**
     * Upload Finalize
     *
     * @return void
     */
    public function uploadfinalizeAction() {
        $this->getHelper('layout')->disableLayout();
        $this->_helper->viewRenderer->setNoRender();

        $basket = new TP_Basket();
        $sessart = $basket->getTempProduct()->getArticle();

        $files = $basket->getTempProduct()->getFiles();
        $basket->getTempProduct()->setCount(1);
        //$basket->addProduct();

        $dateien = '';
        foreach ($files as $key => $file) {
            $dateien = $dateien . ' ' . PUBLIC_PATH . '/uploads/' . $this->shop->uid . '/article/' . $file['value'];
        }

        exec('gm convert ' . $dateien . ' ' . PUBLIC_PATH . '/temp/preview/' . $basket->getAllCount() . '_' . Zend_Session::getId() . '_preview.pdf');

        $result = '<html><?xml version="1.0" encoding="UTF-8"?>
					<response>
					<parameter name="chunksReceived">2</parameter>
					<parameter name="errorId">0</parameter>
					<parameter name="errorMessage" />
					<parameter name="sessionId">' . Zend_Session::getId() . '</parameter>
					</response></html>';
        print($result);
    }

    /**
     * Get Shopbasket url
     *
     * @return void
     */
    public function geturlAction() {
        $this->getHelper('layout')->disableLayout();
        $this->_helper->viewRenderer->setNoRender();

        $basket = new TP_Basket();
        $sessart = $basket->getTempProduct()->getArticleId();

        $article = Doctrine_Query::create()
            ->from('Article c')
            ->where('c.enable = 1 AND c.uuid = ?', array($sessart))
            ->fetchOne();

        $result = '<html><?xml version="1.0" encoding="UTF-8"?>
					<response>
					<parameter name="url">http://' . $this->shop->Domain[0]->name . '/article/show/' . $article["url"] . '?ARTID=' . Zend_Session::getId() . '</parameter>
					<parameter name="errorId">0</parameter>
					<parameter name="errorMessage" />
					<parameter name="sessionId">' . Zend_Session::getId() . '</parameter>
					</response></html>';
        print($result);
    }

    /**
     * Get Datapacks
     *
     */
    public function getdatapacksAction() {
        $this->getHelper('layout')->disableLayout();
        $this->_helper->viewRenderer->setNoRender();
    }

}
