<?php

class booking_queues
{

    public $id = "19";
    public $name = "Booking";
    public $information = "Stores Booking Files";
    public $form = 'Booking/config/form.ini';
    public $title;
    public $pos;
    public $when;
    public $path;
    public $mode;
    public $pattern;

    public function toArray() {
        return array('pos' => $this->pos, 'title' => $this->title, 'when' => $this->when, 'path' => $this->path, 'mode' => $this->mode, 'pattern' => $this->pattern);
    }

    public function process($queue, $obj) {

        $shop = Zend_Registry::get('shop');

        if ($this->path == "") {
            $this->path = APPLICATION_PATH . '/../data/export/' . $shop['uid'] . '/';
            if (!file_exists($this->path)) {
                mkdir($this->path);
            }
        }

        if ($obj instanceof Orders) {


            /*
             * Konto Führendes Buchungskonto
                Gkto Gegenkonto
                Belegnr Belegnummer, Rechnungsnummer
                Buchdat Buchungsdatum
                Belegdat Belegdatum, Rechnungsdatum
                Bucod Code, ob Soll oder Haben
                Steucod Code, ob Vorsteuer oder Mehrwertsteuer
                Betrag Buchungsbetrag für führendes Konto
                Mwst Steuerprozentsatz
                Steuer Steuerbetrag
                Text Buchungstext
                Symbol Belegsymbol
             */

            $date = new DateTime($obj->created);

            $list = array (
                array(
                    215002,
                    4011,
                    $obj->alias,
                    $date->format("d.m.Y"),
                    $obj->preis,
                    20,
                    $obj->preissteuer*-1,
                    'Rechnung R-'.$obj->alias,
                    ),
                array(
                    2817,
                    215002,
                    $obj->alias,
                    $date->format("d.m.Y"),
                    $obj->preisbrutto,
                    0,
                    0,
                    'Bezahlung R-'.$obj->alias,
                    )
            );



            if (!file_exists($this->path)) {
                mkdir($this->path);
            }

            $xmlfiles = array();

            if($this->pattern == "") {

                $fp = fopen($this->path . $obj->alias . '.csv', 'w');

                foreach ($list as $fields) {
                    fputcsv($fp, $fields);
                }

                fclose($fp);


                $xmlfiles[] = array('when' => $this->when, 'name' => $obj->alias . '.csv', 'value' => $this->path . $obj->alias . '.csv');
            }else{
                $loader = new Twig_Loader_String();
                $twig = new Twig_Environment($loader);
                $template = $twig->loadTemplate($this->pattern);
                $name = $template->render(array(
                    'order' => $obj
                ));
                $name = $this->slugify($name);

                $fp = fopen($this->path . $name . '.csv', 'w');

                foreach ($list as $fields) {
                    fputcsv($fp, $fields);
                }

                fclose($fp);

                $xmlfiles[] = array('when' => $this->when, 'name' => $name . '.csv', 'value' => $this->path . $name . '.csv');
            }





            Zend_Registry::set('xmlexport', $xmlfiles);

        }
    }

    public function slugify($string)
    {
        $ret = $string;
        // remove html line break
        $ret = preg_replace("<br/>", '', $ret);
        // strip all non word chars
        $ret = preg_replace('/\W/u', ' ', $ret);
        // replace all white space sections with a dash
        $ret = preg_replace('/\ +/', '-', $ret);
        // trim dashes
        $ret = preg_replace('/\-$/', '', $ret);
        $ret = preg_replace('/^\-/', '', $ret);
        // convert to lower case
        $ret = strtolower($ret);

        return $ret;
    }

}
