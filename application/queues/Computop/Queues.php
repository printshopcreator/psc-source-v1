<?php

class computop_queues {

    public $id = "9";
    public $name = "Computop";
    public $information = "Computop";
    public $form = 'Computop/config/form.ini';
    public $userid;
    public $payssl, $paypal, $payelv, $payotf;
    public $password;
    public $url;
    public $ids;
    public $title;
    public $pos;
    public $when;
    public $mode;

    public function toArray() {
        return array('pos' => $this->pos, 'when' => $this->when, 'mode' => $this->mode, 'title' => $this->title, 'payssl' => $this->payssl, 'payelv' => $this->payelv, 'paypal' => $this->paypal, 'payotf' => $this->payotf, 'userid' => $this->userid, 'password' => $this->password, 'url' => $this->url, 'ids' => $this->ids);
    }

    public function process($queue, $obj) {

        if ($obj instanceof TP_Basket) {

            if ($obj->getPaymenttype() == $this->ids) {

                $params = Zend_Registry::get('params');


                if($params['Data'] != '') {
                    ini_set('display_errors', 0);
                    error_reporting(E_ERROR);
                    require_once(__DIR__ . '/function.inc.php');
                    $BlowFish = new ctBlowfish;
                    $Data = $BlowFish->ctDecrypt($params['Data'], $params['Len'], $this->password);

                    $a = ""; $a = split ('&', $Data);
                    $UserData = $BlowFish->ctSplit($a, '=', 'UserData');

                    $basket = new TP_Basket( );

                    if($UserData == $basket->getToken()) {
                        return true;
                    }else{

                        header('Location: https://'.$_SERVER["SERVER_NAME"].'/basket/finish?error=Computop Error');
                    }

                    die();
                }


                $Response = "&Response=encrypt";

                
                //$Capture = "&Capture=AUTO";
                $basket = new TP_Basket( );
                $basket->setToken(TP_Util::uuid());
                $UserData = "&UserData=".$basket->getToken();
                // format data which is to be transmitted - required

                $TransID    = "&TransID="     .time();
                $URLSuccess = "&URLSuccess="  .'https://'.$_SERVER["SERVER_NAME"].'/basket/finish';
                $URLFailure = "&URLFailure="  .'https://'.$_SERVER["SERVER_NAME"].'/basket/finish?error=Computop Error';
                $OrderDesc  = "&OrderDesc="   ."https://".$_SERVER["SERVER_NAME"];
                $URLNotify  = "&URLNotify="   .'https://'.$_SERVER["SERVER_NAME"].'/service/api/computop';

                $plaintext  = "MerchantID=".$this->userid.$TransID.'&Amount='.(round($obj->getPreisBrutto(),2) * 100).'&Currency=EUR'.
                        $URLSuccess.$URLFailure.$URLNotify.$OrderDesc.$UserData.$Response;
                $Len        = strlen($plaintext);  // Length of the plain text string


                // encryption
                ini_set('display_errors', 0);
                error_reporting(E_ERROR);
                require_once(__DIR__ . '/function.inc.php');
                $BlowFish = new ctBlowfish;
                $Data = $BlowFish->ctEncrypt($plaintext, $Len, $this->password);
                
                if($this->payelv == "1") {
                    header("Location: https://www.netkauf.de/paygate/payelv.aspx?MerchantID=". $this->userid ."&Len=".$Len."&Data=".$Data);
                    die();
                }elseif($this->payssl == "1") {
                    header("Location: https://www.netkauf.de/paygate/payssl.aspx?MerchantID=". $this->userid ."&Len=".$Len."&Data=".$Data);
                    die();
                }elseif($this->payotf == "1") {
                    header("Location: https://www.netkauf.de/paygate/payOTF.aspx?MerchantID=". $this->userid ."&Len=".$Len."&Data=".$Data);
                    die();
                }elseif($this->paypal == "1") {
                    header("Location: https://www.computop-paygate.com/paypal.aspx?MerchantID=". $this->userid ."&Len=".$Len."&Data=".$Data);
                    die();
                }
                die();
            }
        }
    }

}
