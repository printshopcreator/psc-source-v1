<?php

class csv_queues
{

    public $id = "14";
    public $name = "Csv";
    public $information = "Stores Csv Files";
    public $form = 'Csv/config/form.ini';
    public $title;
    public $pos;
    public $when;
    public $path;
    public $server_path;
    public $mode;

    public function toArray()
    {
        return array('pos' => $this->pos, 'title' => $this->title, 'when' => $this->when, 'server_path' => $this->server_path, 'path' => $this->path, 'mode' => $this->mode);
    }

    public function process($queue, $obj)
    {

        $shop = Zend_Registry::get('shop');

        if ($this->path == "") {
            $this->path = APPLICATION_PATH . '/../data/export/' . $shop['uid'] . '/';
            if (!file_exists($this->path)) {
                mkdir($this->path);
            }
        }

        if ($obj instanceof Orders) {

            $data = array();

            foreach ($obj->Orderspos as $orderpos) {

                if(count($orderpos->Upload) >= 1) {
                    foreach ($orderpos->Upload as $file) {

                        $datatemp = array();
                        $datatemp[] = $orderpos->pos;
                        $datatemp[] = $obj->Shop->name;
                        $datatemp[] = $obj->created;

                        $datatemp[] = $obj->preisbrutto;
                        $datatemp[] = $obj->preissteuer;
                        $datatemp[] = $obj->preis;

                        $datatemp[] = $obj->Account->company;

                        $invoice = $obj->getInvoiceAddress();
                        $delivery = $obj->getDeliveryAddress();
                        $sender = $obj->getSenderAddress();

                        $datatemp[] = $invoice->getCompany();
                        $datatemp[] = $invoice->getCompany2();
                        $datatemp[] = $invoice->getStreet();
                        $datatemp[] = $invoice->getHouseNumber();
                        $datatemp[] = $invoice->getCity();
                        $datatemp[] = $invoice->getZip();
                        $datatemp[] = $invoice->getCountry();
                        $datatemp[] = $invoice->getFirstname();
                        $datatemp[] = $invoice->getLastname();
                        $datatemp[] = $invoice->getPhone();
                        $datatemp[] = $invoice->getMail();

                        $datatemp[] = $delivery->getCompany();
                        $datatemp[] = $delivery->getCompany2();
                        $datatemp[] = $delivery->getStreet();
                        $datatemp[] = $delivery->getHouseNumber();
                        $datatemp[] = $delivery->getCity();
                        $datatemp[] = $delivery->getZip();
                        $datatemp[] = $delivery->getCountry();
                        $datatemp[] = $delivery->getFirstname();
                        $datatemp[] = $delivery->getLastname();
                        $datatemp[] = $delivery->getPhone();
                        $datatemp[] = $delivery->getMail();

                        $datatemp[] = $sender->getCompany();
                        $datatemp[] = $sender->getCompany2();
                        $datatemp[] = $sender->getStreet();
                        $datatemp[] = $sender->getHouseNumber();
                        $datatemp[] = $sender->getCity();
                        $datatemp[] = $sender->getZip();
                        $datatemp[] = $sender->getCountry();
                        $datatemp[] = $sender->getFirstname();
                        $datatemp[] = $sender->getLastname();
                        $datatemp[] = $sender->getPhone();
                        $datatemp[] = $sender->getMail();

                        $datatemp[] = $obj->Paymenttype->title;
                        $datatemp[] = $obj->Paymenttype->id;
                        $datatemp[] = $obj->zahlkosten;

                        $datatemp[] = $obj->Shippingtype->title;
                        $datatemp[] = $obj->Shippingtype->id;
                        $datatemp[] = $obj->versandkosten;

                        $datatemp[] = $obj->basketfield1;
                        $datatemp[] = $obj->basketfield2;

                        $article = Doctrine_Query::create()
                            ->from('Article c')
                            ->where('c.id = ?', array($orderpos->article_id))
                            ->fetchOne();
                        if (!$article) {
                            continue;
                        }
                        $pro = unserialize($orderpos->data);
                        $proa = $pro->getArticle();




                        $datatemp[] = $orderpos->count;
                        $datatemp[] = $proa['title'];
                        $datatemp[] = $orderpos->ref;
                        $datatemp[] = $orderpos->kst;
                        $datatemp[] = $orderpos->weight;
                        $datatemp[] = (bool)$pro->getSpecial();

                        $datatemp[] = $article->article_nr_intern;
                        $datatemp[] = $article->article_nr_extern;

                        $datatemp[] = $orderpos->priceone;
                        $datatemp[] = $orderpos->priceall;

                        $datatemp[] = $article->mwert;

                        if ($article->typ == 6) {
                            $datatemp[] = $obj->alias . "_" . $article->id . "_" . $orderpos->pos . ".zip";
                        }
                        $datatemp[] = $this->server_path . '/'.$obj->alias."/".$obj->alias ."_". $article->id . "_" . $orderpos->pos . "/".$obj->alias . "_" . $orderpos->pos."_". $article->id . "_" . $file['name'];
                        $options = $article->getOptArray($orderpos->data, true, true, true);

                        foreach ($options as $opt_key => $opt_name) {
                            $datatemp[] = $opt_name;
                        }

                        $data[] = $datatemp;
                    }
                }else{


                    $datatemp = array();
                    $datatemp[] = $orderpos->pos;
                    $datatemp[] = $obj->Shop->name;
                    $datatemp[] = $obj->created;

                    $datatemp[] = $obj->preisbrutto;
                    $datatemp[] = $obj->preissteuer;
                    $datatemp[] = $obj->preis;

                    $datatemp[] = $obj->Account->company;

                    $invoice = $obj->getInvoiceAddress();
                    $delivery = $obj->getDeliveryAddress();
                    $sender = $obj->getSenderAddress();

                    $datatemp[] = $invoice->getCompany();
                    $datatemp[] = $invoice->getCompany2();
                    $datatemp[] = $invoice->getStreet();
                    $datatemp[] = $invoice->getHouseNumber();
                    $datatemp[] = $invoice->getCity();
                    $datatemp[] = $invoice->getZip();
                    $datatemp[] = $invoice->getCountry();
                    $datatemp[] = $invoice->getFirstname();
                    $datatemp[] = $invoice->getLastname();
                    $datatemp[] = $invoice->getPhone();
                    $datatemp[] = $invoice->getMail();

                    $datatemp[] = $delivery->getCompany();
                    $datatemp[] = $delivery->getCompany2();
                    $datatemp[] = $delivery->getStreet();
                    $datatemp[] = $delivery->getHouseNumber();
                    $datatemp[] = $delivery->getCity();
                    $datatemp[] = $delivery->getZip();
                    $datatemp[] = $delivery->getCountry();
                    $datatemp[] = $delivery->getFirstname();
                    $datatemp[] = $delivery->getLastname();
                    $datatemp[] = $delivery->getPhone();
                    $datatemp[] = $delivery->getMail();

                    $datatemp[] = $sender->getCompany();
                    $datatemp[] = $sender->getCompany2();
                    $datatemp[] = $sender->getStreet();
                    $datatemp[] = $sender->getHouseNumber();
                    $datatemp[] = $sender->getCity();
                    $datatemp[] = $sender->getZip();
                    $datatemp[] = $sender->getCountry();
                    $datatemp[] = $sender->getFirstname();
                    $datatemp[] = $sender->getLastname();
                    $datatemp[] = $sender->getPhone();
                    $datatemp[] = $sender->getMail();

                    $datatemp[] = $obj->Paymenttype->title;
                    $datatemp[] = $obj->Paymenttype->id;
                    $datatemp[] = $obj->zahlkosten;

                    $datatemp[] = $obj->Shippingtype->title;
                    $datatemp[] = $obj->Shippingtype->id;
                    $datatemp[] = $obj->versandkosten;

                    $datatemp[] = $obj->basketfield1;
                    $datatemp[] = $obj->basketfield2;

                    $article = Doctrine_Query::create()
                        ->from('Article c')
                        ->where('c.id = ?', array($orderpos->article_id))
                        ->fetchOne();
                    if (!$article) {
                        continue;
                    }
                    $pro = unserialize($orderpos->data);
                    $proa = $pro->getArticle();




                    $datatemp[] = $orderpos->count;
                    $datatemp[] = $proa['title'];
                    $datatemp[] = $orderpos->ref;
                    $datatemp[] = $orderpos->kst;
                    $datatemp[] = $orderpos->weight;
                    $datatemp[] = (bool)$pro->getSpecial();

                    $datatemp[] = $article->article_nr_intern;
                    $datatemp[] = $article->article_nr_extern;

                    $datatemp[] = $orderpos->priceone;
                    $datatemp[] = $orderpos->priceall;

                    $datatemp[] = $article->mwert;

                    if ($article->typ == 6) {
                        $datatemp[] = $obj->alias . "_" . $article->id . "_" . $orderpos->pos . ".zip";
                    }

                    $options = $article->getOptArray($orderpos->data, true, true, true);

                    foreach ($options as $opt_key => $opt_name) {
                        $datatemp[] = $opt_name;
                    }

                    $data[] = $datatemp;
                }



            }

            if (!file_exists($this->path)) {
                mkdir($this->path);
            }

            $fp = fopen($this->path . $obj->alias . '.csv', 'w');

            foreach ($data as $fields) {
                fputcsv($fp, $fields);
            }

            fclose($fp);
            $xmlfiles = array();
            if(Zend_Registry::isRegistered('xmlexport')) {
                $xmlfiles = Zend_Registry::get('xmlexport');
            }

            $xmlfiles[] = array('when' => $this->when, 'name' => $obj->alias . '.csv', 'value' => $this->path . $obj->alias . '.csv');
            Zend_Registry::set('xmlexport', $xmlfiles);

        }
    }

}
