<?php

class directpos_queues {

    public $id = "11";
    public $name = "DirectPos";
    public $information = "DirectPos";
    public $form = 'Directpos/config/form.ini';
    public $userid;
    public $password;
    public $url;
    public $ids;
    public $title;
    public $pos;
    public $when;
    public $mode;
    public $debit;
    public $maestro;
    public $creditcard;

    public $test_mode;

    public function toArray() {
        return array('pos' => $this->pos, 'test_mode' => $this->test_mode, 'when' => $this->when, 'creditcard' => $this->creditcard,'maestro' => $this->maestro,'debit' => $this->debit, 'mode' => $this->mode, 'title' => $this->title, 'userid' => $this->userid, 'password' => $this->password, 'url' => $this->url, 'ids' => $this->ids);
    }

    public function process($queue, $obj) {

        if ($obj instanceof TP_Basket) {

            if ($obj->getPaymenttype() == $this->ids) {

                $params = Zend_Registry::get('params');

                if($params['hash'] == 'Ja') {
     
                        return true;
                   
                }

                $basket = new TP_Basket( );
                $basket->setToken(TP_Util::uuid());

                if($this->creditcard == "1") {
                    $attributes = array(
                        'amount' => str_replace('.', ',', round($obj->getPreisBrutto(),2)),
                        'basketid' => time(),
                        'command' => 'sslform',
                        'currency' => 'EUR',
                        'date' => date('Ymd_H:m:s'),
                        'orderid' => time(),
                        'paymentmethod' => 'creditcard',
                        'sessionid' => $basket->getToken(),
                        'sslmerchant' => $this->userid,
                        'transactiontype' => 'authorization',
                        'version' => '1.5'
                    );
                }

                if($this->debit == "1") {
                    $attributes = array(
                        'amount' => str_replace('.', ',', round($obj->getPreisBrutto(),2)),
                        'basketid' => time(),
                        'command' => 'sslform',
                        'currency' => 'EUR',
                        'date' => date('Ymd_H:m:s'),
                        'orderid' => time(),
                        'paymentmethod' => 'directdebit',
                        'sessionid' => $basket->getToken(),
                        'sslmerchant' => $this->userid,
                        'transactiontype' => 'authorization',
                        'version' => '1.5'
                    );
                }

                if($this->maestro == "1") {
                    $attributes = array(
                        'amount' => str_replace('.', ',', round($obj->getPreisBrutto(),2)),
                        'basketid' => time(),
                        'command' => 'sslform',
                        'currency' => 'EUR',
                        'date' => date('Ymd_H:m:s'),
                        'orderid' => time(),
                        'paymentmethod' => 'maestro',
                        'sessionid' => $basket->getToken(),
                        'sslmerchant' => $this->userid,
                        'transactiontype' => 'authorization',
                        'version' => '1.5'
                    );
                }

                $mac = $this->hmac($this->password,implode("", array_values($attributes)));
                $attributes['mac'] = $mac;


                $strAttributesArray = array();
                foreach ($attributes as $k => $v)    {
                    $strAttributesArray[] = $k . '=' . $v;
                }
                $strAttributes = implode ('&', $strAttributesArray);

                /* get the PayInit URL from the hosting server */

                if($this->test_mode == "1") {
                    $payinit_url = 'https://testmerch.directpos.de/vbv/mpi_legacy?'.$strAttributes;
                }else{
                    $payinit_url = 'https://merch.directpos.de/vbv/mpi_legacy?'.$strAttributes;
                }

                header("Location: $payinit_url");

                die();
            }
        }
    }

    protected function hmac ($key, $data) {
    // RFC 2104 HMAC implementation for php. // Creates an SHA-1 HMAC.
    $b = 64; // byte length for SHA-1 // if the key has more than 64 bytes, hash it //if (strlen($key) > $b) {
    //	$key = pack("H*",sha1($key)); //}
    
    $key = str_pad($key, $b, chr(0x00));
    
    $ipad = str_pad('', $b, chr(0x36));
    $opad = str_pad('', $b, chr(0x5c));
    $k_ipad = $key ^ $ipad;
    $k_opad = $key ^ $opad;
    return sha1($k_opad . pack("H*",sha1($k_ipad . $data)));
    }

}
