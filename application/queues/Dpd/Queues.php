<?php

class dpd_queues {

    public $id = "21";
    public $name = "Dpd";
    public $information = "DPD";
    public $form = 'Dpd/config/form.ini';
    public $title;
    public $pos;
    public $when;
    public $path;
    public $mode;
    public $pattern;

    public $partnerName;
    public $partnerToken;
    public $partnerPass;
    public $addressName;
    public $addressStreet;
    public $addressZip;
    public $addressCity;
    public $express_830_id;
    public $express_10_id;
    public $express_12_id;
    public $express_18_id;

    public function toArray() {
        return array('express_830_id' => $this->express_830_id,
            'express_10_id' => $this->express_10_id,
            'express_12_id' => $this->express_12_id,
            'express_18_id' => $this->express_18_id,
            'addressName' => $this->addressName,
            'addressStreet' => $this->addressStreet,
            'addressZip' => $this->addressZip,
            'addressCity' => $this->addressCity,
            'partnerName' => $this->partnerName, 'partnerPass' => $this->partnerPass, 'pos' => $this->pos, 'title' => $this->title, 'when' => $this->when, 'path' => $this->path, 'mode' => $this->mode, 'pattern' => $this->pattern);
    }

    public function process($queue, $obj) {

        $shop = Zend_Registry::get('shop');

        if ($this->path == "") {
            $this->path = APPLICATION_PATH . '/../data/export/' . $shop['uid'] . '/';
            if (!file_exists($this->path)) {
                mkdir($this->path);
            }
        }

        if ($obj instanceof Orders) {


            if($obj->package != "") {

                if(Zend_Registry::isRegistered('xmlexport')) {
                    $xmlfiles = Zend_Registry::get('xmlexport');
                }else{
                    $xmlfiles = array();
                }


                if($this->pattern == "") {
                    $xmlfiles[] = array('when' => $this->when, 'name' => $obj->alias . '_dpd.pdf', 'value' => $this->path . $obj->alias . '_dpd.pdf');
                }else{
                    $loader = new Twig_Loader_String();
                    $twig = new Twig_Environment($loader);
                    $template = $twig->loadTemplate($this->pattern);
                    $name = $template->render(array(
                        'order' => $obj
                    ));
                    $name = $this->slugify($name);
                    $xmlfiles[] = array('when' => $this->when, 'name' => $name . '_dpd.pdf', 'value' => $this->path . $name . '_dpd.pdf');
                }

                Zend_Registry::set('xmlexport', $xmlfiles);

                return;
            }

            $invoice = $obj->getInvoiceAddress();
            $delivery = $obj->getDeliveryAddress();
            $sender = $obj->getSenderAddress();


            $shipment = "CL";
            if($obj->Shippingtype->id == $this->express_830_id) {
                $shipment = "E830";
            }
            if($obj->Shippingtype->id == $this->express_10_id) {
                $shipment = "E10";
            }
            if($obj->Shippingtype->id == $this->express_12_id) {
                $shipment = "E12";
            }
            if($obj->Shippingtype->id == $this->express_18_id) {
                $shipment = "E18";
            }

            $weight = 0;
            foreach ($obj->Orderspos as $orderpos) {
                $weight += $orderpos->weight;
            }

            if($weight > 0) {
                $weight = round($weight/1000);
            }

            $service_url = 'https://public-ws.dpd.com/services/LoginService/V2_0?wsdl';

            $client     = new SoapClient($service_url, array("trace" => 1, "exception" => 0));

            $data = array(

                "delisId" => $this->partnerName,
                "password" => $this->partnerPass,
                "messageLanguage" => "de_DE"
            );
            try {
                $res = $client->getAuth($data);
            } catch (SoapFault $exception) {
                Zend_Registry::get('log')->debug(print_r($data,true));

                Zend_Registry::get('log')->debug($exception->faultcode);
                Zend_Registry::get('log')->debug($exception->getMessage());
                return;
            }

            $auth = $res->return;

            if($delivery->getZip() == "") {

                $ansprech = substr($invoice->getFirstname() . ' ' . $invoice->getLastname(), 0, 34);

                if(trim(substr($invoice->getFirstname() . ' ' . $invoice->getLastname(), 0, 34)) == "") {
                    $ansprech = str_replace(array(";"),array(""), substr($invoice->getCompany(), 0, 34));
                }

                $data = array(
                    "printOptions" => array(
                        "paperFormat" => "A6",
                        "printerLanguage" => "PDF"
                    ),
                    "order" => array(
                        "generalShipmentData" => array(
                            "sendingDepot" => $auth->depot,
                            "product" => $shipment,
                            "mpsCompleteDelivery" => false,
                            "mpsCustomerReferenceNumber2" => $obj->alias,
                            "sender" => array(
                                "name1" => $this->addressName,
                                "street" => $this->addressStreet,
                                "country" => "DE",
                                "zipCode" => $this->addressZip,
                                "city" => $this->addressCity
                            ),
                            "recipient" => array(
                                "contact" => $ansprech,
                                "street" => substr($invoice->getStreet(), 0, 34) . ' ' . $invoice->getHouseNumber(),
                                "country" => $invoice->getCountry(),
                                "zipCode" => $invoice->getZip(),
                                "city" => $invoice->getCity(),
                                "email" => $invoice->getMail(),
                                "phone" => $invoice->getPhone(),
                            )
                        ),
                        "parcels" => array(
                            "customerReferenceNumber1" => $obj->alias
                        ),
                        "productAndServiceData" => array(
                            "orderType" => "consignment"
                        )
                    )
                );
                if(str_replace(array(";"),array(""), substr($invoice->getCompany(), 0, 34)) != "") {
                    $data['order']['generalShipmentData']['recipient']['name1'] = str_replace(array(";"),array(""), substr($invoice->getCompany(), 0, 34));
                }else{
                    $data['order']['generalShipmentData']['recipient']['name1'] =  $ansprech;
                }
            }else {

                $ansprech = substr($delivery->getFirstname() . ' ' . $delivery->getLastname(), 0, 34);

                if(trim(substr($delivery->getFirstname() . ' ' . $delivery->getLastname(), 0, 34)) == "") {
                    $ansprech = str_replace(array(";"),array(""), substr($delivery->getCompany(), 0, 34));
                }

                $data = array(
                    "printOptions" => array(
                        "paperFormat" => "A6",
                        "printerLanguage" => "PDF"
                    ),
                    "order" => array(
                        "generalShipmentData" => array(
                            "sendingDepot" => $auth->depot,
                            "product" => $shipment,
                            "mpsCompleteDelivery" => false,
                            "mpsCustomerReferenceNumber2" => $obj->alias,
                            "sender" => array(
                                "name1" => $this->addressName,
                                "street" => $this->addressStreet,
                                "country" => "DE",
                                "zipCode" => $this->addressZip,
                                "city" => $this->addressCity
                            ),
                            "recipient" => array(
                                "contact" => $ansprech,
                                "street" => substr($delivery->getStreet(), 0, 34) . ' ' . $delivery->getHouseNumber(),
                                "country" => $delivery->getCountry(),
                                "zipCode" => $delivery->getZip(),
                                "city" => $delivery->getCity(),
                                "email" => $delivery->getMail(),
                                "phone" => $delivery->getPhone(),
                            )
                        ),
                        "parcels" => array(
                            "customerReferenceNumber1" => $obj->alias
                        ),
                        "productAndServiceData" => array(
                            "orderType" => "consignment"
                        )
                    )
                );

                if(str_replace(array(";"),array(""), substr($delivery->getCompany(), 0, 34)) != "") {
                    $data['order']['generalShipmentData']['recipient']['name1'] = str_replace(array(";"),array(""), substr($delivery->getCompany(), 0, 34));
                }else{
                    $data['order']['generalShipmentData']['recipient']['name1'] =  $ansprech;
                }
            }

            $c = new SoapClient('https://public-ws.dpd.com/services/ShipmentService/V3_2?wsdl');

            $token = array(
                'delisId'         => $auth->delisId,
                'authToken'         => $auth->authToken,
                'messageLanguage'   => 'de_DE'
            );


            $header = new SOAPHeader('http://dpd.com/common/service/types/Authentication/2.0', 'authentication', $token);
            $c->__setSoapHeaders($header);

            try {
                $res = $c->storeOrders($data);

                if (!file_exists($this->path)) {
                    mkdir($this->path);
                }

                if(Zend_Registry::isRegistered('xmlexport')) {
                    $xmlfiles = Zend_Registry::get('xmlexport');
                }else{
                    $xmlfiles = array();
                }


                if(isset($res->orderResult)
                && isset($res->orderResult->shipmentResponses)
                && isset($res->orderResult->shipmentResponses->parcelInformation)) {
                    $obj->package = $res->orderResult->shipmentResponses->parcelInformation->parcelLabelNumber;
                    $obj->save();

                    if($this->pattern == "") {
                        file_put_contents($this->path . $obj->alias ."_dpd.pdf", $res->orderResult->parcellabelsPDF);
                        $xmlfiles[] = array('when' => $this->when, 'name' => $obj->alias . '_dpd.pdf', 'value' => $this->path . $obj->alias . '_dpd.pdf');
                    }else{
                        $loader = new Twig_Loader_String();
                        $twig = new Twig_Environment($loader);
                        $template = $twig->loadTemplate($this->pattern);
                        $name = $template->render(array(
                            'order' => $obj
                        ));
                        $name = $this->slugify($name);
                        file_put_contents($this->path . $name ."_dpd.pdf", $res->orderResult->parcellabelsPDF);
                        $xmlfiles[] = array('when' => $this->when, 'name' => $name . '_dpd.pdf', 'value' => $this->path . $name . '_dpd.pdf');
                    }
                }

                Zend_Registry::set('xmlexport', $xmlfiles);
            } catch (SoapFault $exception) {
                Zend_Registry::get('log')->debug(print_r($data,true));

                Zend_Registry::get('log')->debug($exception->faultcode);
                Zend_Registry::get('log')->debug($exception->getMessage());

            }
        }
    }

}
