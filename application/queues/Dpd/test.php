<?php



$service_url = 'https://public-ws-stage.dpd.com/services/LoginService/V2_0?wsdl';

$client     = new SoapClient($service_url, array("trace" => 1, "exception" => 0));

$data = array(

    "delisId" => "julia159",
    "password" => "g29483m",
    "messageLanguage" => "de_DE"
);

$res = $client->getAuth($data);

$auth = $res->return;

$c = new SoapClient('https://public-ws-stage.dpd.com/services/ShipmentService/V3_2?wsdl');

$token = array(
    'delisId'         => $auth->delisId,
    'authToken'         => $auth->authToken,
    'messageLanguage'   => 'de_DE'
);
var_dump($token);

// Set the header with the authentication token
$header = new SOAPHeader('http://dpd.com/common/service/types/Authentication/2.0', 'authentication', $token);
$c->__setSoapHeaders($header);

try {
    $res = $c->storeOrders( array
        (
            "printOptions" => array(
                "paperFormat" => "A6",
                "printerLanguage" => "PDF"
            ),
            "order" => array(
                "generalShipmentData" => array(
                    "sendingDepot" => $auth->depot,
                    "product" => "CL",
                    "mpsCompleteDelivery" => false,
                    "sender" => array(
                        "name1" => "hülswitt gmbh",
                        "street" => "Neckarstraße 9",
                        "country" => "DE",
                        "zipCode" => "45768",
                        "city" => "Marl",
                        "customerNumber" => "123456789"
                    ),
                    "recipient" => array(
                        "name1" => "Thomas Peterson",
                        "street" => "Chausseestraße 1",
                        "country" => "DE",
                        "zipCode" => "17506",
                        "city" => "Gribow",
                    )
                ),
                "parcels" => array(
                    "parcelLabelNumber" => "45645634636"
                ),
                "productAndServiceData" => array(
                    "orderType" => "consignment"
                )
            )
        )
    );
var_dump($res->orderResult->shipmentResponses->parcelInformation->parcelLabelNumber);

    file_put_contents(time().".pdf", $res->orderResult->parcellabelsPDF);
} catch (SoapFault $exception) {
    echo $exception->getMessage();
    die();
}

die();
$service_url = 'https://cloud-stage.dpd.com/api/v1/setOrder';
$ch = curl_init($service_url);



$data = array(

    'OrderAction' => 'startOrder',
    'OrderSettings' => array('ShipDate' => date("Y-m-d"), 'LabelSize' => 'PDF_A6', 'LabelStartPosition' => 'UpperLeft'),

    'OrderDataList' => array(
        array(
            'ShipAddress' => array(
                'Company' => '',
                'Salutation' => '',
                'Name' => 'Thomas Peterson',
                'Street' => 'Chausseestraße',
                'HouseNo' => '24b',
                'ZipCode' => '17506',
                'City' => 'Gribow',
                'Country' => 'DEU',
                'State' => '',
                'Phone' => '',
                'Mail' => 'info@thomas-peterson.de',
            ),
            'ParcelData' => array(
                'YourInternalID' => '123',
                'Content' => 'Printprodukte',
                'Weight' => '10',
                'Reference1' => '123234',
                'Reference2' => '',
                'ShipService' => 'Classic'
            )
        )
    )

);

$data_string = json_encode($data);

curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        'Content-Type: application/json',
        'Version: 100',
        'Language: de_DE',
        'PartnerCredentials-Name: DPD Sandbox',
        'PartnerCredentials-Token: 06445364853584D75564',
        'UserCredentials-cloudUserID: 1162',
        'UserCredentials-Token: 626C5042414F64727333',
        'Content-Length: ' . strlen($data_string))
);
$curl_response = curl_exec($ch);
$decoded = json_decode($curl_response, true);
var_dump($decoded);
file_put_contents(time().".pdf", base64_decode($decoded['LabelResponse']['LabelPDF']));
