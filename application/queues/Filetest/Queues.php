<?php

class filetest_queues {
	
	public $id = "4";
	
	public $name = "Filetest";
	
	public $information = "Filetest";
	
	public $form = 'Filetest/config/form.ini';
	
	public $title;
	public $pos;
	public $when;
	public $path;
	public $mode;
	
	public function toArray() {
		return array ('pos' => $this->pos, 'title' => $this->title, 'when' => $this->when, 'path' => $this->path, 'mode' => $this->mode );
	}
	
	public function process($queue, $filename) {
		
		if (isset ( $_GET ['progress_key'] )) {
			
			$stringCont = uploadprogress_get_contents ( $_GET ['progress_key'], 'file', 20 );
			
			if ($stringCont != false && strpos ( $stringCont, 'PDF' ) == false) {
				$arguments = array ('error' => true, 'message' => 'Kein PDF' );
				
				die( Zend_Json::encode( $arguments ));
			}
			
		} else {
			
			$handle = fopen ( $filename, "r" );
			
			$stringCont = fgets ( $handle, 8 );
			
			fclose ( $handle );
			
			if ($stringCont != false && strpos ( $stringCont, 'PDF' ) == false) {
				$arguments = array ('error' => true, 'message' => 'Kein PDF' );
				
				die ( Zend_Json::encode ( $arguments ) );
			}
		
		}
	
	}

}