<?php

class heidelpay_queues {

    public $id = "13";
    public $name = "Heidelpay";
    public $information = "Heidelpay";
    public $form = 'Heidelpay/config/form.ini';
    public $login;
    public $testmode;
    public $password;
    public $channel;
    public $sender;
    public $ids;
    public $title;
    public $pos;
    public $when;
    public $mode;

    public function toArray() {
        return array('pos' => $this->pos, 'when' => $this->when, 'mode' => $this->mode, 'title' => $this->title, 'testmode' => $this->testmode, 'channel' => $this->channel, 'sender' => $this->sender, 'login' => $this->login, 'password' => $this->password, 'ids' => $this->ids);
    }

    public function process($queue, $obj) {

        if ($obj instanceof TP_Basket) {

            if ($obj->getPaymenttype() == $this->ids) {

                $params = Zend_Registry::get('params');


                if($params['hash'] == 'Ja') {
     
                        return true;
                   
                }


                $basket = new TP_Basket( );
                $basket->setToken(TP_Util::uuid());
                $parameters = array();

                if($this->testmode) {
                    $url = "https://test-heidelpay.hpcgw.net/sgw/gtw";
                    $parameters['SECURITY.SENDER'] ="31HA07BC8124AD82A9E96D9A35FAFD2A";
                    $parameters['TRANSACTION.CHANNEL'] ="31HA07BC81A71E2A47DA94B6ADC524D8";
                    $parameters['USER.LOGIN'] ="31ha07bc8124ad82a9e96d486d19edaa";
                    $parameters['USER.PWD'] = "password";
                }else{
                    $url = "https://heidelpay.hpcgw.net/sgw/gtw";
                    $parameters['SECURITY.SENDER'] = $this->sender;
                    $parameters['TRANSACTION.CHANNEL'] = $this->channel;
                    $parameters['USER.LOGIN'] = $this->login;
                    $parameters['USER.PWD'] = $this->password;
                }
                $parameters['TRANSACTION.MODE']="INTEGRATOR_TEST";
                $parameters['REQUEST.VERSION']="1.0";
                $parameters['FRONTEND.ENABLED']="true";
                $parameters['PAYMENT.CODE']="CC.DB";
                $parameters['FRONTEND.RESPONSE_URL']='http://'.$_SERVER["SERVER_NAME"].'/service/api/heidelpay/token/' . $basket->getToken();
                $parameters['PRESENTATION.AMOUNT']= round($obj->getPreisBrutto(),2);
                $parameters['PRESENTATION.CURRENCY']='EUR';
                $parameters['FRONTEND.LANGUAGE'] = 'de';
                $parameters['IDENTIFICATION.TRANSACTIONID']='Auftrag ' . $basket->getToken();
                foreach (array_keys($parameters) AS $key) {$$key .= $parameters[$key];
                 $$key = urlencode($$key);
                 $$key .= "&";
                 $var = strtoupper($key);
                 $value = $$key;
                 $result .= "$var=$value";}
                $strPOST = stripslashes($result);
                $cpt = curl_init ();
                curl_setopt($cpt, CURLOPT_URL, $url);
                curl_setopt($cpt, CURLOPT_SSL_VERIFYHOST, 2);
                curl_setopt($cpt, CURLOPT_USERAGENT, "php ctpepost");
                curl_setopt($cpt, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($cpt, CURLOPT_SSL_VERIFYPEER, FALSE);
                curl_setopt($cpt, CURLOPT_POST, 1);
                curl_setopt($cpt, CURLOPT_POSTFIELDS, $strPOST);
                $curlresultURL = curl_exec($cpt);

                $curlerror = curl_error($cpt);
                $curlinfo = curl_getinfo($cpt);
                curl_close($cpt);

                $r_arr=explode("&",$curlresultURL);
                foreach($r_arr AS $buf)
                {
                    $temp=urldecode($buf);
                    $temp=split("=",$temp,2);
                    $postatt=$temp[0];
                    $postvar=$temp[1]; $returnvalue[$postatt]=$postvar;
                }
                $processingresult=$returnvalue['POST.VALIDATION'];
                $redirectURL=$returnvalue['FRONTEND.REDIRECT_URL'];
                
                if ($processingresult=="ACK")
                {
                    if (strstr($redirectURL,"http"))
                    {
                        header("Location: $redirectURL");
                    } else {
                        print_r($curlresultURL);
                    }
                }else {
                    print_r($curlresultURL);
                }
                die();
               
            }
        }
    }

}
