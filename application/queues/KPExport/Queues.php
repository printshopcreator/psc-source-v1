<?php

class kpexport_queues
{

    public $id = "201";

    public $name = "K&P Export Queue";

    public $information = "K&P Export Queue";

    public $install_id = 16;

    public $form = 'KPExport/config/form.ini';

    public $product_xml;
    public $title;
    public $pos;
    public $mandant;
    public $when;
    public $mode;

    public function toArray() {
        return array('pos' => $this->pos, 'when' => $this->when, 'mandant' => $this->mandant, 'mode' => $this->mode, 'title' => $this->title, 'product_xml' => $this->product_xml);
    }

    public function process($queue, $obj) {

        Zend_Registry::set('shop', $obj->Shop);
        $articles = Doctrine_Query::create()->from('Article c')->where('c.shop_id = ? AND c.to_export = 1', array(intval($obj->shop_id)))->execute();


        foreach($articles as $article) {

            $calc = simplexml_load_string($article->a1_xml);

            $params = json_decode($article->params, true);

            $grammatur = $calc->artikel[0]->xpath('//option[@id="papier"]//opt[@id="' . (string) $params['papier'] . '"]');

            $loader = new Twig_Loader_String();
            $twig = new Twig_Environment($loader);
            try{
                $template = $twig->loadTemplate($obj->xml);

                $params = array_merge($params, array(
                    'active' =>  intval($article->a5_buy)
                ,'isbn' => $article->article_nr_extern
                ,'impression_id' => $article->article_nr_intern
                ,'grammatur' => (string)$grammatur[0]['name']
                ,'preis' =>  $article->a4_abpreis
                ));

                file_put_contents(
                    APPLICATION_PATH . '/../data/customer/166/' . $this->mandant . '/export/'.date('YmdHms').$article->article_nr_intern.'.xml',
                    $template->render($params));

            }catch (Exception $e) {
                var_dump($e->getTraceAsString());
            }


            $article->to_export = false;
            $article->save();
        }
    }
}