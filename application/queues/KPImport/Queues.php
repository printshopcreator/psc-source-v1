<?php

class kpimport_queues
{

    public $id = "200";

    public $name = "K&P Import Queue";

    public $information = "K&P Import Queue";

    public $install_id = 16;

    public $form = 'KPImport/config/form.ini';

    public $product_xml;
    public $title;
    public $pos;
    public $when;
    public $mandant;
    public $mode;

    public function toArray() {
        return array('pos' => $this->pos, 'when' => $this->when, 'mandant' => $this->mandant, 'mode' => $this->mode, 'title' => $this->title, 'product_xml' => $this->product_xml);
    }

    public function process($queue, $obj) {

        Zend_Registry::set('shop', $obj->Shop);

        $iterator = new DirectoryIterator(APPLICATION_PATH . '/../data/customer/166/' . $this->mandant . '/import');
        foreach ($iterator as $fileinfo) {
            if ($fileinfo->isFile()) {
                $xml = simplexml_load_file($fileinfo->getPathname());
                foreach($xml->product as $product) {

                    $articlegroup = Doctrine_Query::create()
                        ->from('ArticleGroup a')
                        ->where('a.title = ? AND a.shop_id = ?', array((string)$product->edition_type['term'], $obj->shop_id))->fetchOne();

                    if(!$articlegroup) {
                        $articlegroup = new ArticleGroup();
                        $articlegroup->install_id = $obj->install_id;
                        $articlegroup->shop_id = $obj->shop_id;
                        $articlegroup->title = (string)$product->edition_type['term'];
                        $articlegroup->parent = 0;
                        $articlegroup->pos = 0;
                        $articlegroup->enable = true;
                        $articlegroup->language = 'all';
                        $articlegroup->save();
                    }


                    $article = Doctrine_Query::create()
                        ->from('Article a')
                        ->where('a.article_nr_intern = ? AND a.shop_id = ?', array((string)$product['impression_id'], $obj->shop_id))->fetchOne();

                    if($article) {

                        $temp = array();
                        if($article->title != (string)$product->title) {
                            $temp['title'] = $article->title;
                        }
                        if($article->einleitung != (string)$product->subtitle) {
                            $temp['einleitung'] = $article->einleitung;
                        }
                        if($article->info != (string)$product->content->text) {
                            $temp['info'] = $article->info;
                        }
                        if($article->text_format != (string)$product->format['term']) {
                            $temp['text_format'] = $article->text_format;
                        }
                        if($article->a2_count != (string)$product->pages->arabic) {
                            $temp['a2_count'] = $article->a2_count;
                        }
                        if($article->text_art != (string)$product->edition_type['term']) {
                            $temp['text_art'] =$article->text_art;
                        }
                        if(count($temp) > 0) {
                            $article->title = (string)$product->title;
                            $article->einleitung = (string)$product->subtitle;
                            $article->info = (string)$product->content->text;
                            $article->text_format = (string)$product->format['term'];
                            $article->a2_count = (string)$product->pages->arabic;
                            $article->text_art = (string)$product->edition_type['term'];
                            $article->private = false;
                            $loader = new Twig_Loader_String();
                            $twig = new Twig_Environment($loader);
                            $template = $twig->loadTemplate($obj->xml);
                            $article->a1_xml = $template->render(array(
                                'title' =>  (string)$product->title
                            , 'einleitung' => (string)$product->subtitle
                            , 'formatt' => (string)$product->format['term']
                            , 'formatt_id' => (string)$product->format
                            , 'seitenanzahl' => (string)$product->pages->arabic
                            , 'produktart' => (string)$product->edition_type['term']
                            , 'produktart_id' => (string)$product->edition_type
                            , 'article_nr_extern' => (string)$product->isbn
                            , 'article_nr_intern' => (string)$product['impression_id']
                            , 'sprache' => (string)$product->language['term']));

                            if($article->a3_special) {
                                $article->a5_buy = false;
                                $article->to_export = true;
                                $article->changes = json_encode($temp);
                            }else{
                                $article->params = "";
                            }

                            $article->save();
                        }

                    }else{

                        $article = new Article();
                        $article->install_id = $obj->install_id;
                        $article->shop_id = $obj->shop_id;
                        $article->article_nr_intern = (string)$product['impression_id'];
                        $article->article_nr_extern = (string)$product->isbn;
                        $article->title = (string)$product->title;
                        $article->einleitung = (string)$product->subtitle;
                        $article->info = (string)$product->content->text;
                        $article->typ = 6;
                        $article->mwert = 19;
                        $article->text_art = (string)$product->edition_type['term'];
                        $article->text_format = (string)$product->format['term'];
                        $article->enable = true;
                        $article->a5_buy = true;
                        $article->contact_id = 4825;
                        $article->private = false;
                        $article->a2_count = (string)$product->pages->arabic;
                        $article->a4_auflagen = 1;
                        $article->stock_count = 0;
                        $article->stock_count_min = 0;

                        $loader = new Twig_Loader_String();
                        $twig = new Twig_Environment($loader);
                        try{
                        $template = $twig->loadTemplate($obj->xml);


                        $article->a1_xml = $template->render(array(
                            'title' =>  (string)$product->title
                        , 'einleitung' => (string)$product->subtitle
                        , 'formatt' => (string)$product->format['term']
                        , 'formatt_id' => (string)$product->format
                        , 'seitenanzahl' => (string)$product->pages->arabic
                        , 'produktart' => (string)$product->edition_type['term']
                        , 'produktart_id' => (string)$product->edition_type
                        , 'article_nr_extern' => (string)$product->isbn
                        , 'article_nr_intern' => (string)$product['impression_id']
                        , 'sprache' => (string)$product->language['term']));

                        }catch (Exception $e) {
                            var_dump($e->getTraceAsString());
                        }
                        $article->save();




                    }

                    $articlegrouparticle = Doctrine_Query::create()
                        ->from('ArticleGroupArticle a')
                        ->where('a.article_id = ? AND a.articlegroup_id = ?', array($article->id, $articlegroup->id))->fetchOne();

                    if(!$articlegrouparticle) {
                        $ag = new ArticleGroupArticle();
                        $ag->articlegroup_id = $articlegroup->id;
                        $ag->article_id = $article->id;
                        $ag->save();
                    }

                }

                rename($fileinfo->getPathname(), APPLICATION_PATH . '/../data/customer/166/' . $this->mandant . '/importfinish/'.$fileinfo->getFilename());
            }
        }

    }
}
