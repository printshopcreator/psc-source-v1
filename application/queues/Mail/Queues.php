<?php

class mail_queues
{

    public $id = "1";
    public $name = "Mail";
    public $information = "Send Mails";
    public $form = 'Mail/config/form.ini';
    public $frommail;
    public $fromname;
    public $to;
    public $subject;
    public $attach;
    public $xmlfiles;
    public $jobtiket;
    public $order;
    public $offer;
    public $invoice;
    public $label;
    public $delivery;
    public $storno;
    public $language;
    public $text;
    public $texthtml;
    public $mode;
    public $pos;
    public $title;
    public $articletext;
    public $linkzip;
    public $bcc;
    public $file1;
    public $file2;
    public $shop;

    public function toArray() {
        return array(
            'attach' => $this->attach,
            'xmlfiles' => $this->xmlfiles,
            'order' => $this->order,
            'jobtiket' => $this->jobtiket,
            'offer' => $this->offer,
            'invoice' => $this->invoice,
            'label' => $this->label,
            'storno' => $this->storno,
            'delivery' => $this->delivery,
            'pos' => $this->pos,
            'articletext' => str_replace('\\', '', $this->articletext),
            'title' => $this->title,
            'when' => $this->when,
            'to' => $this->to,
            'subject' => $this->subject,
            'text' => str_replace('\\', '', $this->text),
            'texthtml' => str_replace('\\', '', $this->texthtml),
            'mode' => $this->mode,
            'fromname' => str_replace('\\', '', $this->fromname),
            'frommail' => str_replace('\\', '', $this->frommail),
            'language' => $this->language,
            'linkzip' => $this->linkzip,
            'bcc' => $this->bcc,
            'file1' => $this->file1,
            'file2' => $this->file2,
            'shop' => $this->shop);
    }

    public function process($queue, $obj) {
        if ($this->language != 'all' && $this->language != Zend_Registry::get('locale')) {
            return;
        }

        $shop = Zend_Registry::get('shop');
        $install = Zend_Registry::get('install');

        if($obj instanceof Orders) {
            $shop = $obj->Shop;
        }else{
            $shop = Doctrine_Query::create()->select()->from('Shop s')->where('s.id = ?', array($shop['id']))->fetchOne();
        }


        $archiveLink = "";

        if ($this->linkzip == true) {
            if (!file_exists(PUBLIC_PATH . '/shops/' . $shop['uid'] . '/job')) {
                if (!file_exists(PUBLIC_PATH . '/shops/' . $shop['uid'])) {
                    mkdir(PUBLIC_PATH . '/shops/' . $shop['uid']);
                }
                mkdir(PUBLIC_PATH . '/shops/' . $shop['uid'] . '/job');
            }

            $archiveLink = '/shops/' . $shop['uid'] . '/job/' . time() . '.zip';
            $archive = new ZipArchive();
            $archive->open(PUBLIC_PATH . $archiveLink, ZIPARCHIVE::CREATE);
        }

        $templates = array();
        $twig = new \Twig\Environment(new \Twig\Loader\ArrayLoader($templates));

        $template = $twig->createTemplate(str_replace('\\', '', $this->text));

        $templatehtml = $twig->createTemplate(str_replace('\\', '', $this->texthtml));
        $templateSubj = $twig->createTemplate(str_replace('\\', '', $this->subject));

        $templateTo = $twig->createTemplate(str_replace('\\', '', $this->to));
        $templateFromName = $twig->createTemplate(str_replace('\\', '', $this->fromname));
        $templateFromMail = $twig->createTemplate(str_replace('\\', '', $this->frommail));
        $templateBcc = $twig->createTemplate(str_replace('\\', '', $this->bcc));

        $files = array();

        if ($this->file1 != "") {
            $file1 = Doctrine_Query::create()
                ->from('Image as i')
                ->where('i.id = ?')->fetchOne(array($this->file1));
            if ($file1) {
                $att = new Zend_Mime_Part(file_get_contents($file1->path));
                $att->filename = $file1->name;
                $att->disposition = Zend_Mime::DISPOSITION_ATTACHMENT;
                $att->encoding = Zend_Mime::ENCODING_BASE64;
                array_push($files, $att);
            }
        }

        if ($this->file2 != "") {
            $file2 = Doctrine_Query::create()
                ->from('Image as i')
                ->where('i.id = ?')->fetchOne(array($this->file2));
            if ($file2) {
                $att = new Zend_Mime_Part(file_get_contents($file2->path));
                $att->filename = $file2->name;
                $att->disposition = Zend_Mime::DISPOSITION_ATTACHMENT;
                $att->encoding = Zend_Mime::ENCODING_BASE64;
                array_push($files, $att);
            }
        }

        if ($this->when == 'contactreset') {
            $contact = Doctrine_Query::create()->from('Contact m')->leftJoin('m.Shops as s')->where('s.id = ? AND m.self_email = ? AND m.enable = 1', array(
                $shop['id'], $obj))->fetchOne();
            if ($contact == false) {
                Zend_Registry::set('queueerror', false);
                return;
            }
            $contact->passwordresetdate = date('Y-m-d H:i:s');
            $contact->passwordresetid = TP_Util::uuid();

            if ($this->articletext == "1" && $install['user_passwordreset_mode'] == false) {
                $Buchstaben = array("a", "b", "c", "d", "e", "f", "g", "h", "k", "m", "n", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z");
                $Zahlen = array("2", "3", "4", "5", "6", "7", "8", "9");

                $Laenge = 6;

                for ($i = 0, $Passwort = ""; strlen($Passwort) < $Laenge; $i++) {
                    if (rand(0, 2) == 0 && isset($Buchstaben)) {
                        $Passwort .= $Buchstaben[rand(0, (count($Buchstaben) - 1))];
                    } elseif (rand(0, 2) == 1 && isset($Zahlen)) {
                        $Passwort .= $Zahlen[rand(0, (count($Zahlen) - 1))];
                    }
                }

                $contact->password = $Passwort;
            }

            $contact->save();
            $obj = $contact;
            Zend_Registry::set('queueerror', true);
        }
        if ($this->when == 'orderstatuschange' || $this->when == 'orderposstatuschange') {

            if (!in_array($obj->status, explode(',', $this->articletext))) {
                return;
            }
        }

        if ($this->when == "feedbackform") {
            if ($this->articletext != "" && $this->articletext != $obj['page']) {
                return;
            }
        }

        if ($this->when == "tellafriend") {
            if ((isset($obj['page']) && $this->articletext != $obj['page']) || (!isset($obj['page']) && $this->articletext != "")) {
                return;
            }
            if (isset($obj['article_id']) && $obj['article_id'] != "" && $obj['article_id'] != "undefined") {

                $article = Doctrine_Query::create()->from('Article c')->where('c.url = ? AND c.shop_id = ?', array(
                    $obj['article_id'], $shop->id))->fetchOne();

                $content = $template->render(array(
                    'data' => $obj
                , 'link' => 'http://' . $shop->Domain[0]->name . '/article/show/' . $obj['article_id']
                , 'shop' => $shop, 'domain' => $shop->Domain[0]->name
                , 'article' => $article));
                $contenthtml = $templatehtml->render(array(
                    'data' => $obj
                , 'link' => 'http://' . $shop->Domain[0]->name . '/article/show/' . $obj['article_id']
                , 'shop' => $shop, 'domain' => $shop->Domain[0]->name
                , 'article' => $article));
                $subject = $templateSubj->render(array(
                    'data' => $obj
                , 'link' => 'http://' . $shop->Domain[0]->name . '/article/show/' . $obj['article_id']
                , 'shop' => $shop, 'domain' => $shop->Domain[0]->name
                , 'article' => $article));

                $to = $templateTo->render(array(
                    'data' => $obj
                , 'link' => 'http://' . $shop->Domain[0]->name . '/article/show/' . $obj['article_id']
                , 'shop' => $shop, 'domain' => $shop->Domain[0]->name
                , 'article' => $article));
                $fromName = $templateFromName->render(array(
                    'data' => $obj
                , 'link' => 'http://' . $shop->Domain[0]->name . '/article/show/' . $obj['article_id']
                , 'shop' => $shop, 'domain' => $shop->Domain[0]->name
                , 'article' => $article));
                $fromEmail = $templateFromMail->render(array(
                    'data' => $obj
                , 'link' => 'http://' . $shop->Domain[0]->name . '/article/show/' . $obj['article_id']
                , 'shop' => $shop, 'domain' => $shop->Domain[0]->name
                , 'article' => $article));

                $bcc = $templateBcc->render(array(
                    'data' => $obj
                , 'link' => 'http://' . $shop->Domain[0]->name . '/article/show/' . $obj['article_id']
                , 'shop' => $shop, 'domain' => $shop->Domain[0]->name
                , 'article' => $article));

                TP_Mail::sendTextMail('queue', $content, $contenthtml, $subject, $to, $fromName, $fromEmail, $files, $bcc);

            } else {

                $content = $template->render(array(
                    'data' => $obj
                , 'link' => 'http://' . $shop->Domain[0]->name
                , 'shop' => $shop, 'domain' => $shop->Domain[0]->name));
                $contenthtml = $templatehtml->render(array(
                    'data' => $obj
                , 'link' => 'http://' . $shop->Domain[0]->name
                , 'shop' => $shop, 'domain' => $shop->Domain[0]->name));
                $subject = $templateSubj->render(array(
                    'data' => $obj
                , 'link' => 'http://' . $shop->Domain[0]->name
                , 'shop' => $shop, 'domain' => $shop->Domain[0]->name));

                $to = $templateTo->render(array(
                    'data' => $obj
                , 'link' => 'http://' . $shop->Domain[0]->name
                , 'shop' => $shop, 'domain' => $shop->Domain[0]->name));
                $fromName = $templateFromName->render(array(
                    'data' => $obj
                , 'link' => 'http://' . $shop->Domain[0]->name
                , 'shop' => $shop, 'domain' => $shop->Domain[0]->name));
                $fromEmail = $templateFromMail->render(array(
                    'data' => $obj
                , 'link' => 'http://' . $shop->Domain[0]->name
                , 'shop' => $shop, 'domain' => $shop->Domain[0]->name));
                $bcc = $templateBcc->render(array(
                    'data' => $obj
                , 'link' => 'http://' . $shop->Domain[0]->name
                , 'shop' => $shop, 'domain' => $shop->Domain[0]->name));

                TP_Mail::sendTextMail('queue', $content, $contenthtml, $subject, $to, $fromName, $fromEmail, $files, $bcc);
            }
            return;
        }

        if ($this->when == "orderpos_approval_send") {

            $pos = $obj['pos'];
            $contact = $obj['contact'];
            $article = $obj['article'];

            $content = $template->render(array(
                'pos' => $pos,
                'order' => $pos->Orders
            , 'contact' => $contact
            , 'shop' => $shop, 'domain' => $shop->Domain[0]->name
            , 'article' => $article));
            $contenthtml = $templatehtml->render(array(
                'pos' => $pos,
                'order' => $pos->Orders
            , 'contact' => $contact
            , 'shop' => $shop, 'domain' => $shop->Domain[0]->name
            , 'article' => $article));
            $subject = $templateSubj->render(array(
                'pos' => $pos,
                'order' => $pos->Orders
            , 'shop' => $shop, 'domain' => $shop->Domain[0]->name
            , 'contact' => $contact
            , 'article' => $article));

            $fromName = $templateFromName->render(array(
                'pos' => $pos,
                'order' => $pos->Orders
            , 'shop' => $shop, 'domain' => $shop->Domain[0]->name
            , 'contact' => $contact
            , 'article' => $article));
            $fromEmail = $templateFromMail->render(array(
                'pos' => $pos,
                'order' => $pos->Orders
            , 'shop' => $shop, 'domain' => $shop->Domain[0]->name
            , 'contact' => $contact
            , 'article' => $article));


            if ($this->to != '') {
                TP_Mail::sendTextMail('queue', $content, $contenthtml, $subject, $this->to, $fromName, $fromEmail, $files);
            } else {
                TP_Mail::sendTextMail('queue', $content, $contenthtml, $subject, $contact->self_email, $fromName, $fromEmail, $files);
            }
            return;
        }

        if ($this->when == "orderpos_approval_accepted" || $this->when == "orderpos_approval_rejected") {

            $pos = $obj['orderspos'];
            $approval = $obj['approval'];

            $content = $template->render(array(
                'pos' => $pos,
                'order' => $pos->Orders,
                'contact' => $pos->Orders->Contact,
                'article' => $pos->Article,
                'approval' => $approval
            , 'shop' => $shop, 'domain' => $shop->Domain[0]->name));
            $contenthtml = $templatehtml->render(array(
                'pos' => $pos,
                'order' => $pos->Orders,
                'contact' => $pos->Orders->Contact,
                'article' => $pos->Article,
                'approval' => $approval
            , 'shop' => $shop, 'domain' => $shop->Domain[0]->name));
            $subject = $templateSubj->render(array(
                'pos' => $pos,
                'order' => $pos->Orders,
                'contact' => $pos->Orders->Contact,
                'article' => $pos->Article,
                'approval' => $approval
            , 'shop' => $shop, 'domain' => $shop->Domain[0]->name));

            $this->to = $templateTo->render(array(
                'pos' => $pos,
                'order' => $pos->Orders,
                'contact' => $pos->Orders->Contact,
                'article' => $pos->Article,
                'approval' => $approval
            , 'shop' => $shop, 'domain' => $shop->Domain[0]->name));

            $fromName = $templateFromName->render(array(
                'pos' => $pos,
                'order' => $pos->Orders,
                'contact' => $pos->Orders->Contact,
                'article' => $pos->Article,
                'approval' => $approval
            , 'shop' => $shop, 'domain' => $shop->Domain[0]->name));

            $fromEmail = $templateFromMail->render(array(
                'pos' => $pos,
                'order' => $pos->Orders,
                'contact' => $pos->Orders->Contact,
                'article' => $pos->Article,
                'approval' => $approval
            , 'shop' => $shop, 'domain' => $shop->Domain[0]->name));

            if ($this->to != '') {
                TP_Mail::sendTextMail('queue', $content, $contenthtml, $subject, $this->to, $fromName, $fromEmail, $files);
            }

            return;
        }

        if ($obj instanceof Contact) {

            $password = '';
            if(Zend_Registry::isRegistered('password')) {
                $password = Zend_Registry::get('password');
            }

            $content = $template->render(array(
                'contact' => $obj,
                'password' => $password
            , 'shop' => $shop, 'domain' => $shop->Domain[0]->name));
            $contenthtml = $templatehtml->render(array(
                'contact' => $obj,
                'password' => $password
            , 'shop' => $shop, 'domain' => $shop->Domain[0]->name));
            $subject = $templateSubj->render(array(
                'contact' => $obj,
                'password' => $password
            , 'shop' => $shop, 'domain' => $shop->Domain[0]->name));

            $this->fromname = $templateFromName->render(array(
                'contact' => $obj,
                'password' => $password
            , 'shop' => $shop, 'domain' => $shop->Domain[0]->name));

            $this->frommail = $templateFromMail->render(array(
                'contact' => $obj,
                'password' => $password
            , 'shop' => $shop, 'domain' => $shop->Domain[0]->name));

            $this->bcc = $templateBcc->render(array(
                'contact' => $obj,
                'password' => $password
            , 'shop' => $shop, 'domain' => $shop->Domain[0]->name));

            $this->to = $templateTo->render(array(
                'contact' => $obj,
                'password' => $password
            , 'shop' => $shop, 'domain' => $shop->Domain[0]->name));
        }
        if ($obj instanceof Article) {
            $image = null;
            if($obj->file != "") {
                require_once(__DIR__ ."/../../helpers/Image.php");
                $helper = new TP_View_Helper_Image();

                $image = $helper->thumbnailImage($obj->title, 'articlesinglegreater', $obj->file, true);
            }


            $content = $template->render(array(
                'article' => $obj
            , 'shop' => $shop, 'domain' => $shop->Domain[0]->name));

            $this->to = $templateTo->render(array(
                'article' => $obj
            , 'shop' => $shop, 'domain' => $shop->Domain[0]->name));

            $contenthtml = $templatehtml->render(array(
                'article' => $obj,
                'article_image_1' => $image,
                'shop' => $shop,
                'domain' => $shop->Domain[0]->name));
            $subject = $templateSubj->render(array(
                'article' => $obj
            , 'shop' => $shop, 'domain' => $shop->Domain[0]->name));

            $this->fromname = $templateFromName->render(array(
                'article' => $obj
            , 'shop' => $shop, 'domain' => $shop->Domain[0]->name));

            $this->frommail = $templateFromMail->render(array(
                'article' => $obj
            , 'shop' => $shop, 'domain' => $shop->Domain[0]->name));

            $this->bcc = $templateBcc->render(array(
                'article' => $obj
            , 'shop' => $shop, 'domain' => $shop->Domain[0]->name));
        }
        if ($obj instanceof Shop) {

            $content = $template->render(array(
                'contact' => $obj->Creator
            , 'shop' => $obj, 'domain' => $obj->Domain[0]->name));
            $contenthtml = $templatehtml->render(array(
                'contact' => $obj->Creator
            , 'shop' => $obj, 'domain' => $obj->Domain[0]->name));
            $subject = $templateSubj->render(array(
                'contact' => $obj->Creator
            , 'shop' => $obj, 'domain' => $obj->Domain[0]->name));

            if ($this->to != '') {
                TP_Mail::sendTextMail('queue', $content, $contenthtml, $subject, $this->to, $this->fromname, $this->frommail);
            } else {
                TP_Mail::sendTextMail('queue', $content, $contenthtml, $subject, $obj->Creator->self_email, $this->fromname, $this->frommail);
            }
            return;
        }
        if ($obj instanceof Orderspos) {

            $product = $obj;
            $obj = $obj->Orders;

            $articles = "";

            $cur = new Zend_Currency(Zend_Registry::get('locale')->getLanguage() . '_' . Zend_Registry::get('locale')->getRegion());

            $articlesTemp = array();

            $posnetto = 0;
            $possteuer = 0;
            $posbrutto = 0;

            $mwertalle = array();
            $versand = 0;
            $versandMwert = 0;
            $summewarenwert = array();

            $pro = unserialize($product->data);
            $proa = $pro->getArticle();
            $proo = $pro->getOptions();
            $proaa = $proa;
            $proa['name'] = $proa['title'];

            if ($this->linkzip == true) {
                foreach ($product->Upload as $file) {
                    if (file_exists('uploads/' . $shop['uid'] . '/article/' . $file['path'])) {
                        $pi = pathinfo(PUBLIC_PATH . '/uploads/' . $shop['uid'] . '/article/' . $file['path']);
                        $archive->addFile(PUBLIC_PATH . '/uploads/' . $shop['uid'] . '/article/' . $file['path'], $pi['basename']);
                    } elseif (file_exists($file['path'])) {
                        $pi = pathinfo(PUBLIC_PATH . '/' . $file['path']);
                        $archive->addFile(PUBLIC_PATH . '/' . $file['path'], $pi['basename']);
                    }
                }
            }

            if ($this->attach == true) {
                foreach ($product->Upload as $file) {
                    if (file_exists('uploads/' . $shop['uid'] . '/article/' . $file['path'])) {
                        $att = new Zend_Mime_Part(file_get_contents('uploads/' . $shop['uid'] . '/article/' . $file['path']));
                        $att->filename = $file['name'];
                        $att->disposition = Zend_Mime::DISPOSITION_ATTACHMENT;
                        $att->encoding = Zend_Mime::ENCODING_BASE64;
                        if(filesize('uploads/' . $shop['uid'] . '/article/' . $file['path']) < 20000000) {
                            array_push($files, $att);
                        }
                    } elseif (file_exists($file['path'])) {
                        $att = new Zend_Mime_Part(file_get_contents($file['path']));
                        $att->filename = $file['name'];
                        $att->disposition = Zend_Mime::DISPOSITION_ATTACHMENT;
                        $att->encoding = Zend_Mime::ENCODING_BASE64;
                        if(filesize($file['path']) < 20000000) {
                            array_push($files, $att);
                        }
                    }
                }
            }
            if ($this->xmlfiles == true) {
                try {
                    if (Zend_Registry::isRegistered('xmlexport')) {
                        $xmlexport = Zend_Registry::get('xmlexport');

                        foreach ($xmlexport as $export) {
                            $att = new Zend_Mime_Part(file_get_contents($export['value']));
                            $att->filename = $export['name'];
                            $att->type = Zend_Mime::TYPE_TEXT;
                            $att->disposition = Zend_Mime::DISPOSITION_ATTACHMENT;
                            array_push($files, $att);
                        }
                    }
                } catch (Exception $e) {

                }
            }
            $options = array();
            if (is_array($proo)) {
                foreach ($proo as $key => $value) {
                    array_push($options, $key . ': ' . $value);
                }
            }

            $count = $product->count;

            if (isset($proo['auflage'])) {
                $count = $proo['auflage'];
            }

            $article = Doctrine_Query::create()->from('Article c')->where('c.id = ?', array(
                intval($product->article_id)))->fetchOne();

            $posnetto = $posnetto + $product->priceall;
            $possteuer = $possteuer + $product->priceallsteuer;
            $posbrutto = $posbrutto + $product->priceallbrutto;

            require_once(APPLICATION_PATH . '/helpers/Article.php');
            $helper = new TP_View_Helper_Article();

            $articlesTemp[] = array(
                'article' => $article,
                'helper' => $helper,
                'count' => $count,
                'pos' => $product->pos,
                'delivery_date' => $product->delivery_date,
                'options' => $article->getOptArray($product->data),
                'articlegroup' => $article->getArticlegroupAsRootline(),
                'price' => array(
                    'netto' => $cur->toCurrency(1 * $product->priceone),
                    'steuer' => $cur->toCurrency(1 * $product->priceonesteuer),
                    'brutto' => $cur->toCurrency(1 * $product->priceonebrutto),
                    'allnetto' => $cur->toCurrency(1 * $product->priceall),
                    'allsteuer' => $cur->toCurrency(1 * $product->priceallsteuer),
                    'allbrutto' => $cur->toCurrency(1 * $product->priceallbrutto),
                )
            );

            if (!isset($mwertalle[$article->mwert])) {
                $mwertalle[$article->mwert] = $product->priceallsteuer;
            } else {
                $mwertalle[$article->mwert] = $mwertalle[$article->mwert] + $product->priceallsteuer;
            }
            $summewarenwert[$article->mwert] = $summewarenwert[$article->mwert] + $product->priceallbrutto;
            $versandMwert = $versandMwert + $product->priceallsteuer;
            if ($article->versand == 'Fix') {
                $versand = $versand + $article->versandwert;
            } elseif ($article->versand == 'Position') {

            }

            if ($this->order == true) {
                $file = TP_Util::writeReport($obj->id, 'order');
                if ($file != false) {

                    $att = new Zend_Mime_Part(file_get_contents($file));
                    $att->filename = $obj->alias . '_order.pdf';
                    $att->disposition = Zend_Mime::DISPOSITION_INLINE;
                    $att->type = 'application/pdf';
                    $att->encoding = Zend_Mime::ENCODING_BASE64;
                    array_push($files, $att);

                    if ($this->linkzip == true) {
                        $pi = pathinfo($file);
                        $archive->addFile($file, $pi['basename']);
                    }
                }
            }
            if ($this->invoice == true) {
                $file = TP_Util::writeReport($obj->id, 'invoice');
                if ($file != false) {
                    $att = new Zend_Mime_Part(file_get_contents($file));
                    $att->filename = $obj->alias . '_invoice.pdf';
                    $att->disposition = Zend_Mime::DISPOSITION_ATTACHMENT;
                    $att->encoding = Zend_Mime::ENCODING_BASE64;
                    array_push($files, $att);

                    if ($this->linkzip == true) {
                        $pi = pathinfo($file);
                        $archive->addFile($file, $pi['basename']);
                    }
                }
            }
            if ($this->offer == true) {
                $file = TP_Util::writeReport($obj->id, 'offer');
                if ($file != false) {
                    $att = new Zend_Mime_Part(file_get_contents($file));
                    $att->filename = $obj->alias . '_offer.pdf';
                    $att->disposition = Zend_Mime::DISPOSITION_ATTACHMENT;
                    $att->encoding = Zend_Mime::ENCODING_BASE64;
                    array_push($files, $att);

                    if ($this->linkzip == true) {
                        $pi = pathinfo($file);
                        $archive->addFile($file, $pi['basename']);
                    }
                }
            }
            if ($this->delivery == true) {
                $file = TP_Util::writeReport($obj->id, 'delivery', $product);
                if ($file != false) {
                    $att = new Zend_Mime_Part(file_get_contents($file));
                    $att->filename = $obj->alias . '_delivery.pdf';
                    $att->disposition = Zend_Mime::DISPOSITION_ATTACHMENT;
                    $att->encoding = Zend_Mime::ENCODING_BASE64;
                    array_push($files, $att);

                    if ($this->linkzip == true) {
                        $pi = pathinfo($file);
                        $archive->addFile($file, $pi['basename']);
                    }
                }
            }
            if ($this->jobtiket == true) {
                $file = TP_Util::writeReport($obj->id, 'jobtiket', $product);
                if ($file != false) {
                    $att = new Zend_Mime_Part(file_get_contents($file));
                    $att->filename = $obj->alias . '_jobtiket.pdf';
                    $att->disposition = Zend_Mime::DISPOSITION_ATTACHMENT;
                    $att->encoding = Zend_Mime::ENCODING_BASE64;
                    array_push($files, $att);

                    if ($this->linkzip == true) {
                        $pi = pathinfo($file);
                        $archive->addFile($file, $pi['basename']);
                    }
                }
            }
            if ($this->label == true) {
                $file = TP_Util::writeReport($obj->id, 'label', $product);
                if ($file != false) {
                    $att = new Zend_Mime_Part(file_get_contents($file));
                    $att->filename = $obj->alias . '_label.pdf';
                    $att->disposition = Zend_Mime::DISPOSITION_ATTACHMENT;
                    $att->encoding = Zend_Mime::ENCODING_BASE64;
                    array_push($files, $att);

                    if ($this->linkzip == true) {
                        $pi = pathinfo($file);
                        $archive->addFile($file, $pi['basename']);
                    }
                }
            }
            if ($this->storno == true) {
                $file = TP_Util::writeReport($obj->id, 'storno', $product);
                if ($file != false) {
                    $att = new Zend_Mime_Part(file_get_contents($file));
                    $att->filename = $obj->alias . '_storno.pdf';
                    $att->disposition = Zend_Mime::DISPOSITION_ATTACHMENT;
                    $att->encoding = Zend_Mime::ENCODING_BASE64;
                    array_push($files, $att);

                    if ($this->linkzip == true) {
                        $pi = pathinfo($file);
                        $archive->addFile($file, $pi['basename']);
                    }
                }
            }

            if ($this->linkzip == true) {
                $archive->close();
            }

            $liefer = new ContactAddress();

            if (!$obj->delivery_same) {

                $liefer = Doctrine_Query::create()
                    ->from('ContactAddress a')
                    ->where('a.id = ?')->fetchOne(array(intval($obj->delivery_address)));
            }

            $versand = $versand + $obj->Shippingtype->kosten_fix;
            $mwertShipping = str_replace('%', '', $obj->Shippingtype->mwert);

            $mwertalle[$mwertShipping] = $mwertalle[$mwertShipping] + ($versand / 100 * $mwertShipping);

            $paymentmwert = $obj->preissteuer - $possteuer - ($versand / 100 * $mwertShipping);

            $mwertalle[19] = $mwertalle[19] + $paymentmwert;

            $tempMwertalle = array();
            foreach ($mwertalle as $key => $value) {
                $tempMwertalle[$key] = $cur->toCurrency(1 * $value);
            }

            $nettogutschein = 0;
            if ($obj->gutscheinabzug != "" && $obj->gutscheinabzug > 0) {
                $tempMwert = array();
                $versandMwert = 0;
                foreach ($mwertalle as $key => $mw) {
                    $anteil = (((($summewarenwert[$key] / $posbrutto) * $obj->gutscheinabzug) * 100) / (100 + $key)) * ($key * 0.01);
                    $tempMwert[$key] = $tempMwert[$key] + ($mw - $anteil);
                    $nettogutschein += (((($summewarenwert[$key] / $posbrutto) * $obj->gutscheinabzug) * 100) / (100 + $key));
                    $versandMwert += ($mw - $anteil);
                }

                $params['gutscheinname'] = Doctrine_Query::create()->from('CreditSystemDetail c')->where('c.uuid = ?', array($obj->gutschein))->fetchOne()->CreditSystem->title;

            }

            $invoiceAddress = Doctrine_Query::create()->select()
                ->from('ContactAddress o')->where('o.id = ?', array($obj->invoice_address))->fetchOne();

            if ($invoiceAddress) {
                if ($obj->delivery_address != 0) {

                    $deliveryAddress = Doctrine_Query::create()->select()
                        ->from('ContactAddress o')->where('o.id = ?', array($obj->delivery_address))->fetchOne();

                } else {
                    $deliveryAddress = $invoiceAddress;
                }

                if ($obj->sender_address != 0) {

                    $senderAddress = Doctrine_Query::create()->select()
                        ->from('ContactAddress o')->where('o.id = ?', array($obj->sender_address))->fetchOne();

                } else {
                    $senderAddress = $invoiceAddress;
                }
            } else {
                $invoiceAddress = new ContactAddress();
                $senderAddress = new ContactAddress();
                $deliveryAddress = new ContactAddress();
            }

            $translate = Zend_Registry::get('translate');

            $st = $translate->translate($obj->status);
            if ($st == $obj->status) {
                $st = $translate->translate('status' . $obj->status);
            }

            $templateVars = array(
                'order' => $obj,
                'status' => $st,
                'contact' => $obj->Contact,
                'gutschein' => array(
                    'code' => $obj->gutschein,
                    'abzug' => $cur->toCurrency(1 * $obj->gutscheinabzug),
                    'abzugnetto' => $cur->toCurrency(1 * $nettogutschein)
                ),
                'liefer' => $liefer,

                'invoiceAddress' => $invoiceAddress,
                'deliveryAddress' => $deliveryAddress,
                'senderAddress' => $senderAddress,

                'deliversame' => $obj->delivery_same,
                'positions' => $articlesTemp,
                'mwert' => $tempMwertalle,
                'link' => $archiveLink,
                'payment' => array('id' => $obj->Paymenttype->id, 'title' => $obj->Paymenttype->title, 'price' => $cur->toCurrency(1 * $obj->zahlkosten)),
                'shipping' => array('title' => $obj->Shippingtype->id, 'title' => $obj->Shippingtype->title, 'price' => $cur->toCurrency(1 * $obj->versandkosten)),
                'price' => array(
                    'netto' => $cur->toCurrency((1 * $obj->preisbrutto) - (1 * $obj->preissteuer)),
                    'brutto' => $cur->toCurrency(1 * $obj->preisbrutto),
                    'steuer' => $cur->toCurrency(1 * $obj->preissteuer),
                    'posnetto' => $cur->toCurrency(1 * $posnetto),
                    'posbrutto' => $cur->toCurrency(1 * $posbrutto),
                    'possteuer' => $cur->toCurrency(1 * $possteuer)),
                'shop' => $shop, 'domain' => $shop->Domain[0]->name);

            $content = $template->render($templateVars);
            $contenthtml = $templatehtml->render($templateVars);
            $subject = $templateSubj->render($templateVars);
            $fromName = $templateFromName->render($templateVars);
            $fromEmail = $templateFromMail->render($templateVars);
            $bcc = $templateBcc->render($templateVars);
            $to = $templateTo->render($templateVars);

            if ($to != '') {
                TP_Mail::sendTextMail('queue', $content, $contenthtml, $subject, $to, $fromName, $fromEmail, $files, $bcc);
            } else {
                TP_Mail::sendTextMail('queue', $content, $contenthtml, $subject, $obj->Contact->self_email, $fromName, $fromEmail, $files, $bcc);
            }
            return;

        }
        if ($obj instanceof Orders) {

            $articles = "";

            $cur = new Zend_Currency(Zend_Registry::get('locale')->getLanguage() . '_' . Zend_Registry::get('locale')->getRegion());

            $shop = Doctrine_Query::create()->select()->from('Shop s')->where('s.id = ?', array($obj['shop_id']))->fetchOne();

            $articlesTemp = array();

            $posnetto = 0;
            $possteuer = 0;
            $posbrutto = 0;

            $mwertalle = array();
            $versand = 0;
            $versandMwert = 0;
            $summewarenwert = array();

            $smithAndNephew = false;

            foreach ($obj->Orderspos as $product) {
                $pro = unserialize($product->data);
                $proa = $pro->getArticle();
                $proo = $pro->getOptions();
                $proaa = $proa;
                $proa['name'] = $proa['title'];

                if ($this->linkzip == true) {
                    foreach ($product->Upload as $file) {
                        if (file_exists('uploads/' . $shop['uid'] . '/article/' . $file['path'])) {
                            $pi = pathinfo(PUBLIC_PATH . '/uploads/' . $shop['uid'] . '/article/' . $file['path']);
                            $archive->addFile(PUBLIC_PATH . '/uploads/' . $shop['uid'] . '/article/' . $file['path'], $pi['basename']);
                        } elseif (file_exists($file['path'])) {
                            $pi = pathinfo(PUBLIC_PATH . '/' . $file['path']);
                            $archive->addFile(PUBLIC_PATH . '/' . $file['path'], $pi['basename']);
                        }
                    }
                }

                if ($this->attach == true) {
                    foreach ($product->Upload as $file) {
                        if (file_exists('uploads/' . $shop['uid'] . '/article/' . $file['path'])) {
                            $att = new Zend_Mime_Part(file_get_contents('uploads/' . $shop['uid'] . '/article/' . $file['path']));
                            $att->filename = $file['name'];
                            $att->disposition = Zend_Mime::DISPOSITION_ATTACHMENT;
                            $att->encoding = Zend_Mime::ENCODING_BASE64;
                            if(filesize('uploads/' . $shop['uid'] . '/article/' . $file['path']) < 20000000) {
                                array_push($files, $att);
                            }
                        } elseif (file_exists($file['path'])) {
                            $att = new Zend_Mime_Part(file_get_contents($file['path']));
                            $att->filename = $file['name'];
                            $att->disposition = Zend_Mime::DISPOSITION_ATTACHMENT;
                            $att->encoding = Zend_Mime::ENCODING_BASE64;
                            if(filesize($file['path']) < 20000000) {
                                array_push($files, $att);
                            }
                        }
                    }
                }

                $options = array();
                if (is_array($proo)) {
                    foreach ($proo as $key => $value) {
                        array_push($options, $key . ': ' . $value);
                    }
                }

                $count = $product->count;

                if (isset($proo['auflage'])) {
                    $count = $proo['auflage'];
                }

                $article = Doctrine_Query::create()->from('Article c')->where('c.id = ?', array(
                    intval($product->article_id)))->fetchOne();

                if($product->article_id == 36907) {
                    $smithAndNephew = true;
                }

                $posnetto = $posnetto + $product->priceall;
                $possteuer = $possteuer + $product->priceallsteuer;
                $posbrutto = $posbrutto + $product->priceallbrutto;

                require_once(APPLICATION_PATH . '/helpers/Article.php');
                $helper = new TP_View_Helper_Article();

                $steplayouter = array();

                if($product->a9_designer_data != "") {
                    try{
                        $stepl = json_decode($product->a9_designer_data, true);

                        if(isset($stepl['jsonSaveData']['foto-back_1'])) {
                            $fotoback1 = Doctrine_Query::create()->from('Motiv c')->where('c.uuid = ?', array(
                                $stepl['jsonSaveData']['foto-back_1']))->fetchOne();
                            if($fotoback1) {
                                $steplayouter['foto-back_1'] = $fotoback1->toArray();
                            }
                            $fotoback2 = Doctrine_Query::create()->from('Motiv c')->where('c.uuid = ?', array(
                                $stepl['jsonSaveData']['foto-back_2']))->fetchOne();
                            if($fotoback1) {
                                $steplayouter['foto-back_2'] = $fotoback2->toArray();
                            }
                            $fotoback3 = Doctrine_Query::create()->from('Motiv c')->where('c.uuid = ?', array(
                                $stepl['jsonSaveData']['foto-back_3']))->fetchOne();
                            if($fotoback1) {
                                $steplayouter['foto-back_3'] = $fotoback3->toArray();
                            }
                            $fotoback4 = Doctrine_Query::create()->from('Motiv c')->where('c.uuid = ?', array(
                                $stepl['jsonSaveData']['foto-back_4']))->fetchOne();
                            if($fotoback1) {
                                $steplayouter['foto-back_4'] = $fotoback4->toArray();
                            }

                        }

                    }catch(Exception $e) {

                    }
                }

                $articlesTemp[] = array(
                    'article' => $article,
                    'helper' => $helper,
                    'pos' => $product->pos,
                    'weight' => $product->weight,
                    'count' => $count,
                    'steplayouter' => $steplayouter,
                    'ref'   => $product->ref,
                    'kst'   => $product->kst,
                    'basketposfield1'   => $product->ref,
                    'basketposfield2'   => $product->kst,
                    'options' => $article->getOptArray($product->data),
                    'articlegroup' => $article->getArticlegroupAsRootline(),
                    'price' => array(
                        'netto' => $cur->toCurrency(1 * $product->priceone),
                        'steuer' => $cur->toCurrency(1 * $product->priceonesteuer),
                        'brutto' => $cur->toCurrency(1 * $product->priceonebrutto),
                        'allnetto' => $cur->toCurrency(1 * $product->priceall),
                        'allsteuer' => $cur->toCurrency(1 * $product->priceallsteuer),
                        'allbrutto' => $cur->toCurrency(1 * $product->priceallbrutto),
                    )
                );

                if (!isset($mwertalle[$article->mwert])) {
                    $mwertalle[$article->mwert] = $product->priceallsteuer;
                } else {
                    $mwertalle[$article->mwert] = $mwertalle[$article->mwert] + $product->priceallsteuer;
                }
                $summewarenwert[$article->mwert] = $summewarenwert[$article->mwert] + $product->priceallbrutto;
                $versandMwert = $versandMwert + $product->priceallsteuer;
                if ($article->versand == 'Fix') {
                    $versand = $versand + $article->versandwert;
                } elseif ($article->versand == 'Position') {

                }

            }

            if ($this->order == true) {
                $file = TP_Util::writeReport($obj->id, 'order');
                if ($file != false) {

                    $att = new Zend_Mime_Part(file_get_contents($file));
                    $att->filename = $obj->alias . '_order.pdf';
                    $att->disposition = Zend_Mime::DISPOSITION_INLINE;
                    $att->type = 'application/pdf';
                    $att->encoding = Zend_Mime::ENCODING_BASE64;
                    array_push($files, $att);

                    if ($this->linkzip == true) {
                        $pi = pathinfo($file);
                        $archive->addFile($file, $pi['basename']);
                    }
                }
            }
            if ($this->invoice == true) {
                $file = TP_Util::writeReport($obj->id, 'invoice');
                if ($file != false) {
                    $att = new Zend_Mime_Part(file_get_contents($file));
                    $att->filename = $obj->alias . '_invoice.pdf';
                    $att->disposition = Zend_Mime::DISPOSITION_ATTACHMENT;
                    $att->encoding = Zend_Mime::ENCODING_BASE64;
                    array_push($files, $att);

                    if ($this->linkzip == true) {
                        $pi = pathinfo($file);
                        $archive->addFile($file, $pi['basename']);
                    }
                }
            }
            if ($this->offer == true) {
                $file = TP_Util::writeReport($obj->id, 'offer');
                if ($file != false && ($shop['id'] != 160 || $smithAndNephew)) {
                    $att = new Zend_Mime_Part(file_get_contents($file));
                    $att->filename = $obj->alias . '_offer.pdf';
                    $att->disposition = Zend_Mime::DISPOSITION_ATTACHMENT;
                    $att->encoding = Zend_Mime::ENCODING_BASE64;
                    array_push($files, $att);

                    if ($this->linkzip == true) {
                        $pi = pathinfo($file);
                        $archive->addFile($file, $pi['basename']);
                    }
                }
            }
            if ($this->delivery == true) {
                $file = TP_Util::writeReport($obj->id, 'delivery');
                if ($file != false) {
                    $att = new Zend_Mime_Part(file_get_contents($file));
                    $att->filename = $obj->alias . '_delivery.pdf';
                    $att->disposition = Zend_Mime::DISPOSITION_ATTACHMENT;
                    $att->encoding = Zend_Mime::ENCODING_BASE64;
                    array_push($files, $att);

                    if ($this->linkzip == true) {
                        $pi = pathinfo($file);
                        $archive->addFile($file, $pi['basename']);
                    }
                }
            }
            if ($this->jobtiket == true) {
                $file = TP_Util::writeReport($obj->id, 'jobtiket');
                if ($file != false) {
                    $att = new Zend_Mime_Part(file_get_contents($file));
                    $att->filename = $obj->alias . '_jobtiket.pdf';
                    $att->disposition = Zend_Mime::DISPOSITION_ATTACHMENT;
                    $att->encoding = Zend_Mime::ENCODING_BASE64;
                    array_push($files, $att);

                    if ($this->linkzip == true) {
                        $pi = pathinfo($file);
                        $archive->addFile($file, $pi['basename']);
                    }
                }
            }
            if ($this->label == true) {
                $file = TP_Util::writeReport($obj->id, 'label');
                if ($file != false) {
                    $att = new Zend_Mime_Part(file_get_contents($file));
                    $att->filename = $obj->alias . '_label.pdf';
                    $att->disposition = Zend_Mime::DISPOSITION_ATTACHMENT;
                    $att->encoding = Zend_Mime::ENCODING_BASE64;
                    array_push($files, $att);

                    if ($this->linkzip == true) {
                        $pi = pathinfo($file);
                        $archive->addFile($file, $pi['basename']);
                    }
                }
            }
            if ($this->storno == true) {
                $file = TP_Util::writeReport($obj->id, 'storno');
                if ($file != false) {
                    $att = new Zend_Mime_Part(file_get_contents($file));
                    $att->filename = $obj->alias . '_storno.pdf';
                    $att->disposition = Zend_Mime::DISPOSITION_ATTACHMENT;
                    $att->encoding = Zend_Mime::ENCODING_BASE64;
                    array_push($files, $att);

                    if ($this->linkzip == true) {
                        $pi = pathinfo($file);
                        $archive->addFile($file, $pi['basename']);
                    }
                }
            }

            if ($this->xmlfiles == true) {
                try {
                    if (Zend_Registry::isRegistered('xmlexport')) {
                        $xmlexport = Zend_Registry::get('xmlexport');

                        foreach ($xmlexport as $export) {
                            if(strpos($export['value'], "pdf") || strpos($export['value'], "gif")) {
                                $att = new Zend_Mime_Part(file_get_contents($export['value']));
                                $att->filename = $export['name'];
                                $att->encoding = Zend_Mime::ENCODING_BASE64;
                                $att->disposition = Zend_Mime::DISPOSITION_ATTACHMENT;
                                array_push($files, $att);
                            }else {
                                $att = new Zend_Mime_Part(file_get_contents($export['value']));
                                $att->filename = $export['name'];
                                $att->type = Zend_Mime::TYPE_TEXT;
                                $att->disposition = Zend_Mime::DISPOSITION_ATTACHMENT;
                                array_push($files, $att);
                            }
                        }
                    }
                } catch (Exception $e) {

                }
            }

            if ($this->linkzip == true) {
                $archive->close();
            }

            $liefer = new ContactAddress();

            if (!$obj->delivery_same) {

                $liefer = Doctrine_Query::create()
                    ->from('ContactAddress a')
                    ->where('a.id = ?')->fetchOne(array(intval($obj->delivery_address)));
            }

            $versand = $versand + $obj->Shippingtype->kosten_fix;
            $mwertShipping = str_replace('%', '', $obj->Shippingtype->mwert);

            $mwertalle[$mwertShipping] = $mwertalle[$mwertShipping] + ($versand / 100 * $mwertShipping);

            $paymentmwert = $obj->preissteuer - $possteuer - ($versand / 100 * $mwertShipping);

            $mwertalle[19] = $mwertalle[19] + $paymentmwert;

            $tempMwertalle = array();
            foreach ($mwertalle as $key => $value) {
                if ($value > 0) {
                    $tempMwertalle[$key] = $cur->toCurrency(1 * $value);
                }
            }

            $nettogutschein = 0;
            if ($obj->gutscheinabzug != "" && $obj->gutscheinabzug > 0) {
                $tempMwert = array();
                $versandMwert = 0;
                foreach ($mwertalle as $key => $mw) {
                    $anteil = (((($summewarenwert[$key] / $posbrutto) * $obj->gutscheinabzug) * 100) / (100 + $key)) * ($key * 0.01);
                    $tempMwert[$key] = $tempMwert[$key] + ($mw - $anteil);
                    $nettogutschein += (((($summewarenwert[$key] / $posbrutto) * $obj->gutscheinabzug) * 100) / (100 + $key));
                    $versandMwert += ($mw - $anteil);
                }

                $params['gutscheinname'] = Doctrine_Query::create()->from('CreditSystemDetail c')->where('c.uuid = ?', array($obj->gutschein))->fetchOne()->CreditSystem->title;

            }

            $invoiceAddress = Doctrine_Query::create()->select()
                ->from('ContactAddress o')->where('o.id = ?', array($obj->invoice_address))->fetchOne();

            if ($invoiceAddress) {
                if ($obj->delivery_address != 0) {

                    $deliveryAddress = Doctrine_Query::create()->select()
                        ->from('ContactAddress o')->where('o.id = ?', array($obj->delivery_address))->fetchOne();

                } else {
                    $deliveryAddress = $invoiceAddress;
                }

                if ($obj->sender_address != 0) {

                    $senderAddress = Doctrine_Query::create()->select()
                        ->from('ContactAddress o')->where('o.id = ?', array($obj->sender_address))->fetchOne();

                } else {
                    $senderAddress = $invoiceAddress;
                }
            } else {
                $invoiceAddress = new ContactAddress();
                $senderAddress = new ContactAddress();
                $deliveryAddress = new ContactAddress();
            }

            $translate = Zend_Registry::get('translate');

            $st = $translate->translate($obj->status);
            if ($st == $obj->status) {
                $st = $translate->translate('status' . $obj->status);
            }

            $templateVars = array(
                'order' => $obj,
                'status' => $st,
                'contact' => $obj->Contact,
                'gutschein' => array(
                    'code' => $obj->gutschein,
                    'abzug' => $cur->toCurrency(1 * $obj->gutscheinabzug),
                    'abzugnetto' => $cur->toCurrency(1 * $nettogutschein)
                ),
                'liefer' => $liefer,

                'invoiceAddress' => $invoiceAddress,
                'deliveryAddress' => $deliveryAddress,
                'senderAddress' => $senderAddress,

                'deliversame' => $obj->delivery_same,
                'positions' => $articlesTemp,
                'mwert' => $tempMwertalle,
                'link' => $archiveLink,
                'payment' => array('id' => $obj->Paymenttype->id, 'title' => $obj->Paymenttype->title, 'price' => $cur->toCurrency(1 * $obj->zahlkosten)),
                'shipping' => array('id' => $obj->Shippingtype->id, 'title' => $obj->Shippingtype->title, 'price' => $cur->toCurrency(1 * $obj->versandkosten)),
                'price' => array(
                    'netto' => $cur->toCurrency((1 * $obj->preisbrutto) - (1 * $obj->preissteuer)),
                    'brutto' => $cur->toCurrency(1 * $obj->preisbrutto),
                    'steuer' => $cur->toCurrency(1 * $obj->preissteuer),
                    'posnetto' => $cur->toCurrency(1 * $posnetto),
                    'posbrutto' => $cur->toCurrency(1 * $posbrutto),
                    'possteuer' => $cur->toCurrency(1 * $possteuer)),
                'shop' => $shop, 'domain' => $shop->Domain[0]->name);

            $content = $template->render($templateVars);
            $contenthtml = $templatehtml->render($templateVars);
            $subject = $templateSubj->render($templateVars);
            $fromName = $templateFromName->render($templateVars);
            $fromEmail = $templateFromMail->render($templateVars);
            $bcc = $templateBcc->render($templateVars);
            $to = $templateTo->render($templateVars);

            if ($to != '') {
                TP_Mail::sendTextMail('queue', $content, $contenthtml, $subject, $to, $fromName, $fromEmail, $files, $bcc);
            } else {
                TP_Mail::sendTextMail('queue', $content, $contenthtml, $subject, $obj->Contact->self_email, $fromName, $fromEmail, $files, $bcc);
            }
            return;
        }
        if ($obj instanceof TP_Basket) {

            $cur = new Zend_Currency(Zend_Registry::get('locale')->getLanguage() . '_' . Zend_Registry::get('locale')->getRegion());

            $templateVars = array(
                'contact' => $obj->getOfferContact(),
                'shop' => $shop,
                'domain' => $shop->Domain[0]->name,
                'basket' => $obj,
                'positions' => array(),
                'weight' => 0,
                'price' => array(
                    'versandNetto' => $cur->toCurrency(1 * $obj->getVersandkosten()),
                    'versandBrutto' => $cur->toCurrency(1 * $obj->getVersandBrutto()),
                    'netto' => $cur->toCurrency(1 * $obj->getPreisNetto()),
                    'brutto' => $cur->toCurrency(1 * $obj->getPreisBrutto()),
                    'steuer' => $cur->toCurrency(1 * $obj->getPreisSteuer()),
                    'gesamtNetto' => $cur->toCurrency(1 * ($obj->getVersandkosten() + $obj->getPreisNetto()))
                )
            );

            /**
             * @var String $uuid
             * @var TP_Basket_Item $art
             */
            foreach($obj->getAllArtikel() as $uuid => $art) {
                $article = Doctrine_Query::create()->from('Article c')->where('c.uuid = ?', array(
                    $art->getArticleId()))->fetchOne();

                $options = array();

                if ($article->a6_org_article != 0) {
                    $options = $article->OrgArticle->getOptArray($art, false);
                    $arrayOptions = $article->getOptArray($art, false, true);
                } else {
                    $options = $article->getOptArray($art, false);
                    $arrayOptions = $article->getOptArray($art, false, true);
                }
                $templateVars['weight'] += $art->getWeight();
                $gewichtpos = $art->getWeight();

                array_push($templateVars['positions'], array(
                    'uuid' => $uuid,
                    'article' => $article,
                    'basketarticle' => $art,
                    'options' => $options,
                    'arrayOptions' => $arrayOptions,
                    'weight' => $gewichtpos,
                    'price' => array(
                        'netto' => $cur->toCurrency(1 * $art->getNetto()),
                        'steuer' => $cur->toCurrency(1 * $art->getSteuer()),
                        'brutto' => $cur->toCurrency(1 * $art->getBrutto())
                    )));

            }


            $content = $template->render($templateVars);
            $contenthtml = $templatehtml->render($templateVars);
            $subject = $templateSubj->render($templateVars);
            $fromName = $templateFromName->render($templateVars);
            $fromEmail = $templateFromMail->render($templateVars);
            $bcc = $templateBcc->render($templateVars);
            $to = $templateTo->render($templateVars);


            if ($to != '') {
                TP_Mail::sendTextMail('queue', $content, $contenthtml, $subject, $to, $fromName, $fromEmail, $files, $bcc);
            } else {
                TP_Mail::sendTextMail('queue', $content, $contenthtml, $subject, $obj->getOfferContact()['email'], $fromName, $fromEmail, $files, $bcc);
            }
            return;
        }
        if (is_array($obj)) {

            $files = array();

            if ($this->when == 'cmspost' || $this->when == 'overviewpost') {

                if ($this->articletext != "" && !in_array($obj['page'], explode(',', $this->articletext))) {
                    return;
                }

                $filess = array();
                $files = new Zend_Session_Namespace('Basket');
                if (isset($files->files[$obj['page']])) {
                    foreach ($files->files[$obj['page']] as $key => $row) {
                        $att = new Zend_Mime_Part(file_get_contents('uploads/' . $shop['uid'] . '/pages/' . $row['value']));
                        $att->filename = $row['name'];
                        $att->disposition = Zend_Mime::DISPOSITION_ATTACHMENT;
                        $att->encoding = Zend_Mime::ENCODING_BASE64;
                        array_push($filess, $att);
                    }
                }
            }

            $content = $template->render(array(
                'rows' => $obj, 'files' => $files, 'shop' => $shop, 'domain' => $shop->Domain[0]->name));
            $contenthtml = $templatehtml->render(array(
                'rows' => $obj, 'files' => $files, 'shop' => $shop, 'domain' => $shop->Domain[0]->name));
            $subject = $templateSubj->render(array(
                'rows' => $obj, 'files' => $files, 'shop' => $shop, 'domain' => $shop->Domain[0]->name));

            $this->to = $templateTo->render(array(
                'rows' => $obj, 'files' => $files, 'shop' => $shop, 'domain' => $shop->Domain[0]->name));

            $this->fromname = $templateFromName->render(array(
                'rows' => $obj, 'files' => $files, 'shop' => $shop, 'domain' => $shop->Domain[0]->name));

            $this->frommail = $templateFromMail->render(array(
                'rows' => $obj, 'files' => $files, 'shop' => $shop, 'domain' => $shop->Domain[0]->name));

            $this->bcc = $templateBcc->render(array(
                'rows' => $obj, 'files' => $files, 'shop' => $shop, 'domain' => $shop->Domain[0]->name));


            if ($this->when == 'contactpost') {

                if ($this->to == "") {
                    $this->to = $shop->betreiber_email;
                }

                $this->fromname = $obj['name'];
                $this->frommail = $obj['email'];

            }

            if ($this->to != '') {
                TP_Mail::sendTextMail('queue', $content, $contenthtml, $subject, $this->to, $this->fromname, $this->frommail, $filess, $this->bcc);
            } else {
                TP_Mail::sendTextMail('queue', $content, $contenthtml, $subject, $obj->self_email, $this->fromname, $this->frommail, $filess, $this->bcc);
            }
            return;
        }

        if ($this->to != '') {
            TP_Mail::sendTextMail('queue', $content, $contenthtml, $subject, $this->to, $this->fromname, $this->frommail, array(), $this->bcc);
        } else {
            TP_Mail::sendTextMail('queue', $content, $contenthtml, $subject, $obj->self_email, $this->fromname, $this->frommail, array(), $this->bcc);
        }
    }

}
