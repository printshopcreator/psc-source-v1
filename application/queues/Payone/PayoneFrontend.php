<?php



class financegateFrontend {

    var $frontend_url, $secret;
    var $protected_keys = array(	"aid","portalid","mode","request","clearingtype","reference","customerid","param",
        "narrative_text","successurl","errorurl","backurl",
        "encoding","display_name","display_address","autosubmit","targetwindow",
        "amount","currency","invoiceid","invoiceappendix","id","pr","no","de","ti","va",
        "productid","accessname","accesscode",
        "access_expiretime","access_canceltime","access_starttime","access_period","access_aboperiod",
        "access_price","access_aboprice","access_vat",
        "settleperiod","settletime","vaccountname","vreference");

    protected $hashParameters = array(
        'mid',
        'amount',
        'productid',
        'aid',
        'currency',
        'accessname',
        'portalid',
        'due_time',
        'accesscode',
        'mode',
        'storecarddata',
        'access_expiretime',
        'request',
        'checktype',
        'access_canceltime',
        'responsetype',
        'addresschecktype',
        'access_starttime',
        'reference',
        'consumerscoretype',
        'access_period',
        'userid',
        'invoiceid',
        'access_aboperiod',
        'customerid',
        'invoiceappendix',
        'access_price',
        'param',
        'invoice_deliverymode',
        'access_aboprice',
        'narrative_text',
        'eci',
        'access_vat',
        'successurl',
        'settleperiod',
        'errorurl',
        'settletime',
        'backurl',
        'vaccountname',
        'exiturl',
        'vreference',
        'clearingtype',
        'encoding',
        // @todo Item parameters not supported
        // 'id[x]', 'pr[x]', 'no[x]', 'de[x]', 'ti[x]', 'va[x]',
        // 'id', 'pr', 'no', 'de', 'ti', 'va',
    );

    function setFrontendUrl($url){
        $this->frontend_url=$url;
    }

    function setSecret($secret){
        $this->secret=$secret;
    }


    function financegateFrontend(){

    }

    function generateHash($data){

        $requestData = $data;

        sort($this->hashParameters);

        $hashString = '';
        foreach ($this->hashParameters as $key) {
            if (!array_key_exists($key, $requestData)) {
                continue;
            }
            $hashString .= $requestData[$key];
        }
        $hashString .= $this->secret;
        $hash = md5($hashString);
        return $hash;
    }

    function generateUrlByArray($request_array){

        $request_url = "";

        if(!$request_array['aid']
            || !$request_array['portalid']
            || !$this->frontend_url
            || !$this->secret){
            $output['errormessage']="Payone Frontend Setup Data not complete (Frontend-URL, AId, PortalId, Key)";
            return $output;
        }

        foreach($request_array as $key => $val) {
            if(is_array($val)) foreach($val as $i => $val1) {
                $request_url.="&".$key."[".$i."]=".urlencode($val1);
            }
            else {
                $request_url.="&".$key."=".urlencode($val);
            }
        }
        $request_url=$this->frontend_url."?".substr($request_url,1);

        $hash=$this->generateHash($request_array);

        $request_url.="&hash=".$hash;

        $output['redirecturl']=$request_url;

        return $output;

    }

}