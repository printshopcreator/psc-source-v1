<?php

require_once __DIR__ . '/PayoneFrontend.php';

class payone_queues {

    public $id = "20";
    public $name = "Payone";
    public $information = "Payone";
    public $form = 'Payone/config/form.ini';
    public $portalid;
    public $key;
    public $aid;
    public $mid;
    public $paypal;
    public $url;
    public $ids;
    public $title;
    public $pos;
    public $when;
    public $mode;

    public $test_mode;
    
    public function toArray() {
        return array('pos' => $this->pos, 'pnt' => $this->pnt, 'gpy' => $this->gpy, 'paypal' => $this->paypal, 'test_mode' => $this->test_mode, 'when' => $this->when, 'mode' => $this->mode, 'title' => $this->title, 'portalid' => $this->portalid, 'aid' => $this->aid, 'mid' => $this->mid, 'key' => $this->key, 'url' => $this->url, 'ids' => $this->ids);
    }

    public function process($queue, $obj) {

        if ($obj instanceof TP_Basket) {

            if ($obj->getPaymenttype() == $this->ids) {

                $params = Zend_Registry::get('params');

                if($params['hash'] == 'Ja' || (round($obj->getPreisBrutto(), 2)) <= 0) {
                    return true;

                    die();



                }

                $user = Zend_Auth::getInstance()->getIdentity();

                $basket = new TP_Basket( );
                $basket->setToken(substr(md5(TP_Util::uuid()),0,15));

                $frontend= new financegateFrontend();
                $frontend->setFrontendURL($this->url);
                $frontend->setSecret($this->key);

                $country = trim($basket->getIban());
                $country = strtoupper(substr($country, 0, 2));
                if(!$country) {
                    $country = 'DE';
                }
                // Weiterleitung an FinanceGate Frontend
                $data = array(
                    'portalid' => $this->portalid,
                    'aid' => $this->aid,
                    'mid' => $this->mid,
                    'responsetype' => 'REDIRECT',
                    'wallettype' => 'PPE',
                    'country' => $country,
                    'lastname' => $user['self_lastname'],
                    'firstname' => $user['self_firstname'],
                    'request' => 'authorization',
                    'clearingtype' => 'wlt',
                    'currency' => 'EUR',
                    'mode' => 'live',
                    'successurl' => ('http://' . $_SERVER["SERVER_NAME"] . '/basket/finish/hash/Ja/token=' . $basket->getToken()),
                    'errorurl' => ('http://' . $_SERVER["SERVER_NAME"] . '/basket/finish/error/Fehler mit der Bezahlung'),
                    'backurl' => ('http://' . $_SERVER["SERVER_NAME"] . '/basket'),
                    'reference' => $basket->getPaymentRef(),
                    'amount' => round($obj->getPreisBrutto(), 2) * 100,
                    'narrative_text' => $basket->getPaymentRef()
                );


                if($this->gpy) {
                    $data['bic'] = $basket->getBic();
                    $data['iban'] = $basket->getIban();
                    $data['bankcountry'] = $country;
                    $data['clearingtype'] = 'sb';
                    $data['onlinebanktransfertype'] = 'GPY';
                }

                if($this->pnt) {
                    $data['bic'] = $basket->getBic();
                    $data['iban'] = $basket->getIban();
                    $data['bankcountry'] = $country;
                    $data['clearingtype'] = 'sb';
                    $data['onlinebanktransfertype'] = 'PNT';
                }


                if(!$this->test_mode) {
                    $data['mode'] = 'live';
                }

                $aRedirectURL = $frontend->generateUrlByArray($data);

                header("Location: " . $aRedirectURL['redirecturl']);
                die();

            }
        }
    }

}
