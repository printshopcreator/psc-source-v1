<?php

class paypal_queues {

    public $id = "7";

    public $name = "Paypal";

    public $information = "PayPal Schnittstelle";

    public $form = 'Paypal/config/form.ini';

    public $api_username;
    public $test;
    public $api_password;
    public $api_signature;
    public $ids;
    public $title;
    public $pos;
    public $when;
    public $mode;


    public function toArray() {
        return array('pos' => $this->pos, 'when' => $this->when, 'mode' => $this->mode, 'title' => $this->title, 'api_signature' => $this->api_signature, 'api_username' => $this->api_username, 'api_password' => $this->api_password, 'test' => $this->test, 'ids' => $this->ids);
    }


    public function process($queue, $obj) {

        if ( $obj instanceof TP_Basket ) {

            if($obj->getPaymenttype() == $this->ids) {

                if($obj->getToken() == "") {
                    $obj->setToken(TP_Util::uuid());
                }

                if((round($obj->getPreisBrutto(), 2)) <= 0) {
                    return true;

                    die();

                }
                $params = Zend_Registry::get('params');

                $user = Zend_Auth::getInstance()->getIdentity();
                $user = Doctrine_Query::create()
                    ->from('Contact m')
                    ->where('id = ?')->fetchOne(array($user['id']));

                if(isset($params['paymentRef'])) {
                    $obj->setPaymentRef($params['paymentRef']);
                }

                if(isset($params['paymentId'])) {
                    $shop = Zend_Registry::get('shop');
                    $shop = Doctrine_Query::create()->from('Shop c')->where('c.id = ?', array(
                        $shop['id']))->fetchOne();

                    header('Location: /apps/payment/do/'.$shop->getApiKey().'/paypal?&hash='.urlencode($obj->getToken()).'&paymentId='.$params['paymentId'].'&PayerID='.$params['PayerID'].'&amount='.round($obj->getPreisBrutto(),2));
                    die();
                }

                if((!isset($params['hash']) || 'Ja' != $params['hash']) && !isset($params['token'])) {
                    $shop = Zend_Registry::get('shop');
                    $shop = Doctrine_Query::create()->from('Shop c')->where('c.id = ?', array(
                        $shop['id']))->fetchOne();

                    header('Location: /apps/payment/pay/'.$shop->getApiKey().'/paypal?&hash='.urlencode($obj->getToken()).'&amount='.round($obj->getPreisBrutto(),2));

                    die();
                }
            }

        }

    }
}
