<?php

class paypalold_queues {

    public $id = "71";

    public $name = "Paypal Alte Version";

    public $information = "PayPal Schnittstelle";

    public $form = 'PaypalOld/config/form.ini';

    public $api_username;
    public $test;
    public $api_password;
    public $api_signature;
    public $ids;
    public $title;
    public $pos;
    public $when;
    public $mode;


    public function toArray() {
        return array('pos' => $this->pos, 'when' => $this->when, 'mode' => $this->mode, 'title' => $this->title, 'api_signature' => $this->api_signature, 'api_username' => $this->api_username, 'api_password' => $this->api_password, 'test' => $this->test, 'ids' => $this->ids);
    }


    public function process($queue, $obj) {

        if ( $obj instanceof TP_Basket ) {

            if($obj->getPaymenttype() == $this->ids) {

                $params = Zend_Registry::get('params');

                $serverName = 'http://' . $_SERVER["SERVER_NAME"];
                if ((isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on')) {
                    $serverName = 'https://' . $_SERVER["SERVER_NAME"];
                }



                if((round($obj->getPreisBrutto(), 2)) <= 0) {
                    return true;

                    die();

                }

                $user = Zend_Auth::getInstance()->getIdentity();
                $user = Doctrine_Query::create()
                    ->from('Contact m')
                    ->where('id = ?')->fetchOne(array($user['id']));


                $authinfo = new Payment_PayPal_Data_AuthInfo($this->api_username, $this->api_password, $this->api_signature);
                $config = array();
                if($this->test == true) {
                    $config['endpoint'] = 'https://api-3t.sandbox.paypal.com/nvp';
                }

                $paypal = new Payment_PayPal_Nvp($authinfo, $config);

                if (isset($_GET['error'])) {
                    header('Location: '.$serverName.'/basket/finish');
                    die();
                }


                if (isset($_GET['token'])) {
                    $coDetails = $paypal->getExpressCheckoutDetails($_GET['token']);
                    if ($coDetails->isSuccess() && ($payerId = $coDetails->getValue('PAYERID'))) {
                        $response = $paypal->doExpressCheckoutPayment($_GET['token'], $coDetails->getValue('PAYERID'), round($obj->getPreisBrutto(),2));
                        return;
                    }
                    return;
                }


                $params = array('NOSHIPPING' => 1,
                    'LOCALECODE' => 'DE',
                    'CURRENCYCODE' => 'EUR',
                    'COUNTRY' => 'DE',
                    'L_NAME1' => 'Auftragsabrechnung',
                    'L_NUMBER1' => $obj->getPaymentRef(),
                    'L_DESC1' => '',
                    'L_AMT1' => round($obj->getPreisBrutto(),2));

                $response = $paypal->setExpressCheckout(round($obj->getPreisBrutto(),2),
                    $serverName.'/basket/finish?status=ok',
                    $serverName.'/basket/finish?error=Paypal Abbruch', $params);


                if ($response->isSuccess() && ($token = $response->getValue('TOKEN'))) {
                    $basket = new TP_Basket( );
                    $basket->setToken($token);
                    if($this->test == true) {
                        header("Location: https://www.sandbox.paypal.com/cgi-bin/webscr?cmd=_express-checkout&token=$token");
                    }else{
                        header("Location: https://www.paypal.com/cgi-bin/webscr?cmd=_express-checkout&token=$token");
                    }
                    die();
                } else {
                    // Error

                    header("Location: ".$serverName."/basket/finish?error=".$response->getValue("L_LONGMESSAGE0"). ' '. $response->getValue('L_ERRORCODE0'));
                    die();

                }
            }

        }

    }
}
