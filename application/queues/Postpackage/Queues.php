<?php

class postpackage_queues
{

    public $id = "24";

    public $name = "Post Paketnummer";

    public $form = 'Postpackage/config/form.ini';

    public $title;
    public $pos;
    public $when;
    public $mode;

    public function toArray() {
        return array('pos' => $this->pos, 'when' => $this->when, 'mode' => $this->mode, 'title' => $this->title);
    }

    public function process($queue, $obj)
    {

        $shop = Zend_Registry::get('shop');

        if ($obj instanceof Orders) {

            $m = TP_Mongo::getInstance();
            $request =  $m->Parcelnumber->find( array('shippingId' => $obj->shippingtype_id, 'used' => false) );
            $request->sort( array( 'created' => 1 ) );
            $request->limit(1);
            $request->next();
            $parcelnumber = $request->current();

            if($parcelnumber) {

                $parcelnumberArr = str_split($parcelnumber['number'], 1);

                $sum = $parcelnumberArr[2] * 8;
                $sum += $parcelnumberArr[3] * 6;
                $sum += $parcelnumberArr[4] * 4;
                $sum += $parcelnumberArr[5] * 2;
                $sum += $parcelnumberArr[6] * 3;
                $sum += $parcelnumberArr[7] * 5;
                $sum += $parcelnumberArr[8] * 9;
                $sum += $parcelnumberArr[9] * 7;

                $rest = $sum % 11;
                $checksum = 11 - $rest;

                $obj->package = $parcelnumber['number'] .trim($checksum);
                $obj->save();

                $parcelnumber['used'] = true;
                $m->Parcelnumber->save($parcelnumber);
            }
        }
    }


}