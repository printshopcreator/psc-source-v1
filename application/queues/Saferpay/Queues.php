<?php

class saferpay_queues
{

    public $id = "8";
    public $name = "Saferpay";
    public $information = "Saferpay";
    public $form = 'Saferpay/config/form.ini';
    public $userid;
    public $projectid;
    public $password;
    public $url;
    public $ids;
    public $title;
    public $pos;
    public $when;
    public $mode;
    public $paymentMethods;

    public function toArray() {
        return array('paymentMethods' => $this->paymentMethods, 'pos' => $this->pos, 'when' => $this->when, 'mode' => $this->mode, 'title' => $this->title, 'projectid' => $this->projectid, 'userid' => $this->userid, 'password' => $this->password, 'url' => $this->url, 'ids' => $this->ids);
    }

    public function process($queue, $obj) {

        if ($obj instanceof TP_Basket) {

            if ($obj->getPaymenttype() == $this->ids) {

                $params = Zend_Registry::get('params');

                if (isset($params['hash']) && $params['hash'] == 'Ja') {

                    $attributes = array(
                        'DATA' => $params['DATA'],
                        'SIGNATURE' => $params['SIGNATURE']
                    );

                    $strAttributesArray = array();
                    foreach ($attributes as $k => $v) {
                        $strAttributesArray[] = $k . '=' . rawurlencode($v);
                    }
                    $strAttributes = implode('&', $strAttributesArray);

                    /* get the PayInit URL from the hosting server */

                    //CURL Session initialisieren
                    $ch = curl_init('https://www.saferpay.com/hosting/VerifyPayConfirm.asp?' . $strAttributes);
                    curl_setopt($ch, CURLOPT_PORT, 443);
                    // Prüfung des SSL-Zertifikats abschalten (SSL ist dennoch sicher)
                    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
                    //Session Optionen setzen
                    // kein Header in der Ausgabe
                    curl_setopt($ch, CURLOPT_HEADER, 0);
                    // Rückgabe schalten
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
                    //Ausführen der Aktionen
                    $payinit_url = curl_exec($ch);
                    //Session beenden
                    curl_close($ch);

                    $result = explode(':', $payinit_url);

                    if ($result[0] == 'OK') {

                        return true;
                    } else {
                        header('Location: http://' . $_SERVER["SERVER_NAME"] . '/basket/finish?error=Saferpay Error');
                    }

                    die();

                }

                $basket = new TP_Basket();
                $basket->setToken(TP_Util::uuid());

                $shop = Zend_Registry::get('shop');

                $attributes = array(
                    'AMOUNT' => round($obj->getPreisBrutto(), 2) * 100,
                    'CURRENCY' => 'EUR',
                    'DESCRIPTION' => 'OnlineShop',
                    'ALLOWCOLLECT' => 'no',
                    'ORDERID' => $basket->getToken(),
                    'DELIVERY' => 'no',
                    'ACCOUNTID' => $this->projectid, //  "99867-94913822"
                    'BACKLINK' => 'http://' . $_SERVER["SERVER_NAME"] . '/basket/finish',
                    'FAILLINK' => 'http://' . $_SERVER["SERVER_NAME"] . '/basket/finish?error=Saferpay Error',
                    'SUCCESSLINK' => 'http://' . $_SERVER["SERVER_NAME"] . '/basket/finish?hash=Ja'
                );

                if($this->paymentMethods != "") {
                    $attributes['PAYMENTMETHODS'] = $this->paymentMethods;
                }

                $strAttributesArray = array();
                foreach ($attributes as $k => $v) {
                    $strAttributesArray[] = $k . '=' . rawurlencode($v);
                }
                $strAttributes = implode('&', $strAttributesArray);

                /* get the PayInit URL from the hosting server */

                //CURL Session initialisieren
                $ch = curl_init('https://www.saferpay.com/hosting/CreatePayInit.asp?' . $strAttributes);
                curl_setopt($ch, CURLOPT_PORT, 443);
                // Prüfung des SSL-Zertifikats abschalten (SSL ist dennoch sicher)
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
                //Session Optionen setzen
                // kein Header in der Ausgabe
                curl_setopt($ch, CURLOPT_HEADER, 0);
                // Rückgabe schalten
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
                //Ausführen der Aktionen
                $payinit_url = curl_exec($ch);

                //Session beenden
                curl_close($ch);
          
                header("Location: $payinit_url");
                die();
            }
        }
    }

}
