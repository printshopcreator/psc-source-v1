<?php

class sofortold_queues {

    public $id = "31";

    public $name = "Sofort Alte Version";

    public $information = "Sofortüberweisungsschnittstelle";

    public $form = 'SofortOld/config/form.ini';

    public $userid;
    public $projectid;
    public $password;
    public $url;
    public $ids;
    public $title;
    public $pos;
    public $when;
    public $mode;


    public function toArray() {
        return array('pos' => $this->pos, 'when' => $this->when, 'mode' => $this->mode, 'title' => $this->title, 'projectid' => $this->projectid, 'userid' => $this->userid, 'password' => $this->password, 'url' => $this->url, 'ids' => $this->ids);
    }


    public function process($queue, $obj) {

        if ( $obj instanceof TP_Basket ) {

            if($obj->getPaymenttype() == $this->ids) {

                $obj->setToken(TP_Util::uuid());

                $params = Zend_Registry::get('params');

                $user = Zend_Auth::getInstance()->getIdentity();
                $user = Doctrine_Query::create()
                    ->from('Contact m')
                    ->where('id = ?')->fetchOne(array($user['id']));

                $data = array(
                    $this->userid, // user_id
                    $this->projectid, // project_id
                    '', // sender_holder
                    '', // sender_account_number
                    '', // sender_bank_code
                    '', // sender_country_id
                    round($obj->getPreisBrutto(),2), // amount
                    'EUR', // currency_id
                    'Onlinebestellung', // reason_1
                    '', // reason_2
                    urlencode($user->self_firstname), // user_variable_0
                    urlencode($user->self_lastname), // user_variable_1
                    urlencode($obj->getToken()), // user_variable_2
                    urlencode($_SERVER["SERVER_NAME"]), // user_variable_3
                    '', // user_variable_4
                    '', // user_variable_5
                    $this->password // project_password
                );

                $data_implode = implode('|', $data);

                $hash = md5($data_implode);




                if((!isset($params['hash']) || 'Ja' != $params['hash']) && !isset($params['token'])) {
                    header('Location: ' .$this->url . '?currency_id=EUR&user_variable_0='.urlencode($user->self_firstname).'&user_variable_1='.urlencode($user->self_lastname).'&user_variable_2='.urlencode($obj->getToken()).'&user_variable_3='.urlencode($_SERVER["SERVER_NAME"]).'&reason_1=Onlinebestellung&user_id='.$this->userid.'&project_id='.$this->projectid.'&amount='.round($obj->getPreisBrutto(),2).'&hash='.$hash);
                    die();
                }
            }

        }

    }
}
