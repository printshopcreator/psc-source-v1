<?php

class stockcopy_queues
{

    public $id = "34";

    public $name = "Stockcopy";

    public $information = "";

    public $form = 'Stockcopy/config/form.ini';

    public $template_xml;

    public $title;
    public $pos;
    public $when;
    public $mode;

    public function toArray() {
        return array('pos' => $this->pos, 'when' => $this->when, 'mode' => $this->mode, 'title' => $this->title, 'template_xml' => $this->template_xml);
    }

    public function process($queue, $obj)
    {

        $shop = Zend_Registry::get('shop');


        if ($obj instanceof Orders) {

            $positionObjs = $obj->Orderspos;

            foreach($positionObjs as $pos) {
                $article = Doctrine_Query::create()
                    ->from('Article c')
                    ->where('c.id = ?', array($pos->article_id))
                    ->fetchOne();
                if(!$article) {
                    continue;
                }

                $params = unserialize($pos->data);
                if($params instanceof TP_Basket_Item) {
                    $special = $params->getSpecial();
                    $params = $params->getOptions();
                }

                $auflage = $params['auflage'];
                if(isset($params['calc_base'])) {
                    $calc_base = $params['calc_base'];

                    $doc = new DOMDOcument;
                    $doc->loadxml($article->a1_xml);

                    $xpath = new DOMXpath($doc);

                    $copy = true;

                    foreach ($xpath->query('//option[@id="calc_base"]') as $option) {

                        if ($option->getAttribute('type') == 'Hidden') {
                            $copy = false;
                        }

                        $option->setAttribute('type', 'Hidden');
                        $option->setAttribute('default', $calc_base);

                        foreach ($xpath->query('//option[@id="calc_base"]//opt') as $node) {
                            $node->parentNode->removeChild($node);
                        }
                    }

                    foreach ($xpath->query('(//option[@id="auflage"]//opt)[last()]') as $opt) {
                        if (!$copy) {
                            $calc_base = $opt->getAttribute('id');
                        }
                    }

                    foreach ($xpath->query('//option[@id="auflage"]//opt') as $opt) {
                        if ($opt->getAttribute('id') > ($calc_base - $auflage)) {

                            foreach ($xpath->query('//option[@id="auflage"]//opt[@id="' . $opt->getAttribute('id') . '"]') as $node) {
                                $node->parentNode->removeChild($node);
                            }

                        }
                    }

                    $user = Zend_Auth::getInstance()->getIdentity();
                    if ($copy) {
                        $articleNew = $article->copy();
                        $articleNew->a1_xml = $doc->saveXML();
                        $articleNew->uuid = "";
                        $articleNew->url = "";
                        $articleNew->used = 0;
                        $articleNew->private = 1;
                        $articleNew->contact_id = $user['id'];
                        $articleNew->upload_article = false;
                        $articleNew->upload_multi = false;
                        $articleNew->upload_center = false;
                        $articleNew->upload_post = false;
                        $articleNew->upload_email = false;
                        $articleNew->upload_weblayouter = false;
                        $articleNew->upload_templateprint = false;
                        $articleNew->upload_steplayouter = false;
                        $articleNew->upload_steplayouter2 = false;
                        $articleNew->upload_steplayouter_xml = false;
                        $articleNew->upload_collecting_orders = false;
                        if ($calc_base - $auflage < 80) {
                            $article->enable = false;
                        }
                        $articleNew->title = '('.$obj->alias.') ' . $articleNew->title;
                        $articleNew->save();

                        $ca = new ContactArticle();
                        $ca->article_id = $articleNew->id;
                        $ca->contact_id = $user['id'];
                        $ca->save();

                    } else {
                        if ($calc_base - $auflage < 80) {
                            $article->enable = false;
                        }

                        $article->a1_xml = $doc->saveXML();
                        $article->save();


                    }
                }

            }



        }
    }

}