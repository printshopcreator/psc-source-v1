<?php

class telecash_queues {
	
	public $id = "36";
	
	public $name = "Telecash";
	
	public $information = "Telecashschnittstelle";
	
	public $form = 'Telecash/config/form.ini';

    public $ids;
    public $title;
    public $pos;
    public $when;
    public $mode;

    
    public function toArray() {
        return array('pos' => $this->pos, 'when' => $this->when, 'mode' => $this->mode, 'title' => $this->title, 'ids' => $this->ids);
    }
    
    
    public function process($queue, $obj) {

    	if ( $obj instanceof TP_Basket ) {
  
            if($obj->getPaymenttype() == $this->ids) {

                if($obj->getToken() == "") {
                    $obj->setToken(TP_Util::uuid());
                }

                if((round($obj->getPreisBrutto(), 2)) <= 0) {
                    return true;

                    die();

                }

                $params = Zend_Registry::get('params');

                $user = Zend_Auth::getInstance()->getIdentity();
	    	    $user = Doctrine_Query::create()
	                    ->from('Contact m')
	                    ->where('id = ?')->fetchOne(array($user['id']));

                if((!isset($params['hash']) || 'Ja' != $params['hash']) && !isset($params['token'])) {
                    $shop = Zend_Registry::get('shop');
                    $shop = Doctrine_Query::create()->from('Shop c')->where('c.id = ?', array(
                        $shop['id']))->fetchOne();

                    header('Location: /apps/payment/pay/'.$shop->getApiKey().'/telecash?&token='.urlencode($obj->getToken()).'&amount='.round($obj->getPreisBrutto(),2));

                    die();
                }
            }
    		
    	}
    	
    }
}
