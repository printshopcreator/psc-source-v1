<?php

class ups_queues {

    public $id = "12";
    public $name = "UPS";
    public $information = "Ups";
    public $form = 'Ups/config/form.ini';
    public $title;
    public $pos;
    public $when;
    public $path;
    public $mode;

    public function toArray() {
        return array('pos' => $this->pos, 'title' => $this->title, 'when' => $this->when, 'path' => $this->path, 'mode' => $this->mode);
    }

    public function process($queue, $obj) {

        $shop = Zend_Registry::get('shop');

        if($this->path == "") {
            $this->path = APPLICATION_PATH . '/../data/export/'.$shop['uid'].'/';
            if(!file_exists($this->path)) {
                mkdir($this->path);
            }
        }

        if ($obj instanceof Contact) {

        }elseif ($obj instanceof Orders) {

            /*

            <?xml version="1.0" encoding="UTF-8" standalone="no"?>
            <OpenShipments xmlns="x-schema:OpenShipments.xdr">
                <OpenShipment ProcessStatus="" ShipmentOption="">

                    <ShipTo>
                      <Attention>Name</Attention>
      <Address1>1234 Address</Address1>
      <Address2 />Suite 1<Address3 />
      <CountryTerritory>United States</CountryTerritory>
      <PostalCode>90001</PostalCode>
      <CityOrTown>Los Angeles</CityOrTown>
      <StateProvinceCounty>CA</StateProvinceCounty>
      <Telephone>3231112222</Telephone>
      <FaxNumber>3231113333</FaxNumber>
      <EmailAddress />
      <TaxIDNumber />
      <ReceiverUpsAccountNumber>999999</ReceiverUpsAccountNumber>
      <LocationID />
      <ResidentialIndicator>1</ResidentialIndicator>
                    </ShipTo>

                    <ShipmentInformation>
                      <ServiceType>      </ServiceType>
                      <PackageType>      </PackageType>
                      <NumberOfPackages>      </NumberOfPackages>
                      <ShipmentActualWeight>      </ShipmentActualWeight>
                      <BillingOption>      </BillingOption>
                    </ShipmentInformation>

                </OpenShipment>
            </OpenShipments>

            */
        
            $xml = new SimpleXMLElement('<OpenShipments xmlns="x-schema:OpenShipments.xdr"></OpenShipments>');

            $OpenShipment = $xml->addChild('OpenShipment');
            $OpenShipment->addAttribute('ProcessStatus', "");
            $OpenShipment->addAttribute('ShipmentOption', "");

            $rech = $OpenShipment->addChild('ShipTo');

            if($obj->getDeliveryAddress()->city != "") {
                $rech->addChild('CustomerID', $obj->getDeliveryAddress()->id);
                $rech->addChild('CompanyOrName', $obj->getDeliveryAddress()->company);
                $rech->addChild('Attention', $obj->getDeliveryAddress()->firstname . ' ' . $obj->getDeliveryAddress()->lastname);
                $rech->addChild('Address1', $obj->getDeliveryAddress()->street . ' '.$obj->getDeliveryAddress()->house_number);
                $rech->addChild('Address2', '');
                $rech->addChild('CountryTerritory', $obj->getDeliveryAddress()->country);
                $rech->addChild('PostalCode', $obj->getDeliveryAddress()->zip);
                $rech->addChild('CityOrTown', $obj->getDeliveryAddress()->city);

                $rech->addChild('Telephone', $obj->getDeliveryAddress()->phone);
                $rech->addChild('EmailAddress', $obj->getDeliveryAddress()->email);
            }elseif($obj->getInvoiceAddress()->city != "") {
                $rech->addChild('CustomerID', $obj->getInvoiceAddress()->id);
                $rech->addChild('CompanyOrName', $obj->getInvoiceAddress()->company);
                $rech->addChild('Attention', $obj->getInvoiceAddress()->firstname . ' ' . $obj->getInvoiceAddress()->lastname);
                $rech->addChild('Address1', $obj->getInvoiceAddress()->street . ' '.$obj->getInvoiceAddress()->house_number);
                $rech->addChild('Address2', '');
                $rech->addChild('CountryTerritory', $obj->getInvoiceAddress()->country);
                $rech->addChild('PostalCode', $obj->getInvoiceAddress()->zip);
                $rech->addChild('CityOrTown', $obj->getInvoiceAddress()->city);

                $rech->addChild('Telephone', $obj->getInvoiceAddress()->phone);
                $rech->addChild('EmailAddress', $obj->getInvoiceAddress()->email);
            }else{
                $rech->addChild('CustomerID', $obj->Contact->id);
                $rech->addChild('CompanyOrName', $obj->Contact->department);
                $rech->addChild('Attention', $obj->Contact->self_firstname . ' ' . $obj->Contact->self_lastname);
                $rech->addChild('Address1', $obj->Contact->self_street . ' '.$obj->Contact->self_house_number);
                $rech->addChild('Address2', '');
                $rech->addChild('CountryTerritory', $obj->Contact->self_country);
                $rech->addChild('PostalCode', $obj->Contact->self_zip);
                $rech->addChild('CityOrTown', $obj->Contact->self_city);

                $rech->addChild('Telephone', $obj->Contact->self_phone);
                $rech->addChild('EmailAddress', $obj->Contact->self_email);
            }

            $ShipmentInformation = $OpenShipment->addChild('ShipmentInformation');
            $ShipmentInformation->addChild("ServiceType", 'ST');
            $ShipmentInformation->addChild("PackageType", 'CP');
            $ShipmentInformation->addChild("NumberOfPackages", '1');
            $ShipmentInformation->addChild("ShipmentActualWeight", '1');
            $ShipmentInformation->addChild("BillingOption", 'PP');
            
            if (!file_exists($this->path))
                mkdir($this->path);
                $xml->asXML($this->path . $obj->id . '.xml');
            
            if(Zend_Registry::isRegistered('xmlexport')) {
                $xmlfiles = Zend_Registry::get('xmlexport');
                $xmlfiles[] = array('when' => $this->when , 'name' => $obj->id . '_ups.xml', 'value' => $this->path . $obj->id . '.xml');
                Zend_Registry::set('xmlexport', $xmlfiles);
            }else{
                $xmlfiles = array();
                $xmlfiles[] = array('when' => $this->when , 'name' => $obj->id . '_ups.xml', 'value' => $this->path . $obj->id . '.xml');
                Zend_Registry::set('xmlexport', $xmlfiles);
            }
        }
    }

}
