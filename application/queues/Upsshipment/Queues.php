<?php

class upsshipment_queues {

    public $id = "25";
    public $name = "Ups Shipment";
    public $information = "Ups Shipment";
    public $form = 'Upsshipment/config/form.ini';
    public $title;
    public $pos;
    public $when;
    public $path;
    public $mode;
    public $ids;
    public $pattern;

    public function toArray() {
        return array('pos' => $this->pos, 'title' => $this->title, 'when' => $this->when, 'path' => $this->path, 'mode' => $this->mode, 'pattern' => $this->pattern, 'ids' => $this->ids);
    }

    public function process($queue, $obj) {

        $shop = Zend_Registry::get('shop');

        if ($this->path == "") {
            $this->path = APPLICATION_PATH . '/../market/export/';
            if (!file_exists($this->path)) {
                mkdir($this->path);
            }
            $this->path = APPLICATION_PATH . '/../market/export/' . $shop['uid'] . '/';
            
            if (!file_exists($this->path)) {
                mkdir($this->path);
            }
        }

        if ($obj instanceof Orders) {

            if($this->ids == '' || in_array($obj->getShipping()->id, explode(",", $this->ids))) {
                if ($obj->package != "") {
                    if (Zend_Registry::isRegistered('xmlexport')) {
                        $xmlfiles = Zend_Registry::get('xmlexport');
                    } else {
                        $xmlfiles = array();
                    }

                    if ($this->pattern == "") {
                        $xmlfiles[] = array('when' => $this->when, 'name' => $obj->alias . '_ups.gif', 'value' => $this->path . $obj->alias . '_ups.gif');
                    } else {
                        $loader = new Twig_Loader_String();
                        $twig = new Twig_Environment($loader);
                        $template = $twig->loadTemplate($this->pattern);
                        $name = $template->render(array(
                            'order' => $obj
                        ));
                        $name = $this->slugify($name);
                        $xmlfiles[] = array('when' => $this->when, 'name' => $name . '_ups.gif', 'value' => $this->path . $name . '_ups.gif');
                    }

                    Zend_Registry::set('xmlexport', $xmlfiles);

                    return;
                }

                try {
                    if (!file_exists($this->path)) {
                        mkdir($this->path);
                    }

                    if (Zend_Registry::isRegistered('xmlexport')) {
                        $xmlfiles = Zend_Registry::get('xmlexport');
                    } else {
                        $xmlfiles = array();
                    }


                    if ($this->pattern == "") {
                        exec('/usr/local/bin/php /data/www/new/bin/console --env=prod service:shipping:create ups "'.$obj->uuid.'" "'.$this->path . $obj->alias . '_ups.gif'.'"');

                        $xmlfiles[] = array('when' => $this->when, 'name' => $obj->alias . '_ups.gif', 'value' => $this->path . $obj->alias . '_ups.gif');
                    } else {
                        $loader = new Twig_Loader_String();
                        $twig = new Twig_Environment($loader);
                        $template = $twig->loadTemplate($this->pattern);
                        $name = $template->render(array(
                            'order' => $obj
                        ));
                        $name = $this->slugify($name);
                        exec('/usr/local/bin/php /data/www/new/bin/console --env=prod service:shipping:create ups "'.$obj->uuid.'" "'.$this->path . $name . '_ups.gif'.'"');
                        $xmlfiles[] = array('when' => $this->when, 'name' => $name . '_ups.gif', 'value' => $this->path . $name . '_ups.gif');
                    }



                    Zend_Registry::set('xmlexport', $xmlfiles);

                } catch (Exception $exception) {

                    Zend_Registry::get('log')->debug($exception->getMessage());

                }
            }
        }
    }

    protected function get_data($url)
    {
        $ch = curl_init();
        $timeout = 5;
        curl_setopt($ch,CURLOPT_URL,$url);
        curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
        curl_setopt($ch,CURLOPT_CONNECTTIMEOUT,$timeout);
        $data = curl_exec($ch);

        if(!curl_errno($ch)){
            return $data;
        }else{
            echo 'Curl error: ' . curl_error($ch);
        }
        curl_close($ch);
    }

}
