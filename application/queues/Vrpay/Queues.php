<?php

class vrpay_queues {

    public $id = "10";
    public $name = "VRPay";
    public $information = "VRPay";
    public $form = 'Vrpay/config/form.ini';
    public $userid;
    public $password;
    public $url;
    public $ids;
    public $title;
    public $pos;
    public $when;
    public $mode;

    public $test_mode;
    
    public function toArray() {
        return array('pos' => $this->pos, 'test_mode' => $this->test_mode, 'when' => $this->when, 'mode' => $this->mode, 'title' => $this->title, 'userid' => $this->userid, 'password' => $this->password, 'url' => $this->url, 'ids' => $this->ids);
    }

    public function process($queue, $obj) {

        if ($obj instanceof TP_Basket) {

            if ($obj->getPaymenttype() == $this->ids) {

                $params = Zend_Registry::get('params');

                if($params['hash'] == 'Ja') {

                    
                        
                    return true;
                    

                    die();



                }

                $basket = new TP_Basket( );
                $basket->setToken(TP_Util::uuid());

                $hiddenFieldsArr = array(
                    'HAENDLERNR' => $this->userid, // "80000......"
                    'REFERENZNR' => time(), // "Bestnr.1"
                    'BETRAG' => round($obj->getPreisBrutto(),2) * 100, // "100" -> 1,-
                    'WAEHRUNG' => "EUR", // "EUR"
                    'ARTIKELANZ' => '1',
                    'URLERFOLG' => 'http://'.$_SERVER["SERVER_NAME"].'/basket/finish/hash/Ja/token='.$basket->getToken(), // "http://meinshop.de/erfolg.php"
                    'URLANTWORT' => 'http://'.$_SERVER["SERVER_NAME"].'/basket/finish', // "http://meinshop.de/antwort.php"
                    'URLABBRUCH' => 'http://'.$_SERVER["SERVER_NAME"].'/basket/finish/error/VRPay_Abbruch',
                    'URLAGB' => 'http://'.$_SERVER["SERVER_NAME"].'/basket/finish',
                    'URLFEHLER' => 'http://'.$_SERVER["SERVER_NAME"].'/basket/finish/error/VRPay_Error',
                    'ZAHLART' => "KAUFEN", // "KAUFEN" || "RESERVIEREN"
                    'SERVICENAME' => "DIALOG", // "DIALOG"
                    'VERWENDANZ' => "0",
                    'SPRACHE' => "DE", // "DE"
                    'BENACHRPROF' => 'KEI',
                    'AUSWAHL' => "J",
                );


                //CURL Session initialisieren
                $query = http_build_query($hiddenFieldsArr);



                $ch = curl_init();
                if($this->test_mode == "1") {
                    curl_setopt($ch, CURLOPT_URL, 'https://payinte.vr-epay.de/pbr/transaktion');
                }else{
                    curl_setopt($ch, CURLOPT_URL, 'https://pay.vr-epay.de/pbr/transaktion');
                }
                curl_setopt($ch, CURLOPT_USERPWD, $this->userid.':'.$this->password);

                curl_setopt($ch, CURLOPT_HTTP_VERSION, 1.0);

                curl_setopt($ch, CURLOPT_POST, 1);

                curl_setopt($ch, CURLOPT_POSTFIELDS, $query);

                curl_setopt($ch, CURL_SSLVERSION_TLSv1_2, 3);

                curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);

                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);

                curl_setopt($ch, CURLOPT_HEADER, 1);                 // show http-header

                curl_setopt($ch, CURLOPT_FOLLOWACTION, 0);           // no automatic redirect

                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);



                $ret = curl_exec($ch);

				$info = curl_getinfo($ch);
				
				curl_close($ch);
				
                //QUICK AND DIRTY		
				switch ( $info['http_code'] ) {
               		case "302":
               			$message = str_replace("\r","", $ret);
               			$message = explode("\n\n", $message); 
               			$header_array = explode("\n",$message[0]);
						foreach($header_array as $value) {
							$param = explode(": ",$value);
							if(strtoupper($param[0]) == 'LOCATION') {
								header($value);
								die();
							}
						}
               		break;
               		case "200":
               			$message = str_replace("\r","", $ret);
               			$content = explode("\n\n", $message);
               			header("Location: http://".$_SERVER['SERVER_NAME']."/basket/finish?error=".$content[1]);
               		break;
               		case "401":
               			header("Location: http://".$_SERVER['SERVER_NAME']."/basket/finish?error=Bitte prüfen Sie die Anmeldedaten (Partner-Nr/ sendpay-Passwort).");
               		break;
                	default:
                		header("Location: http://".$_SERVER['SERVER_NAME']."/basket/finish?error=Ein Systemfehler ist aufgetreten.");
                	break;
                }
            }
        }
    }

}
