<?php


class wirecard_queues
{

    public $id = "18";
    public $name = "Wirecard";
    public $information = "Wirecard";
    public $form = 'Wirecard/config/form.ini';
    public $url;
    public $title;
    public $pos;
    public $when;
    public $mode;
    public $ids;
    public $customer;
    public $shopid;
    public $secret;
    public $paymenttype;

    public $test_mode;

    public function toArray() {
        return array('ids' => $this->ids, 'shopid' => $this->shopid, 'pos' => $this->pos, 'when' => $this->when,
            'secret' => $this->secret, 'paymenttype' => $this->paymenttype, 'customer' => $this->customer, 'mode' => $this->mode, 'title' => $this->title,
           );
    }

    public function process($queue, $obj) {

        if ($obj instanceof TP_Basket) {

            if ($obj->getPaymenttype() == $this->ids) {

                $basket = new TP_Basket();
                $params = Zend_Registry::get('params');
                if ($params['token'] == $basket->getToken() && $basket->getToken() != "" && $basket->getToken() != false) {

                    return true;

                }


                $basket->setToken(TP_Util::uuid());

                if ((isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on')) {
                    $server_URL = 'https://' . $_SERVER["SERVER_NAME"];
                } else {
                    $server_URL = 'http://' . $_SERVER["SERVER_NAME"];
                }



                $requestFingerprintOrder = "";
                $requestFingerprint = "";

                $amount = round($obj->getPreisBrutto(),2);

                // sets values for parameters
                $orderDescription = "Auftragsabrechnung" .$basket->getToken();
                $successURL = $server_URL . '/basket/finish?token='.$basket->getToken();
                $cancelURL = $server_URL . '/basket/finish?error=Abbruch';
                $failureURL = $server_URL . '/basket/finish?error=Abbruch';
                $serviceURL = $server_URL . "service.html";
                $pendingURL = $server_URL . "return_pending.html";
                $confirmURL = $server_URL . "confirm.php";

                $consumerUserAgent = $_SERVER['HTTP_USER_AGENT'];
                $consumerIpAddress = $_SERVER['REMOTE_ADDR'];

                //--------------------------------------------------------------------------------//
                // Computes the fingerprint and the fingerprint order.
                //--------------------------------------------------------------------------------//

                $requestFingerprintSeed  = "";
                $requestFingerprintOrder .= "secret,";
                $requestFingerprintSeed  .= $this->secret;
                $requestFingerprintOrder .= "customerId,";
                $requestFingerprintSeed  .= $this->customer;
                $requestFingerprintOrder .= "shopId,";
                $requestFingerprintSeed  .= $this->shopid;
                $requestFingerprintOrder .= "language,";
                $requestFingerprintSeed  .= "DE";
                $requestFingerprintOrder .= "amount,";
                $requestFingerprintSeed  .= $amount;
                $requestFingerprintOrder .= "currency,";
                $requestFingerprintSeed  .= "EUR";
                $requestFingerprintOrder .= "orderDescription,";
                $requestFingerprintSeed  .= $orderDescription;
                $requestFingerprintOrder .= "successUrl,";
                $requestFingerprintSeed  .= $successURL;
                $requestFingerprintOrder .= "pendingUrl,";
                $requestFingerprintSeed  .= $pendingURL;
                $requestFingerprintOrder .= "confirmUrl,";
                $requestFingerprintSeed  .= $confirmURL;
                $requestFingerprintOrder .= "consumerUserAgent,";
                $requestFingerprintSeed  .= $consumerUserAgent;
                $requestFingerprintOrder .= "consumerIpAddress,";
                $requestFingerprintSeed  .= $consumerIpAddress;

                // adds fingerprint order to fingerprint
                $requestFingerprintOrder .= "requestFingerprintOrder";
                $requestFingerprintSeed  .= $requestFingerprintOrder;

                // computes the request fingerprint
                $requestFingerprint = hash("sha512", $requestFingerprintSeed);

                //--------------------------------------------------------------------------------//
                // Creates and sends a POST request (server-to-server request) to the
                // Wirecard Checkout Seamless for initiating the checkout.
                //--------------------------------------------------------------------------------//

                // initiates the string containing all POST parameters and
                // adds them as key-value pairs to the post fields
                $postFields = "";

                $postFields .= "customerId=" . $this->customer;
                $postFields .= "&shopId=" . $this->shopid;
                $postFields .= "&amount=" . $amount;
                $postFields .= "&currency=EUR";
                $postFields .= "&paymentType=".$this->paymenttype;
                $postFields .= "&language=DE";
                $postFields .= "&orderDescription=" . $orderDescription;
                $postFields .= "&successUrl=" . $successURL;
                $postFields .= "&cancelUrl=" . $cancelURL;
                $postFields .= "&failureUrl=" . $failureURL;
                $postFields .= "&serviceUrl=" . $serviceURL;
                $postFields .= "&pendingUrl=" . $pendingURL;
                $postFields .= "&confirmUrl=" . $confirmURL;
                $postFields .= "&requestFingerprintOrder=" . $requestFingerprintOrder;
                $postFields .= "&requestFingerprint=" . $requestFingerprint;
                $postFields .= "&consumerUserAgent=" . $consumerUserAgent;
                $postFields .= "&consumerIpAddress=" . $consumerIpAddress;

                // initializes the libcurl of PHP used for sending a POST request
                // to the Wirecard data storage as a server-to-server request
                // (please be aware that you have to use a web server where a
                // server-to-server request is enabled)
                $curl = curl_init();

                // sets the required options for the POST request via curl
                curl_setopt($curl, CURLOPT_URL, 'https://checkout.wirecard.com/seamless/frontend/init');
                curl_setopt($curl, CURLOPT_PORT, 443);
                curl_setopt($curl, CURLOPT_PROTOCOLS, CURLPROTO_HTTPS);
                curl_setopt($curl, CURLOPT_POST, true);
                curl_setopt($curl, CURLOPT_POSTFIELDS, $postFields);
                curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, true);
                curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($curl, CURLOPT_USERAGENT, $_SERVER['HTTP_USER_AGENT']);

                // sends a POST request to the Wirecard Checkout Platform and stores the
                // result returned from the Wirecard data storage in a string for later use
                $curlResult = curl_exec($curl);

                // closes the connection to the Wirecard Checkout Platform
                curl_close($curl);

                $redirectURL = "";
                foreach (explode('&', $curlResult) as $keyvalue) {
                    $param = explode('=', $keyvalue);
                    if (sizeof($param) == 2) {
                        $key = urldecode($param[0]);
                        if ($key == "redirectUrl") {
                            $redirectURL = urldecode($param[1]);
                            break;
                        }
                    }
                }

                //--------------------------------------------------------------------------------//
                // Redirects consumer to payment page.
                //--------------------------------------------------------------------------------//

                if ($redirectURL == "") {
                    echo "<pre>";
                    echo "Frontend Intitiation failed with errors:\n\n";
                    foreach (explode('&', $curlResult) as $keyvalue) {
                        $param = explode('=', $keyvalue);
                        if (sizeof($param) == 2) {
                            $key = urldecode($param[0]);
                            $value = urldecode($param[1]);
                            echo $key . " = " . $value . "\n";
                        }
                    }
                    echo "</pre>";
                } else {
                    header("Location: " . $redirectURL);
                }
                die();
            }
        }
    }

}
