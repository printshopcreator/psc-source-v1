<?php

class xerox_queues
{

    public $id = "15";
    public $name = "Xerox Export";
    public $information = "Xerox Export";
    public $form = 'Xerox/config/form.ini';
    public $title;
    public $status;
    public $pos;
    public $when;
    public $path;
    public $server_path;
    public $mode;

    public function toArray()
    {
        return array('pos' => $this->pos, 'status' => $this->status, 'title' => $this->title, 'when' => $this->when, 'server_path' => $this->server_path, 'path' => $this->path, 'mode' => $this->mode);
    }


    public function convertToWindowsCharset($string) {
        $charset =  mb_detect_encoding(
            $string,
            "UTF-8, ISO-8859-1, ISO-8859-15",
            true
        );

        $string =  mb_convert_encoding($string, "Windows-1252", $charset);
        return $string;
    }


    public function process($queue, $obj)
    {

        $shop = Zend_Registry::get('shop');

        if ($this->path == "") {
            $this->path = APPLICATION_PATH . '/../data/export/' . $shop['uid'] . '/';
            if (!file_exists($this->path)) {
                mkdir($this->path);
            }
        }

        if ($obj instanceof Orders) {

            $data = array();
            if($obj->status < $this->status) {
                return;
            }

            foreach ($obj->Orderspos as $orderpos) {

                if(count($orderpos->Upload) >= 1) {
                    foreach ($orderpos->Upload as $file) {

                        $datatemp = array();
                        $datatemp[] = $orderpos->pos;
                        $datatemp[] = $obj->alias;
                        $datatemp[] = "";
                        $datatemp[] = $obj->created;

                        $article = Doctrine_Query::create()
                            ->from('Article c')
                            ->where('c.id = ?', array($orderpos->article_id))
                            ->fetchOne();
                        if (!$article) {
                            continue;
                        }
                        $options = $article->getOptArray($orderpos->data, true, true, true);
                        $params = unserialize($orderpos->data);
                        $params = $params->getOptions();
                        $papierSingle = Doctrine_Query::create()->select()->from('Papier p')->andWhere('p.art_nr = ? AND p.install_id = ?', array($params['papier'], $orderpos->install_id))->fetchOne();


                        $datatemp[] = @$options['produktion'];
                        $datatemp[] = "";


                        $datatemp[] = $obj->alias . "_" . $orderpos->pos."_". $article->id . "_" . $file['name'];
                        $datatemp[] = $this->server_path . '/'.$obj->alias."/".$obj->alias ."_". $article->id . "_" . $orderpos->pos . "/";

                        $pro = unserialize($orderpos->data);
                        $proa = $pro->getArticle();

                        $datatemp[] = $proa['title'];
                        $datatemp[] = @$options['typ'];
                        $datatemp[] = "";

                        $datatemp[] = @$options['auflage'];
                        $datatemp[] = @$options['sorten'];
                        $datatemp[] = "";
                        $datatemp[] = @$options['anzseiten'];
                        $datatemp[] = @$options['anzblatt'];
                        $datatemp[] = @$options['umschlag'];
                        $datatemp[] = "";
                        $datatemp[] = "";
                        $datatemp[] = "";
                        $datatemp[] = @$options['druck'];
                        $datatemp[] = @$options['druckum'];
                        $datatemp[] = "";


                        $papierSingle = Doctrine_Query::create()->select()->from('Papier p')->andWhere('p.art_nr = ? AND p.install_id = ?', array($params['papier'], $orderpos->install_id))->fetchOne();
                        $datatemp[] = $papierSingle->description_2;
                        $datatemp[] = @$options['papier'];
                        $datatemp[] = @$options['papierum'];
                        $datatemp[] = @$options['papierum1'];
                        $datatemp[] = @$options['papierum2'];
                        $datatemp[] = "";
                        $datatemp[] = "";
                        $datatemp[] = "";
                        $datatemp[] = "";
                        $datatemp[] = @$options['exformat'];
                        $datatemp[] = @$options['breite'];
                        $datatemp[] = @$options['hoehe'];
                        $datatemp[] = @$options['schlitz'];
                        $datatemp[] = "";
                        $datatemp[] = @$options['nutzen'];
                        $datatemp[] = "";
                        $datatemp[] = @$options['nummerierung'];
                        $datatemp[] = @$options['perforierung'];
                        $datatemp[] = @$options['lackierung'];
                        $datatemp[] = "";
                        $datatemp[] = "";
                        $datatemp[] = "";
                        $datatemp[] = "";
                        $datatemp[] = "";
                        $datatemp[] = "";
                        $datatemp[] = @$options['datenformat'];
                        $datatemp[] = @$options['datenuebertragung'];
                        $datatemp[] = @$options['musterdruck'];
                        $datatemp[] = @$options['musterkalender'];
                        $datatemp[] = @$options['bezeichnung'];
                        $datatemp[] = "";
                        $datatemp[] = "";
                        $datatemp[] = @$options['verarbeitung'];
                        $datatemp[] = @$options['formatadk'];
                        $datatemp[] = @$options['einschweissen'];
                        $datatemp[] = @$options['option'];
                        $datatemp[] = @$options['spirale'];
                        $datatemp[] = @$options['veredelung'];
                        $datatemp[] = "";
                        $datatemp[] = "";
                        $datatemp[] = "";
                        $datatemp[] = $orderpos->priceall;
                        $datatemp[] = $obj->versandkosten;
                        $datatemp[] = $obj->zahlkosten;
                        $datatemp[] = $obj->preis;
                        $datatemp[] = $obj->preissteuer;
                        $datatemp[] = $obj->preisbrutto;
                        $datatemp[] = $article->mwert;
                        $datatemp[] = $obj->Paymenttype->title;
                        $datatemp[] = $obj->Paymenttype->id;
                        $datatemp[] = "";
                        $datatemp[] = $obj->Shippingtype->title;
                        $datatemp[] = $obj->Shippingtype->id;
                        $datatemp[] = "";
                        $datatemp[] = $obj->basketfield1;
                        $datatemp[] = $obj->basketfield2;
                        $datatemp[] = "";

                        $invoice = $obj->getInvoiceAddress();
                        $delivery = $obj->getDeliveryAddress();
                        $sender = $obj->getSenderAddress();

                        $datatemp[] = $invoice->getCompany();
                        $datatemp[] = $invoice->getCompany2();
                        $datatemp[] = $invoice->getStreet();
                        $datatemp[] = $invoice->getHouseNumber();
                        $datatemp[] = $invoice->getCity();
                        $datatemp[] = $invoice->getZip();
                        $datatemp[] = $invoice->getCountry();
                        $datatemp[] = $invoice->getFirstname();
                        $datatemp[] = $invoice->getLastname();
                        $datatemp[] = $invoice->getPhone();
                        $datatemp[] = $invoice->getMail();
                        $datatemp[] = "";
                        $datatemp[] = $delivery->getCompany();
                        $datatemp[] = $delivery->getCompany2();
                        $datatemp[] = $delivery->getStreet();
                        $datatemp[] = $delivery->getHouseNumber();
                        $datatemp[] = $delivery->getCity();
                        $datatemp[] = $delivery->getZip();
                        $datatemp[] = $delivery->getCountry();
                        $datatemp[] = $delivery->getFirstname();
                        $datatemp[] = $delivery->getLastname();
                        $datatemp[] = $delivery->getPhone();
                        $datatemp[] = $delivery->getMail();
                        $datatemp[] = "";
                        $datatemp[] = $sender->getCompany();
                        $datatemp[] = $sender->getCompany2();
                        $datatemp[] = $sender->getStreet();
                        $datatemp[] = $sender->getHouseNumber();
                        $datatemp[] = $sender->getCity();
                        $datatemp[] = $sender->getZip();
                        $datatemp[] = $sender->getCountry();
                        $datatemp[] = $sender->getFirstname();
                        $datatemp[] = $sender->getLastname();
                        $datatemp[] = $sender->getPhone();
                        $datatemp[] = $sender->getMail();

                        $data[] = $datatemp;
                    }
                }else{

                    $datatemp = array();
                    $datatemp[] = $orderpos->pos;
                    $datatemp[] = $obj->alias;
                    $datatemp[] = "";
                    $datatemp[] = $obj->created;

                    $article = Doctrine_Query::create()
                        ->from('Article c')
                        ->where('c.id = ?', array($orderpos->article_id))
                        ->fetchOne();
                    if (!$article) {
                        continue;
                    }
                    $options = $article->getOptArray($orderpos->data, true, true, true);

                    $datatemp[] = @$options['produktion'];
                    $datatemp[] = "";
                    if ($article->typ == 6) {
                        $datatemp[] = $obj->alias . "_" . $article->id . "_" . $orderpos->pos . ".zip";
                    }

                    $datatemp[] = "";
                    $datatemp[] = $this->server_path . '/'.$obj->alias."/".$obj->alias ."_". $article->id . "_" . $orderpos->pos . "/";

                    $pro = unserialize($orderpos->data);
                    $proa = $pro->getArticle();

                    $datatemp[] = $proa['title'];
                    $datatemp[] = @$options['typ'];
                    $datatemp[] = "";

                    $datatemp[] = @$options['auflage'];
                    $datatemp[] = @$options['sorten'];
                    $datatemp[] = "";
                    $datatemp[] = @$options['anzseiten'];
                    $datatemp[] = @$options['anzblatt'];
                    $datatemp[] = @$options['umschlag'];
                    $datatemp[] = "";
                    $datatemp[] = "";
                    $datatemp[] = "";
                    $datatemp[] = @$options['druck'];
                    $datatemp[] = @$options['druckum'];
                    $datatemp[] = "";
                    $datatemp[] = "";
                    $datatemp[] = @$options['papier'];
                    $datatemp[] = @$options['papierum'];
                    $datatemp[] = @$options['papierum1'];
                    $datatemp[] = @$options['papierum2'];
                    $datatemp[] = "";
                    $datatemp[] = "";
                    $datatemp[] = "";
                    $datatemp[] = "";
                    $datatemp[] = @$options['exformat'];
                    $datatemp[] = @$options['breite'];
                    $datatemp[] = @$options['hoehe'];
                    $datatemp[] = @$options['schlitz'];
                    $datatemp[] = "";
                    $datatemp[] = @$options['nutzen'];
                    $datatemp[] = "";
                    $datatemp[] = @$options['nummerierung'];
                    $datatemp[] = @$options['perforierung'];
                    $datatemp[] = @$options['lackierung'];
                    $datatemp[] = "";
                    $datatemp[] = "";
                    $datatemp[] = "";
                    $datatemp[] = "";
                    $datatemp[] = "";
                    $datatemp[] = "";
                    $datatemp[] = @$options['datenformat'];
                    $datatemp[] = @$options['datenuebertragung'];
                    $datatemp[] = @$options['musterdruck'];
                    $datatemp[] = @$options['musterkalender'];
                    $datatemp[] = @$options['bezeichnung'];
                    $datatemp[] = "";
                    $datatemp[] = "";
                    $datatemp[] = @$options['verarbeitung'];
                    $datatemp[] = @$options['formatadk'];
                    $datatemp[] = @$options['einschweissen'];
                    $datatemp[] = @$options['option'];
                    $datatemp[] = @$options['spirale'];
                    $datatemp[] = @$options['veredelung'];
                    $datatemp[] = "";
                    $datatemp[] = "";
                    $datatemp[] = "";
                    $datatemp[] = $orderpos->priceall;
                    $datatemp[] = $obj->versandkosten;
                    $datatemp[] = $obj->zahlkosten;
                    $datatemp[] = $obj->preis;
                    $datatemp[] = $obj->preissteuer;
                    $datatemp[] = $obj->preisbrutto;
                    $datatemp[] = $article->mwert;
                    $datatemp[] = $obj->Paymenttype->title;
                    $datatemp[] = $obj->Paymenttype->id;
                    $datatemp[] = "";
                    $datatemp[] = $obj->Shippingtype->title;
                    $datatemp[] = $obj->Shippingtype->id;
                    $datatemp[] = "";
                    $datatemp[] = $obj->basketfield1;
                    $datatemp[] = $obj->basketfield2;
                    $datatemp[] = "";

                    $invoice = $obj->getInvoiceAddress();
                    $delivery = $obj->getDeliveryAddress();
                    $sender = $obj->getSenderAddress();

                    $datatemp[] = $invoice->getCompany();
                    $datatemp[] = $invoice->getCompany2();
                    $datatemp[] = $invoice->getStreet();
                    $datatemp[] = $invoice->getHouseNumber();
                    $datatemp[] = $invoice->getCity();
                    $datatemp[] = $invoice->getZip();
                    $datatemp[] = $invoice->getCountry();
                    $datatemp[] = $invoice->getFirstname();
                    $datatemp[] = $invoice->getLastname();
                    $datatemp[] = $invoice->getPhone();
                    $datatemp[] = $invoice->getMail();
                    $datatemp[] = "";
                    $datatemp[] = $delivery->getCompany();
                    $datatemp[] = $delivery->getCompany2();
                    $datatemp[] = $delivery->getStreet();
                    $datatemp[] = $delivery->getHouseNumber();
                    $datatemp[] = $delivery->getCity();
                    $datatemp[] = $delivery->getZip();
                    $datatemp[] = $delivery->getCountry();
                    $datatemp[] = $delivery->getFirstname();
                    $datatemp[] = $delivery->getLastname();
                    $datatemp[] = $delivery->getPhone();
                    $datatemp[] = $delivery->getMail();
                    $datatemp[] = "";
                    $datatemp[] = $sender->getCompany();
                    $datatemp[] = $sender->getCompany2();
                    $datatemp[] = $sender->getStreet();
                    $datatemp[] = $sender->getHouseNumber();
                    $datatemp[] = $sender->getCity();
                    $datatemp[] = $sender->getZip();
                    $datatemp[] = $sender->getCountry();
                    $datatemp[] = $sender->getFirstname();
                    $datatemp[] = $sender->getLastname();
                    $datatemp[] = $sender->getPhone();
                    $datatemp[] = $sender->getMail();

                    $data[] = $datatemp;
                }



            }

            if (!file_exists($this->path)) {
                mkdir($this->path);
            }

            $fp = fopen($this->path . $obj->alias . '.csv', 'w');

            foreach ($data as $fields) {
                fwrite($fp, $this->convertToWindowsCharset('"'.implode('","', $fields).'",'));
                fwrite($fp, "\r\n");
            }

            fclose($fp);

            $xmlfiles = array();
            $xmlfiles[] = array('when' => $this->when, 'name' => $obj->alias . '.csv', 'value' => $this->path . $obj->alias . '.csv');
            Zend_Registry::set('xmlexport', $xmlfiles);

        }
    }

}
