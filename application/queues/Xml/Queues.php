<?php

class xml_queues
{

    public $id = "2";
    public $name = "Xml";
    public $information = "Stores XML Files";
    public $form = 'Xml/config/form.ini';
    public $title;
    public $pos;
    public $when;
    public $path;
    public $mode;
    public $pattern;

    public function toArray() {
        return array('pos' => $this->pos, 'title' => $this->title, 'when' => $this->when, 'path' => $this->path, 'mode' => $this->mode, 'pattern' => $this->pattern);
    }

    public function process($queue, $obj) {

        $shop = Zend_Registry::get('shop');

        if ($this->path == "") {
            $this->path = APPLICATION_PATH . '/../data/export/' . $shop['uid'] . '/';
            if (!file_exists($this->path)) {
                mkdir($this->path);
            }
        }

        if ($obj instanceof Contact) {

            $xml = new SimpleXMLElement('<import version="1.0"></import>');

            $xml->addChild('firstname', $obj->self_firstname);
            $xml->addChild('lastname', $obj->self_lastname);
            $xml->addChild('phone', $obj->name);
            $xml->addChild('email', $obj->password);

            if (!file_exists($this->path)) {
                mkdir($this->path);
            }
            $xml->asXML($this->path . $obj->id . '_' . $obj->self_lastname . '_' . $obj->self_firstname . '.xml');
        } elseif ($obj instanceof Orders) {

            $xml = new SimpleXMLElement('<import version="1.0"></import>');

            $overview = $xml->addChild('overview');
            $overview->addChild('alias', $obj->alias);
            $overview->addChild('shop', $obj->Shop->name);
            $overview->addChild('shop_id', $obj->Shop->id);
            $overview->addChild('created', $obj->created);
            $overview->addChild('contact_id', $obj->contact_id);
            $overview->addChild('lang', $obj->lang);

            $overview->addChild('brutto', $obj->preisbrutto);
            $overview->addChild('steuer', $obj->preissteuer);
            $overview->addChild('file1', $obj->file1);
            $overview->addChild('netto', $obj->preis);

            $account = $xml->addChild('account');
            $account->addChild('company', $obj->Account->company);

            $invoice = $obj->getInvoiceAddress();
            $delivery = $obj->getDeliveryAddress();
            $sender = $obj->getSenderAddress();

            $rech = $xml->addChild('invoice');

            $rech->addChild('anrede', $invoice->getAnrede());
            $rech->addChild('anrede_raw', $invoice->anrede);
            $rech->addChild('company', $this->convertSonderToXml($invoice->getCompany()));
            $rech->addChild('company2', $this->convertSonderToXml($invoice->getCompany2()));
            $rech->addChild('street', $invoice->getStreet());
            $rech->addChild('house_number', $invoice->getHouseNumber());
            $rech->addChild('city', $invoice->getCity());
            $rech->addChild('zip', $invoice->getZip());
            $rech->addChild('country', $invoice->getCountry());
            $rech->addChild('firstname', $invoice->getFirstname());
            $rech->addChild('lastname', $invoice->getLastname());
            $rech->addChild('phone', $invoice->getPhone());
            $rech->addChild('email', $invoice->getMail());

            $rech->addChild('fax', $invoice->getFax());
            $rech->addChild('ustid', $invoice->getUstId());
            $rech->addChild('zusatz1', $invoice->getZusatz1());
            $rech->addChild('zusatz2', $invoice->getZusatz2());

            $lief = $xml->addChild('delivery');

            $lief->addChild('anrede', $delivery->getAnrede());
            $lief->addChild('anrede_raw', $delivery->anrede);
            $lief->addChild('company', $this->convertSonderToXml($delivery->getCompany()));
            $lief->addChild('company2', $this->convertSonderToXml($delivery->getCompany2()));
            $lief->addChild('street', $delivery->getStreet());
            $lief->addChild('house_number', $delivery->getHouseNumber());
            $lief->addChild('city', $delivery->getCity());
            $lief->addChild('zip', $delivery->getZip());
            $lief->addChild('country', $delivery->getCountry());
            $lief->addChild('firstname', $delivery->getFirstname());
            $lief->addChild('lastname', $delivery->getLastname());
            $lief->addChild('phone', $delivery->getPhone());
            $lief->addChild('email', $delivery->getMail());
            $lief->addChild('position', $delivery->getPosition());
            $lief->addChild('ustid', $delivery->getUstId());
            $lief->addChild('zusatz1', $delivery->getZusatz1());
            $lief->addChild('zusatz2', $delivery->getZusatz2());


            $lief->addChild('fax', $delivery->getFax());
            $lief->addChild('ustid', $delivery->getUstId());
            $lief->addChild('zusatz1', $delivery->getZusatz1());
            $lief->addChild('zusatz2', $delivery->getZusatz2());

            $send = $xml->addChild('sender');

            $send->addChild('country', $sender->getCountry());
            $send->addChild('anrede', $sender->getAnrede());
            $send->addChild('anrede_raw', $sender->anrede);
            $send->addChild('company', $this->convertSonderToXml($sender->getCompany()));
            $send->addChild('company2', $this->convertSonderToXml($sender->getCompany2()));
            $send->addChild('street', $sender->getStreet());
            $send->addChild('house_number', $sender->getHouseNumber());
            $send->addChild('city', $sender->getCity());
            $send->addChild('zip', $sender->getZip());
            $send->addChild('firstname', $sender->getFirstname());
            $send->addChild('lastname', $sender->getLastname());
            $send->addChild('phone', $sender->getPhone());
            $send->addChild('email', $sender->getMail());


            $send->addChild('fax', $sender->getFax());
            $send->addChild('ustid', $sender->getUstId());
            $send->addChild('zusatz1', $sender->getZusatz1());
            $send->addChild('zusatz2', $sender->getZusatz2());

            $payment = $xml->addChild('payment');
            $payment->addChild('name', $obj->Paymenttype->title);
            $payment->addChild('id', $obj->Paymenttype->id);
            $payment->addChild('price', $obj->zahlkosten);

            $shipping = $xml->addChild('shipping');
            $shipping->addChild('name', $obj->Shippingtype->title);
            $shipping->addChild('id', $obj->Shippingtype->id);
            $shipping->addChild('price', $obj->versandkosten);

            $info = $obj->getInfo();

            if(!empty($info) && is_array($info) && ($info['company'] != '' || $info['firstname'] != '' || $info['lastname'] != '')) {
                $infoXml = $xml->addChild('info');
                foreach($info as $xmlKey => $xmlInh) {
                    $infoXml->addChild($xmlKey, $xmlInh);

                    if($xmlKey == "firstname") {
                        $rech->firstname = $xmlInh;
                        $lief->firstname = $xmlInh;
                    }
                    if($xmlKey == "lastname") {
                        $rech->lastname = $xmlInh;
                        $lief->lastname = $xmlInh;
                    }
                    if($xmlKey == "email") {
                        $rech->email = $xmlInh;
                        $lief->email = $xmlInh;
                    }
                    if($xmlKey == "street") {
                        $rech->street = $xmlInh;
                        $lief->street = $xmlInh;
                    }
                    if($xmlKey == "number") {
                        $rech->house_number = $xmlInh;
                        $lief->house_number = $xmlInh;
                    }
                    if($xmlKey == "zip") {
                        $rech->zip = $xmlInh;
                        $lief->zip = $xmlInh;
                    }
                    if($xmlKey == "company") {
                        $rech->company = $xmlInh;
                        $lief->company = $xmlInh;
                    }
                    if($xmlKey == "company2") {
                        $rech->company2 = $xmlInh;
                        $lief->company2 = $xmlInh;
                    }
                    if($xmlKey == "city") {
                        $rech->city = $xmlInh;
                        $lief->city = $xmlInh;
                    }
                }
            }
            $overview->addChild('basketfield1', $obj->basketfield1);
            $overview->addChild('basketfield2', $obj->basketfield2);

            $liefer = $xml->addChild('pos');

            foreach ($obj->Orderspos as $orderpos) {

                $article = Doctrine_Query::create()
                    ->from('Article c')
                    ->where('c.id = ?', array($orderpos->article_id))
                    ->fetchOne();

                if(!$article) {
                    continue;
                }


                $pro = unserialize($orderpos->data);
                $proa = $pro->getArticle();

                mkdir($this->path . $obj->id . '_' . $orderpos->id);

                foreach ($orderpos->Upload as $file) {
                    if (file_exists($file['path'])) {
                        copy($file['path'], $this->path . $obj->id . '_' . $orderpos->id . '/' . $file['name']);
                    } elseif (file_exists('uploads/' . $shop['uid'] . '/article/' . $file['path'])) {
                        copy('uploads/' . $shop['uid'] . '/article/' . $file['path'], $this->path . $obj->id . '_' . $orderpos->id . '/' . $file['name']);
                    }
                }

                $pos = $liefer->addChild('artikel');
                if($orderpos->getPostionData('auflage')) {
                    $pos->addChild('menge', $orderpos->getPostionData('auflage'));
                }else{
                    $pos->addChild('menge', $orderpos->count);
                }

                $pos->addChild('art', $this->convertSonderToXml($proa['title']));
                $pos->addChild('ref', $orderpos->ref);
                $pos->addChild('position', $orderpos->pos);
                $pos->addChild('alias', $obj->alias . '-' . $orderpos->pos);
                $pos->addChild('kst', $orderpos->kst);
                $pos->addChild('weight', $orderpos->weight);
                $pos->addChild('special', (bool)$pro->getSpecial());

                if($article->a6_org_article != 0) {
                    $articleOrg = Doctrine_Query::create()
                        ->from('Article c')
                        ->where('c.id = ?', array($article->a6_org_article))
                        ->fetchOne();

                    $pos->addChild('title', $this->convertSonderToXml($articleOrg->getTitle(false, $obj->lang)));
                    $pos->addChild('article_nr_intern', $articleOrg->article_nr_intern);
                    $pos->addChild('article_nr_extern', $articleOrg->article_nr_extern);
                    $pos->addChild('a6_org_article', $articleOrg->id);
                }else{
                    $pos->addChild('title', $this->convertSonderToXml($article->getTitle(false, $obj->lang)));
                    $pos->addChild('article_nr_intern', $article->article_nr_intern);
                    $pos->addChild('article_nr_extern', $article->article_nr_extern);

                }

                $pos->addChild('article_id', $article->id);

                $pos->addChild('text_art', $this->convertSonderToXml($article->getTextArt(false, $obj->lang)));

                $pos->addChild('netto', $orderpos->priceone);
                $pos->addChild('netto_sum', $orderpos->priceall);

                $pos->addChild('article_mwert', $article->mwert);


                $set = $pos->addChild('set');
                if($article->set_config != "") {

                    $setConfig = json_decode($orderpos->calc_xml, true);

                    foreach($article->getSetProducts() as $product) {
                        $setprod = $set->addChild('artikel');
                        $setprod->addChild('title', $this->convertSonderToXml($product->getTitle(false, $obj->lang)));
                        $setprod->addChild('article_nr_intern', $product->article_nr_intern);
                        $setprod->addChild('article_nr_extern', $product->article_nr_extern);
                        $setprod->addChild('text_art', $this->convertSonderToXml($product->getTextArt(false, $obj->lang)));
                        if(isset($setConfig[$product->id])) {
                            $setprod->addChild('available', $setConfig[$product->id]);
                            $setprod->addChild('menge', $orderpos->count);
                        }else{
                            $setprod->addChild('available', $orderpos->count);
                            $setprod->addChild('menge', $orderpos->count);
                        }
                    }
                }

                $layouterID = unserialize($orderpos->data)->getLayouterId();
                if($layouterID != "") {
                    try {
                        $contentCheck = json_decode(file_get_contents("http://localhost/apps/psc/component/configurationlayouter/store/check/" . $layouterID), true);

                        if ($contentCheck['found'] == true) {
                            if(isset($contentCheck['data']['products'])) {
                                $set = $pos->addChild('set');
                                foreach ($contentCheck['data']['products'] as $art) {

                                    $articleSet = Doctrine_Query::create()
                                        ->from('Article c')
                                        ->where('c.id = ?', array($art['uid']))
                                        ->fetchOne();

                                    $setprod = $set->addChild('artikel');
                                    $setprod->addChild('title', $this->convertSonderToXml($articleSet->getTitle(false, $obj->lang)));
                                    $setprod->addChild('article_nr_intern', $articleSet->article_nr_intern);
                                    $setprod->addChild('article_nr_extern', $articleSet->article_nr_extern);
                                    $setprod->addChild('text_art', $this->convertSonderToXml($articleSet->getTextArt(false, $obj->lang)));
                                    $setprod->addChild('menge', $art['amount']);

                                }
                            }elseif(isset($contentCheck['data']['folientyp'])) {

                                $set = $pos->addChild('banner');
                                $set->addChild('verrechungsPreis', $contentCheck['data']['verrechungsPreis']);
                                $set->addChild('content', $contentCheck['data']['content']);
                                $set->addChild('folientyp', $contentCheck['data']['folientyp']);
                                $set->addChild('groesseSelect', $contentCheck['data']['groesseSelect']);
                                $set->addChild('groesseInput', $contentCheck['data']['groesseInput']);
                                $set->addChild('mirror', $contentCheck['data']['mirror']);
                                $set->addChild('count', $contentCheck['data']['count']);
                                $lines = $set->addChild('lines');

                                foreach($contentCheck['data']['lines'] as $line) {
                                    $lineRow = $lines->addChild('line');
                                    $lineRow->addChild('bold', $line['bold']);
                                    $lineRow->addChild('italic', $line['italic']);
                                    $lineRow->addChild('align', $line['align']);
                                    $lineRow->addChild('color', $line['color']);
                                    $lineRow->addChild('font', $line['font']);
                                    $lineRow->addChild('zeichenabstand', $line['zeichenabstand']);
                                }
                            }
                        }
                    }catch(Exception $e) {}
                }

                if($article->typ == 6 && $orderpos->layouter_mode != 0) {
                    $pos->addChild('filename', $obj->alias."_".$article->id."_".$orderpos->pos.".zip");
                }


                $options = $article->getOptArray($orderpos->data, true, true, true);

                $option = $pos->addChild('options');

                foreach ($options as $opt_key => $opt_name) {
                    $option->addChild($opt_key, $opt_name);
                }
            }

            if (!file_exists($this->path)) {
                mkdir($this->path);
            }

            $xmlfiles = array();
            if(Zend_Registry::isRegistered('xmlexport')) {
                $xmlfiles = Zend_Registry::get('xmlexport');
            }

            if($this->pattern == "") {
                $xml->asXML($this->path . $obj->alias . '.xml');
                $xmlfiles[] = array('when' => $this->when, 'name' => $obj->alias . '.xml', 'value' => $this->path . $obj->alias . '.xml');
            }else{
                $loader = new Twig_Loader_String();
                $twig = new Twig_Environment($loader);
                $template = $twig->loadTemplate($this->pattern);
                $name = $template->render(array(
                    'order' => $obj
                ));
                $name = $this->slugify($name);
                $xml->asXML($this->path . $name . '.xml');
                $xmlfiles[] = array('when' => $this->when, 'name' => $name . '.xml', 'value' => $this->path . $name . '.xml');
            }





            Zend_Registry::set('xmlexport', $xmlfiles);

        }
    }

    public function slugify($string)
    {
        $ret = $string;
        // remove html line break
        $ret = preg_replace("<br/>", '', $ret);
        // strip all non word chars
        $ret = preg_replace('/\W/u', ' ', $ret);
        // replace all white space sections with a dash
        $ret = preg_replace('/\ +/', '-', $ret);
        // trim dashes
        $ret = preg_replace('/\-$/', '', $ret);
        $ret = preg_replace('/^\-/', '', $ret);
        // convert to lower case
        $ret = strtolower($ret);

        return $ret;
    }

    public function convertSonderToXml($str) {
        /**
        <	&lt;
        >	&gt;
        &	&amp;
        "	&quot;
        '	&apos;
         */
        $str = htmlspecialchars($str, ENT_XML1 | ENT_QUOTES, 'UTF-8', true);

        return $str;
    }

}
