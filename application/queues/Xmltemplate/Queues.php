<?php

class xmltemplate_queues
{

    public $id = "22";

    public $name = "XML Template";

    public $information = "";

    public $form = 'Xmltemplate/config/form.ini';

    public $template_xml;

    public $title;
    public $pos;
    public $mandant;
    public $when;
    public $mode;

    public function toArray() {
        return array('pos' => $this->pos, 'when' => $this->when, 'mode' => $this->mode, 'title' => $this->title, 'template_xml' => $this->template_xml);
    }

    public function process($queue, $obj)
    {

        $shop = Zend_Registry::get('shop');

        if ($this->path == "") {
            $this->path = APPLICATION_PATH . '/../data/export/' . $shop['uid'] . '/';
            if (!file_exists($this->path)) {
                mkdir($this->path);
            }
        }

        if ($obj instanceof Orders) {

            $loader = new Twig_Loader_String();
            $twig = new Twig_Environment($loader, array(
                'autoescape' => false
            ));

            $template = $twig->loadTemplate($this->template_xml);

            $invoice = $obj->getInvoiceAddress();
            $contact = $obj->Contact;
            $delivery = $obj->getDeliveryAddress();
            $sender = $obj->getSenderAddress();
            $shop = $obj->Shop;
            $account = $obj->Account;
            $payment = $obj->Paymenttype;
            $shipping = $obj->Shippingtype;
            $positionObjs = $obj->Orderspos;


            $positions = array();

            foreach($positionObjs as $pos) {
                $article = Doctrine_Query::create()
                    ->from('Article c')
                    ->where('c.id = ?', array($pos->article_id))
                    ->fetchOne();
                if(!$article) {
                    continue;
                }

                $options = $article->getOptArray($pos->data, true, true, true);
                $rawOptions = $article->getRawOptArray($pos->data, true);
                $calcValues = Zend_Json::decode($pos->calc_values);
                $deliveryData = $pos->getDeliveryData();
                $positions[] = array(
                    'article' => $pos->Article,
                    'pos' => $pos,
                    'options' => $options,
                    'rawOptions' => $rawOptions,
                    'calcValues' => $calcValues,
                    'deliveryData' => $deliveryData
                );
            }


            $content = $template->render(array(
                'order' => $obj,
                'senderAddress' => $sender,
                'deliveryData' => $deliveryData,
                'invoiceAddress' => $invoice,
                'deliveryAddress' => $delivery,
                'shop' => $shop,
                'account' => $account,
                'payment' => $payment,
                'contact' => $contact,
                'shipping' => $shipping,
                'positions' => $positions
            ));


            if (!file_exists($this->path)) {
                mkdir($this->path);
            }

            $xmlfiles = array();

            if ($this->pattern == "") {
                file_put_contents($this->path . $obj->alias . '.xml', $content);
                $xmlfiles[] = array('when' => $this->when, 'name' => $obj->alias . '.xml', 'value' => $this->path . $obj->alias . '.xml');
            } else {
                $loader = new Twig_Loader_String();
                $twig = new Twig_Environment($loader);
                $template = $twig->loadTemplate($this->pattern);
                $name = $template->render(array(
                    'order' => $obj
                ));
                $name = $this->slugify($name);
                file_put_contents($this->path . $name . '.xml', $content);
                $xmlfiles[] = array('when' => $this->when, 'name' => $name . '.xml', 'value' => $this->path . $name . '.xml');
            }

            Zend_Registry::set('xmlexport', $xmlfiles);
        }
    }

    public function slugify($string)
    {
        $ret = $string;
        // remove html line break
        $ret = preg_replace("<br/>", '', $ret);
        // strip all non word chars
        $ret = preg_replace('/\W/u', ' ', $ret);
        // replace all white space sections with a dash
        $ret = preg_replace('/\ +/', '-', $ret);
        // trim dashes
        $ret = preg_replace('/\-$/', '', $ret);
        $ret = preg_replace('/^\-/', '', $ret);
        // convert to lower case
        $ret = strtolower($ret);

        return $ret;
    }

    public function convertSonderToXml($str) {
        /**
        <	&lt;
        >	&gt;
        &	&amp;
        "	&quot;
        '	&apos;
         */
        $str = htmlspecialchars($str, ENT_XML1 | ENT_QUOTES, 'UTF-8', true);

        return $str;
    }
}