<?php
/**
 * Term query
 *
 * @link http://www.elasticsearch.com/docs/elasticsearch/rest_api/query_dsl/term_query/
 * @uses Elastica_Query_Abstract
 * @category Xodoa
 * @package Elastica
 * @author Nicolas Ruflin <spam@ruflin.com>
 */
class Elastica_Filter_And extends Elastica_Filter_Abstract
{
	protected $_terms = array();
	
	public function __construct() {
	}
	
	/**
	 * Adds a term to the term query
	 * 
	 * @param string $key Key to query
	 * @param string|array $value Values(s) for the query. Boost can be set with array
	 */
	public function addField($value) {
		$this->_terms[] = $value;
	}
	
	public function toArray() {
		$args = array();

                foreach($this->_terms as $key) {
                    $args[] = $key->toArray();
                }
		
		return array('and' => $args);
	}
}
