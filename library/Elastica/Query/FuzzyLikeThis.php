<?php
/**
 * Fuzzy query
 *
 * @link http://www.elasticsearch.com/docs/elasticsearch/rest_api/query_dsl/fuzzy_query/
 * @uses Elastica_Query_Abstract
 * @category Xodoa
 * @package Elastica
 * @author Nicolas Ruflin <spam@ruflin.com>
 */
class Elastica_Query_FuzzyLikeThis extends Elastica_Query_Abstract
{
	protected $_fields = array();
    protected $_search = "";
	
	public function addField($fieldName) {
		$this->_fields[] = $fieldName;
	}

    public function addSearch($var) {
        $this->_search = $var;
    }
	
	public function toArray() {
		$args = $this->_fields;
		return array('fuzzy_like_this' => array("fields" => $args, "like_text" => $this->_search));
	}
}
