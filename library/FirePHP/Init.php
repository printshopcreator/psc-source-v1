<?php

// This function is called at the bottom of this page
function FirePHP__main() {

    // Only activate FirePHP if certain header prefixes are found:
    //  * x-wf-
    //  * x-insight
    
    $headers = false;
    if(function_exists('getallheaders')) {
        $headers = getallheaders();
    } else {
        $headers = $_SERVER;
    }

    $activate = false;
    foreach( $headers as $name => $value ) {
        $name = strtolower($name);
        if(substr($name, 0, 5) == 'http_') {
            $name = str_replace(' ', '-', str_replace('_', ' ', substr($name, 5)));
        }
        if(substr($name, 0, 5)=='x-wf-') {
            $activate = true;
        } else
        if(substr($name, 0, 9)=='x-insight') {
            $activate = true;
        }
    }
 
    if($activate) {

        set_include_path(get_include_path() . PATH_SEPARATOR . dirname(dirname(__FILE__)));
        
        require_once('FirePHP/Insight.php');
        
        FirePHP::setInstance(new FirePHP_Insight());
        
        Insight_Helper__main();

        FirePHP::plugin('firephp')->trapProblems();
        FirePHP::plugin('firephp')->recordEnvironment(
            FirePHP::to('request')->console('Environment')->on('Show Environment')
        );
    
    } else {
    
        set_include_path(get_include_path() . PATH_SEPARATOR . dirname(dirname(__FILE__)));

        require_once('FirePHP/Insight.php');

        FirePHP::setInstance(new FirePHP_Insight());

        Insight_Helper__main();
    }
}

FirePHP__main();
