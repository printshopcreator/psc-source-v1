<?php
/**
 * Map request parameters to action method
 * @author Thomas Peterson
 * @licence public domain
 */
class TP_Action extends Zend_Controller_Action
{
    /**
     * Dispatch the requested action
     *
     * @param string $action Method name of action
     * @return void
     */
    public function dispatch($action)
    {
        // Notify helpers of action preDispatch state
        $this->_helper->notifyPreDispatch();

        $this->preDispatch();
        if ($this->getRequest()->isDispatched()) {
            if (null === $this->_classMethods) {
                $this->_classMethods = get_class_methods($this);
            }

            // preDispatch() didn't change the action, so we can continue
            if ($this->getInvokeArg('useCaseSensitiveActions') || in_array($action, $this->_classMethods)) {
                if ($this->getInvokeArg('useCaseSensitiveActions')) {
                    trigger_error('Using case sensitive actions without word separators is deprecated; please do not rely on this "feature"');
                }

                $reflMethod = new Zend_Reflection_Method($this, $action);
                $actionParams = $reflMethod->getParameters();
                $requestParams = $this->_request->getParams();
                $args = array ();
                foreach ($actionParams as $param)
                {
                    // get parameter type
                    if (($reflClass = $param->getClass()) instanceof Zend_Reflection_Class) {
                        $type = $reflClass->getName();
                    } else if ($param->isArray()) {
                        $type = 'array';
                    } else {
                        $type = $param->getType();
                    }

                    // get passed parameter
                    $name = $param->getName();
                    if (isset($requestParams[$name])) {
                        $value = $requestParams[$name];
                    } else if ($param->isDefaultValueAvailable()) {
                        $value = $param->getDefaultValue();
                        $type = '';
                    } else {
                    	$docBlock = $reflMethod->getDocblock();
                    	if (($tagRefl = $docBlock->getTag("require_$name")) instanceof Zend_Reflection_Docblock_Tag) {
                    		$tryClass = trim($tagRefl->getDescription());
                    		if (class_exists($tryClass, true))
                                throw new $tryClass("Missing value for argument $name");
                    		else
                                throw new Zend_Controller_Action_Exception("Missing value for argument $name");
                    	}
                        $value = null;
                    }

                    // fix value type
                    $basicTypes = array(
                        'int', 'integer', 'bool', 'boolean',
                        'string', 'array', 'object',
                        'double', 'float'
                    );
                    if (in_array($type, $basicTypes)) settype($value, $type);
                    else if (strlen($type) && class_exists($type, true)) $value = new $type($value);

                    $args[] = $value;
                }
                // dispatch the action
                call_user_func_array(array($this, $action), $args);
            } else {
                $this->__call($action, array());
            }
            $this->postDispatch();
        }

        // whats actually important here is that this action controller is
        // shutting down, regardless of dispatching; notify the helpers of this
        // state
        $this->_helper->notifyPostDispatch();
    }
}