<?php

class TP_Admin_Controller_Action extends Zend_Controller_Action {

    protected $translate = null;
    protected $shop = null;
    protected $shopIds = array();
    protected $user = null;
    protected $highRole = null;
    protected $highRoleName = null;
    public $install = null;

    const CONTEXT_JSON = 'json';

    public function init() {


        $this->view->addHelperPath(APPLICATION_PATH . '/helpers', 'TP_View_Helper');
        $this->view->initialData = array();

        if (stripos($this->_request->getParam('id'), 'S') !== false) {
            $this->view->sid = intval(str_replace('S', '', $this->_request->getParam('id')));
            $this->_request->setParam('sid', intval(str_replace('S', '', $this->_request->getParam('id'))));
        }
        $this->view->type = intval($this->_request->getParam('type'));

        if (($this->_request->getParam('sid') == '' || $this->_request->getParam('sid') == 0) && (stripos($this->_request->getParam('sid'), 'S') === false)) {
            $this->_request->setParam('sid', '');
        }
        if ($this->_request->getParam('sid')) {
            array_push($this->view->initialData, array('sid' => $this->_request->getParam('sid')));
        } else {
            array_push($this->view->initialData, array('sid' => ''));
        }
        if ($this->_request->getParam('uid'))
            array_push($this->view->initialData, array('uid' => $this->_request->getParam('uid')));
        else
            array_push($this->view->initialData, array('uid' => '0'));
        if ($this->_request->getParam('type'))
            array_push($this->view->initialData, array('type' => $this->_request->getParam('type')));

        $conf = Zend_Registry::get('shop');

        $this->_helper->layout->setLayout('admin')
                ->setLayoutPath(APPLICATION_PATH . '/design/admin/layout')
                ->setContentKey('CONTENT');
        $this->view->setScriptPath(APPLICATION_PATH . '/design/admin/templates');

        $translate = Zend_Registry::get('translate');

        $this->translate = $translate;


        $this->shop = Doctrine_Query::create()
                        ->from('Shop s')
                        ->where('s.id = ?')->fetchOne(array($conf['id']));

        $this->install = $this->shop->Install;
        $this->view->shop = $this->shop;


        $this->view->currency = new Zend_Currency(Zend_Registry::get('locale')->getLanguage() . '_' . Zend_Registry::get('locale')->getRegion());

        $user = Zend_Auth::getInstance()->getIdentity();
        $user = Doctrine_Query::create()
                        ->from('Contact m')
                        ->where('id = ?')->fetchOne(array($user['id']));
        if ($user != false) {
            foreach ($user->Roles as $rol) {
                if ($this->highRole == null || $rol->level > $this->highRole->level)
                    $this->highRole = $rol;
            }
        }

        $this->user = $user;
        $this->view->user = $user;
        $this->view->username = "";
        $this->view->firstname = "";
        $this->view->lastname = "";

        if ($this->user) {
            $this->view->username = $this->user->name;
            $this->view->firstname = $this->user->self_firstname;
            $this->view->lastname = $this->user->self_lastname;

            $accessLocal = false;
            $accessRemote = false;

            if ($this->getRequest()->getControllerName() == 'motiv') {


                $valid = Doctrine_Query::create()->select()->from('Motiv m')->where('m.uuid = ? AND m.contact_id = ?', array($this->_getParam('uid'), $this->user->id))->execute()->count();

                if ($valid) {
                    $accessLocal = true;
                    $accessRemote = true;
                }
            }

            if ($this->getRequest()->getControllerName() == 'article') {


                $valid = Doctrine_Query::create()->select()->from('Article m')->where('m.id = ? AND m.contact_id = ?', array($this->_getParam('uid'), $this->user->id))->execute()->count();

                if ($valid) {
                    $accessLocal = true;
                    $accessRemote = true;
                }
            }

            if ($this->getRequest()->getModuleName() == 'service' && $this->getRequest()->getControllerName() == 'index'
                    && ($this->getRequest()->getParam('type') == 'getTranslations' ||
                    ($this->getRequest()->getParam('type') == 'get' && 
                    ($this->getRequest()->getParam('service') == 'articlegroupmarket' || $this->getRequest()->getParam('service') == 'articlegroup' || 
                            $this->getRequest()->getParam('service') == 'articles' || $this->getRequest()->getParam('service') == 'confirmaccount' ||
                            $this->getRequest()->getParam('service') == 'articlethemes' || $this->getRequest()->getParam('service') == 'articlemarketthemes' ||
                            $this->getRequest()->getParam('service') == 'articlemarketthemes' || $this->getRequest()->getParam('service') == 'confirmcontact' 
                            || $this->getRequest()->getParam('service') == 'motivthemes' || $this->getRequest()->getParam('service') == 'motivmarketthemes'))))
            {
                $accessLocal = true;
                $accessRemote = true;
            }

            if ($this->getRequest()->getActionName() == 'loadsavearticletheme' || $this->getRequest()->getActionName() == 'loadsavemotivtheme') {
                $wizard = new TP_ResaleWizard();
                $shopid = $wizard->getArticleShop();
                foreach ($user->ShopContact as $shop) {
                    if ($shop->admin != true)
                        continue;
                    if ($shopid == $shop->shop_id)
                        $accessLocal = true;
                    array_push($this->shopIds, $shop->shop_id);
                    if (str_replace('S', '', $this->_request->getParam('sid')) == $shop->shop_id || str_replace('S', '', $this->_request->getParam('sid')) == '')
                        $accessRemote = true;
                }
            }

            foreach ($user->ShopContact as $shop) {
                if ($shop->admin != true)
                    continue;
                if ($this->shop->id == $shop->shop_id)
                    $accessLocal = true;
                array_push($this->shopIds, $shop->shop_id);
                if (str_replace('S', '', $this->_request->getParam('sid')) == $shop->shop_id || str_replace('S', '', $this->_request->getParam('sid')) == '')
                    $accessRemote = true;
            }
            if (($accessLocal == false || $accessRemote == false) && $this->_getParam('module') != 'default' && $this->_getParam('action') != 'contacts' && $this->_getParam('action') != 'getprintpreviewpdf') {

                foreach ($user->ShopContact as $shop) {
                    var_dump($shop->admin);
                    var_dump($this->shop->id == $shop->shop_id);
                }
                die('You are not allowed'.$this->shop->id);
            }

        }

        $this->view->highRole = $this->highRole;

        Zend_Registry::set('params', $this->getRequest()->getParams());
    }

}