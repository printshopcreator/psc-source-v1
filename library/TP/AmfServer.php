<?php

class TP_AmfServer {


    /**
     * Get AutoFillMotive
     *T
     * @param string ArticleId
     * @return XML User
     */
    public function getAutoFillMotive($ArticleId) {
        $xml = new SimpleXMLElement('<root></root>');
        $xml->addChild('status', 'SUCCESS');
        $xml->addChild('article_id', $ArticleId);

        $motive = new TP_FavMotiv();
        $motivexml = $xml->addChild('motive');
        
        foreach($motive->getAutoFillMotive($ArticleId) as $key => $row) {
            $motivexml->addChild('motiv', $row);
        }

        //$motivexml->addChild('motiv', 'B63991C6-AF77-169B-68DC-97C59EA004FF');
        //$motivexml->addChild('motiv', '0001-584e8cfd-4e271f62-41e2-c505dee8');
        return $xml->asXML();
    }

    /**
     * Get User and Fields
     *
     * @param int ArticleID
     * @return XML User
     */
    public function loadUser($ArticleId, $copy)
    {

        $highRole = null;
        $xml = new SimpleXMLElement('<root></root>');
        $xml->addChild('status', 'SUCCESS');

        $translate = Zend_Registry::get('translate');


        if ($ArticleId == "") {
            $xml = new SimpleXMLElement('<root></root>');
            $xml->addChild('status', 'ERROR');
            $xml->addChild('message', 'ArticleID empty');
            return $xml->asXML();
        }

        $shop = Zend_Registry::get('shop');
        $xml->addChild('install_id', $shop['install_id']);

        if ($copy == 2) {

            $articleSession = new TP_Layoutersession();
            $articleSess = $articleSession->getLayouterArticle($ArticleId);

            $article = Doctrine_Query::create()
                    ->from('Article c')
                    ->where('c.uuid = ?', array($articleSess->getOrgArticleId()))
                    ->fetchOne();
        } else {
            $article = Doctrine_Query::create()
                    ->from('Article c')
                    ->where('c.uuid = ?', array($ArticleId))
                    ->fetchOne();
        }
        if ($article == false) {
            $xml = new SimpleXMLElement('<root></root>');
            $xml->addChild('status', 'ERROR');
            $xml->addChild('message', 'Article Not Found');
            return $xml->asXML();
        }

        if (Zend_Auth::getInstance()->hasIdentity()) {


            $user = Zend_Auth::getInstance()->getIdentity();
            $mode = new Zend_Session_Namespace('adminmode');
            if ($mode->over_ride_contact != false && $mode->over_ride_contact != 'new') {
                $user = Doctrine_Query::create()
                                ->from('Contact m')
                                ->where('id = ?')->fetchOne(array($mode->over_ride_contact));
            } else {
                $user = Doctrine_Query::create()
                                ->from('Contact m')
                                ->where('id = ?')->fetchOne(array($user['id']));
            }
            if ($user != false) {
                foreach ($user->Roles as $rol) {
                    if ($highRole == null || $rol->level > $highRole->level)
                        $highRole = $rol;
                }
            }

            $level = $highRole->level;

            if ($level >= 30 && $level < 40) {
                $level = 0;
                foreach ($user->ShopContact as $sp) {

                    if ($sp->admin == true && $sp->shop_id == $article->shop_id) {
                        $level = 30;
                        break;
                    }
                }
            }

            $xml->addChild('userGroup', $level);

            $system = $xml->addChild('system');
            if ((isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on')) {
                $system->addChild('image_placeholder', 'https://' . $_SERVER["SERVER_NAME"] . '/images/empty_photo.png');
                if (!file_exists(PUBLIC_PATH . '/shops/' . $shop['uid'] . '/layouter/')) {
                    //mkdir(PUBLIC_PATH . '/shops/' . $shop['uid'] . '/layouter/');
                }
                if($shop['layouter_version'] != "") {
                    $system->addChild('custom_dir', 'http://' . $_SERVER["SERVER_NAME"] . '/layouter/shop' . $shop['install_id'] . '/dpdir/');
                }else{
                    $system->addChild('custom_dir', 'http://' . $_SERVER["SERVER_NAME"] . '/shops/' . $shop['uid'] . '/layouter/');
                }
            } else {
                $system->addChild('image_placeholder', 'http://' . $_SERVER["SERVER_NAME"] . '/images/empty_photo.png');
                if (!file_exists(PUBLIC_PATH . '/shops/' . $shop['uid'] . '/layouter/')) {
                    //mkdir(PUBLIC_PATH . '/shops/' . $shop['uid'] . '/layouter/');
                }
                if($shop['layouter_version'] != "") {
                    $system->addChild('custom_dir', 'http://' . $_SERVER["SERVER_NAME"] . '/layouter/shop' . $shop['install_id'] . '/dpdir/');
                }else{
                    $system->addChild('custom_dir', 'http://' . $_SERVER["SERVER_NAME"] . '/shops/' . $shop['uid'] . '/layouter/');
                }
            }
            $system->addChild('date', date('d/m/Y H:i'));

            $l = $system->addChild('language', 'de_DE');
            $l->addAttribute('datafield', 'language');
            $l->addAttribute('type', 'type_string');
            $l->addAttribute('display', $translate->translate('contact_settings_fieldLabel_Language'));

            $un = $system->addChild('username', $user->name);
            $un->addAttribute('datafield', 'username');
            $un->addAttribute('type', 'type_string');
            $un->addAttribute('display', $translate->translate('contact_settings_fieldLabel_Username'));

            $contact = $xml->addChild('datafields');

            $sf = $contact->addChild('order_number', 'ORDER_NUMBER');
            $sf->addAttribute('datafield', 'alias');
            $sf->addAttribute('type', 'type_string');
            $sf->addAttribute('display', $translate->translate('contact_settings_fieldLabel_Order_Number'));

            $sf = $contact->addChild('c1', $user->self_firstname);
            $sf->addAttribute('datafield', 'c1');
            $sf->addAttribute('type', 'type_string');
            $sf->addAttribute('display', 'Contact ' . $translate->translate('contact_settings_fieldLabel_Firstname'));

            $sl = $contact->addChild('c2', $user->self_lastname);
            $sl->addAttribute('datafield', 'c2');
            $sl->addAttribute('type', 'type_string');
            $sl->addAttribute('display', 'Contact ' . $translate->translate('contact_settings_fieldLabel_Lastname_Name'));

            $ss = $contact->addChild('c3', $user->self_street);
            $ss->addAttribute('datafield', 'c3');
            $ss->addAttribute('type', 'type_string');
            $ss->addAttribute('display', 'Contact ' . $translate->translate('contact_settings_fieldLabel_Street'));

            $sh = $contact->addChild('c4', $user->self_house_number);
            $sh->addAttribute('datafield', 'c4');
            $sh->addAttribute('type', 'type_string');
            $sh->addAttribute('display', 'Contact ' . $translate->translate('contact_settings_fieldLabel_Housenumber'));

            $sz = $contact->addChild('c5', $user->self_state);
            $sz->addAttribute('datafield', 'c5');
            $sz->addAttribute('type', 'type_string');
            $sz->addAttribute('display', 'Contact ' . $translate->translate('contact_settings_fieldLabel_Lkz'));

            $sz = $contact->addChild('c6', $user->self_zip);
            $sz->addAttribute('datafield', 'c6');
            $sz->addAttribute('type', 'type_string');
            $sz->addAttribute('display', 'Contact ' . $translate->translate('contact_settings_fieldLabel_Zip'));

            $sc = $contact->addChild('c7', $user->self_city);
            $sc->addAttribute('datafield', 'c7');
            $sc->addAttribute('type', 'type_string');
            $sc->addAttribute('display', 'Contact ' . $translate->translate('contact_settings_fieldLabel_City'));

            $se = $contact->addChild('c8', $user->self_email);
            $se->addAttribute('datafield', 'c8');
            $se->addAttribute('type', 'type_string');
            $se->addAttribute('display', 'Contact ' . $translate->translate('contact_settings_fieldLabel_Email'));

            $se = $contact->addChild('c9', $user->name);
            $se->addAttribute('datafield', 'c9');
            $se->addAttribute('type', 'type_string');
            $se->addAttribute('display', 'Contact ' . $translate->translate('contact_settings_fieldLabel_Name'));

            $se = $contact->addChild('c10', $user->self_department);
            $se->addAttribute('datafield', 'c10');
            $se->addAttribute('type', 'type_string');
            $se->addAttribute('display', 'Contact ' . $translate->translate('contact_settings_fieldLabel_Department'));

            $se = $contact->addChild('ca', $user->self_position);
            $se->addAttribute('datafield', 'c10');
            $se->addAttribute('type', 'type_string');
            $se->addAttribute('display', 'Contact ' . $translate->translate('contact_settings_fieldLabel_position'));

            $se = $contact->addChild('cb', $user->self_title);
            $se->addAttribute('datafield', 'c10');
            $se->addAttribute('type', 'type_string');
            $se->addAttribute('display', 'Contact ' . $translate->translate('contact_settings_fieldLabel_title'));



            $sp = $contact->addChild('c11', $user->self_phone_vorwahl);
            $sp->addAttribute('datafield', 'c11');
            $sp->addAttribute('type', 'type_string');
            $sp->addAttribute('display', 'Contact ' . $translate->translate('contact_settings_fieldLabel_Telefon_Vorwahl'));

            $sp = $contact->addChild('c12', $user->self_phone);
            $sp->addAttribute('datafield', 'c12');
            $sp->addAttribute('type', 'type_string');
            $sp->addAttribute('display', 'Contact ' . $translate->translate('contact_settings_fieldLabel_Telefon'));

            $sp = $contact->addChild('c13', $user->self_phone_durchwahl);
            $sp->addAttribute('datafield', 'c13');
            $sp->addAttribute('type', 'type_string');
            $sp->addAttribute('display', 'Contact ' . $translate->translate('contact_settings_fieldLabel_Telefon_Durchwahl'));

            $sp = $contact->addChild('c21', $user->self_mobile_vorwahl);
            $sp->addAttribute('datafield', 'c21');
            $sp->addAttribute('type', 'type_string');
            $sp->addAttribute('display', 'Contact ' . $translate->translate('contact_settings_fieldLabel_Mobil_Vorwahl'));

            $sp = $contact->addChild('c22', $user->self_phone_mobile);
            $sp->addAttribute('datafield', 'c22');
            $sp->addAttribute('type', 'type_string');
            $sp->addAttribute('display', 'Contact ' . $translate->translate('contact_settings_fieldLabel_Mobil'));

            $sp = $contact->addChild('c23', $user->self_mobile_durchwahl);
            $sp->addAttribute('datafield', 'c23');
            $sp->addAttribute('type', 'type_string');
            $sp->addAttribute('display', 'Contact ' . $translate->translate('contact_settings_fieldLabel_Mobil_Durchwahl'));

            $sp = $contact->addChild('c31', $user->self_fax_vorwahl);
            $sp->addAttribute('datafield', 'c31');
            $sp->addAttribute('type', 'type_string');
            $sp->addAttribute('display', 'Contact ' . $translate->translate('contact_settings_fieldLabel_Fax_Vorwahl'));

            $sp = $contact->addChild('c32', $user->self_fax_phone);
            $sp->addAttribute('datafield', 'c32');
            $sp->addAttribute('type', 'type_string');
            $sp->addAttribute('display', 'Contact ' . $translate->translate('contact_settings_fieldLabel_Fax'));

            $sp = $contact->addChild('c33', $user->self_fax_durchwahl);
            $sp->addAttribute('datafield', 'c33');
            $sp->addAttribute('type', 'type_string');
            $sp->addAttribute('display', 'Contact ' . $translate->translate('contact_settings_fieldLabel_Fax_Durchwahl'));

            $sp = $contact->addChild('c41', $user->self_phone_alternative_type);
            $sp->addAttribute('datafield', 'c41');
            $sp->addAttribute('type', 'type_string');
            $sp->addAttribute('display', 'Contact ' . $translate->translate('contact_settings_fieldLabel_Alternativ_Type'));

            $sp = $contact->addChild('c42', $user->self_phone_alternative);
            $sp->addAttribute('datafield', 'c42');
            $sp->addAttribute('type', 'type_string');
            $sp->addAttribute('display', 'Contact ' . $translate->translate('contact_settings_fieldLabel_Alternativ'));

            $sp = $contact->addChild('c43', $user->self_phone_alternative_durchwahl);
            $sp->addAttribute('datafield', 'c43');
            $sp->addAttribute('type', 'type_string');
            $sp->addAttribute('display', 'Contact ' . $translate->translate('contact_settings_fieldLabel_Alternativ_Durchwahl'));


            $sp = $contact->addChild('c91', $user->bank_name);
            $sp->addAttribute('datafield', 'c91');
            $sp->addAttribute('type', 'type_string');
            $sp->addAttribute('display', 'Contact ' . $translate->translate('contact_settings_fieldLabel_Alternativ_bank_name'));

            $sp = $contact->addChild('c92', $user->bank_kto);
            $sp->addAttribute('datafield', 'c92');
            $sp->addAttribute('type', 'type_string');
            $sp->addAttribute('display', 'Contact ' . $translate->translate('contact_settings_fieldLabel_Alternativ_bank_kto'));

            $sp = $contact->addChild('c93', $user->bank_blz);
            $sp->addAttribute('datafield', 'c93');
            $sp->addAttribute('type', 'type_string');
            $sp->addAttribute('display', 'Contact ' . $translate->translate('contact_settings_fieldLabel_Alternativ_bank_blz'));

            $sp = $contact->addChild('c94', $user->bank_iban);
            $sp->addAttribute('datafield', 'c94');
            $sp->addAttribute('type', 'type_string');
            $sp->addAttribute('display', 'Contact ' . $translate->translate('contact_settings_fieldLabel_Alternativ_bank_iban'));

            $sp = $contact->addChild('c95', $user->bank_bic);
            $sp->addAttribute('datafield', 'c95');
            $sp->addAttribute('type', 'type_string');
            $sp->addAttribute('display', 'Contact ' . $translate->translate('contact_settings_fieldLabel_Alternativ_bank_bic'));

            $sp = $contact->addChild('c96', $user->bank_bank_name);
            $sp->addAttribute('datafield', 'c96');
            $sp->addAttribute('type', 'type_string');
            $sp->addAttribute('display', 'Contact ' . $translate->translate('contact_settings_fieldLabel_Alternativ_bank_bank_name'));



            $sf = $contact->addChild('a1', $user->Account->company);
            $sf->addAttribute('datafield', 'a1');
            $sf->addAttribute('type', 'type_string');
            $sf->addAttribute('display', 'Account ' . $translate->translate('contact_settings_fieldLabel_Company'));

            $ss = $contact->addChild('a3', $user->Account->street);
            $ss->addAttribute('datafield', 'a3');
            $ss->addAttribute('type', 'type_string');
            $ss->addAttribute('display', 'Account ' . $translate->translate('contact_settings_fieldLabel_Street'));

            $sh = $contact->addChild('a4', $user->Account->house_number);
            $sh->addAttribute('datafield', 'a4');
            $sh->addAttribute('type', 'type_string');
            $sh->addAttribute('display', 'Account ' . $translate->translate('contact_settings_fieldLabel_Housenumber'));

            $sz = $contact->addChild('a5', $user->Account->state);
            $sz->addAttribute('datafield', 'a5');
            $sz->addAttribute('type', 'type_string');
            $sz->addAttribute('display', 'Account ' . $translate->translate('contact_settings_fieldLabel_Lkz'));

            $sz = $contact->addChild('a6', $user->Account->zip);
            $sz->addAttribute('datafield', 'a6');
            $sz->addAttribute('type', 'type_string');
            $sz->addAttribute('display', 'Account ' . $translate->translate('contact_settings_fieldLabel_Zip'));

            $sc = $contact->addChild('a7', $user->Account->city);
            $sc->addAttribute('datafield', 'a7');
            $sc->addAttribute('type', 'type_string');
            $sc->addAttribute('display', 'Account ' . $translate->translate('contact_settings_fieldLabel_City'));

            $se = $contact->addChild('a8', $user->Account->email);
            $se->addAttribute('datafield', 'a8');
            $se->addAttribute('type', 'type_string');
            $se->addAttribute('display', 'Account ' . $translate->translate('contact_settings_fieldLabel_Email'));

            $se = $contact->addChild('a9', $user->Account->appendix);
            $se->addAttribute('datafield', 'a9');
            $se->addAttribute('type', 'type_string');
            $se->addAttribute('display', 'Account ' . $translate->translate('contact_settings_fieldLabel_Appendix'));

            $se = $contact->addChild('a10', $user->Account->homepage);
            $se->addAttribute('datafield', 'a10');
            $se->addAttribute('type', 'type_string');
            $se->addAttribute('display', 'Account ' . $translate->translate('Homepage'));

            $se = $contact->addChild('aa', $user->Account->usid);
            $se->addAttribute('datafield', 'aa');
            $se->addAttribute('type', 'type_string');
            $se->addAttribute('display', 'Account ' . $translate->translate('contact_settings_fieldLabel_USID'));

            $sp = $contact->addChild('a11', $user->Account->phone_vorwahl);
            $sp->addAttribute('datafield', 'a11');
            $sp->addAttribute('type', 'type_string');
            $sp->addAttribute('display', 'Account ' . $translate->translate('contact_settings_fieldLabel_Telefon_Vorwahl'));

            $sp = $contact->addChild('a12', $user->Account->phone);
            $sp->addAttribute('datafield', 'a12');
            $sp->addAttribute('type', 'type_string');
            $sp->addAttribute('display', 'Account ' . $translate->translate('contact_settings_fieldLabel_Telefon'));

            $sp = $contact->addChild('a13', $user->Account->phone_durchwahl);
            $sp->addAttribute('datafield', 'a13');
            $sp->addAttribute('type', 'type_string');
            $sp->addAttribute('display', 'Account ' . $translate->translate('contact_settings_fieldLabel_Telefon_Durchwahl'));



            $sp = $contact->addChild('a21', $user->Account->mobile_vorwahl);
            $sp->addAttribute('datafield', 'a21');
            $sp->addAttribute('type', 'type_string');
            $sp->addAttribute('display', 'Account ' . $translate->translate('contact_settings_fieldLabel_Mobil_Vorwahl'));

            $sp = $contact->addChild('a22', $user->Account->mobile);
            $sp->addAttribute('datafield', 'a22');
            $sp->addAttribute('type', 'type_string');
            $sp->addAttribute('display', 'Account ' . $translate->translate('contact_settings_fieldLabel_Mobil'));

            $sp = $contact->addChild('a23', $user->Account->mobile_durchwahl);
            $sp->addAttribute('datafield', 'a23');
            $sp->addAttribute('type', 'type_string');
            $sp->addAttribute('display', 'Account ' . $translate->translate('contact_settings_fieldLabel_Mobil_Durchwahl'));



            $sp = $contact->addChild('a31', $user->Account->fax_vorwahl);
            $sp->addAttribute('datafield', 'a31');
            $sp->addAttribute('type', 'type_string');
            $sp->addAttribute('display', 'Account ' . $translate->translate('contact_settings_fieldLabel_Fax_Vorwahl'));

            $sp = $contact->addChild('a32', $user->Account->fax);
            $sp->addAttribute('datafield', 'a32');
            $sp->addAttribute('type', 'type_string');
            $sp->addAttribute('display', 'Account ' . $translate->translate('contact_settings_fieldLabel_Fax'));

            $sp = $contact->addChild('a33', $user->Account->fax_durchwahl);
            $sp->addAttribute('datafield', 'a33');
            $sp->addAttribute('type', 'type_string');
            $sp->addAttribute('display', 'Account ' . $translate->translate('contact_settings_fieldLabel_Fax_Durchwahl'));



            $sp = $contact->addChild('a41', $user->Account->alternativ_type);
            $sp->addAttribute('datafield', 'a41');
            $sp->addAttribute('type', 'type_string');
            $sp->addAttribute('display', 'Account ' . $translate->translate('contact_settings_fieldLabel_Alternativ_Type'));

            $sp = $contact->addChild('a42', $user->Account->alternativ);
            $sp->addAttribute('datafield', 'a42');
            $sp->addAttribute('type', 'type_string');
            $sp->addAttribute('display', 'Account ' . $translate->translate('contact_settings_fieldLabel_Alternativ'));

            $sp = $contact->addChild('a43', $user->Account->alternativ_durchwahl);
            $sp->addAttribute('datafield', 'a43');
            $sp->addAttribute('type', 'type_string');
            $sp->addAttribute('display', 'Account ' . $translate->translate('contact_settings_fieldLabel_Alternativ_Durchwahl'));


            $sp = $contact->addChild('a91', $user->Account->bank_name);
            $sp->addAttribute('datafield', 'a91');
            $sp->addAttribute('type', 'type_string');
            $sp->addAttribute('display', 'Account ' . $translate->translate('contact_settings_fieldLabel_Alternativ_bank_name'));

            $sp = $contact->addChild('a92', $user->Account->bank_kto);
            $sp->addAttribute('datafield', 'a92');
            $sp->addAttribute('type', 'type_string');
            $sp->addAttribute('display', 'Account ' . $translate->translate('contact_settings_fieldLabel_Alternativ_bank_kto'));

            $sp = $contact->addChild('a93', $user->Account->bank_blz);
            $sp->addAttribute('datafield', 'a93');
            $sp->addAttribute('type', 'type_string');
            $sp->addAttribute('display', 'Account ' . $translate->translate('contact_settings_fieldLabel_Alternativ_bank_blz'));

            $sp = $contact->addChild('a94', $user->Account->bank_iban);
            $sp->addAttribute('datafield', 'a94');
            $sp->addAttribute('type', 'type_string');
            $sp->addAttribute('display', 'Account ' . $translate->translate('contact_settings_fieldLabel_Alternativ_bank_iban'));

            $sp = $contact->addChild('a95', $user->Account->bank_bic);
            $sp->addAttribute('datafield', 'a95');
            $sp->addAttribute('type', 'type_string');
            $sp->addAttribute('display', 'Account ' . $translate->translate('contact_settings_fieldLabel_Alternativ_bank_bic'));

            $sp = $contact->addChild('a96', $user->Account->bank_bank_name);
            $sp->addAttribute('datafield', 'a96');
            $sp->addAttribute('type', 'type_string');
            $sp->addAttribute('display', 'Account ' . $translate->translate('contact_settings_fieldLabel_Alternativ_bank_bank_name'));
        } else {

            $xml->addChild('userGroup', '0');

            $system = $xml->addChild('system');
            if ((isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on')) {
                $system->addChild('image_placeholder', 'https://' . $_SERVER["SERVER_NAME"] . '/images/empty_photo.jpg');
                if (!file_exists(PUBLIC_PATH . '/shops/' . $shop['uid'] . '/layouter/')) {
                    mkdir(PUBLIC_PATH . '/shops/' . $shop['uid'] . '/layouter/');
                }
                if($shop['layouter_version'] != "") {
                    $system->addChild('custom_dir', 'http://' . $_SERVER["SERVER_NAME"] . '/layouter/shop' . $shop['install_id'] . '/dpdir/');
                }else{
                    $system->addChild('custom_dir', 'http://' . $_SERVER["SERVER_NAME"] . '/shops/' . $shop['uid'] . '/layouter/');
                }
            } else {
                $system->addChild('image_placeholder', 'http://' . $_SERVER["SERVER_NAME"] . '/images/empty_photo.jpg');
                if (!file_exists(PUBLIC_PATH . '/shops/' . $shop['uid'] . '/layouter/')) {
                    mkdir(PUBLIC_PATH . '/shops/' . $shop['uid'] . '/layouter/');
                }
                if($shop['layouter_version'] != "") {
                    $system->addChild('custom_dir', 'http://' . $_SERVER["SERVER_NAME"] . '/layouter/shop' . $shop['install_id'] . '/dpdir/');
                }else{
                    $system->addChild('custom_dir', 'http://' . $_SERVER["SERVER_NAME"] . '/shops/' . $shop['uid'] . '/layouter/');
                }
            }
            $system->addChild('date', date('d/m/Y H:i'));

            $l = $system->addChild('language', 'de_DE');
            $l->addAttribute('datafield', 'language');
            $l->addAttribute('type', 'type_string');
            $l->addAttribute('display', $translate->translate('contact_settings_fieldLabel_Language'));

            $un = $system->addChild('username', 'username');
            $un->addAttribute('datafield', 'username');
            $un->addAttribute('type', 'type_string');
            $un->addAttribute('display', $translate->translate('contact_settings_fieldLabel_Username'));

            $contact = $xml->addChild('datafields');

            $sf = $contact->addChild('self_firstname', 'Max');
            $sf->addAttribute('datafield', 'self_firstname');
            $sf->addAttribute('type', 'type_string');
            $sf->addAttribute('display', $translate->translate('contact_settings_fieldLabel_Firstname'));

            $sl = $contact->addChild('self_lastname', 'Mustermann');
            $sl->addAttribute('datafield', 'self_lastname');
            $sl->addAttribute('type', 'type_string');
            $sl->addAttribute('display', $translate->translate('contact_settings_fieldLabel_Lastname'));

            $ss = $contact->addChild('self_street', 'Musterstrasse');
            $ss->addAttribute('datafield', 'self_street');
            $ss->addAttribute('type', 'type_string');
            $ss->addAttribute('display', $translate->translate('contact_settings_fieldLabel_Street'));

            $sh = $contact->addChild('self_house_number', '24b');
            $sh->addAttribute('datafield', 'self_house_number');
            $sh->addAttribute('type', 'type_string');
            $sh->addAttribute('display', $translate->translate('contact_settings_fieldLabel_Housenumber'));

            $sz = $contact->addChild('self_zip', '17506');
            $sz->addAttribute('datafield', 'self_zip');
            $sz->addAttribute('type', 'type_string');
            $sz->addAttribute('display', $translate->translate('contact_settings_fieldLabel_Zip'));

            $sc = $contact->addChild('self_city', 'Gribow');
            $sc->addAttribute('datafield', 'self_city');
            $sc->addAttribute('type', 'type_string');
            $sc->addAttribute('display', $translate->translate('contact_settings_fieldLabel_City'));

            $se = $contact->addChild('self_email', 'info@domain.de');
            $se->addAttribute('datafield', 'self_email');
            $se->addAttribute('type', 'type_string');
            $se->addAttribute('display', $translate->translate('contact_settings_fieldLabel_Email'));

            $sp = $contact->addChild('self_phone', '123456789');
            $sp->addAttribute('datafield', 'self_phone');
            $sp->addAttribute('type', 'type_string');
            $sp->addAttribute('display', $translate->translate('contact_settings_fieldLabel_Mobil'));

            $c = $contact->addChild('company', 'Firma');
            $c->addAttribute('datafield', 'company');
            $c->addAttribute('type', 'type_string');
            $c->addAttribute('display', $translate->translate('contact_settings_fieldLabel_Company'));
        }

        $product = $xml->addChild('product');

        if (Zend_Auth::getInstance()->hasIdentity() && (!isset($copy) || $copy != 2)) {
            $product->addChild('title', $article->title);
        } else {
            $articleSession = new TP_Layoutersession();
            $articleSess = $articleSession->getLayouterArticle($ArticleId);
            $product->addChild('title', $articleSess->getTitle());
        }

        $product->addChild('einleitung', $article->einleitung);
        $product->addChild('info', $article->info);

        $install = Zend_Registry::get('install');

        $agb = $xml->addChild('agb');
        if($shop['custom_agb'] != "") {
            $agb->addChild('motiv_text', $shop['custom_agb']);
        }elseif($install['layouter_agb'] != "") {
        	$agb->addChild('motiv_text', $install['layouter_agb']);
        }else{
        	$agb->addChild('motiv_text', $install['agb']);
        }
        $agb->addChild('motiv_checkbox', $install['check_agb_motiv']);

        return $xml->asXML();
    }

    protected function getSubThemes($themes, $theme_id)
    {
        $th = Doctrine_Query::create()->from('MotivTheme c')->where('c.parent_id = ?', array($theme_id))->execute();
        foreach ($th as $row) {
            $themes[] = $row['id'];
            $themes = $this->getSubThemes($themes, $row['id']);
        }
        return $themes;
    }

    protected function getSubClipartThemes($themes, $theme_id)
    {
        $th = Doctrine_Query::create()->from('ClipartTheme c')->where('c.parent_id = ?', array($theme_id))->execute();
        foreach ($th as $row) {
            $themes[] = $row['id'];
            $themes = $this->getSubClipartThemes($themes, $row['id']);
        }
        return $themes;
    }

    /**
     * addmerkliste
     *
     * @param uuid Motivid
     */
    public function addFavMotiv($motiv_id)
    {
        if (Zend_Auth::getInstance()->hasIdentity()) {
            $user = Zend_Auth::getInstance()->getIdentity();
            if ($motiv_id != "") {

                $motiv = Doctrine_Query::create()
                        ->from('Motiv c')
                        ->where('c.uuid = ?', array($motiv_id))
                        ->fetchOne();


                if ($motiv == false) {
                    $xml = new SimpleXMLElement('<root></root>');
                    $xml->addChild('status', 'ERROR');
                    $xml->addChild('Message', 'Motiv nicht gefunden');
                    return $xml->asXML();
                }

                $found = Doctrine_Query::create()
                        ->from('ContactMotiv c')
                        ->where('c.contact_id = ? AND c.motiv_id = ?', array($user['id'], $motiv->id))
                        ->fetchOne();

                if ($found != false) {
                    $xml = new SimpleXMLElement('<root></root>');
                    $xml->addChild('status', 'ERROR');
                    $xml->addChild('Message', 'Motiv schon vorhanden');
                    return $xml->asXML();
                }

                $contactmotiv = new ContactMotiv();
                $contactmotiv->contact_id = $this->user->id;
                $contactmotiv->motiv_id = $motiv->id;
                $contactmotiv->save();
            }
        } else {

            if ($motiv_id != "") {

                $motiv = Doctrine_Query::create()
                        ->from('Motiv c')
                        ->where('c.uuid = ?', array($motiv_id))
                        ->fetchOne();


                if ($motiv == false) {
                    $xml = new SimpleXMLElement('<root></root>');
                    $xml->addChild('status', 'ERROR');
                    $xml->addChild('Message', 'Motiv nicht gefunden');
                    return $xml->asXML();
                }

                $motivBasket = new TP_FavMotiv();
                $found = $motivBasket->addMotiv($motiv->id);

                if ($found == false) {
                    $xml = new SimpleXMLElement('<root></root>');
                    $xml->addChild('status', 'ERROR');
                    $xml->addChild('Message', 'Motiv schon vorhanden');
                    return $xml->asXML();
                }
            }
        }

        $xml = new SimpleXMLElement('<root></root>');
        $xml->addChild('status', 'SUCCESS');
        return $xml->asXML();
    }

    /**
     * updateMotiveparameter
     * 
     * @param uuid uuid
     * @param string title
     * @param string copyright
     * @param string theme_market
     * @param string theme
     * @param string tags
     * @param float price1
     * @param float price2
     * @param float price3
     * @param boolean display_shop
     * @param boolean display_market
     * @return boolean Motive
     */
    public function updateMotivparameter($uuid, $title = '', $copyright = '', $theme_market_ids = '', $theme_ids = '', $tags = '', $price1 = 0, $price2 = 0, $price3 = 0, $display_shop = false, $display_marktplatz = false)
    {
        if (Zend_Auth::getInstance()->hasIdentity()) {
            $user = Zend_Auth::getInstance()->getIdentity();

            $motive = Doctrine_Query::create()
                            ->from('Motiv a')
                            ->where('a.uuid = ? AND  a.contact_id = ?')->fetchOne(array($uuid, $user['id']));

            if ($motive == false) {
                $xml = new SimpleXMLElement('<root></root>');
                $xml->addChild('status', 'ERROR');
                $xml->addChild('message', 'Not Found');
                return $xml->asXML();
            }

            $motiv->title = $title;
            $motiv->copyright = $copyright;
            $motiv->price1 = $price1;
            $motiv->price2 = $price2;
            $motiv->price3 = $price3;
            $motiv->resale_shop = $display_shop;
            $motiv->resale_market = $display_marktplatz;

            $motiv->save();

            if (isset($theme_ids)) {
                Doctrine_Query::create()
                        ->delete()
                        ->from('MotivThemeMotiv as a')
                        ->where('a.motiv_id = ?', array(intval($motiv->id)))
                        ->execute();

                if ($theme_ids != '') {
                    foreach (explode(',', $theme_ids) as $key) {
                        $group = new MotivThemeMotiv();
                        $group->theme_id = $key;
                        $group->motiv_id = $motiv->id;
                        $group->save();
                    }
                }
            }

            if (isset($theme_market_ids)) {
                Doctrine_Query::create()
                        ->delete()
                        ->from('MotivThemeMarketMotiv as a')
                        ->where('a.motiv_id = ?', array(intval($motiv->id)))
                        ->execute();

                if ($theme_market_ids != '') {
                    foreach (explode(',', $theme_market_ids) as $key) {
                        $group = new MotivThemeMarketMotiv();
                        $group->theme_id = $key;
                        $group->motiv_id = $motiv->id;
                        $group->save();
                    }
                }
            }
            if (isset($tags) && trim($tags) != "") {
                $tagsTemp = array();
                $filter = new TP_Filter_Badwords();
                foreach (explode(',', $tags) as $key) {
                    $tag = strtolower(trim($filter->filter($key)));
                    if ($tag) {
                        $tagsTemp[] = array('name' => $tag, 'url' => Doctrine_Inflector::urlize($tag));
                    }
                }

                $m = TP_Mongo::getInstance();
                $collection = $m->motive;
                $collection->save(array_merge($data->getRiakData(), array('tags' => $tagsTemp, '_id' => $motiv->uuid)));
            }


            $xml = new SimpleXMLElement('<root></root>');
            $xml->addChild('status', 'SUCCESS');
            return $xml->asXML();
        }


        $xml = new SimpleXMLElement('<root></root>');
        $xml->addChild('status', 'ERROR');
        $xml->addChild('message', 'Not logged in');
        return $xml->asXML();
    }

    /**
     * getMotive
     * 
     * @param integer Page
     * @param integer Limit
     * @param integer Theme_id
     * @param string search
     * @return xml Motive
     */
    public function getMotive($page = 1, $limit = 20, $theme_id = 0, $search = '')
    {

        $xml = new SimpleXmlElement("<?xml version='1.0' standalone='yes'?><root></root>");

        $system = $xml->addChild('system');
        $system->addChild('page', $page);

        $xmlthemes = $xml->addChild('motive');
        $cur = new Zend_Currency(Zend_Registry::get('locale')->getLanguage() . '_' . Zend_Registry::get('locale')->getRegion());
        $shop = Zend_Registry::get('shop');

        if ((isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on')) {
            $url = 'https://' . $_SERVER["SERVER_NAME"] . '/market/';
        } else {
            $url = 'http://' . $_SERVER["SERVER_NAME"] . '/market/';
        }

        if ($theme_id != 0) {

            $themes = array($theme_id);
            $themes = $this->getSubThemes($themes, $theme_id);

            if ($shop['pmb']) {
                $motive = Doctrine_Query::create()
                        ->from('Motiv c')
                        ->where('c.install_id = ? AND c.resale_market = 1 AND c.status >= 50
                            AND c.id IN (SELECT d.motiv_id
                            FROM MotivThemeMarketMotiv as d WHERE
                            d.theme_id = ' . implode(' OR d.theme_id =', $themes) . ' GROUP BY d.motiv_id)', array($shop['install_id']));
            } else {
                $motive = Doctrine_Query::create()
                        ->from('Motiv c')
                        ->where('c.shop_id = ? AND c.resale_shop = 1 AND c.status >= 50
                            AND c.id IN (SELECT d.motiv_id
                            FROM MotivThemeMotiv as d WHERE
                            d.theme_id = ' . implode(' OR d.theme_id =', $themes) . ' GROUP BY d.motiv_id)', array($shop['id']));
            }
        } else {
            if ($shop['pmb']) {
                $motive = Doctrine_Query::create()
                        ->from('Motiv c')
                        ->where('c.install_id = ? AND c.resale_market = 1 AND c.status >= 50', array($shop['install_id']));
            } else {
                $motive = Doctrine_Query::create()
                        ->from('Motiv c')
                        ->where('c.shop_id = ? AND c.resale_shop = 1 AND c.status >= 50', array($shop['id']));
            }
        }

        if ($search != '') {
            $query = explode(' ', $search);

            $searchQuery = '*' . implode('* *', $query) . '*';
            $client = new Elastica_Client();
            $index = $client->getIndex('psc');
            $type = $index->getType('motive');
            if ($shop['pmb']) {
                $query = new Elastica_Query();
                $filterSI = new Elastica_Filter_Term(array('install_id' => $shop['install_id']));
                $filterEnable = new Elastica_Filter_Term(array('resale_market' => 1));

                $filterD = new Elastica_Filter_And();
                $filterD->addField($filterSI);
                $filterD->addField($filterEnable);

                $filter = new Elastica_Filter($filterD, new Elastica_Query_MatchAll());
                $filter->addQuery(new Elastica_Query_QueryString($searchQuery));
                $query->setLimit(30);
                $query->addFilter($filter);
                $resultSet = $type->search($query);
            } else {
                $query = new Elastica_Query();
                $filterSI = new Elastica_Filter_Term(array('shop_id' => $shop['id']));
                $filterEnable = new Elastica_Filter_Term(array('resale_shop' => 1));

                $filterD = new Elastica_Filter_And();
                $filterD->addField($filterSI);
                $filterD->addField($filterEnable);

                $filter = new Elastica_Filter($filterD, new Elastica_Query_MatchAll());
                $filter->addQuery(new Elastica_Query_QueryString($searchQuery));
                $query->setLimit(30);
                $query->addFilter($filter);
                $resultSet = $type->search($query);
            }


            $row_ids = array();
            if (count($resultSet->getResults()) > 0) {

                foreach ($resultSet->getResults() as $docinfo) {
                    array_push($row_ids, $docinfo->getId());
                }
            }
            $motive = $motive->andWhereIn('c.uuid', $row_ids);
        }


        $system->addChild('count', $motive->count());

        $motive = $motive->offset(($page - 1) * $limit)->limit($limit)->execute();

        foreach ($motive as $motiv) {

            $xmlmotiv = $xmlthemes->addChild('motiv');
            $xmlmotiv->addAttribute('uuid', $motiv['uuid']);
            if (isset($motiv['install_id'])) {
                $xmlmotiv->addAttribute('url', $url . 'motive/');
            } else {
                $xmlmotiv->addAttribute('url', $url . 'guest/');
            }
            $xmlmotiv->addAttribute('dpi_org', $motiv['dpi_org']);
            $xmlmotiv->addAttribute('visits', $motiv['visits']);
            $xmlmotiv->addAttribute('used', $motiv['used']);
            if ($motiv['rate_count'] > 0) {
                $xmlmotiv->addAttribute('rateing', round($motiv['rate'] / $motiv['rate_count']));
            } else {
                $xmlmotiv->addAttribute('rateing', 0);
            }
            $xmlmotiv->addAttribute('created', $motiv['created']);

            if ($shop['pmb']) {
                if (count($motiv->MotivThemeMarketMotiv) > 0) {
                    $th = $xmlmotiv->addChild('themes');

                    foreach ($motiv->MotivThemeMarketMotiv as $themesmotiv) {
                        $xmltheme = $th->addChild('theme');
                        $xmltheme->addAttribute('uuid', $themesmotiv->MotivTheme->id);
                        $xmltheme->addAttribute('title', $themesmotiv->MotivTheme->title);
                        $xmltheme->addAttribute('parent_id', $themesmotiv->MotivTheme->parent_id);
                    }
                }
            } else {
                if (count($motiv->MotivThemeMotiv) > 0) {
                    $th = $xmlmotiv->addChild('themes');

                    foreach ($motiv->MotivThemeMotiv as $themesmotiv) {
                        $xmltheme = $th->addChild('theme');
                        $xmltheme->addAttribute('uuid', $themesmotiv->MotivTheme->id);
                        $xmltheme->addAttribute('title', $themesmotiv->MotivTheme->title);
                        $xmltheme->addAttribute('parent_id', $themesmotiv->MotivTheme->parent_id);
                    }
                }
            }

            $xmlmotiv->addAttribute('dpi_con', $motiv['dpi_con']);
            $xmlmotiv->addAttribute('copyright', $motiv['copyright']);
            $exif = $xmlmotiv->addChild('exif');
            if ($motiv['exif'] != "") {
                $data = Zend_Json::decode($motiv['exif']);

                foreach ($data as $key => $value) {
                    $exif->addChild(str_replace("exif:", "", $key), $value);
                }
            }


            $attr = $xmlmotiv->addChild('image_orginal', $motiv['file_orginal']);
            $attr->addAttribute('width', $motiv['width']);
            $attr->addAttribute('height', $motiv['height']);
            $attr->addAttribute('typ', $motiv['typ']);

            $attr = $xmlmotiv->addChild('image_work', $motiv['file_work']);
            $attr->addAttribute('width', $motiv['width']);
            $attr->addAttribute('height', $motiv['height']);
            $attr->addAttribute('typ', $motiv['typ']);

            $attr = $xmlmotiv->addChild('image_mid', $motiv['file_mid']);
            $attr->addAttribute('width', $motiv['mid_width']);
            $attr->addAttribute('height', $motiv['mid_height']);

            $attr = $xmlmotiv->addChild('image_thumb', $motiv['file_thumb']);

            $xmlmotiv->addChild('filename', $motiv['title']);


            if ($motiv['price1'] > 0) {
                $xmlmotiv->addChild('price', number_format($motiv['price1'], 2));
                $xmlmotiv->addChild('currency', '€');
            }elseif ($motiv['price2'] > 0) {
                $xmlmotiv->addChild('price', number_format($motiv['price2'], 2));
                $xmlmotiv->addChild('currency', '€');
            }elseif ($motiv['price3'] > 0) {
                $xmlmotiv->addChild('price', number_format($motiv['price3'], 2));
                $xmlmotiv->addChild('currency', '€');
            } else {
                $xmlmotiv->addChild('price', number_format('0', 2));
                $xmlmotiv->addChild('currency', '€');
            }

            $xmlmotiv->addChild('orgfilename', $motiv['orgfilename']);
        }

        return $xml->asXML();
    }

    /**
     * getCliparts
     * 
     * @param integer Page
     * @param integer Limit
     * @param integer Theme_id
     * @param string search
     * @return xml cliparts
     */
    public function getCliparts($page = 1, $limit = 20, $theme_id = 0, $search = '')
    {

        $xml = new SimpleXmlElement("<?xml version='1.0' standalone='yes'?><root></root>");

        $system = $xml->addChild('system');
        $system->addChild('page', $page);

        $xmlthemes = $xml->addChild('cliparts');
        $cur = new Zend_Currency(Zend_Registry::get('locale')->getLanguage() . '_' . Zend_Registry::get('locale')->getRegion());
        $shop = Zend_Registry::get('shop');

        if ((isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on')) {
            $url = 'https://' . $_SERVER["SERVER_NAME"] . '/cliparts/';
        } else {
            $url = 'http://' . $_SERVER["SERVER_NAME"] . '/cliparts/';
        }

        if ($theme_id != 0) {

            $themes = array($theme_id);
            $themes = $this->getClipartThemes($themes, $theme_id);

            $cliparts = Doctrine_Query::create()
                    ->from('Clipart c')
                    ->where('c.install_id = ? AND (c.parent_id = ' . implode(' OR c.parent_id =', $themes) . ')', array($shop['install_id']));
        } else {

            $cliparts = Doctrine_Query::create()
                    ->from('Clipart c')
                    ->where('c.install_id = ?', array($shop['install_id']));
        }

        if ($search != '') {

            $cliparts = $cliparts->andWhere('c.title LIKE ?', array('%' . $search . '%'));
        }


        $system->addChild('count', $cliparts->count());

        $cliparts = $cliparts->offset(($page - 1) * $limit)->limit($limit)->execute();

        foreach ($cliparts as $clipart) {

            $xmlmotiv = $xmlthemes->addChild('motiv');
            $xmlmotiv->addAttribute('uuid', $clipart['uuid']);

            $xmlmotiv->addAttribute('url', $url);

            $xmlmotiv->addAttribute('created', $clipart['created']);


            if ($clipart->ClipartTheme) {
                $th = $xmlmotiv->addChild('themes');

                $xmltheme = $th->addChild('theme');
                $xmltheme->addAttribute('uuid', $clipart->ClipartTheme->id);
                $xmltheme->addAttribute('title', $clipart->ClipartTheme->title);
                $xmltheme->addAttribute('parent_id', $clipart->ClipartTheme->parent_id);
            }


            $xmlmotiv->addChild('filename', $motiv['xml']);
            $xmlmotiv->addChild('editable', $motiv['editable']);
            $xmlmotiv->addChild('title', $motiv['title']);
            $xmlmotiv->addChild('price', number_format('0', 2));
            $xmlmotiv->addChild('plotter', $motiv['plotter']);
            $xmlmotiv->addChild('preview', $motiv['preview']);
            
            $xmlmotiv->addChild('options', $motiv['options']);
            
            $xmlmotiv->addChild('tags', $motiv['tags']);
            
            $xmlmotiv->addChild('currency', '€');
        }

        return $xml->asXML();
    }

    /**
     * Save Clipart
     *
     * @param string Articlename
     * @param int themeid
     * @param float price
     * @param string colors
     * @param boolean editable
     * @param string xml
     * @param boolean plotter
     * @param string preview
     * @param string options
     * @param string tags
     * @return xml xml
     */
    public function saveClipart($name = '', $themeid = 0, $price = 0, $colors = '', $editable = 0, $xml = '', $plotter = 0, $preview = "", $options = "", $tags = "")
    {

        $shop = Zend_Registry::get('shop');
        $user = Zend_Auth::getInstance()->getIdentity();
        $mode = new Zend_Session_Namespace('adminmode');
        if ($mode->over_ride_contact != false && $mode->over_ride_contact != 'new') {
            $user = Doctrine_Query::create()
                            ->from('Contact m')
                            ->where('id = ?')->fetchOne(array($mode->over_ride_contact));
        } else {
            $user = Doctrine_Query::create()
                            ->from('Contact m')
                            ->where('id = ?')->fetchOne(array($user['id']));
        }
        if ($user != false) {
            foreach ($user->Roles as $rol) {
                if ($highRole == null || $rol->level > $highRole->level) {
                    $highRole = $rol;
                }
            }
        }
        if ($highRole->level >= 40) {

            try {
                $clp = new Clipart();
                $clp->install_id = $shop['install_id'];
                $clp->title = $name;
                $clp->parent_id = $themeid;
                $clp->clip_price = $price;
                $clp->clip_colors = $colors;
                $clp->editable = $editable;
                $clp->xml = $xml;
                $clp->plotter = $plotter;
                $clp->preview = $preview;
                $clp->options = $options;
                $clp->tags = $tags;

                $uuid = TP_Util::uuid();
                $clp->uuid = $uuid;
                $clp->xml = $uuid . '.svg';

                $clp->webbasedlayouter = 1;
                $clp->save();

                if (!file_exists(PUBLIC_PATH . '/cliparts/' . $clp->install_id)) {
                    mkdir(PUBLIC_PATH . '/cliparts/' . $clp->install_id, 0777, true);
                }
                file_put_contents(PUBLIC_PATH . '/cliparts/' . $clp->install_id . '/' . $clp->xml, $xml);

                $xml = new SimpleXMLElement('<root></root>');
                $xml->addChild('status', 'SUCCESS');
                return $xml->asXML();
            } catch (Exception $e) {
                $xml = new SimpleXMLElement('<root></root>');
                $xml->addChild('status', 'ERROR');
                $xml->addChild('message', $e->getMessage());
                return $xml->asXML();
            }
        }

        $xml = new SimpleXMLElement('<root></root>');
        $xml->addChild('status', 'ERROR');
        $xml->addChild('message', 'Not allowed');
        return $xml->asXML();
    }

    /**
     * getclipartthemes
     * 
     * @return xml Motivxml
     */
    public function loadClipartThemes()
    {

        $xml = new SimpleXmlElement("<?xml version='1.0' standalone='yes'?><root></root>");
        $xmlthemes = $xml->addChild('themes');

        $shop = Zend_Registry::get('shop');

        $themesmotive = Doctrine_Query::create()->from('ClipartTheme c')->where('c.install_id = ? AND c.parent_id = 0', array(
                    $shop['install_id']))->orderBy('c.title ASC')->execute();

        foreach ($themesmotive as $themesmotiv) {
            $xmltheme = $xmlthemes->addChild('theme');
            $xmltheme->addAttribute('uuid', $themesmotiv->id);
            $xmltheme->addAttribute('title', $themesmotiv->title);
            $xmltheme->addAttribute('parent_id', $themesmotiv->parent_id);

            $this->getClipartThemes($xmltheme, $themesmotiv);
        }

        return $xml->asXML();
    }

    protected function getClipartThemes($xmlthemes, $theme)
    {

        $themesmotive = Doctrine_Query::create()->select()->from('ClipartTheme t')->where('t.parent_id = ?', array($theme->id))->execute();
        if (count($themesmotive) > 0) {

            $xmlthemes = $xmlthemes->addChild('themes');
            foreach ($themesmotive as $themesmotiv) {
                $xmltheme = $xmlthemes->addChild('theme');
                $xmltheme->addAttribute('uuid', $themesmotiv->id);
                $xmltheme->addAttribute('title', $themesmotiv->title);
                $xmltheme->addAttribute('parent_id', $themesmotiv->parent_id);

                $this->getMotivThemes($xmltheme, $themesmotiv);
            }
        }
    }

    /**
     * getMotivthemes
     * 
     * @return xml Motivxml
     */
    public function loadMotivThemes()
    {

        $xml = new SimpleXmlElement("<?xml version='1.0' standalone='yes'?><root></root>");
        $xmlthemes = $xml->addChild('themes');

        $shop = Zend_Registry::get('shop');

        $themesmotive = Doctrine_Query::create()->from('MotivTheme c')->where('c.shop_id = ? AND c.parent_id = 0', array(
                    $shop['id']))->orderBy('c.title ASC')->execute();

        foreach ($themesmotive as $themesmotiv) {
            $xmltheme = $xmlthemes->addChild('theme');
            $xmltheme->addAttribute('uuid', $themesmotiv->id);
            $xmltheme->addAttribute('title', $themesmotiv->title);
            $xmltheme->addAttribute('parent_id', $themesmotiv->parent_id);

            $this->getMotivThemes($xmltheme, $themesmotiv);
        }

        return $xml->asXML();
    }

    protected function getMotivThemes($xmlthemes, $theme)
    {

        $themesmotive = Doctrine_Query::create()->select()->from('MotivTheme t')->where('t.parent_id = ?', array($theme->id))->execute();
        if (count($themesmotive) > 0) {

            $xmlthemes = $xmlthemes->addChild('themes');
            foreach ($themesmotive as $themesmotiv) {
                $xmltheme = $xmlthemes->addChild('theme');
                $xmltheme->addAttribute('uuid', $themesmotiv->id);
                $xmltheme->addAttribute('title', $themesmotiv->title);
                $xmltheme->addAttribute('parent_id', $themesmotiv->parent_id);

                $this->getMotivThemes($xmltheme, $themesmotiv);
            }
        }
    }

    /**
     * Get Articlexml
     *
     * @param int ArticleID
     * @return xml Articlexml
     */
    public function loadProductConfigXml($ArticleId, $copy)
    {

        $shop = Zend_Registry::get('shop');

        if (intval($ArticleId) == 0) {
            throw new Zend_Amf_Exception;
        }

        if (Zend_Auth::getInstance()->hasIdentity() && (!isset($copy) || $copy != 2)) {
            try {

                $article = Doctrine_Query::create()
                        ->from('Article c')
                        ->where('c.uuid = ?', array($ArticleId))
                        ->fetchOne();

                if ($article == false) {
                    
                }
            } catch (Exception $e) {
                Zend_Registry::get('logger')->CRIT($e->getTraceAsString());
            }

            $data = file_get_contents(APPLICATION_PATH . '/../market/products/' . $article->a6_directory . '/' . $article->a6_xmlconfigfile);
            Zend_Registry::get('log')->debug(APPLICATION_PATH . '/../market/products/' . $article->a6_directory . '/' . $article->a6_xmlconfigfile);
        } else {



            $articleSession = new TP_Layoutersession();
            $articleSess = $articleSession->getLayouterArticle($ArticleId);
            $data = $articleSess->getConfigXml();

            if($data == '') {
                $article = Doctrine_Query::create()
                        ->from('Article c')
                        ->where('c.uuid = ?', array($articleSess->getOrgArticleId()))
                        ->fetchOne();
                if($article) {
                    $data = file_get_contents(APPLICATION_PATH . '/../market/products/' . $article->a6_directory . '/' . $article->a6_xmlconfigfile);
                    Zend_Registry::get('log')->debug(APPLICATION_PATH . '/../market/products/' . $article->a6_directory . '/' . $article->a6_xmlconfigfile);
                }
            }

        }

        if ($data == '') {
            $data = '<?xml version="1.0" encoding="UTF-8"?>
<root>
    <status>INIT</status>
</root>';
        }
        return $data;
    }

    /*
     * Save Layouter Presets
     * 
     * @param string Data
     * return string
     */

    public function saveLayouterPresets($data)
    {
        $shop = Zend_Registry::get('shop');
        try {
            $shop = Doctrine_Query::create()
                    ->from('Shop c')
                    ->where('c.id = ?', array($shop['id']))
                    ->fetchOne();
            $shop->layouter_presets = $data;
            $shop->save();
        } catch (Exception $e) {
            $xml = new SimpleXMLElement('<root></root>');
            $xml->addChild('status', 'ERROR');
            $xml->addChild('message', 'Fehler beim Speichern der Layouter Presets');
            return $xml->asXML();
        }

        $xml = new SimpleXMLElement('<root></root>');
        $xml->addChild('status', 'SUCCESS');
        return $xml->asXML();
    }

    /**
     * Load Layouter Presets
     * 
     * @return xml LayouterXml
     */
    public function loadLayouterPresets()
    {
        $shop = Zend_Registry::get('shop');
        if ($shop['layouter_presets'] == "") {
            $xml = new SimpleXMLElement('<root></root>');
            $xml->addChild('status', 'ERROR');
            $xml->addChild('message', 'NO Content');
            return $xml->asXML();
        }
        return $shop['layouter_presets'];
    }

    /**
     * Save DesignTemplate
     *
     * @param string orgid
     * @param string title
     * @param string einleitung
     * @param string description
     * @param string price
     * @param string enable
     * @param string marktplatz
     * @param string resale
     * @param string not_edit
     * @param string config
     * @param string pages
     * @param string pagetemplates
     * @param string pageobjects
     * @param string xmlfo
     * @param string previewxmlfo
     * @param string extendpreviewxmlfo
     * @return xml Status
     */
    public function saveDesignTemplate($orgid, $title, $einleitung, $description, $price, $enable, $marktplatz, $resale, $not_edit, $config, $pages, $pagetemplates, $pageobjects, $xmlfo, $previewxmlfo, $extendpreviewxmlfo)
    {
        if(!Zend_Auth::getInstance()->hasIdentity()) {
            $xml = new SimpleXMLElement('<root></root>');
            $xml->addChild('status', 'ERROR');
            $xml->addChild('uuid', $orgid);
            $xml->addChild('message', 'Sie sind nicht angemeldet');
            return $xml->asXML();
        }

        $article = Doctrine_Query::create()->from('Article c')->where('c.uuid = ?', array($orgid))->fetchOne();
        $user = Zend_Auth::getInstance()->getIdentity();
        if(!$article) {

            $article = Doctrine_Query::create()->from('LayouterSession c')->where('c.uuid = ?', array($orgid))->fetchOne();
            if(!$article) {
                $xml = new SimpleXMLElement('<root></root>');
                $xml->addChild('status', 'ERROR');
                $xml->addChild('uuid', $orgid);
                $xml->addChild('message', 'Orginal Artikel nicht gefunden');
                return $xml->asXML();
            }

            $article = Doctrine_Query::create()->from('Article c')->where('c.uuid = ?', array($article->org_article_id))->fetchOne();
            if(!$article) {
                $xml = new SimpleXMLElement('<root></root>');
                $xml->addChild('status', 'ERROR');
                $xml->addChild('uuid', $orgid);
                $xml->addChild('message', 'Orginal Artikel nicht gefunden');
                return $xml->asXML();
            }

        }

        $orgid = $article->id;
        if($article->a6_org_article != 0) {
            $orgid = $article->a6_org_article;
        }

        $ag = array();
        foreach($article->ArticleGroupArticle as $row) {
            $ag[] = $row->articlegroup_id;
        }
        $agm = array();
        foreach($article->ArticleGroupMarketArticle as $row) {
            $agm[] = $row->articlegroup_id;
        }

        $article = $article->copy();
        $article->uuid = "";
        $article->contact_id = $user ['id'];
        $article->private = true;

        $article->a6_org_article = $orgid;
        $article->save();

        $articles = Zend_Registry::get('articles');

        $articleObj = new $articles [$article->typ] ( );

        if (method_exists($articleObj, 'copyPreDispatch'))
        {
            $articleObj->copyPreDispatch($article);
        }
        

        $article->title = $title;
        $article->info = $description;
        $article->einleitung = $einleitung;
        $article->private = false;
        $article->a6_resale_price = (str_replace(',', '.', $price)/1.19);
        $article->display_market = $marktplatz;
        $article->resale = false;
        $article->resale_design = true;

        $article->not_edit = $not_edit;
        $article->resale_design = $resale;
        $article->file = "";
        $article->file1 = "";
        $article->visits = 0;
        $article->confirm = false;
        $article->confirmone = false;
        $article->used = 0;
        $article->pos = 0;

        $article->rate = 0;
        $article->rate_count = 0;

        $article->render_new_preview_image = true;
        $article->render_new_preview_pdf = true;
        $article->render_new_preview_gallery = true;

        $article->save();

        foreach($ag as $rw) {
            $g = new ArticleGroupArticle();
            $g->article_id = $article->id;
            $g->articlegroup_id = $rw;
            $g->save();
        }

        foreach($agm as $rw) {
            $g = new ArticleGroupMarketArticle();
            $g->article_id = $article->id;
            $g->articlegroup_id = $rw;
            $g->save();
        }
        
        $data = file_put_contents(APPLICATION_PATH . '/../market/products/' . $article->a6_directory . '/' . $article->a6_xmlconfigfile, $config);
        $data = file_put_contents(APPLICATION_PATH . '/../market/products/' . $article->a6_directory . '/' . $article->a6_xmlpagesfile, $pages);
        $data = file_put_contents(APPLICATION_PATH . '/../market/products/' . $article->a6_directory . '/' . $article->a6_xmlpagetemplatesfile, $pagetemplates);
        $data = file_put_contents(APPLICATION_PATH . '/../market/products/' . $article->a6_directory . '/' . $article->a6_xmlpageobjectsfile, $pageobjects);
        $data = file_put_contents(APPLICATION_PATH . '/../market/products/' . $article->a6_directory . '/' . $article->a6_xmlxslfofile, $xmlfo);
        $data = file_put_contents(APPLICATION_PATH . '/../market/products/' . $article->a6_directory . '/' . $article->a6_xmlpreviewxslfofile, $previewxmlfo);
        $data = file_put_contents(APPLICATION_PATH . '/../market/products/' . $article->a6_directory . '/' . $article->a6_xmlextendpreviewxslfofile, $extendpreviewxmlfo);
        
        $xml = new SimpleXMLElement('<root></root>');
        $xml->addChild('status', 'SUCCESS');
        $xml->addChild('uuid', $article->uuid);
        $xml->addChild('message', '');
        return $xml->asXML();  
    } 

    /*
     * Save Articlexml
     *
     * @param int ArticleID
     * @param string Data
     * return boolean if save succeded
     */

    public function saveProductConfigXml($ArticleId, $data, $copy)
    {

        if (intval($ArticleId) == 0) {
            throw new Zend_Amf_Exception;
        }

        $shop = Zend_Registry::get('shop');

        if (isset($copy) && ($copy == 3 || $copy == 2)) {

            $articleSession = new TP_Layoutersession();
            $articleSess = $articleSession->getLayouterArticle($ArticleId);
            $articleSess->setConfigXml($data);
        } elseif (Zend_Auth::getInstance()->hasIdentity()) {

            $article = Doctrine_Query::create()
                    ->from('Article c')
                    ->where('c.uuid = ?', array($ArticleId))
                    ->fetchOne();

            if ($article == false) {
                throw new Zend_Amf_Exception;
            }

            $data = file_put_contents(APPLICATION_PATH . '/../market/products/' . $article->a6_directory . '/' . $article->a6_xmlconfigfile, $data);
        } else {
            $articleSession = new TP_Layoutersession();
            $articleSess = $articleSession->getLayouterArticle($ArticleId);
            $articleSess->setConfigXml($data);
        }

        if ($data == false) {
            throw new Zend_Amf_Exception;
        }
        return $data;
    }

    /**
     * Get ArticlePagesxml
     *
     * @param int ArticleID
     * @return xml Articlexml
     */
    public function loadProductPagesXml($ArticleId, $copy)
    {

        $shop = Zend_Registry::get('shop');

        if (intval($ArticleId) == 0) {
            throw new Zend_Amf_Exception;
        }

        $basket = new TP_Basket();

        if (Zend_Auth::getInstance()->hasIdentity() && (!isset($copy) || $copy != 2)) {
            try {

                $article = Doctrine_Query::create()
                        ->from('Article c')
                        ->where('c.uuid = ?', array($ArticleId))
                        ->fetchOne();

                if ($article == false) {
                    
                }
            } catch (Exception $e) {
                Zend_Registry::get('log')->CRIT($e->getTraceAsString());
            }

            if (!file_exists(APPLICATION_PATH . '/../market/products/' . $article->a6_directory . '/' . $article->a6_xmlpagesfile)) {

                $path = str_split($article->id);

                if (!file_exists(APPLICATION_PATH . '/../market/products/' . implode('/', $path) . '')) {
                    $an = '';
                    foreach ($path as $pt) {
                        $an.=$pt . '/';
                        if (!file_exists(APPLICATION_PATH . '/../market/products/' . $an))
                            mkdir(APPLICATION_PATH . '/../market/products/' . $an);
                        if (!file_exists(APPLICATION_PATH . '/../market/preview/' . $an))
                            mkdir(APPLICATION_PATH . '/../market/preview/' . $an);
                    }
                }


                $article->a6_directory = implode('/', $path) . '/';

                copy(APPLICATION_PATH . '/../market/products/configdummy.xml', APPLICATION_PATH . '/../market/products/' . implode('/', $path) . '/config.xml');
                copy(APPLICATION_PATH . '/../market/products/pagesdummy.xml', APPLICATION_PATH . '/../market/products/' . implode('/', $path) . '/pages.xml');
                copy(APPLICATION_PATH . '/../market/products/pagetemplatesdummy.xml', APPLICATION_PATH . '/../market/products/' . implode('/', $path) . '/pagetemplates.xml');
                copy(APPLICATION_PATH . '/../market/products/pageobjectsdummy.xml', APPLICATION_PATH . '/../market/products/' . implode('/', $path) . '/pageobjects.xml');
                copy(APPLICATION_PATH . '/../market/products/xslfodummy.fop', APPLICATION_PATH . '/../market/products/' . implode('/', $path) . '/xslfo.fop');
                copy(APPLICATION_PATH . '/../market/products/previewxslfodummy.fop', APPLICATION_PATH . '/../market/products/' . implode('/', $path) . '/previewxslfo.fop');
                copy(APPLICATION_PATH . '/../market/products/extendpreviewxslfodummy.fop', APPLICATION_PATH . '/../market/products/' . implode('/', $path) . '/extendpreviewxslfo.fop');

                $article->a6_xmlconfigfile = 'config.xml';
                $article->a6_xmlpagesfile = 'pages.xml';
                $article->a6_xmlpagetemplatesfile = 'pagetemplates.xml';
                $article->a6_xmlpageobjectsfile = 'pageobjects.xml';
                $article->a6_xmlxslfofile = 'xslfo.fop';
                $article->a6_xmlpreviewxslfofile = 'previewxslfo.fop';
                $article->a6_xmlextendpreviewxslfofile = 'extendpreviewxslfo.fop';
                $article->save();
            }

            $data = file_get_contents(APPLICATION_PATH . '/../market/products/' . $article->a6_directory . '/' . $article->a6_xmlpagesfile);

            $tempProdukt = $basket->getTempProduct();
        }else {
            $articleSession = new TP_Layoutersession();
            $articleSess = $articleSession->getLayouterArticle($ArticleId);
            $data = $articleSess->getPagesXml();

            $tempProdukt = $basket->getTempProduct();
        }

        if ($data == '') {
            throw new Zend_Amf_Exception;
        }

        $params = $tempProdukt->getOptions();

        if ($tempProdukt->getLayouterWidth() != false && $tempProdukt->getLayouterWidth() != "" && $tempProdukt->getLayouterWidth() != 0) {

            if ($tempProdukt->getLayouterWidth() > $tempProdukt->getLayouterHeight()) {
                $width = $tempProdukt->getLayouterWidth();
            } else {
                $width = $tempProdukt->getLayouterHeight();
            }

            if ($tempProdukt->getLayouterWidthEn() == 'cm') {
                $width = $width * 10;
            }

            if ((isset($params['dimensions']) && $params['dimensions'] == 'hoch') || (isset($params['format']) && $params['format'] == 'hoch')) {
                $data = preg_replace('/<a>([0-9]+)<\/a>/', '<a>' . $width . '</a>', $data);
            } else {
                $data = preg_replace('/<b>([0-9]+)<\/b>/', '<b>' . $width . '</b>', $data);
            }
        }
        if ($tempProdukt->getLayouterHeight() != false && $tempProdukt->getLayouterHeight() != "" && $tempProdukt->getLayouterHeight() != 0) {

            if ($tempProdukt->getLayouterWidth() > $tempProdukt->getLayouterHeight()) {
                $height = $tempProdukt->getLayouterHeight();
            } else {
                $height = $tempProdukt->getLayouterWidth();
            }

            if ($tempProdukt->getLayouterHeightEn() == 'cm') {
                $height = $height * 10;
            }

            if ((isset($params['dimensions']) && $params['dimensions'] == 'hoch') || (isset($params['format']) && $params['format'] == 'hoch')) {
                $data = preg_replace('/<b>([0-9]+)<\/b>/', '<b>' . $height . '</b>', $data);
            } else {
                $data = preg_replace('/<a>([0-9]+)<\/a>/', '<a>' . $height . '</a>', $data);
            }
        }

        return $data;
    }

    /*
     * Save ArticlePagesxml
     *
     * @param int ArticleID
     * @param string Data
     * @param int copy
     * return boolean if save succeded
     */

    public function saveProductPagesXml($ArticleId, $data, $copy)
    {
        if (intval($ArticleId) == 0) {
            throw new Zend_Amf_Exception;
        }

        $shop = Zend_Registry::get('shop');
        if (isset($copy) && ($copy == 3 || $copy == 2)) {

            $articleSession = new TP_Layoutersession();
            $articleSess = $articleSession->getLayouterArticle($ArticleId);
            $articleSess->setPagesXml($data);
        } elseif (Zend_Auth::getInstance()->hasIdentity()) {
            $article = Doctrine_Query::create()
                    ->from('Article c')
                    ->where('c.uuid = ?', array($ArticleId))
                    ->fetchOne();

            if ($article == false) {
                throw new Zend_Amf_Exception;
            }

            $data = file_put_contents(APPLICATION_PATH . '/../market/products/' . $article->a6_directory . '/' . $article->a6_xmlpagesfile, $data);
        } else {
            $articleSession = new TP_Layoutersession();
            $articleSess = $articleSession->getLayouterArticle($ArticleId);
            $articleSess->setPagesXml($data);
        }

        if ($data == false) {
            throw new Zend_Amf_Exception;
        }
        $xml = new SimpleXMLElement('<root></root>');
        $xml->addChild('status', 'SUCCESS');
        return $xml->asXML();
    }

    /**
     * Get ArticlePageTemplatesxml
     *
     * @param int ArticleID
     * @return xml Articlexml
     */
    public function loadProductPageTemplatesXml($ArticleId, $copy)
    {

        $shop = Zend_Registry::get('shop');

        if (intval($ArticleId) == 0) {
            throw new Zend_Amf_Exception;
        }

        $basket = new TP_Basket();

        if (Zend_Auth::getInstance()->hasIdentity() && (!isset($copy) || $copy != 2)) {
            try {

                $article = Doctrine_Query::create()
                        ->from('Article c')
                        ->where('c.uuid = ?', array($ArticleId))
                        ->fetchOne();

                if ($article == false) {
                    
                }
            } catch (Exception $e) {
                Zend_Registry::get('logger')->CRIT($e->getTraceAsString());
            }

            $data = file_get_contents(APPLICATION_PATH . '/../market/products/' . $article->a6_directory . '/' . $article->a6_xmlpagetemplatesfile);

            $tempProdukt = $basket->getTempProduct();
        } else {
            $articleSession = new TP_Layoutersession();
            $articleSess = $articleSession->getLayouterArticle($ArticleId);
            $data = $articleSess->getPageTemplates();

            $tempProdukt = $basket->getTempProduct();
        }

        if ($data == '') {
            throw new Zend_Amf_Exception;
        }

        $params = $tempProdukt->getOptions();


        if ($tempProdukt->getLayouterWidth() != false && $tempProdukt->getLayouterWidth() != "" && $tempProdukt->getLayouterWidth() != 0) {

            if ($tempProdukt->getLayouterWidth() > $tempProdukt->getLayouterHeight()) {
                $width = $tempProdukt->getLayouterWidth();
            } else {
                $width = $tempProdukt->getLayouterHeight();
            }

            if ($tempProdukt->getLayouterWidthEn() == 'cm') {
                $width = $width * 10;
            }

            if ((isset($params['dimensions']) && $params['dimensions'] == 'hoch') || (isset($params['format']) && $params['format'] == 'hoch')) {
                $data = preg_replace('/<a>([0-9]+)<\/a>/', '<a>' . $width . '</a>', $data);
            } else {
                $data = preg_replace('/<b>([0-9]+)<\/b>/', '<b>' . $width . '</b>', $data);
            }
        }

        if ($tempProdukt->getLayouterHeight() != false && $tempProdukt->getLayouterHeight() != "" && $tempProdukt->getLayouterHeight() != 0) {

            if ($tempProdukt->getLayouterWidth() > $tempProdukt->getLayouterHeight()) {
                $height = $tempProdukt->getLayouterHeight();
            } else {
                $height = $tempProdukt->getLayouterWidth();
            }

            if ($tempProdukt->getLayouterHeightEn() == 'cm') {
                $height = $height * 10;
            }

            if ((isset($params['dimensions']) && $params['dimensions'] == 'hoch') || (isset($params['format']) && $params['format'] == 'hoch')) {
                $data = preg_replace('/<b>([0-9]+)<\/b>/', '<b>' . $height . '</b>', $data);
            } else {
                $data = preg_replace('/<a>([0-9]+)<\/a>/', '<a>' . $height . '</a>', $data);
            }
        }

        return $data;
    }

    /*
     * Save ArticlePageTemplatesxml
     *
     * @param int ArticleID
     * @param string Data
     * return boolean if save succeded
     */

    public function saveProductPageTemplatesXml($ArticleId, $data, $copy)
    {
        if (intval($ArticleId) == 0) {
            throw new Zend_Amf_Exception;
        }
        $shop = Zend_Registry::get('shop');

        if (isset($copy) && ($copy == 3 || $copy == 2)) {

            $articleSession = new TP_Layoutersession();
            $articleSess = $articleSession->getLayouterArticle($ArticleId);
            $articleSess->setPageTemplates($data);
        } elseif (Zend_Auth::getInstance()->hasIdentity()) {
            $article = Doctrine_Query::create()
                    ->from('Article c')
                    ->where('c.uuid = ?', array($ArticleId))
                    ->fetchOne();

            if ($article == false) {
                throw new Zend_Amf_Exception;
            }

            $data = file_put_contents(APPLICATION_PATH . '/../market/products/' . $article->a6_directory . '/' . $article->a6_xmlpagetemplatesfile, $data);
        } else {
            $articleSession = new TP_Layoutersession();
            $articleSess = $articleSession->getLayouterArticle($ArticleId);
            $articleSess->setPageTemplates($data);
        }

        if ($data == false) {
            throw new Zend_Amf_Exception;
        }
        $xml = new SimpleXMLElement('<root></root>');
        $xml->addChild('status', 'SUCCESS');
        return $xml->asXML();
    }

    /**
     * Get ArticlePageObjectsxml
     *
     * @param int ArticleID
     * @return xml Articlexml
     */
    public function loadProductPageObjectsXml($ArticleId, $copy)
    {

        $shop = Zend_Registry::get('shop');

        if (intval($ArticleId) == 0) {
            throw new Zend_Amf_Exception;
        }

        if (Zend_Auth::getInstance()->hasIdentity() && (!isset($copy) || $copy != 2)) {

            try {

                $article = Doctrine_Query::create()
                        ->from('Article c')
                        ->where('c.uuid = ?', array($ArticleId))
                        ->fetchOne();

                if ($article == false) {
                    
                }
            } catch (Exception $e) {
                Zend_Registry::get('logger')->CRIT($e->getTraceAsString());
            }

            $data = file_get_contents(APPLICATION_PATH . '/../market/products/' . $article->a6_directory . '/' . $article->a6_xmlpageobjectsfile);
            $article->render_new_preview_pdf = true;
            $article->render_new_preview_image = true;
            $article->render_new_preview_gallery = true;
            $article->save();
        } else {

            $articleSession = new TP_Layoutersession();
            $articleSess = $articleSession->getLayouterArticle($ArticleId);
            $data = $articleSess->getPageObjects();
        }

        if ($data == '') {
            throw new Zend_Amf_Exception;
        }
        //return $data;
        return str_replace('<source>', '<work_source>', str_replace('</source>', '</work_source>', $data));
    }

    /*
     * Save ArticlePageObjectsxml
     *
     * @param int ArticleID
     * @param string Data
     * @param boolean copy
     * return boolean if save succeded
     */

    public function saveProductPageObjectsXml($ArticleId, $data, $copy)
    {

        if (intval($ArticleId) == 0) {
            throw new Zend_Amf_Exception;
        }
        $shop = Zend_Registry::get('shop');

        $uuid = "";
        $highRole = null;
        if (Zend_Auth::getInstance()->hasIdentity() && (!isset($copy) || (isset($copy) && ($copy != 3 && $copy != 2)))) {

            $article = Doctrine_Query::create()
                    ->from('Article c')
                    ->where('c.uuid = ?', array($ArticleId))
                    ->fetchOne();

            if ($article == false) {
                throw new Zend_Amf_Exception;
            }


            $user = Zend_Auth::getInstance()->getIdentity();
            $mode = new Zend_Session_Namespace('adminmode');
            if ($mode->over_ride_contact != false && $mode->over_ride_contact != 'new') {
                $user = Doctrine_Query::create()
                                ->from('Contact m')
                                ->where('id = ?')->fetchOne(array($mode->over_ride_contact));
            } else {
                $user = Doctrine_Query::create()
                                ->from('Contact m')
                                ->where('id = ?')->fetchOne(array($user['id']));
            }
            if ($user != false) {
                foreach ($user->Roles as $rol) {
                    if ($highRole == null || $rol->level > $highRole->level)
                        $highRole = $rol;
                }
            }
            if ($article->contact_id != $user->id && $highRole->level < 40) {

                $orgid = $article->id;

                $article = $article->copy();
                $article->uuid = "";
                $article->contact_id = $user->id;
                $article->private = true;
                $article->a6_org_article = $orgid;
                $article->save();

                $rel = new ContactArticle();
                $rel->article_id = $article->id;
                $rel->contact_id = $user->id;
                $rel->save();

                $articles = Zend_Registry::get('articles');

                $articleObj = new $articles[$article->typ]();

                if (method_exists($articleObj, 'copyPreDispatch'))
                    $articleObj->copyPreDispatch($article);

                $datastored = file_put_contents(APPLICATION_PATH . '/../market/products/' . $article->a6_directory . '/' . $article->a6_xmlpageobjectsfile, $data);
                $uuid = $article->uuid;
            }else {

                $datastored = file_put_contents(APPLICATION_PATH . '/../market/products/' . $article->a6_directory . '/' . $article->a6_xmlpageobjectsfile, $data);
                $uuid = $article->uuid;
            }

            preg_match('/<product_name>([\D\d]+?)<\/product_name>/i', $data, $treffer);
            $article->a4_abpreis_must_recalc = true;
            $article->title = $treffer[1];
            $article->save();
        } else {

            $articleSession = new TP_Layoutersession();
            $articleSess = $articleSession->getLayouterArticle($ArticleId);
            $articleSess->setPageObjects($data);

            preg_match('/<product_name>([\D\d]+?)<\/product_name>/i', $data, $treffer);

            $articleSess->setTitle($treffer[1]);

            $articleSess->setIsEdit();
            $uuid = $articleSess->getArticleId();
        }

        if ($data == false) {
            throw new Zend_Amf_Exception;
        }

        $xml = new SimpleXMLElement('<root></root>');
        $xml->addChild('status', 'SUCCESS');
        $xml->addChild('uuid', $uuid);
        return $xml->asXML();
    }

    /**
     * Get Articlexslfo
     *
     * @param int ArticleID
     * @return xml Articlexml
     */
    public function loadProductXslFo($ArticleId, $copy)
    {

        if (intval($ArticleId) == 0) {
            throw new Zend_Amf_Exception;
        }

        $shop = Zend_Registry::get('shop');

        if (Zend_Auth::getInstance()->hasIdentity() && (!isset($copy) || $copy != 2)) {
            $article = Doctrine_Query::create()
                    ->from('Article c')
                    ->where('c.uuid = ?', array($ArticleId))
                    ->fetchOne();

            if ($article == false) {
                
            }

            $data = file_get_contents(APPLICATION_PATH . '/../market/products/' . $article->a6_directory . '/' . $article->a6_xmlxslfofile);
        } else {
            $articleSession = new TP_Layoutersession();
            $articleSess = $articleSession->getLayouterArticle($ArticleId);
            $data = $articleSess->getXslFo();
        }

        if ($data == '') {
            
        }
        return $data;
    }

    /*
     * Save Articlexslfo
     *
     * @param int ArticleID
     * @param string Data
     * return boolean if save succeded
     */

    public function saveProductXslFo($ArticleId, $data, $copy)
    {
        if (intval($ArticleId) == 0) {
            throw new Zend_Amf_Exception;
        }
        $data = str_replace('Frutiger LT Com 67 Bold Cn', 'Frutiger LT Com 67 Bold Condensed', $data);
        $shop = Zend_Registry::get('shop');
        try {
            if (isset($copy) && ($copy == 3 || $copy == 2)) {

                $articleSession = new TP_Layoutersession();
                $articleSess = $articleSession->getLayouterArticle($ArticleId);
                $articleSess->setXslFo($data);
            } elseif (Zend_Auth::getInstance()->hasIdentity()) {
                $article = Doctrine_Query::create()
                        ->from('Article c')
                        ->where('c.uuid = ?', array($ArticleId))
                        ->fetchOne();

                if ($article == false) {
                    throw new Zend_Amf_Exception;
                }

                $data = file_put_contents(APPLICATION_PATH . '/../market/products/' . $article->a6_directory . '/' . $article->a6_xmlxslfofile, $data);
            } else {
                $articleSession = new TP_Layoutersession();
                $articleSess = $articleSession->getLayouterArticle($ArticleId);
                $articleSess->setXslFo($data);
            }
        } catch (Exception $e) {
            Zend_Registry::get('log')->DEBUG('SAVE XSLFO:' . $e->getMessage());
        }
        $xml = new SimpleXMLElement('<root></root>');
        $xml->addChild('status', 'SUCCESS');
        return $xml->asXML();
    }

    /**
     * Get Articlepreviewxslfo
     *
     * @param int ArticleID
     * @return xml Articlexml
     */
    public function loadProductPreviewXslFo($ArticleId, $copy)
    {

        if (intval($ArticleId) == 0) {
            throw new Zend_Amf_Exception;
        }

        $shop = Zend_Registry::get('shop');

        if (Zend_Auth::getInstance()->hasIdentity() && (!isset($copy) || $copy != 2)) {
            $article = Doctrine_Query::create()
                    ->from('Article c')
                    ->where('c.uuid = ?', array($ArticleId))
                    ->fetchOne();

            if ($article == false) {
                
            }

            $data = file_get_contents(APPLICATION_PATH . '/../market/products/' . $article->a6_directory . '/' . $article->a6_xmlpreviewxslfofile);
        } else {
            $articleSession = new TP_Layoutersession();
            $articleSess = $articleSession->getLayouterArticle($ArticleId);
            $data = $articleSess->getPreviewXslFo();
        }

        if ($data == '') {
            
        }
        return $data;
    }

    /*
     * Save Articlexslfo
     *
     * @param int ArticleID
     * @param string Data
     * return boolean if save succeded
     */

    public function saveProductPreviewXslFo($ArticleId, $data, $copy)
    {

        if (intval($ArticleId) == 0) {
            throw new Zend_Amf_Exception;
            Zend_Registry::get('log')->DEBUG('SAVE PREVIEW:' . $ArticleId);
        }
        $data = str_replace('Frutiger LT Com 67 Bold Cn', 'Frutiger LT Com 67 Bold Condensed', $data);

        $shop = Zend_Registry::get('shop');
        try {
            if (isset($copy) && ($copy == 3 || $copy == 2)) {

                $articleSession = new TP_Layoutersession();
                $articleSess = $articleSession->getLayouterArticle($ArticleId);
                $articleSess->setPreviewXslFo($data);
            } elseif (Zend_Auth::getInstance()->hasIdentity()) {
                $article = Doctrine_Query::create()
                        ->from('Article c')
                        ->where('c.uuid = ?', array($ArticleId))
                        ->fetchOne();

                if ($article == false) {
                    throw new Zend_Amf_Exception;
                }

                $data = file_put_contents(APPLICATION_PATH . '/../market/products/' . $article->a6_directory . '/' . $article->a6_xmlpreviewxslfofile, $data);
            } else {
                $articleSession = new TP_Layoutersession();
                $articleSess = $articleSession->getLayouterArticle($ArticleId);
                $articleSess->setPreviewXslFo($data);
            }

            if ($data == '') {
                
            }
        } catch (Exception $e) {
            Zend_Registry::get('log')->DEBUG('SAVE PREVIEW:' . $e->getMessage());
        }
        $xml = new SimpleXMLElement('<root></root>');
        $xml->addChild('status', 'SUCCESS');
        return $xml->asXML();
    }

    /*
     * Save Articlexslfo
     *
     * @param int ArticleID
     * @param string Data
     * return boolean if save succeded
     */

    public function saveProductExtendPreviewXslFo($ArticleId, $data, $copy)
    {
        if (intval($ArticleId) == 0) {
            throw new Zend_Amf_Exception;
            Zend_Registry::get('log')->DEBUG('SAVE EXTENDPREVIEW:' . $ArticleId);
        }

        $data = str_replace('Frutiger LT Com 67 Bold Cn', 'Frutiger LT Com 67 Bold Condensed', $data);
        $shop = Zend_Registry::get('shop');
        try {
            if (isset($copy) && ($copy == 3 || $copy == 2)) {

                $articleSession = new TP_Layoutersession();
                $articleSess = $articleSession->getLayouterArticle($ArticleId);
                $articleSess->setExtendPreviewXslFo($data);
            } elseif (Zend_Auth::getInstance()->hasIdentity()) {
                $article = Doctrine_Query::create()
                        ->from('Article c')
                        ->where('c.uuid = ?', array($ArticleId))
                        ->fetchOne();

                if ($article == false) {
                    throw new Zend_Amf_Exception;
                }
                if ($article->a6_xmlextendpreviewxslfofile == "") {
                    $article->a6_xmlextendpreviewxslfofile = "extendpreviewxslfo.fop";
                    $article->save();
                }
                $data = file_put_contents(APPLICATION_PATH . '/../market/products/' . $article->a6_directory . '/' . $article->a6_xmlextendpreviewxslfofile, $data);
            } else {
                $articleSession = new TP_Layoutersession();
                $articleSess = $articleSession->getLayouterArticle($ArticleId);
                $articleSess->setExtendPreviewXslFo($data);
            }

            if ($data == '') {
                
            }
        } catch (Exception $e) {
            Zend_Registry::get('log')->DEBUG('SAVE PREVIEW:' . $e->getMessage());
        }
        $xml = new SimpleXMLElement('<root></root>');
        $xml->addChild('status', 'SUCCESS');
        return $xml->asXML();
    }

    /**
     * Upload files!
     *
     * @param  $file as a TP_Amf_Contracts_FileVO Object
     * @return string
     * */
    public function upload($file)
    {

        $st = md5(time());

        $data = $file->filedata;
        $uid = $file->uuid;

        $orgfilename = $file->filename;

        $umlaute = Array("/ä/", "/ö/", "/ü/", "/Ä/", "/Ö/", "/Ü/", "/ß/");
        $replace = Array("ae", "oe", "ue", "Ae", "Oe", "Ue", "ss");
        $file->filename = preg_replace($umlaute, $replace, $file->filename);
        $filter = new Zend_Filter_Alnum();
        $file->filename = $filter->filter($file->filename);

        $path_parts = pathinfo($file->filename);

        $finfo = new finfo(FILEINFO_MIME_TYPE);
        $finfo = $finfo->buffer($data);



        if ($finfo != 'image/png' && $finfo != 'image/jpeg' && $finfo != 'image/jpg' && $finfo != 'application/pdf' && $finfo != 'image/tiff' && $finfo != 'image/svg' && $finfo != 'image/svg+xml') {
            $xml = new SimpleXMLElement('<root></root>');
            $xml->addChild('status', 'ERROR');
            $xml->addChild('uuid', $uid);
            $xml->addChild('message', 'Falsche Extension');
            return $xml->asXML();
        }

        $ext = "";
        switch ($finfo) {
            case "image/png":
                $ext = 'png';
                break;
            case "image/jpeg":
            case "image/jpg":
                $ext = 'jpg';
                break;
            case "image/tif":
                $ext = 'tif';
            case "image/tiff":
                $ext = 'tiff';
                break;
            case "image/svg":
                $ext = 'svg';
                break;
            case "image/svg+xml":
                $ext = 'svg';
                break;
            case "application/pdf":
                $ext = 'pdf';
                break;
        }
        $filetype = strrchr($orgfilename, ".");
        if ($filetype == '.ai' || $filetype == '.eps') {
            $ext = 'png';
        }

        $file->filename . '.' . $ext;

        $org = $st . '_' . $path_parts['filename'] . '.' . $ext;

        if (Zend_Auth::getInstance()->hasIdentity()) {

            $user = Zend_Auth::getInstance()->getIdentity();
            $user = Doctrine_Query::create()
                            ->from('Contact m')
                            ->where('id = ?')->fetchOne(array($user['id']));

            $i = 0;
            $shop = Zend_Registry::get('shop');

            if ($shop['market']) {
                $shopMarket = Doctrine_Query::create()->from('Shop c')->where('c.id = ?', array($user->Install->defaultmarket))->fetchOne();
            } else {
                $shopMarket = $shop;
            }

            if (!file_exists(APPLICATION_PATH . '/../market/motive/' . $user->uuid))
                mkdir(APPLICATION_PATH . '/../market/motive/' . $user->uuid);
            file_put_contents(APPLICATION_PATH . '/../market/motive/' . $user->uuid . '/' . $org, $data);

            if ($ext == 'pdf') {

                $im = new imagick(APPLICATION_PATH . '/../market/motive/' . $user->uuid . '/' . $org);
                $pdfcount = $im->getNumberImages();

                while ($i < $pdfcount) {
                    try {

                        $im = new imagick();
                        $im->setResolution(300, 300);

                        $im->readImage(APPLICATION_PATH . '/../market/motive/' . $user->uuid . '/' . $org . '[' . $i . ']');

                        $im->setImageFormat("jpg");
                        $imgwork = $im->clone();
                        $imgwork->writeImage(APPLICATION_PATH . '/../market/motive/' . $user->uuid . '/work_' . $st . '_' . $path_parts['filename'] . '_' . $i . '_' . '.jpg');

                        $imgmid = $imgwork->clone();
                        $imgmid->thumbnailImage(500, null);
                        $imgmid->writeImage(APPLICATION_PATH . '/../market/motive/' . $user->uuid . '/mid_' . $st . '_' . $path_parts['filename'] . '_' . $i . '_' . '.jpg');

                        $imgthumb = $imgmid->clone();
                        $imgthumb->thumbnailImage(null, 110);
                        $imgthumb->writeImage(APPLICATION_PATH . '/../market/motive/' . $user->uuid . '/thumb_' . $st . '_' . $path_parts['filename'] . '_' . $i . '_' . '.jpg');

                        $motive = new Motiv();
                        $motive->Account = $user->Account;
                        $motive->shop_id = $shop['id'];
                        $motive->Contact = $user;

                        $user->motiv_count = $user->motiv_count + 1;
                        $user->save();
                        //$motive->deleted = false;
                        $motive->Install = $user->Install;
                        $motive->resale_shop = false;
                        $motive->resale_market = false;
                        $motive->resale_download = false;

                        $motive->title = $path_parts['filename'];
                        $motive->dpi_org = '300';
                        $motive->dpi_con = '300';
                        $motive->orgfilename = $orgfilename;

                        $motive->file_orginal = $user->uuid . '/' . $org;
                        $motive->file_work = $user->uuid . '/work_' . $st . '_' . $path_parts['filename'] . '_' . $i . '_' . '.jpg';
                        $motive->file_mid = $user->uuid . '/mid_' . $st . '_' . $path_parts['filename'] . '_' . $i . '_' . '.jpg';
                        $motive->file_thumb = $user->uuid . '/thumb_' . $st . '_' . $path_parts['filename'] . '_' . $i . '_' . '.jpg';

                        $motive->status = $shopMarket['default_motiv_status'];
                        $motive->width = $imgwork->getImageWidth();
                        $motive->height = $imgwork->getImageHeight();
                        $motive->mid_width = $imgmid->getImageWidth();
                        $motive->mid_height = $imgmid->getImageHeight();
                        $motive->typ = $im->getFormat();
                        $motive->copyright = $im->getImageProperty('exif:Copyright');
                        $motive->exif = Zend_Json::encode($im->getImageProperties("exif:*"));
                        $motive->save();

                        $contactmotiv = new ContactMotiv();
                        $contactmotiv->contact_id = $user->id;
                        $contactmotiv->motiv_id = $motive->id;
                        $contactmotiv->save();
                        if ((isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on')) {
                            $url = 'https://' . $_SERVER["SERVER_NAME"] . '/market/motive/';
                        } else {
                            $url = 'http://' . $_SERVER["SERVER_NAME"] . '/market/motive/';
                        }

                        $i++;
                    } catch (Exception $e) {
                        $i = 100;
                        Zend_Registry::get('log')->DEBUG($e->getMessage());
                        break;
                    }
                }
            } else {

                try {
                    $im = new imagick();
                    $im->readImage(APPLICATION_PATH . '/../market/motive/' . $user->uuid . '/' . $org);
                    $res = $im->getImageResolution();
                    $res['x'] = intval($res['x']);
                    if ($res['x'] != '300') {
                        $res['x'] = 300;
                        $im->setImageResolution(300, 300);
                    }

                    if ($im->getImageUnits() == 2) {
                        $im->setImageUnits(1);
                        $res['x'] = $res['x'] * 2.54;
                    } elseif ($im->getImageUnits() == 0) {
                        $im->setImageUnits(1);
                    }
                    $im->setimagecompressionquality(100);
                    $im->setcompressionquality(100);
                    $imgwork = $im->clone();
                    $imgwork->writeImage(APPLICATION_PATH . '/../market/motive/' . $user->uuid . '/work_' . $org);
                    $imgmid = $im->clone();
                    $imgmid->thumbnailImage(500, null);
                    $imgmid->writeImage(APPLICATION_PATH . '/../market/motive/' . $user->uuid . '/mid_' . $org);
                    $imgthumb = $im->clone();
                    $imgthumb->thumbnailImage(null, 110);
                    $imgthumb->writeImage(APPLICATION_PATH . '/../market/motive/' . $user->uuid . '/thumb_' . $org);


                    $shop = Zend_Registry::get('shop');

                    $motive = new Motiv();
                    if ($uid != "") {
                        $motive->uuid = $uid;
                    }
                    $motive->Account = $user->Account;
                    $motive->shop_id = $shop['id'];
                    $motive->Contact = $user;

                    $user->motiv_count = $user->motiv_count + 1;
                    $user->save();

                    $motive->Install = $user->Install;
                    $motive->resale_shop = false;
                    $motive->resale_market = false;
                    $motive->resale_download = false;

                    $motive->title = $path_parts['filename'];
                    $motive->dpi_org = $res['x'];
                    $motive->dpi_con = '300';
                    $motive->orgfilename = $orgfilename;

                    $motive->file_orginal = $user->uuid . '/' . $org;
                    $motive->file_work = $user->uuid . '/work_' . $org;
                    $motive->file_mid = $user->uuid . '/mid_' . $org;
                    $motive->file_thumb = $user->uuid . '/thumb_' . $org;
                    $motive->status = $shopMarket['default_motiv_status'];
                    $motive->width = $im->getImageWidth();
                    $motive->height = $im->getImageHeight();
                    $motive->mid_width = $imgmid->getImageWidth();
                    $motive->mid_height = $imgmid->getImageHeight();
                    $motive->typ = $im->getFormat();
                    $motive->copyright = $im->getImageProperty('exif:Copyright');
                    $motive->exif = Zend_Json::encode($im->getImageProperties("exif:*"));
                    $motive->save();

                    $contactmotiv = new ContactMotiv();
                    $contactmotiv->contact_id = $user->id;
                    $contactmotiv->motiv_id = $motive->id;
                    $contactmotiv->save();

                    if ((isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on')) {
                        $url = 'https://' . $_SERVER["SERVER_NAME"] . '/market/motive/';
                    } else {
                        $url = 'http://' . $_SERVER["SERVER_NAME"] . '/market/motive/';
                    }
                } catch (Exception $e) {
					Zend_Registry::get('log')->debug($e->getMessage());
                    $xml = new SimpleXMLElement('<root></root>');
                    $xml->addChild('status', 'ERROR');
                    $xml->addChild('uuid', $uid);
                    $xml->addChild('message', 'Beim Speichern gabs einen Fehler.');
                    return $xml->asXML();
                }
            }
        } else {
            try {
                if ((isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on')) {
                    $url = 'https://' . $_SERVER["SERVER_NAME"] . '/market/guest/';
                } else {
                    $url = 'http://' . $_SERVER["SERVER_NAME"] . '/market/guest/';
                }

                if (!file_exists(APPLICATION_PATH . '/../market/guest/' . Zend_Session::getId()))
                    mkdir(APPLICATION_PATH . '/../market/guest/' . Zend_Session::getId());
                file_put_contents(APPLICATION_PATH . '/../market/guest/' . Zend_Session::getId() . '/' . $org, $data);
                $im = new imagick();
                $im->readImage(APPLICATION_PATH . '/../market/guest/' . Zend_Session::getId() . '/' . $org);
                $res = $im->getImageResolution();
                $res['x'] = intval($res['x']);
                if ($res['x'] != '300') {
                    $res['x'] = 300;
                    $im->setImageResolution(300, 300);
                }

                if ($im->getImageUnits() == 2) {
                    $im->setImageUnits(1);
                    $res['x'] = $res['x'] * 2.54;
                } elseif ($im->getImageUnits() == 0) {
                    $im->setImageUnits(1);
                }
                $im->setimagecompressionquality(100);
                $im->setcompressionquality(100);
                $imgwork = $im->clone();
                $imgwork->writeImage(APPLICATION_PATH . '/../market/guest/' . Zend_Session::getId() . '/work_' . $org);
                $imgmid = $im->clone();
                $imgmid->thumbnailImage(500, null);
                $imgmid->writeImage(APPLICATION_PATH . '/../market/guest/' . Zend_Session::getId() . '/mid_' . $org);
                $imgthumb = $im->clone();
                $imgthumb->thumbnailImage(null, 110);
                $imgthumb->writeImage(APPLICATION_PATH . '/../market/guest/' . Zend_Session::getId() . '/thumb_' . $org);

                $images = new Zend_Session_Namespace('amfmotive');

                $images->unlock();

                if (!$images->motive)
                    $images->motive = array();


                $uuid = TP_Util::uuid();
                if ($uid != "") {
                    $uuid = $uid;
                }
                $images->motive[$uuid] = array(
                    'file_orginal' => Zend_Session::getId() . '/' . $org,
                    'file_work' => Zend_Session::getId() . '/work_' . $org,
                    'file_mid' => Zend_Session::getId() . '/mid_' . $org,
                    'file_thumb' => Zend_Session::getId() . '/thumb_' . $org,
                    'width' => $im->getImageWidth(),
                    'height' => $im->getImageHeight(),
                    'mid_width' => $imgmid->getImageWidth(),
                    'mid_height' => $imgmid->getImageHeight(),
                    'copyright' => $im->getImageProperty('exif:Copyright'),
                    'dpi_org' => $res['x'],
                    'dpi_con' => '300',
                    'typ' => $im->getImageType(),
                    'title' => $path_parts['filename'],
                    'uuid' => $uuid,
                    'org' => $org,
                    'orgfilename' => $orgfilename,
                    'exif' => Zend_Json::encode($im->getImageProperties("exif:*"))
                );

                $motive = $images->motive[$uuid];

                $motivBasket = new TP_FavMotiv();
                $motivBasket->addMotivUUID($uuid);

                $images->lock();
            } catch (Exception $e) {
				Zend_Registry::get('log')->debug($e->getMessage());
                $xml = new SimpleXMLElement('<root></root>');
                $xml->addChild('status', 'ERROR');
                $xml->addChild('uuid', $uid);
                $xml->addChild('message', 'Beim speichern gabs einen Fehler');
                return $xml->asXML();
            }
        }

        $xml = new SimpleXMLElement('<root></root>');
        $xml->addChild('status', 'SUCCESS');

        $xmlmotiv = $xml->addChild('motiv');
        $xmlmotiv->addAttribute('uuid', $motive['uuid']);
        $xmlmotiv->addAttribute('url', $url);
        $xmlmotiv->addAttribute('dpi_org', $res['x']);
        $xmlmotiv->addAttribute('dpi_con', $motive['dpi_con']);
        $xmlmotiv->addAttribute('copyright', $motive['copyright']);
        $exif = $xmlmotiv->addChild('exif');
        if ($motive['exif'] != "") {
            $data = Zend_Json::decode($motive['exif']);

            foreach ($data as $key => $value) {
                if($key != 'exif:MakerNote') {
                    $exif->addChild(str_replace("exif:", "", $key), $value);
                }
            }
        }

        $attr = $xmlmotiv->addChild('image_orginal', $motive['file_orginal']);
        $attr->addAttribute('width', $motive['width']);
        $attr->addAttribute('height', $motive['height']);
        $attr->addAttribute('typ', $motive['typ']);

        $attr = $xmlmotiv->addChild('image_work', $motive['file_work']);
        $attr->addAttribute('width', $motive['width']);
        $attr->addAttribute('height', $motive['height']);
        $attr->addAttribute('typ', $motive['typ']);

        $attr = $xmlmotiv->addChild('image_mid', $motive['file_mid']);
        $attr->addAttribute('width', $motive['mid_width']);
        $attr->addAttribute('height', $motive['mid_height']);

        $attr = $xmlmotiv->addChild('image_thumb', $motive['file_thumb']);

        $xmlmotiv->addChild('filename', $motive['title']);
        $xmlmotiv->addChild('orgfilename', $motive['orgfilename']);

        $xml->addChild('uuid', $uid);

        return $xml->asXML();
    }

    /**
     * Delete Motiv
     *
     * @param int debug
     * @return boolean success
     */
    public function deleteMotiv($id)
    {

        if (Zend_Auth::getInstance()->hasIdentity()) {

            $user = Zend_Auth::getInstance()->getIdentity();
            $motive = Doctrine_Query::create()
                    ->from('Motiv m')
                    ->where('m.contact_id = ? AND m.uuid = ?')
                    ->fetchOne(array($user['id'], $id));

            if ($motive == false) {
                $xml = new SimpleXMLElement('<root></root>');
                $xml->addChild('status', 'ERROR');
                $xml->addChild('message', 'Not Found');
                return $xml->asXML();
            }

            Doctrine_Query::create()
                    ->delete('ContactMotiv a')
                    ->where('a.motiv_id = ?', array($motive->id))->execute();
        } else {

            $motiv = Doctrine_Query::create()
                    ->from('Motiv c')
                    ->where('c.uuid = ?', array($id))
                    ->fetchOne();


            $motivBasket = new TP_FavMotiv();
            $motivBasket->delMotivUUID($id);

            if ($motiv) {
                $motivBasket->delMotiv($motiv->id);
            }
        }

        $xml = new SimpleXMLElement('<root></root>');
        $xml->addChild('status', 'SUCCESS');
        return $xml->asXML();
    }

    /**
     * Get Working Gallery xml
     *
     * @param int debug
     * @param string uuid
     * @return XML WorkingGallery
     */
    public function getWorkingGallery($debug = 0, $uuid, $copy)
    {

        $xml = new SimpleXmlElement("<?xml version='1.0' standalone='yes'?><root></root>");
        $cur = new Zend_Currency(Zend_Registry::get('locale')->getLanguage() . '_' . Zend_Registry::get('locale')->getRegion());


        if (Zend_Auth::getInstance()->hasIdentity() && (!isset($copy) || $copy != 2)) {
            try {

                $article = Doctrine_Query::create()
                        ->from('Article c')
                        ->where('c.uuid = ?', array($uuid))
                        ->fetchOne();

                if ($article == false) {
                    
                }
            } catch (Exception $e) {
                Zend_Registry::get('logger')->CRIT($e->getTraceAsString());
            }

            if ($article->motiv_group == 0) {
                $group = 3;
            } else {
                $group = $article->motiv_group;
            }
        } else {
            $articleSession = new TP_Layoutersession();
            $articleSess = $articleSession->getLayouterArticle($uuid);
            try {

                $article = Doctrine_Query::create()
                        ->from('Article c')
                        ->where('c.uuid = ?', array($articleSess->getOrgArticleId()))
                        ->fetchOne();

                if ($article == false) {
                    
                }
            } catch (Exception $e) {
                Zend_Registry::get('logger')->CRIT($e->getTraceAsString());
            }

            if ($article->motiv_group == 0) {
                $group = 3;
            } else {
                $group = $article->motiv_group;
            }
        }

        if (Zend_Auth::getInstance()->hasIdentity() || $debug == 1) {

            if ($debug != 1) {
                $user = Zend_Auth::getInstance()->getIdentity();
                $user = Doctrine_Query::create()
                                ->from('Motiv m')
                                ->leftJoin('m.ContactMotiv as v')
                                ->where('v.contact_id = ?')->execute(array($user['id']));
            } else {
                $user = Doctrine_Query::create()
                                ->from('Motiv m')
                                ->leftJoin('m.ContactMotiv as v')
                                ->where('v.contact_id = ?')->execute(array(19));
            }
            if ($user != false) {
                $guestMotive = $user->toArray(false);
            } else {
                $guestMotive = array();
            }
            if ((isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on')) {
                $url = 'https://' . $_SERVER["SERVER_NAME"] . '/market/';
            } else {
                $url = 'http://' . $_SERVER["SERVER_NAME"] . '/market/';
            }
            $xml->addChild('guest', false);
        } else {

            $images = new Zend_Session_Namespace('amfmotive');

            $images->unlock();

            if (!$images->motive)
                $images->motive = array();

            $motivBasket = new TP_FavMotiv();

            $motiv = Doctrine_Query::create()
                    ->from('Motiv c')
                    ->where('c.id = ' . implode(' OR c.id = ', array_values($motivBasket->getMotive())))
                    ->execute();


            $guestMotive = array_merge($images->motive, $motiv->toArray(false));

            if ((isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on')) {
                $url = 'https://' . $_SERVER["SERVER_NAME"] . '/market/';
            } else {
                $url = 'http://' . $_SERVER["SERVER_NAME"] . '/market/';
            }

            $xml->addChild('guest', true);
        }

        $xmlmotive = $xml->addChild('gallerys');

        $xmlgallery = $xmlmotive->addChild('gallery');

        $myuploads = $xmlgallery->addChild('name', 'Meine Uploads');

        $xmlgallery->addAttribute('url', $url);

        $xmlmot = $xmlgallery->addChild('motive');

        foreach ($guestMotive as $motiv) {

            $xmlmotiv = $xmlmot->addChild('motiv');
            $xmlmotiv->addAttribute('uuid', $motiv['uuid']);
            if (isset($motiv['install_id'])) {
                $xmlmotiv->addAttribute('url', $url . 'motive/');
            } else {
                $xmlmotiv->addAttribute('url', $url . 'guest/');
            }
            $xmlmotiv->addAttribute('dpi_org', $motiv['dpi_org']);
            $xmlmotiv->addAttribute('dpi_con', $motiv['dpi_con']);
            $xmlmotiv->addAttribute('copyright', $motiv['copyright']);
            $exif = $xmlmotiv->addChild('exif');
            if ($motiv['exif'] != "") {
                $data = Zend_Json::decode($motiv['exif']);

                foreach ($data as $key => $value) {
                    if($key != 'exif:MakerNote') {
                        $exif->addChild(str_replace("exif:", "", $key), $value);
                    }
                }
            }


            $attr = $xmlmotiv->addChild('image_orginal', $motiv['file_orginal']);
            $attr->addAttribute('width', $motiv['width']);
            $attr->addAttribute('height', $motiv['height']);
            $attr->addAttribute('typ', $motiv['typ']);

            $attr = $xmlmotiv->addChild('image_work', $motiv['file_work']);
            $attr->addAttribute('width', $motiv['width']);
            $attr->addAttribute('height', $motiv['height']);
            $attr->addAttribute('typ', $motiv['typ']);

            $attr = $xmlmotiv->addChild('image_mid', $motiv['file_mid']);
            $attr->addAttribute('width', $motiv['mid_width']);
            $attr->addAttribute('height', $motiv['mid_height']);

            $attr = $xmlmotiv->addChild('image_thumb', $motiv['file_thumb']);

            $xmlmotiv->addChild('filename', $motiv['title']);


            if ($motiv['price1'] > 0) {
                $xmlmotiv->addChild('price', number_format($motiv['price1'], 2));
                $xmlmotiv->addChild('currency', '€');
            }elseif ($motiv['price2'] > 0) {
                $xmlmotiv->addChild('price', number_format($motiv['price2'], 2));
                $xmlmotiv->addChild('currency', '€');
            }elseif ($motiv['price3'] > 0) {
                $xmlmotiv->addChild('price', number_format($motiv['price3'], 2));
                $xmlmotiv->addChild('currency', '€');
            } else {
                $xmlmotiv->addChild('price', number_format('0', 2));
                $xmlmotiv->addChild('currency', '€');
            }
            $xmlmotiv->addChild('orgfilename', $motiv['orgfilename']);
        }
        return $xml->asXML();
    }

    /**
     * Get DesignProdukte
     *
     * @param string uuid
     * @param int copy
     * @return XML Designproducts
     */
    public function getDesignProducts($uuid, $copy)
    {

        if ((!isset($copy) || $copy != 2)) {
            $article = Doctrine_Query::create()
                    ->from('Article c')
                    ->where('c.uuid = ?', array($uuid))
                    ->fetchOne();
        } else {
            $articleSession = new TP_Layoutersession();
            $articleSess = $articleSession->getLayouterArticle($uuid);
            $uuid = $articleSess->getOrgArticleId();
            $article = Doctrine_Query::create()
                    ->from('Article c')
                    ->where('c.uuid = ?', array($uuid))
                    ->fetchOne();
        }

        if ($article == false) {
            $xml = new SimpleXmlElement("<?xml version='1.0' standalone='yes'?><root></root>");
            $xmlmotive = $xml->addChild('designs');
            return $xml->asXML();
        }

        $xml = new SimpleXmlElement("<?xml version='1.0' standalone='yes'?><root></root>");
        $xmldesigns = $xml->addChild('designs');

        $shop = Zend_Registry::get('shop');

        if ($shop['market']) {
            if ($article->a6_org_article != 0 && $article->a6_org_article != "") {
                $articles = Doctrine_Query::create()
                        ->from('Article c')
                        ->where('c.a6_org_article = ? AND c.shop_id = ? AND c.private = 0 AND c.enable = 1 AND c.resale_design = 1', array($article->a6_org_article, $shop['id']))
                        ->execute();
            } else {
                $articles = Doctrine_Query::create()
                        ->from('Article c')
                        ->where('c.a6_org_article = ? AND c.shop_id = ? AND c.private = 0 AND c.enable = 1 AND c.resale_design = 1', array($article->id, $shop['id']))
                        ->execute();
            }
        } else {
            if ($article->a6_org_article != 0 && $article->a6_org_article != "") {
                $articles = Doctrine_Query::create()
                        ->from('Article c')
                        ->where('(c.a6_org_article = ? AND c.private = 0 AND c.enable = 1 AND c.resale_design = 1 AND c.display_market = 1) OR (c.id = ?)', array($article->a6_org_article, $article->a6_org_article))
                        ->execute();
            } else {
                $articles = Doctrine_Query::create()
                        ->from('Article c')
                        ->where('(c.a6_org_article = ? AND c.private = 0 AND c.enable = 1 AND c.resale_design = 1 AND c.display_market = 1) OR (c.id = ?)', array($article->id, $article->id))
                        ->execute();
            }
        }

        if ((isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on')) {
            $url = 'https://' . $_SERVER["SERVER_NAME"] . '/';
        } else {
            $url = 'http://' . $_SERVER["SERVER_NAME"] . '/';
        }

        require_once APPLICATION_PATH . '/helpers/Image.php';
        $image = new TP_View_Helper_Image();
        foreach ($articles as $article) {
            $xmldesign = $xmldesigns->addChild('design');
            $xmldesign->addAttribute('uuid', $article->uuid);
            $xmldesign->addAttribute('title', $article->title);
            $xmldesign->addAttribute('url', $article->url);
            $xmldesign->addAttribute('info', strip_tags($article->info));
            $xmldesign->addAttribute('created', $article->created);
            $xmldesign->addAttribute('views', $article->visits);
            $xmldesign->addAttribute('rateing', $article->getRate());
            $xmldesign->addAttribute('used', $article->used);
            $xmldesign->addAttribute('price', $article->a4_abpreis + $article->motive_price);
            $xmldesign->addAttribute('creator', $this->article->Contact->self_firstname . ' ' . $this->article->Contact->self_lastname);

            $xmlthemes = $xmldesign->addChild('themes');

            $themes = array();
            if ($shop['market']) {
                foreach ($article->ArticleTheme as $theme) {
                    $xmltheme = $xmlthemes->addChild('theme');
                    $xmltheme->addAttribute('uuid', $theme->id);
                    $xmltheme->addAttribute('title', $theme->title);
                    $xmltheme->addAttribute('parent_id', $theme->parent_id);
                    $this->recurseThemes($xmlthemes, $theme);
                }
            } else {
                foreach ($article->ArticleMarketTheme as $theme) {
                    $xmltheme = $xmlthemes->addChild('theme');
                    $xmltheme->addAttribute('uuid', $theme->id);
                    $xmltheme->addAttribute('title', $theme->title);
                    $xmltheme->addAttribute('parent_id', $theme->parent_id);
                    $this->recurseThemes($xmlthemes, $theme);
                }
            }

            if ($article->file == "") {
                $xmldesign->addAttribute('thumbnail', $url . $image->thumbnailFop($article->title, 'articlelist', $article->getMarketFile(), true, $article->id));
            } else {
                $xmldesign->addAttribute('thumbnail', $url . $image->thumbnailImage($article->title, 'articlelist', $article->file, true));
            }
        }

        return $xml->asXML();
    }

    protected function recurseThemes($xmlthemes, $theme)
    {

        if ($theme->parent_id != 0) {
            $theme = Doctrine_Query::create()->select()->from('ArticleTheme t')->where('t.id = ?', array($theme->parent_id))->fetchOne();
            if ($theme) {
                $xmltheme = $xmlthemes->addChild('theme');
                $xmltheme->addAttribute('uuid', $theme->id);
                $xmltheme->addAttribute('title', $theme->title);
                $xmltheme->addAttribute('parent_id', $theme->parent_id);
                $this->recurseThemes($xmlthemes, $theme);
            }
        }
    }

    /**
     * Rendert PDF mit fop
     *
     * @param int ArticleId
     * @return string path to pdf
     */
    public function renderFOP($ArticleId)
    {

        if (intval($ArticleId) == 0) {
            throw new Zend_Amf_Exception;
        }

        return 'http://' . $_SERVER["SERVER_NAME"] . '/market/preview/' . $article->directory . 'preview.pdf';
    }

    /**
     * Renew Session
     *
     * @param string Sessid
     * @return string Message
     */
    public function sessionReNew($id)
    {
        session_destroy();
        session_id($id);
        session_start();
        return 'success';
    }

    /**
     * Login für Debug
     *
     * @return string Message
     */
    public function loginDebug()
    {


        $_authAdapter = new TP_Plugin_AuthAdapter();
        $_authAdapter->setIdentity('demo@printshopcreator.de')
                ->setCredential('pscpmb');
        $result = Zend_Auth::getInstance()->authenticate($_authAdapter);
        $xml = new SimpleXMLElement('<root></root>');

        if ($result->isValid()) {
            $xml->addChild('status', 'SUCCESS');
        } else {
            $xml->addChild('status', 'ERROR');
            $xml->addChild('message', 'Invalid Username');
        }

        return $xml->asXML();
    }

    /**
     * Logging
     * 
     * @return string Message
     */
    public function log($text)
    {
        $writer = new Zend_Log_Writer_Stream(PUBLIC_PATH . '/layouter/log.txt');
        $logger = new Zend_Log($writer);
        $logger->debug($text);
    }

}
