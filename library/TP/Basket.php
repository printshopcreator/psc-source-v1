<?php
class TP_Basket
{
    /**
     * Warenkorb-Sessionnamespace
     *
     * Der Warenkorb ist readonly somit wird eine ungewollte �nderung vermieden
     *
     * @var Zend_Session_Namespace
     */
    private $_Warenkorb;

    const GUTSCHEIN_PRODUCT = 1;

    const GUTSCHEIN_ALL = 2;

    public function setTempProduct ($article, $options, $netto)
    {

        if ( $this->_Warenkorb->isLocked() )
        {
            $this->_Warenkorb->unLock();
        }
        $this->_Warenkorb->TempProduct[$article->uuid] = new TP_Basket_Item( );
        $this->_Warenkorb->TempProduct[$article->uuid]->setArticle( $article );
        $this->_Warenkorb->TempProduct[$article->uuid]->setOptions( $options );
        $this->_Warenkorb->TempProduct[$article->uuid]->setNetto( $netto );
        $this->_Warenkorb->LastTempProductId = $article->uuid;
        $this->_Warenkorb->lock();
    }

    public static function getBasket() {
        return new TP_Basket();
    }

    public function getWeight() {
        $weight = 0;
        foreach ( $this->_Warenkorb->Articles as $Artikel )
        {
            $weight = $weight + $Artikel->getWeight();
        }
        return $weight;
    }

    public function setTempProductClass (TP_Basket_Item $basketArticle)
    {
        if ( $this->_Warenkorb->isLocked() )
        {
            $this->_Warenkorb->unLock();
        }
        $this->_Warenkorb->TempProduct[$basketArticle->getArticleId()] = $basketArticle;
        $this->_Warenkorb->LastTempProductId = $basketArticle->getArticleId();
        $this->_Warenkorb->lock();
    }
    /**
     *
     * Gibt das Basket Item
     *
     * @param string $uuid
     * @return TP_Basket_Item
     */

    public function getTempProduct ($uuid = false)
    {
        if(!$uuid && !$this->_Warenkorb->LastTempProductId) {
            return new TP_Basket_Item( );
        }

        if(!$uuid && $this->_Warenkorb->LastTempProductId) {
            $uuid = $this->_Warenkorb->LastTempProductId;
        }

        if ( !isset($this->_Warenkorb->TempProduct[$uuid]))
        {

            if ( $this->_Warenkorb->isLocked() )
            {
                $this->_Warenkorb->unLock();
            }

            $this->_Warenkorb->TempProduct[$uuid] = new TP_Basket_Item( );
            $this->_Warenkorb->LastTempProductId = $uuid;
            $this->_Warenkorb->lock();

        }
        if(isset($this->_Warenkorb->TempProduct[$this->_Warenkorb->LastTempProductId])) {
            if ( $this->_Warenkorb->isLocked() )
            {
                $this->_Warenkorb->unLock();
            }
            $this->_Warenkorb->LastTempProductId = $uuid;
            $this->_Warenkorb->lock();
        }
        return $this->_Warenkorb->TempProduct[$uuid];
    }
    /**
     * Konstruiert das Model für den Warenkorb
     *
     * @return void
     */
    public function __construct ()
    {
        $this->_Warenkorb = new Zend_Session_Namespace( 'Basket' );
        $this->_Warenkorb->unlock();
        if ( Zend_Session::namespaceIsset( 'Basket' ) === false )
        {
            $this->InitWarenkorb();
        }
        // Zur Sicherheit ist der Warenkorb standardm��ig readonly
        $this->_Warenkorb->lock();
    }


    /**
     * Fuegt einen Artikel hinzu
     * @param Integer - Artikelid
     * @param Integer - anzahl
     * @param array(schemaid, optionid)
     * @return true -> alles ok
     * @return false -> konnte nicht uebernommen werden
     */
    public function addProduct ($copy = false, $uuid)
    {
        // unlocking read-only lock
        if ( $this->_Warenkorb->isLocked() )
        {
            $this->_Warenkorb->unLock();
        }

        $id = $this->_Warenkorb->Articles->addItem( $this->_Warenkorb->TempProduct[$uuid] );
        
        //Optionen eintragen
        if(!$copy) {
            $this->_Warenkorb->TempProduct[$uuid] = new TP_Basket_Item( );
        }else{
            $this->_Warenkorb->TempProduct[$uuid] = clone $this->_Warenkorb->TempProduct[$uuid];
        }
        $this->_Warenkorb->LastTempProductId = $uuid;
        //Namespace wieder sperren
        $this->_Warenkorb->lock();
        return $id;
    }
    
    public function updateProduct ($uuid)
    {
        // unlocking read-only lock
        if ( $this->_Warenkorb->isLocked() )
        {
            $this->_Warenkorb->unLock();
        }
        $this->_Warenkorb->Articles->updateItem( $this->_Warenkorb->TempProduct[$uuid]->getInLoad(), $this->_Warenkorb->TempProduct[$uuid] );
        
        //Optionen eintragen
        $this->_Warenkorb->TempProduct[$uuid] = new TP_Basket_Item( );
        $this->_Warenkorb->LastTempProductId = $uuid;
        //Namespace wieder sperren
        $this->_Warenkorb->lock();
        return true;
    }

    public function updateBasketProduct ($uuid, $obj)
    {
        // unlocking read-only lock
        if ( $this->_Warenkorb->isLocked() )
        {
            $this->_Warenkorb->unLock();
        }
        $this->_Warenkorb->Articles->updateItem( $uuid, $obj );

        //Optionen eintragen
        $this->_Warenkorb->TempProduct[$uuid] = new TP_Basket_Item( );
        $this->_Warenkorb->LastTempProductId = $uuid;
        //Namespace wieder sperren
        $this->_Warenkorb->lock();
        return true;
    }

    public function setPaymenttype ($paymentid)
    {
        // unlocking read-only lock
        if ( $this->_Warenkorb->isLocked() )
        {
            $this->_Warenkorb->unLock();
        }
        $this->_Warenkorb->Paymenttype = $paymentid;
        //Namespace wieder sperren
        $this->_Warenkorb->lock();
        return true;
    }
    public function getPaymenttype ()
    {
        // unlocking read-only lock
        if ( $this->_Warenkorb->isLocked() )
        {
            $this->_Warenkorb->unLock();
        }
        $paymentid = $this->_Warenkorb->Paymenttype;
        //Namespace wieder sperren
        $this->_Warenkorb->lock();
        return $paymentid;
    }
    public function setShippingtype ($shippingid)
    {
        // unlocking read-only lock
        if ( $this->_Warenkorb->isLocked() )
        {
            $this->_Warenkorb->unLock();
        }
        $this->_Warenkorb->Shippingtype = $shippingid;
        //Namespace wieder sperren
        $this->_Warenkorb->lock();
        return true;
    }
    public function getShippingtype ()
    {
        // unlocking read-only lock
        if ( $this->_Warenkorb->isLocked() )
        {
            $this->_Warenkorb->unLock();
        }
        $shippingid = $this->_Warenkorb->Shippingtype;
        //Namespace wieder sperren
        $this->_Warenkorb->lock();
        return $shippingid;
    }
    /**
     * L�scht einen Artikel aus dem Warenkorb
     * @param Integer - Artikelid
     * @return false -> Fehler
     * @return true -> Artikel entfernt
     */
    public function removeProduct ($artikelID)
    {
        // unlocking read-only lock
        if ( $this->_Warenkorb->isLocked() )
        {
            $this->_Warenkorb->unLock();
        }
        $this->_Warenkorb->Articles->removeItemByKey( $artikelID );
        //Namespace wieder sperren
        $this->_Warenkorb->lock();
        return true;
    }
    /**
     * Gibt einige Informationen �ber den Warenkorb zur�ck
     * @return array()
     *                    -> 'WarenkorbPreis' = Preis aller Artikel im Warenkorb
     *                    -> 'Artikelanzahl'  = Wieviele Artikel im Warenkorb liegen
     */
    public function getWarenkorbInfo ()
    {
        $WarenkorbPreis = 0;
        $ArtikelAnzahl = $this->_Warenkorb->ArtikelAnzahl;
        if ( $ArtikelAnzahl < 1 )
        {
            return array(
                
            "WarenkorbPreis" => $WarenkorbPreis , 
            "Artikelanzahl" => $ArtikelAnzahl
            );
        }
        else
        {
            foreach ( $this->_Warenkorb->Artikel as $Artikel )
            {
                // DB-Prefix & Adapter auslesen
                $db = Zend_registry::get( 'db' );
                $prefix = Zend_Registry::get( 'config' )->db->prefix;
                //Zend_Db_Select aufbauen
                //Wir joinen die tables von Hand zusammen da dies einfacher ist :)
                $select = $db->select()->from( $prefix . '_shop_artikel', 'shop_artikel_preis' )->where( 'shop_artikel_id = "' . $this->_Warenkorb->Artikel["ArtikelID"] . '"' );
                //Query abschicken
                $stmt = $db->query( $select );
                $result = $stmt->fetchOne();
                $WarenkorbPreis += $result->shop_artikel_preis * $Artikel["Anzahl"];
            }
        }
        return array(
            
        "WarenkorbPreis" => $WarenkorbPreis , 
        "Artikelanzahl" => $ArtikelAnzahl
        );
    }
    
    public function checkIsLayouterIdInUse($id) {
        
        foreach($this->_Warenkorb->Articles as $article) {
            if($id == $article->getLayouterId()) {
                return true;
            }
        }
        
    }
    
    /**
     * Gibt alle Warenkorbeinträge zurück
     * @return iterator
     */
    public function getAllArtikel ()
    {
        return $this->_Warenkorb->Articles;
    }
    public function getBasketArtikel ($uuid)
    {
        return $this->_Warenkorb->Articles->getItemById( $uuid );
    }
    public function getBasketArtikelId ($uuid, $getKey = false)
    {
        return $this->_Warenkorb->Articles->getItemByArticleId( $uuid, $getKey );
    }
    public function getAllCount ()
    {
        return $this->_Warenkorb->Articles->count();
    }
    public function setPreisNetto ($preis)
    {
        if ( $this->_Warenkorb->isLocked() )
        {
            $this->_Warenkorb->unLock();
        }
        $this->_Warenkorb->preis = $preis;
        //Namespace wieder sperren
        $this->_Warenkorb->lock();
    }
    public function setGutschein ($id)
    {
        if ( $this->_Warenkorb->isLocked() )
        {
            $this->_Warenkorb->unLock();
        }
        $this->_Warenkorb->Gutschein = $id;
        //Namespace wieder sperren
        $this->_Warenkorb->lock();
    }
    public function setGutscheinAbzug ($value)
    {
        if ( $this->_Warenkorb->isLocked() )
        {
            $this->_Warenkorb->unLock();
        }
        $this->_Warenkorb->GutscheinAbzug = $value;
        //Namespace wieder sperren
        $this->_Warenkorb->lock();
    }

    public function setGutscheinAbzugNetto ($value)
    {
        if ( $this->_Warenkorb->isLocked() )
        {
            $this->_Warenkorb->unLock();
        }
        $this->_Warenkorb->GutscheinAbzugNetto = $value;
        //Namespace wieder sperren
        $this->_Warenkorb->lock();
    }
    
    public function setGutscheinAbzugTyp ($value)
    {
        if ( $this->_Warenkorb->isLocked() )
        {
            $this->_Warenkorb->unLock();
        }
        $this->_Warenkorb->GutscheinAbzugTyp = $value;
        //Namespace wieder sperren
        $this->_Warenkorb->lock();
    }
    public function setVersandkosten ($value)
    {
        if ( $this->_Warenkorb->isLocked() )
        {
            $this->_Warenkorb->unLock();
        }
        $this->_Warenkorb->Versandkosten = $value;
        //Namespace wieder sperren
        $this->_Warenkorb->lock();
    }
	public function getVersandkosten ()
    {
        if ( $this->_Warenkorb->isLocked() )
        {
            $this->_Warenkorb->unLock();
        }
        $ids = $this->_Warenkorb->Versandkosten;
        //Namespace wieder sperren
        $this->_Warenkorb->lock();
        return $ids;
    }

    public function setShippingtypeExtraLabel($value) {
        if ( $this->_Warenkorb->isLocked() )
        {
            $this->_Warenkorb->unLock();
        }
        $this->_Warenkorb->shippingtype_extra_label = $value;
        $this->_Warenkorb->lock();
    }

	public function setZahlkosten ($value)
    {
        if ( $this->_Warenkorb->isLocked() )
        {
            $this->_Warenkorb->unLock();
        }
        $this->_Warenkorb->Zahlkosten = $value;
        //Namespace wieder sperren
        $this->_Warenkorb->lock();
    }
	public function getZahlkosten ()
    {
        if ( $this->_Warenkorb->isLocked() )
        {
            $this->_Warenkorb->unLock();
        }
        $ids = $this->_Warenkorb->Zahlkosten;
        //Namespace wieder sperren
        $this->_Warenkorb->lock();
        return $ids;
    }
    public function getGutschein ()
    {
        if ( $this->_Warenkorb->isLocked() )
        {
            $this->_Warenkorb->unLock();
        }
        $ids = $this->_Warenkorb->Gutschein;
        //Namespace wieder sperren
        $this->_Warenkorb->lock();
        return $ids;
    }
    public function getGutscheinAbzug ()
    {
        if ( $this->_Warenkorb->isLocked() )
        {
            $this->_Warenkorb->unLock();
        }
        $ids = $this->_Warenkorb->GutscheinAbzug;
        //Namespace wieder sperren
        $this->_Warenkorb->lock();
        return $ids;
    }

    public function getGutscheinAbzugNetto ()
    {
        if ( $this->_Warenkorb->isLocked() )
        {
            $this->_Warenkorb->unLock();
        }
        $ids = $this->_Warenkorb->GutscheinAbzugNetto;
        //Namespace wieder sperren
        $this->_Warenkorb->lock();
        return $ids;
    }

    public function getGutscheinAbzugTyp ()
    {
        if ( $this->_Warenkorb->isLocked() )
        {
            $this->_Warenkorb->unLock();
        }
        $ids = $this->_Warenkorb->GutscheinAbzugTyp;
        //Namespace wieder sperren
        $this->_Warenkorb->lock();
        return $ids;
    }

    public function setPreisSteuer ($preis)
    {
        if ( $this->_Warenkorb->isLocked() )
        {
            $this->_Warenkorb->unLock();
        }
        $this->_Warenkorb->steuer = $preis;
        //Namespace wieder sperren
        $this->_Warenkorb->lock();
    }
    public function setPreisBrutto ($preis)
    {
        if ( $this->_Warenkorb->isLocked() )
        {
            $this->_Warenkorb->unLock();
        }
        $this->_Warenkorb->brutto = $preis;
        //Namespace wieder sperren
        $this->_Warenkorb->lock();
    }
    public function getPreisNetto ()
    {
        if ( $this->_Warenkorb->isLocked() )
        {
            $this->_Warenkorb->unLock();
        }
        $preis = $this->_Warenkorb->preis;
        //Namespace wieder sperren
        $this->_Warenkorb->lock();
        return $preis;
    }
    public function getPreisSteuer ()
    {
        if ( $this->_Warenkorb->isLocked() )
        {
            $this->_Warenkorb->unLock();
        }
        $steuer = $this->_Warenkorb->steuer;
        //Namespace wieder sperren
        $this->_Warenkorb->lock();
        return $steuer;
    }
    public function getPreisBrutto ()
    {
        if ( $this->_Warenkorb->isLocked() )
        {
            $this->_Warenkorb->unLock();
        }
        $brutto = $this->_Warenkorb->brutto;
        //Namespace wieder sperren
        $this->_Warenkorb->lock();
        return $brutto;
    }
    public function getBasketField1 ()
    {
        if ( $this->_Warenkorb->isLocked() )
        {
            $this->_Warenkorb->unLock();
        }
        $bs1 = $this->_Warenkorb->basketfield1;
        //Namespace wieder sperren
        $this->_Warenkorb->lock();
        return $bs1;
    }
    public function setBasketField1 ($bs1)
    {
        if ( $this->_Warenkorb->isLocked() )
        {
            $this->_Warenkorb->unLock();
        }
        $this->_Warenkorb->basketfield1 = $bs1;
        //Namespace wieder sperren
        $this->_Warenkorb->lock();
    }
    public function getBasketField2 ()
    {
        if ( $this->_Warenkorb->isLocked() )
        {
            $this->_Warenkorb->unLock();
        }
        $bs2 = $this->_Warenkorb->basketfield2;
        //Namespace wieder sperren
        $this->_Warenkorb->lock();
        return $bs2;
    }
    public function setBasketField2 ($bs2)
    {
        if ( $this->_Warenkorb->isLocked() )
        {
            $this->_Warenkorb->unLock();
        }
        $this->_Warenkorb->basketfield2 = $bs2;
        //Namespace wieder sperren
        $this->_Warenkorb->lock();
    }
    public function getPaymentZusatz ()
    {
        if ( $this->_Warenkorb->isLocked() )
        {
            $this->_Warenkorb->unLock();
        }
        $bs2 = $this->_Warenkorb->payment_zusatz;
        //Namespace wieder sperren
        $this->_Warenkorb->lock();
        return $bs2;
    }
    public function setPaymentZusatz ($bs2)
    {
        if ( $this->_Warenkorb->isLocked() )
        {
            $this->_Warenkorb->unLock();
        }
        $this->_Warenkorb->payment_zusatz = $bs2;
        //Namespace wieder sperren
        $this->_Warenkorb->lock();
    }
    public function getBic ()
    {
        if ( $this->_Warenkorb->isLocked() )
        {
            $this->_Warenkorb->unLock();
        }
        $bs2 = $this->_Warenkorb->bic;
        //Namespace wieder sperren
        $this->_Warenkorb->lock();
        return $bs2;
    }
    public function setBic ($bs2)
    {
        if ( $this->_Warenkorb->isLocked() )
        {
            $this->_Warenkorb->unLock();
        }
        $this->_Warenkorb->bic = strtoupper(trim($bs2));
        //Namespace wieder sperren
        $this->_Warenkorb->lock();
    }
    public function getIban ()
    {
        if ( $this->_Warenkorb->isLocked() )
        {
            $this->_Warenkorb->unLock();
        }
        $bs2 = $this->_Warenkorb->iban;
        //Namespace wieder sperren
        $this->_Warenkorb->lock();
        return $bs2;
    }
    public function setIban ($bs2)
    {
        if ( $this->_Warenkorb->isLocked() )
        {
            $this->_Warenkorb->unLock();
        }
        $this->_Warenkorb->iban = str_replace(" ", "", $bs2);
        //Namespace wieder sperren
        $this->_Warenkorb->lock();
    }
    public function setDeliveryIsSame($val) {
        if ( $this->_Warenkorb->isLocked() )
        {
            $this->_Warenkorb->unLock();
        }
        $this->_Warenkorb->deliverysame = $val;
        //Namespace wieder sperren
        $this->_Warenkorb->lock();
    }
    public function setDelivery($val) {
        if ( $this->_Warenkorb->isLocked() )
        {
            $this->_Warenkorb->unLock();
        }
        $this->_Warenkorb->delivery = $val;
        //Namespace wieder sperren
        $this->_Warenkorb->lock();
    }
    public function setInvoiceIsSame($val) {
        if ( $this->_Warenkorb->isLocked() )
        {
            $this->_Warenkorb->unLock();
        }
        $this->_Warenkorb->invoicesame = $val;
        //Namespace wieder sperren
        $this->_Warenkorb->lock();
    }
    public function setInvoice($val) {
        if ( $this->_Warenkorb->isLocked() )
        {
            $this->_Warenkorb->unLock();
        }
        $this->_Warenkorb->invoice = $val;
        //Namespace wieder sperren
        $this->_Warenkorb->lock();
    }
    public function setSenderIsSame($val) {
        if ( $this->_Warenkorb->isLocked() )
        {
            $this->_Warenkorb->unLock();
        }
        $this->_Warenkorb->sendersame = $val;
        //Namespace wieder sperren
        $this->_Warenkorb->lock();
    }
    public function setSender($val) {
        if ( $this->_Warenkorb->isLocked() )
        {
            $this->_Warenkorb->unLock();
        }
        $this->_Warenkorb->sender = $val;
        //Namespace wieder sperren
        $this->_Warenkorb->lock();
    }
    public function getDeliveryIsSame() {
        return $this->_Warenkorb->deliverysame;
    }
    public function getPaymentRef() {
        return $this->_Warenkorb->paymentRef;
    }
    public function setPaymentRef($value) {
        if ( $this->_Warenkorb->isLocked() )
        {
            $this->_Warenkorb->unLock();
        }
        $this->_Warenkorb->paymentRef = $value;
        //Namespace wieder sperren
        $this->_Warenkorb->lock();
    }
    public function getDelivery() {
        return $this->_Warenkorb->delivery;
    }
    public function getInvoiceIsSame() {
        return $this->_Warenkorb->invoicesame;
    }
    public function getInvoice() {
        return $this->_Warenkorb->invoice;
    }
    public function getSenderIsSame() {
        return $this->_Warenkorb->sendersame;
    }
    public function getSender() {
        return $this->_Warenkorb->sender;
    }
    public function getToken() {
        return $this->_Warenkorb->token;
    }
    public function setToken($value) {
        if ( $this->_Warenkorb->isLocked() )
        {
            $this->_Warenkorb->unLock();
        }
        $this->_Warenkorb->token = $value;
        //Namespace wieder sperren
        $this->_Warenkorb->lock();
    }

    public function getPLZ() {
        return $this->_Warenkorb->plz;
    }
    public function getOfferNumber() {
        return $this->_Warenkorb->offerNumber;
    }

    public function generateOfferNumber() {
        if ( $this->_Warenkorb->isLocked() )
        {
            $this->_Warenkorb->unLock();
        }
        $this->_Warenkorb->offerNumber = time();
        //Namespace wieder sperren
        $this->_Warenkorb->lock();
    }

    public function setPLZ($value) {
        if ( $this->_Warenkorb->isLocked() )
        {
            $this->_Warenkorb->unLock();
        }
        $this->_Warenkorb->plz = $value;
        //Namespace wieder sperren
        $this->_Warenkorb->lock();
    }
	
    public function getDeliveryPLZ() {
        return $this->_Warenkorb->delivery_plz;
    }
    public function setDeliveryPLZ($value) {
        if ( $this->_Warenkorb->isLocked() )
        {
            $this->_Warenkorb->unLock();
        }
        $this->_Warenkorb->delivery_plz = $value;
        //Namespace wieder sperren
        $this->_Warenkorb->lock();
    }
    public function setDeliveryCountry($value) {
        if ( $this->_Warenkorb->isLocked() )
        {
            $this->_Warenkorb->unLock();
        }
        $this->_Warenkorb->delivery_country = $value;
        //Namespace wieder sperren
        $this->_Warenkorb->lock();
    }
    public function setDeliveryUstId($value) {
        if ( $this->_Warenkorb->isLocked() )
        {
            $this->_Warenkorb->unLock();
        }
        $this->_Warenkorb->delivery_ustid = $value;
        //Namespace wieder sperren
        $this->_Warenkorb->lock();
    }
    public function getDeliveryCountry() {
        return $this->_Warenkorb->delivery_country;
    }
    public function getDeliveryUstId() {
        return $this->_Warenkorb->delivery_ustid;
    }

    public function setAdditional($productpreisNetto, $productpreisBrutto, $zahlart, $versandart, $mwert) {
        if ( $this->_Warenkorb->isLocked() )
        {
            $this->_Warenkorb->unLock();
        }
        $this->_Warenkorb->productpreisbrutto = $productpreisBrutto;
        $this->_Warenkorb->productpreisnetto = $productpreisNetto;
        $this->_Warenkorb->ZahlkostenBrutto = $zahlart;
        $this->_Warenkorb->VersandkostenBrutto = $versandart;
        $this->_Warenkorb->mwert = $mwert;
        //Namespace wieder sperren
        $this->_Warenkorb->lock();
    }

    public function getProduktPreisBrutto() {
        return $this->_Warenkorb->productpreisbrutto;
    }

    public function getProduktPreisSteuer() {
        return $this->_Warenkorb->productpreissteuer;
    }

    public function setProduktPreisSteuer($value) {
        if ( $this->_Warenkorb->isLocked() )
        {
            $this->_Warenkorb->unLock();
        }
        $this->_Warenkorb->productpreissteuer = $value;

        $this->_Warenkorb->lock();
    }

    public function getProduktPreisNetto() {
        return $this->_Warenkorb->productpreisnetto;
    }

    public function getVersandBrutto() {
        return $this->_Warenkorb->VersandkostenBrutto;
    }

    public function getZahlartBrutto() {
        return $this->_Warenkorb->ZahlkostenBrutto;
    }

    public function getShippingtypeExtraLabel() {
        return $this->_Warenkorb->shippingtype_extra_label;
    }

    public function getInfo() {
        return $this->_Warenkorb->info;
    }

    public function isWithTax() {
        return $this->_Warenkorb->withTax;
    }

    public function setWithTax($withTax) {
        if ( $this->_Warenkorb->isLocked() )
        {
            $this->_Warenkorb->unLock();
        }
        $this->_Warenkorb->withTax = $withTax;
        //Namespace wieder sperren
        $this->_Warenkorb->lock();
    }

    public function setInfo($info) {
        if ( $this->_Warenkorb->isLocked() )
        {
            $this->_Warenkorb->unLock();
        }
        $this->_Warenkorb->info = $info;
        //Namespace wieder sperren
        $this->_Warenkorb->lock();
    }

    public function getOfferContact() {
        return $this->_Warenkorb->offerContact;
    }

    public function setOfferContact($offerContact) {
        if ( $this->_Warenkorb->isLocked() )
        {
            $this->_Warenkorb->unLock();
        }
        $this->_Warenkorb->offerContact = $offerContact;
        //Namespace wieder sperren
        $this->_Warenkorb->lock();
    }

    public function setMWert($value) {
        if ( $this->_Warenkorb->isLocked() )
        {
            $this->_Warenkorb->unLock();
        }
        $this->_Warenkorb->mwert = $value;
        //Namespace wieder sperren
        $this->_Warenkorb->lock();
    }

    public function setVersandBrutto($value) {
        if ( $this->_Warenkorb->isLocked() )
        {
            $this->_Warenkorb->unLock();
        }
        $this->_Warenkorb->VersandkostenBrutto = $value;
        //Namespace wieder sperren
        $this->_Warenkorb->lock();
    }

    public function getMWert() {
        return $this->_Warenkorb->mwert;
    }

    public function clearBasketItems ()
    {
        $this->_Warenkorb->unlock();
        $this->_Warenkorb->Articles->clear();
        $this->_Warenkorb->lock();
    }

    public function clearBasketItemsOf ($articleUUId)
    {
        $removeAble = false;

        /** @var TP_Basket_Item $article */
        foreach($this->_Warenkorb->Articles as $uuid => $article) {
            if($article->getArticleId() == $articleUUId) {
                $removeAble = $uuid;
            }
        }

        $this->removeProduct($removeAble);

    }

    public function clear ()
    {
        $this->_Warenkorb->unlock();
        $this->_Warenkorb->ArtikelAnzahl = 0;
        $this->_Warenkorb->Paymenttype = 0;
        $this->_Warenkorb->Articles->clear();
        $this->_Warenkorb->Gutschein = "";
        $this->_Warenkorb->GutscheinAbzug = 0;
        $this->_Warenkorb->GutscheinAbzugNetto = 0;
        $this->_Warenkorb->preis = 0;
        $this->_Warenkorb->steuer = 0;
        $this->_Warenkorb->brutto = 0;

        $this->_Warenkorb->productpreisbrutto = 0;
        $this->_Warenkorb->productpreissteuer = 0;
        $this->_Warenkorb->productpreisnetto = 0;
        $this->_Warenkorb->ZahlkostenBrutto = 0;
        $this->_Warenkorb->VersandkostenBrutto = 0;
        $this->_Warenkorb->mwert = array();

        $this->_Warenkorb->Zahlkosten = 0;
        $this->_Warenkorb->Versandkosten = 0;
        $this->_Warenkorb->basketfield1 = '';
        $this->_Warenkorb->basketfield2 = '';
        $this->_Warenkorb->payment_zusatz = '';
        $this->_Warenkorb->offerContact = array();
        $this->_Warenkorb->initialized = true;
        $this->_Warenkorb->deliverysame = true;
        $this->_Warenkorb->delivery = 0;
        $this->_Warenkorb->invoicesame = true;
        $this->_Warenkorb->invoice = 0;
        $this->_Warenkorb->sendersame = true;
        $this->_Warenkorb->sender = 0;
        $this->_Warenkorb->token = "";
        $this->_Warenkorb->plz = false;
        $this->_Warenkorb->delivery_plz = false;
        $this->_Warenkorb->delivery_country = false;
        $this->_Warenkorb->delivery_ustid = NULL;

        $this->_Warenkorb->shippingtype_extra_label = "";
        $this->_Warenkorb->info = array();
        $this->_Warenkorb->paymentRef = substr( md5(rand()), 0, 7);
        $this->_Warenkorb->TempProduct = array();
        $this->_Warenkorb->LastTempProductId = false;
        $this->_Warenkorb->offerNumber = time();
        $this->_Warenkorb->withTax = true;
        $this->_Warenkorb->lock();
    }
    /**
     * Initialisiert den Warenkorb
     */
    private function InitWarenkorb ()
    {
        $this->_Warenkorb->ArtikelAnzahl = 0;

        $this->_Warenkorb->Articles = new TP_Basket_Collection( );
        $this->_Warenkorb->Gutschein = "";
        $this->_Warenkorb->GutscheinAbzug = 0;
        $this->_Warenkorb->GutscheinAbzugNetto = 0;
        $this->_Warenkorb->productpreissteuer = 0;
        $this->_Warenkorb->initialized = true;
        $this->_Warenkorb->deliverysame = true;
        $this->_Warenkorb->delivery = 0;
        $this->_Warenkorb->invoicesame = true;
        $this->_Warenkorb->shippingtype_extra_label = "";
        $this->_Warenkorb->invoice = 0;
        $this->_Warenkorb->offerContact = array();
        $this->_Warenkorb->sendersame = true;
        $this->_Warenkorb->sender = 0;
        $this->_Warenkorb->Paymenttype = 0;
        $this->_Warenkorb->Zahlkosten = 0;
        $this->_Warenkorb->Versandkosten = 0;
        $this->_Warenkorb->Shippingtype = 0;
        $this->_Warenkorb->preis = 0;
        $this->_Warenkorb->steuer = 0;
        $this->_Warenkorb->brutto = 0;
        $this->_Warenkorb->token = "";
        $this->_Warenkorb->basketfield1 = '';
        $this->_Warenkorb->basketfield2 = '';
        $this->_Warenkorb->payment_zusatz = '';
        $this->_Warenkorb->plz = false;
		$this->_Warenkorb->delivery_plz = false;
		$this->_Warenkorb->delivery_country = false;
        $this->_Warenkorb->info = array();
        $this->_Warenkorb->paymentRef = substr( md5(rand()), 0, 7);
        $this->_Warenkorb->TempProduct = array();
        $this->_Warenkorb->LastTempProductId = false;
        $this->_Warenkorb->offerNumber = time();
        $this->_Warenkorb->withTax = true;
        $this->_Warenkorb->delivery_ustid = NULL;
    }
}