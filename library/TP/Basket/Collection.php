<?php

class TP_Basket_Collection implements IteratorAggregate, Countable {

    /**
     * Collection items
     *
     * @var array
     */
    protected $_items = array();

    /**
     * Item object class name
     *
     * @var string
     */
    protected $_itemObjectClass = 'Varien_Object';

    /**
     * Current page number for items pager
     *
     * @var int
     */
    protected $_curPage = 1;

    /**
     * Pager page size
     *
     * if page size is false, then we works with all items
     *
     * @var int || false
     */
    protected $_pageSize = false;

    /**
     * Total items number
     *
     * @var int
     */
    protected $_totalRecords;

    /**
     * Loading state flag
     *
     * @var bool
     */
    protected $_isCollectionLoaded;

    public function __construct()
    {

    }

    /**
     * Adding item to item array
     *
     * @param   TP_Basket_Item $item
     * @return  TP_Basket_Collection
     */
    public function addItem(TP_Basket_Item $item)
    {
        $itemId = TP_Util::uuid();
        if (!is_null($itemId)) {
            if (isset($this->_items[$itemId])) {
                throw new Exception('Item ('.get_class($item).') with the same id "'.$itemId.'" already exist');
            }
            $this->_items[$itemId] = $item;
        }
        else {
            $this->_items[] = $item;
        }
        return $itemId;
    }

    /**
     * Adding item to item array
     *
     * @param   TP_Basket_Item $item
     * @return  TP_Basket_Collection
     */
    public function updateItem($itemId, TP_Basket_Item $item)
    {
        $itemId = strtolower($itemId);
        if (!is_null($itemId)) {
            if (!isset($this->_items[$itemId])) {
                throw new Exception('Item ('.get_class($item).') with the same id "'.$itemId.'" do not exist');
            }
            $this->_items[$itemId] = $item;
        }
        return $this;
    }

    /**
     * Remove item from collection by item key
     *
     * @param   mixed $key
     * @return  TP_Basket_Collection
     */
    public function removeItemByKey($key)
    {
        $key = strtolower($key);
        if (isset($this->_items[$key])) {
            unset($this->_items[$key]);
        }
        
        return $this;
    }

    /**
     * Clear collection
     *
     * @return TP_Basket_Collection
     */
    public function clear()
    {
        $this->_items = array();
        return $this;
    }

    /**
     * Implementation of IteratorAggregate::getIterator()
     */
    public function getIterator()
    {
        return new ArrayIterator($this->_items);
    }

    /**
     * Retireve count of collection loaded items
     *
     * @return int
     */
    public function count()
    {
        return count($this->_items);
    }

    /**
     * Retrieve item by id
     *
     * @param   mixed $idValue
     * @return  TP_Basket_Item
     */
    public function getItemById($idValue)
    {
        $idValue = strtolower($idValue);
        if (isset($this->_items[$idValue])) {
            return $this->_items[$idValue];
        }
        return null;
    }

    /**
     * Retrieve item by id
     *
     * @param   mixed $idValue
     * @return  TP_Basket_Item
     */
    public function getItemByArticleId($idValue, $getKey = false)
    {
        foreach($this->_items as $key => $item) {
            if($item->getArticleId() == $idValue) {
                if($getKey) {
                    return $key;
                }
                return $item;
            }
        }
        return null;
    }

    /**
     * Set current page
     *
     * @param   int $page
     * @return  TP_Basket_Collection
     */
    public function setCurPage($page)
    {
        $this->_curPage = $page;
        return $this;
    }

    /**
     * Walk through the collection and run model method or external callback
     * with optional arguments
     *
     * Returns array with results of callback for each item
     *
     * @param string $method
     * @param array $args
     * @return array
     */
    public function walk($callback, array $args=array())
    {
        $results = array();
        $useItemCallback = is_string($callback) && strpos($callback, '::')===false;
        foreach ($this->getItems() as $id=>$item) {
            if ($useItemCallback) {
                $cb = array($item, $callback);
            } else {
                $cb = $callback;
                array_unshift($args, $item);
            }
            $results[$id] = call_user_func_array($cb, $args);
        }
        return $results;
    }

    public function each($obj_method, $args=array())
    {
        foreach ($args->_items as $k => $item) {
            $args->_items[$k] = call_user_func($obj_method, $item);
        }
    }

    /**
     * Retrieve collection first item
     *
     * @return TP_Basket_Item
     */
    public function getFirstItem()
    {

        if (count($this->_items)) {
            reset($this->_items);
            return current($this->_items);
        }

        return new $this->_itemObjectClass();
    }

    /**
     * Retrieve collection last item
     *
     * @return TP_Basket_Item
     */
    public function getLastItem()
    {

        if (count($this->_items)) {
            return end($this->_items);
        }

        return new $this->_itemObjectClass();
    }

    /**
     * Retrieve collection items
     *
     * @return array
     */
    public function getItems()
    {
        return $this->_items;
    }
}
