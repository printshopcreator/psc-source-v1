<?php

class TP_Basket_Item
{
  
    private $_article;
    
    private $_options = array();

    private $_additionalInfos = array();

    private $_calc_values = array();

    private $_pre_calc_values = array();
    
    private $_files = array();

    private $_special = false;

    private $_netto = 0;

    private $_steuer = 0;

    private $_brutto = 0;

    private $_inLoad = "";

    private $_count = 1;

    private $_articleId = "";

    private $_articleTyp = 0;

    private $_iscopy = false;

    private $_resale_price = 0;

    private $_motive = array();

    private $_motiv_price = 0;
    
    private $_layouter_id = "";
    
    private $_upload_mode = "";

    private $_designer_file = "";

    private $_designer_data = array();

    private $_designer_param = array();

    private $_designer_cost = 0;

    private $_designer_user_cost = 0;

    private $_layouter_width_en = false;

    private $_layouter_width = false;

    private $_layouter_height_en = false;

    private $_layouter_height = false;

    private $_has_gutschein = false;

    private $_weight = 0;

    private $_weight_single = 0;

    private $_designer_seiten = 0;
    
    private $_template_print_id = false;

    private $_shipping_type = false;

    private $_shipping_price = 0;

    private $_shipping_price_mwert = 0;

    private $_ref = false;

    private $_kst = false;

    private $templatePrintContactId_ = 0;

    private $_preflight_check_status = false;

    private $_deliverys = array();

    private $_rabatte = array();

    private $_calc_references = array();

    public function __construct()
    {

    }

    /**
     * @return array
     */
    public function getRabatte()
    {
        return $this->_rabatte;
    }

    /**
     * @param array $rabatte
     */
    public function setRabatte($rabatte)
    {
        $this->_rabatte = $rabatte;
    }

    public function setPreflightCheckStatus($var) {
        $this->_preflight_check_status = $var;
    }

    public function getPreflightCheckStatus() {
        return $this->_preflight_check_status;
    }

    public function getRef() {
        return $this->_ref;
    }

    public function setRef($var) {
        $this->_ref = $var;
    }

    public function getKst() {
        return $this->_kst;
    }

    public function setKst($var) {
        $this->_kst = $var;
    }

    public function getShippingType() {
        return $this->_shipping_type;
    }

    public function setShippingType($var) {
        $this->_shipping_type = $var;
    }

    public function getShippingPrice() {
        return $this->_shipping_price;
    }

    public function setShippingPrice($var) {
        $this->_shipping_price = $var;
    }

    public function getShippingPriceMWert() {
        return $this->_shipping_price_mwert;
    }

    public function setShippingPriceMWert($var) {
        $this->_shipping_price_mwert = $var;
    }
    
    public function setTemplatePrintId($file) {
        $this->_template_print_id = $file;
    }
    
    public function getTemplatePrintId() {
        if($this->_template_print_id == false) {
            $this->_template_print_id = md5(TP_Util::uuid());
        }
        return $this->_template_print_id;
    }

    public function getNewTemplatePrintId() {
        $this->_template_print_id = md5(TP_Util::uuid());
        return $this->_template_print_id;
    }

    public function setCalcValues($var) {
        $this->_calc_values = $var;
    }

    public function setDesignerParam($var) {
        $this->_designer_param = $var;
    }

    public function getDesignerParam() {
        return $this->_designer_param;
    }

    public function getCalcValues() {
        return $this->_calc_values;
    }

    public function setDesignerSeiten($var) {
        $this->_designer_seiten = $var;
    }

    public function getDesignerSeiten() {
        return $this->_designer_seiten;
    }
    
    private $_upload_seiten = 0;

    public function setUploadSeiten($var) {
        $this->_upload_seiten = $var;
    }

    public function getUploadSeiten() {
        return $this->_upload_seiten;
    }

    public function getWeight() {
        return $this->_weight;
    }

    public function setWeight($var) {
        $this->_weight = round($var, 0);
    }

    public function setHasGutschein($val) {
        $this->_has_gutschein = $val;
    }

    public function getHasGutschein() {
        return $this->_has_gutschein;
    }
    
    public function setLayouterWidth($val) {
        $this->_layouter_width = $val;
    }

    public function setLayouterWidthEn($val) {
        $this->_layouter_width_en = $val;
    }

    public function setLayouterHeight($val) {
        $this->_layouter_height = $val;
    }

    public function setLayouterHeightEn($val) {
        $this->_layouter_height_en = $val;
    }

    public function getLayouterWidth() {
        return $this->_layouter_width;
    }

    public function setDesignerData($val) {
        $this->_designer_data = $val;
    }

    public function getDesignerData() {
        return $this->_designer_data;
    }

    public function setDesignerFile($val) {
        $this->_designer_file = $val;
    }

    public function getDesignerFile() {
        return $this->_designer_file;
    }

    public function setDesignerCost($val) {
        $this->_designer_cost = $val;
    }

    public function getDesignerCost() {
        return $this->_designer_cost;
    }

    public function setDesignerUserCost($val) {
        $this->_designer_user_cost = $val;
    }

    public function getDesignerUserCost() {
        return $this->_designer_user_cost;
    }

    public function getLayouterWidthEn() {
        return $this->_layouter_width_en;
    }

    public function getLayouterHeight() {
        return $this->_layouter_height;
    }

    public function getLayouterHeightEn() {
        return $this->_layouter_height_en;
    }

    public function setLayouterId($val) {
        $this->_layouter_id = $val;
    }
    
    public function setUploadMode($val) {
    	$this->_upload_mode = $val;
    }
    
    public function getUploadMode() {
    	return $this->_upload_mode;
    }
    
    public function getLayouterId() {
        return $this->_layouter_id;
    }
    
    public function getIsCopy() {
        return $this->_iscopy;
    }

    public function setIsCopy($copy) {
        $this->_iscopy = $copy;
    }
    
    public function setCount($count) {
        $this->_count = $count;
    }

    public function getAuflage() {
        $options = $this->getOptions();
        if(isset($options['auflage']) && $options['auflage'] != 0 && $this->getCount() == 1) {
            return $options['auflage'];
        }
        if(isset($this->_article['typ']) && $this->_article['typ'] == 2
            && ($this->_article['preis'] == "" || $this->_article['preis'] == 0) && $this->getCount() <= 1) {
            $fix = new waren_article();
            $this->setOptions($fix->fixBasketItem($this->_article));
            $options = $this->getOptions();
            $this->setWeight($options['weight']);
            if(!isset($options['auflage']) || $options['auflage'] == 0) {
                return $this->getCount();
            }
            return $options['auflage'];
        }
        return $this->getCount();
    }

    public function getCount() {

        if(intval($this->_count) == 0) {
            return 1;
        }
        return $this->_count;
    }

    public function setArticleType($typ) {
            $this->_articleTyp = $typ;
    }

    public function clearMotive() {
        $this->_motive = array();
        $this->_motiv_price = 0;
    }

    public function addMotiv(Motiv $motiv, $motiv_group = 1) {
        if($motiv == false || isset($this->_motive[$motiv->id])){
            return;
        }
        $this->_motiv_price = $this->_motiv_price + $motiv['price'.$motiv_group];
        $this->_motive[$motiv->id] = $motiv->toArray(false);
    }

    public function getMotivPrice() {
        return $this->_motiv_price;
    }

    public function getMotive() {
        return $this->_motive;
    }

    public function getArticleType() {
            return $this->_articleTyp;
    }

    public function setArticleId($id, $overload = false) {
        if($this->_articleId != $id && $overload == false) {
            $this->_inLoad = "";
            $this->_files = array();
            $this->_special = false;
        }
        $this->_articleId = $id;
    }

    public function getArticleId() {
        return $this->_articleId;
    }
    
    public function setNetto($netto) {
        $this->_netto = $netto;

    }

    public function getNetto() {
        return $this->_netto;
    }

    public function setResalePrice($var) {
        $this->_resale_price = str_replace(',', '.', $var);
    }

    public function getResalePrice() {
        return $this->_resale_price;
    }

    public function setSteuer($steuer) {
        $this->_steuer = $steuer;
    }

    public function getSteuer() {
        return $this->_steuer;
    }

    public function setBrutto($brutto) {
        $this->_brutto = $brutto;
    }

    public function getBrutto() {
        return $this->_brutto;
    }
    
    public function setInLoad($key) {
    	$this->_inLoad = $key;
    }
    
    public function getInload() {
    	return $this->_inLoad;
    }
    
    public function setFiles($key, $value, $name, $chunktitle = '', $what = '') {
    	$this->_files[$key] = array('value' => $value, 'name' => $name, 'chunktitle' => $chunktitle, 'what' => $what);
    }

    public function getFiles() {
        return $this->_files;	
    }
    
    public function getFile($key) {
        return $this->_files[$key];	
    }

    public function clearFiles() {
        $this->_files = array();
    }

    public function setSpecial($value) {
        $this->_special = $value;
    }
    
    public function getSpecial() {
        return $this->_special;
    }

    public function setArticle($article) {
    	$this->_article = $this->clean($article);
    }
    
    public function delFile($key) {

    	if(isset($this->_files[$key]) && is_array($this->_files[$key])) {
    		unset($this->_files[$key]);
    		return true;
    	}
    	return false;
    }
     
    public function getArticle() {
        return $this->clean($this->_article);
    }

    public function getArticleName() {
        return $this->_article['title'];
    }

    public function setOptions($options) {
    	$this->_options = $this->cleanOptions($options);
    }
    
    public function getOptions() {
    	return $this->cleanOptions($this->_options);
    }
    
    protected function clean($arr) {
        $tmp = [];
        $tmp['id'] = $arr['id'];
        $tmp['name'] = $arr['name'];
        $tmp['title'] = $arr['title'];
        $tmp['url'] = $arr['url'];

    	return $tmp;
    }

    protected function cleanOptions($arr) {
        if(isset($arr['send'])) unset($arr['send']);
        if(isset($arr['PHPSESSID'])) unset($arr['PHPSESSID']);
        if(isset($arr['Shops'])) unset($arr['Shops']);
        if(isset($arr['Account'])) unset($arr['Account']);
        return $arr;
    }

    /**
     * @return array
     */
    public function getDeliverys()
    {
        return $this->_deliverys;
    }

    /**
     * @param array $deliverys
     */
    public function setDeliverys($deliverys)
    {
        $this->_deliverys = $deliverys;
    }

    /**
     * @return array
     */
    public function getPreCalcValues()
    {
        return $this->_pre_calc_values;
    }

    /**
     * @param array $pre_calc_values
     */
    public function setPreCalcValues($pre_calc_values)
    {
        $this->_pre_calc_values = $pre_calc_values;
    }

    /**
     * @return int
     */
    public function getWeightSingle()
    {
        return $this->_weight_single;
    }

    /**
     * @param int $weight_single
     */
    public function setWeightSingle($weight_single)
    {
        $this->_weight_single = round($weight_single, 2);
    }

    /**
     * @return int
     */
    public function getTemplatePrintContactId()
    {
        return $this->templatePrintContactId_;
    }

    /**
     * @param int $templatePrintContactId_
     */
    public function setTemplatePrintContactId($templatePrintContactId_)
    {
        Zend_Registry::get('log')->debug("SET TEMPLATE PRINT ID");
        $this->templatePrintContactId_ = $templatePrintContactId_;
    }

    /**
     * @return array
     */
    public function getCalcReferences()
    {
        return $this->_calc_references;
    }

    /**
     * @param array $value
     */
    public function setCalcReferences($value)
    {
        $this->_calc_references = $value;
    }

    public function addCalcReference($key, $ref) {
        $this->_calc_references[$key] = $ref;
    }

    public function deleteCalcReference($key) {
        unset($this->_calc_references[$key]);
    }

    /**
     * @return array
     */
    public function getAdditionalInfos()
    {
        return $this->_additionalInfos;
    }

    /**
     * @param array $additionalInfos
     */
    public function setAdditionalInfos($additionalInfos)
    {
        $this->_additionalInfos = $additionalInfos;
    }
}