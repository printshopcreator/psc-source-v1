<?php

function tonumber($value)
{
    if(strpos($value,',') == (strlen($value)-1)) {
         return str_replace(',', '', $value);
    }

    return str_replace(',', '.', $value);

}

class TP_Calc {

    protected $xml;
    protected $logical;
    protected $activeArtikel;
    protected $overwrites;
    protected $precalc = array();
    protected $special;
    protected $isAjax = false;
    protected $calcArr = array();
    protected $calc_value = array();
    protected $actCount = 0;
    protected $calc_value_precalc = array();
    protected $precalcuses = array();
    protected $account_id = false;
    protected $calcParams = null;
    protected $calcFormel = null;

    function __construct($file, $content = '', $ajax = false)
    {

        $this->isAjax = $ajax;
        try {
            if (file_exists($file) && $file != null)
                $this->xml = new SimpleXMLElement($file, null, true);
            if ($content != '' && $file == null) {
                $this->xml = new SimpleXMLElement($content, null, false);
            }
        } catch (Exception $e) {
            Zend_Registry::get('log')->debug($e->getMessage());
            die('ERROR CALC');
        }
    }

    public function getOptionsArray()
    {
        return $this->activeArtikel ['options'];
    }

    public function getArticleArray()
    {
        return $this->activeArtikel;
    }

    public function parse($view, $special = false, $params = array(), $addPrice = 0)
    {
        $this->special = $special;
        $artikelcount = 0;
        foreach ($this->xml as $node) {
            $this->parseArticle($node, $artikelcount, $view);
            $artikelcount++;
            $this->actCount++;
        }
        $view->getRequest()->setParam('reload', 0);
        $view->getRequest()->setPost('reload', 0);
        $form = $this->createForm($view, $params, $addPrice);
        return $form;
    }

    protected function parseArticle($nodes, $artikelcount, $view)
    {
        $view->view->debug = array();
        if (isset($view->view->user) && $view->view->user->test_calc) {
            $this->calcParams = $view->view->shop->test_parameter;
            $this->calcFormel = $view->view->shop->test_formel;
        } else {
            $this->calcParams = $view->view->shop->parameter;
            $this->calcFormel = $view->view->shop->formel;
        }

        $view->view->calc_steps = array();

        $view->view->calc_steps_jquery = array();
        // Artikelname
        if ((string) $an = & $nodes->name)
            $this->logical [$artikelcount] ['name'] = (string) $an;
        // Kommentar
        if ((string) $kt = & $nodes->kommentar)
            $this->logical [$artikelcount] ['kommentar'] = (string) $kt;
        // Format
        if (isset($nodes->columns)) {
            foreach ($nodes->columns->children() as $columns) {
                $view->view->calc_steps[(string)$columns['id']] = array('elements' => array(), 'name' => (string)$columns['name'], 'price' => (bool)$columns['price']);
                foreach ($columns->children() as $set) {
                    $view->view->calc_steps[(string)$columns['id']]['elements'][(string)$set['id']] = array(
                        'id' => (string)$set['id'],
                        'name' => (string)$set['name'],
                        'helplink' => (string)$set['helplink'],
                        'description' => (string)$set['description'],
                        'elements' => array()
                    );
                }
                $view->view->calc_steps_jquery[] = array('id' => (string)$columns['id']);
            }
        }

        if (isset($nodes->precalc)) {

            if ((string) $nodes->precalc->attributes()->horizontal != "") {
                $this->logical [$artikelcount] ['precalc_h'] = (string) $nodes->precalc->attributes()->horizontal;
            }
            if ((int) $nodes->precalc->attributes()->mode != "" && (int) $nodes->precalc->attributes()->mode != 1) {
                foreach ($nodes->precalc->children() as $calc) {
                    $this->logical [$artikelcount] ['precalc'] [(string) $calc ['id']] = array('horizontal' => (string) $calc->attributes()->horizontal, 'data' => (string) $calc->attributes()->data, 'options' => array());
                    foreach ($calc->calc as $chielda) {
                        
                        $this->logical [$artikelcount] ['precalc'] [(string) $calc ['id']]['options'] [(string)$chielda['name']] = array();
                        foreach ($chielda->children() as $key => $chield) {
                            $this->logical [$artikelcount] ['precalc'] [(string) $calc ['id']]['options'] [(string)$chielda['name']][$key] = (string) $chield;
                        }
                    }
                    
                }

                $this->logical [$artikelcount] ['precalc_mode'] = (int) $nodes->precalc->attributes()->mode;
                $this->logical [$artikelcount] ['precalc_ddmin'] = (int) $nodes->precalc->attributes()->ddmin;
                $this->logical [$artikelcount] ['precalc_ddmax'] = (int) $nodes->precalc->attributes()->ddmax;
                $this->logical [$artikelcount] ['display'] = true;
                if(isset($nodes->precalc->attributes()->display)) {
                    $this->logical [$artikelcount] ['display'] = (int) $nodes->precalc->attributes()->display;
                }
            } else {

                foreach ($nodes->precalc->calc as $calc) {
                    $this->logical [$artikelcount] ['precalc'] [(string) $calc ['name']] = array();
                    foreach ($calc->children() as $key => $chield) {
                        $this->logical [$artikelcount] ['precalc'] [(string) $calc ['name']] [$key] = (string) $chield;
                    }
                }

                $this->logical [$artikelcount] ['precalc_mode'] = 1;
                $this->logical [$artikelcount] ['display'] = true;
                if(isset($nodes->precalc->attributes()->display)) {
                    $this->logical [$artikelcount] ['display'] = (int) $nodes->precalc->attributes()->display;
                }
            }
        }

        if ((string) $nodes->width != "")
            $this->logical [$artikelcount] ['width'] = (string) $nodes->width;
        if ((string) $nodes->height != "")
            $this->logical [$artikelcount] ['height'] = (string) $nodes->height;

        if (isset($view->view->user) && $view->view->user->test_calc) {
            $view->view->debug[] = 'IM TEST MODE<br/><br/>';
            $template = simplexml_load_string(str_replace('\\"', '"', '<root>' . $view->install->calc_template_test . '</root>'));
        } else {
            $template = simplexml_load_string(str_replace('\\"', '"', '<root>' . $view->install->calc_template . '</root>'));
        }

        foreach ($nodes->option as $node) {

            if (strtolower((string) $node->attributes()->type) == 'template') {
                $default = false;
                if ((string) $node->attributes()->default != "") {
                    $default = (string) $node->attributes()->default;
                }

                $node = $template->xpath('//option[@id="' . (string) $node->attributes()->select . '"]');
                $node = $node[0];

                if ($default == false) {
                    if ($node) {
                        $this->logical [$artikelcount] ['options'] [(string) $node->attributes()->id] ['Default'] = (string) $node->attributes()->default;
                        $default = (string) $node->attributes()->default;
                    } else {

                        continue;
                    }
                } else {
                    $this->logical [$artikelcount] ['options'] [(string) $node->attributes()->id] ['Default'] = $default;
                }
            } else {
                $this->logical [$artikelcount] ['options'] [(string) $node->attributes()->id] ['Default'] = (string) $node->attributes()->default;
                $default = (string) $node->attributes()->default;
            }

            if(count($view->view->calc_steps) > 0
                && isset($node->attributes()->col)
                && isset($node->attributes()->set)) {

                $view->view->calc_steps[(string) $node->attributes()->col]['elements'][(string) $node->attributes()->set]['elements'][] = array('id' => (string) $node->attributes()->id);
            }

            
            $this->logical [$artikelcount] ['options'] [(string) $node->attributes()->id] ['Type'] = (string) $node->attributes()->type;

            if ($this->logical [$artikelcount] ['options'] [(string) $node->attributes()->id] ['Type'] == 'TextArea' || $this->logical [$artikelcount] ['options'] [(string) $node->attributes()->id] ['Type'] == 'Textarea' || $this->logical [$artikelcount] ['options'] [(string) $node->attributes()->id] ['Type'] == 'Input' || $this->logical [$artikelcount] ['options'] [(string) $node->attributes()->id] ['Type'] == 'Hidden') {
                foreach ($node->children() as $key => $nextNode) {

                    $grenzecount = 0;
                    if (isset($nextNode->attributes()->formel)) {
                        $this->logical [$artikelcount] ['options'] [(string) $node->attributes()->id] ['options'] ['calcs'][$key] = $nextNode->attributes()->formel;
                    }
                    if (is_array((array) $nextNode)) {
                        foreach ($nextNode->grenze as $grenze) {
                            if ((string) $grenze->attributes()->value == null || (string) $grenze->attributes()->value == "") {
                                $this->logical [$artikelcount] ['options'] [(string) $node->attributes()->id] ['options'] ['input'] [(string) $key] [$grenzecount] = array('recrusiv' => null, 'overwrite' => (string) $grenze->attributes()->overwrite, 'option' => $grenze, 'preis' => (string) $grenze->attributes()->preis, 'pauschale' => (string) $grenze->attributes()->pauschale, 'formel' => (string) $grenze->attributes()->formel, 'calc_value' => (string) $grenze->attributes()->calc_value, 'calc_value1' => (string) $grenze->attributes()->calc_value_1, 'calc_value_2' => (string) $grenze->attributes()->calc_value_2, 'calc_value_3' => (string) $grenze->attributes()->calc_value_3, 'calc_value_4' => (string) $grenze->attributes()->calc_value_4, 'calc_value_5' => (string) $grenze->attributes()->calc_value_5, 'calc_value_6' => (string) $grenze->attributes()->calc_value_6, 'calc_value_7' => (string) $grenze->attributes()->calc_value_7, 'calc_value_8' => (string) $grenze->attributes()->calc_value_8, 'calc_value_9' => (string) $grenze->attributes()->calc_value_9, 'calc_value_10' => (string) $grenze->attributes()->calc_value_10);
                            } else {
                                $this->logical [$artikelcount] ['options'] [(string) $node->attributes()->id] ['options'] ['input'] [(string) $key] [$grenzecount] = array('recrusiv' => $grenze, 'overwrite' => (string) $grenze->attributes()->overwrite, 'option' => (string) $grenze->attributes()->value, 'preis' => (string) $grenze->attributes()->preis, 'pauschale' => (string) $grenze->attributes()->pauschale, 'formel' => (string) $grenze->attributes()->formel, 'calc_value' => (string) $grenze->attributes()->calc_value, 'calc_value1' => (string) $grenze->attributes()->calc_value_1, 'calc_value_2' => (string) $grenze->attributes()->calc_value_2, 'calc_value_3' => (string) $grenze->attributes()->calc_value_3, 'calc_value_4' => (string) $grenze->attributes()->calc_value_4, 'calc_value_5' => (string) $grenze->attributes()->calc_value_5, 'calc_value_6' => (string) $grenze->attributes()->calc_value_6, 'calc_value_7' => (string) $grenze->attributes()->calc_value_7, 'calc_value_8' => (string) $grenze->attributes()->calc_value_8, 'calc_value_9' => (string) $grenze->attributes()->calc_value_9, 'calc_value_10' => (string) $grenze->attributes()->calc_value_10);
                            }
                            $grenzecount++;
                        }
                    }
                }
            }

            $this->logical [$artikelcount] ['options'] [(string) $node->attributes()->id] ['Width'] = (string) $node->attributes()->width;
            $this->logical [$artikelcount] ['options'] [(string) $node->attributes()->id] ['Height'] = (string) $node->attributes()->height;
            $this->logical [$artikelcount] ['options'] [(string) $node->attributes()->id] ['Value'] = (string) $node->attributes()->value;
            $this->logical [$artikelcount] ['options'] [(string) $node->attributes()->id] ['Name'] = (string) $node->attributes()->name;


            if (($view->getRequest()->getParam((string) $node->attributes()->id) == "" && $view->getRequest()->getParam('reload') != '1') || ($view->getRequest()->getParam('reload') == '1' && $this->logical [$artikelcount] ['name'] == $view->getRequest()->getParam('kalk_artikel'))) {
                $view->getRequest()->setParam((string) $node->attributes()->id, $default);
                $view->getRequest()->setPost((string) $node->attributes()->id, $default);
            }

            $this->logical [$artikelcount] ['options'] [(string) $node->attributes()->id] ['Require'] = false;
            $this->logical [$artikelcount] ['options'] [(string) $node->attributes()->id] ['site'] = false;
            $this->logical [$artikelcount] ['options'] [(string) $node->attributes()->id] ['printfont'] = false;
            $this->logical [$artikelcount] ['options'] [(string) $node->attributes()->id] ['printfontsize'] = false;
            $this->logical [$artikelcount] ['options'] [(string) $node->attributes()->id] ['printfontbold'] = false;
            $this->logical [$artikelcount] ['options'] [(string) $node->attributes()->id] ['printmaxh'] = false;
            $this->logical [$artikelcount] ['options'] [(string) $node->attributes()->id] ['printmaxw'] = false;
            $this->logical [$artikelcount] ['options'] [(string) $node->attributes()->id] ['showx'] = false;
            $this->logical [$artikelcount] ['options'] [(string) $node->attributes()->id] ['showy'] = false;
            $this->logical [$artikelcount] ['options'] [(string) $node->attributes()->id] ['showpercent'] = false;
            $this->logical [$artikelcount] ['options'] [(string) $node->attributes()->id] ['showcolor'] = false;
            $this->logical [$artikelcount] ['options'] [(string) $node->attributes()->id] ['showfont'] = false;
            $this->logical [$artikelcount] ['options'] [(string) $node->attributes()->id] ['hidden'] = false;
            $this->logical [$artikelcount] ['options'] [(string) $node->attributes()->id] ['amount'] = true;
            $this->logical [$artikelcount] ['options'] [(string) $node->attributes()->id] ['exportAjax'] = false;
            $this->logical [$artikelcount] ['options'] [(string) $node->attributes()->id] ['displayOnly'] = false;

            if ((string) $node->attributes()->require == true)
                $this->logical [$artikelcount] ['options'] [(string) $node->attributes()->id] ['Require'] = (string) $node->attributes()->require;
            if ((string) $node->attributes()->mode == true)
                $this->logical [$artikelcount] ['options'] [(string) $node->attributes()->id] ['mode'] = (string) $node->attributes()->mode;
            if ((string) $node->attributes()->standardtext == true)
                $this->logical [$artikelcount] ['options'] [(string) $node->attributes()->id] ['standardtext'] = (string) $node->attributes()->standardtext;
            if ((string) $node->attributes()->layouterlong == true)
                $this->logical [$artikelcount] ['options'] [(string) $node->attributes()->id] ['layouterwidth'] = (string) $node->attributes()->layouterlong;
            if ((string) $node->attributes()->layoutershort == true)
                $this->logical [$artikelcount] ['options'] [(string) $node->attributes()->id] ['layouterheight'] = (string) $node->attributes()->layoutershort;
            if ((string) $node->attributes()->help != "")
                $this->logical [$artikelcount] ['options'] [(string) $node->attributes()->id] ['help'] = (string) $node->attributes()->help;
            if ((string) $node->attributes()->helplink != "")
                $this->logical [$artikelcount] ['options'] [(string) $node->attributes()->id] ['helplink'] = (string) $node->attributes()->helplink;
            if ((string) $node->attributes()->container == true)
                $this->logical [$artikelcount] ['options'] [(string) $node->attributes()->id] ['container'] = (string) $node->attributes()->container;

            if ((string) $node->attributes()->site != "")
                $this->logical [$artikelcount] ['options'] [(string) $node->attributes()->id] ['site'] = (string) $node->attributes()->site;
            if ((string) $node->attributes()->min != "")
                $this->logical [$artikelcount] ['options'] [(string) $node->attributes()->id] ['min'] = (string) $node->attributes()->min;
            if ((string) $node->attributes()->max != "")
                $this->logical [$artikelcount] ['options'] [(string) $node->attributes()->id] ['max'] = (string) $node->attributes()->max;
            if ((string) $node->attributes()->exportAjax != "")
                $this->logical [$artikelcount] ['options'] [(string) $node->attributes()->id] ['exportAjax'] = (int) $node->attributes()->exportAjax;
            if ((string) $node->attributes()->displayOnly != "")
                $this->logical [$artikelcount] ['options'] [(string) $node->attributes()->id] ['displayOnly'] = (int) $node->attributes()->displayOnly;
            if ((string) $node->attributes()->validate != "")
            	$this->logical [$artikelcount] ['options'] [(string) $node->attributes()->id] ['validate'] = (int) $node->attributes()->validate;
            if ((string) $node->attributes()->hidden != "")
                $this->logical [$artikelcount] ['options'] [(string) $node->attributes()->id] ['hidden'] = true;
            if ((string) $node->attributes()->amount != "")
                $this->logical [$artikelcount] ['options'] [(string) $node->attributes()->id] ['amount'] = (int) $node->attributes()->amount;
            if ((string) $node->attributes()->printfont != "")
                $this->logical [$artikelcount] ['options'] [(string) $node->attributes()->id] ['printfont'] = (string) $node->attributes()->printfont;
            if ((string) $node->attributes()->printfontsize != "")
                $this->logical [$artikelcount] ['options'] [(string) $node->attributes()->id] ['printfontsize'] = (string) $node->attributes()->printfontsize;
            if ((string) $node->attributes()->printfontbold != "")
                $this->logical [$artikelcount] ['options'] [(string) $node->attributes()->id] ['printfontbold'] = (string) $node->attributes()->printfontbold;
            if ((string) $node->attributes()->printmaxw != "")
                $this->logical [$artikelcount] ['options'] [(string) $node->attributes()->id] ['printmaxw'] = (string) $node->attributes()->printmaxw;
            if ((string) $node->attributes()->printmaxh != "")
                $this->logical [$artikelcount] ['options'] [(string) $node->attributes()->id] ['printmaxh'] = (string) $node->attributes()->printmaxh;

            if ((string) $node->attributes()->showX != "")
                $this->logical [$artikelcount] ['options'] [(string) $node->attributes()->id] ['showx'] = (string) $node->attributes()->showX;
            if ((string) $node->attributes()->showY != "")
                $this->logical [$artikelcount] ['options'] [(string) $node->attributes()->id] ['showy'] = (string) $node->attributes()->showY;
            if ((string) $node->attributes()->showPercent != "")
                $this->logical [$artikelcount] ['options'] [(string) $node->attributes()->id] ['showpercent'] = (string) $node->attributes()->showPercent;
            if ((string) $node->attributes()->showColor != "")
                $this->logical [$artikelcount] ['options'] [(string) $node->attributes()->id] ['showcolor'] = (string) $node->attributes()->showColor;
            if ((string) $node->attributes()->showFont != "")
                $this->logical [$artikelcount] ['options'] [(string) $node->attributes()->id] ['showfont'] = (string) $node->attributes()->showFont;


            if (isset($node->grenzen)) {
                foreach ($node->grenzen->children() as $key => $nextNode) {

                    $grenzecount = 0;
                    if (isset($nextNode->attributes()->formel)) {
                        $this->logical [$artikelcount] ['options'] [(string) $node->attributes()->id] ['grenzen']['calcs'][$key] = $nextNode->attributes()->formel;
                    }
                    if (is_array((array) $nextNode)) {
                        foreach ($nextNode->grenze as $grenze) {
                            if ((string) $grenze->attributes()->value == null || (string) $grenze->attributes()->value == "") {
                                $this->logical [$artikelcount] ['options'] [(string) $node->attributes()->id] ['grenzen'][(string) $key] [$grenzecount] = array('recrusiv' => null, 'overwrite' => (string) $grenze->attributes()->overwrite, 'option' => $grenze, 'preis' => (string) $grenze->attributes()->preis, 'pauschale' => (string) $grenze->attributes()->pauschale, 'formel' => (string) $grenze->attributes()->formel, 'calc_value' => (string) $grenze->attributes()->calc_value, 'calc_value1' => (string) $grenze->attributes()->calc_value_1, 'calc_value_2' => (string) $grenze->attributes()->calc_value_2, 'calc_value_3' => (string) $grenze->attributes()->calc_value_3, 'calc_value_4' => (string) $grenze->attributes()->calc_value_4, 'calc_value_5' => (string) $grenze->attributes()->calc_value_5, 'calc_value_6' => (string) $grenze->attributes()->calc_value_6, 'calc_value_7' => (string) $grenze->attributes()->calc_value_7, 'calc_value_8' => (string) $grenze->attributes()->calc_value_8, 'calc_value_9' => (string) $grenze->attributes()->calc_value_9, 'calc_value_10' => (string) $grenze->attributes()->calc_value_10);
                            } else {
                                $this->logical [$artikelcount] ['options'] [(string) $node->attributes()->id] ['grenzen'][(string) $key] [$grenzecount] = array('recrusiv' => $grenze, 'overwrite' => (string) $grenze->attributes()->overwrite, 'option' => (string) $grenze->attributes()->value, 'preis' => (string) $grenze->attributes()->preis, 'pauschale' => (string) $grenze->attributes()->pauschale, 'formel' => (string) $grenze->attributes()->formel, 'calc_value' => (string) $grenze->attributes()->calc_value, 'calc_value1' => (string) $grenze->attributes()->calc_value_1, 'calc_value_2' => (string) $grenze->attributes()->calc_value_2, 'calc_value_3' => (string) $grenze->attributes()->calc_value_3, 'calc_value_4' => (string) $grenze->attributes()->calc_value_4, 'calc_value_5' => (string) $grenze->attributes()->calc_value_5, 'calc_value_6' => (string) $grenze->attributes()->calc_value_6, 'calc_value_7' => (string) $grenze->attributes()->calc_value_7, 'calc_value_8' => (string) $grenze->attributes()->calc_value_8, 'calc_value_9' => (string) $grenze->attributes()->calc_value_9, 'calc_value_10' => (string) $grenze->attributes()->calc_value_10);
                            }
                            $grenzecount++;
                        }
                    }
                }
            }

            $optioncount = 0;
            foreach ($node->opt as $opt) {

                // Value
                $value = (string) $opt->attributes()->id;
                $this->logical [$artikelcount] ['options'] [(string) $node->attributes()->id] ['options'] [$optioncount] ['id'] = (string) $value;


                if ((string) $value = & $opt->attributes()->name) {
                    $this->logical [$artikelcount] ['options'] [(string) $node->attributes()->id] ['options'] [$optioncount] ['name'] = (string) $value;
                }

                if ((string) $value = & $opt->attributes()->default) {
                    $this->logical [$artikelcount] ['options'] [(string) $node->attributes()->id] ['options'] [$optioncount] ['default'] = (string) $value;
                }

                // Vorlagen


                foreach ($opt->children() as $key => $nextNode) {

                    $grenzecount = 0;
                    if (isset($nextNode->attributes()->formel)) {
                        $this->logical [$artikelcount] ['options'] [(string) $node->attributes()->id] ['options'] [$optioncount]['calcs'][$key] = $nextNode->attributes()->formel;
                    }
                    if (is_array((array) $nextNode)) {
                        foreach ($nextNode->grenze as $grenze) {
                            if ((string) $grenze->attributes()->value == null || (string) $grenze->attributes()->value == "") {
                                $this->logical [$artikelcount] ['options'] [(string) $node->attributes()->id] ['options'] [$optioncount] [(string) $key] [$grenzecount] = array('recrusiv' => null, 'overwrite' => (string) $grenze->attributes()->overwrite, 'option' => $grenze, 'preis' => (string) $grenze->attributes()->preis, 'pauschale' => (string) $grenze->attributes()->pauschale, 'formel' => (string) $grenze->attributes()->formel, 'calc_value' => (string) $grenze->attributes()->calc_value, 'calc_value1' => (string) $grenze->attributes()->calc_value_1, 'calc_value_2' => (string) $grenze->attributes()->calc_value_2, 'calc_value_3' => (string) $grenze->attributes()->calc_value_3, 'calc_value_4' => (string) $grenze->attributes()->calc_value_4, 'calc_value_5' => (string) $grenze->attributes()->calc_value_5, 'calc_value_6' => (string) $grenze->attributes()->calc_value_6, 'calc_value_7' => (string) $grenze->attributes()->calc_value_7, 'calc_value_8' => (string) $grenze->attributes()->calc_value_8, 'calc_value_9' => (string) $grenze->attributes()->calc_value_9, 'calc_value_10' => (string) $grenze->attributes()->calc_value_10);
                            } else {
                                $this->logical [$artikelcount] ['options'] [(string) $node->attributes()->id] ['options'] [$optioncount] [(string) $key] [$grenzecount] = array('recrusiv' => $grenze, 'overwrite' => (string) $grenze->attributes()->overwrite, 'option' => (string) $grenze->attributes()->value, 'preis' => (string) $grenze->attributes()->preis, 'pauschale' => (string) $grenze->attributes()->pauschale, 'formel' => (string) $grenze->attributes()->formel, 'calc_value' => (string) $grenze->attributes()->calc_value, 'calc_value1' => (string) $grenze->attributes()->calc_value_1, 'calc_value_2' => (string) $grenze->attributes()->calc_value_2, 'calc_value_3' => (string) $grenze->attributes()->calc_value_3, 'calc_value_4' => (string) $grenze->attributes()->calc_value_4, 'calc_value_5' => (string) $grenze->attributes()->calc_value_5, 'calc_value_6' => (string) $grenze->attributes()->calc_value_6, 'calc_value_7' => (string) $grenze->attributes()->calc_value_7, 'calc_value_8' => (string) $grenze->attributes()->calc_value_8, 'calc_value_9' => (string) $grenze->attributes()->calc_value_9, 'calc_value_10' => (string) $grenze->attributes()->calc_value_10);
                            }
                            $grenzecount++;
                        }
                    }
                }

                $optioncount++;
            }


        }
    }

    protected function getAmountLimit($limits, $amount, $type, $params = array(), $keyval = '')
    {

        $i = 0;
        $found = false;

        foreach ($limits as $limit) {
            $this->calcArr[$keyval][$i][$type] = $limit[$type];

            $grenzen = explode('-', trim($limit ['option']));
            $this->calcArr[$keyval][$i]['from'] = $grenzen[0];

            if (isset($grenzen[1])) {
                $this->calcArr[$keyval][$i]['to'] = $grenzen[1];
            } else {
                $this->calcArr[$keyval][$i]['to'] = false;
            }

            if ((preg_match("/^([0-9a-zA-Z_]+)$/", trim($limit ['option']), $regs) && (isset($regs [1]) && $regs [1] == $amount) || (strpos('_' . $keyval, 'kunde') && count($regs) && $regs [1] == 0) ) && $found == false) {
                if (isset($limit ['recrusiv']) && $limit ['recrusiv'] != null) {
                    foreach ($limit ['recrusiv']->children() as $key => $nextNode) {
                        (isset($params [$key])) ? $paramsection = $params[$key] : $paramsection = 0;
                        if (isset($nextNode->attributes()->formel)) {
                            $paramsection = $this->createFormel((string)$nextNode->attributes()->formel, $params);
                        }
                        $grenzecount = 0;
                        foreach ($nextNode->grenze as $grenze) {
                            if ((string) $grenze->attributes()->value == null || (string) $grenze->attributes()->value == "") {
                                $row [$grenzecount] = array('recrusiv' => null, 'option' => (string) $grenze, 'preis' => (string) $grenze->attributes()->preis, 'pauschale' => (string) $grenze->attributes()->pauschale, 'overwrite' => (string) $grenze->attributes()->overwrite, 'formel' => (string) $grenze->attributes()->formel, 'calc_value' => (string) $grenze->attributes()->calc_value, 'calc_value1' => (string) $grenze->attributes()->calc_value_1, 'calc_value_2' => (string) $grenze->attributes()->calc_value_2, 'calc_value_3' => (string) $grenze->attributes()->calc_value_3, 'calc_value_4' => (string) $grenze->attributes()->calc_value_4, 'calc_value_5' => (string) $grenze->attributes()->calc_value_5, 'calc_value_6' => (string) $grenze->attributes()->calc_value_6, 'calc_value_7' => (string) $grenze->attributes()->calc_value_7, 'calc_value_8' => (string) $grenze->attributes()->calc_value_8, 'calc_value_9' => (string) $grenze->attributes()->calc_value_9, 'calc_value_10' => (string) $grenze->attributes()->calc_value_10);
                            } else {
                                $row [$grenzecount] = array('recrusiv' => $grenze, 'option' => (string) $grenze->attributes()->value, 'preis' => (string) $grenze->attributes()->preis, 'pauschale' => (string) $grenze->attributes()->pauschale, 'overwrite' => (string) $grenze->attributes()->overwrite, 'formel' => (string) $grenze->attributes()->formel, 'calc_value' => (string) $grenze->attributes()->calc_value, 'calc_value1' => (string) $grenze->attributes()->calc_value_1, 'calc_value_2' => (string) $grenze->attributes()->calc_value_2, 'calc_value_3' => (string) $grenze->attributes()->calc_value_3, 'calc_value_4' => (string) $grenze->attributes()->calc_value_4, 'calc_value_5' => (string) $grenze->attributes()->calc_value_5, 'calc_value_6' => (string) $grenze->attributes()->calc_value_6, 'calc_value_7' => (string) $grenze->attributes()->calc_value_7, 'calc_value_8' => (string) $grenze->attributes()->calc_value_8, 'calc_value_9' => (string) $grenze->attributes()->calc_value_9, 'calc_value_10' => (string) $grenze->attributes()->calc_value_10);
                            }
                            $grenzecount++;
                        }
                        $value = $this->getAmountLimit($row, $paramsection, $type, $params, $keyval . '_' . $key);

                        if ($value != "") {
                            $this->calc_value[$keyval . '_' . $key] = $value;
                            return $value;
                        }
                    }
                }
                if (isset($limit ['overwrite']) && $limit ['overwrite'] != "") {
                    $exp = explode('|', $limit ['overwrite']);
                    $this->overwrites [$exp [0]] [$exp [1]] = $exp [2];
                }
                $this->calcArr[$keyval]['found'] = $i;

                $found = $limit [$type];
            } elseif ((preg_match("/^([0-9]+)-([0-9]+)$/", trim($limit ['option']), $regs) && ($amount >= $regs [1] && $amount <= $regs [2])) && $found == false) {

                if (isset($limit ['recrusiv']) && $limit ['recrusiv'] != null) {
                    foreach ($limit ['recrusiv']->children() as $key => $nextNode) {
                        (isset($params [$key])) ? $paramsection = $params[$key] : $paramsection = 0;
                        if (isset($nextNode->attributes()->formel)) {
                            $paramsection = $this->createFormel((string)$nextNode->attributes()->formel, $params);
                        }
                        $grenzecount = 0;
                        foreach ($nextNode->grenze as $grenze) {
                            if ((string) $grenze->attributes()->value == null || (string) $grenze->attributes()->value == "") {
                                $row [$grenzecount] = array('recrusiv' => null, 'option' => (string) $grenze, 'preis' => (string) $grenze->attributes()->preis, 'pauschale' => (string) $grenze->attributes()->pauschale, 'overwrite' => (string) $grenze->attributes()->overwrite, 'formel' => (string) $grenze->attributes()->formel, 'calc_value' => (string) $grenze->attributes()->calc_value, 'calc_value1' => (string) $grenze->attributes()->calc_value_1, 'calc_value_2' => (string) $grenze->attributes()->calc_value_2, 'calc_value_3' => (string) $grenze->attributes()->calc_value_3, 'calc_value_4' => (string) $grenze->attributes()->calc_value_4, 'calc_value_5' => (string) $grenze->attributes()->calc_value_5, 'calc_value_6' => (string) $grenze->attributes()->calc_value_6, 'calc_value_7' => (string) $grenze->attributes()->calc_value_7, 'calc_value_8' => (string) $grenze->attributes()->calc_value_8, 'calc_value_9' => (string) $grenze->attributes()->calc_value_9, 'calc_value_10' => (string) $grenze->attributes()->calc_value_10);
                            } else {
                                $row [$grenzecount] = array('recrusiv' => $grenze, 'option' => (string) $grenze->attributes()->value, 'preis' => (string) $grenze->attributes()->preis, 'pauschale' => (string) $grenze->attributes()->pauschale, 'overwrite' => (string) $grenze->attributes()->overwrite, 'formel' => (string) $grenze->attributes()->formel, 'calc_value' => (string) $grenze->attributes()->calc_value, 'calc_value1' => (string) $grenze->attributes()->calc_value_1, 'calc_value_2' => (string) $grenze->attributes()->calc_value_2, 'calc_value_3' => (string) $grenze->attributes()->calc_value_3, 'calc_value_4' => (string) $grenze->attributes()->calc_value_4, 'calc_value_5' => (string) $grenze->attributes()->calc_value_5, 'calc_value_6' => (string) $grenze->attributes()->calc_value_6, 'calc_value_7' => (string) $grenze->attributes()->calc_value_7, 'calc_value_8' => (string) $grenze->attributes()->calc_value_8, 'calc_value_9' => (string) $grenze->attributes()->calc_value_9, 'calc_value_10' => (string) $grenze->attributes()->calc_value_10);
                            }
                            $grenzecount++;
                        }
                        $value = $this->getAmountLimit($row, $paramsection, $type, $params, $keyval . '_' . $key);

                        if ($value != "") {
                            $this->calc_value[$keyval . '_' . $key] = $value;
                            return $value;
                        }
                    }
                }
                if (isset($limit ['overwrite']) && $limit ['overwrite'] != "") {
                    $exp = explode('|', $limit ['overwrite']);
                    $this->overwrites [$exp [0]] [$exp [1]] = $exp [2];
                }
                $this->calcArr[$keyval]['found'] = $i;

                $found = $limit [$type];
            } elseif ((preg_match("/^([0-9]+)-$/", trim($limit ['option']), $regs) && $amount >= $regs [1]) && $found == false) {

                if (isset($limit ['recrusiv']) && $limit ['recrusiv'] != null) {
                    foreach ($limit ['recrusiv']->children() as $key => $nextNode) {
                        (isset($params [$key])) ? $paramsection = $params[$key] : $paramsection = 0;
                        if (isset($nextNode->attributes()->formel)) {
                            $paramsection = $this->createFormel((string)$nextNode->attributes()->formel, $params);
                        }
                        $grenzecount = 0;
                        foreach ($nextNode->grenze as $grenze) {
                            if ((string) $grenze->attributes()->value == null || (string) $grenze->attributes()->value == "") {
                                $row [$grenzecount] = array('recrusiv' => null, 'option' => (string) $grenze, 'preis' => (string) $grenze->attributes()->preis, 'pauschale' => (string) $grenze->attributes()->pauschale, 'overwrite' => (string) $grenze->attributes()->overwrite, 'formel' => (string) $grenze->attributes()->formel, 'calc_value' => (string) $grenze->attributes()->calc_value, 'calc_value1' => (string) $grenze->attributes()->calc_value_1, 'calc_value_2' => (string) $grenze->attributes()->calc_value_2, 'calc_value_3' => (string) $grenze->attributes()->calc_value_3, 'calc_value_4' => (string) $grenze->attributes()->calc_value_4, 'calc_value_5' => (string) $grenze->attributes()->calc_value_5, 'calc_value_6' => (string) $grenze->attributes()->calc_value_6, 'calc_value_7' => (string) $grenze->attributes()->calc_value_7, 'calc_value_8' => (string) $grenze->attributes()->calc_value_8, 'calc_value_9' => (string) $grenze->attributes()->calc_value_9, 'calc_value_10' => (string) $grenze->attributes()->calc_value_10);
                            } else {
                                $row [$grenzecount] = array('recrusiv' => $grenze, 'option' => (string) $grenze->attributes()->value, 'preis' => (string) $grenze->attributes()->preis, 'pauschale' => (string) $grenze->attributes()->pauschale, 'overwrite' => (string) $grenze->attributes()->overwrite, 'formel' => (string) $grenze->attributes()->formel, 'calc_value' => (string) $grenze->attributes()->calc_value, 'calc_value1' => (string) $grenze->attributes()->calc_value_1, 'calc_value_2' => (string) $grenze->attributes()->calc_value_2, 'calc_value_3' => (string) $grenze->attributes()->calc_value_3, 'calc_value_4' => (string) $grenze->attributes()->calc_value_4, 'calc_value_5' => (string) $grenze->attributes()->calc_value_5, 'calc_value_6' => (string) $grenze->attributes()->calc_value_6, 'calc_value_7' => (string) $grenze->attributes()->calc_value_7, 'calc_value_8' => (string) $grenze->attributes()->calc_value_8, 'calc_value_9' => (string) $grenze->attributes()->calc_value_9, 'calc_value_10' => (string) $grenze->attributes()->calc_value_10);
                            }
                            $grenzecount++;
                        }
                        $value = $this->getAmountLimit($row, $paramsection, $type, $params, $keyval . '_' . $key);

                        if ($value != "") {
                            $this->calc_value[$keyval . '_' . $key] = $value;
                            return $value;
                        }
                    }
                }
                if (isset($limit ['overwrite']) && $limit ['overwrite'] != "") {
                    $exp = explode('|', $limit ['overwrite']);
                    $this->overwrites [$exp [0]] [$exp [1]] = $exp [2];
                }
                $this->calcArr[$keyval]['found'] = $i;

                $found = $limit [$type];
            } elseif (strpos('u' . trim($limit ['option']), ',') >= 1) {
                $matches = explode(',', trim($limit ['option']));

                foreach ($matches as $row_calc) {

                    if (((preg_match("/^([0-9a-zA-Z_]+)$/", trim($row_calc), $regs) && (isset($regs [1]) && $regs [1] == $amount) || (strpos('_' . $keyval, 'kunde') && count($regs) && $regs [1] == 0)) ||
                            (preg_match("/^([0-9]+)-([0-9]+)$/", trim($row_calc), $regs) && ($amount >= $regs [1] && $amount <= $regs [2])) ||
                            (preg_match("/^([0-9]+)-$/", trim($row_calc), $regs) && $amount >= $regs [1]))
                            && $found == false) {
                        if (isset($limit ['recrusiv']) && $limit ['recrusiv'] != null) {
                            foreach ($limit ['recrusiv']->children() as $key => $nextNode) {

                                (isset($params [$key])) ? $paramsection = $params[$key] : $paramsection = 0;
                                if (isset($nextNode->attributes()->formel)) {
                                    $paramsection = $this->createFormel((string)$nextNode->attributes()->formel, $params);
                                }
                                $grenzecount = 0;
                                foreach ($nextNode->grenze as $grenze) {
                                    if ((string) $grenze->attributes()->value == null || (string) $grenze->attributes()->value == "") {
                                        $row [$grenzecount] = array('recrusiv' => null, 'option' => (string) $grenze, 'preis' => (string) $grenze->attributes()->preis, 'pauschale' => (string) $grenze->attributes()->pauschale, 'overwrite' => (string) $grenze->attributes()->overwrite, 'formel' => (string) $grenze->attributes()->formel, 'calc_value' => (string) $grenze->attributes()->calc_value, 'calc_value1' => (string) $grenze->attributes()->calc_value_1, 'calc_value_2' => (string) $grenze->attributes()->calc_value_2, 'calc_value_3' => (string) $grenze->attributes()->calc_value_3, 'calc_value_4' => (string) $grenze->attributes()->calc_value_4, 'calc_value_5' => (string) $grenze->attributes()->calc_value_5, 'calc_value_6' => (string) $grenze->attributes()->calc_value_6, 'calc_value_7' => (string) $grenze->attributes()->calc_value_7, 'calc_value_8' => (string) $grenze->attributes()->calc_value_8, 'calc_value_9' => (string) $grenze->attributes()->calc_value_9, 'calc_value_10' => (string) $grenze->attributes()->calc_value_10);
                                    } else {
                                        $row [$grenzecount] = array('recrusiv' => $grenze, 'option' => (string) $grenze->attributes()->value, 'preis' => (string) $grenze->attributes()->preis, 'pauschale' => (string) $grenze->attributes()->pauschale, 'overwrite' => (string) $grenze->attributes()->overwrite, 'formel' => (string) $grenze->attributes()->formel, 'calc_value' => (string) $grenze->attributes()->calc_value, 'calc_value1' => (string) $grenze->attributes()->calc_value_1, 'calc_value_2' => (string) $grenze->attributes()->calc_value_2, 'calc_value_3' => (string) $grenze->attributes()->calc_value_3, 'calc_value_4' => (string) $grenze->attributes()->calc_value_4, 'calc_value_5' => (string) $grenze->attributes()->calc_value_5, 'calc_value_6' => (string) $grenze->attributes()->calc_value_6, 'calc_value_7' => (string) $grenze->attributes()->calc_value_7, 'calc_value_8' => (string) $grenze->attributes()->calc_value_8, 'calc_value_9' => (string) $grenze->attributes()->calc_value_9, 'calc_value_10' => (string) $grenze->attributes()->calc_value_10);
                                    }
                                    $grenzecount++;
                                }
                                $value = $this->getAmountLimit($row, $paramsection, $type, $params, $keyval . '_' . $key);
                                if ($value != "") {
                                    $this->calc_value[$keyval . '_' . $key] = $value;
                                    return $value;
                                }
                            }
                        }
                        if (isset($limit ['overwrite']) && $limit ['overwrite'] != "") {
                            $exp = explode('|', $limit ['overwrite']);
                            $this->overwrites [$exp [0]] [$exp [1]] = $exp [2];
                        }
                        $this->calcArr[$keyval]['found'] = $i;

                        $found = $limit [$type];
                    }
                }
            }

            $i++;
        }

        return $found;
    }

    protected function getAmountLimitPrecalc($limits, $amount, $type, $params = array(), $keyval = '')
    {

        $i = 0;
        $found = false;
        foreach ($limits as $limit) {
            $this->calcArr[$keyval][$i][$type] = $limit[$type];

            $grenzen = explode('-', trim($limit ['option']));
            $this->calcArr[$keyval][$i]['from'] = $grenzen[0];

            if (isset($grenzen[1])) {
                $this->calcArr[$keyval][$i]['to'] = $grenzen[1];
            } else {
                $this->calcArr[$keyval][$i]['to'] = false;
            }

            if ((preg_match("/^([0-9a-zA-Z_]+)$/", trim($limit ['option']), $regs) && (isset($regs [1]) && $regs [1] == $amount) || (strpos('_' . $keyval, 'kunde') && count($regs) && $regs [1] == 0)) && $found == false) {

                if (isset($limit ['recrusiv']) && $limit ['recrusiv'] != null) {
                    foreach ($limit ['recrusiv']->children() as $key => $nextNode) {
                        $grenzecount = 0;
                        foreach ($nextNode->grenze as $grenze) {
                            if ((string) $grenze->attributes()->value == null || (string) $grenze->attributes()->value == "") {
                                $row [$grenzecount] = array('recrusiv' => null, 'option' => (string) $grenze, 'preis' => (string) $grenze->attributes()->preis, 'pauschale' => (string) $grenze->attributes()->pauschale, 'overwrite' => (string) $grenze->attributes()->overwrite, 'formel' => (string) $grenze->attributes()->formel, 'calc_value' => (string) $grenze->attributes()->calc_value, 'calc_value1' => (string) $grenze->attributes()->calc_value_1, 'calc_value_2' => (string) $grenze->attributes()->calc_value_2, 'calc_value_3' => (string) $grenze->attributes()->calc_value_3, 'calc_value_4' => (string) $grenze->attributes()->calc_value_4, 'calc_value_5' => (string) $grenze->attributes()->calc_value_5, 'calc_value_6' => (string) $grenze->attributes()->calc_value_6, 'calc_value_7' => (string) $grenze->attributes()->calc_value_7, 'calc_value_8' => (string) $grenze->attributes()->calc_value_8, 'calc_value_9' => (string) $grenze->attributes()->calc_value_9, 'calc_value_10' => (string) $grenze->attributes()->calc_value_10);
                            } else {
                                $row [$grenzecount] = array('recrusiv' => $grenze, 'option' => (string) $grenze->attributes()->value, 'preis' => (string) $grenze->attributes()->preis, 'pauschale' => (string) $grenze->attributes()->pauschale, 'overwrite' => (string) $grenze->attributes()->overwrite, 'formel' => (string) $grenze->attributes()->formel, 'calc_value' => (string) $grenze->attributes()->calc_value, 'calc_value1' => (string) $grenze->attributes()->calc_value_1, 'calc_value_2' => (string) $grenze->attributes()->calc_value_2, 'calc_value_3' => (string) $grenze->attributes()->calc_value_3, 'calc_value_4' => (string) $grenze->attributes()->calc_value_4, 'calc_value_5' => (string) $grenze->attributes()->calc_value_5, 'calc_value_6' => (string) $grenze->attributes()->calc_value_6, 'calc_value_7' => (string) $grenze->attributes()->calc_value_7, 'calc_value_8' => (string) $grenze->attributes()->calc_value_8, 'calc_value_9' => (string) $grenze->attributes()->calc_value_9, 'calc_value_10' => (string) $grenze->attributes()->calc_value_10);
                            }
                            $grenzecount++;
                        }
                        $value = $this->getAmountLimitPrecalc($row, $params [$key], $type, $params, $keyval . '_' . $key);

                        if ($value != "") {
                            $this->calc_value_precalc[$keyval . '_' . $key] = $value;
                            return $value;
                        }
                    }
                }
                if (isset($limit ['overwrite']) && $limit ['overwrite'] != "") {
                    $exp = explode('|', $limit ['overwrite']);
                    $this->overwrites [$exp [0]] [$exp [1]] = $exp [2];
                }
                $this->calcArr[$keyval]['found'] = $i;

                $found = $limit [$type];
            } elseif ((preg_match("/^([0-9]+)-([0-9]+)$/", trim($limit ['option']), $regs) && ($amount >= $regs [1] && $amount <= $regs [2])) && $found == false) {

                if (isset($limit ['recrusiv']) && $limit ['recrusiv'] != null) {
                    foreach ($limit ['recrusiv']->children() as $key => $nextNode) {
                        $grenzecount = 0;
                        foreach ($nextNode->grenze as $grenze) {
                            if ((string) $grenze->attributes()->value == null || (string) $grenze->attributes()->value == "") {
                                $row [$grenzecount] = array('recrusiv' => null, 'option' => (string) $grenze, 'preis' => (string) $grenze->attributes()->preis, 'pauschale' => (string) $grenze->attributes()->pauschale, 'overwrite' => (string) $grenze->attributes()->overwrite, 'formel' => (string) $grenze->attributes()->formel, 'calc_value' => (string) $grenze->attributes()->calc_value, 'calc_value1' => (string) $grenze->attributes()->calc_value_1, 'calc_value_2' => (string) $grenze->attributes()->calc_value_2, 'calc_value_3' => (string) $grenze->attributes()->calc_value_3, 'calc_value_4' => (string) $grenze->attributes()->calc_value_4, 'calc_value_5' => (string) $grenze->attributes()->calc_value_5, 'calc_value_6' => (string) $grenze->attributes()->calc_value_6, 'calc_value_7' => (string) $grenze->attributes()->calc_value_7, 'calc_value_8' => (string) $grenze->attributes()->calc_value_8, 'calc_value_9' => (string) $grenze->attributes()->calc_value_9, 'calc_value_10' => (string) $grenze->attributes()->calc_value_10);
                            } else {
                                $row [$grenzecount] = array('recrusiv' => $grenze, 'option' => (string) $grenze->attributes()->value, 'preis' => (string) $grenze->attributes()->preis, 'pauschale' => (string) $grenze->attributes()->pauschale, 'overwrite' => (string) $grenze->attributes()->overwrite, 'formel' => (string) $grenze->attributes()->formel, 'calc_value' => (string) $grenze->attributes()->calc_value, 'calc_value1' => (string) $grenze->attributes()->calc_value_1, 'calc_value_2' => (string) $grenze->attributes()->calc_value_2, 'calc_value_3' => (string) $grenze->attributes()->calc_value_3, 'calc_value_4' => (string) $grenze->attributes()->calc_value_4, 'calc_value_5' => (string) $grenze->attributes()->calc_value_5, 'calc_value_6' => (string) $grenze->attributes()->calc_value_6, 'calc_value_7' => (string) $grenze->attributes()->calc_value_7, 'calc_value_8' => (string) $grenze->attributes()->calc_value_8, 'calc_value_9' => (string) $grenze->attributes()->calc_value_9, 'calc_value_10' => (string) $grenze->attributes()->calc_value_10);
                            }
                            $grenzecount++;
                        }
                        $value = $this->getAmountLimitPrecalc($row, $params [$key], $type, $params, $keyval . '_' . $key);

                        if ($value != "") {
                            $this->calc_value_precalc[$keyval . '_' . $key] = $value;
                            return $value;
                        }
                    }
                }
                if (isset($limit ['overwrite']) && $limit ['overwrite'] != "") {
                    $exp = explode('|', $limit ['overwrite']);
                    $this->overwrites [$exp [0]] [$exp [1]] = $exp [2];
                }
                $this->calcArr[$keyval]['found'] = $i;

                $found = $limit [$type];
            } elseif ((preg_match("/^([0-9]+)-$/", trim($limit ['option']), $regs) && $amount >= $regs [1]) && $found == false) {

                if (isset($limit ['recrusiv']) && $limit ['recrusiv'] != null) {
                    foreach ($limit ['recrusiv']->children() as $key => $nextNode) {
                        $grenzecount = 0;
                        foreach ($nextNode->grenze as $grenze) {
                            if ((string) $grenze->attributes()->value == null || (string) $grenze->attributes()->value == "") {
                                $row [$grenzecount] = array('recrusiv' => null, 'option' => (string) $grenze, 'preis' => (string) $grenze->attributes()->preis, 'pauschale' => (string) $grenze->attributes()->pauschale, 'overwrite' => (string) $grenze->attributes()->overwrite, 'formel' => (string) $grenze->attributes()->formel, 'calc_value' => (string) $grenze->attributes()->calc_value, 'calc_value1' => (string) $grenze->attributes()->calc_value_1, 'calc_value_2' => (string) $grenze->attributes()->calc_value_2, 'calc_value_3' => (string) $grenze->attributes()->calc_value_3, 'calc_value_4' => (string) $grenze->attributes()->calc_value_4, 'calc_value_5' => (string) $grenze->attributes()->calc_value_5, 'calc_value_6' => (string) $grenze->attributes()->calc_value_6, 'calc_value_7' => (string) $grenze->attributes()->calc_value_7, 'calc_value_8' => (string) $grenze->attributes()->calc_value_8, 'calc_value_9' => (string) $grenze->attributes()->calc_value_9, 'calc_value_10' => (string) $grenze->attributes()->calc_value_10);
                            } else {
                                $row [$grenzecount] = array('recrusiv' => $grenze, 'option' => (string) $grenze->attributes()->value, 'preis' => (string) $grenze->attributes()->preis, 'pauschale' => (string) $grenze->attributes()->pauschale, 'overwrite' => (string) $grenze->attributes()->overwrite, 'formel' => (string) $grenze->attributes()->formel, 'calc_value' => (string) $grenze->attributes()->calc_value, 'calc_value1' => (string) $grenze->attributes()->calc_value_1, 'calc_value_2' => (string) $grenze->attributes()->calc_value_2, 'calc_value_3' => (string) $grenze->attributes()->calc_value_3, 'calc_value_4' => (string) $grenze->attributes()->calc_value_4, 'calc_value_5' => (string) $grenze->attributes()->calc_value_5, 'calc_value_6' => (string) $grenze->attributes()->calc_value_6, 'calc_value_7' => (string) $grenze->attributes()->calc_value_7, 'calc_value_8' => (string) $grenze->attributes()->calc_value_8, 'calc_value_9' => (string) $grenze->attributes()->calc_value_9, 'calc_value_10' => (string) $grenze->attributes()->calc_value_10);
                            }
                            $grenzecount++;
                        }
                        $value = $this->getAmountLimitPrecalc($row, $params [$key], $type, $params, $keyval . '_' . $key);

                        if ($value != "") {
                            $this->calc_value_precalc[$keyval . '_' . $key] = $value;
                            return $value;
                        }
                    }
                }
                if (isset($limit ['overwrite']) && $limit ['overwrite'] != "") {
                    $exp = explode('|', $limit ['overwrite']);
                    $this->overwrites [$exp [0]] [$exp [1]] = $exp [2];
                }
                $this->calcArr[$keyval]['found'] = $i;

                $found = $limit [$type];
            } elseif (strpos('u' . trim($limit ['option']), ',') >= 1) {
                $matches = explode(',', trim($limit ['option']));

                foreach ($matches as $row_calc) {

                    if (((preg_match("/^([0-9a-zA-Z_]+)$/", trim($row_calc), $regs) && ($regs [1] == $amount) || (strpos('_' . $keyval, 'kunde') && $regs [1] == 0 && count($regs))) ||
                            (preg_match("/^([0-9]+)-([0-9]+)$/", trim($row_calc), $regs) && ($amount >= $regs [1] && $amount <= $regs [2])) ||
                            (preg_match("/^([0-9]+)-$/", trim($row_calc), $regs) && $amount >= $regs [1]))
                            && $found == false) {
                        if (isset($limit ['recrusiv']) && $limit ['recrusiv'] != null) {
                            foreach ($limit ['recrusiv']->children() as $key => $nextNode) {
                                $grenzecount = 0;
                                foreach ($nextNode->grenze as $grenze) {
                                    if ((string) $grenze->attributes()->value == null || (string) $grenze->attributes()->value == "") {
                                        $row [$grenzecount] = array('recrusiv' => null, 'option' => (string) $grenze, 'preis' => (string) $grenze->attributes()->preis, 'pauschale' => (string) $grenze->attributes()->pauschale, 'overwrite' => (string) $grenze->attributes()->overwrite, 'formel' => (string) $grenze->attributes()->formel, 'calc_value' => (string) $grenze->attributes()->calc_value, 'calc_value1' => (string) $grenze->attributes()->calc_value_1, 'calc_value_2' => (string) $grenze->attributes()->calc_value_2, 'calc_value_3' => (string) $grenze->attributes()->calc_value_3, 'calc_value_4' => (string) $grenze->attributes()->calc_value_4, 'calc_value_5' => (string) $grenze->attributes()->calc_value_5, 'calc_value_6' => (string) $grenze->attributes()->calc_value_6, 'calc_value_7' => (string) $grenze->attributes()->calc_value_7, 'calc_value_8' => (string) $grenze->attributes()->calc_value_8, 'calc_value_9' => (string) $grenze->attributes()->calc_value_9, 'calc_value_10' => (string) $grenze->attributes()->calc_value_10);
                                    } else {
                                        $row [$grenzecount] = array('recrusiv' => $grenze, 'option' => (string) $grenze->attributes()->value, 'preis' => (string) $grenze->attributes()->preis, 'pauschale' => (string) $grenze->attributes()->pauschale, 'overwrite' => (string) $grenze->attributes()->overwrite, 'formel' => (string) $grenze->attributes()->formel, 'calc_value' => (string) $grenze->attributes()->calc_value, 'calc_value1' => (string) $grenze->attributes()->calc_value_1, 'calc_value_2' => (string) $grenze->attributes()->calc_value_2, 'calc_value_3' => (string) $grenze->attributes()->calc_value_3, 'calc_value_4' => (string) $grenze->attributes()->calc_value_4, 'calc_value_5' => (string) $grenze->attributes()->calc_value_5, 'calc_value_6' => (string) $grenze->attributes()->calc_value_6, 'calc_value_7' => (string) $grenze->attributes()->calc_value_7, 'calc_value_8' => (string) $grenze->attributes()->calc_value_8, 'calc_value_9' => (string) $grenze->attributes()->calc_value_9, 'calc_value_10' => (string) $grenze->attributes()->calc_value_10);
                                    }
                                    $grenzecount++;
                                }
                                $value = $this->getAmountLimitPrecalc($row, $params [$key], $type, $params, $keyval . '_' . $key);
                                if ($value != "") {
                                    $this->calc_value_precalc[$keyval . '_' . $key] = $value;
                                    return $value;
                                }
                            }
                        }
                        if (isset($limit ['overwrite']) && $limit ['overwrite'] != "") {
                            $exp = explode('|', $limit ['overwrite']);
                            $this->overwrites [$exp [0]] [$exp [1]] = $exp [2];
                        }
                        $this->calcArr[$keyval]['found'] = $i;

                        $found = $limit [$type];
                    }
                }
            }

            $i++;
        }

        return $found;
    }

    protected function getEnablePreCalc($limits, $amount, $params = array(), $precalcuse = false, $keyval = '')
    {
        $re = false;
        foreach ($limits as $limit) {

            if (preg_match("/^([0-9a-zA-Z_]+)$/", trim($limit), $regs) && ($regs [1] == $amount) || (strpos('_' . $keyval, 'kunde') && $regs [1] == 0 && count($regs))) {
                $re = true;
            } elseif (preg_match("/^([0-9]+)-([0-9]+)$/", trim($limit), $regs) && ($amount >= $regs [1] && $amount <= $regs [2])) {
                $re = true;
            } elseif (preg_match("/^([0-9]+)-$/", trim($limit), $regs) && $amount >= $regs [1]) {
                $re = true;
            } elseif (strpos(trim('u' . $limit), ',') >= 1) {

                $matches = explode(',', trim($limit));

                foreach ($matches as $row) {

                    if ((preg_match("/^([0-9a-zA-Z_]+)$/", trim($row), $regs) && ($regs [1] == $amount) || (strpos('_' . $keyval, 'kunde') && $regs [1] == 0 && count($regs))) ||
                            (preg_match("/^([0-9]+)-([0-9]+)$/", trim($row), $regs) && ($amount >= $regs [1] && $amount <= $regs [2])) ||
                            (preg_match("/^([0-9]+)-$/", trim($row), $regs) && $amount >= $regs [1])) {
                        $re = true;
                    }
                }
            }

            if (isset($limit ['recrusiv']) && $limit ['recrusiv'] != null && $re == true) {
                $re = false;
                foreach ($limit ['recrusiv']->children() as $key => $nextNode) {
                    $grenzecount = 0;
                    foreach ($nextNode->grenze as $grenze) {
                        if ((string) $grenze->attributes()->value == null || (string) $grenze->attributes()->value == "") {
                            $row [$grenzecount] = array('recrusiv' => null, 'option' => (string) $grenze, 'preis' => (string) $grenze->attributes()->preis, 'pauschale' => (string) $grenze->attributes()->pauschale, 'overwrite' => (string) $grenze->attributes()->overwrite, 'formel' => (string) $grenze->attributes()->formel, 'calc_value' => (string) $grenze->attributes()->calc_value, 'calc_value1' => (string) $grenze->attributes()->calc_value_1, 'calc_value_2' => (string) $grenze->attributes()->calc_value_2, 'calc_value_3' => (string) $grenze->attributes()->calc_value_3, 'calc_value_4' => (string) $grenze->attributes()->calc_value_4, 'calc_value_5' => (string) $grenze->attributes()->calc_value_5, 'calc_value_6' => (string) $grenze->attributes()->calc_value_6, 'calc_value_7' => (string) $grenze->attributes()->calc_value_7, 'calc_value_8' => (string) $grenze->attributes()->calc_value_8, 'calc_value_9' => (string) $grenze->attributes()->calc_value_9, 'calc_value_10' => (string) $grenze->attributes()->calc_value_10);
                        } else {
                            $row [$grenzecount] = array('recrusiv' => $grenze, 'option' => (string) $grenze->attributes()->value, 'preis' => (string) $grenze->attributes()->preis, 'pauschale' => (string) $grenze->attributes()->pauschale, 'overwrite' => (string) $grenze->attributes()->overwrite, 'formel' => (string) $grenze->attributes()->formel, 'calc_value' => (string) $grenze->attributes()->calc_value, 'calc_value1' => (string) $grenze->attributes()->calc_value_1, 'calc_value_2' => (string) $grenze->attributes()->calc_value_2, 'calc_value_3' => (string) $grenze->attributes()->calc_value_3, 'calc_value_4' => (string) $grenze->attributes()->calc_value_4, 'calc_value_5' => (string) $grenze->attributes()->calc_value_5, 'calc_value_6' => (string) $grenze->attributes()->calc_value_6, 'calc_value_7' => (string) $grenze->attributes()->calc_value_7, 'calc_value_8' => (string) $grenze->attributes()->calc_value_8, 'calc_value_9' => (string) $grenze->attributes()->calc_value_9, 'calc_value_10' => (string) $grenze->attributes()->calc_value_10);
                        }
                        $grenzecount++;
                    }
                    $value = $this->getEnablePreCalc($row, $params [$key], $params, $precalcuse = false, $keyval . '_' . $key);
                    if ($value != "")
                        $re = $value;
                }
            }
        }
        return $re;
    }

    protected function getEnable($limits, $amount, $params = array(), $precalcuse = false, $keyval = '')
    {
        $re = false;
        foreach ($limits as $limit) {

            if ($precalcuse != false) {
                if (!isset($this->precalcuses[$precalcuse]))
                    $this->precalcuses[$precalcuse] = array();
                $this->precalcuses[$precalcuse][] = trim($limit ['option']);
            }

            if (is_array($amount)) {
                foreach ($amount as $amount_value) {

                    if ((preg_match("/^([0-9a-zA-Z_]+)$/", trim($limit ['option']), $regs) && ($regs [1] == $amount_value) || (strpos('_' . $keyval, 'kunde') && $regs [1] == 0 && count($regs))) ||
                        (preg_match("/^([0-9]+)-([0-9]+)$/", trim($limit ['option']), $regs) && ($amount_value >= $regs [1] && $amount_value <= $regs [2])) ||
                        (preg_match("/^([0-9]+)-$/", trim($limit ['option']), $regs) && $amount_value >= $regs [1])) {

                        $re = true;
                    }
                }
            } elseif (preg_match("/^([0-9a-zA-Z_]+)$/", trim($limit ['option']), $regs) && (isset($regs [1]) && $regs [1] == $amount) || (strpos('_' . $keyval, 'kunde') && count($regs) && $regs [1] == 0)) {
                $re = true;
            } elseif (preg_match("/^([0-9]+)-([0-9]+)$/", trim($limit ['option']), $regs) && ($amount >= $regs [1] && $amount <= $regs [2])) {
                $re = true;
            } elseif (preg_match("/^([0-9]+)-$/", trim($limit ['option']), $regs) && $amount >= $regs [1]) {
                $re = true;
            } elseif (strpos(trim('u' . $limit ['option']), ',') >= 1) {

                $matches = explode(',', trim($limit ['option']));
                foreach ($matches as $row_calc) {
					
                    if ((preg_match("/^([0-9a-zA-Z_]+)$/", trim($row_calc), $regs) && ($regs [1] == $amount) || (strpos('_' . $keyval, 'kunde') && $regs [1] == 0 && count($regs))) ||
                            (preg_match("/^([0-9]+)-([0-9]+)$/", trim($row_calc), $regs) && ($amount >= $regs [1] && $amount <= $regs [2])) ||
                            (preg_match("/^([0-9]+)-$/", trim($row_calc), $regs) && $amount >= $regs [1])) {
                           
                        $re = true;
                    }
                }
            }


            if (isset($limit ['recrusiv']) && $limit ['recrusiv'] != null && $re == true) {
                $re = false;

                foreach ($limit ['recrusiv']->children() as $key => $nextNode) {

                    (isset($params [$key])) ? $paramsection = $params[$key] : $paramsection = 0;
                    if (isset($nextNode->attributes()->formel)) {
                        $paramsection = $this->createFormel((string)$nextNode->attributes()->formel, $params);
                    }

                    $grenzecount = 0;
                    foreach ($nextNode->grenze as $grenze) {
                        if ((string) $grenze->attributes()->value == null || (string) $grenze->attributes()->value == "") {
                            $row [$grenzecount] = array('recrusiv' => null, 'option' => (string) $grenze, 'preis' => (string) $grenze->attributes()->preis, 'pauschale' => (string) $grenze->attributes()->pauschale, 'overwrite' => (string) $grenze->attributes()->overwrite, 'formel' => (string) $grenze->attributes()->formel, 'calc_value' => (string) $grenze->attributes()->calc_value, 'calc_value1' => (string) $grenze->attributes()->calc_value_1, 'calc_value_2' => (string) $grenze->attributes()->calc_value_2, 'calc_value_3' => (string) $grenze->attributes()->calc_value_3, 'calc_value_4' => (string) $grenze->attributes()->calc_value_4, 'calc_value_5' => (string) $grenze->attributes()->calc_value_5, 'calc_value_6' => (string) $grenze->attributes()->calc_value_6, 'calc_value_7' => (string) $grenze->attributes()->calc_value_7, 'calc_value_8' => (string) $grenze->attributes()->calc_value_8, 'calc_value_9' => (string) $grenze->attributes()->calc_value_9, 'calc_value_10' => (string) $grenze->attributes()->calc_value_10);
                        } else {
                            $row [$grenzecount] = array('recrusiv' => $grenze, 'option' => (string) $grenze->attributes()->value, 'preis' => (string) $grenze->attributes()->preis, 'pauschale' => (string) $grenze->attributes()->pauschale, 'overwrite' => (string) $grenze->attributes()->overwrite, 'formel' => (string) $grenze->attributes()->formel, 'calc_value' => (string) $grenze->attributes()->calc_value, 'calc_value1' => (string) $grenze->attributes()->calc_value_1, 'calc_value_2' => (string) $grenze->attributes()->calc_value_2, 'calc_value_3' => (string) $grenze->attributes()->calc_value_3, 'calc_value_4' => (string) $grenze->attributes()->calc_value_4, 'calc_value_5' => (string) $grenze->attributes()->calc_value_5, 'calc_value_6' => (string) $grenze->attributes()->calc_value_6, 'calc_value_7' => (string) $grenze->attributes()->calc_value_7, 'calc_value_8' => (string) $grenze->attributes()->calc_value_8, 'calc_value_9' => (string) $grenze->attributes()->calc_value_9, 'calc_value_10' => (string) $grenze->attributes()->calc_value_10);
                        }
                        $grenzecount++;
                    }

                    $value = $this->getEnable($row, $paramsection, $params, $precalcuse, $keyval . '_' . $key);
                    if ($value != "")
                        $re = $value;

                    if ($re) {
                        return $re;
                    }
                }
            }
        }
        return $re;
    }

    protected function getEnableHidden($limits, $amount, $params = array(), $precalcuse = false, $keyval = '')
    {
        $re = false;
        foreach ($limits as $limit) {

            if ($precalcuse != false) {
                if (!isset($this->precalcuses[$precalcuse]))
                    $this->precalcuses[$precalcuse] = array();
                $this->precalcuses[$precalcuse][] = trim($limit ['option']);
            }

            if (is_array($amount)) {
                foreach ($amount as $amount_value) {

                    if ((preg_match("/^([0-9a-zA-Z_]+)$/", trim($limit ['option']), $regs) && ($regs [1] == $amount_value) || (strpos('_' . $keyval, 'kunde') && $regs [1] == 0 && count($regs))) ||
                        (preg_match("/^([0-9]+)-([0-9]+)$/", trim($limit ['option']), $regs) && ($amount_value >= $regs [1] && $amount_value <= $regs [2])) ||
                        (preg_match("/^([0-9]+)-$/", trim($limit ['option']), $regs) && $amount_value >= $regs [1])) {

                        $re = true;
                    }
                }
            } elseif (preg_match("/^([0-9a-zA-Z_]+)$/", trim($limit ['option']), $regs) && (isset($regs [1]) && $regs [1] == $amount) || (strpos('_' . $keyval, 'kunde') && count($regs) && $regs [1] == 0)) {
                $re = true;
            } elseif (preg_match("/^([0-9]+)-([0-9]+)$/", trim($limit ['option']), $regs) && ($amount >= $regs [1] && $amount <= $regs [2])) {
                $re = true;
            } elseif (preg_match("/^([0-9]+)-$/", trim($limit ['option']), $regs) && $amount >= $regs [1]) {
                $re = true;
            } elseif (strpos(trim('u' . $limit ['option']), ',') >= 1) {

                $matches = explode(',', trim($limit ['option']));
                foreach ($matches as $row_calc) {

                    if ((preg_match("/^([0-9a-zA-Z_]+)$/", trim($row_calc), $regs) && ($regs [1] == $amount) || (strpos('_' . $keyval, 'kunde') && $regs [1] == 0 && count($regs))) ||
                        (preg_match("/^([0-9]+)-([0-9]+)$/", trim($row_calc), $regs) && ($amount >= $regs [1] && $amount <= $regs [2])) ||
                        (preg_match("/^([0-9]+)-$/", trim($row_calc), $regs) && $amount >= $regs [1])) {

                        $re = true;
                    }
                }
            }

            if (isset($limit ['recrusiv']) && $limit ['recrusiv'] != null && $re == true) {
                $re = false;

                foreach ($limit ['recrusiv']->children() as $key => $nextNode) {

                    (isset($params [$key])) ? $paramsection = $params[$key] : $paramsection = 0;
                    if (isset($nextNode->attributes()->formel)) {
                        $paramsection = $this->createFormel((string)$nextNode->attributes()->formel, $params);
                    }

                    $grenzecount = 0;
                    foreach ($nextNode->grenze as $grenze) {
                        if ((string) $grenze->attributes()->value == null || (string) $grenze->attributes()->value == "") {
                            $row [$grenzecount] = array('recrusiv' => null, 'option' => (string) $grenze, 'preis' => (string) $grenze->attributes()->preis, 'pauschale' => (string) $grenze->attributes()->pauschale, 'overwrite' => (string) $grenze->attributes()->overwrite, 'formel' => (string) $grenze->attributes()->formel, 'calc_value' => (string) $grenze->attributes()->calc_value, 'calc_value1' => (string) $grenze->attributes()->calc_value_1, 'calc_value_2' => (string) $grenze->attributes()->calc_value_2, 'calc_value_3' => (string) $grenze->attributes()->calc_value_3, 'calc_value_4' => (string) $grenze->attributes()->calc_value_4, 'calc_value_5' => (string) $grenze->attributes()->calc_value_5, 'calc_value_6' => (string) $grenze->attributes()->calc_value_6, 'calc_value_7' => (string) $grenze->attributes()->calc_value_7, 'calc_value_8' => (string) $grenze->attributes()->calc_value_8, 'calc_value_9' => (string) $grenze->attributes()->calc_value_9, 'calc_value_10' => (string) $grenze->attributes()->calc_value_10);
                        } else {
                            $row [$grenzecount] = array('recrusiv' => $grenze, 'option' => (string) $grenze->attributes()->value, 'preis' => (string) $grenze->attributes()->preis, 'pauschale' => (string) $grenze->attributes()->pauschale, 'overwrite' => (string) $grenze->attributes()->overwrite, 'formel' => (string) $grenze->attributes()->formel, 'calc_value' => (string) $grenze->attributes()->calc_value, 'calc_value1' => (string) $grenze->attributes()->calc_value_1, 'calc_value_2' => (string) $grenze->attributes()->calc_value_2, 'calc_value_3' => (string) $grenze->attributes()->calc_value_3, 'calc_value_4' => (string) $grenze->attributes()->calc_value_4, 'calc_value_5' => (string) $grenze->attributes()->calc_value_5, 'calc_value_6' => (string) $grenze->attributes()->calc_value_6, 'calc_value_7' => (string) $grenze->attributes()->calc_value_7, 'calc_value_8' => (string) $grenze->attributes()->calc_value_8, 'calc_value_9' => (string) $grenze->attributes()->calc_value_9, 'calc_value_10' => (string) $grenze->attributes()->calc_value_10);
                        }
                        $grenzecount++;
                    }

                    $value = $this->getEnableHidden($row, $paramsection, $params, $precalcuse, $keyval . '_' . $key);
                    if ($value != "")
                        $re = $value;

                    if ($re) {
                        return $re;
                    }
                }
            }
        }
        return $re;
    }

    protected function createFormel($formel, $params)
    {

        eval(str_replace('\\', '', $this->calcParams));
        eval(str_replace("#", '"', str_replace('"', "'", str_replace('\\', '', $this->calcFormel))));

        preg_match_all('/\$CV\w*\$CV/', $formel, $founds);
        if (!empty($founds [0])) {

            foreach ($founds [0] as $key => $found) {

                $foundvalue = str_replace('$CV', '', $found);
                if (isset($this->calc_value[$foundvalue])) {
                    $formel = str_replace($found, $this->calc_value[$foundvalue], $formel);
                } else {
                    $formel = str_replace($found, 0, $formel);
                }
            }
        }

        preg_match_all('/\$CV\w*\$CV/', $formel, $foundsSecound);
        if (!empty($foundsSecound [0])) {

            foreach ($foundsSecound [0] as $keySecound => $foundSecound) {

                $foundvalue = str_replace('$CV', '', $foundSecound);
                if (isset($this->calc_value[$foundvalue])) {
                    $formel = str_replace($foundSecound, $this->calc_value[$foundvalue], $formel);
                } else {
                    $formel = str_replace($foundSecound, 0, $formel);
                }
            }
        }

        preg_match_all('/\$CV\w*\$CV/', $formel, $foundsSecound);
        if (!empty($foundsSecound [0])) {

            foreach ($foundsSecound [0] as $keySecound => $foundSecound) {

                $foundvalue = str_replace('$CV', '', $foundSecound);
                if (isset($this->calc_value[$foundvalue])) {
                    $formel = str_replace($foundSecound, $this->calc_value[$foundvalue], $formel);
                } else {
                    $formel = str_replace($foundSecound, 0, $formel);
                }
            }
        }
        
        preg_match_all('/\$CV\w*\$CV/', $formel, $foundsSecound);
        if (!empty($foundsSecound [0])) {

            foreach ($foundsSecound [0] as $keySecound => $foundSecound) {

                $foundvalue = str_replace('$CV', '', $foundSecound);
                if (isset($this->calc_value[$foundvalue])) {
                    $formel = str_replace($foundSecound, $this->calc_value[$foundvalue], $formel);
                } else {
                    $formel = str_replace($foundSecound, 0, $formel);
                }
            }
        }

        preg_match_all('/\$P\w*\$P/', $formel, $founds);
        if (!empty($founds [0])) {

            foreach ($founds [0] as $key => $found) {

                $foundvalue = str_replace('$P', '', $found);
                if (isset($$foundvalue)) {
                    $formel = str_replace($found, $$foundvalue, $formel);
                } else {
                    $formel = str_replace($found, 0, $formel);
                }
            }
        }
        preg_match_all('/\$V\w*\$V/', $formel, $founds);
        if (!empty($founds [0])) {
            foreach ($founds [0] as $key => $found) {
                $foundvalue = str_replace('$V', '', $found);

                if (isset($params [$foundvalue])) {
                    if ($params [$foundvalue] == 'null') {
                        $formel = str_replace($found, 0, $formel);
                    } else {
                        $formel = str_replace($found, ($params [$foundvalue] * 1), $formel);
                    }
                }
            }
        }
        preg_match_all('/\$V\w*\$V/', $formel, $founds);
        if (!empty($founds [0])) {
            foreach ($founds [0] as $key => $found) {
                $foundvalue = str_replace('$V', '', $found);

                if (isset($params [$foundvalue])) {
                    if ($params [$foundvalue] == 'null') {
                        $formel = str_replace($found, 0, $formel);
                    } else {
                        $formel = str_replace($found, ($params [$foundvalue] * 1), $formel);
                    }
                }else{
                    $formel = str_replace($found, 0, $formel);
                }
            }
        }

        eval('$tempwert = ' . $formel . ';');
        return $tempwert;
    }

    protected function createForm($view, $params = array(), $addPrice = 0)
    {

        $params = array_merge($view->getRequest()->getParams(), $params);
        $sc = Zend_Registry::get('shop');
        if ($view->view->user) {
            $params['kunde'] = $view->view->user->id;
            $params['firma'] = $view->view->user->account_id;
        } else {
            $params['kunde'] = 0;
            $params['firma'] = 0;
        }
        $params['shop_id'] = $sc['id'];

        if (!$this->isAjax) {
            $view->view->form->addElementPrefixPath('TP_Form_Decorator', 'TP/Form/Decorator/', 'decorator');
        }

        $tpl = '';
        $search = array();
        $replace = array();
        $singlePreis = 0;
        $preis = array();
        $paus = array();
        $formel = array();

        $preisvorlagen = array();
        $pauschalevorlagen = array();
        $kalkulieren = isset($params ['btn'] ['btnSubmit']) ? true : false;

        // Artikelauswahl
        $artikelarr = array();
        foreach ($this->logical as $artikel) {
            if (count($artikelarr) == 0) {
                if (!isset($params ['kalk_artikel']) && (!isset($selected) || $selected == "")) {
                    $activeartikel = $artikel;
                    $selected = $artikel ['name'];
                    $view->getRequest()->setParam('kalk_artikel', $selected);
                    $view->getRequest()->setPost('kalk_artikel', $selected);
                } else {
                    $selected = $params ['kalk_artikel'];
                }
            }
            if (isset($params ['kalk_artikel']) && $params ['kalk_artikel'] == $artikel ['name'])
                $activeartikel = $artikel;
            array_push($artikelarr, array('key' => $artikel ['name'], 'value' => $artikel ['name']));
        }

        if (!$this->isAjax) {
            if (count($artikelarr) < 2) {
                $view->view->form->addElement('hidden', 'kalk_artikel', array('value' => $artikel ['name']));
                $view->view->form->kalk_artikel->removeDecorator('label')->removeDecorator('HtmlTag');
            } else {

                $view->view->form->addElement('hidden', 'reload', array('value' => 0));
                $view->view->form->reload->removeDecorator('label')->removeDecorator('HtmlTag');

                $kalk_artikel = $view->view->form->createElement('select', 'kalk_artikel')->setLabel('Typ')->setMultiOptions($artikelarr)->setRequired(true)->setAttrib('onChange', 'document.getElementById(\'reload\').value=\'1\';this.form.submit()');

                $view->view->form->addElement($kalk_artikel);
            }
        } else {
            if (count($artikelarr) < 2) {
                $view->view->form[] = array('type' => 'hidden', 'name' => 'kalk_artikel', 'value' => $artikel ['name']);
            } else {
                $view->view->form[] = array('type' => 'hidden', 'name' => 'reload', 'value' => 0);
                $view->view->form[] = array('type' => 'select', 'name' => 'kalk_artikel', 'value' => $selected, 'options' => $artikelarr, 'label' => 'Typ', 'onChange' => 'document.getElementById(\'reload\').value=\'1\';this.form.submit()');
            }
        }

        $params ['kalk_artikel'] = $selected;
        if (!$this->isAjax) {
            $view->view->form->addAttribs(array('class' => 'niceform'));
        }
        $i = 0;

        $basket = new TP_Basket();
        /** @var TP_Basket_Item $tempProdukt */
        $tempProdukt = $basket->getTempProduct();
        $tempProdukt->setLayouterWidth(false);
        $tempProdukt->setLayouterWidthEn(false);
        $tempProdukt->setLayouterHeight(false);
        $tempProdukt->setLayouterHeightEn(false);

        foreach ($activeartikel ['options'] as $key => $art) {

            $druckarr = array();
            $druckarrAjax = array();

            switch ($art ['Type']) {

                case 'Select' :

                    if (isset($art ['mode']) && $art ['mode'] == 'papierdb') {

                        if ($this->overwrites [$key] && $this->overwrites [$key] ['container']) {
                            $art ['container'] = $this->overwrites [$key] ['container'];
                        }

                        $containerXML = simplexml_load_string(str_replace('\\"', '"', $view->view->shop->Install->containerdb));

                        $papieredb = explode(',', $art ['container']);
                        $papiercontainer = array();
                        foreach ($papieredb as $papierkey) {
                            $papiere = $containerXML->xpath('//papiercontainer[@id="' . $papierkey . '"]');

                            $papierdb = array();
                            foreach ($papiere [0]->papier as $papier) {
                                if(isset($papier['admin']) && intval($papier['admin']) == 1 && !$view->view->admin) continue;
                                if (isset($papier->grenzen)) {
                                    $validp = true;
                                    foreach ($papier->grenzen->children() as $section => $nextNode) {
                                        $grenzeSection = array();
                                        $precalcuse = false;
                                        if (isset($params[$key]) && $params[$key] == (string) $papier ['id']) {
                                            $precalcuse = $section;
                                        }
                                        foreach ($nextNode->grenze as $grenze) {
                                            $grenzeSection[] = array('recrusiv' => null, 'overwrite' => (string) $grenze->attributes()->overwrite, 'option' => $grenze, 'preis' => (string) $grenze->attributes()->preis, 'pauschale' => (string) $grenze->attributes()->pauschale, 'formel' => (string) $grenze->attributes()->formel, 'calc_value' => (string) $grenze->attributes()->calc_value, 'calc_value1' => (string) $grenze->attributes()->calc_value_1, 'calc_value_2' => (string) $grenze->attributes()->calc_value_2, 'calc_value_3' => (string) $grenze->attributes()->calc_value_3, 'calc_value_4' => (string) $grenze->attributes()->calc_value_4, 'calc_value_5' => (string) $grenze->attributes()->calc_value_5, 'calc_value_6' => (string) $grenze->attributes()->calc_value_6, 'calc_value_7' => (string) $grenze->attributes()->calc_value_7, 'calc_value_8' => (string) $grenze->attributes()->calc_value_8, 'calc_value_9' => (string) $grenze->attributes()->calc_value_9, 'calc_value_10' => (string) $grenze->attributes()->calc_value_10);
                                        }
                                        (isset($params [$section])) ? $paramsection = $params [$section] : $paramsection = 0;
                                        if ($this->getEnable($grenzeSection, $paramsection, $params, $precalcuse) == false) {
                                            $validp = false;
                                        }

                                    }
                                    if ($validp) {
                                        $papierdb[] = (string) $papier ['id'];
                                        $papiercontainer[(string) $papier ['id']] = (float) $papier ['value'];
                                    }
                                } else {
                                    $papierdb[] = (string) $papier ['id'];
                                    $papiercontainer[(string) $papier ['id']] = (float) $papier ['value'];
                                }
                            }
                        }

                        $papierdb = Doctrine_Query::create()->select()->from('Papier p')->andWhere('p.install_id = ?', array($view->view->shop->Install->id))->andWhereIn('p.art_nr', $papierdb)->orderBy('FIELD(art_nr, "' . implode('","', $papierdb) . '")')->execute();


                        foreach ($papierdb as $papierSingle) {

                            $valid = true;

                            if (count($druckarr) == 0) {
                                if (!isset($params [$key])) {
                                    $selected = (string) $papierSingle ['art_nr'];
                                } else {
                                    $selected = $params [$key];
                                }
                            }

                            if ((isset($params [$key]) && (string) $papierSingle ['art_nr'] == $params [$key]) || (!isset($params [$key]))) {

                                $papervalue = $papiercontainer[$params [$key]];

                                if ((string) $papierSingle->preis != '' && $papervalue != '') {
                                    $params [$key . '_value'] = (string) $papervalue + (string) $papierSingle->preis;
                                } elseif ((string) $papierSingle->preis != '') {
                                    $params [$key . '_value'] = (string) $papierSingle->preis;
                                } elseif ($papervalue != '') {
                                    $params [$key . '_value'] = $papervalue;
                                }
                                $params [$key . '_grammatur'] = (string) $papierSingle->grammatur;
                                $params [$key . '_papiertyp1'] = (string) $papierSingle->getPapierTyp1();
                                $params [$key . '_papiertyp2'] = (string) $papierSingle->getPapierTyp2();
                                $params [$key . '_papiertyp3'] = (string) $papierSingle->getPapierTyp3();
                                $params [$key . '_papiertyp4'] = (string) $papierSingle->getPapierTyp4();
                                $params [$key . '_papiertyp5'] = (string) $papierSingle->getPapierTyp5();
                                $params [$key . '_papiertyp6'] = (string) $papierSingle->getPapierTyp6();
                                $params [$key . '_papiertyp7'] = (string) $papierSingle->getPapierTyp7();
                                $params [$key . '_papiertyp8'] = (string) $papierSingle->getPapierTyp8();
                                $params [$key . '_papiertyp9'] = (string) $papierSingle->getPapierTyp9();
                                $params [$key . '_papiertyp10'] = (string) $papierSingle->getPapierTyp10();
                                $params [$key . '_papiertyp11'] = (string) $papierSingle->getPapierTyp11();
                                $params [$key . '_papiertyp12'] = (string) $papierSingle->getPapierTyp12();
                                $params [$key . '_papiertyp13'] = (string) $papierSingle->getPapierTyp13();
                                $params [$key . '_papiertyp14'] = (string) $papierSingle->getPapierTyp14();
                                $params [$key . '_papiertypes'] = $papierSingle->getPaperTypes();
                                $params [$key . '_umschlagen'] = (string) $papierSingle->getUmschlagen();
                                $params [$key . '_happy'] = (string) $papierSingle->getHappy();
                                $params [$key . '_eq'] = (string) $papierSingle->getEq();
                                $params [$key . '_sense'] = (string) $papierSingle->getSense();
                                $params [$key . '_sky'] = (string) $papierSingle->getSky();
                                $params [$key . '_glam'] = (string) $papierSingle->getGlam();
                                $params [$key . '_post'] = (string) $papierSingle->getPost();
                                $params [$key . '_volume'] = (string) $papierSingle->getVolume();

                                $params [$key . '_sammelform'] = (string) $papierSingle->getSammelform();

                                $view->getRequest()->setPost($key . '_grammatur', (string) $papierSingle->grammatur);
                                $params [$key . '_offset_fix'] = floatval($papierSingle->offset_fix);
                                $params [$key . '_offset_var'] = floatval($papierSingle->offset_var);
                                $params [$key . '_digital_fix'] = floatval($papierSingle->digital_fix);
                                $params [$key . '_digital_var'] = floatval($papierSingle->digital_var);
                                if (isset($params ['special'])) {
                                    $view->getRequest()->setPost($key, (string) $papierSingle->description_1);
                                }

                                $tempProdukt->addCalcReference($key, $papierSingle->asArray());
                            } else {

                            }

                            if ($valid == true || !$view->getRequest()->isPost()) {
                                array_push($druckarr, array('key' => (string) $papierSingle ['art_nr'], 'value' => (string) $papierSingle->description_1));
                                array_push($druckarrAjax, array('key' => (string) $papierSingle ['art_nr'], 'value' => (string) $papierSingle->description_1,'addData' =>  array('value' => (string) $papierSingle->description_1, 'description_1' => (string) $papierSingle->description_1, 'description_2' => (string) $papierSingle->description_2)));
                            } else {
                                $view->view->messages [$key] = (string) $papierSingle->description_1 . ' nicht mehr m&ouml;glich';
                            }
                        }

                        if (isset($art['grenzen'])) {
                            foreach ($art['grenzen'] as $section => $row) {

                                (isset($params [$section])) ? $paramsection = $params [$section] : $paramsection = 0;
                                if (isset($farbclone['calcs'][$section]) && $farbclone['calcs'][$section])
                                    $paramsection = $this->createFormel($farbclone['calcs'][$section], $params);

                                $preis [$key] [$section] [$i] = $this->getAmountLimit($row, $paramsection, 'preis', $params, $key . '_' . $section);
                                $paus [$key] [$section] [$i] = $this->getAmountLimit($row, $paramsection, 'pauschale', $params, $key . '_' . $section);

                                $this->calc_value [$key . '_' . $section] = $this->getAmountLimit($row, $paramsection, 'calc_value', $params, $key . '_' . $section);
                                $this->calc_value [$key . '_' . $section . '_1'] = $this->getAmountLimit($row, $paramsection, 'calc_value_1', $params, $key . '_' . $section . '_1');
                                $this->calc_value [$key . '_' . $section . '_2'] = $this->getAmountLimit($row, $paramsection, 'calc_value_2', $params, $key . '_' . $section . '_2');
                                $this->calc_value [$key . '_' . $section . '_3'] = $this->getAmountLimit($row, $paramsection, 'calc_value_3', $params, $key . '_' . $section . '_3');
                                $this->calc_value [$key . '_' . $section . '_4'] = $this->getAmountLimit($row, $paramsection, 'calc_value_4', $params, $key . '_' . $section . '_4');
                                $this->calc_value [$key . '_' . $section . '_5'] = $this->getAmountLimit($row, $paramsection, 'calc_value_5', $params, $key . '_' . $section . '_5');
                                $this->calc_value [$key . '_' . $section . '_6'] = $this->getAmountLimit($row, $paramsection, 'calc_value_6', $params, $key . '_' . $section . '_6');
                                $this->calc_value [$key . '_' . $section . '_7'] = $this->getAmountLimit($row, $paramsection, 'calc_value_7', $params, $key . '_' . $section . '_7');
                                $this->calc_value [$key . '_' . $section . '_8'] = $this->getAmountLimit($row, $paramsection, 'calc_value_8', $params, $key . '_' . $section . '_8');
                                $this->calc_value [$key . '_' . $section . '_9'] = $this->getAmountLimit($row, $paramsection, 'calc_value_9', $params, $key . '_' . $section . '_9');
                                $this->calc_value [$key . '_' . $section . '_10'] = $this->getAmountLimit($row, $paramsection, 'calc_value_10', $params, $key . '_' . $section . '_10');

                                if ($this->getAmountLimit($row, $paramsection, 'formel', $params, $key . '_' . $section)) {
                                    $formel [$key] [$section] [$i] = $this->getAmountLimit($row, $paramsection, 'formel', $params, $key . '_' . $section);
                                }

                                if ($this->getEnable($row, $paramsection, $params, null, $key . '_' . $section) == false) {
                                    $valid = false;
                                }

                                $i++;
                            }
                        }
                        if (isset($params ['special']) && $valid) {
                            if ($this->isAjax) {
                                $view->view->form[] = array('type' => 'input', 'label' => $art ['Name'], 'name' => $key, 'value' => $params [$key]);
                            } else {
                                $$key = $view->view->form->createElement('text', $key)->setLabel($art ['Name'])->setValue('0')->setRequired($art ['Require']);
                            }
                        } else {

                            if ($art ['hidden']) {
                                if($this->isAjax) {
                                    $view->view->form[] = array('type' => 'hidden', 'name' => $key, 'value' => $params [$key]);
                                }else{
                                    $view->view->form->addElement('hidden', $key, array('value' => $params [$key]));
                                    $view->view->form->$key->removeDecorator('label')->removeDecorator('HtmlTag');
                                }
                            } else {

                                if ($valid && (!empty($druckarr) && (!isset($art ['mode']) || (isset($art ['mode']) && $art ['mode'] != 'papierdb') || (isset($art ['mode']) && $art ['mode'] == 'papierdb' && $valid == true)))) {
                                    if ($this->isAjax) {
                                        $view->view->form[] = array('type' => 'select', 'name' => $key, 'value' => $params [$key], 'options' => $druckarrAjax, 'label' => $art ['Name'], 'onChange' => 'document.getElementById(\'reload\').value=\'1\';this.form.submit()');
                                    } else {
                                        $$key = $view->view->form->createElement('select', $key);

                                        if (isset($art ['help']) && $art ['help'] != "") {
                                            /* $$key->addDecorator('Help', array(
                                              'markup' => '<span class="imgwrapper">'.
                                              '<a href="#" class="show-tooltip" title="'.$art['help'].'"><img src="/images/admin/oxygen/16x16/actions/help-about.png" border="0" style="display:inline" alt="info icon"/></a>
                                              </span></dd>',
                                              'placement' => 'APPEND'
                                              ))
                                              ->addDecorator('HtmlTag', array('tag' => 'dd', 'openOnly' => true)); */

                                            $$key->setAttrib('title', $art ['help']);
                                        }
                                        if (isset($art ['helplink']) && $art ['helplink'] != "") {
                                            $helplink = explode('#', $art ['helplink']);
                                            if (!isset($helplink[1]))
                                                $helplink[1] = '';
                                            $$key->setAttrib('helplink', $helplink[0]);
                                            $$key->addDecorator('Help', array('markup' => '' . '<a href="' . $helplink[0] . '?help=1&width=700&height=450#' . $helplink[1] . '" class="calcinfo thickbox" title="Hilfelink zu ' . $art ['Name'] . '" target="_blank"><img src="/images/transparent.png" width="22" height="22"/></a>
                                                    </dd>', 'placement' => 'APPEND'))->addDecorator('HtmlTag', array('tag' => 'dd', 'openOnly' => true));
                                            //$$key->setAttrib('title', '<a onClick="window.open(this.href,\'Help\',\'width=300, height=300\'); return false;" href="'.$art['helplink'].'?help=1" title="Hilfelink zu '.$art['Name'].'" target="_blank">Hilfelink zu '.$art['Name'].'</a>');
                                        }

                                        $$key->setLabel($art ['Name'])->setMultiOptions($druckarr)->setValue($params [$key])->setAttrib('onchange', 'javascript:this.form.submit();')->setRequired($art ['Require']);
                                    }
                                } else {
                                    unset($view->view->messages [$key]);
                                    $view->getRequest()->setPost($key, '');
                                    $view->getRequest()->setParam($key, null);
                                }
                            }
                        }
                    } elseif (isset($art ['mode']) && $art ['mode'] == 'produktdb' && $view->view->article->a6_org_article == 0 && (!isset($view->view->layouterSession) || $view->view->layouterSession == "" )) {
                        $isActive = false;
                        $valid = true;
                        foreach (explode(',', $art ['container']) as $rowkey) {
                            $row = Doctrine_Query::create()->select()->from('Article a')->where('a.id = ?', array($rowkey))->fetchOne();
                            if ($row->url == $view->view->article->url && $isActive == false)
                                $isActive = true;
                            array_push($druckarr, array('key' => $row->url, 'value' => $row->title));
                            array_push($druckarrAjax, array('key' => $row->url, 'value' => $row->title));
                        }

                        if ($isActive) {

                            $view->getRequest()->setPost($key, $view->view->article->url);
                            $view->getRequest()->setParam($key, $view->view->article->url);
                            $params[$key] = $view->view->article->url;


                            if ($this->isAjax) {
                                if(!$view->getRequest()->getParam('loaded', false) && !$view->getRequest()->getParam('load', false)) {
                                    $view->view->form[] = array('type' => 'select', 'name' => $key, 'value' => $view->view->article->url, 'options' => $druckarrAjax, 'label' => $art ['Name'], 'onChange' => 'javascript:document.location.href="/article/show/" + $("#' . $key . '")[0].value;');
                                }
                            } else {

                                $$key = $view->view->form->createElement('select', $key);

                                if (isset($art ['help']) && $art ['help'] != "") {

                                    $$key->setAttrib('title', $art ['help']);
                                }
                                if (isset($art ['helplink']) && $art ['helplink'] != "") {
                                    $helplink = explode('#', $art ['helplink']);
                                    if (!isset($helplink[1]))
                                        $helplink[1] = '';
                                    $$key->setAttrib('helplink', $helplink[0]);
                                    $$key->addDecorator('Help', array('markup' => '' . '<a href="' . $helplink[0] . '?help=1&width=700&height=450#' . $helplink[1] . '" class="calcinfo thickbox" title="Hilfelink zu ' . $art ['Name'] . '" target="_blank"><img src="/images/transparent.png" width="22" height="22"/></a>
                                            </dd>', 'placement' => 'APPEND'))->addDecorator('HtmlTag', array('tag' => 'dd', 'openOnly' => true));
                                }
                                $$key->setLabel($art ['Name'])->setValue($view->view->article->url)->setMultiOptions($druckarr)->setAttrib('onchange', 'javascript:document.location.href="/article/show/" + $("#' . $key . '")[0].value;')->setRequired($art ['Require']);
                            }
                        } else {

                            $view->getRequest()->setPost($key, 'select');
                            $view->getRequest()->setParam($key, 'select');
                            $params[$key] = 'select';

                            if (isset($art ['standardtext'])) {
                                $druckarr = array_merge(array(array('key' => 'select', 'value' => $art ['standardtext'])), $druckarr);
                            } else {
                                $druckarr = array_merge(array(array('key' => 'select', 'value' => 'Bitte auswählen')), $druckarr);
                            }

                            if ($this->isAjax) {
                                if(!$view->getRequest()->getParam('loaded', false) && !$view->getRequest()->getParam('load', false)) {
                                    $view->view->form[] = array('type' => 'select', 'name' => $key, 'value' => 'select', 'options' => $druckarrAjax, 'label' => $art ['Name'], 'onChange' => 'javascript:document.location.href="/article/show/" + $("#' . $key . '")[0].value;');
                                }
                            } else {
                                $$key = $view->view->form->createElement('select', $key);

                                if (isset($art ['help']) && $art ['help'] != "") {

                                    $$key->setAttrib('title', $art ['help']);
                                }
                                if (isset($art ['helplink']) && $art ['helplink'] != "") {
                                    $helplink = explode('#', $art ['helplink']);
                                    if (!isset($helplink[1]))
                                        $helplink[1] = '';
                                    $$key->setAttrib('helplink', $helplink[0]);
                                    $$key->addDecorator('Help', array('markup' => '' . '<a href="' . $helplink[0] . '?help=1&width=700&height=450#' . $helplink[1] . '" class="calcinfo thickbox" title="Hilfelink zu ' . $art ['Name'] . '" target="_blank"><img src="/images/transparent.png" width="22" height="22"/></a>
                                            </dd>', 'placement' => 'APPEND'))->addDecorator('HtmlTag', array('tag' => 'dd', 'openOnly' => true));
                                }

                                $$key->setLabel($art ['Name'])->setValue('select')->setMultiOptions($druckarr)->setAttrib('onchange', 'javascript:document.location.href="/article/show/" + $("#' . $key . '")[0].value;')->setRequired($art ['Require']);
                            }
                        }
                        if($view->getRequest()->getParam('loaded', false) || $view->getRequest()->getParam('load', false)) {
                            $valid = false;
                        }
                    } elseif (!isset($art ['mode']) || (isset($art ['mode']) && $art ['mode'] != 'produktdb')) {

                        foreach ($art ['options'] as $farbe) {

                            if (count($druckarr) == 0) {
                                if (!isset($params [$key])) {
                                    $selected = $farbe ['id'];
                                } else {
                                    $selected = $params [$key];
                                }
                            }
                            $farbclone = $farbe;
                            if(strpos(strtolower($farbclone['name']), "stück")) {
                                $translate = Zend_Registry::get('Zend_Translate');
                                $farbclone['name'] = str_replace("Stück", $translate->translate("Stück"), $farbclone['name']);
                            }

                            $valid = true;
                            if ((isset($params [$key]) && $farbe ['id'] == $params [$key]) || (!isset($params [$key]) && $farbe ['default'] == 'true')) {

                                if (isset($params ['special'])) {
                                    $view->getRequest()->setPost($key, $farbe ['name']);
                                }

                                unset($farbe ['id']);
                                unset($farbe ['default']);
                                unset($farbe ['name']);
                                unset($farbe ['calcs']);

                                foreach ($farbe as $section => $row) {
                                    (isset($params [$section])) ? $paramsection = $params [$section] : $paramsection = 0;
                                    if (isset($farbclone['calcs'][$section]) && $farbclone['calcs'][$section])
                                        $paramsection = $this->createFormel($farbclone['calcs'][$section], $params);

                                    $preis [$key] [$section] [$i] = $this->getAmountLimit($row, $paramsection, 'preis', $params, $key . '_' . $section);
                                    $paus [$key] [$section] [$i] = $this->getAmountLimit($row, $paramsection, 'pauschale', $params, $key . '_' . $section);

                                    $this->calc_value [$key . '_' . $section] = $this->getAmountLimit($row, $paramsection, 'calc_value', $params, $key . '_' . $section);
                                    $this->calc_value [$key . '_' . $section . '_1'] = $this->getAmountLimit($row, $paramsection, 'calc_value_1', $params, $key . '_' . $section . '_1');
                                    $this->calc_value [$key . '_' . $section . '_2'] = $this->getAmountLimit($row, $paramsection, 'calc_value_2', $params, $key . '_' . $section . '_2');
                                    $this->calc_value [$key . '_' . $section . '_3'] = $this->getAmountLimit($row, $paramsection, 'calc_value_3', $params, $key . '_' . $section . '_3');
                                    $this->calc_value [$key . '_' . $section . '_4'] = $this->getAmountLimit($row, $paramsection, 'calc_value_4', $params, $key . '_' . $section . '_4');
                                    $this->calc_value [$key . '_' . $section . '_5'] = $this->getAmountLimit($row, $paramsection, 'calc_value_5', $params, $key . '_' . $section . '_5');
                                    $this->calc_value [$key . '_' . $section . '_6'] = $this->getAmountLimit($row, $paramsection, 'calc_value_6', $params, $key . '_' . $section . '_6');
                                    $this->calc_value [$key . '_' . $section . '_7'] = $this->getAmountLimit($row, $paramsection, 'calc_value_7', $params, $key . '_' . $section . '_7');
                                    $this->calc_value [$key . '_' . $section . '_8'] = $this->getAmountLimit($row, $paramsection, 'calc_value_8', $params, $key . '_' . $section . '_8');
                                    $this->calc_value [$key . '_' . $section . '_9'] = $this->getAmountLimit($row, $paramsection, 'calc_value_9', $params, $key . '_' . $section . '_9');
                                    $this->calc_value [$key . '_' . $section . '_10'] = $this->getAmountLimit($row, $paramsection, 'calc_value_10', $params, $key . '_' . $section . '_10');

                                    if ($this->getAmountLimit($row, $paramsection, 'formel', $params, $key . '_' . $section)) {
                                        $formel [$key] [$section] [$i] = $this->getAmountLimit($row, $paramsection, 'formel', $params, $key . '_' . $section);
                                    }

                                    if ($this->getEnable($row, $paramsection, $params, null, $key . '_' . $section) == false) {
                                        $valid = false;
                                    }
                                    $i++;
                                }
                                $this->calc_value [$key . '_label'] = $farbclone ['name'];
                            } else {
                                unset($farbe ['id']);
                                unset($farbe ['default']);
                                unset($farbe ['name']);
                                unset($farbe ['calcs']);

                                foreach ($farbe as $section => $row) {
                                    (isset($params [$section])) ? $paramsection = $params [$section] : $paramsection = 0;

                                    if (isset($farbclone['calcs'][$section]) && $farbclone['calcs'][$section])
                                        $paramsection = $this->createFormel($farbclone['calcs'][$section], $params);

                                    if ($this->getEnable($row, $paramsection, $params, null, $key . '_' . $section) == false) {
                                        $valid = false;
                                    }
                                    $i++;
                                }
                            }
                            if (isset($this->precalcuses[$key])) {
                                if ($this->getEnablePreCalc($this->precalcuses[$key], $farbclone ['id']) == false) {
                                    $valid = false;
                                }
                            }

                            if ($valid == true) {
                                array_push($druckarr, array('key' => $farbclone ['id'], 'value' => $farbclone ['name']));
                            } else {
                                $view->view->messages [$key] [] = $farbclone ['name'] . ' nicht mehr m&ouml;glich';
                            }
                        }
                        $valid = true;
                        if (isset($art['grenzen'])) {
                            foreach ($art['grenzen'] as $section => $row) {

                                (isset($params [$section])) ? $paramsection = $params [$section] : $paramsection = 0;
                                if (isset($farbclone['calcs'][$section]) && $farbclone['calcs'][$section])
                                    $paramsection = $this->createFormel($farbclone['calcs'][$section], $params);

                                if ($this->getEnable($row, $paramsection, $params, null, $key . '_' . $section) == false) {
                                    $valid = false;
                                }
                                $i++;
                            }
                        }

                        if (isset($params ['special'])) {
                            if ($this->isAjax) {
                                $view->view->form[] = array('type' => 'input', 'label' => $art ['Name'], 'name' => $key, 'value' => $params [$key]);
                            } else {
                                $$key = $view->view->form->createElement('text', $key)->setLabel($art ['Name'])->setValue('0')->setRequired($art ['Require']);
                            }
                        } else {

                            if ($art ['hidden']) {
                                if ($this->isAjax) {
                                    $view->view->form[] = array('type' => 'hidden', 'name' => $key, 'value' => $params [$key]);
                                } else {
                                    $view->view->form->addElement('hidden', $key, array('value' => $params [$key]));
                                }
                            } else {

                                if (!empty($druckarr) && ((!isset($art ['mode']) && $valid == true) || (isset($art ['mode']) && $art ['mode'] != 'papierdb' && $valid == true) || (isset($art ['mode']) && $art ['mode'] == 'papierdb' && $valid == true))) {
                                    if (!$this->isAjax) {
                                        $$key = $view->view->form->createElement('select', $key);

                                        if (isset($art ['help']) && $art ['help'] != "") {
                                            /* $$key->addDecorator('Help', array(
                                              'markup' => '<span class="imgwrapper">'.
                                              '<a href="#" class="show-tooltip" title="'.$art['help'].'"><img src="/images/admin/oxygen/16x16/actions/help-about.png" border="0" style="display:inline" alt="info icon"/></a>
                                              </span></dd>',
                                              'placement' => 'APPEND'
                                              ))
                                              ->addDecorator('HtmlTag', array('tag' => 'dd', 'openOnly' => true)); */

                                            $$key->setAttrib('title', $art ['help']);
                                        }
                                        if (isset($art ['helplink']) && $art ['helplink'] != "") {
                                            $helplink = explode('#', $art ['helplink']);
                                            if (!isset($helplink[1]))
                                                $helplink[1] = '';
                                            $$key->setAttrib('helplink', $art ['helplink']);
                                            $$key->addDecorator('Help', array('markup' => '' . '<a href="' . $helplink[0] . '?help=1&width=700&height=450#' . $helplink[1] . '" class="calcinfo thickbox" title="Hilfelink zu ' . $art ['Name'] . '" target="_blank"><img src="/images/transparent.png" width="22" height="22"/></a>
                                                    </dd>', 'placement' => 'APPEND'))->addDecorator('HtmlTag', array('tag' => 'dd', 'openOnly' => true));
                                            //$$key->setAttrib('title', '<a onClick="window.open(this.href,\'Help\',\'width=300, height=300\'); return false;" href="'.$art['helplink'].'?help=1" title="Hilfelink zu '.$art['Name'].'" target="_blank">Hilfelink zu '.$art['Name'].'</a>');
                                        }

                                        $$key->setLabel($art ['Name'])->setMultiOptions($druckarr)->setValue($params [$key])->setAttrib('onchange', 'javascript:this.form.submit();')->setRequired($art ['Require']);
                                    } else {
                                        if (isset($art ['helplink']) && $art ['helplink'] != "") {
                                            $helplink = explode('#', $art ['helplink']);
                                            if (!isset($helplink[1])) {
                                                $helplink[1] = '';
                                                if (isset($art ['help']) && $art ['help'] != "") {
                                                    $view->view->form[] = array('type' => 'select','helplink' => $helplink[0], 'help' => $art ['help'], 'name' => $key, 'value' => $params [$key], 'options' => $druckarr, 'label' => $art ['Name'], 'onChange' => 'this.form.submit()');
                                                }else{
                                                    $view->view->form[] = array('type' => 'select','helplink' => $helplink[0], 'name' => $key, 'value' => $params [$key], 'options' => $druckarr, 'label' => $art ['Name'], 'onChange' => 'this.form.submit()');
                                                }
                                            }else{
                                                if (isset($art ['help']) && $art ['help'] != "") {
                                                    $view->view->form[] = array('type' => 'select','helplink' => $helplink[0], 'ankerlink' => $helplink[1], 'help' => $art ['help'], 'name' => $key, 'value' => $params [$key], 'options' => $druckarr, 'label' => $art ['Name'], 'onChange' => 'this.form.submit()');
                                                }else{
                                                    $view->view->form[] = array('type' => 'select','helplink' => $helplink[0], 'ankerlink' => $helplink[1], 'name' => $key, 'value' => $params [$key], 'options' => $druckarr, 'label' => $art ['Name'], 'onChange' => 'this.form.submit()');
                                                }
                                            }
                                        }else {
                                            if (isset($art ['help']) && $art ['help'] != "") {
                                                $view->view->form[] = array('help' => $art ['help'], 'type' => 'select', 'name' => $key, 'value' => $params [$key], 'options' => $druckarr, 'label' => $art ['Name'], 'onChange' => 'this.form.submit()');
                                            }else{
                                                $view->view->form[] = array('type' => 'select', 'name' => $key, 'value' => $params [$key], 'options' => $druckarr, 'label' => $art ['Name'], 'onChange' => 'this.form.submit()');
                                            }
                                        }
                                    }
                                } else {
                                    unset($view->view->messages [$key]);
                                    $view->getRequest()->setPost($key, '');
                                    $view->getRequest()->setParam($key, null);
                                }
                            }
                        }
                    }

                    if (isset($art['layouterwidth']) || isset($art['layouterheight'])) {

                        $basket = new TP_Basket();
                        $tempProdukt = $basket->getTempProduct();


                        if (isset($art['layouterwidth'])) {
                            $tempProdukt->setLayouterWidth($params [$key]);

                            $tempProdukt->setLayouterWidthEn($art['layouterwidth']);
                        }

                        if (isset($art['layouterheight'])) {
                            $tempProdukt->setLayouterHeight($params [$key]);

                            $tempProdukt->setLayouterHeightEn($art['layouterheight']);
                        }
                    }

                    if ($valid && ($key != 'format' || ($key == 'format' && $tempProdukt->getUploadMode() == 'weblayouter'))) {
                        if (!$this->isAjax && isset($$key)) {
                            $view->view->form->addElements(array($$key));
                        }
                    }

                    //$context->form->addElement('select', $$key, $art['Name'], $druckarr);
                    break;
                case 'File' :

                    if (isset($_SESSION [$key])) {
                        $params [$key] = $_SESSION [$key];
                        $view->getRequest()->setParams($params);
                    }

                    $$key = $view->view->form->createElement('file', $key)->setDestination('uploads/' . $view->getShop()->uid . '/article/')->addValidator('Extension', false, 'jpg,png,gif,jpeg,tiff')->setOptions(array('size' => 20))->setLabel($art ['Name']);
                    $view->view->form->setAttrib('enctype', 'multipart/form-data');
                    $view->view->form->addElements(array($$key));

                    if ($$key->receive() && $$key->isUploaded()) {

                        $_SESSION [$key] = 'uploads/' . $view->getShop()->uid . '/article/' . $$key->getFileName($key, false);
                        $params [$key] = 'uploads/' . $view->getShop()->uid . '/article/' . $$key->getFileName($key, false);
                    }
                    if (isset($art ['showx']) && $art ['showx'] != false) {
                        $showx = $view->view->form->createElement('text', 'showx' . $key)->setLabel('X')->setOptions(array('size' => 3))->setValue((isset($params ['showx' . $key])) ? $params ['showx' . $key] : $art ['showx']);

                        $view->view->form->addElements(array($showx));

                        (isset($params ['showx' . $key])) ? $activeartikel ['options'] [$key] ['showx' . $key] = $params ['showx' . $key] : 0;
                    }

                    if (isset($art ['showy']) && $art ['showy'] != false) {
                        $showy = $view->view->form->createElement('text', 'showy' . $key)->setLabel('Y')->setOptions(array('size' => 3))->setValue((isset($params ['showy' . $key])) ? $params ['showy' . $key] : $art ['showy']);

                        $view->view->form->addElements(array($showy));

                        (isset($params ['showy' . $key])) ? $activeartikel ['options'] [$key] ['showy' . $key] = $params ['showy' . $key] : 0;
                    }

                    if (isset($art ['showpercent']) && $art ['showpercent'] != false) {
                        $showpercent = $view->view->form->createElement('text', 'showpercent' . $key)->setLabel('%')->addValidator('GreaterThan', false, - 1)->addValidator('LessThan', false, 301)->setOptions(array('size' => 3))->setValue((isset($params ['showpercent' . $key])) ? $params ['showpercent' . $key] : $art ['showpercent']);

                        $view->view->form->addElements(array($showpercent));

                        (isset($params ['showpercent' . $key])) ? $activeartikel ['options'] [$key] ['showpercent' . $key] = $params ['showpercent' . $key] : 0;
                    }

                    if (isset($art ['showpercent']) && isset($art ['showx']) && isset($art ['showy']) && $art ['showx'] != false && $art ['showy'] != false && $art ['showpercent'] != false) {
                        $view->view->form->addDisplayGroup(array($key, 'showx' . $key, 'showy' . $key, 'showpercent' . $key), $key . 'group', array('legend' => $art ['Name']));
                        $view->view->form->$key->setLabel('');
                    }

                    $activeartikel ['options'] [$key] ['id'] = $key;
                    (isset($params [$key])) ? $activeartikel ['options'] [$key] ['value'] = $params [$key] : 0;

                    break;
                case 'Text' :
                    $valid = true;
                    if (isset($art['grenzen'])) {
                        foreach ($art['grenzen'] as $section => $row) {

                            (isset($params [$section])) ? $paramsection = $params [$section] : $paramsection = 0;
                            if (isset($farbclone['calcs'][$section]) && $farbclone['calcs'][$section])
                                $paramsection = $this->createFormel($farbclone['calcs'][$section], $params);

                            if ($this->getEnable($row, $paramsection, $params, null, $key . '_' . $section) == false) {
                                $valid = false;
                            }
                            $i++;
                        }
                    }
                    if ($valid) {
                        if (!$art ['hidden']) {
                            if ($this->isAjax) {
                                $view->view->form[] = array('type' => 'text', 'label' => $art ['Name'], 'name' => $key, 'value' => $params [$key]);
                            } else {
                                $element = new TP_Form_Element_Textdisplay($key);
                                $element->setLabel($art ['Name']);
                                $element->setValue((isset($params [$key])) ? $params [$key] : '');
                                $view->view->form->addElement($element);
                            }
                        }
                    }else{
                        unset($view->view->messages [$key]);
                        if(isset($_POST[$key])) {
                            unset($_POST[$key]);
                        }
                        $view->getRequest()->setParam($key, null);
                    }
                    break;
                case 'Checkbox' :
                    if (!$this->isAjax) {
                        $element = new Zend_Form_Element_MultiCheckbox($key);
                        $element->setLabel($art ['Name']);
                    }
                    foreach ($art ['options'] as $farbe) {
                        $valid = true;
                        $farbclone = $farbe;
                        if ((isset($params [$key]) && is_array($params [$key]) && in_array($farbe ['id'], $params [$key])) || (!isset($params [$key]) && $farbe ['default'] == 'true')) {

                            unset($farbe ['id']);
                            unset($farbe ['default']);
                            unset($farbe ['name']);
                            unset($farbe ['calcs']);

                            foreach ($farbe as $section => $row) {

                                (isset($params [$section])) ? $paramsection = $params [$section] : $paramsection = 0;
                                if ($farbclone['calcs'][$section])
                                    $paramsection = $this->createFormel($farbclone['calcs'][$section], $params);


                                $preis [$key] [$section] [$i] = $this->getAmountLimit($row, $paramsection, 'preis', $params, $key . '_' . $section);
                                $paus [$key] [$section] [$i] = $this->getAmountLimit($row, $paramsection, 'pauschale', $params, $key . '_' . $section);

                                $this->calc_value [$key . '_' . $section] = $this->getAmountLimit($row, $paramsection, 'calc_value', $params, $key . '_' . $section);
                                $this->calc_value [$key . '_' . $section . '_1'] = $this->getAmountLimit($row, $paramsection, 'calc_value_1', $params, $key . '_' . $section . '_1');
                                $this->calc_value [$key . '_' . $section . '_2'] = $this->getAmountLimit($row, $paramsection, 'calc_value_2', $params, $key . '_' . $section . '_2');
                                $this->calc_value [$key . '_' . $section . '_3'] = $this->getAmountLimit($row, $paramsection, 'calc_value_3', $params, $key . '_' . $section . '_3');
                                $this->calc_value [$key . '_' . $section . '_4'] = $this->getAmountLimit($row, $paramsection, 'calc_value_4', $params, $key . '_' . $section . '_4');
                                $this->calc_value [$key . '_' . $section . '_5'] = $this->getAmountLimit($row, $paramsection, 'calc_value_5', $params, $key . '_' . $section . '_5');
                                $this->calc_value [$key . '_' . $section . '_6'] = $this->getAmountLimit($row, $paramsection, 'calc_value_6', $params, $key . '_' . $section . '_6');
                                $this->calc_value [$key . '_' . $section . '_7'] = $this->getAmountLimit($row, $paramsection, 'calc_value_7', $params, $key . '_' . $section . '_7');
                                $this->calc_value [$key . '_' . $section . '_8'] = $this->getAmountLimit($row, $paramsection, 'calc_value_8', $params, $key . '_' . $section . '_8');
                                $this->calc_value [$key . '_' . $section . '_9'] = $this->getAmountLimit($row, $paramsection, 'calc_value_9', $params, $key . '_' . $section . '_9');
                                $this->calc_value [$key . '_' . $section . '_10'] = $this->getAmountLimit($row, $paramsection, 'calc_value_10', $params, $key . '_' . $section . '_10');

                                if ($this->getAmountLimit($row, $paramsection, 'formel', $params, $key . '_' . $section)) {
                                    $formel [$key] [$section] [$i] = $this->getAmountLimit($row, $paramsection, 'formel', $params, $key . '_' . $section);
                                }

                                if ($this->getEnable($row, $paramsection, $params, null, $key . '_' . $section) == false) {
                                    $valid = false;
                                }
                                $i++;
                            }
                        } else {
                            unset($farbe ['id']);
                            unset($farbe ['default']);
                            unset($farbe ['name']);
                            unset($farbe ['calcs']);

                            foreach ($farbe as $section => $row) {

                                (isset($params [$section])) ? $paramsection = $params [$section] : $paramsection = 0;
                                if ($farbclone['calcs'][$section])
                                    $paramsection = $this->createFormel($farbclone['calcs'][$section], $params);

                                if ($this->getEnable($row, $paramsection, $params, null, $key . '_' . $section) == false) {
                                    $valid = false;
                                }
                                $i++;
                            }
                        }

                        if (isset($art['grenzen'])) {
                            foreach ($art['grenzen'] as $section => $row) {

                                (isset($params [$section])) ? $paramsection = $params [$section] : $paramsection = 0;
                                if (isset($farbclone['calcs'][$section]) && $farbclone['calcs'][$section])
                                    $paramsection = $this->createFormel($farbclone['calcs'][$section], $params);

                                if ($this->getEnable($row, $paramsection, $params, null, $key . '_' . $section) == false) {
                                    $valid = false;
                                }
                                $i++;
                            }
                        }

                        if ($valid == true) {
                            array_push($druckarr, array('key' => $farbclone ['id'], 'value' => $farbclone ['name']));
                        } else {
                            $view->view->messages [$key] [] = $farbclone ['name'] . ' nicht mehr m&ouml;glich';
                        }
                    }
                    if (!$this->isAjax) {
                        $element->addMultiOptions($druckarr);
                        $element->setValue((isset($params [$key])) ? $params [$key] : '');
                        $element->setAttrib('onchange', 'javascript:this.form.submit();');
                        $view->view->form->addElement($element);
                    } elseif (count($druckarr) > 0) {
                        $view->view->form[] = array('type' => 'checkbox', 'options' => $druckarr, 'value' => $params [$key], 'label' => $art ['Name'], 'name' => $key);
                    }
                    break;
                case 'Radio' :
                    if ($view->view->article->a6_org_article != 0 && isset($art ['mode']) && $art ['mode'] == 'produktdb') {
                        continue;
                    }

                    if (!$this->isAjax) {
                        $element = new Zend_Form_Element_Radio($key);
                        $element->setLabel($art ['Name']);
                    }
                    if (isset($art ['mode']) && $art ['mode'] == 'produktdb') {


                        foreach (explode(',', $art ['container']) as $rowkey) {
                            $row = Doctrine_Query::create()->select()->from('Article a')->where('a.id = ?', array($rowkey))->fetchOne();
                            array_push($druckarr, array('key' => $row->url, 'value' => $row->title));
                        }

                        $view->getRequest()->setPost($key, $view->view->article->url);
                        $view->getRequest()->setParam($key, $view->view->article->url);
                        $params[$key] = $view->view->article->url;
                        if ($this->isAjax) {
                            if (count($druckarr) > 0)
                                $view->view->form[] = array('type' => 'radio', 'options' => $druckarr, 'value' => $params [$key], 'label' => $art ['Name'], 'name' => $key, 'onchange' => "javascript:document.location.href='/article/show/' + $(this).val();");
                        }else {
                            $element->setValue($view->view->article->url)->setMultiOptions($druckarr)->setAttrib('onchange', "javascript:document.location.href='/article/show/' + $(this).val();")->setRequired($art ['Require']);
                        }
                    } else {

                        foreach ($art ['options'] as $farbe) {
                            $valid = true;
                            $farbclone = $farbe;
                            if ((isset($params [$key]) && $farbe ['id'] == $params [$key]) || (!isset($params [$key]) && $farbe ['default'] == 'true')) {

                                unset($farbe ['id']);
                                unset($farbe ['default']);
                                unset($farbe ['name']);
                                unset($farbe ['calcs']);

                                foreach ($farbe as $section => $row) {

                                    (isset($params [$section])) ? $paramsection = $params [$section] : $paramsection = 0;
                                    if (isset($farbclone['calcs'][$section]) && $farbclone['calcs'][$section])
                                        $paramsection = $this->createFormel($farbclone['calcs'][$section], $params);

                                    $preis [$key] [$section] [$i] = $this->getAmountLimit($row, $paramsection, 'preis', $params, $key . '_' . $section);
                                    $paus [$key] [$section] [$i] = $this->getAmountLimit($row, $paramsection, 'pauschale', $params, $key . '_' . $section);

                                    $this->calc_value [$key . '_' . $section] = $this->getAmountLimit($row, $paramsection, 'calc_value', $params, $key . '_' . $section);
                                    $this->calc_value [$key . '_' . $section . '_1'] = $this->getAmountLimit($row, $paramsection, 'calc_value_1', $params, $key . '_' . $section . '_1');
                                    $this->calc_value [$key . '_' . $section . '_2'] = $this->getAmountLimit($row, $paramsection, 'calc_value_2', $params, $key . '_' . $section . '_2');
                                    $this->calc_value [$key . '_' . $section . '_3'] = $this->getAmountLimit($row, $paramsection, 'calc_value_3', $params, $key . '_' . $section . '_3');
                                    $this->calc_value [$key . '_' . $section . '_4'] = $this->getAmountLimit($row, $paramsection, 'calc_value_4', $params, $key . '_' . $section . '_4');
                                    $this->calc_value [$key . '_' . $section . '_5'] = $this->getAmountLimit($row, $paramsection, 'calc_value_5', $params, $key . '_' . $section . '_5');
                                    $this->calc_value [$key . '_' . $section . '_6'] = $this->getAmountLimit($row, $paramsection, 'calc_value_6', $params, $key . '_' . $section . '_6');
                                    $this->calc_value [$key . '_' . $section . '_7'] = $this->getAmountLimit($row, $paramsection, 'calc_value_7', $params, $key . '_' . $section . '_7');
                                    $this->calc_value [$key . '_' . $section . '_8'] = $this->getAmountLimit($row, $paramsection, 'calc_value_8', $params, $key . '_' . $section . '_8');
                                    $this->calc_value [$key . '_' . $section . '_9'] = $this->getAmountLimit($row, $paramsection, 'calc_value_9', $params, $key . '_' . $section . '_9');
                                    $this->calc_value [$key . '_' . $section . '_10'] = $this->getAmountLimit($row, $paramsection, 'calc_value_10', $params, $key . '_' . $section . '_10');

                                    if ($this->getAmountLimit($row, $paramsection, 'formel', $params, $key . '_' . $section)) {
                                        $formel [$key] [$section] [$i] = $this->getAmountLimit($row, $paramsection, 'formel', $params, $key . '_' . $section);
                                    }

                                    if ($this->getEnable($row, $paramsection, $params, null, $key . '_' . $section) == false) {
                                        $valid = false;
                                    }
                                    $i++;
                                }
                            } else {
                                unset($farbe ['id']);
                                unset($farbe ['default']);
                                unset($farbe ['name']);
                                unset($farbe ['calcs']);

                                foreach ($farbe as $section => $row) {
                                    (isset($params [$section])) ? $paramsection = $params [$section] : $paramsection = 0;
                                    if ($farbclone['calcs'][$section])
                                        $paramsection = $this->createFormel($farbclone['calcs'][$section], $params);

                                    if ($this->getEnable($row, $paramsection, $params, null, $key . '_' . $section) == false) {
                                        $valid = false;
                                    }
                                    $i++;
                                }
                            }

                            if ($valid == true) {
                                array_push($druckarr, array('key' => $farbclone ['id'], 'value' => $farbclone ['name']));
                            } else {
                                $view->view->messages [$key] [] = $farbclone ['name'] . ' nicht mehr m&ouml;glich';
                            }
                        }
                        if (!$this->isAjax) {
                            $element->addMultiOptions($druckarr);
                            $element->setValue((isset($params [$key])) ? $params [$key] : '');
                            $element->setAttrib('onchange', 'javascript:this.form.submit();');
                        } elseif (count($druckarr) > 0) {
                            $view->view->form[] = array('type' => 'radio', 'options' => $druckarr, 'value' => $params [$key], 'label' => $art ['Name'], 'name' => $key);
                        }
                    }
                    if (!$this->isAjax && count($druckarr) > 0) {
                        $view->view->form->addElement($element);
                    }
                    break;
                case 'Input' :
                    if ($key == 'static') {
                        $singlePreis = $singlePreis + str_replace(',', '.', (isset($params [$key])) ? $params ['static'] : 0);
                    }
                    $params [$key] = str_replace(",", ".", $params [$key]);
                    if (!$this->isAjax) {
                        $$key = $view->view->form->createElement('text', $key)->setLabel($art ['Name'])->setValue((isset($params [$key])) ? $params [$key] : '0')->setRequired($art ['Require']);
                    }
                    if (isset($art ['min']) && isset($art ['max'])) {
                        if (!$this->isAjax) {
                            $$key->addValidator('Between', false, array('min' => $this->createFormel($art ['min'], $params), 'max' => $this->createFormel($art ['max'], $params)));
                        }
                    }
                    
                    if (isset($art ['help']) && $art ['help'] != "") {
                        if (!$this->isAjax) {
                            $$key->setAttrib('title', $art ['help']);
                        }
                    }

                    if (isset($art ['helplink']) && $art ['helplink'] != "") {
                        if (!$this->isAjax) {
                            $helplink = explode('#', $art ['helplink']);
                            if (!isset($helplink[1]))
                                $helplink[1] = '';
                            $$key->setAttrib('helplink', $helplink[0]);
                            $$key->addDecorator('Help', array('markup' => '' . '<a href="' . $helplink[0] . '?help=1&width=700&height=450#' . $helplink[1] . '" class="calcinfo thickbox" title="Hilfelink zu ' . $art ['Name'] . '" target="_blank"><img src="/images/transparent.png" width="22" height="22"/></a>
                                                    </dd>', 'placement' => 'APPEND'))->addDecorator('HtmlTag', array('tag' => 'dd', 'openOnly' => true));
                        }
                    }

                    (isset($params [$key])) ? $activeartikel ['options'] [$key] ['value'] = $params [$key] : 0;
                    $valid = true;
                    if (isset($art ['options'] ['input']) && is_array($art ['options'] ['input'])) {
                        foreach ($art ['options'] ['input'] as $section => $row) {

                            (isset($params [$section])) ? $paramsection = $params [$section] : $paramsection = 0;
                            if (isset($art ['options']['calcs'][$section]) && $art ['options']['calcs'][$section])
                                $paramsection = $this->createFormel($art ['options']['calcs'][$section], $params);


                            $preis [$key] [$section] [$i] = $this->getAmountLimit($row, $paramsection, 'preis', $params, $key . '_' . $section);
                            $paus [$key] [$section] [$i] = $this->getAmountLimit($row, $paramsection, 'pauschale', $params, $key . '_' . $section);

                            $this->calc_value [$key . '_' . $section] = $this->getAmountLimit($row, $paramsection, 'calc_value', $params, $key . '_' . $section);
                            $this->calc_value [$key . '_' . $section . '_1'] = $this->getAmountLimit($row, $paramsection, 'calc_value_1', $params, $key . '_' . $section . '_1');
                            $this->calc_value [$key . '_' . $section . '_2'] = $this->getAmountLimit($row, $paramsection, 'calc_value_2', $params, $key . '_' . $section . '_2');
                            $this->calc_value [$key . '_' . $section . '_3'] = $this->getAmountLimit($row, $paramsection, 'calc_value_3', $params, $key . '_' . $section . '_3');
                            $this->calc_value [$key . '_' . $section . '_4'] = $this->getAmountLimit($row, $paramsection, 'calc_value_4', $params, $key . '_' . $section . '_4');
                            $this->calc_value [$key . '_' . $section . '_5'] = $this->getAmountLimit($row, $paramsection, 'calc_value_5', $params, $key . '_' . $section . '_5');
                            $this->calc_value [$key . '_' . $section . '_6'] = $this->getAmountLimit($row, $paramsection, 'calc_value_6', $params, $key . '_' . $section . '_6');
                            $this->calc_value [$key . '_' . $section . '_7'] = $this->getAmountLimit($row, $paramsection, 'calc_value_7', $params, $key . '_' . $section . '_7');
                            $this->calc_value [$key . '_' . $section . '_8'] = $this->getAmountLimit($row, $paramsection, 'calc_value_8', $params, $key . '_' . $section . '_8');
                            $this->calc_value [$key . '_' . $section . '_9'] = $this->getAmountLimit($row, $paramsection, 'calc_value_9', $params, $key . '_' . $section . '_9');
                            $this->calc_value [$key . '_' . $section . '_10'] = $this->getAmountLimit($row, $paramsection, 'calc_value_10', $params, $key . '_' . $section . '_10');

                            if ($this->getAmountLimit($row, $paramsection, 'formel', $params, $key . '_' . $section)) {
                                $formel [$key] [$section] [$i] = $this->getAmountLimit($row, $paramsection, 'formel', $params, $key . '_' . $section);
                            }
                            if ($this->getEnable($row, $paramsection, $params, null, $key . '_' . $section) == false) {
                                $valid = false;
                            }

                            $i++;
                        }
                    }
                    if ($valid) {
                        if ($this->isAjax) {
                        	$validInput = true;
                            if (isset($art ['min']) && isset($art ['max'])) {
                                if ($params [$key] < $this->createFormel($art ['min'], $params) || $params [$key] > $this->createFormel($art ['max'], $params)) {
                                    $view->view->form[] = array('type' => 'input', 'label' => $art ['Name'], 'name' => $key, 'value' => $params [$key], 'error' => 'between', 'min' => $this->createFormel($art ['min'], $params), 'max' => $this->createFormel($art ['max'], $params));
                                	$validInput = false;
                                }
                            }
                            if(isset($art ['validate']) && $validInput) {
                            	if($art ['validate'] == 'checkRest' && ($params [$key] % 2) != 0) {
                            		$view->view->form[] = array('type' => 'input', 'label' => $art ['Name'], 'name' => $key, 'value' => $params [$key], 'error' => 'checkRest', 'wert1' => $params ['auflage'], 'wert2' => $params ['fo']);
                            		$validInput = false;
                            	}
                            }
                            if($art ['Require'] && $validInput) {
                                if($params [$key] == "") {
                                    $view->view->form[] = array('type' => 'input', 'label' => $art ['Name'], 'name' => $key, 'value' => $params [$key], 'error' => 'require');
                                    $validInput = false;
                                }
                            }
                            if($validInput) {
                                if (isset($art ['helplink']) && $art ['helplink'] != "") {
                                    $helplink = explode('#', $art ['helplink']);
	                                $view->view->form[] = array('type' => 'input', 'label' => $art ['Name'], 'name' => $key, 'value' => $params [$key], 'error' => false, 'helplink' => $helplink[0]);
                                }else{
                                    $view->view->form[] = array('type' => 'input', 'label' => $art ['Name'], 'name' => $key, 'value' => $params [$key], 'error' => false);
                                }
                            }
                        } else {
                            $view->view->form->addElements(array($$key));
                        }
                    	
                    }else{
                        unset($view->view->messages [$key]);
                        if(isset($_POST[$key])) {
                            unset($_POST[$key]);
                        }
                        $view->getRequest()->setParam($key, null);
                    }

                    if (isset($art ['showx']) && $art ['showx'] != false) {
                        $showx = $view->view->form->createElement('text', 'showx' . $key)->setLabel('X')->setOptions(array('size' => 3))->setValue((isset($params ['showx' . $key])) ? $params ['showx' . $key] : $art ['showx']);

                        $view->view->form->addElements(array($showx));

                        (isset($params ['showx' . $key])) ? $activeartikel ['options'] [$key] ['showx' . $key] = $params ['showx' . $key] : $activeartikel ['options'] [$key] ['showx' . $key] = $art ['showx'];
                    }

                    if (isset($art ['showy']) && $art ['showy'] != false) {
                        $showy = $view->view->form->createElement('text', 'showy' . $key)->setLabel('Y')->setOptions(array('size' => 3))->setValue((isset($params ['showy' . $key])) ? $params ['showy' . $key] : $art ['showy']);

                        $view->view->form->addElements(array($showy));

                        (isset($params ['showy' . $key])) ? $activeartikel ['options'] [$key] ['showy' . $key] = $params ['showy' . $key] : $activeartikel ['options'] [$key] ['showy' . $key] = $art ['showy'];
                    }

                    if (isset($art ['showpercent']) && $art ['showpercent'] != false) {
                        $showpercent = $view->view->form->createElement('text', 'showpercent' . $key)->setLabel('pt')->addValidator('GreaterThan', false, - 1)->addValidator('LessThan', false, 101)->setOptions(array('size' => 3))->setValue((isset($params ['showpercent' . $key])) ? $params ['showpercent' . $key] : $art ['showpercent']);

                        $view->view->form->addElements(array($showpercent));

                        (isset($params ['showpercent' . $key])) ? $activeartikel ['options'] [$key] ['showpercent' . $key] = $params ['showpercent' . $key] : $activeartikel ['options'] [$key] ['showpercent' . $key] = $art ['showpercent'];
                    }

                    if (isset($art ['showcolor']) && $art ['showcolor'] != false) {

                        $temp = explode(',', $art ['showcolor']);
                        $temp1 = array();
                        foreach ($temp as $row) {
                            $row = explode(':', $row);
                            $temp1 [$row [1]] = $row [0];
                        }

                        $showcolor = $view->view->form->createElement('select', 'showcolor' . $key)->setLabel('')->addMultiOptions($temp1)->setAttrib('onchange', 'javascript:this.form.submit();')->setValue((isset($params ['showcolor' . $key])) ? $params ['showcolor' . $key] : $row [1]);

                        $view->view->form->addElements(array($showcolor));

                        (isset($params ['showcolor' . $key])) ? $activeartikel ['options'] [$key] ['showcolor' . $key] = $params ['showcolor' . $key] : $activeartikel ['options'] [$key] ['showcolor' . $key] = $row [1];
                    }
                    if (isset($art ['showfont']) && $art ['showfont'] != false) {

                        $temp = explode(',', $art ['showfont']);
                        $temp1 = array();
                        foreach ($temp as $row) {
                            $row = explode(':', $row);
                            $temp1 [$row [1]] = $row [0];
                        }

                        $showfont = $view->view->form->createElement('select', 'showfont' . $key)->setLabel('')->addMultiOptions($temp1)->setAttrib('onchange', 'javascript:this.form.submit();')->setValue((isset($params ['showfont' . $key])) ? $params ['showfont' . $key] : $row [1]);

                        $view->view->form->addElements(array($showfont));

                        (isset($params ['showfont' . $key])) ? $activeartikel ['options'] [$key] ['showfont' . $key] = $params ['showfont' . $key] : $activeartikel ['options'] [$key] ['showfont' . $key] = $row [1];
                    }

                    if (isset($art ['showpercent']) && isset($art ['showx']) && isset($art ['showy']) && $art ['showy'] != false && $art ['showpercent'] != false && $art ['showx'] != false) {
                        $view->view->form->addDisplayGroup(array($key, 'showx' . $key, 'showy' . $key, 'showpercent' . $key, 'showcolor' . $key, 'showfont' . $key), $key . 'group', array('legend' => $art ['Name']));
                        $view->view->form->$key->setLabel('');
                    }

                    $activeartikel ['options'] [$key] ['id'] = $key;

                    if (isset($art['layouterwidth']) || isset($art['layouterheight'])) {

                        $basket = new TP_Basket();
                        $tempProdukt = $basket->getTempProduct();


                        if (isset($art['layouterwidth'])) {
                            $tempProdukt->setLayouterWidth($params [$key]);

                            $tempProdukt->setLayouterWidthEn($art['layouterwidth']);
                        }

                        if (isset($art['layouterheight'])) {
                            $tempProdukt->setLayouterHeight($params [$key]);

                            $tempProdukt->setLayouterHeightEn($art['layouterheight']);
                        }
                    }

                    break;
                case 'Textarea' :
                case 'TextArea' :
                    $valid = true;
                    if (isset($art ['options'] ['input']) && is_array($art ['options'] ['input'])) {
                        foreach ($art ['options'] ['input'] as $section => $row) {

                            (isset($params [$section])) ? $paramsection = $params [$section] : $paramsection = 0;
                            if (isset($art ['options']['calcs'][$section]) && $art ['options']['calcs'][$section])
                                $paramsection = $this->createFormel($art ['options']['calcs'][$section], $params);


                            $preis [$key] [$section] [$i] = $this->getAmountLimit($row, $paramsection, 'preis', $params, $key . '_' . $section);
                            $paus [$key] [$section] [$i] = $this->getAmountLimit($row, $paramsection, 'pauschale', $params, $key . '_' . $section);

                            $this->calc_value [$key . '_' . $section] = $this->getAmountLimit($row, $paramsection, 'calc_value', $params, $key . '_' . $section);
                            $this->calc_value [$key . '_' . $section . '_1'] = $this->getAmountLimit($row, $paramsection, 'calc_value_1', $params, $key . '_' . $section . '_1');
                            $this->calc_value [$key . '_' . $section . '_2'] = $this->getAmountLimit($row, $paramsection, 'calc_value_2', $params, $key . '_' . $section . '_2');
                            $this->calc_value [$key . '_' . $section . '_3'] = $this->getAmountLimit($row, $paramsection, 'calc_value_3', $params, $key . '_' . $section . '_3');
                            $this->calc_value [$key . '_' . $section . '_4'] = $this->getAmountLimit($row, $paramsection, 'calc_value_4', $params, $key . '_' . $section . '_4');
                            $this->calc_value [$key . '_' . $section . '_5'] = $this->getAmountLimit($row, $paramsection, 'calc_value_5', $params, $key . '_' . $section . '_5');
                            $this->calc_value [$key . '_' . $section . '_6'] = $this->getAmountLimit($row, $paramsection, 'calc_value_6', $params, $key . '_' . $section . '_6');
                            $this->calc_value [$key . '_' . $section . '_7'] = $this->getAmountLimit($row, $paramsection, 'calc_value_7', $params, $key . '_' . $section . '_7');
                            $this->calc_value [$key . '_' . $section . '_8'] = $this->getAmountLimit($row, $paramsection, 'calc_value_8', $params, $key . '_' . $section . '_8');
                            $this->calc_value [$key . '_' . $section . '_9'] = $this->getAmountLimit($row, $paramsection, 'calc_value_9', $params, $key . '_' . $section . '_9');
                            $this->calc_value [$key . '_' . $section . '_10'] = $this->getAmountLimit($row, $paramsection, 'calc_value_10', $params, $key . '_' . $section . '_10');

                            if ($this->getAmountLimit($row, $paramsection, 'formel', $params, $key . '_' . $section)) {
                                $formel [$key] [$section] [$i] = $this->getAmountLimit($row, $paramsection, 'formel', $params, $key . '_' . $section);
                            }
                            if ($this->getEnable($row, $paramsection, $params, null, $key . '_' . $section) == false) {
                                $valid = false;
                            }

                            $i++;
                        }
                    }
                    if($valid) {
                        if ($this->isAjax) {
                            $view->view->form[] = array('type' => 'textarea', 'label' => $art ['Name'], 'cols' => $art ['Width'], 'rows' => $art ['Height'], 'name' => $key, 'value' => $params [$key]);
                        } else {
                            $$key = $view->view->form->createElement('textarea', $key)->setLabel($art ['Name'])->setOptions(array('cols' => $art ['Width'], 'rows' => $art ['Height']))->setValue($params [$key])->setRequired($art ['Require']);
                            if (isset($art ['help']) && $art ['help'] != "") {
                                $$key->addDecorator('Help', array('markup' => '<span class="imgwrapper">' . '<a href="#" class="show-tooltip" title="' . $art ['help'] . '"><img src="/images/admin/oxygen/16x16/actions/help-about.png" style="display:inline" border="0" alt="info icon"/></a>
                                                        </span></dd>', 'placement' => 'PREPEND'))->addDecorator('HtmlTag', array('tag' => 'dd', 'openOnly' => true));
                            }
                            if (isset($art ['helplink']) && $art ['helplink'] != "") {
                                $$key->addDecorator('Help', array('markup' => '' . '<a href="' . $art ['helplink'] . '?help=1&width=700&height=450" class="calcinfo thickbox" title="Hilfelink zu ' . $art ['Name'] . '" target="_blank"><img src="/images/transparent.png" width="22" height="22"/></a>
                </dd>', 'placement' => 'APPEND'))->addDecorator('HtmlTag', array('tag' => 'dd', 'openOnly' => true));
                                //$$key->setAttrib('title', '<a onClick="window.open(this.href,\'Help\',\'width=300, height=300\'); return false;" href="'.$art['helplink'].'?help=1" title="Hilfelink zu '.$art['Name'].'" target="_blank">Hilfelink zu '.$art['Name'].'</a>');
                            }
                            $view->view->form->addElements(array($$key));
                        }
                    }
                    break;
                case 'Hidden' :
                    $valid = true;

                    if (isset($art ['options'] ['input']) && is_array($art ['options'] ['input'])) {
                        foreach ($art ['options'] ['input'] as $section => $row) {

                            (isset($params [$section])) ? $paramsection = $params [$section] : $paramsection = 0;
                            if (isset($art ['options']['calcs'][$section]) && $art ['options']['calcs'][$section]) {
                                $paramsection = $this->createFormel($art ['options']['calcs'][$section], $params);
                            }


                            $preis [$key] [$section] [$i] = $this->getAmountLimit($row, $paramsection, 'preis', $params, $key . '_' . $section);
                            $paus [$key] [$section] [$i] = $this->getAmountLimit($row, $paramsection, 'pauschale', $params, $key . '_' . $section);

                            $this->calc_value [$key . '_' . $section] = $this->getAmountLimit($row, $paramsection, 'calc_value', $params, $key . '_' . $section);
                            $this->calc_value [$key . '_' . $section . '_1'] = $this->getAmountLimit($row, $paramsection, 'calc_value_1', $params, $key . '_' . $section . '_1');
                            $this->calc_value [$key . '_' . $section . '_2'] = $this->getAmountLimit($row, $paramsection, 'calc_value_2', $params, $key . '_' . $section . '_2');
                            $this->calc_value [$key . '_' . $section . '_3'] = $this->getAmountLimit($row, $paramsection, 'calc_value_3', $params, $key . '_' . $section . '_3');
                            $this->calc_value [$key . '_' . $section . '_4'] = $this->getAmountLimit($row, $paramsection, 'calc_value_4', $params, $key . '_' . $section . '_4');
                            $this->calc_value [$key . '_' . $section . '_5'] = $this->getAmountLimit($row, $paramsection, 'calc_value_5', $params, $key . '_' . $section . '_5');
                            $this->calc_value [$key . '_' . $section . '_6'] = $this->getAmountLimit($row, $paramsection, 'calc_value_6', $params, $key . '_' . $section . '_6');
                            $this->calc_value [$key . '_' . $section . '_7'] = $this->getAmountLimit($row, $paramsection, 'calc_value_7', $params, $key . '_' . $section . '_7');
                            $this->calc_value [$key . '_' . $section . '_8'] = $this->getAmountLimit($row, $paramsection, 'calc_value_8', $params, $key . '_' . $section . '_8');
                            $this->calc_value [$key . '_' . $section . '_9'] = $this->getAmountLimit($row, $paramsection, 'calc_value_9', $params, $key . '_' . $section . '_9');
                            $this->calc_value [$key . '_' . $section . '_10'] = $this->getAmountLimit($row, $paramsection, 'calc_value_10', $params, $key . '_' . $section . '_10');

                            if ($this->getAmountLimit($row, $paramsection, 'formel', $params, $key . '_' . $section)) {
                                $formel [$key] [$section] [$i] = $this->getAmountLimit($row, $paramsection, 'formel', $params, $key . '_' . $section);
                            }else{

                            }
                            if ($this->getEnableHidden($row, $paramsection, $params, null, $key . '_' . $section) == false) {
                                $valid = false;
                            }
                            $i++;
                        }
                    }
                    if($valid) {
                        if ($this->isAjax) {
                            $view->view->form[] = array('type' => 'hidden', 'label' => $art ['Name'], 'name' => $key, 'value' => $params [$key]);
                        } else {
                            if ($key == 'hplayouter_price' && isset($params [$key])) {
                                $view->view->form->addElement('hidden', $key, array('value' => $params [$key]));
                            } else {
                                $view->view->form->addElement('hidden', $key, array('value' => $art ['Value']));
                            }
                            $view->view->form->$key->removeDecorator('label')->removeDecorator('HtmlTag');
                        }
                    }

                    break;
                case 'GroupStart' :
                    if ($this->isAjax) {
                        $view->view->form[] = array('type' => 'groupstart', 'label' => $art ['Name'], 'name' => $key);
                    } else {
                        $elm = new TP_Form_Element_Groupstart($key);
                        $elm->setLabel($art ['Name']);
                        $view->view->form->addElement($elm);
                    }
                    break;
                case 'GroupEnd' :
                    if ($this->isAjax) {
                        $view->view->form[] = array('type' => 'groupstart', 'label' => $art ['Name'], 'name' => $key);
                    } else {
                        $view->view->form->addElement(new TP_Form_Element_Groupend($key));
                    }
                    break;
            }
        }

        $this->activeArtikel = $activeartikel;
        if (!$this->isAjax) {
            $view->view->form->setName('CALCFORM');
        }
        global $account_id;
        if(isset($view->view->user)) {
            $account_id = $view->view->user->account_id;
        }else{
        	$account_id = false;
        }
        global $calcValue1;
        global $calcValue2;
        global $calcValueAccount1;
        global $calcValueAccount2;
        if(isset($view->view->user)) {
            $calcValue1 = $view->view->user->getCalcValue1();
            $calcValue2 = $view->view->user->getCalcValue2();
            $calcValueAccount1 = $view->view->user->Account->getCalcValue1();
            $calcValueAccount2 = $view->view->user->Account->getCalcValue2();

        }else{
            $calcValue1 = 0;
            $calcValue2 = 0;
            $calcValueAccount1 = 0;
            $calcValueAccount2 = 0;
        }

        // Berechnung
        //$gesamt = $this->language['NoCalc'];
        if (isset($view->view->user) && $view->view->user->test_calc) {
            eval(str_replace('\\', '', $view->view->shop->test_parameter));
            eval(str_replace("#", '"', str_replace('"', "'", str_replace('\\', '', $view->view->shop->test_formel))));
        } else {
            eval(str_replace('\\', '', $view->view->shop->parameter));
            eval(str_replace("#", '"', str_replace('"', "'", str_replace('\\', '', $view->view->shop->formel))));
        }

        if ($view->view->shop->market) {
            $shopMarket = Doctrine_Query::create()->from('Shop c')->where('c.id = ?', array($view->view->shop->Install->defaultmarket))->fetchOne();
            if (isset($view->view->user) && $view->view->user->test_calc) {
                eval(str_replace('\\', '', $shopMarket->test_parameter));
                eval(str_replace("#", '"', str_replace('"', "'", str_replace('\\', '', $shopMarket->test_formel))));
            } else {
                eval(str_replace('\\', '', $shopMarket->parameter));
                eval(str_replace("#", '"', str_replace('"', "'", str_replace('\\', '', $shopMarket->formel))));
            }
        }

        if (!isset($params ['special']) && ($this->isAjax || $view->view->form->isValid($params))) {

            $gesamt = 0.00;

            $founds = null;
            foreach ($formel as $keys => $forms) {
                foreach ($forms as $keyss => $formss) {
                    foreach ($formss as $keysss => $formsss) {
                        preg_match_all('/\$F\w*\$F/', $formsss, $founds);

                        if (!empty($founds [0])) {

                            foreach ($founds [0] as $key => $found) {

                                $foundvalue = str_replace('$F', '', $found);
                                $formel [$keys] [$keyss] = str_replace($found, $$foundvalue, $formel [$keys] [$keyss]);
                            }
                        }
                    }
                }
            }

            $founds = null;
            foreach ($formel as $keys => $forms) {
                foreach ($forms as $keyss => $formss) {
                    foreach ($formss as $keysss => $formsss) {
                        preg_match_all('/\$F\w*\$F/', $formsss, $founds);

                        if (!empty($founds [0])) {

                            foreach ($founds [0] as $key => $found) {

                                $foundvalue = str_replace('$F', '', $found);
                                $formel [$keys] [$keyss] = str_replace($found, $$foundvalue, $formel [$keys] [$keyss]);
                            }
                        }
                    }
                }
            }

            $founds = null;
            foreach ($formel as $keys => $forms) {
                foreach ($forms as $keyss => $formss) {
                    foreach ($formss as $keysss => $formsss) {
                        preg_match_all('/\$F\w*\$F/', $formsss, $founds);

                        if (!empty($founds [0])) {

                            foreach ($founds [0] as $key => $found) {

                                $foundvalue = str_replace('$F', '', $found);
                                $formel [$keys] [$keyss] = str_replace($found, $$foundvalue, $formel [$keys] [$keyss]);
                            }
                        }
                    }
                }
            }

            $founds = null;
            foreach ($formel as $keys => $forms) {
                foreach ($forms as $keyss => $formss) {
                    foreach ($formss as $keysss => $formsss) {
                        preg_match_all('/\$CV[\w,+-]*\$CV/', $formsss, $founds);
                        if (!empty($founds [0])) {

                            foreach ($founds [0] as $key => $found) {

                                $foundvalue = str_replace('$CV', '', $found);
                                $arr = explode(',', $foundvalue);

                                if (count($arr) == 1) {
                                    if (isset($this->calc_value[$arr[0]])) {
                                        $formel [$keys] [$keyss] = str_replace($found, $this->calc_value[$arr[0]], $formel [$keys] [$keyss]);
                                    } else {
                                        $formel [$keys] [$keyss] = str_replace($found, 0, $formel [$keys] [$keyss]);
                                    }
                                } elseif (count($arr) == 2) {
                                    $actval = $this->calcArr[$arr[0]]['found'];
                                    $formel [$keys] [$keyss] = str_replace($found, $this->calcArr[$arr[0]][$actval][$arr[1]], $formel [$keys] [$keyss]);
                                } elseif (count($arr) == 3) {
                                    $actval = $this->calcArr[$arr[0]]['found'];
                                    $formel [$keys] [$keyss] = str_replace($found, $this->calcArr[$arr[0]][$actval][$arr[1]], $formel [$keys] [$keyss]);
                                } else {
                                    $actval = $this->calcArr[$arr[0]]['found'];
                                    if ($arr[2] == '+') {
                                        $actval = $actval + $arr[3];
                                    } else {
                                        $actval = $actval - $arr[3];
                                    }
                                    if ($actval < 0) {
                                        $actval = 0;
                                    }
                                    $formel [$keys] [$keyss] = str_replace($found, $this->calcArr[$arr[0]][$actval][$arr[1]], $formel [$keys] [$keyss]);
                                }

                                preg_match_all('/\$CV[\w,+-]*\$CV/', $formel [$keys] [$keyss] [$keysss], $foundsParamSubCV);

                                if (!empty($foundsParamSubCV [0])) {

                                    foreach ($foundsParamSubCV [0] as $keyParamSubCV => $foundParamSubCV) {

                                        $foundvalue = str_replace('$CV', '', $foundParamSubCV);
                                        if (isset($this->calc_value[$foundvalue])) {
                                            $formel [$keys] [$keyss] [$keysss] = str_replace($foundParamSubCV, $this->calc_value[$foundvalue], $formel [$keys] [$keyss] [$keysss]);
                                        }
                                    }
                                }

                                preg_match_all('/\$CV[\w,+-]*\$CV/', $formel [$keys] [$keyss] [$keysss], $foundsParamSubCV);

                                if (!empty($foundsParamSubCV [0])) {

                                    foreach ($foundsParamSubCV [0] as $keyParamSubCV => $foundParamSubCV) {

                                        $foundvalue = str_replace('$CV', '', $foundParamSubCV);
                                        if (isset($this->calc_value[$foundvalue])) {
                                            $formel [$keys] [$keyss] [$keysss] = str_replace($foundParamSubCV, $this->calc_value[$foundvalue], $formel [$keys] [$keyss] [$keysss]);
                                        }
                                    }
                                }

                                preg_match_all('/\$F\w*\$F/', $formel [$keys] [$keyss] [$keysss], $foundsParamSubCV);

                                if (!empty($foundsParamSubCV [0])) {

                                    foreach ($foundsParamSubCV [0] as $keyParamSubCV => $foundParamSubCV) {

                                        $foundvalue = str_replace('$F', '', $foundParamSubCV);
                                        $formel [$keys] [$keyss] [$keysss] = str_replace($foundParamSubCV, $$foundvalue, $formel [$keys] [$keyss] [$keysss]);

                                    }
                                }

                                preg_match_all('/\$CV[\w,+-]*\$CV/', $formel [$keys] [$keyss] [$keysss], $foundsParamSubCV);

                                if (!empty($foundsParamSubCV [0])) {

                                    foreach ($foundsParamSubCV [0] as $keyParamSubCV => $foundParamSubCV) {

                                        $foundvalue = str_replace('$CV', '', $foundParamSubCV);
                                        if (isset($this->calc_value[$foundvalue])) {
                                            $formel [$keys] [$keyss] [$keysss] = str_replace($foundParamSubCV, $this->calc_value[$foundvalue], $formel [$keys] [$keyss] [$keysss]);
                                        }
                                    }
                                }

                                preg_match_all('/\$CV[\w,+-]*\$CV/', $formel [$keys] [$keyss] [$keysss], $foundsParamSubCV);

                                if (!empty($foundsParamSubCV [0])) {

                                    foreach ($foundsParamSubCV [0] as $keyParamSubCV => $foundParamSubCV) {

                                        $foundvalue = str_replace('$CV', '', $foundParamSubCV);
                                        if (isset($this->calc_value[$foundvalue])) {
                                            $formel [$keys] [$keyss] [$keysss] = str_replace($foundParamSubCV, $this->calc_value[$foundvalue], $formel [$keys] [$keyss] [$keysss]);
                                        }
                                    }
                                }

                                preg_match_all('/\$CV[\w,+-]*\$CV/', $formel [$keys] [$keyss] [$keysss], $foundsParamSubCV);

                                if (!empty($foundsParamSubCV [0])) {

                                    foreach ($foundsParamSubCV [0] as $keyParamSubCV => $foundParamSubCV) {

                                        $foundvalue = str_replace('$CV', '', $foundParamSubCV);
                                        if (isset($this->calc_value[$foundvalue])) {
                                            $formel [$keys] [$keyss] [$keysss] = str_replace($foundParamSubCV, $this->calc_value[$foundvalue], $formel [$keys] [$keyss] [$keysss]);
                                        }
                                    }
                                }

                                preg_match_all('/\$CV[\w,+-]*\$CV/', $formel [$keys] [$keyss] [$keysss], $foundsParamSubCV);

                                if (!empty($foundsParamSubCV [0])) {

                                    foreach ($foundsParamSubCV [0] as $keyParamSubCV => $foundParamSubCV) {

                                        $foundvalue = str_replace('$CV', '', $foundParamSubCV);
                                        if (isset($this->calc_value[$foundvalue])) {
                                            $formel [$keys] [$keyss] [$keysss] = str_replace($foundParamSubCV, $this->calc_value[$foundvalue], $formel [$keys] [$keyss] [$keysss]);
                                        }
                                    }
                                }

                                preg_match_all('/\$F\w*\$F/', $formel [$keys] [$keyss] [$keysss], $foundsParamSubCV);

                                if (!empty($foundsParamSubCV [0])) {

                                    foreach ($foundsParamSubCV [0] as $keyParamSubCV => $foundParamSubCV) {

                                        $foundvalue = str_replace('$F', '', $foundParamSubCV);
                                        $formel [$keys] [$keyss] [$keysss] = str_replace($foundParamSubCV, $$foundvalue, $formel [$keys] [$keyss] [$keysss]);

                                    }
                                }

                                preg_match_all('/\$CV[\w,+-]*\$CV/', $formel [$keys] [$keyss] [$keysss], $foundsParamSubCV);

                                if (!empty($foundsParamSubCV [0])) {

                                    foreach ($foundsParamSubCV [0] as $keyParamSubCV => $foundParamSubCV) {

                                        $foundvalue = str_replace('$CV', '', $foundParamSubCV);
                                        if (isset($this->calc_value[$foundvalue])) {
                                            $formel [$keys] [$keyss] [$keysss] = str_replace($foundParamSubCV, $this->calc_value[$foundvalue], $formel [$keys] [$keyss] [$keysss]);
                                        }
                                    }
                                }

                                preg_match_all('/\$CV[\w,+-]*\$CV/', $formel [$keys] [$keyss] [$keysss], $foundsParamSubCV);

                                if (!empty($foundsParamSubCV [0])) {

                                    foreach ($foundsParamSubCV [0] as $keyParamSubCV => $foundParamSubCV) {

                                        $foundvalue = str_replace('$CV', '', $foundParamSubCV);
                                        if (isset($this->calc_value[$foundvalue])) {
                                            $formel [$keys] [$keyss] [$keysss] = str_replace($foundParamSubCV, $this->calc_value[$foundvalue], $formel [$keys] [$keyss] [$keysss]);
                                        }
                                    }
                                }

                                preg_match_all('/\$P\w*\$P/', $formel [$keys] [$keyss] [$keysss], $foundsParam);

                                if (!empty($foundsParam [0])) {

                                    foreach ($foundsParam [0] as $keyParam => $foundParam) {

                                        $foundvalue = str_replace('$P', '', $foundParam);
                                        $formel [$keys] [$keyss] [$keysss] = str_replace($foundParam, $$foundvalue, $formel [$keys] [$keyss] [$keysss]);
                                    }
                                }

                                preg_match_all('/\$CV[\w,+-]*\$CV/', $formel [$keys] [$keyss] [$keysss], $foundsParamSubCV);

                                if (!empty($foundsParamSubCV [0])) {

                                    foreach ($foundsParamSubCV [0] as $keyParamSubCV => $foundParamSubCV) {

                                        $foundvalue = str_replace('$CV', '', $foundParamSubCV);
                                        if (isset($this->calc_value[$foundvalue])) {
                                            $formel [$keys] [$keyss] [$keysss] = str_replace($foundParamSubCV, $this->calc_value[$foundvalue], $formel [$keys] [$keyss] [$keysss]);
                                        }
                                    }
                                }

                                preg_match_all('/\$F\w*\$F/', $formel [$keys] [$keyss] [$keysss], $foundsParamSubCV);

                                if (!empty($foundsParamSubCV [0])) {

                                    foreach ($foundsParamSubCV [0] as $keyParamSubCV => $foundParamSubCV) {

                                        $foundvalue = str_replace('$F', '', $foundParamSubCV);
                                        $formel [$keys] [$keyss] [$keysss] = str_replace($foundParamSubCV, $$foundvalue, $formel [$keys] [$keyss] [$keysss]);

                                    }
                                }

                                preg_match_all('/\$CV[\w,+-]*\$CV/', $formel [$keys] [$keyss] [$keysss], $foundsParamSubCV);

                                if (!empty($foundsParamSubCV [0])) {

                                    foreach ($foundsParamSubCV [0] as $keyParamSubCV => $foundParamSubCV) {

                                        $foundvalue = str_replace('$CV', '', $foundParamSubCV);
                                        if (isset($this->calc_value[$foundvalue])) {
                                            $formel [$keys] [$keyss] [$keysss] = str_replace($foundParamSubCV, $this->calc_value[$foundvalue], $formel [$keys] [$keyss] [$keysss]);
                                        }
                                    }
                                }

                                preg_match_all('/\$CV[\w,+-]*\$CV/', $formel [$keys] [$keyss] [$keysss], $foundsParamSubCV);

                                if (!empty($foundsParamSubCV [0])) {

                                    foreach ($foundsParamSubCV [0] as $keyParamSubCV => $foundParamSubCV) {

                                        $foundvalue = str_replace('$CV', '', $foundParamSubCV);
                                        if (isset($this->calc_value[$foundvalue])) {
                                            $formel [$keys] [$keyss] [$keysss] = str_replace($foundParamSubCV, $this->calc_value[$foundvalue], $formel [$keys] [$keyss] [$keysss]);
                                        }
                                    }
                                }

                                preg_match_all('/\$P\w*\$P/', $formel [$keys] [$keyss] [$keysss], $foundsParam);

                                if (!empty($foundsParam [0])) {

                                    foreach ($foundsParam [0] as $keyParam => $foundParam) {

                                        $foundvalue = str_replace('$P', '', $foundParam);
                                        $formel [$keys] [$keyss] [$keysss] = str_replace($foundParam, $$foundvalue, $formel [$keys] [$keyss] [$keysss]);
                                    }
                                }

                                preg_match_all('/\$CV[\w,+-]*\$CV/', $formel [$keys] [$keyss] [$keysss], $foundsParamSubCV);

                                if (!empty($foundsParamSubCV [0])) {

                                    foreach ($foundsParamSubCV [0] as $keyParamSubCV => $foundParamSubCV) {

                                        $foundvalue = str_replace('$CV', '', $foundParamSubCV);
                                        if (isset($this->calc_value[$foundvalue])) {
                                            $formel [$keys] [$keyss] [$keysss] = str_replace($foundParamSubCV, $this->calc_value[$foundvalue], $formel [$keys] [$keyss] [$keysss]);
                                        }
                                    }
                                }

                                preg_match_all('/\$CV[\w,+-]*\$CV/', $formel [$keys] [$keyss] [$keysss], $foundsParamSubCV);

                                if (!empty($foundsParamSubCV [0])) {

                                    foreach ($foundsParamSubCV [0] as $keyParamSubCV => $foundParamSubCV) {

                                        $foundvalue = str_replace('$CV', '', $foundParamSubCV);
                                        if (isset($this->calc_value[$foundvalue])) {
                                            $formel [$keys] [$keyss] [$keysss] = str_replace($foundParamSubCV, $this->calc_value[$foundvalue], $formel [$keys] [$keyss] [$keysss]);
                                        }
                                    }
                                }

                                preg_match_all('/\$CV[\w,+-]*\$CV/', $formel [$keys] [$keyss] [$keysss], $foundsParamSubCV);

                                if (!empty($foundsParamSubCV [0])) {

                                    foreach ($foundsParamSubCV [0] as $keyParamSubCV => $foundParamSubCV) {

                                        $foundvalue = str_replace('$CV', '', $foundParamSubCV);
                                        if (isset($this->calc_value[$foundvalue])) {
                                            $formel [$keys] [$keyss] [$keysss] = str_replace($foundParamSubCV, $this->calc_value[$foundvalue], $formel [$keys] [$keyss] [$keysss]);
                                        }
                                    }
                                }

                                preg_match_all('/\$F\w*\$F/', $formel [$keys] [$keyss] [$keysss], $foundsParamSubCV);

                                if (!empty($foundsParamSubCV [0])) {

                                    foreach ($foundsParamSubCV [0] as $keyParamSubCV => $foundParamSubCV) {

                                        $foundvalue = str_replace('$F', '', $foundParamSubCV);
                                        $formel [$keys] [$keyss] [$keysss] = str_replace($foundParamSubCV, $$foundvalue, $formel [$keys] [$keyss] [$keysss]);

                                    }
                                }

                                preg_match_all('/\$CV[\w,+-]*\$CV/', $formel [$keys] [$keyss] [$keysss], $foundsParamSubCV);

                                if (!empty($foundsParamSubCV [0])) {

                                    foreach ($foundsParamSubCV [0] as $keyParamSubCV => $foundParamSubCV) {

                                        $foundvalue = str_replace('$CV', '', $foundParamSubCV);
                                        if (isset($this->calc_value[$foundvalue])) {
                                            $formel [$keys] [$keyss] [$keysss] = str_replace($foundParamSubCV, $this->calc_value[$foundvalue], $formel [$keys] [$keyss] [$keysss]);
                                        }else{
                                            $formel [$keys] [$keyss] [$keysss] = str_replace($foundParamSubCV, 0, $formel [$keys] [$keyss] [$keysss]);
                                        }
                                    }
                                }

                                preg_match_all('/\$P\w*\$P/', $formel [$keys] [$keyss] [$keysss], $foundsParam);

                                if (!empty($foundsParam [0])) {

                                    foreach ($foundsParam [0] as $keyParam => $foundParam) {

                                        $foundvalue = str_replace('$P', '', $foundParam);
                                        $formel [$keys] [$keyss] [$keysss] = str_replace($foundParam, $$foundvalue, $formel [$keys] [$keyss] [$keysss]);
                                    }
                                }

                            }
                        }

                        preg_match_all('/\$P\w*\$P/', $formsss, $founds);
                        if (!empty($founds [0])) {

                            foreach ($founds [0] as $key => $found) {

                                $foundvalue = str_replace('$P', '', $found);
                                $formel [$keys] [$keyss] = str_replace($found, $$foundvalue, $formel [$keys] [$keyss]);
                            }
                        }
                    }
                }
            }

            $founds = null;
            foreach ($formel as $keys => $forms) {

                foreach ($forms as $keyss => $formss) {
                    foreach ($formss as $keysss => $formsss) {
                        preg_match_all('/\$V\w*\$V/', $formsss, $founds);
                        if (!empty($founds [0])) {
                            foreach ($founds [0] as $key => $found) {
                                $foundvalue = str_replace('$V', '', $found);
                                if (isset($params [$foundvalue])) {
                                    if ($params [$foundvalue] == 'null') {
                                        $formel [$keys] [$keyss] = str_replace($found, 0, $formel [$keys] [$keyss]);
                                    } else {
                                        if ($foundvalue == 'auflage') {
                                            $formel [$keys] [$keyss] = str_replace($found, str_replace(',', '.', $params [$foundvalue]), $formel [$keys] [$keyss]);
                                        } else {
                                            if ($params [$foundvalue] == '') {
                                                $formel [$keys] [$keyss] = str_replace($found, 0, $formel [$keys] [$keyss]);
                                            } else {
                                                $formel [$keys] [$keyss] = str_replace($found, $params [$foundvalue], $formel [$keys] [$keyss]);
                                            }
                                        }
                                    }
                                } else {
                                    $formel [$keys] [$keyss] = str_replace($found, 0, $formel [$keys] [$keyss]);
                                }
                            }
                        }
                    }
                }
            }

            foreach ($preis as $key => $gp) {

                $tempWert = 0;
                foreach ($gp as $keys => $gps) {
                    if (is_array($gps)) {
                        foreach ($gps as $keyss => $gpss) {
                            if (isset($params [$keys]) && $gpss != "") {

                                $tempWert += $params [$keys] * $gpss;
                            }
                        }
                    }
                }

                $gesamt += $tempWert;
            }
            foreach ($paus as $keyp => $pp) {
                $tempWert = 0;
                foreach ($pp as $keypp => $ppp) {
                    foreach ($ppp as $keyppp => $pppp) {
                        $tempWert += $pppp;
                    }
                }

                $gesamt += $tempWert;
            }
            $gewicht = 0;
            $gewicht_single = 0;

            $view->view->ownValues = array();
            $view->view->displayValues = array();

            if (is_array($formel)) {
                foreach ($formel as $fk => $form) {
                    foreach ($form as $formkey => $formrow) {
                        foreach ($formrow as $formkeys => $formrows) {
                            if ($formrows != "") {
                                if($activeartikel ['options'][$fk]['amount'] == false) {
                                    if (isset($view->view->user) && $view->view->user->show_calc) {
                                        $view->view->debug[] = $formkey . '=' . str_replace('\\', '', $formrows) . ';<br/><br/>';
                                    }
                                    eval('$tempWert=' . str_replace('\\', '', $formrows) . ';');
                                    $this->calc_value[$fk] = $tempWert;
                                    continue;
                                }
                                if ($fk == 'weight') {
                                    if (isset($view->view->user) && $view->view->user->show_calc) {
                                        $view->view->debug[] = $fk . '=' . str_replace('\\', '', $formrows) . ';<br/><br/>';
                                    }

                                    eval('$tempWert=' . str_replace('\\', '', $formrows) . ';');
                                    $gewicht += $tempWert;
                                } elseif($fk == 'weight_single') {
                                    if (isset($view->view->user) && $view->view->user->show_calc) {
                                        $view->view->debug[] = $fk . '=' . str_replace('\\', '', $formrows) . ';<br/><br/>';
                                    }

                                    eval('$tempWert=' . str_replace('\\', '', $formrows) . ';');
                                    $gewicht_single += $tempWert;
                                }elseif($activeartikel ['options'][$fk]['exportAjax']) {
                                    if($this->isValidPHP('$tempWert=' . str_replace('\\', '', $formrows) . ';')) {
                                        eval('$tempWert=' . str_replace('\\', '', $formrows) . ';');
                                    }

                                    $view->view->ownValues[$fk] = $tempWert;
                                }elseif($activeartikel ['options'][$fk]['displayOnly']) {
                                    $view->view->displayValues[$fk] = $formrows;
                                } else {
                                    if($this->isValidPHP('$tempWert=' . str_replace('\\', '', $formrows) . ';')) {
                                        eval('$tempWert=' . str_replace('\\', '', $formrows) . ';');
                                    }
                                    if (isset($view->view->user) && $view->view->user->show_calc) {
                                        $view->view->debug[] = $formkey . '=' . str_replace('\\', '', $formrows) . ';<br/><br/>'.$tempWert;
                                    }

                                    $gesamt += $tempWert;
                                }
                            }
                        }
                    }
                }
            }

            if ($gewicht != 0) {
                $tempProdukt->setWeight($gewicht);
            }
            if ($gewicht_single != 0) {
                $tempProdukt->setWeightSingle($gewicht_single);
            }
            $tempProdukt->setCalcValues($this->calc_value);
            $tempProdukt->setRabatte($view->view->ownValues);

            $gesamt = $gesamt + $singlePreis;

            return round($gesamt, 2);
        }
    }

    private function isValidPHP($str) {
        return (strpos(trim(shell_exec("echo " . escapeshellarg('<?php '.$str) . " | php -l")),"No syntax errors detected") !== false);
    }

}
