<?php
class TP_Config extends Zend_Config {
	
	public function __construct($array, $allowModifications = false)
    {
        $this->_allowModifications = (boolean) $allowModifications;
        $this->_loadedSection = null;
        $this->_index = 0;
        $this->_data = array();
        foreach ($array as $key => $value) {
            if (is_array($value)) {
                $this->_data[$key] = new self($value, $this->_allowModifications);
            } else {
                $this->_data[$key] = $value;
            }
        }
        $this->_count = count($this->_data);
    }
	
	/**
     * Return an associative array of the stored data.
     *
     * @return array
     */
    public function toArray()
    {
        $array = array();
        foreach ($this->_data as $key => $value) {
            if ($value instanceof TP_Config) {
                $array[$key] = $value->toArray();
            } else {
            	if($value=="true") $array[$key] = true;
            	elseif ($value == "false") $array[$key] = false;
            	else $array[$key] = $value;
            }
        }
        return $array;
    }

}