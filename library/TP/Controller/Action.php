<?php

class TP_Controller_Action extends Zend_Controller_Action {

    public $_templatePath;
    protected $_mailPath;
    protected $_layoutPath;
    protected $_configPath;
    protected $translate = null;
    protected $shopIds = array();

    /** @var Shop */
    protected $shop = null;
    /** @var Contact */
    protected $user = null;
    public $highRole = null;
    public $install = null;

    final protected function forwardSpeak($action, $controller = null, $module = null, array $params = null)
    {
        $dbMongo = TP_Mongo::getInstance();

        $route = $dbMongo->Route->findOne(array('url' => '/'.$controller.'/'.$action));
        if($route) {
            $str = '';
            if($params) {
                foreach ($params as $key => $value) {
                    $str .= $key . '=' . $value . '&';
                }
            }
            $this->redirect($route->parameter->getArrayCopy()['target'] .'?'.$str);
        }
        $this->forward($action, $controller, $module, $params);
    }

    final protected function redirectSpeak($url, $options = [])
    {
        $dbMongo = TP_Mongo::getInstance();

        $route = $dbMongo->Route->findOne(array('url' => $url));

        if($route) {
            $this->redirect($route->parameter->getArrayCopy()['target']);
        }
        $this->redirect($url, $options);
    }

    public function init()
    {

        parent::init();

        $this->view->tag = $this->_getParam('tag');

        $conf = Zend_Registry::get('shop');
        $confinstall = Zend_Registry::get('install');
        $this->view->subshop = true;
        $this->view->addHelperPath('EasyBib/View/Helper', 'EasyBib_View_Helper');

        if ($conf['id'] == $confinstall['defaultmarket']) {
            $this->view->subshop = false;
        } else {
            $shopMarket = Doctrine_Query::create()->from('Shop c')->where('c.id = ?', array($confinstall['defaultmarket']))->fetchOne();
            $this->view->marktplatzurl = $shopMarket->Domain[0]->name;
            $this->view->marktplatzname = $shopMarket->name;

            Zend_Registry::set('market_shop', $shopMarket->toArray());
        }
        $backurl = new Zend_Session_Namespace('backbutton');
        if (!isset($backurl->urls) || $backurl->urls == null)
            $backurl->urls = array();
        if (isset($_SERVER['HTTP_REFERER']) && !strpos($_SERVER['HTTP_REFERER'], $_SERVER['REQUEST_URI']) && !strpos($_SERVER['REQUEST_URI'], 'styles') && !strpos($_SERVER['REQUEST_URI'], 'upload') && (!isset($backurl->urls[0]) || $_SERVER['HTTP_REFERER'] != $backurl->urls[0])) {

            array_unshift($backurl->urls, $_SERVER['HTTP_REFERER']);

            $backurl->urls = array_slice($backurl->urls, 0, 10);
        }
        if (isset($backurl->urls[0])) {
            $this->view->backurl = $backurl->urls[0];
        }
        if (isset($backurl->urls[1])) {
            $this->view->backurl1 = $backurl->urls[1];
        }
        $this->view->shop = Doctrine_Query::create()->from('Shop s')->where('s.id = ?')->fetchOne(array(
            $conf['id']));

        if($this->view->shop->template_switch && Zend_Auth::getInstance()->hasIdentity()) {
            $user = Zend_Auth::getInstance()->getIdentity();
            $accountPath = $this->getAccountTemplatePath($user['account_id']);
            if($accountPath != "") {
                $conf['layout'] = $accountPath;

                Zend_Registry::set('layout_path', APPLICATION_PATH . '/design/clients/' . $this->view->shop->uid . '/' . $accountPath  . '');
            }
        }

        $this->_templatePath = Zend_Registry::get('layout_path') . '/templates';
        $this->_articletemplatePath = Zend_Registry::get('layout_path') . '/articletemplates';
        $this->_mailPath = Zend_Registry::get('layout_path') . '/mails';
        $this->_layoutPath = Zend_Registry::get('layout_path') . '/layout';
        if (file_exists(Zend_Registry::get('layout_path') . '/config')) {
            $this->_configPath = Zend_Registry::get('layout_path') . '/config';
        } else {
            $this->_configPath = APPLICATION_PATH . '/design/vorlagen/' . $conf['layout'] . '/config';
        }



        $this->view->addHelperPath(APPLICATION_PATH . '/helpers', 'TP_View_Helper');

        if ($conf['customtemplates'] == 1 && !isset($_REQUEST['testMode'])) {
            $this->view->designPath = 'styles/vorlagen/' . $conf['layout'] . '/';

            $this->view->setScriptPath(Zend_Registry::get('layout_path') . '/templates');
            $this->view->addBasePath($this->_mailPath);
            $this->view->addBasePath($this->_articletemplatePath);
        } else {
            if (isset($_REQUEST['testMode']) && $_REQUEST['testMode'] == 'q3o847tqcp236r') {
                $this->view->designPath = 'styles/' . $conf['uid'] . '/design/' . $_REQUEST['testLayout'] . '/';
                if($conf['customtemplates'] == 1) {
                    $this->view->designPath = 'styles/vorlagen/' . $_REQUEST['testLayout'] . '/';
                }
            } elseif (strpos('/' . $conf['layout'], '..')) {
                $this->view->designPath = str_replace("subshopdesigns", "design/subshopdesigns", str_replace("..", "styles", $conf['layout'])) . '/';
            } elseif (file_exists('styles/' . $conf['uid'] . '/design/' . $conf['layout'])) {
                $this->view->designPath = 'styles/' . $conf['uid'] . '/design/' . $conf['layout'] . '/';
            } else {
                $this->view->designPath = 'styles/vorlagen/' . $conf['layout'] . '/';
            }

            $this->view->setScriptPath(APPLICATION_PATH . '/design/vorlagen/' . $conf['layout'] . '/templates/');
            $this->view->addScriptPath(APPLICATION_PATH . '/design/vorlagen/' . $conf['layout'] . '/layout/');
            $this->view->addBasePath(APPLICATION_PATH . '/design/vorlagen/' . $conf['layout'] . '/mails/');
            $this->view->addBasePath(APPLICATION_PATH . '/design/vorlagen/' . $conf['layout'] . '/articletemplates/');
            $this->view->addScriptPath(Zend_Registry::get('layout_path') . '/templates');

            if($this->view->shop->market) {
                $install = Zend_Registry::get('install');
                $market = Doctrine_Query::create()->from('Shop s')->where('s.id = ?')->fetchOne(array(
                    $confinstall['defaultmarket']));
                if(file_exists(APPLICATION_PATH . '/design/clients/' . $market->uid . '/subshopdesigns/public/' . $this->view->shop->layout . '/templates/index/ms.phtml')) {
                    $this->view->addBasePath(APPLICATION_PATH . '/design/clients/' . $market->uid . '/subshopdesigns/public/' . $this->view->shop->layout . '/articletemplates');
                    $this->view->addScriptPath(APPLICATION_PATH . '/design/clients/' . $market->uid . '/subshopdesigns/public/' . $this->view->shop->layout . '/templates');
                }
            }
            $this->view->addBasePath($this->_mailPath);
            $this->view->addBasePath($this->_articletemplatePath);
        }

        $this->view->term = "Suchbegriff eingeben";

        $this->shop = $this->view->shop;

        $artCount = Doctrine_Query::create ()->from('Article a')
            ->where('a.shop_id = ? AND a.private = 0 AND a.enable = 1', array($this->shop->id))->execute()->count();

        $this->view->finisharticles = $artCount;

        $filter = new TP_Themesfilter( );
        $this->view->filter = $filter;
        $this->view->messages = array();
        $this->view->title = $this->view->shop->title;
        $this->view->subtitle = $this->view->shop->subtitle;
        $this->view->logo1 = $this->view->shop->logo1;
        $this->view->logo2 = $this->view->shop->logo2;
        $this->view->actmenu = array();
        $this->view->actmenu[] = $this->getRequest()->getActionName();
        $this->install = $this->view->shop->Install;

        if ((isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on') || (isset($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] == 'https')) {
            $this->view->basepath = 'https://' . $_SERVER["SERVER_NAME"];
        } else {
            $this->view->basepath = 'http://' . $_SERVER["SERVER_NAME"];
            if($this->shop->isRedirectToSSL() && strpos($_SERVER['REQUEST_URI'], 'service/template') === false) {
                header('Location: '. 'https://' . $_SERVER["SERVER_NAME"].$_SERVER['REQUEST_URI']);
                die();
            }
        }
        $this->view->redirect_ssl_path = $this->view->basepath;
        if ($this->install->redirect_ssl && ($this->getRequest()->getControllerName() == 'basket' || $this->getRequest()->getControllerName() == 'user')) {
            $this->view->redirect_ssl_path = 'https://' . $_SERVER["SERVER_NAME"];
        }

        $translate = Zend_Registry::get('Zend_Translate');
        $translation = Zend_Registry::get('layout_path') . '/locale';

        if ($this->getRequest()->getParam('speak')) {
            $locale = Zend_Registry::get('locale');
            $locale->setLocale($this->getRequest()->getParam('speak'));
            Zend_Registry::set('locale', $locale);
            $lang = new Zend_Session_Namespace('lang');
            $lang->code = $this->getRequest()->getParam('speak');
        }
        if ($this->getRequest()->getParam('liveedit', false) && $this->getRequest()->getParam('adminmode', false)) {
            $mode = new Zend_Session_Namespace('adminmode');
            $mode->liveedit = true;
        } elseif ($this->getRequest()->getParam('adminmode', false)) {
            $mode = new Zend_Session_Namespace('adminmode');
            $mode->liveedit = false;
        }
        $this->view->liveedit = false;

        $mode = new Zend_Session_Namespace('adminmode');
        if ($mode->liveedit == true) {
            $this->view->liveedit = true;
        }
        if ($conf['customtemplates'] == 1) {
            $translate->addTranslation($translation, Zend_Registry::get('locale'));
        } else {

            if (file_exists($translation)) {
                $translate->addTranslation($translation, Zend_Registry::get('locale'));
            } else {

                $translate->addTranslation(APPLICATION_PATH . '/design/vorlagen/' . $conf['layout'] . '/locale', Zend_Registry::get('locale'));
            }
        }
        $translate->setLocale(Zend_Registry::get('locale'));
        Zend_Registry::set('Zend_Translate', $translate);
        $this->translate = $translate;

        $this->view->currency = new Zend_Currency(Zend_Registry::get('locale')->getLanguage() . '_' . Zend_Registry::get('locale')->getRegion());
        //$this->view->currency = new Zend_Currency('de_DE');
        $this->view->langCode = Zend_Registry::get('locale')->getLanguage();

        $this->view->currency = new Zend_Currency($this->shop->getDefaultLocale(), $this->shop->getDefaultCurrency()); 
        $group = $this->_getParam('id');
        if (!isset($group)) {
            $group = 0;
        } else {
            $group = intval($this->_getParam('id'));
        }

        $config = new Zend_Config_Xml(APPLICATION_PATH . '/configs/navigation.xml', 'nav');
        $container = new Zend_Navigation($config);

        $dbMongo = TP_Mongo::getInstance();

        foreach($container->getPages() as $page) {
            $route = $dbMongo->Route->findOne(array('url' => $page->getHref()));
            if($route) {
                $page->setUri($route->parameter->getArrayCopy()['target']);

            }
            foreach($page->getPages() as $subPage) {
                $route = $dbMongo->Route->findOne(array('url' => $subPage->getHref()));
                if($route) {
                    $subPage->setUri($route->parameter->getArrayCopy()['target']);
                }
                foreach($subPage->getPages() as $subSubPage) {
                    $route = $dbMongo->Route->findOne(array('url' => $subSubPage->getHref()));
                    if($route) {
                        $subSubPage->setUri($route->parameter->getArrayCopy()['target']);
                    }

                }
            }
        }


        if (!$cms = Zend_Registry::get('filecache')->load('cms_navigation' . $this->shop->id . '_' . Zend_Registry::get('locale')->getLanguage() . '_' . Zend_Registry::get('locale')->getRegion())
        ) {
            $rows = Doctrine_Query::create()->from('Cms m')->where('private = 0 AND enable = 1 AND notinmenu != 1 AND shop_id = ? AND (language = ? OR language = \'all\') AND parent = 0', array(
                        intval($conf['id']),
                        Zend_Registry::get('locale')))->orderBy('sor ASC')->execute();

            $page = $container->findOneBy('label', 'Home');
            foreach ($rows as $row) {
                $pagepos = $container->findOneBy('label', $row->pos);
                $this->getCms($page, $pagepos, $row, $container);
            }

            Zend_Registry::get('filecache')->save(array(
                'left' => $container->findOneBy('label', 'left'),
                'right' => $container->findOneBy('label', 'right'),
                'top' => $container->findOneBy('label', 'top'),
                'bottom' => $container->findOneBy('label', 'bottom'),
                'custom1' => $container->findOneBy('label', 'custom1'),
                'custom2' => $container->findOneBy('label', 'custom2'),
                'custom3' => $container->findOneBy('label', 'custom3'),
                'custom4' => $container->findOneBy('label', 'custom4'),
                'home' => $container->findOneBy('label', 'Home')
                    ), 'cms_navigation' . $this->shop->id . '_' . Zend_Registry::get('locale')->getLanguage() . '_' . Zend_Registry::get('locale')->getRegion(), array('cms_' . $this->shop->id, 'shop_' . $this->shop->id));
        } else {
            $container->removePage($container->findOneBy('label', 'Home'));
            $container->removePage($container->findOneBy('label', 'right'));
            $container->removePage($container->findOneBy('label', 'left'));
            $container->removePage($container->findOneBy('label', 'top'));
            $container->removePage($container->findOneBy('label', 'bottom'));
            $container->removePage($container->findOneBy('label', 'custom1'));
            $container->removePage($container->findOneBy('label', 'custom2'));
            $container->removePage($container->findOneBy('label', 'custom3'));
            $container->removePage($container->findOneBy('label', 'custom4'));
            $container->addPage($cms['home']);
            $container->addPage($cms['left']);
            $container->addPage($cms['right']);
            $container->addPage($cms['top']);
            if ($cms['bottom']) {
                $container->addPage($cms['bottom']);
            }
            if ($cms['custom1']) {
                $container->addPage($cms['custom1']);
            }
            if ($cms['custom2']) {
                $container->addPage($cms['custom2']);
            }
            if ($cms['custom3']) {
                $container->addPage($cms['custom3']);
            }
            if ($cms['custom4']) {
                $container->addPage($cms['custom4']);
            }
        }

        if (!$groups = Zend_Registry::get('filecache')->load('articlegroups_navigation' . $this->shop->id . '_' . Zend_Registry::get('locale')->getLanguage() . '_' . Zend_Registry::get('locale')->getRegion())) {

            $articlegroupsd = Doctrine_Query::create()->from('ArticleGroup c')->where('c.shop_id = ? AND c.notinmenu = 0 AND c.enable = 1 AND (c.parent=0 OR c.parent is null) AND (language = ? OR language = \'all\')', array(
                        $this->shop->id,
                        Zend_Registry::get('locale')))->orderBy('c.pos ASC')->execute();


            $container->findOneBy('id', 'Home')->addPage(array(
                'label' => 'Articlegroups',
                'id' => 'Articlegroups',
                'uri' => '/overview'));

            $container->findOneBy('id', 'Home')->addPage(array(
                'label' => 'ArticlegroupsArticle',
                'id' => 'ArticlegroupsArticle',
                'uri' => '/overview/'));

            $page = $container->findOneBy('id', 'Articlegroups');
            $pageart = $container->findOneBy('id', 'ArticlegroupsArticle');

            if ($this->shop->template_navi_display_all) {
                $page->addPage(array(
                    'label' => 'All',
                    'id' => 'All',
                    'uri' => '/overview'));
            }

            foreach ($articlegroupsd as $articlegroup) {
                $this->getArticleGroups($page, $pageart, $articlegroup, $container);
            }

            Zend_Registry::get('filecache')->save(array(
                'ag' => $container->findOneBy('id', 'Articlegroups'),
                'aga' => $container->findOneBy('id', 'ArticlegroupsArticle')
                    ), 'articlegroups_navigation' . $this->shop->id . '_' . Zend_Registry::get('locale')->getLanguage() . '_' . Zend_Registry::get('locale')->getRegion(), array('articlegroups_' . $this->shop->id, 'shop_' . $this->shop->id));
        } else {
            $container->findOneBy('label', 'Home')->addPage($groups['ag']);
            $container->findOneBy('label', 'Home')->addPage($groups['aga']);
        }

        if (!$at = Zend_Registry::get('filecache')->load('articletheme_navigation' . $this->shop->id . '_' . Zend_Registry::get('locale')->getLanguage() . '_' . Zend_Registry::get('locale')->getRegion())) {

            $themesproducts = Doctrine_Query::create()->from('ArticleTheme c')->where('c.shop_id = ? AND c.parent_id = 0', array(
                        $this->shop->id))->orderBy('c.title ASC')->execute();

            $container->findOneBy('id', 'Home')->addPage(array(
                'label' => 'Articlethemes',
                'id' => 'Articlethemes',
                'uri' => '/themes/product/'));

            $tp = $container->findOneBy('id', 'Articlethemes');
            foreach ($themesproducts as $themesproduct) {
                $this->getArticleThemes($tp, $themesproduct, $container);
            }

            Zend_Registry::get('filecache')->save($container->findOneBy('id', 'Articlethemes'), 'articletheme_navigation' . $this->shop->id . '_' . Zend_Registry::get('locale')->getLanguage() . '_' . Zend_Registry::get('locale')->getRegion(), array('articletheme_' . $this->shop->id, 'shop_' . $this->shop->id));
        } else {
            $container->findOneBy('label', 'Home')->addPage($at);
        }


        $filter = new TP_Themesfilter ( );

        if (count($filter->getArticleFilter()) > 0) {
            foreach ($filter->getArticleFilter() as $key => $value) {
                $tp = $container->findOneBy('id', 'AT' . $key);
                if ($tp) {
                    $tp->active = true;
                    $tp->setClass("active");
                    $tp->setOptions(array('uri' => '/themes/product/filter/del/' . $key));
                }
            }
        }


        if (!$mt = Zend_Registry::get('filecache')->load('motivtheme_navigation' . $this->shop->id . '_' . Zend_Registry::get('locale')->getLanguage() . '_' . Zend_Registry::get('locale')->getRegion())) {

            $themesmotive = Doctrine_Query::create()->from('MotivTheme c')->where('c.shop_id = ? AND c.parent_id = 0', array(
                        $this->shop->id))->orderBy('c.title ASC')->execute();

            $container->findOneBy('id', 'Home')->addPage(array(
                'label' => 'Motivthemes',
                'id' => 'Motivthemes',
                'uri' => '/themes/motiv/'));

            $tm = $container->findOneBy('id', 'Motivthemes');
            foreach ($themesmotive as $themesmotiv) {
                $this->getMotivThemes($tm, $themesmotiv, $container);
            }

            Zend_Registry::get('filecache')->save($container->findOneBy('id', 'Motivthemes'), 'motivtheme_navigation' . $this->shop->id . '_' . Zend_Registry::get('locale')->getLanguage() . '_' . Zend_Registry::get('locale')->getRegion(), array('motivtheme_' . $this->shop->id, 'shop_' . $this->shop->id));
        } else {
            $container->findOneBy('label', 'Home')->addPage($mt);
        }

        if (count($filter->getMotivFilter()) > 0) {
            foreach ($filter->getMotivFilter() as $key => $value) {
                $tp = $container->findOneBy('id', 'MT' . $key);
                if (is_object($tp)) {
                    $tp->active = true;
                    $tp->setClass("active");
                    $tp->setOptions(array('uri' => '/themes/motiv/filter/del/' . $key));
                }
            }
        }

        if (!$st = Zend_Registry::get('filecache')->load('shoptheme_navigation' . $this->shop->id . '_' . Zend_Registry::get('locale')->getLanguage() . '_' . Zend_Registry::get('locale')->getRegion())) {

            $themesshops = Doctrine_Query::create()->from('ShopTheme c')->where('c.shop_id = ? AND c.parent_id = 0', array(
                        $this->shop->Install->defaultmarket))->orderBy('c.title ASC')->execute();

            $container->findOneBy('id', 'Home')->addPage(array(
                'label' => 'Shopthemes',
                'id' => 'Shopthemes',
                'uri' => '/themes/shop/'));


            $tm = $container->findOneBy('id', 'Shopthemes');
            foreach ($themesshops as $themesshop) {
                $this->getShopThemes($tm, $themesshop, $container);
            }

            Zend_Registry::get('filecache')->save($container->findOneBy('id', 'Shopthemes'), 'shoptheme_navigation' . $this->shop->id . '_' . Zend_Registry::get('locale')->getLanguage() . '_' . Zend_Registry::get('locale')->getRegion(), array('shoptheme_' . $this->shop->id, 'shop_' . $this->shop->id));
        } else {
            $container->findOneBy('label', 'Home')->addPage($st);
        }

        if (count($filter->getShopFilter()) > 0) {
            foreach ($filter->getShopFilter() as $key => $value) {
                $tp = $container->findOneBy('id', 'ST' . $key);
                $tp->active = true;
                $tp->setClass("active");
                $tp->setOptions(array('uri' => '/themes/shop/filter/del/' . $key));
            }
        }


        $container->findOneBy('id', 'Article')->setOptions(array('uri' => $this->view->backurl));


        $temp = array();
        if ($this->install->id != 7) {
            $articles = Doctrine_Query::create()->from('ArticleGroupArticle c')->leftJoin('c.Article a')->where('a.shop_id = ? AND a.private = 0 AND c.articlegroup_id = ? AND a.enable = 1', array(
                $this->shop->id,
                $group))->execute();

            foreach ($articles as $art) {
                array_push($temp, $art->Article);
            }
        }
        $this->view->articles = $temp;
        $this->view->my_articles = false;
        $this->view->admin = false;
        Zend_Registry::set('params', $this->getRequest()->getParams());
        if (Zend_Auth::getInstance()->hasIdentity()) {
            $user = Zend_Auth::getInstance()->getIdentity();
            $user = Doctrine_Query::create()->from('Contact m')->where('id = ?')->fetchOne(array(
                $user['id']));
            $this->user = $user;
            $this->view->user = $user;
            if ($user != false) {
                foreach ($user->Roles as $rol) {
                    if ($this->highRole == null || $rol->level > $this->highRole->level)
                        $this->highRole = $rol;
                }
            }
            foreach ($user->ShopContact as $shop) {
                if ($this->shop->id == $shop->shop_id && $shop->admin == true) {
                    $this->view->admin = true;
                    $this->view->role = $this->highRole;
                }
            }

            if ($this->shop->uid == '0001-557e6b9d-531afcb3-9700-4e8b7d8a') {

                $budget = json_decode($user->self_information, true);

                $this->view->budget = $budget;
            }
            $this->view->my_articles = 0;
            if ($this->install->id != 7) {
                $this->view->my_articles = Doctrine_Query::create()->from('ContactArticle c')->where('c.contact_id = ?', array($user['id']))->execute()->count();
            }
        }

        if ($this->getRequest()->getParam('sek_admin', false)) {
            $mode = new Zend_Session_Namespace('adminmode');

            if ($this->getRequest()->getParam('sek_admin') == 'new' && $this->view->admin == true) {
                $mode->over_ride_contact = 'new';
                $mode->over_ride_contact_id = "";
            } elseif ($this->getRequest()->getParam('sek_admin') == 'no') {
                $mode->over_ride_contact = false;
                $mode->over_ride_contact_id = "";
            } elseif ($this->view->admin && $this->getRequest()->getParam('sek_admin') == 'user') {

                $row = Doctrine_Query::create()->select()->from('Contact c')->leftJoin('c.Shops s')
                                ->where('s.id = ? AND c.id = ?', array($this->shop->id, $this->getRequest()->getParam('sek_admin_user')))->execute()->count();

                if ($row > 0) {
                    $mode->over_ride_contact = $this->getRequest()->getParam('sek_admin_user');
                    $mode->over_ride_contact_id = $this->getRequest()->getParam('sek_admin_user_id');
                }
            } elseif ($this->user->is_sek && $this->getRequest()->getParam('sek_admin') == 'user') {

                $row = Doctrine_Query::create()->select()->from('Contact c')
                                ->where('c.id = ? AND c.account_id = ?', array($this->getRequest()->getParam('sek_admin_user'), $this->user->account_id))->execute()->count();

                if ($row > 0) {
                    $mode->over_ride_contact = $this->getRequest()->getParam('sek_admin_user');
                    $mode->over_ride_contact_id = $this->getRequest()->getParam('sek_admin_user_id');
                }
            }
        }



        $uri = $this->_request->getPathInfo();
        if(strpos($uri, "/mode")) {
            $activeNav = $container->findAllBy('uri', substr($uri, 0, strpos($uri, "/mode")));
        }else{
            $activeNav = $container->findAllBy('uri', $uri);
        }

        foreach ($activeNav as $an) {
            $an->active = true;
            $an->setClass("active");
        }

        $this->view->activeNav = $uri;

        $this->view->navigation($container);

        if($conf['keywords'] != "") {
            $this->view->headMeta()->setName('keywords', $conf['keywords']);
        }
        if($conf['author'] != "") {
            $this->view->headMeta()->setName('author', $conf['author']);
        }
        if($conf['description'] != "") {
            $this->view->headMeta()->setName('description', $conf['description']);
        }
        if($conf['copyright'] != "") {
            $this->view->headMeta()->setName('copyright', $conf['copyright']);
        }

        if ($this->shop->private && !Zend_Auth::getInstance()->hasIdentity() && $this->getRequest()->getParam('controller') != 'user' && $this->getRequest()->getParam('controller') != 'cms' && $this->getRequest()->getParam('action') != 'login') {
            $this->_redirect('/user/login');
        }

        $conv = new Zend_Session_Namespace('conversion_tracking');

        $conv->unlock();

        if (!$conv->conv)
            $conv->conv = 0;

        if ($conv->conv != 0) {
            $this->view->conversion = $conv->conv;
            $conv->conv = 0;
        }

        $this->view->myfavcount = 0;
        if($this->install->id != 7) {
            if (Zend_Auth::getInstance()->hasIdentity() == true) {
                $motive = Doctrine_Query::create()
                    ->from('ContactMotiv c')
                    ->leftJoin('c.Motiv as d')
                    ->where('d.shop_id = ? AND c.contact_id = ?', array($this->shop->id, $this->user->id))->count();

                $this->view->myfavcount = $motive;
            } else {
                $motivBasket = new TP_FavMotiv();

                $this->view->myfavcount = count($motivBasket->getMotive()) - 1;
            }
        }

        $this->view->inworkCount = 0;
        if($this->install->id != 7) {
            $articleSession = new TP_Layoutersession();
            $this->view->inworkCount = count($articleSession->getAllLayouterArticle());
        }
        $this->view->display_top_modul = $this->shop->display_top_modul;

        if ($this->shop->display_top_modul && $this->shop->top_modul_settings != "") {

            $xml = simplexml_load_string($this->shop->top_modul_settings);

            $modul = array();
            foreach ($xml as $item) {
                $temp = array();
                $limit = 10;
                $sort = 'DESC';

                if (isset($item['count']) && $item['count'] > 0 && $item['count'] < 20)
                    $limit = intval($item['count']);
                if (isset($item['sort']) && $item['sort'] == 'ASC')
                    $sort = 'ASC';

                if (isset($item['query']) && (strpos('t' . strtolower($item['query']), 'update') || strpos('t' . strtolower($item['query']), 'delete') || strpos('t' . strtolower($item['query']), 'replace')))
                    continue;

                $temp['title'] = (string) $item;

                if ($item['modul'] == 'product' && isset($item['mode'])) {

                    switch ($item['mode']) {
                        case "rate":
                            $order = 'ratesum ' . $sort;
                            $temp['link'] = '/overview/all/mode//page/1/sort/rate/' . $sort;
                            break;
                        case "visits":
                            $order = 'c.visits ' . $sort;
                            $temp['link'] = '/overview/all/mode//page/1/sort/visits/' . $sort;
                            break;
                        case "price":
                            $order = 'c.a4_abpreis ' . $sort;
                            $temp['link'] = '/overview/all/mode//page/1/sort/price/' . $sort;
                            break;
                        case "date":
                            $order = 'c.created ' . $sort;
                            $temp['link'] = '/overview/all/mode//page/1/sort/date/' . $sort;
                            break;
                        case "top":
                        default:
                            $order = 'c.used ' . $sort;
                            $temp['link'] = '/overview/all/mode//page/1/sort/buy/' . $sort;
                            break;
                    }

                    if ($item['mode'] == 'sql') {
                        if ($this->shop->pmb) {
                            $articles = Doctrine_Query::create()->query(str_replace('WHERE', 'WHERE a.install_id = ' . $this->shop->install_id . ' AND a.enable = 1 AND a.private = 0 AND', $item['query']));
                        } else {
                            $articles = Doctrine_Query::create()->query(str_replace('WHERE', 'WHERE a.shop_id = ' . $this->shop->id . ' AND a.enable = 1 AND a.private = 0 AND', $item['query']));
                        }
                    } else {
                        if ($this->shop->pmb) {
                            $articles = Doctrine_Query::create()->select('*, (c.rate/c.rate_count) as ratesum')->from('Article c')->where('c.install_id = ? AND c.enable = 1 AND c.private = 0 AND c.enable = 1 AND c.display_market = 1', array($this->shop->install_id))->orderBy($order)->limit($limit)->execute();
                        } else {
                            $articles = Doctrine_Query::create()->select('*, (c.rate/c.rate_count) as ratesum')->from('Article c')->where('c.shop_id = ? AND c.enable = 1 AND c.private = 0 AND c.enable = 1', array($this->shop->id))->orderBy($order)->limit($limit)->execute();
                        }
                    }

                    $temp['type'] = 'product';
                } elseif ($item['modul'] == 'motiv' && isset($item['mode'])) {

                    switch ($item['mode']) {
                        case "rate":
                            $order = 'ratesum ' . $sort;
                            $temp['link'] = '/motiv/page/1/sort/rate/' . $sort;
                            break;
                        case "price":
                            $order = 'c.price1 ' . $sort;
                            $temp['link'] = '/motiv/page/1/sort/price/' . $sort;
                            break;
                        case "date":
                            $order = 'c.created ' . $sort;
                            $temp['link'] = '/motiv/page/1/sort/date/' . $sort;
                            break;
                        case "top":
                        default:
                            $order = 'c.used ' . $sort;
                            $temp['link'] = '/motiv/page/1/sort/buy/' . $sort;
                            break;
                    }

                    if ($item['mode'] == 'sql') {
                        if ($this->shop->pmb) {
                            $articles = Doctrine_Query::create()->query(str_replace('WHERE', 'WHERE m.install_id = ' . $this->shop->install_id . ' AND', $item['query']));
                        } else {
                            $articles = Doctrine_Query::create()->query(str_replace('WHERE', 'WHERE m.shop_id = ' . $this->shop->id . ' AND', $item['query']));
                        }
                    } else {
                        if ($this->shop->pmb) {
                            $articles = Doctrine_Query::create()->select('*, (c.rate/c.rate_count) as ratesum')->from('Motiv c')->where('c.install_id = ? AND c.resale_market = 1', array($this->shop->install_id))->orderBy($order)->limit($limit)->execute();
                        } else {
                            $articles = Doctrine_Query::create()->select('*, (c.rate/c.rate_count) as ratesum')->from('Motiv c')->where('c.shop_id = ? AND c.resale_shop = 1', array($this->shop->id))->orderBy($order)->limit($limit)->execute();
                        }
                    }

                    $temp['type'] = 'motiv';
                } elseif ($item['modul'] == 'shop' && isset($item['mode'])) {

                    switch ($item['mode']) {
                        case "rate":
                            $order = 'ratesum ' . $sort;
                            $temp['link'] = '/market/page/1/sort/rate/' . $sort;
                            break;
                        case "date":
                            $order = 'c.created ' . $sort;
                            $temp['link'] = '/market/page/1/sort/date/' . $sort;
                            break;
                        case "top":
                        default:
                            $order = 'c.views ' . $sort;
                            $temp['link'] = '/market/page/1/sort/visits/' . $sort;
                            break;
                    }

                    if ($item['mode'] == 'sql') {
                        if ($this->shop->pmb) {
                            $articles = Doctrine_Query::create()->query(str_replace('WHERE', 'WHERE s.private = 0 AND s.display_market = 1 AND s.install_id = ' . $this->shop->install_id . ' AND', $item['query']));
                        } else {
                            continue;
                        }
                    } else {
                        if ($this->shop->pmb) {
                            $articles = Doctrine_Query::create()->select('*, (c.rate/c.rate_count) as ratesum')->from('Shop c')->where('c.install_id = ? AND c.display_market = 1 AND c.private = 0 AND c.market = 1 AND c.pmb != 1', array($this->shop->install_id))->orderBy($order)->limit($limit)->execute();
                        } else {
                            continue;
                        }
                    }

                    $temp['type'] = 'shop';
                } else {
                    continue;
                }

                if (isset($item['custom_link']) && $item['custom_link'] != "") {
                    $temp['link'] = $item['custom_link'];
                }

                $temp['rows'] = $articles;
                array_push($modul, $temp);
            }

            $this->view->top_modul = $modul;
        }


        if ($this->_getParam('contact_newsletter')) {

            $filter = new Zend_Validate_EmailAddress();

            if ($filter->isValid($this->_getParam('contact_newsletter'))) {

                if ($this->_getParam('contact_newsletter_delete')) {

                    $entry = Doctrine_Query::create()->select()->from('Contactnewsletter c')->where('c.email = ?  AND c.shop_id = ?', array($this->_getParam('contact_newsletter'), $this->shop->id))->fetchOne();

                    if ($entry) {
                        $entry->delete();
                    }

                    $contacts = Doctrine_Query::create()->from('Contact m')->where('m.self_email = ? AND m.install_id = ?',array(
                        $this->_getParam('contact_newsletter'), $this->shop->install_id))->execute();

                    foreach($contacts as $contact) {
                        $contact->newsletter = false;
                        $contact->self_newsletter = false;
                        $contact->save();
                        TP_Queue::process('newsletter_deactive', 'global', $contact);
                        TP_Queue::process('newsletter_change_status', 'global', $contact);
                    }

                    $this->view->priorityMessenger('Für den Newsletter ausgetragen', 'info');
                } else {

                    $contacts = Doctrine_Query::create()->from('Contact m')->where('m.self_email = ? AND m.install_id = ?',array(
                        $this->_getParam('contact_newsletter'), $this->shop->install_id))->execute();


                    foreach($contacts as $contact) {
                        $contact->newsletter = true;
                        $contact->self_newsletter = true;
                        $contact->save();
                        TP_Queue::process('newsletter_active', 'global', $contact);
                        TP_Queue::process('newsletter_change_status', 'global', $contact);

                        $this->view->priorityMessenger('Für den Newsletter eingetragen', 'info');
                    }

                    if(count($contacts) == 0) {
                    $entry = Doctrine_Query::create()->select()->from('Contactnewsletter c')->where('c.email = ? AND c.shop_id = ?', array($this->_getParam('contact_newsletter'), $this->shop->id))->fetchOne();

                        if ($entry) {
                            $this->view->priorityMessenger('Sie sind schon für den Newsletter eingetragen', 'error');
                        }else{

                            $entry = new Contactnewsletter();
                            $entry->shop_id = $this->shop->id;
                            $entry->install_id = $this->install->id;
                            $entry->email = $this->_getParam('contact_newsletter');
                            $entry->save();
                            $this->view->priorityMessenger('Für den Newsletter eingetragen', 'info');
                        }
                    }
                }
            } else {
                $this->view->priorityMessenger('Bitte gültige Adresse eingeben', 'error');
            }
        }

    }

    protected function getArticleGroups($page, $pageart, $articlegroup, $container)
    {
        $page->addPage(array(
            'label' => $articlegroup->getTitle(),
            'id' => $articlegroup->id,
            'uri' => '/overview/' . $articlegroup->url));
        $pageart->addPage(array(
            'label' => $articlegroup->getTitle(),
            'id' => 'A' . $articlegroup->id,
            'uri' => '/overview/' . $articlegroup->url));
        $articlegroupsd = Doctrine_Query::create()->from('ArticleGroup c')->where('c.shop_id = ? AND c.enable = 1 AND c.parent=?', array(
                    $this->shop->id,
                    $articlegroup->id))->orderBy('c.pos ASC')->execute();
        foreach ($articlegroupsd as $articlegroups) {
            $this->getArticleGroups($container->findOneBy('id', $articlegroup->id), $container->findOneBy('id', 'A' . $articlegroup->id), $articlegroups, $container);
        }
    }

    protected function getArticleThemes($tp, $articletheme, $container)
    {
        $tp->addPage(array(
            'label' => $articletheme->title,
            'id' => 'AT' . $articletheme->id,
            'uri' => '/themes/product/filter/add/' . $articletheme->url));
        $articlethemesd = Doctrine_Query::create()->from('ArticleTheme c')->where('c.shop_id = ? AND c.parent_id=?', array(
                    $this->shop->id,
                    $articletheme->id))->orderBy('c.title ASC')->execute();
        foreach ($articlethemesd as $articlethemes) {
            $this->getArticleThemes($container->findOneBy('id', 'AT' . $articletheme->id), $articlethemes, $container);
        }
    }

    protected function getMotivThemes($tm, $motivetheme, $container)
    {
        $tm->addPage(array(
            'label' => $motivetheme->title,
            'id' => 'MT' . $motivetheme->id,
            'uri' => '/themes/motiv/filter/add/' . $motivetheme->url));
        $motivthemesd = Doctrine_Query::create()->from('MotivTheme c')->where('c.shop_id = ? AND c.parent_id=?', array(
                    $this->shop->id,
                    $motivetheme->id))->orderBy('c.title ASC')->execute();
        foreach ($motivthemesd as $motivthemes) {
            $this->getMotivThemes($container->findOneBy('id', 'MT' . $motivetheme->id), $motivthemes, $container);
        }
    }

    protected function getShopThemes($tm, $shoptheme, $container)
    {
        $tm->addPage(array(
            'label' => $shoptheme->title,
            'id' => 'ST' . $shoptheme->id,
            'uri' => '/themes/shop/filter/add/' . $shoptheme->url));
        $shopthemesd = Doctrine_Query::create()->from('ShopTheme c')->where('c.shop_id = ? AND c.parent_id=?', array(
                    $this->shop->id,
                    $shoptheme->id))->orderBy('c.title ASC')->execute();
        foreach ($shopthemesd as $shopthemes) {
            $this->getShopThemes($container->findOneBy('id', 'ST' . $shoptheme->id), $shopthemes, $container);
        }
    }

    protected function getCms($page, $pagepos, $cms, $container)
    {

        if (strpos('t' . $cms->parameter, 'target') > 0) {
            $page->addPage(array(
                'label' => $cms->menu,
                'target' => '_blank',
                'id' => $cms->pos . '_' . $cms->url,
                'uri' => substr(strrchr($cms->parameter, "="), 1)));
            if ($pagepos != null) {
                $pagepos->addPage(array(
                    'label' => $cms->menu,
                    'id' => $cms->pos . '_' . $cms->url,
                    'target' => '_blank',
                    'uri' => substr(strrchr($cms->parameter, "="), 1)));
            }
        } else {
            $page->addPage(array(
                'label' => $cms->menu,
                'id' => $cms->pos . '_' . $cms->url,
                'uri' => '/cms/' . $cms->url));
            if ($pagepos != null) {
                $pagepos->addPage(array(
                    'label' => $cms->menu,
                    'id' => $cms->pos . '_' . $cms->url,
                    'uri' => '/cms/' . $cms->url));
            }
        }
        $rows = Doctrine_Query::create()->from('Cms m')->where('private = 0 AND enable = 1 AND notinmenu != 1 AND shop_id = ? AND (language = ? OR language = \'all\') AND parent = ?', array(
                    intval($this->shop->id),
                    Zend_Registry::get('locale'),
                    $cms->id))->orderBy('sor ASC')->execute();
        foreach ($rows as $row) {
            $this->getCms($container->findOneBy('label', $cms->menu), $container->findOneBy('id', $cms->pos . '_' . $cms->url), $row, $container);
        }
    }

    protected function setHeadData(Doctrine_Record $row)
    {
        if ($row->meta_keywords != "") {
            $this->view->headMeta()->setName('keywords', $row->meta_keywords);
        }
        if ($row->meta_author != "") {
            $this->view->headMeta()->setName('author', $row->meta_author);
        }
        if ($row->meta_description != "") {
            $this->view->headMeta()->setName('description', $row->meta_description);
        }

        if ($row->meta_custom_title != "") {
            $this->view->page_title = $row->meta_custom_title;
        } else {
            $this->view->page_title = $row->title;
        }

        $this->view->doctype()->setDoctype(Zend_View_Helper_Doctype::XHTML1_RDFA);
        if ($row->meta_og_title != "") {
            $this->view->headMeta()->setProperty('og:title', $row->meta_og_title);
        } else {
            $this->view->headMeta()->setProperty('og:title', $this->view->page_title);
        }
        if ($row->meta_og_type != "") {
            $this->view->headMeta()->setProperty('og:type', $row->meta_og_type);
        }
        if ($row->meta_og_url != "") {
            $this->view->headMeta()->setProperty('og:url', $row->meta_og_url);
        }
        if ($row->meta_og_image != "") {
            $this->view->headMeta()->setProperty('og:image', $row->meta_og_image);
        } elseif ($this->shop->logo1 != "") {
            $this->view->headMeta()->setProperty('og:image', $this->view->basepath . '/' . $this->view->image()->thumbnailImage('logo', 'logo1', $this->shop->logo1, true, true));
        }
        if ($row->meta_og_description != "") {
            $this->view->headMeta()->setProperty('og:description', $row->meta_og_description);
        } elseif ($row->meta_description != "") {
            $this->view->headMeta()->setProperty('og:description', $row->meta_description);
        }

    }

    protected function getAccountTemplatePath($account_id) {
        $account = Doctrine_Query::create()->from('Account c')->where('c.id = ?', array($account_id))->fetchOne();
        if($account['template_switch'] != "") {
            return $account['template_switch'];
        }
        if($account['filiale_id'] != "" && $account['filiale_id'] != 0) {
            return $this->getAccountTemplatePath($account['filiale_id']);
        }
        return "";
    }

    public function getUser() {
        return $this->user;
    }

}
