<?php

class TP_Controller_Request_Cli extends Zend_Controller_Request_Abstract
{
    public function __construct()
    {
        $opts = new Zend_Console_Getopt(
            array(
                'module|m-s'     => 'module name',
                'controller|c-s' => 'controller name',
                'action|a-s'     => 'action name',
                'server|h-s'     => 'server name'
            )
        );

        $params = array_merge($opts->getOptions(), $opts->getRemainingArgs());
 
        foreach ($params as $param) {
            $this->setParam($param, $opts->getOption($param));
        }
        return $this;
    }


}