<?php


class TP_Controller_Request_Http extends Zend_Controller_Request_Http {

    protected $filter = array('lang' => array('Alnum','allowEmpty' => true),
                                'id' => array('Digits','allowEmpty' => true),
                                'limit' => array('Digits','allowEmpty' => true),
                                'start' => array('Digits','allowEmpty' => true),
                                'pass' => array('Alnum','allowEmpty' => true)
                        );

    protected $filterWhiteList = array('error_handler');

    public function getParam($key, $default = null)
    {

        if(in_array($key, $this->filterWhiteList)) {
            return parent::getParam($key, $default);
        }
        if (!isset($this->filter[$key])) {
            throw new Zend_Exception('Param is not filtered or validated: '.$key);
        }
        $input = new Zend_Filter_Input(null, $this->filter, array($key => parent::getParam($key, $default)));
        if($input->isValid($key)) {
            return $input->getUnescaped($key);
        }else{
            throw new Zend_Exception('Error with param filter and validation: '.$key);
            
        }
    }

    public function addFilter($filter) {
        $this->filter = array_merge_recursive($filter, $this->filter);
    }
}