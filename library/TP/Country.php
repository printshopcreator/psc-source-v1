<?php

class TP_Country {

    public static function getCountrysAsArray() {

        $shop = Zend_Registry::get('shop');
        $statusCollection = TP_Mongo::getInstance()->Country;

        $countrys = $statusCollection->find(array('shop' => (string)$shop['id']));
        $temp = [];
        foreach($countrys as $country) {
            $temp[$country['code']] = Zend_Locale::getTranslation($country['code'], 'territory');
        }

        return $temp;

    }

    public static function getTaxForCountry($code, $ustid)
    {
        if($code === NULL || $code == '') {
            return true;
        }
        $shop = Zend_Registry::get('shop');
        $statusCollection = TP_Mongo::getInstance()->Country;

        $country = $statusCollection->findOne(array('shop' => (string)$shop['id'], 'code' => $code));

        if($country === NULL) {
            return true;
        }

        if($ustid == NULL || $ustid == "") {
            return $country['withTaxWithoutUstNr'];
        }else{
            return $country['withTaxWithUstNr'];

        }
    }

}