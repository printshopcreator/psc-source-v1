<?php

class TP_Crypt {

    protected static $key = "p489r7nwpefsnfp0";
    
    /** Encryption Procedure
     *
    */
    public static function encrypt( $data) {
        $crypttext = openssl_encrypt(http_build_query($data), "AES-128-ECB", self::$key);
        if ((isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on') || (isset($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] == 'https')) {
            return str_replace("+", "PLUS", base64_encode($crypttext)) . "&ARTHOST=".urlencode("https://".$_SERVER["SERVER_NAME"]);
        }
        return str_replace("+", "PLUS", base64_encode($crypttext)) . "&ARTHOST=".urlencode("http://".$_SERVER["SERVER_NAME"]);
    }

    /** Encryption Procedure
     *
     */
    public static function encryptDocker( $data, $url = "service/template", $saveHost = "http://web") {
        $crypttext = openssl_encrypt	 (http_build_query($data), "AES-128-ECB", self::$key);
        if ((isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on') || (isset($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] == 'https')) {
            return str_replace("+", "PLUS", base64_encode($crypttext)) . "&URL=".urlencode($url) . "&WEBHOST=".urlencode("https://".$_SERVER["SERVER_NAME"]) . "&SAVEHOST=" . urlencode($saveHost );
        }
        return str_replace("+", "PLUS", base64_encode($crypttext)) . "&URL=".urlencode($url) . "&WEBHOST=".urlencode("http://".$_SERVER["SERVER_NAME"]) . "&SAVEHOST=". urlencode($saveHost );
    }

    /** Decryption Procedure
    */
    public static function decrypt( $enc ) {

        $str = base64_decode(str_replace("PLUS", "+", $enc));
        $decrypttext = openssl_decrypt($str, "AES-128-ECB", self::$key);
        parse_str($decrypttext, $output);
        return $output;
    }

}