<?php

class TP_Date_Holidays {

    protected $type;
    
    
    public static function factory($type) {
        $obj = new TP_Date_Holidays($type);
        $obj = $obj->getClass();
        return $obj;
    }


    public function __construct($type = 'Germany') {
        $this->type = $type;
    }
    
    
    public function __invoke($type = 'Germany') {
        $this->type = $type;
    }
    
    public function getClass() {
        $class = 'TP_Date_Holidays_Driver_'.$this->type;
        if(class_exists($class)) {
            return new $class();
        }elseif(file_exists(__DIR__ . '/Holidays/Driver/' . $this->type . '.php')) {
            require_once __DIR__ . '/Holidays/Driver/' . $this->type . '.php';
            return new $class();
        }
        throw new InvalidArgumentException('No Valid Country: '.__DIR__ . '/Holidays/Driver/' . $this->type . '.php');
    }
    
}