<?php

class TP_Date_Holidays_Driver {
    
    
    protected $_actDate;
    
    protected $_holidays = array();
    
    protected $_year;
    
    protected $_day;
    
    protected $_month;
    
    protected $_holidaysAreBuild = false;
    
    protected $_filter = NULL;
    
    
    public function __construct() {
        $this->_actDate = new DateTime();
    }
    
    protected function _buildHolidays() {
        
    }
    
    public function setFilter(TP_Date_Holidays_Filter $filter) {
        $this->_filter = $filter;
    }
    
    public function setDate($date) {
        if(is_object($date) && $date instanceof DateTime){
            $this->_actDate = $date;
        }else{
            try{
                $this->_actDate = new DateTime($date);
            }catch(Exception $e) {
                throw new InvalidArgumentException($e->getMessage());
            }
        }
        $this->_year = $this->_actDate->format('Y');
        $this->_mounth = $this->_actDate->format('m');
        $this->_day = $this->_actDate->format('d');
    }
    
    
    protected function _addHoliday(TP_Date_Holidays_Item $item)
    {
        $this->_holidays[$item->getId()] = $item;
    }
    
    protected function getHolidayDate($internalName)
    {
        $this->checkBuild();
        
        if(isset($this->_holidays[$internalName])) {
            return $this->_holidays[$internalName]->getDate();
        }
    }
    
    public function getHoliday($internalName)
    {
        $this->checkBuild();
        
        if(isset($this->_holidays[$internalName])) {
            return $this->_holidays[$internalName];
        }
        throw new InvalidArgumentException($internalName . ' not exists');
    }
    
    public function isHoliday()
    {
        
        $this->checkBuild();
        
        foreach($this->_holidays as $key => $item) {
            if(isset($this->_filter) && !$this->_filter->accept($key)) continue;
            if($item->isHoliday($this->_actDate)) {
                return true;
            }
        }
        
        return false;
        
    }
    
    public function getHolidayForDate($multi = false) {
        $this->checkBuild();
        
        $holidays = array();
        
        foreach($this->_holidays as $key => $item) {
            if(isset($this->_filter) && !$this->_filter->accept($key)) continue;
            if($item->isHoliday($this->_actDate)) {
                $holidays[] = $item;
                if(!$multi) {
                    return $item;
                }
            }
        }
        return $holidays;
    }
    
    protected function checkBuild()
    {
        if(!$this->_holidaysAreBuild)
        {
            $this->_holidaysAreBuild = true;
            $this->_buildHolidays();
        }
    }
    
}