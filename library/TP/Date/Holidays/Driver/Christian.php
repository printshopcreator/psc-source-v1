<?php

class TP_Date_Holidays_Driver_Christian extends TP_Date_Holidays_Driver {

    
    public function _buildHolidays() {
     
        /**
         * Circumcision of Jesus
         */
        $this->_addHoliday(new TP_Date_Holidays_Item('jesusCircumcision', $this->_year . '-01-01', 'Circumcision of Jesus'));     
        
        /**
         * Epiphanias
         */
        $this->_addHoliday(new TP_Date_Holidays_Item('epiphany', $this->_year . '-01-06', 'Epiphany'));

        /**
         * Cleaning of Mari√§
         */
        $this->_addHoliday(new TP_Date_Holidays_Item('mariaCleaning', $this->_year . '-02-02', 'Cleaning of Maria'));

        /**
         * Josef's Day
         */
        $this->_addHoliday(new TP_Date_Holidays_Item('josefsDay', $this->_year . '-03-19', 'Josef\'s Day'));

        /**
         * Maria Announcement
         */
        $this->_addHoliday(new TP_Date_Holidays_Item('mariaAnnouncement', $this->_year . '-03-25', 'Maria Announcement'));
        
        /**
         * Easter Sunday
         */
        $easterDate = $this->calcEaster();
        $this->_addHoliday(new TP_Date_Holidays_Item('easter', $easterDate, 'Easter Sunday'));
        
        /**
         * Palm Sunday
         */
        $palmSundayDate = $this->calcDays($easterDate, '-7 days');
        $this->_addHoliday(new TP_Date_Holidays_Item('palmSunday', $palmSundayDate, 'Palm Sunday'));

        /**
         * Passion Sunday
         */
        $passionSundayDate = $this->calcDays($palmSundayDate, '-7 days');
        $this->_addHoliday(new TP_Date_Holidays_Item('passionSunday', $passionSundayDate, 'Passion Sunday'));

        /**
         * Painful Friday
         */
        $painfulFridayDate = $this->calcDays($palmSundayDate, '-2 days');
        $this->_addHoliday(new TP_Date_Holidays_Item('painfulFriday', $painfulFridayDate, 'Painful Friday'));

        /**
         * White Sunday
         */
        $whiteSundayDate = $this->calcDays($easterDate, '+7 days');
        $this->_addHoliday(new TP_Date_Holidays_Item('whiteSunday', $whiteSundayDate, 'White Sunday'));

        /**
         * Ash Wednesday
         */
        $ashWednesdayDate = $this->calcDays($easterDate, '-46 days');
        $this->_addHoliday(new TP_Date_Holidays_Item('ashWednesday', $ashWednesdayDate, 'Ash Wednesday'));

        /**
         * Good Friday / Black Friday
         */
        $goodFridayDate = $this->calcDays($easterDate, '-2 days');
        $this->_addHoliday(new TP_Date_Holidays_Item('goodFriday', $goodFridayDate, 'Good Friday'));

        /**
         * Green Thursday
         */
        $this->_addHoliday(new TP_Date_Holidays_Item('greenThursday', $this->calcDays($goodFridayDate, '-1 days'), 'Green Thursday'));
             
        /**
         * Easter Monday
         */
        $this->_addHoliday(new TP_Date_Holidays_Item('easterMonday', $this->calcDays($easterDate, '+1 days'), 'Easter Monday'));
            
        /**
         * Whitsun (determines Whit Monday, Ascension Day and
         * Feast of Corpus Christi)
         */
        $whitsunDate = $this->calcDays($easterDate, '+49 days');
        $this->_addHoliday(new TP_Date_Holidays_Item('whitsun', $whitsunDate, 'Whitsun'));

        /**
         * Request Sunday
         */
        $requestSunday = $this->calcDays($whitsunDate, '-14 days');
        $this->_addHoliday(new TP_Date_Holidays_Item('requestSunday', $requestSunday, 'Request Sunday'));

        /**
         * Ascension Day
         */
        $ascensionDayDate = $this->calcDays($whitsunDate, '-10 days');
        $this->_addHoliday(new TP_Date_Holidays_Item('ascensionDay', $ascensionDayDate, 'Ascension Day'));

        /**
         * Whit Monday
         */
        $this->_addHoliday(new TP_Date_Holidays_Item('whitMonday', $this->calcDays($whitsunDate, '+1 days'), 'Whit Monday'));

        /**
         * Haunting of Mari√§
         */
        $this->_addHoliday(new TP_Date_Holidays_Item('mariaHaunting', $this->_year . '-05-31', 'Haunting of Maria'));

        /**
         * Trinitatis
         */
        $trinitatisDate = $this->calcDays($whitsunDate, '+7 days');
        $this->_addHoliday(new TP_Date_Holidays_Item('trinitatis', $trinitatisDate, 'Trinitatis'));

        /**
         * Feast of Corpus Christi
         */
        $corpusChristiDate = $this->calcDays($whitsunDate, '+11 days');
        $this->_addHoliday(new TP_Date_Holidays_Item('corpusChristi', $corpusChristiDate, 'Feast of Corpus Christi'));

        /**
         * Heart of Jesus
         *
         * Friday of the 3rd week past Whitsun
         */
        $heartJesusDate = $this->calcDays($whitsunDate, '+19 days');
        $this->_addHoliday(new TP_Date_Holidays_Item('heartJesus', $heartJesusDate, 'Heart of Jesus celebration'));

        /**
         * Johannis celebration
         */
        $this->_addHoliday(new TP_Date_Holidays_Item('johannisCelebration', $this->_year . '-06-24', 'Johannis celebration'));

        /**
         * Petrus and Paulus
         */
        $this->_addHoliday(new TP_Date_Holidays_Item('petrusAndPaulus', $this->_year . '-06-29', 'Petrus and Paulus'));

        /**
         * Ascension of Maria
         */
        $this->_addHoliday(new TP_Date_Holidays_Item('mariaAscension', $this->_year . '-08-15', 'Ascension of Maria'));

        /**
         * Celebration of raising the Cross
         */
        $this->_addHoliday(new TP_Date_Holidays_Item('crossRaising', $this->_year . '-09-14', 'Celebration of raising the Cross'));

        /**
         * Thanks Giving
         *
         * Sunday past Michaelis (29. September)
         */
        $michaelisDate = new DateTime($this->_year . '-09-29');
        $dayOfWeek     = $michaelisDate->format('w');
        $michaelisDate = $this->calcDays($michaelisDate, '+'. 7 - $dayOfWeek . ' days');
        $thanksGivingDate = $michaelisDate;
        $this->_addHoliday(new TP_Date_Holidays_Item('thanksGiving', $thanksGivingDate, 'Thanks Giving'));

        /**
         * Kermis
         *
         * 3rd Sunday in October
         */
        $kermisDate = new DateTime($this->_year . '-10-01');
        $dayOfWeek  = $kermisDate->format('w');
        if ($dayOfWeek != 0) {
            $kermisDate = $this->calcDays($kermisDate, '+'. 7 - $dayOfWeek . ' days');
        }
        $kermisDate = $this->calcDays($kermisDate, '+14 days');
        $this->_addHoliday(new TP_Date_Holidays_Item('kermis', $kermisDate, 'Kermis'));

        /**
         * Reformation Day
         */
        $this->_addHoliday(new TP_Date_Holidays_Item('reformationDay', $this->_year . '-10-31', 'Reformation Day'));

        /**
         * All Saints' Day
         */
        $this->_addHoliday(new TP_Date_Holidays_Item('allSaintsDay', $this->_year . '-11-01', 'All Saints\' Day'));

        /**
         * All Souls' Day
         */
        $this->_addHoliday(new TP_Date_Holidays_Item('allSoulsDay', $this->_year . '-11-02', 'All Souls\' Day'));

        /**
         * Martin's Day
         */
        $this->_addHoliday(new TP_Date_Holidays_Item('martinsDay', $this->_year . '-11-11', 'Martin\'s Day'));

        /**
         * 4th Advent
         */
        $Advent4Date = new DateTime($this->_year . '-12-25');
        $dayOfWeek   = $Advent4Date->format('w');
        if ($dayOfWeek == 0) {
            $dayOfWeek = 7;
        }
        $Advent4Date = $this->calcDays($Advent4Date, '-' . $dayOfWeek .' days');
        $this->_addHoliday(new TP_Date_Holidays_Item('advent4', $Advent4Date, '4th Advent'));

        /**
         * 1st Advent
         */
        $Advent1Date = $this->calcDays($Advent4Date, '-21 days');
        $this->_addHoliday(new TP_Date_Holidays_Item('advent1', $Advent1Date, '1st Advent'));

        /**
         * 2nd Advent
         */
        $Advent2Date = $this->calcDays($Advent4Date, '-14 days');
        $this->_addHoliday(new TP_Date_Holidays_Item('advent2', $Advent2Date, '2nd Advent'));

        /**
         * 3rd Advent
         */
        $Advent3Date = $this->calcDays($Advent4Date, '-7 days');
        $this->_addHoliday(new TP_Date_Holidays_Item('advent3', $Advent3Date, '3rd Advent'));

        /**
         * Death' Sunday
         */
        $deathSundayDate = $this->calcDays($Advent1Date, '-7 days');
        $this->_addHoliday(new TP_Date_Holidays_Item('deathSunday', $deathSundayDate, 'Death\' Sunday'));

        /**
         * Day of Repentance
         */
        $dayOfRepentance = $this->calcDays($deathSundayDate, '-4 days');
        $this->_addHoliday(new TP_Date_Holidays_Item('dayOfRepentance', $dayOfRepentance, 'Day of Repentance'));
        
        
        /**
         * St. Nicholas' Day
         */
        $this->_addHoliday(new TP_Date_Holidays_Item('stNicholasDay', $this->_year . '-12-06', 'St. Nicholas\' Day'));

        /**
         * Maria' conception
         */
        $this->_addHoliday(new TP_Date_Holidays_Item('mariaConception', $this->_year . '-12-08', 'Conception of Maria'));
        
        /**
         * Christmas Eve
         */
        $this->_addHoliday(new TP_Date_Holidays_Item('christmasEve', $this->_year . '-12-24', 'Christmas Eve'));

        /**
         * Christmas day
         */
        $this->_addHoliday(new TP_Date_Holidays_Item('christmasDay', $this->_year . '-12-25', 'Christmas Day'));
        
        /**
         * Boxing day
         */
        $this->_addHoliday(new TP_Date_Holidays_Item('boxingDay', $this->_year . '-12-26', 'Boxing Day'));

        /**
         * New Year's Eve
         */
        $this->_addHoliday(new TP_Date_Holidays_Item('newYearsEve', $this->_year . '-12-31', 'New Year\'s Eve'));
        
    }
 
    protected function calcEaster()
    {
        $date = new DateTime("$this->_year-03-21");
        return $date->modify("+ " . easter_days($this->_year) . " days");
        
    }
    
    protected function calcDays(DateTime $dt, $days)
    {
        $df = clone $dt;
        $df->modify($days);
        return $df;
    }
}
