<?php

class TP_Date_Holidays_Driver_Germany extends TP_Date_Holidays_Driver_Christian {

    
    public function _buildHolidays() {
     
        parent::_buildHolidays();
        
        $easterDate       = $this->getHolidayDate('easter');
        $ashWednesdayDate = $this->getHolidayDate('ashWednesday');
        $ascensionDayDate = $this->getHolidayDate('ascensionDay');
        $advent1Date      = $this->getHolidayDate('advent1');
        
        /**
         * New Year's Day
         */
        $this->_addHoliday(new TP_Date_Holidays_Item('newYearsDay', $this->_year . '-01-01', 'New Year\'s Day'));

        /**
         * Valentine's Day
         */
        $this->_addHoliday(new TP_Date_Holidays_Item('valentinesDay', $this->_year . '-02-14', 'Valentine\'s Day'));

        /**
         * "Weiberfastnacht"
         */
        $wFasnetDate = $this->calcDays($ashWednesdayDate, '-6 days');
        $this->_addHoliday(new TP_Date_Holidays_Item('womenFasnet', $wFasnetDate, 'Carnival'));

        /**
         * Carnival / "Fastnacht"
         */
        $fasnetDate = $this->calcDays($easterDate, '-47 days');
        $this->_addHoliday(new TP_Date_Holidays_Item('fasnet', $fasnetDate, 'Carnival'));

        /**
         * Rose Monday
         */
        $roseMondayDate = $this->calcDays($easterDate, '-48 days');
        $this->_addHoliday(new TP_Date_Holidays_Item('roseMonday', $roseMondayDate, 'Rose Monday'));

        /**
         * International Women's Day
         */
        $this->_addHoliday(new TP_Date_Holidays_Item('womensDay', $this->_year . '-03-08', 'International Women\'s Day'));

        /**
         * April 1st
         */
        $this->_addHoliday(new TP_Date_Holidays_Item('april1st', $this->_year . '-04-01', 'April 1st'));
        
        /**
         * Girls' Day (fourth Thursday in April)
         */
        $girlsDayDate = new DateTime($this->_year . '-04-01');
        $dayOfWeek    = $girlsDayDate->format('w');
        switch ($dayOfWeek) {
        case 0:
        case 1:
        case 2:
        case 3:
            $girlsDayDate = $this->calcDays($girlsDayDate, '+'. 4 - $dayOfWeek + 21 . ' days');
            break;
        case 4:
            $girlsDayDate = $this->calcDays($girlsDayDate, '+21 days');
            break;
        case 5:
        case 6:
            $girlsDayDate = $this->calcDays($girlsDayDate, '-' . $dayOfWeek + 11 + 21 . ' days');
            break;
        }
        $this->_addHoliday(new TP_Date_Holidays_Item('girlsDay', $girlsDayDate, 'Girls\' Day'));

        /**
         * International Earth' Day
         */
        $this->_addHoliday(new TP_Date_Holidays_Item('earthDay', $this->_year . '-04-22', 'International Earth\' Day'));

        /**
         * German Beer's Day
         */
        $this->_addHoliday(new TP_Date_Holidays_Item('beersDay', $this->_year . '-04-23', 'German Beer\'s Day'));

        /**
         * Walpurgis Night
         */
        $this->_addHoliday(new TP_Date_Holidays_Item('walpurgisNight', $this->_year . '-04-30', 'Walpurgis Night'));

        /**
         * Day of Work
         */
        $this->_addHoliday(new TP_Date_Holidays_Item('dayOfWork', $this->_year . '-05-01', 'Day of Work'));
        
        /**
         * World's Laughing Day
         */
        $laughingDayDate = new DateTime($this->_year . '-05-01');
        while ($laughingDayDate->format('w') != 0) {
            $laughingDayDate->modify('+1 days');
        }
        $this->_addHoliday(new TP_Date_Holidays_Item('laughingDay', $laughingDayDate, 'World\'s Laughing Day'));

        /**
         * Europe Day
         */
        $this->_addHoliday(new TP_Date_Holidays_Item('europeDay', $this->_year . '-05-05', 'Europe Day'));

        /**
         * Mothers' Day
         */
        $mothersDay = $this->calcDays($laughingDayDate, '+7 days');
        $this->_addHoliday(new TP_Date_Holidays_Item('mothersDay', $mothersDay, 'Mothers\' Day'));

        /**
         * End of World War 2 in Germany
         */
        $this->_addHoliday(new TP_Date_Holidays_Item('endOfWWar2', $this->_year . '-05-08', 'End of World War 2 in Germany'));

        /**
         * Fathers' Day
         */
        $this->_addHoliday(new TP_Date_Holidays_Item('fathersDay', $ascensionDayDate, 'Fathers\' Day'));

        /**
         * Amnesty International Day
         */
        $this->_addHoliday(new TP_Date_Holidays_Item('aiDay', $this->_year . '-05-28', 'Amnesty International Day'));

        /**
         * International Children' Day
         */
        $this->_addHoliday(new TP_Date_Holidays_Item('intChildrenDay', $this->_year . '-06-01', 'International Children\'s Day'));

        /**
         * Day of organ donation
         */
        $organDonationDate = new DateTime($this->_year . '-06-01');
        while ($organDonationDate->format('w') != 6) {
            $organDonationDate->modify('+1 days');
        }
        $this->_addHoliday(new TP_Date_Holidays_Item('organDonationDay', $organDonationDate, 'Day of organ donation'));

        /**
         * Dormouse' Day
         */
        $this->_addHoliday(new TP_Date_Holidays_Item('dormouseDay', $this->_year . '-06-27', 'Dormouse\' Day'));

        /**
         * Christopher Street Day
         */
        $this->_addHoliday(new TP_Date_Holidays_Item('christopherStreetDay', $this->_year . '-06-27', 'Christopher Street Day'));

        /**
         * Hiroshima Commemoration Day
         */
        $this->_addHoliday(new TP_Date_Holidays_Item('hiroshimaCommemorationDay', $this->_year . '-08-06', 'Hiroshima Commemoration Day'));

        /**
         * Augsburg peace celebration
         */
        $this->_addHoliday(new TP_Date_Holidays_Item('augsburgPeaceCelebration', $this->_year . '-08-08', 'Augsburg peace celebration'));

        /**
         * International left-handeds' Day
         */
        $this->_addHoliday(new TP_Date_Holidays_Item('leftHandedDay', $this->_year . '-08-13', 'International left-handeds\' Day'));

        /**
         * Anti-War Day
         */
        $this->_addHoliday(new TP_Date_Holidays_Item('antiWarDay', $this->_year . '-09-01', 'Anti-War Day'));

        /**
         * Day of German Language
         */
        $germanLangDayDate = new DateTime($this->_year . '-09-01');
        while ($germanLangDayDate->format('w') != 6) {
            $germanLangDayDate->modify('+1 days');
        }
        $germanLangDayDate->modify('+7 days');
        $this->_addHoliday(new TP_Date_Holidays_Item('germanLanguageDay', $germanLangDayDate, 'Day of German Language'));

        /**
         * International diabetes day
         */
        $this->_addHoliday(new TP_Date_Holidays_Item('diabetesDay', $this->_year . '-11-14', 'International diabetes day'));

        /**
         * German Unification Day
         */
        $this->_addHoliday(new TP_Date_Holidays_Item('germanUnificationDay', $this->_year . '-10-03', 'German Unification Day'));

        /**
         * Libraries' Day
         */
        $this->_addHoliday(new TP_Date_Holidays_Item('librariesDay', $this->_year . '-10-24', 'Libraries\' Day'));

        /**
         * World's Savings Day
         */
        $this->_addHoliday(new TP_Date_Holidays_Item('savingsDay', $this->_year . '-10-30', 'World\'s Savings Day'));

        /**
         * Halloween
         */
        $this->_addHoliday(new TP_Date_Holidays_Item('halloween', $this->_year . '-10-31', 'Halloween'));

        /**
         * Stamp's Day
         *
         * year <= 1948: 7th of January
         * year > 1948: last Sunday in October
         */
        $stampsDayDate = null;
        if ($this->_year <= 1948) {
            $stampsDayDate = new DateTime($this->_year . '-01-07');
            while ($stampsDayDate->format('w') != 0) {
                $stampsDayDate->modify('+1 days');
            }
        } else {
            $stampsDayDate = new DateTime($this->_year . '-10-31');
            while ($stampsDayDate->format('w') != 0) {
                $stampsDayDate->modify('-1 days');
            }
        }
        $this->_addHoliday(new TP_Date_Holidays_Item('stampsDay', $stampsDayDate, 'Stamp\'s Day'));

        /**
         * International Men's Day
         */
        $this->_addHoliday(new TP_Date_Holidays_Item('mensDay', $this->_year . '-11-03', 'International Men\'s Day'));

        /**
         * Fall of the Wall of Berlin
         */
        $this->_addHoliday(new TP_Date_Holidays_Item('wallOfBerlin', $this->_year . '-11-09', 'Fall of the Wall of Berlin 1989'));

        /**
         * Beginning of the Carnival
         */
        $this->_addHoliday(new TP_Date_Holidays_Item('carnivalBeginning', $this->_year . '-11-11', 'Beginning of the Carnival'));

        /**
         * People's Day of Mourning
         */
        $dayOfMourning = $this->calcDays($advent1Date, '-14 days');
        $this->_addHoliday(new TP_Date_Holidays_Item('dayOfMourning', $dayOfMourning, 'People\'s Day of Mourning'));
    }
    
}
