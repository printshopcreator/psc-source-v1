<?php

class TP_Date_Holidays_Filter
{
    /**
     * Internal names of holidays that are subject to the filter.
     *
     * @access   private
     * @var      array
     */
    var $_internalNames = array();

    /**
     * Constructor.
     *
     * Creates a new filter that knows, which holidays the
     * calculating driver shall be restricted to.
     *
     * @param array $holidays numerical array that contains internal
     *                          names of holidays
     */
    function __construct($holidays)
    {
        if (! is_array($holidays)) {
            $holidays = array();
        }

        $this->_internalNames = $holidays;
    }

    /**
     * Constructor.
     *
     * Creates a new filter that knows, which holidays the
     * calculating driver shall be restricted to.
     *
     * @param array $holidays numerical array that contains internal
     *                          names of holidays
     */
    function Date_Holidays_Filter($holidays)
    {
        $this->__construct($holidays);
    }

    /**
     * Returns the internal names of holidays that are subject to the filter.
     *
     * @return array
     */
    function getFilteredHolidays()
    {
        return $this->_internalNames;
    }

    /**
     * Sets the internal names of holidays that are subject to the filter.
     *
     * @param array $holidays internal holiday-names
     *
     * @return void
     */
    function setFilteredHolidays($holidays)
    {
        if (! is_array($holidays)) {
            $holidays = array();
        }

        $this->_internalNames = $holidays;
    }

    /**
     * Lets the filter decide whether a holiday shall be processed or not.
     *
     * @param string $internalName a holidays' internal name
     *
     * @abstract
     * @return   boolean true, if a holidays shall be processed, false otherwise
     */
    function accept($internalName)
    {
    }
}