<?php

class TP_Date_Holidays_Filter_Germany_MecklenburgWesternPomerania extends TP_Date_Holidays_Filter_Whitelist
{
    /**
     * Constructor.
     */
    function __construct()
    {
        parent::__construct(array('newYearsDay',
                                  'goodFriday',
                                  'easterMonday',
                                  'dayOfWork',
                                  'ascensionDay',
                                  'whitMonday',
                                  'germanUnificationDay',
                                  'reformationDay',
                                  'christmasDay',
                                  'boxingDay'));
    }

}
