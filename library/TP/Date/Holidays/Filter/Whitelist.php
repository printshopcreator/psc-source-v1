<?php

class TP_Date_Holidays_Filter_Whitelist extends TP_Date_Holidays_Filter
{
    /**
     * Constructor.
     *
     * Creates a filter which has knowledge about the
     * holidays that driver-calculations are limited to.
     *
     * @param array $holidays numerical array containing internal names of holidays
     */
    function __construct($holidays)
    {
        parent::__construct($holidays);
    }

    /**
     * Constructor.
     *
     * @param array $holidays numerical array containing internal names of holidays
     *
     * @return void
     */
    function Date_Holidays_Filter_Whitelist($holidays)
    {
        $this->__construct($holidays);
    }

    /**
     * Lets the filter decide whether a holiday shall be processed or not.
     *
     * @param string $holiday a holidays' internal name
     *
     * @return   boolean true, if a holidays shall be processed, false otherwise
     */
    function accept($holiday)
    {
        return (in_array($holiday, $this->_internalNames));
    }
}