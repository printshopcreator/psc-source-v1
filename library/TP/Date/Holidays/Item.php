<?php

class TP_Date_Holidays_Item {
    
    private $_id = "";
    private $_name = "";
    private $_date;
    
    public function __construct($id, $date, $name) {
        $this->_id   = $id;
        $this->_name = $name;
        if(is_string($date)) {
            $this->_date = new DateTime($date);
        }else{
            $this->_date = $date;
        }
    }
    
    public function getId() {
        return $this->_id;
    }
    
    public function getName() {
        return $this->_name;
    }
    
    public function getDate() {
        return $this->_date;
    }
    
    public function getDay() {
        return $this->_date->format('d');
    }
    
    public function getMonth() {
        return $this->_date->format('m');
    }
    
    public function getYear() {
        return $this->_date->format('Y');
    }
    
    public function isHoliday(DateTime $date) {
        if($this->_date == $date) {
            return true;
        }
        return false;
    }
}
