<?php

class TP_Delivery extends DateTime {
    
    
    public function getDeliveryDate($date = 4) {
        $obj = TP_Date_Holidays::factory('Germany');
        $obj->setFilter(new TP_Date_Holidays_Filter_Germany_MecklenburgWesternPomerania());
        for($i=1; $i<=$date; $i++) {
            $this->modify('+1day');
            $obj->setDate($this);
            if($obj->isHoliday() || $this->checkSaturdaySunday()) {
                $date++;
            }
        }
        return $this;
    }

    public function getDeliveryDateBack($date = 4) {
        $obj = TP_Date_Holidays::factory('Germany');
        $obj->setFilter(new TP_Date_Holidays_Filter_Germany_MecklenburgWesternPomerania());
        for($i=1; $i<=$date; $i++) {
            $this->modify('-1day');
            $obj->setDate($this);
            if($obj->isHoliday() || $this->checkSaturdaySunday()) {
                $date++;
            }
        }
        return $this;
    }
    
    public function checkSaturdaySunday() {
        
        if($this->format('w') == 0 || $this->format('w') == 6)
        {
            return true;
        }
        return false;
    }
    
}
