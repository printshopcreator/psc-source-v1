<?php

class TP_Doctrine_Pager_Arrows extends Doctrine_Pager_Layout
{

    public function display($options = array(), $return = false)
    {
        $pager = $this->getPager();
        $str = '';

        if($this->firstName != "") {
        // First page
            $this->addMaskReplacement('page', $this->firstName, true);
            $options['page_number'] = $pager->getFirstPage();

            $selectedTemplate = $this->getSelectedTemplate();
            $this->setSelectedTemplate($this->getFirstNameSelectedTemplate());

            $str .= $this->processPage($options);

            $this->setSelectedTemplate($selectedTemplate);
        }

        if($this->previousName != "") {
            // Previous page
            $this->addMaskReplacement('page', $this->previousName, true);
            $options['page_number'] = $pager->getPreviousPage();

            $selectedTemplate = $this->getSelectedTemplate();
            $this->setSelectedTemplate($this->getPreviousNameSelectedTemplate());

            $str .= $this->processPage($options);

            $this->setSelectedTemplate($selectedTemplate);
        }

        // Pages listing
        $this->removeMaskReplacement('page');
        $str .= parent::display($options, true);

        if($this->nextName != "") {
            // Next page

            $this->addMaskReplacement('page', $this->nextName, true);
            $options['page_number'] = $pager->getNextPage();

            $selectedTemplate = $this->getSelectedTemplate();
            $this->setSelectedTemplate($this->getNextNameSelectedTemplate());

            $str .= $this->processPage($options);

            $this->setSelectedTemplate($selectedTemplate);
        }

        if($this->lastName != "") {
            // Last page
            $this->addMaskReplacement('page', $this->lastName, true);
            $options['page_number'] = $pager->getLastPage();

            $selectedTemplate = $this->getSelectedTemplate();
            $this->setSelectedTemplate($this->getLastNameSelectedTemplate());

            $str .= $this->processPage($options);

            $this->setSelectedTemplate($selectedTemplate);
        }
        // Possible wish to return value instead of print it on screen
        if ($return) {
            return $str;
        }

        echo $str;
    }

    protected $firstName = "&laquo;";
    protected $lastName = "&raquo;";

    protected $previousName = "&lsaquo;";
    protected $nextName = "&rsaquo;";

    protected $firstNameSelectedTemplate = '<li><a href="{%url}">{%page}</a></li>';
    protected $lastNameSelectedTemplate = '<li><a href="{%url}">{%page}</a></li>';

    protected $nextNameSelectedTemplate = '<li><a href="{%url}">{%page}</a></li>';
    protected $previousNameSelectedTemplate = '<li><a href="{%url}">{%page}</a></li>';

    
    public function getPageSortBar() {
    	
    	$arr = explode('/',$this->getUrlMask());
    	return '/'.$arr[1].'/'.$arr[2].'/'.$arr[3].'/'.$arr[4].'/'.$arr[5].'/'.$arr[6].'/'.$arr[7].'/';
    }
    
    public function getPageSortBarMotiv() {
    	
    	$arr = explode('/',$this->getUrlMask());
    	return '/'.$arr[1].'/'.$arr[2].'/'.$arr[3].'/'.$arr[4].'/';
    }

    public function getPageSortBarMarket() {
    	
    	$arr = explode('/',$this->getUrlMask());
    	return '/'.$arr[1].'/'.$arr[2].'/'.$arr[3].'/'.$arr[4].'/';
    }

    /**
     * @return string
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * @param string $firstSiteName
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;
        return $this;
    }

    /**
     * @return string
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * @param string $lastSiteName
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;
        return $this;
    }

    /**
     * @return string
     */
    public function getPreviousName()
    {
        return $this->previousName;
    }

    /**
     * @param string $previousName
     */
    public function setPreviousName($previousName)
    {
        $this->previousName = $previousName;
        return $this;
    }

    /**
     * @return string
     */
    public function getNextName()
    {
        return $this->nextName;
    }

    /**
     * @param string $nextName
     */
    public function setNextName($nextName)
    {
        $this->nextName = $nextName;
        return $this;
    }

    /**
     * @return string
     */
    public function getFirstNameSelectedTemplate()
    {
        return $this->firstNameSelectedTemplate;
    }

    /**
     * @param string $firstNameSelectedTemplate
     */
    public function setFirstNameSelectedTemplate($firstNameSelectedTemplate)
    {
        $this->firstNameSelectedTemplate = $firstNameSelectedTemplate;
    }

    /**
     * @return string
     */
    public function getLastNameSelectedTemplate()
    {
        return $this->lastNameSelectedTemplate;
    }

    /**
     * @param string $lastNameSelectedTemplate
     */
    public function setLastNameSelectedTemplate($lastNameSelectedTemplate)
    {
        $this->lastNameSelectedTemplate = $lastNameSelectedTemplate;
    }

    /**
     * @return string
     */
    public function getNextNameSelectedTemplate()
    {
        return $this->nextNameSelectedTemplate;
    }

    /**
     * @param string $nextNameSelectedTemplate
     */
    public function setNextNameSelectedTemplate($nextNameSelectedTemplate)
    {
        $this->nextNameSelectedTemplate = $nextNameSelectedTemplate;
    }

    /**
     * @return string
     */
    public function getPreviousNameSelectedTemplate()
    {
        return $this->previousNameSelectedTemplate;
    }

    /**
     * @param string $previousNameSelectedTemplate
     */
    public function setPreviousNameSelectedTemplate($previousNameSelectedTemplate)
    {
        $this->previousNameSelectedTemplate = $previousNameSelectedTemplate;
    }
}
