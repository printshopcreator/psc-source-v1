<?php
class TP_Doctrine_Pager_Layout extends Doctrine_Pager_Layout
{
    public function __construct($pager, $pagerRange, $urlMask)
    {
        parent::__construct($pager, $pagerRange, $urlMask);
    }

    protected function _parseTemplate($options = array())
    {
        $str = parent::_parseTemplate($options);

        return preg_replace(
            '/\{link_to\}(.*?)\{\/link_to\}/', 'test', $str
        );
    }
}
