<?php

class TP_FOP {

    public static function generatePreview($articleId, $layouterId = false) {

        if ($articleId == null) {
            throw new TP_FOP_Exception;
        }


        $shop = Zend_Registry::get('shop');

        $article = Doctrine_Query::create()
                        ->from('Article c')
                        ->where('c.id = ?', array(intval($articleId)))
                        ->fetchOne();

        if ($article == false) {
            throw new Zend_Amf_Exception;
        }

        $config = new Zend_Config_Ini(APPLICATION_PATH . '/configs/program.ini', APPLICATION_ENV);
        $output = "";

        if (Zend_Auth::getInstance()->hasIdentity() && $layouterId == false) {

            if (!file_exists(APPLICATION_PATH . '/../market/preview/' . $article->a6_directory)) {
                $t = APPLICATION_PATH . '/../market/preview/';
                foreach (explode('/', $article->a6_directory) as $dd) {
                    $t = $t . $dd . '/';
                    if (!file_exists($t)) {
                        mkdir($t);
                    }
                }
            }


            if ($article->render_new_preview_image) {
                $dom = new DOMDocument();
                $dom->load(APPLICATION_PATH . '/../market/products/' . $article->a6_directory . '' . $article->a6_xmlpageobjectsfile);
                $xpath = new DOMXPath($dom);

                $pages = $xpath->query('//root/pages/item/MASTERPAGE_UID');
                $init = $xpath->query('//root/status');
                if($init->length == 0) {
                    $dompt = new DOMDocument();
                    $dompt->load(APPLICATION_PATH . '/../market/products/' . $article->a6_directory . '' . $article->a6_xmlpagetemplatesfile);
                    $xpathpt = new DOMXPath($dompt);

                    $a = (int) $xpathpt->query('//root/item[contains(MASTERPAGE_UID,"' . $pages->item(0)->nodeValue . '")]/a')->item(0)->nodeValue;
                    $b = (int) $xpathpt->query('//root/item[contains(MASTERPAGE_UID,"' . $pages->item(0)->nodeValue . '")]/b')->item(0)->nodeValue;

                    if($a > 0 && $b > 0 ) {
                        if($a > $b) {
                            $dpi =  round(72 / (($a / 25 * 72) / 250));
                        }else{
                            $dpi =  round(72 / (($b / 25 * 72) / 250));
                        }
                    }else{
                        $dpi = 72;
                    }

                    if($dpi > 150) {

                        $eh = exec($config->fop->path . ' -r -c "' . APPLICATION_PATH . '/../application/configs/fop.xml" -dpi '.$dpi.' "' . APPLICATION_PATH . '/../market/products/' . $article->a6_directory . '' . $article->a6_xmlpreviewxslfofile
                                        . '" -png "' . APPLICATION_PATH . '/../market/preview/' . $article->a6_directory . 'preview.png"');

                    }else{

                        $eh = exec($config->fop->path . ' -r -c "' . APPLICATION_PATH . '/../application/configs/fop.xml" -dpi '.$dpi.' "' . APPLICATION_PATH . '/../market/products/' . $article->a6_directory . '' . $article->a6_xmlpreviewxslfofile
                                        . '" "' . APPLICATION_PATH . '/../market/preview/' . $article->a6_directory . 'preview.pdf"');
                        if (file_exists(APPLICATION_PATH . '/../market/preview/' . $article->a6_directory . 'preview.pdf')) {
                            
                            exec("gm convert ".APPLICATION_PATH . '/../market/preview/' . $article->a6_directory . 'preview.pdf -resize 1000x ' . APPLICATION_PATH . '/../market/preview/' . $article->a6_directory . 'preview.png');
                            /*$img = new imagick();
                            $img->readImage(APPLICATION_PATH . '/../market/preview/' . $article->a6_directory . 'preview.pdf');
                            $img->thumbnailImage(1000, null);
                            //$img->setCompressionQuality(80);
                            $img->writeImage(APPLICATION_PATH . '/../market/preview/' . $article->a6_directory . 'preview.png');
                            $img->destroy();
                            unset($img);
                            */
                            unlink(APPLICATION_PATH . '/../market/preview/' . $article->a6_directory . 'preview.pdf');


                        }

                        $article->render_new_preview_image = false;

                        $article->save();
                    }
                }
            }
            return APPLICATION_PATH . '/../market/preview/' . $article->a6_directory . 'preview.png';
        } else {


            if (!file_exists(APPLICATION_PATH . '/../market/guestfop/'))
                mkdir(APPLICATION_PATH . '/../market/guestfop/');

            if ($layouterId == false) {

                if (!file_exists(APPLICATION_PATH . '/../market/preview/' . $article->a6_directory)) {
                    $t = APPLICATION_PATH . '/../market/preview/';
                    foreach (explode('/', $article->a6_directory) as $dd) {
                        $t = $t . $dd . '/';
                        if (!file_exists($t)) {
                            mkdir($t);
                        }
                    }
                }

                if ($article->a6_directory != "" && ($article->render_new_preview_image || !file_exists(APPLICATION_PATH . '/../market/preview/' . $article->a6_directory . 'preview.png'))) {

                    $dom = new DOMDocument();
                    $dom->load(APPLICATION_PATH . '/../market/products/' . $article->a6_directory . '' . $article->a6_xmlpageobjectsfile);
                    $xpath = new DOMXPath($dom);

                    $pages = $xpath->query('//root/pages/item/MASTERPAGE_UID');
                    $init = $xpath->query('//root/status');
                    
                    if($init->length == 0) {
                        $dompt = new DOMDocument();
                        $dompt->load(APPLICATION_PATH . '/../market/products/' . $article->a6_directory . '' . $article->a6_xmlpagetemplatesfile);
                        $xpathpt = new DOMXPath($dompt);

                        $a = (int) $xpathpt->query('//root/item[contains(MASTERPAGE_UID,"' . $pages->item(0)->nodeValue . '")]/a')->item(0)->nodeValue;
                        $b = (int) $xpathpt->query('//root/item[contains(MASTERPAGE_UID,"' . $pages->item(0)->nodeValue . '")]/b')->item(0)->nodeValue;

                        if($a > 0 && $b > 0 ) {
                            if($a > $b) {
                                $dpi =  round(72 / (($a / 25 * 72) / 250));
                            }else{
                                $dpi =  round(72 / (($b / 25 * 72) / 250));
                            }
                        }else{
                            $dpi = 72;
                        }

                        if($dpi > 150) {

                            $eh = exec($config->fop->path . ' -r -c "' . APPLICATION_PATH . '/../application/configs/fop.xml" -dpi '.$dpi.' "' . APPLICATION_PATH . '/../market/products/' . $article->a6_directory . '' . $article->a6_xmlpreviewxslfofile
                                            . '" -png "' . APPLICATION_PATH . '/../market/preview/' . $article->a6_directory . 'preview.png"');
                        }else{

                            $eh = exec($config->fop->path . ' -r -c "' . APPLICATION_PATH . '/../application/configs/fop.xml" -dpi '.$dpi.' "' . APPLICATION_PATH . '/../market/products/' . $article->a6_directory . '' . $article->a6_xmlpreviewxslfofile
                                            . '" "' . APPLICATION_PATH . '/../market/preview/' . $article->a6_directory . 'preview.pdf"');

                            if (file_exists(APPLICATION_PATH . '/../market/preview/' . $article->a6_directory . 'preview.pdf')) {
                                
                                exec("gm convert ".APPLICATION_PATH . '/../market/preview/' . $article->a6_directory . 'preview.pdf -resize 1000x ' . APPLICATION_PATH . '/../market/preview/' . $article->a6_directory . 'preview.png');
  /*                              
                                $img = new imagick();
                                $img->readImage(APPLICATION_PATH . '/../market/preview/' . $article->a6_directory . 'preview.pdf');
                                $img->thumbnailImage(1000, null);
                                //$img->setCompressionQuality(80);
                                $img->writeImage(APPLICATION_PATH . '/../market/preview/' . $article->a6_directory . 'preview.png');
                                $img->destroy();
                                unset($img);
                                */
                                unlink(APPLICATION_PATH . '/../market/preview/' . $article->a6_directory . 'preview.pdf');

                                $article->render_new_preview_image = false;
                                $article->save();
                            }
                        }
                    }
                }

                return APPLICATION_PATH . '/../market/preview/' . $article->a6_directory . 'preview.png';
            } else {

                $articleSession = new TP_Layoutersession();
                $articleSess = $articleSession->getLayouterArticle($layouterId);

                if (!file_exists(APPLICATION_PATH . '/../market/guestfop/' . md5($articleSess->getPreviewXslFo()) . '_preview.png') && $articleSess->getPreviewXslFo() != "") {

                    file_put_contents(APPLICATION_PATH . '/../market/guestfop/' . md5($articleSess->getPreviewXslFo()) . '.fop', $articleSess->getPreviewXslFo());

                    $dom = new DOMDocument();
                    $dom->loadXML($articleSess->getPageObjects());
                    $xpath = new DOMXPath($dom);

                    $pages = $xpath->query('//root/pages/item/MASTERPAGE_UID');

                    $dompt = new DOMDocument();
                    $dompt->loadXML($articleSess->getPageTemplates());
                    $xpathpt = new DOMXPath($dompt);

                    $a = (int) $xpathpt->query('//root/item[contains(MASTERPAGE_UID,"' . $pages->item(0)->nodeValue . '")]/a')->item(0)->nodeValue;
                    $b = (int) $xpathpt->query('//root/item[contains(MASTERPAGE_UID,"' . $pages->item(0)->nodeValue . '")]/b')->item(0)->nodeValue;

                    if($a > 0 && $b > 0 ) {
                        if($a > $b) {
                            $dpi =  round(72 / (($a / 25 * 72) / 250));
                        }else{
                            $dpi =  round(72 / (($b / 25 * 72) / 250));
                        }
                    }else{
                        $dpi = 72;
                    }
                    Zend_Registry::get('log')->debug($config->fop->path . ' -r -c "' . APPLICATION_PATH . '/../application/configs/fop.xml" -dpi '.$dpi.' "' . APPLICATION_PATH . '/../market/guestfop/' . md5($articleSess->getPreviewXslFo()) . '.fop'
                        . '" -png "' . APPLICATION_PATH . '/../market/guestfop/' . md5($articleSess->getPreviewXslFo()) . '_preview.png"');
                    if($dpi > 150) {

                        $eh = exec($config->fop->path . ' -r -c "' . APPLICATION_PATH . '/../application/configs/fop.xml" -dpi '.$dpi.' "' . APPLICATION_PATH . '/../market/guestfop/' . md5($articleSess->getPreviewXslFo()) . '.fop'
                                        . '" -png "' . APPLICATION_PATH . '/../market/guestfop/' . md5($articleSess->getPreviewXslFo()) . '_preview.png"');

                    }else{
                        $eh = exec($config->fop->path . ' -r -c "' . APPLICATION_PATH . '/../application/configs/fop.xml" -dpi '.$dpi.' "' . APPLICATION_PATH . '/../market/guestfop/' . md5($articleSess->getPreviewXslFo()) . '.fop'
                                    . '" "' . APPLICATION_PATH . '/../market/guestfop/' . md5($articleSess->getPreviewXslFo()) . '_preview.pdf"');

						exec("gm convert ".APPLICATION_PATH . '/../market/guestfop/' . md5($articleSess->getPreviewXslFo()) . '_preview.pdf -resize 1000x ' . APPLICATION_PATH . '/../market/guestfop/' . md5($articleSess->getPreviewXslFo()) . '_preview.png');
/*
                        $img = new imagick();
                        $img->readImage(APPLICATION_PATH . '/../market/guestfop/' . md5($articleSess->getPreviewXslFo()) . '_preview.pdf');
                        $img->thumbnailImage(1000, null);
                        //$img->setCompressionQuality(80);
                        $img->writeImage(APPLICATION_PATH . '/../market/guestfop/' . md5($articleSess->getPreviewXslFo()) . '_preview.png');
                        $img->destroy();
                        unset($img);
                        */
                        unlink(APPLICATION_PATH . '/../market/guestfop/' . md5($articleSess->getPreviewXslFo()) . '_preview.pdf');
                    }
                }

                return APPLICATION_PATH . '/../market/guestfop/' . md5($articleSess->getPreviewXslFo()) . '_preview.png';
            }
        }
    }

    public static function generatePngPreview($article, $layouterid = false) {

        if ($article == false) {
            throw new Zend_Amf_Exception;
        }
        $layoutPath = Zend_Registry::get('layout_path');

        $config = new Zend_Config_Ini(APPLICATION_PATH . '/configs/program.ini', APPLICATION_ENV);

        if (Zend_Auth::getInstance()->hasIdentity() && ($layouterid == false || $layouterid == "")) {

            if ($article->render_new_preview_gallery) {
                $eh = exec($config->fop->path . ' -r -c "' . APPLICATION_PATH . '/../application/configs/fop.xml" -q -dpi 72 "' . APPLICATION_PATH . '/../market/products/' . $article->a6_directory . '' . $article->a6_xmlextendpreviewxslfofile
                                . '" "' . APPLICATION_PATH . '/../market/preview/' . $article->a6_directory . 'preview.pdf" > ' . APPLICATION_PATH . '/../logs/programm.log');

                $article->render_new_preview_gallery = false;
                $article->save();

                $xml = new SimpleXMLElement($layoutPath . '/config/gallery.xml', null, true);

                $dom = new DOMDocument();

                $dom->load(APPLICATION_PATH . '/../market/products/' . $article->a6_directory . '' . $article->a6_xmlpageobjectsfile);

                $xpath = new DOMXPath($dom);
                $seiten = $xpath->query("//root/pages/item")->length;

                $item = $xml->items->addChild('item');
                $item->addAttribute('source', '/market/preview/' . $article->a6_directory . 'preview.jpg');

                $im = new imagick(APPLICATION_PATH . '/../market/preview/' . $article->a6_directory . 'preview.pdf[0]');
                $im->thumbnailImage(800, null);
                $im->setImageResolution(72, 72);
                $im->setCompression(imagick::COMPRESSION_JPEG);
                //$im->setCompressionQuality(80);
                $im->writeImage(APPLICATION_PATH . '/../market/preview/' . $article->a6_directory . 'preview.jpg');
                $im->clear();
                $im->destroy();
                if($seiten > 1) {
	                for ($i = 1; $i < $seiten; $i++) {
	
	                    $im = new imagick(APPLICATION_PATH . '/../market/preview/' . $article->a6_directory . 'preview.pdf[' . $i . ']');
	                    $im->thumbnailImage(600, null);
	                    $im->setImageResolution(72, 72);
	                    $im->setCompression(imagick::COMPRESSION_JPEG);
	                    //$im->setCompressionQuality(80);
	                    $im->writeImage(APPLICATION_PATH . '/../market/preview/' . $article->a6_directory . 'preview' . $i . '.jpg');
	                    $im->clear();
	                    $im->destroy();
	
	                    $item = $xml->items->addChild('item');
	                    $item->addAttribute('source', '/market/preview/' . $article->a6_directory . 'preview' . $i . '.jpg');
	                }
                }
                $xml->saveXML(APPLICATION_PATH . '/../market/preview/' . $article->a6_directory . 'gallery.xml');
            }

            return '/market/preview/' . $article->a6_directory . 'gallery.xml';
        } else {


            if (!file_exists(APPLICATION_PATH . '/../market/guestfop/'))
                mkdir(APPLICATION_PATH . '/../market/guestfop/');

            if ($layouterid == "") {
				
                if ($article->render_new_preview_gallery) {
                    $eh = exec($config->fop->path . ' -r -c "' . APPLICATION_PATH . '/../application/configs/fop.xml" -q -dpi 72 "' . APPLICATION_PATH . '/../market/products/' . $article->a6_directory . '' . $article->a6_xmlextendpreviewxslfofile
                                    . '" "' . APPLICATION_PATH . '/../market/preview/' . $article->a6_directory . 'preview.pdf" > ' . APPLICATION_PATH . '/../logs/programm.log');

                    $article->render_new_preview_gallery = false;
                    $article->save();

                    $xml = new SimpleXMLElement($layoutPath . '/config/gallery.xml', null, true);

                    $dom = new DOMDocument();

                    $dom->load(APPLICATION_PATH . '/../market/products/' . $article->a6_directory . '' . $article->a6_xmlpageobjectsfile);

                    $xpath = new DOMXPath($dom);
                    $seiten = $xpath->query("//root/pages/item")->length;

                    $item = $xml->items->addChild('item');
                    $item->addAttribute('source', '/market/preview/' . $article->a6_directory . 'preview.jpg');

                    $im = new imagick(APPLICATION_PATH . '/../market/preview/' . $article->a6_directory . 'preview.pdf[0]');
                    $im->thumbnailImage(600, null);
                    $im->setImageResolution(72, 72);
                    $im->setCompression(imagick::COMPRESSION_JPEG);
                    $im->setCompressionQuality(80);
                    $im->writeImage(APPLICATION_PATH . '/../market/preview/' . $article->a6_directory . 'preview.jpg');
                    $im->clear();
                    $im->destroy();
					if($seiten > 1) {
	                    for ($i = 1; $i <= $seiten; $i++) {
	
	                        $im = new imagick(APPLICATION_PATH . '/../market/preview/' . $article->a6_directory . 'preview.pdf[' . $i . ']');
	                        $im->thumbnailImage(600, null);
	                        $im->setImageResolution(72, 72);
	                        $im->setCompression(imagick::COMPRESSION_JPEG);
	                        //$im->setCompressionQuality(80);
	                        $im->writeImage(APPLICATION_PATH . '/../market/preview/' . $article->a6_directory . 'preview' . $i . '.jpg');
	                        $im->clear();
	                        $im->destroy();
	
	                        $item = $xml->items->addChild('item');
	                        $item->addAttribute('source', '/market/preview/' . $article->a6_directory . 'preview' . $i . '.jpg');
	                    }
					}
					$xml->saveXML(APPLICATION_PATH . '/../market/preview/' . $article->a6_directory . 'gallery.xml');
                }

                return '/market/preview/' . $article->a6_directory . 'gallery.xml';
            } else {

                $articleSession = new TP_Layoutersession();
                $articleSess = $articleSession->getLayouterArticle($layouterid);
                if (!file_exists(APPLICATION_PATH . '/../market/guestfop/' . md5($articleSess->getPageObjects()) . '.fop')) {

                    file_put_contents(APPLICATION_PATH . '/../market/guestfop/' . md5($articleSess->getPageObjects()) . '.fop', $articleSess->getXslFo());

                    $eh = exec($config->fop->path . ' -r -c "' . APPLICATION_PATH . '/../application/configs/fop.xml" -q -dpi 72 "' . APPLICATION_PATH . '/../market/guestfop/' . md5($articleSess->getPageObjects()) . '.fop'
                                    . '" "' . APPLICATION_PATH . '/../market/guestfop/' . md5($articleSess->getPageObjects()) . '_preview.pdf"');

                    if (!file_exists(APPLICATION_PATH . '/../market/guestfop/' . md5($articleSess->getPageObjects()) . 'gallery.xml')) {

                        $xml = new SimpleXMLElement($layoutPath . '/config/gallery.xml', null, true);

                        $dom = new DOMDocument();
                        $dom->loadXML($articleSess->getPageObjects());

                        $xpath = new DOMXPath($dom);
                        $seiten = $xpath->query("//root/pages/item")->length;

                        $item = $xml->items->addChild('item');
                        $item->addAttribute('source', '/market/guestfop/' . md5($articleSess->getPageObjects()) . '_preview.jpg');

                        $im = new imagick(APPLICATION_PATH . '/../market/guestfop/' . md5($articleSess->getPageObjects()) . '_preview.pdf[0]');
                        $im->thumbnailImage(600, null);
                        $im->setImageResolution(72, 72);
                        $im->setCompression(imagick::COMPRESSION_JPEG);
                        //$im->setCompressionQuality(80);
                        $im->writeImage(APPLICATION_PATH . '/../market/guestfop/' . md5($articleSess->getPageObjects()) . '_preview.jpg');
                        $im->clear();
                        $im->destroy();
                        if($seiten > 1) {
	                        for ($i = 1; $i < $seiten; $i++) {
	
	                            $im = new imagick(APPLICATION_PATH . '/../market/guestfop/' . md5($articleSess->getPageObjects()) . '_preview.pdf[' . $i . ']');
	                            $im->thumbnailImage(600, null);
	                            $im->setImageResolution(72, 72);
	                            $im->setCompression(imagick::COMPRESSION_JPEG);
	                            //$im->setCompressionQuality(80);
	                            $im->writeImage(APPLICATION_PATH . '/../market/guestfop/' . md5($articleSess->getPageObjects()) . '_preview' . $i . '.jpg');
	                            $im->clear();
	                            $im->destroy();
	
	                            $item = $xml->items->addChild('item');
	                            $item->addAttribute('source', '/market/guestfop/' . md5($articleSess->getPageObjects()) . '_preview' . $i . '.jpg');
	                        }
                        }
                        $xml->saveXML(APPLICATION_PATH . '/../market/guestfop/' . md5($articleSess->getPageObjects()) . 'gallery.xml');
                    }
                }

                return '/market/guestfop/' . md5($articleSess->getPageObjects()) . 'gallery.xml';
            }
        }
    }

    public static function generatePdfPreview($article) {

        if ($article == false) {
            throw new Zend_Amf_Exception;
        }

        $config = new Zend_Config_Ini(APPLICATION_PATH . '/configs/program.ini', APPLICATION_ENV);

        $eh = exec($config->fop->path_pdf . ' -r -c "' . APPLICATION_PATH . '/../application/configs/fop.xml" -q "' . APPLICATION_PATH . '/../market/products/' . $article->a6_directory . '' . $article->a6_xmlxslfofile
                        . '" -pdf "' . APPLICATION_PATH . '/../market/preview/' . $article->a6_directory . 'file.pdf" > ' . APPLICATION_PATH . '/../logs/programm.log');


        return APPLICATION_PATH . '/../market/preview/' . $article->a6_directory . 'file.pdf';
    }

    public static function generatePdfPrint($from, $to) {

        $config = new Zend_Config_Ini(APPLICATION_PATH . '/configs/program.ini', APPLICATION_ENV);

        $eh = exec($config->fop->path_pdf . ' -r -c "' . APPLICATION_PATH . '/../application/configs/fop_print.xml" -q "' . $from
                        . '" -pdf "' . $to . '" > ' . APPLICATION_PATH . '/../logs/programm.log');

    }

    public static function generatePdfPrintDPI($from, $to) {

        $config = new Zend_Config_Ini(APPLICATION_PATH . '/configs/program.ini', APPLICATION_ENV);

        $eh = exec($config->fop->path_pdf . ' -r -c "' . APPLICATION_PATH . '/../application/configs/fop_print.xml" -q -dpi 72 "' . $from
            . '" -pdf "' . $to . '" > ' . APPLICATION_PATH . '/../logs/programm.log');

    }

    public static function generatePdfPrintPreview($from, $to) {

        $config = new Zend_Config_Ini(APPLICATION_PATH . '/configs/program.ini', APPLICATION_ENV);

        $eh = exec($config->fop->path_pdf . ' -r -c "' . APPLICATION_PATH . '/../application/configs/fop.xml" -q "' . $from
                        . '" -pdf "' . $to . '" > ' . APPLICATION_PATH . '/../logs/programm.log');

    }

}
