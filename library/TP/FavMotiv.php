<?php

class TP_FavMotiv {

    /**
     * Layoutersession
     *
     * @var Zend_Session_Namespace
     */
    private $_Items;

    /**
     * Konstruiert das Model für die Suche
     *
     * @return void
     */
    public function __construct()
    {

        $this->_Items = new Zend_Session_Namespace('FavMotivsession');

        $this->_Items->unlock();

        if(Zend_Session::namespaceIsset ('FavMotivsession') === false)
        {
            $this->initSession();
        }

        $this->_Items->lock();
    }

    /**
     * Gibt das Item aus der Session zurück
     *
     * @param string $articleId
     * @return TP_Layouter_Item
     */
    public function getMotive() {
            return $this->_Items->data;
    }

    public function getAutoFillMotive($ProdId = false) {
            if($ProdId) {
                return $this->_Items->dataautofill[$ProdId];
            }
            return $this->_Items->dataautofill;
    }
    
    public function getMotiveUUID() {
            return $this->_Items->data;
    }

    public function getMotiv($MotivId) {
            if(isset($this->_Items->data[$MotivId])) {
            return $this->_Items->data[$MotivId];
            }
            return false;
    }

    public function delMotiv($MotivId) {
        $this->_Items->unlock();
        if(isset($this->_Items->data[$MotivId])) unset($this->_Items->data[$MotivId]);
        $this->_Items->lock();
    }
    
    public function delMotivUUID($MotivId) {
        $this->_Items->unlock();
        if(isset($this->_Items->dataUUID[$MotivId])) unset($this->_Items->dataUUID[$MotivId]);
        $this->_Items->lock();
    }

    public function addMotiv($MotivId) {
        if(!isset($this->_Items->data[$MotivId])) {
            $this->_Items->unlock();
            $this->_Items->data[$MotivId] = $MotivId;
            $this->_Items->lock();
            return true;
        }else{
            return false;
        }
    }

    public function addAutoFillMotiv($ProdId, $MotivId) {
        if(!isset($this->_Items->dataautofill[$ProdId]) || !in_array($MotivId, $this->_Items->dataautofill[$ProdId])) {
            $this->_Items->unlock();
            if(!isset($this->_Items->dataautofill[$ProdId])) {
                $this->_Items->dataautofill[$ProdId] = array();
            }
            $this->_Items->dataautofill[$ProdId][] = $MotivId;
            $this->_Items->lock();
            return true;
        }else{
            return false;
        }
    }
    
    public function addMotivUUID($MotivId) {
        if(!isset($this->_Items->dataUUID[$MotivId])) {
            $this->_Items->unlock();
            $this->_Items->dataUUID[$MotivId] = $MotivId;
            $this->_Items->lock();
            return true;
        }else{
            return false;
        }
    }

    private function initSession() {
        $this->_Items->unlock();
        $this->_Items->data = array(0 => 0);
        $this->_Items->dataUUID = array(0 => 0);
        $this->_Items->dataautofill = array();
        $this->_Items->lock();
    }

    public function clearSession() {
        $this->initSession();
    }

}
