<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Filter
 *
 * @author boonkerz
 */
class TP_Filter {


    public static function proccess($var) {

        $filter = Zend_Registry::get('params');
        
        
        if (isset($filter['filter']) && is_array($filter['filter'])) {
            $filter = $filter['filter'];
            for ($i=0;$i<count($filter);$i++){
                $filter[$i]['field'] = str_replace('/','.',$filter[$i]['field']);
                $filter[$i]['data']['value'] = str_replace('ext-record-', '', $filter[$i]['data']['value']);
                
                switch($filter[$i]['data']['type']){
                    case 'string' : $var->addWhere($filter[$i]['field'].' LIKE ?', array('%'.$filter[$i]['data']['value'].'%')); Break;
                    case 'list' :
                        if (strstr($filter[$i]['data']['value'],',')){
                        	if($filter[$i]['field'] == 'typName') {
                        		$fi = explode(',',$filter[$i]['data']['value']);
                        		$mode = true;
                                $select = "";
                        		foreach($fi as $key) {

                        			if($key == '6') {
                        				if($mode) {
                        					$select .= "(typ = 6 AND a6_org_article = 0)";
                        					$mode = false;
                        				}else{
                        					$select .= " OR (typ = 6 AND a6_org_article = 0)";
                        				}
                        			}elseif($key == '6b') {
                        				if($mode) {
                        					$select .= "(typ = 6 AND a6_org_article != 0)";
                                            $mode = false;
                        				}else{
                        					$select .= " OR (typ = 6 AND a6_org_article != 0)";
                        				}
                        			}else{
                        				if($mode) {
                        					$select .= "(typ = $key)";
                                            $mode = false;
                        				}else{
                        					$select .= " OR (typ = $key)";
                        				}
                        			}

                        		}
                        		$var->addWhere($select);
                        	}else{
	                            $fi = explode(',',$filter[$i]['data']['value']);
	                            for ($q=0;$q<count($fi);$q++){
	                                $fi[$q] = "".($fi[$q])."";
	                            }
	                            $filter[$i]['data']['value'] = implode(',',$fi);
	                            $var->addWhere($filter[$i]['field'].' IN ('.$filter[$i]['data']['value'].')', array());
                        	}
                        }else{
                        	if($filter[$i]['field'] == 'typName') {
                        		if($filter[$i]['data']['value'] == '6b') {
                        			$var->addWhere('typ = 6 AND a6_org_article != 0');
                        		}elseif($filter[$i]['data']['value'] == '6') {
                            		$var->addWhere('typ = 6 AND a6_org_article = 0');
                        		}else{
                        			$var->addWhere('typ = ?', array(($filter[$i]['data']['value'])));
                        		}
                        	}else{
                        		$var->addWhere($filter[$i]['field'].' = ?', array(($filter[$i]['data']['value'])));
                        	}
                        }
                    Break;
                    case 'boolean' : 
                        if($filter[$i]['data']['value'] == 'true' || $filter[$i]['data']['value'] == '1') {
                            $var->addWhere($filter[$i]['field'].' = ?', array(1)); Break;
                        }else{
                            $var->addWhere($filter[$i]['field'].' = ?', array(0)); Break;
                        }
                    case 'numeric' :
                        switch ($filter[$i]['data']['comparison']) {
                            case 'eq' : $var->addWhere($filter[$i]['field'].' = ?', array($filter[$i]['data']['value'])); Break;
                            case 'lt' : $var->addWhere($filter[$i]['field'].' < ?', array($filter[$i]['data']['value'])); Break;
                            case 'gt' : $var->addWhere($filter[$i]['field'].' > ?', array($filter[$i]['data']['value'])); Break;
                        }
                    Break;
                    case 'date' :
                        switch ($filter[$i]['data']['comparison']) {
                            case 'eq' : $var->addWhere($filter[$i]['field'].' = ?', array(date('Y-m-d',strtotime($filter[$i]['data']['value'])))); Break;
                            case 'lt' : $var->addWhere($filter[$i]['field'].' < ?', array(date('Y-m-d',strtotime($filter[$i]['data']['value'])))); Break;
                            case 'gt' : $var->addWhere($filter[$i]['field'].' > ?', array(date('Y-m-d',strtotime($filter[$i]['data']['value'])))); Break;
                        }
                    Break;
                }
            }
        }

    }

}
?>
