<?php
class TP_Filter_Umlaut implements Zend_Filter_Interface
{

    protected $_trans = array(
                        'ä'    =>    'ae',
                        'ö'    =>    'oe',
                        'ü'    =>    'ue',
                        'Ä'    =>    'Ae',
                        'Ö'    =>    'Oe',
                        'Ü'    =>    'Ue',
                        'ß'    =>    'ss');

    public function filter($value)
    {
        // einige Transformationen über $value durchführen um $valueFiltered zu erhalten
        $valueFiltered = strtr($value, $this->_trans);
        return $valueFiltered;
    }
}