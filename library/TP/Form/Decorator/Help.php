<?php
class TP_Form_Decorator_Help extends Zend_Form_Decorator_Abstract {

	public function render($content) {
		$placement = $this->getPlacement();
		$separator = $this->getSeparator();

		switch ($placement) {
			case self::PREPEND:
				return $separator . $this->_options['markup'] . $content;
			case self::APPEND:
			default:
				return $content . $this->_options['markup'] . $separator;
		}
	}

}
