<?php

class TP_Form_Element_Alternatelegend extends Zend_Form_Element_Text
{


    public function  render(Zend_View_Interface $view = null) {
        
        return '<dt class="alternatelegend_dt">' . $this->getLabel() . '</dt><dd class="alternatelegend_dd"></dd>';
    }

}
