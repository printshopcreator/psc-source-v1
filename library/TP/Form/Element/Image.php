<?php

class TP_Form_Element_Image extends Zend_Form_Element_File {
    public $options;
  
    public function __construct($image_name, $attributes = array(), $image_path = false) {
        $this->options = $image_path;
        
        parent::__construct($image_name, $attributes);
        
        //
        // By default the description is rendered last, but we want to use
        // it to show the <img> when the user is editing it, so we rearrange
        // the order in which the things are rendered
        //
      
        $this->clearDecorators();
        $this
            ->addDecorator('Description', array('tag' => 'div', 'class' => 'description image-preview', 'escape' => false))
            ->addDecorator('File')
            ->addDecorator('Errors')
            ->addDecorator('HtmlTag', array('tag' => 'dd'))
            ->addDecorator('Label', array('tag' => 'dt'));

    }

    public function render() {
    	$content = trim(parent::render());
    	
    	if(strpos($content, '</dd>')) {
    		if($this->options != "") {
    			$this->options = '<br/><br/><span class="remove_image_helper"><input type="checkbox" name="'.$this->getId().'_remove" value="1"/> Entfernen</span><img src="'.$this->options.'">';
    			$content = substr(trim(parent::render()), 0, -5).$this->options . '</dd>';
    		}
    	}else{
    		if($this->options != "") {
    			$this->options = '<img src="'.$this->options.'"><span class="remove_image_helper"> <input type="checkbox" name="'.$this->getId().'_remove" value="1"/> Entfernen</span>';
    			$content = substr(trim(parent::render()), 0, -6).$this->options . '</div>';
    		}
    	}
    	
    	
    	return $content;

    }
}
