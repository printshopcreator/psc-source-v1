<?php

class TP_Form_Element_Mobile extends Zend_Form_Element_Text {

    public function  render(Zend_View_Interface $view = null) {

        parent::render();
        if($this->getId() == "rech-self_mobile" || $this->getId() == "self_mobile" || $this->getId() == "mobile_2" || $this->getId() == "mobile_3") {
            $content = '<div class="control-group"><label for="'.$this->getId().'" class="control-label optional">'.$this->getLabel().'</label>';
            $content .= '<div class="controls">';
            $content .= '<input type="text" class="input-mini" name="rech['.$this->getId().'][lv]"/><input type="text" class="input-mini" name="rech['.$this->getId().'][vorwahl]"/><input class="input-mini" type="text" name="rech['.$this->getId().'][durchwahl]"/></div></div>';

            return $content;
        }
        $content = '<div class="control-group"><label for="'.$this->getId().'" class="control-label optional">'.$this->getLabel().'</label>';
        $content .= '<div class="controls">';
        $content .= '<input type="text" class="input-mini" name="rech['.$this->getId().'][lv]"/><input type="text" class="input-mini" name="rech['.$this->getId().'][vorwahl]"/><input class="input-mini" type="text" name="rech['.$this->getId().'][phone]"/><input class="input-mini" type="text" name="rech['.$this->getId().'][durchwahl]"/></div></div>';

        return $content;
    }
}
