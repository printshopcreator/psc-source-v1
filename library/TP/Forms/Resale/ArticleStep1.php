<?php


class TP_Forms_Resale_ArticleStep1 extends Zend_Form {

    protected $name;

    protected $tags;

    protected $description;

    protected $shop;

    protected $resale_price;
    
    protected $not_edit;
    
    protected $allow_layouter;

    protected $resale_design;
    
    protected $prodpreis;

    protected $submit;

    public function __construct(Article $article = null, $heightRole = null) {

        $wizard = new TP_ResaleWizard();

        $this->setAction('/resale/articlestep1');

        $this->addAttribs(array('class' => 'niceform', 'id' => 'articlestep1'));

        $this->name = new Zend_Form_Element_Text('name');
        $this->name->setAttrib('size', 37)
                    ->setRequired(true)
                    ->setAttrib('class', 'required')
                    ->addValidator('regex', false, array('pattern' => '/^[a-zA-Z0-9öÖäÄüÜß&\\s\"\_\-\.]+$/'))
                    ->setLabel('Name')
                    ->addFilter(new TP_Filter_Badwords())
                    ->setValue(($article != null && $wizard->getArticleName() == "")?$article->title:$wizard->getArticleName())
                    ->addPrefixPath('TP_Validate', 'TP/Validate/', 'Validate')
                    ->addValidator('Db_NoDomainRecordExistsDoctrine', false, array('table' => 'domain', 'field' => 'name'));

        
        $this->description = new Zend_Form_Element_Textarea('description');
        $this->description->setAttrib('rows', 6);
        $this->description->setAttrib('cols', 50);
        $this->description->setLabel('Beschreibung')
                        ->setRequired(true)
                    	->addFilter(new TP_Filter_Badwords())
                        ->setAttrib('class', 'required')
                        ->setValue(strip_tags(($article != null && $wizard->getArticleName() == "" && $wizard->getArticleDescription() == "")?$article->info:$wizard->getArticleDescription()));

        $this->tags = new Zend_Form_Element_Text('tags');
        $this->tags->setAttrib('size', 37)
                    ->setLabel('Tags (Tag1,Tag2)')
                    ->addFilter(new TP_Filter_Badwords())
                    ->setValue($wizard->getArticleTags());
                        
        $this->shop = new Zend_Form_Element_Select('shop');
        $this->shop->setLabel('Shop')
                        ->setRequired(true)
                        ->setAttrib('class', 'required')
                        ->setValue($wizard->getArticleShop());

        $cur = new Zend_Currency( Zend_Registry::get( 'locale' )->getLanguage() . '_' . Zend_Registry::get( 'locale' )->getRegion() );
        
        $this->prodpreis = new TP_Form_Element_Textdisplay('prodpreis');
        $this->prodpreis->setValue($cur->toCurrency($article->a4_abpreis))->setLabel('Produktionspreis');
                        
       	$this->resale_price = new TP_Form_Element_Price('resale_price');
        $this->resale_price->setAttrib('size', 30)
                    ->setLabel('Resale Price')
                    ->addValidator('Between', false, array('min' => -1, 'max' => 1000))
                    ->setValue($wizard->getResalePrice());
        
        
        
        $this->not_edit = new Zend_Form_Element_Checkbox('not_edit');
        $this->not_edit->setLabel('Als Produkt verkaufen')
                    ->setValue($wizard->getArticleNotEdit());

        $this->resale_design = new Zend_Form_Element_Checkbox('resale_design');
        $this->resale_design->setLabel('Als Designvorlage verkaufen')
                ->setValue($wizard->getArticleResaleDesign());

        $this->allow_layouter = new Zend_Form_Element_Checkbox('allow_layouter');
        $this->allow_layouter->setLabel('Layouten erlauben')
                ->setValue($wizard->getAllowLayouter());

        $user = Zend_Auth::getInstance()->getIdentity();
        $user = Doctrine_Query::create()
                ->from('Contact m')
                ->useQueryCache(true)
                ->where('id = ?')->fetchOne(array($user['id']));
        
        $install = Zend_Registry::get('install');
                
        foreach($user->Shops as $shop) {
            if($shop->id != $install['defaultmarket'] || $heightRole->level >= 40)
                $this->shop->addMultiOption($shop->id, $shop->name);
        }

        $this->submit = new Zend_Form_Element_Submit('Weiter');

        $this->addElements(array(
            $this->name,
            $this->description,
            $this->tags,
            $this->shop,
            $this->prodpreis,
            $this->resale_price,
            $this->not_edit,
            $this->resale_design,
            $this->allow_layouter,
            $this->submit
        ));


        $this->addDecorators(array(
            'FormElements',
                array('HtmlTag', array('tag' => 'dl')),
            'Form'
        ));
    }

    public function storeInSession() {
        $wizard = new TP_ResaleWizard();
        
        $wizard->setArticleStep1(
            $this->name->getValue(),
            $this->description->getValue(),
            "",
            $this->shop->getValue()
        );
        $wizard->setArticleStep4(
            false
        );
        $wizard->setResalePrice($this->resale_price->getValue());
        $wizard->setArticleNotEdit($this->not_edit->getValue());
        $wizard->setAllowLayouter($this->allow_layouter->getValue());
        $wizard->setArticleTags($this->tags->getValue());
        $wizard->setArticleResaleDesign($this->resale_design->getValue());
    }

}