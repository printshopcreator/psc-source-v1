<?php


class TP_Forms_Resale_ArticleStep2 extends Zend_Form {

    protected $articlegroup1;

    protected $articletheme1;

    protected $articlegroup2;

    protected $articletheme2;

    protected $articlegroup3;

    protected $articletheme3;

    protected $submit;

    public function __construct(Article $article = null) {

        $wizard = new TP_ResaleWizard();

        $this->setAction('/resale/articlestep2');
        
        $this->addAttribs(array('class' => 'niceform'));

        $this->submit = new Zend_Form_Element_Submit('Weiter');

        $this->articletheme1 = new Zend_Form_Element_Hidden('articletheme1');
        $this->articletheme1->setValue($wizard->getArticleTheme1());
        
        $this->articletheme2 = new Zend_Form_Element_Hidden('articletheme2');
        $this->articletheme2->setValue($wizard->getArticleTheme2());
        
        $this->articletheme3 = new Zend_Form_Element_Hidden('articletheme3');
        $this->articletheme3->setValue($wizard->getArticleTheme3());
               
        $this->articlegroup1 = new Zend_Form_Element_Hidden('articlegroup1');
        $this->articlegroup1->setValue($wizard->getArticleGroup1());
        
        $this->articlegroup2 = new Zend_Form_Element_Hidden('articlegroup2');
        $this->articlegroup2->setValue($wizard->getArticleGroup2());
        
        $this->articlegroup3 = new Zend_Form_Element_Hidden('articlegroup3');
        $this->articlegroup3->setValue($wizard->getArticleGroup3());
        
        $this->addElements(array(
			$this->articletheme1,
			$this->articletheme2,
			$this->articletheme3,
			$this->articlegroup1,
			$this->articlegroup2,
			$this->articlegroup3,
            $this->submit
        ));


        $this->addDecorators(array(
            'FormElements',
                array('HtmlTag', array('tag' => 'dl')),
            'Form'
        ));
    }
    
    

    public function storeInSession() {
        $wizard = new TP_ResaleWizard();
        $wizard->setArticleStep2(
            $this->articlegroup1->getValue(),
            $this->articlegroup2->getValue(),
            $this->articlegroup3->getValue(),
            $this->articletheme1->getValue(),
            $this->articletheme2->getValue(),
            $this->articletheme3->getValue()
        );
    }

}