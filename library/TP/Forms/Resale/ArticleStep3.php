<?php


class TP_Forms_Resale_ArticleStep3 extends Zend_Form {

    protected $display_market;

    protected $articlemarkettheme1;

    protected $articlemarkettheme2;

    protected $articlemarkettheme3;

    protected $submit;

    public function __construct(Article $article = null) {

        $wizard = new TP_ResaleWizard();

        $this->setAction('/resale/articlestep3');
        
        $this->addAttribs(array('class' => 'niceform'));

        $this->display_market = new Zend_Form_Element_Checkbox('resale');
        $this->display_market->setLabel('Im Marktplatz anzeigen')
                    ->setValue($wizard->getDisplayMarket());
                    
        $this->articlemarkettheme1 = new Zend_Form_Element_Hidden('articlemarkettheme1');
        $this->articlemarkettheme1
                        ->setValue($wizard->getArticleThemeMarket1());

        $this->articlemarkettheme2 = new Zend_Form_Element_Hidden('articlemarkettheme2');
        $this->articlemarkettheme2
                        ->setValue($wizard->getArticleThemeMarket2());

        $this->articlemarkettheme3 = new Zend_Form_Element_Hidden('articlemarkettheme3');
        $this->articlemarkettheme3
                        ->setValue($wizard->getArticleThemeMarket3());


        $this->submit = new Zend_Form_Element_Submit('Weiter');

        $this->addElements(array(
            $this->display_market,
            $this->articlemarkettheme1,
            $this->articlemarkettheme2,
            $this->articlemarkettheme3,
            $this->submit
        ));

        $this->addDecorators(array(
            'FormElements',
                array('HtmlTag', array('tag' => 'dl')),
            'Form'
        ));
    }
    
    public function storeInSession() {
        $wizard = new TP_ResaleWizard();
        $wizard->setArticleStep3(
            $this->display_market->getValue(),
            $this->articlemarkettheme1->getValue(),
            $this->articlemarkettheme2->getValue(),
            $this->articlemarkettheme3->getValue()
        );
    }

}