<?php


class TP_Forms_Resale_ArticleStep4 extends Zend_Form {

	protected $account;
	
	protected $contact;
	
    protected $private;

    protected $submit;

    public function __construct() {

        $this->setAttribs(array('class' => 'niceform', 'id' => 'articlestep4'));

        $wizard = new TP_ResaleWizard();  

        $this->setAction('/resale/articlestep4');

        $this->private = new Zend_Form_Element_Checkbox('private');
        $this->private->setLabel('Privatisieren')
                    ->setValue($wizard->getArticlePrivate());

        $this->account = new Zend_Form_Element_Select('account');
        $this->account->setLabel('Account');
        
        $shop = $wizard->getArticleShop();
        
        $accounts = Doctrine_Query::CREATE()->select('a.id as id, a.company as company')->from('Account a')->leftJoin('a.Shops as s')->where('s.id = ?', array($shop))->execute()->toKeyValueArray('id','company');
        
        $this->account->addMultiOption('all', 'Alle');
        $this->account->addMultiOptions($accounts);
        
        $this->contact = new Zend_Form_Element_Multiselect('contact');

        $rows = Doctrine_Query::CREATE()->select('c.id, c.self_email')->from('Contact c')->leftJoin('c.ShopContact as s')->where('s.shop_id = ?', array($shop))->execute()->toKeyValueArray('id','self_email');

        $this->contact->setLabel('Mitglieder')->setAttrib('class', 'navigator');
        $this->contact->addMultiOptions($rows);
       
        $this->submit = new Zend_Form_Element_Submit('submit');
        $this->submit->setLabel('Weiter');


        $this->addElements(array(
            $this->private,
            $this->account,
            $this->contact,
            $this->submit
        ));

        $this->addDecorators(array(
            'FormElements',
                array('HtmlTag', array('tag' => 'dl')),
            'Form'
        ));

    }

    public function storeInSession() {
        $wizard = new TP_ResaleWizard();
        $wizard->setArticleStep4(
            $this->private->getValue()
        );
        $wizard->setArticlePrivateAccount($this->account->getValue());
        $wizard->setArticlePrivateContact($this->contact->getValue());
    }

}