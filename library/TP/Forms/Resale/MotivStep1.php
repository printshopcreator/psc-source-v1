<?php

class TP_Forms_Resale_MotivStep1 extends Zend_Form {

    protected $type;
    protected $submit;
    protected $motive;

    public function __construct($heightRole) {

        $wizard = new TP_ResaleWizard();

        $this->setAction('/resale/motivstep1');

        $this->setAttribs(array('class' => 'niceform', 'id' => 'motivstep1'));


        $this->submit = new Zend_Form_Element_Submit('submit');
        $this->submit->setLabel('Weiter');

        $baseInstall = Zend_Registry::get('install');

        $baseShop = Zend_Registry::get('shop');

        $basket = new TP_ResaleWizard();
        $resaleMotive = $basket->getResaleMotive();

        $this->addElement('checkbox', 'resale_shop', array('class' => 'required', 'label' => 'Im Shop bereitstellen?', 'size' => 40, 'value' => $resaleMotive['resale_shop']));

        $this->addElement('checkbox', 'resale_market', array('class' => 'required', 'label' => 'Im Marktplatz bereitstellen?', 'size' => 40, 'value' => $resaleMotive['resale_market']));

        $this->addElement(new TP_Form_Element_Motivprice('price1'));
        $this->price1->setOptions(array('class' => 'required numberDE', 'label' => 'Preis Staffel 1', 'size' => 4, 'value' => $resaleMotive['price1']))
                ->addValidator('Between', false, array('min' => -1, 'max' => 1000));

        $this->addElement(new TP_Form_Element_Motivprice('price2'));
        $this->price2->setOptions(array('class' => 'required numberDE', 'label' => 'Preis Staffel 2', 'size' => 4, 'value' => $resaleMotive['price2']))
                ->addValidator('Between', false, array('min' => -1, 'max' => 1000));

        $this->addElement(new TP_Form_Element_Motivprice('price3'));
        $this->price3->setOptions(array('class' => 'required numberDE', 'label' => 'Preis Staffel 3', 'size' => 4, 'value' => $resaleMotive['price3']))
                ->addValidator('Between', false, array('min' => -1, 'max' => 1000));

        $this->addElement('checkbox', 'resale_download', array('class' => 'required', 'label' => 'Als Download bereitstellen?', 'size' => 40, 'value' => $resaleMotive['resale_download']));

        $this->addElement(new TP_Form_Element_Motivprice('download_price'));
        $this->download_price->setOptions(array('class' => 'required numberDE', 'label' => 'Download Preis', 'size' => 4, 'value' => $resaleMotive['download_price']))
                ->addValidator('Between', false, array('min' => -1, 'max' => 1000));

        $images = new Zend_Session_Namespace('buymotive');

        $images->unlock();

        if (!$images->product_motive)
            $images->product_motive = array();

        if (!$images->motive)
            $images->motive = array();

        foreach($images->product_motive as $key => $row) {
            $this->addElement('text', 'title_'.md5($key), array('class' => 'required', 'label' => 'Name', 'size' => 40, 'value' => $resaleMotive['motivnames'][$key]));
            $this->getElement('title_'.md5($key))->addValidator('regex', false, array('pattern' => '/^[a-zA-Z0-9öÖäÄüÜß&\\s\"\_\-\.]+$/'))->setRequired(true);

            $this->getElement('title_'.md5($key))->addFilter(new TP_Filter_Badwords());
        }

        foreach($images->motive as $key => $row) {
            $this->addElement('text', 'title_'.md5($key), array('class' => 'required', 'label' => 'Name', 'size' => 40, 'value' => $resaleMotive['motivnames'][$key]));
            $this->getElement('title_'.md5($key))->addValidator('regex', false, array('pattern' => '/^[a-zA-Z0-9öÖäÄüÜß&\\s\"\_\-\.]+$/'))->setRequired(true);

            $this->getElement('title_'.md5($key))->addFilter(new TP_Filter_Badwords());
        }
        

        $this->addElement('text', 'copyright', array('class' => 'required', 'label' => 'Copyright', 'size' => 10, 'value' => $resaleMotive['copyright']));
        $this->getElement('copyright')->addFilter(new TP_Filter_Badwords());

        $this->addElement('text', 'tags', array('label' => 'Tags (Tag1,Tag2,...)', 'size' => 40, 'value' => $resaleMotive['tags']));

        $this->addElement('hidden', 'motivtheme1', array('value' => $resaleMotive['motivtheme1']));
        $this->addElement('hidden', 'motivtheme2', array('value' => $resaleMotive['motivtheme2']));
        $this->addElement('hidden', 'motivtheme3', array('value' => $resaleMotive['motivtheme3']));


        $this->shop = new Zend_Form_Element_Select('shop');
        $this->shop->setLabel('Shop')
                        ->setRequired(true)
                        ->setValue($resaleMotive['shop'])
                        ->setAttrib('class', 'required');

        $user = Zend_Auth::getInstance()->getIdentity();
        $user = Doctrine_Query::create()
                ->from('Contact m')
                ->useQueryCache(true)
                ->where('id = ?')->fetchOne(array($user['id']));

        $install = Zend_Registry::get('install');

        foreach($user->Shops as $shop) {
            if($shop->id != $install['defaultmarket'] || $heightRole->level >= 40)
                $this->shop->addMultiOption($shop->id, $shop->name);
        }

        $this->addElement('hidden', 'markettheme1', array('value' => $resaleMotive['markettheme1']));
        $this->addElement('hidden', 'markettheme2', array('value' => $resaleMotive['markettheme2']));
        $this->addElement('hidden', 'markettheme3', array('value' => $resaleMotive['markettheme3']));


        $this->addElement($this->submit);

        $this->addDecorators(array(
            'FormElements',
            array('HtmlTag', array('tag' => 'dl')),
            'Form'
        ));
    }

    protected function getMotivTheme($id, $alltheme, $i) {
        $motivthemes = Doctrine_Query::create()
                        ->select('a.title as title, a.id as id')
                        ->from('MotivTheme as a')
                        ->where('a.parent_id = ?', array($id))
                        ->execute();

        foreach ($motivthemes as $motivtheme) {
            $alltheme[$motivtheme->id] = $i . $motivtheme->title;

            $alltheme = $this->getMotivTheme($motivtheme->id, $alltheme, '-' . $i);
        }
        return $alltheme;
    }

    public function storeInSession($with_resale = true) {

        $wizard = new TP_ResaleWizard();

        $motiveNames = array();

        $images = new Zend_Session_Namespace('buymotive');

        $images->unlock();

        if (!$images->product_motive)
            $images->product_motive = array();

        if (!$images->motive)
            $images->motive = array();


        foreach($images->product_motive as $key => $row) {
            $motiveNames[$key] = $this->getElement('title_'.md5($key))->getValue();
        }

        foreach($images->motive as $key => $row) {
            $motiveNames[$key] = $this->getElement('title_'.md5($key))->getValue();
        }
        
        if($with_resale) {

        $wizard->setMotivStep1(array(

            'resale_shop' => $this->resale_shop->getValue(),
            'resale_market' => $this->resale_market->getValue(),
            'resale_download' => $this->resale_download->getValue(),
            'price1' => $this->price1->getValue(),
            'price2' => $this->price2->getValue(),
            'price3' => $this->price3->getValue(),
            'download_price' => $this->download_price->getValue(),
            'copyright' => $this->copyright->getValue(),
            'tags' => $this->tags->getValue(),
            'shop' => $this->shop->getValue(),
            'motivtheme1' => $this->motivtheme1->getValue(),
            'motivtheme2' => $this->motivtheme2->getValue(),
            'motivtheme3' => $this->motivtheme3->getValue(),
            'markettheme1' => $this->markettheme1->getValue(),
            'markettheme2' => $this->markettheme2->getValue(),
            'markettheme3' => $this->markettheme3->getValue(),
            'motivnames' => $motiveNames
        ));

        }else{
        	$resaleMotive = $wizard->getResaleMotive();

        
        	$wizard->setMotivStep1(array(

            'resale_shop' => $this->resale_shop->getValue(),
            'resale_market' => $resaleMotive['resale_market'],
            'resale_download' => $this->resale_download->getValue(),
            'price1' => $this->price1->getValue(),
            'price2' => $this->price2->getValue(),
            'price3' => $this->price3->getValue(),
            'download_price' => $this->download_price->getValue(),
            'copyright' => $this->copyright->getValue(),
            'tags' => $this->tags->getValue(),
            'shop' => $this->shop->getValue(),
            'motivtheme1' => $this->motivtheme1->getValue(),
            'motivtheme2' => $this->motivtheme2->getValue(),
            'motivtheme3' => $this->motivtheme3->getValue(),
            'markettheme1' => $this->markettheme1->getValue(),
            'markettheme2' => $this->markettheme2->getValue(),
            'markettheme3' => $this->markettheme3->getValue(),
            'motivnames' => $motiveNames
        ));

        }
        
    }

    protected function getMotivThemeText($id, $text) {
        $articlethemes = Doctrine_Query::create ()->select('a.title as title, a.id as id')->from('MotivTheme as a')->where('a.id=?', array($id))->fetchOne();
        if ($articlethemes->parent_id != 0) {
            $text .= $this->getMotivThemeText($articlethemes->parent_id, $text);
        }
        $text .= ' ' . $articlethemes->title . ' >';
        return $text;
    }

}