<?php


class TP_Forms_Resale_MotivStep2 extends Zend_Form {

    protected $agb;

    protected $submit;
    
    protected $captcha;

    public function __construct() {

        

        $wizard = new TP_ResaleWizard();

        $this->setAttribs(array('class' => 'niceform', 'id' => 'motivstep2'));

        $this->setAction('/resale/motivstep3');
        

    	$install = Zend_Registry::get('install');
        $i = 1;
        
        foreach (explode('<br />', nl2br($install['check_agb_motiv'])) as $row) {
            if($row != "") {
            $row=str_replace('\r\n','',$row);
                $row=str_replace('\n','',$row);
            $row=str_replace('\r','',$row);

            $name = 'agb'.$i;

                $this->$name = new Zend_Form_Element_Checkbox('agb'.$i);
                $this->$name->setAttrib('size', 30)
                    ->setRequired(true)
                    ->setAttrib('class', 'required')
                    ->setUncheckedValue('')
                    ->setLabel($row);
            $this->addElement($this->$name);
           $i++;
            }
        }

        $this->captcha = new Zend_Form_Element_Captcha('cp', array(
            'label' => "Bitte die Buchstaben aus dem Bild eingeben",
            'captcha' => array(
                'captcha' => 'Image',
                'font'    => PUBLIC_PATH . '/layouter/fonts/verdana.ttf',
                'imgDir'  => PUBLIC_PATH . '/temp/thumb/',
                'imgUrl'  => '/temp/thumb/',
                'wordLen' => '4'
            ),
        ));
        
        $this->submit = new Zend_Form_Element_Submit('submit');
        $this->submit->setLabel('Fertigstellen');
        
        $this->addElements(array(
            $this->captcha,
            $this->submit
        ));

        $this->addDecorators(array(
            'FormElements',
                array('HtmlTag', array('tag' => 'dl')),
            'Form'
        ));
    }
    
    public function finishUpload($requestParams) {
        
        $wizard = new TP_ResaleWizard();
        
        $basket = new TP_ResaleWizard();
        $resaleMotive = $basket->getResaleMotive();

        $baseShop = Zend_Registry::get('shop');
        $baseInstall = Zend_Registry::get('install');

        $user = Zend_Auth::getInstance()->getIdentity();
        $user = Doctrine_Query::create()
                ->from('Contact m')
                ->useQueryCache(true)
                ->where('id = ?')->fetchOne(array($user['id']));

        $images = new Zend_Session_Namespace('buymotive');

        $images->unlock();

        if (!$images->product_motive)
            $images->product_motive = array();

        if (!$images->motive)
            $images->motive = array();

        $shop = Doctrine_Query::create ()->from('Shop c')->where('c.id = ?', array($resaleMotive['shop']))->fetchOne();
        if($shop->market) {
            $shopMarket = Doctrine_Query::create ()->from('Shop c')->where('c.id = ?', array($shop->Install->defaultmarket))->fetchOne();
        }else{
            $shopMarket = $shop;
        }
        if(!$shop) {
            $shop = Zend_Registry::get('shop');
        }
        
        foreach($images->motive as $key => $motiv) {

            $path_parts = pathinfo($motiv['file']);
            $st = md5(time());

            $finfo = new finfo(FILEINFO_MIME_TYPE);
            $finfo = $finfo->file($motiv['file']);

            if($finfo != 'image/png' && $finfo != 'image/jpeg' && $finfo != 'image/jpg' && $finfo != 'application/pdf' && $finfo != 'image/tiff' && $finfo != 'image/svg' && $finfo != 'image/svg+xml') {
                continue;
            }

            $ext = "";
            switch($finfo) {
                case "image/png":
                    $ext = 'png';
                break;
                case "image/jpeg":
                case "image/jpg":
                    $ext = 'jpg';
                break;
                case "image/tiff":
                    $ext = 'tiff';
                break;
                case "image/svg":
                    $ext = 'svg';
                break;
                case "image/svg+xml":
                    $ext = 'svg';
                break;
                case "application/pdf":
                    $ext = 'pdf';
                break;
            }
            $filetype=strrchr($motiv['orgfilename'], ".");
            if($filetype=='.ai' || $filetype=='.eps') {
                    $ext = 'png';
            }

            $org = $st.'_'.$motiv['orgfilename'].'.'.$ext;

            $im = new imagick();
            $im->readImage( $motiv['file'] );
            $res = $im->getImageResolution();
            $res['x'] = intval($res['x']);
            if($res['x'] != '300') {
                $im->setImageResolution(300, 300);
            }

            if($im->getImageUnits() == 2) {
                $im->setImageUnits(1);
                $res['x'] = $res['x'] * 2.54;
            }

            if(!file_exists(APPLICATION_PATH . '/../market/motive/' . $user->uuid)) mkdir(APPLICATION_PATH . '/../market/motive/' . $user->uuid, 0777, true);

            $umlaute = Array("/ä/","/ö/","/ü/","/Ä/","/Ö/","/Ü/","/ß/");
            $replace = Array("ae","oe","ue","Ae","Oe","Ue","ss");
            $org = preg_replace($umlaute, $replace, $motiv['orgfilename']);
            $filter = new Zend_Filter_Alnum();
            $org = $filter->filter($org);

            $org = $st.'_'.$org.'.'.$ext;

            copy($motiv['file'], APPLICATION_PATH . '/../market/motive/' . $user->uuid . '/' . $org);
            unlink($motiv['file']);
            $imgwork = $im->clone();
            $imgwork->setImageCompressionQuality(70);
            $imgwork->writeImage(APPLICATION_PATH . '/../market/motive/' . $user->uuid . '/work_' . $org);
            $imgmid = $im->clone();
            $imgmid->thumbnailImage(500,null);
            $imgmid->writeImage(APPLICATION_PATH . '/../market/motive/' . $user->uuid . '/mid_' . $org);
            $imgthumb = $im->clone();
            $imgthumb->thumbnailImage(null,110);
            $imgthumb->writeImage(APPLICATION_PATH . '/../market/motive/' . $user->uuid . '/thumb_' . $org);

            $motive = new Motiv();
            $motive->Account = $user->Account;
            $motive->shop_id = $shop['id'];
            $motive->Contact = $user;

            $motive->Install = $user->Install;
            $motive->resale_shop = false;
            $motive->resale_market = false;
            $motive->resale_download = false;

            $motive->status = $shopMarket['default_motiv_status'];

            $motive->price1 = 0;
            $motive->price2 = 0;
            $motive->price3 = 0;
            $motive->download_price = 0;

            if(!isset($requestParams['title_'.$key]) || $requestParams['title_'.$key] == "") {
                $motive->title = $motiv['orgfilename'];
            }else{
                $motive->title = $requestParams['title_'.$key];
            }
            $motive->dpi_org = $res['x'];
            $motive->dpi_con = '300';
            $motive->orgfilename = $motiv['orgfilename'];

            $motive->file_orginal = $user->uuid . '/' . $org;
            $motive->file_work = $user->uuid . '/work_' . $org;
            $motive->file_mid = $user->uuid . '/mid_' . $org;
            $motive->file_thumb = $user->uuid . '/thumb_' . $org;

            $motive->width = $im->getImageWidth();
            $motive->height = $im->getImageHeight();
            $motive->mid_width = $imgmid->getImageWidth();
            $motive->mid_height = $imgmid->getImageHeight();
            $motive->typ = $im->getFormat();
            $motive->copyright = $resaleMotive['copyright'];
            $motive->exif = Zend_Json::encode($imgmid->getImageProperties( "exif:*" ));
            $motive->save();

            $contactmotiv = new ContactMotiv();
            $contactmotiv->contact_id = $user->id;
            $contactmotiv->motiv_id = $motive->id;
            $contactmotiv->save();

        }

        unset($images->motive);
    }
    

    public function storeInSession() {
        $wizard = new TP_ResaleWizard();
        
        $basket = new TP_ResaleWizard();
        $resaleMotive = $basket->getResaleMotive();

        $baseShop = Zend_Registry::get('shop');
        $baseInstall = Zend_Registry::get('install');

        $user = Zend_Auth::getInstance()->getIdentity();
        $user = Doctrine_Query::create()
                ->from('Contact m')
                ->useQueryCache(true)
                ->where('id = ?')->fetchOne(array($user['id']));

        $images = new Zend_Session_Namespace('buymotive');

        $images->unlock();

        if (!$images->product_motive)
            $images->product_motive = array();

        if (!$images->motive)
            $images->motive = array();

        $shop = Doctrine_Query::create ()->from('Shop c')->where('c.id = ?', array($resaleMotive['shop']))->fetchOne();
        if($shop->market) {
            $shopMarket = Doctrine_Query::create ()->from('Shop c')->where('c.id = ?', array($shop->Install->defaultmarket))->fetchOne();
        }else{
            $shopMarket = $shop;
        }

        foreach($images->product_motive as $key => $value) {
            
            $motiv = Doctrine_Query::create ()->from('Motiv c')->where('c.uuid = ?', array($key))->fetchOne();

            $motiv->title = $resaleMotive['motivnames'][$key];
            $motiv->price1 = $resaleMotive['price1'];
            $motiv->price2 = $resaleMotive['price2'];
            $motiv->price3 = $resaleMotive['price3'];
            $motiv->download_price = $resaleMotive['download_price'];
            $motiv->copyright = $resaleMotive['copyright'];
            $motiv->resale_shop = $resaleMotive['resale_shop'];
            $motiv->resale_market = $resaleMotive['resale_market'];
            $motiv->resale_download = $resaleMotive['resale_download'];
            $motiv->status = $shopMarket['default_motiv_status'];
            $motiv->save();

            if(trim($resaleMotive['tags']) != "") {
                $tagsTemp = array();
                $filter = new TP_Filter_Badwords();
                foreach (explode(',', $resaleMotive['tags']) as $key) {
                    $tag = strtolower(trim($filter->filter($key)));
                    if ($tag) {
                        $tagsTemp[] = array('name' => $tag, 'url' => Doctrine_Inflector::urlize($tag));
                    }
                }

                $m = TP_Mongo::getInstance();
                $collection = $m->motive;
                $collection->save( array_merge($motiv->getRiakData(), array('tags' => $tagsTemp, '_id' => $motiv->uuid)) );
            }

            Doctrine_Query::create ()->delete()->from('MotivThemeMotiv c')->where('c.motiv_id = ?', array($key))->execute();
            Doctrine_Query::create ()->delete()->from('MotivThemeMarketMotiv c')->where('c.motiv_id = ?', array($key))->execute();


            if($resaleMotive['motivtheme1'] != '') {
                $motivetheme = new MotivThemeMotiv();
                $motivetheme->motiv_id = $motiv->id;
                $motivetheme->theme_id = $resaleMotive['motivtheme1'];
                $motivetheme->save();
                
            }

            if($resaleMotive['motivtheme2'] != '') {
                $motivetheme = new MotivThemeMotiv();
                $motivetheme->motiv_id = $motiv->id;
                $motivetheme->theme_id = $resaleMotive['motivtheme2'];
                $motivetheme->save();

            }

            if($resaleMotive['motivtheme3'] != '') {
                $motivetheme = new MotivThemeMotiv();
                $motivetheme->motiv_id = $motiv->id;
                $motivetheme->theme_id = $resaleMotive['motivtheme3'];
                $motivetheme->save();

            }

            if($resaleMotive['markettheme1'] != '') {
                $motivetheme = new MotivThemeMarketMotiv();
                $motivetheme->motiv_id = $motiv->id;
                $motivetheme->theme_id = $resaleMotive['markettheme1'];
                $motivetheme->save();
            }
            if($resaleMotive['markettheme2'] != '') {
                $motivetheme = new MotivThemeMarketMotiv();
                $motivetheme->motiv_id = $motiv->id;
                $motivetheme->theme_id = $resaleMotive['markettheme2'];
                $motivetheme->save();
            }
            if($resaleMotive['markettheme3'] != '') {
                $motivetheme = new MotivThemeMarketMotiv();
                $motivetheme->motiv_id = $motiv->id;
                $motivetheme->theme_id = $resaleMotive['markettheme3'];
                $motivetheme->save();
            }
            
        }

        unset($images->product_motive);

        foreach($images->motive as $key => $motiv) {

            $path_parts = pathinfo($motiv['file']);
            $st = md5(time());

            $finfo = new finfo(FILEINFO_MIME_TYPE);
            $finfo = $finfo->file($motiv['file']);

            if($finfo != 'image/png' && $finfo != 'image/jpeg' && $finfo != 'image/jpg' && $finfo != 'application/pdf' && $finfo != 'image/tiff' && $finfo != 'image/svg' && $finfo != 'image/svg+xml') {
                continue;
            }

            $ext = "";
            switch($finfo) {
                case "image/png":
                    $ext = 'png';
                break;
                case "image/jpeg":
                case "image/jpg":
                    $ext = 'jpg';
                break;
                case "image/tiff":
                    $ext = 'tiff';
                break;
                case "image/svg":
                    $ext = 'svg';
                break;
                case "image/svg+xml":
                    $ext = 'svg';
                break;
                case "application/pdf":
                    $ext = 'pdf';
                break;
            }
            $filetype=strrchr($motiv['orgfilename'], ".");
            if($filetype=='.ai' || $filetype=='.eps') {
                    $ext = 'png';
            }

            $org = $st.'_'.$motiv['orgfilename'].'.'.$ext;

            $im = new imagick();
            $im->readImage( $motiv['file'] );
            $res = $im->getImageResolution();
            $res['x'] = intval($res['x']);
            if($res['x'] != '300') {
                $im->setImageResolution(300, 300);
            }

            if($im->getImageUnits() == 2) {
                $im->setImageUnits(1);
                $res['x'] = $res['x'] * 2.54;
            }

            if(!file_exists(APPLICATION_PATH . '/../market/motive/' . $user->uuid)) mkdir(APPLICATION_PATH . '/../market/motive/' . $user->uuid, 0777, true);

            $umlaute = Array("/ä/","/ö/","/ü/","/Ä/","/Ö/","/Ü/","/ß/");
            $replace = Array("ae","oe","ue","Ae","Oe","Ue","ss");
            $org = preg_replace($umlaute, $replace, $motiv['orgfilename']);
            $filter = new Zend_Filter_Alnum();
            $org = $filter->filter($org);

            $org = $st.'_'.$org.'.'.$ext;

            copy($motiv['file'], APPLICATION_PATH . '/../market/motive/' . $user->uuid . '/' . $org);
            unlink($motiv['file']);
            $imgwork = $im->clone();
            $imgwork->setImageCompressionQuality(70);
            $imgwork->writeImage(APPLICATION_PATH . '/../market/motive/' . $user->uuid . '/work_' . $org);
            $imgmid = $im->clone();
            $imgmid->thumbnailImage(500,null);
            $imgmid->writeImage(APPLICATION_PATH . '/../market/motive/' . $user->uuid . '/mid_' . $org);
            $imgthumb = $im->clone();
            $imgthumb->thumbnailImage(null,110);
            $imgthumb->writeImage(APPLICATION_PATH . '/../market/motive/' . $user->uuid . '/thumb_' . $org);

            $motive = new Motiv();
            $motive->Account = $user->Account;
            $motive->shop_id = $shop->id;
            $motive->Contact = $user;

            $motive->Install = $user->Install;
            $motive->resale_shop = $resaleMotive['resale_shop'];
            $motive->resale_market = $resaleMotive['resale_market'];
            $motive->resale_download = $resaleMotive['resale_download'];

            $motive->status = $shopMarket['default_motiv_status'];

            $motive->price1 = $resaleMotive['price1'];
            $motive->price2 = $resaleMotive['price2'];
            $motive->price3 = $resaleMotive['price3'];
            $motive->download_price = $resaleMotive['download_price'];

            $motive->title = $resaleMotive['motivnames'][$key];
            $motive->dpi_org = $res['x'];
            $motive->dpi_con = '300';
            $motive->orgfilename = $motiv['orgfilename'];

            $motive->file_orginal = $user->uuid . '/' . $org;
            $motive->file_work = $user->uuid . '/work_' . $org;
            $motive->file_mid = $user->uuid . '/mid_' . $org;
            $motive->file_thumb = $user->uuid . '/thumb_' . $org;

            $motive->width = $im->getImageWidth();
            $motive->height = $im->getImageHeight();
            $motive->mid_width = $imgmid->getImageWidth();
            $motive->mid_height = $imgmid->getImageHeight();
            $motive->typ = $im->getFormat();
            $motive->copyright = $resaleMotive['copyright'];
            $motive->exif = Zend_Json::encode($imgmid->getImageProperties( "exif:*" ));
            $motive->save();

            $contactmotiv = new ContactMotiv();
            $contactmotiv->contact_id = $user->id;
            $contactmotiv->motiv_id = $motive->id;
            $contactmotiv->save();


            if(trim($resaleMotive['tags']) != "") {
                $tagsTemp = array();
                $filter = new TP_Filter_Badwords();
                foreach (explode(',', $resaleMotive['tags']) as $key) {
                    $tag = strtolower(trim($filter->filter($key)));
                    if ($tag) {
                        $tagsTemp[] = array('name' => $tag, 'url' => Doctrine_Inflector::urlize($tag));
                    }
                }

                $m = TP_Mongo::getInstance();
                $collection = $m->motive;
                $collection->save( array_merge($motive->getRiakData(), array('tags' => $tagsTemp, '_id' => $motive->uuid)) );
            }

            if($resaleMotive['motivtheme1'] != '') {
                $motivetheme = new MotivThemeMotiv();
                $motivetheme->motiv_id = $motive->id;
                $motivetheme->theme_id = $resaleMotive['motivtheme1'];
                $motivetheme->save();

            }

            if($resaleMotive['motivtheme2'] != '') {
                $motivetheme = new MotivThemeMotiv();
                $motivetheme->motiv_id = $motive->id;
                $motivetheme->theme_id = $resaleMotive['motivtheme2'];
                $motivetheme->save();

            }

            if($resaleMotive['motivtheme3'] != '') {
                $motivetheme = new MotivThemeMotiv();
                $motivetheme->motiv_id = $motive->id;
                $motivetheme->theme_id = $resaleMotive['motivtheme3'];
                $motivetheme->save();

            }

            if($resaleMotive['markettheme1'] != '') {
                $motivetheme = new MotivThemeMarketMotiv();
                $motivetheme->motiv_id = $motive->id;
                $motivetheme->theme_id = $resaleMotive['markettheme1'];
                $motivetheme->save();
            }
            if($resaleMotive['markettheme2'] != '') {
                $motivetheme = new MotivThemeMarketMotiv();
                $motivetheme->motiv_id = $motive->id;
                $motivetheme->theme_id = $resaleMotive['markettheme2'];
                $motivetheme->save();
            }
            if($resaleMotive['markettheme3'] != '') {
                $motivetheme = new MotivThemeMarketMotiv();
                $motivetheme->motiv_id = $motive->id;
                $motivetheme->theme_id = $resaleMotive['markettheme3'];
                $motivetheme->save();
            }

        }

        unset($images->motive);
    }

}