<?php

class TP_Forms_Resale_MotivStep21 extends Zend_Form {

    protected $type;
    protected $submit;
    protected $motive;

    public function __construct() {

        $wizard = new TP_ResaleWizard();

        $this->setAction('/resale/motivstep1');

        $this->setAttribs(array('class' => 'niceform', 'id' => 'motivstep1'));


        $this->submit = new Zend_Form_Element_Submit('submit');
        $this->submit->setLabel('Weiter');

        $baseInstall = Zend_Registry::get('install');

        $baseShop = Zend_Registry::get('shop');

        $basket = new TP_ResaleWizard();
    
        $resaleMotive = $basket->getResaleMotive();
        
        $this->addElement('checkbox', 'resale_market', array('class' => 'required', 'label' => 'Im Marktplatz bereitstellen?', 'size' => 40, 'value' => $resaleMotive['resale_market']));
        
        $this->addElement('hidden', 'motivtheme1', array('value' => $resaleMotive['motivtheme1']));
        $this->addElement('hidden', 'motivtheme2', array('value' => $resaleMotive['motivtheme2']));
        $this->addElement('hidden', 'motivtheme3', array('value' => $resaleMotive['motivtheme3']));

        $this->addElement('hidden', 'markettheme1', array('value' => $resaleMotive['markettheme1']));
        $this->addElement('hidden', 'markettheme2', array('value' => $resaleMotive['markettheme2']));
        $this->addElement('hidden', 'markettheme3', array('value' => $resaleMotive['markettheme3']));


        $this->addElement($this->submit);

        $this->addDecorators(array(
            'FormElements',
            array('HtmlTag', array('tag' => 'dl')),
            'Form'
        ));
    }

    protected function getMotivTheme($id, $alltheme, $i) {
        $motivthemes = Doctrine_Query::create()
                        ->select('a.title as title, a.id as id')
                        ->from('MotivTheme as a')
                        ->where('a.parent_id = ?', array($id))
                        ->execute();

        foreach ($motivthemes as $motivtheme) {
            $alltheme[$motivtheme->id] = $i . $motivtheme->title;

            $alltheme = $this->getMotivTheme($motivtheme->id, $alltheme, '-' . $i);
        }
        return $alltheme;
    }

    public function storeInSession() {

        $wizard = new TP_ResaleWizard();

        $motive = $wizard->getResaleMotive();
        $motive['resale_market'] = $this->resale_market->getValue();
        $motive['motivtheme1'] = $this->motivtheme1->getValue();
        $motive['motivtheme2'] = $this->motivtheme2->getValue();
        $motive['motivtheme3'] = $this->motivtheme3->getValue();
        $motive['markettheme1'] = $this->markettheme1->getValue();
        $motive['markettheme2'] = $this->markettheme2->getValue();
        $motive['markettheme3'] = $this->markettheme3->getValue();
        $wizard->setMotivStep1($motive);

        
        
    }

    protected function getMotivThemeText($id, $text) {
        $articlethemes = Doctrine_Query::create ()->select('a.title as title, a.id as id')->from('MotivTheme as a')->where('a.id=?', array($id))->fetchOne();
        if ($articlethemes->parent_id != 0) {
            $text .= $this->getMotivThemeText($articlethemes->parent_id, $text);
        }
        $text .= ' ' . $articlethemes->title . ' >';
        return $text;
    }

}