<?php


class TP_Forms_Resale_ShopStep0 extends EasyBib_Form {

    protected $type;

    protected $submit;

    public function __construct($role) {

        $wizard = new TP_ResaleWizard();

        $this->setAction('/resale/shopstep0');

        $this->setAttribs(array('class' => 'niceform', 'id' => 'shopstep0'));
       

        $this->submit = new Zend_Form_Element_Submit('Weiter');

        $baseInstall = Zend_Registry::get('install');
        
        $this->type = new Zend_Form_Element_Radio('type');
        $this->type->setLabel('Shoptyp')
                        ->setRequired(true)
                        ->setAttrib('class', 'required')
                        ->setValue($wizard->getType());

        if($role->level >= 40) {
            $this->type->addMultiOptions(array(
                    '1' => 'Offener Shop',
                    '2' => 'Privater Shop',
                    '3' => 'eigenständiger, offener Printshop (MyWeb2Print)',
                    '4' => 'eigenständiger, passwortgeschützter Mandantenshop (MyWeb2Print)'
            ));
        }else{
            $this->type->addMultiOptions(array(
                    '1' => 'Offener Shop',
                    '2' => 'Privater Shop'
            ));
        };

        $this->addElements(array(
            $this->type,
            $this->submit
        ));

        $this->addDecorators(array(
            'FormElements',
                array('HtmlTag', array('tag' => 'dl')),
            'Form'
        ));
    }

    public function storeInSession() {
        $wizard = new TP_ResaleWizard();
        $wizard->setStep0(
            $this->type->getValue()
        );
    }

}