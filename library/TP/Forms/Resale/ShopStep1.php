<?php

class TP_Forms_Resale_ShopStep1 extends EasyBib_Form {

    protected $name;
    protected $description;
    protected $keywords;
    protected $shoptheme1;
    protected $shoptheme2;
    protected $shoptheme3;
    protected $shop_kto = false;
    protected $shop_blz = false;
    protected $shop_bank_name = false;
    protected $shop_iban = false;
    protected $shop_bic = false;
    protected $logo;
    protected $submit;

    public function __construct() {

        $wizard = new TP_ResaleWizard();

        $this->setAction('/resale/shopstep1');

        $this->setAttribs(array('class' => 'niceform', 'id' => 'shopstep1'));

        $this->name = new Zend_Form_Element_Text('name');
        $this->name->setAttrib('size', 37)
        ->setRequired(true)
        ->setLabel('Name')
        ->addValidator('regex', false, array('pattern' => '/^[a-zA-Z0-9öÖäÄüÜß&\\s\"\_\-\.]+$/'))
        ->addFilter(new TP_Filter_Badwords())
        ->setAttrib('class', 'required')
        ->setValue($wizard->getShopName())
        ->addPrefixPath('TP_Validate', 'TP/Validate/', 'Validate')
        ->addValidator('Db_NoDomainRecordExistsDoctrine', false, array('table' => 'domain', 'field' => 'name'));

        $this->description = new Zend_Form_Element_Textarea('description');
        $this->description->setAttrib('rows', 6);
        $this->description->setAttrib('cols', 50);
        $this->description->setLabel('Beschreibung')
        ->setRequired(true)
        ->addFilter(new TP_Filter_Badwords())
        ->setAttrib('class', 'required')
        ->setValue($wizard->getShopDescription());

        $this->keywords = new Zend_Form_Element_Text('keywords');
        $this->keywords->setLabel('Keywords');
        $this->keywords->setAttrib('size', 37)
        ->setRequired(true)
        ->addFilter(new TP_Filter_Badwords())
        ->setAttrib('class', 'required')
        ->setValue($wizard->getShopKeywords());

        $this->logo = new Zend_Form_Element_File('logo');
        $this->logo->setLabel('Logo');
        $this->logo->addValidator('Extension', false, 'jpg,gif,png,jpeg,tiff');

        $id = TP_Util::uuid();

        $this->logo->setDestination('temp/');

        $this->logo->addFilter('Rename', 'temp/' . $id . '_' . $this->logo->getFileName(null, false));


        $this->submit = new Zend_Form_Element_Submit('Weiter');

        if ($wizard->getType() != 2 && $wizard->getType() != 3 && $wizard->getType() != 4) {
            $this->shoptheme1 = new Zend_Form_Element_Hidden('shoptheme1');
            $this->shoptheme1->setValue($wizard->getShopTheme1());

            $this->shoptheme2 = new Zend_Form_Element_Hidden('shoptheme2');
            $this->shoptheme2->setValue($wizard->getShopTheme2());

            $this->shoptheme3 = new Zend_Form_Element_Hidden('shoptheme3');
            $this->shoptheme3->setValue($wizard->getShopTheme3());


            

            $this->addElements(array(
                $this->name,
                $this->description,
                $this->keywords,
                $this->shoptheme1,
                $this->shoptheme2,
                $this->shoptheme3,
                $this->logo
            ));
        } else {
            $this->addElements(array(
                $this->name,
                $this->description,
                $this->keywords,
                $this->logo
            ));
        }


        $user = Zend_Auth::getInstance()->getIdentity();

        if ($wizard->getShopKto() == "") {

            $contact = Doctrine_Query::create()->from('Contact as c')->where('c.id = ?', array($user ['id']))->fetchOne();

            if($contact->shop_kto != "") {
                $wizard->setBank(
                    $contact->shop_kto,
                    $contact->shop_blz,
                    $contact->shop_bank_name,
                    $contact->shop_bic,
                    $contact->shop_iban
                );
            }
        }


        $this->shop_kto = new Zend_Form_Element_Text('shop_kto');
        $this->shop_kto->setLabel('Konto Nr.:');
        $this->shop_kto->setAttrib('size', 37)
        ->setRequired(true)
        ->addFilter(new TP_Filter_Badwords())
        ->setAttrib('class', 'required')
        ->setValue($wizard->getShopKto());

        $this->shop_blz = new Zend_Form_Element_Text('shop_blz');
        $this->shop_blz->setLabel('BLZ:');
        $this->shop_blz->setAttrib('size', 37)
        ->setRequired(true)
        ->addFilter(new TP_Filter_Badwords())
        ->setAttrib('class', 'required')
        ->setValue($wizard->getShopBlz());

        $this->shop_bank_name = new Zend_Form_Element_Text('shop_bank_name');
        $this->shop_bank_name->setLabel('Bankname:');
        $this->shop_bank_name->setAttrib('size', 37)
        ->setRequired(true)
        ->addFilter(new TP_Filter_Badwords())
        ->setAttrib('class', 'required')
        ->setValue($wizard->getShopBankName());

        $this->shop_bic = new Zend_Form_Element_Text('shop_bic');
        $this->shop_bic->setLabel('BIC:');
        $this->shop_bic->setAttrib('size', 37)
        ->addValidator('Alnum')
        ->addFilter(new TP_Filter_Badwords())
        ->setValue($wizard->getShopBic());

        $this->shop_iban = new Zend_Form_Element_Text('shop_iban');
        $this->shop_iban->setLabel('Iban:');
        $this->shop_iban->setAttrib('size', 37)
        ->addValidator('Alnum')
        ->addFilter(new TP_Filter_Badwords())
        ->setValue($wizard->getShopIban());

        $subForm = new Zend_Form_SubForm();
        $subForm->setLegend('Bankverbindung zur Abrechnung');
        $subForm->addElements(array(
            $this->shop_kto,
            $this->shop_bank_name,
            $this->shop_blz,
            $this->shop_bic,
            $this->shop_iban
        ));
        $this->addSubForm($subForm, 'shop_bank');
        


        $this->addElements(array(
            $this->submit));

        $this->addDecorators(array(
            'FormElements',
            array('HtmlTag', array('tag' => 'dl')),
            'Form'
        ));
    }

    protected function getShopTheme($id, $temparttheme, $i) {
        $articlethemes = Doctrine_Query::create()
                ->select('a.title as title, a.id as id')
                ->from('ShopTheme as a')
                ->where('a.parent_id = ?', array($id))
                ->execute();
        foreach ($articlethemes as $articletheme) {
            $temparttheme[$articletheme->id] = $i . ' ' . $articletheme->title;

            $temparttheme = $this->getShopTheme($articletheme->id, $temparttheme, '-' . $i);
        }
        return $temparttheme;
    }

    public function storeInSession($file = false) {

        $wizard = new TP_ResaleWizard();
        if ($wizard->getType() != 2 && $wizard->getType() != 3 && $wizard->getType() != 4) {

            $wizard->setStep1(
                    $this->name->getValue(),
                    $this->description->getValue(),
                    $this->keywords->getValue(),
                    $this->shoptheme1->getValue(),
                    $this->shoptheme2->getValue(),
                    $this->shoptheme3->getValue()
            );
            if ($file) {
                $wizard->setLogo($file->id);
            }
        } else {
            $wizard->setStep1Simple(
                    $this->name->getValue(),
                    $this->description->getValue(),
                    $this->keywords->getValue()
            );
            if ($file) {
                $wizard->setLogo($file->id);
            }
        }

        $wizard->setBank(
                $this->shop_kto->getValue(),
                $this->shop_blz->getValue(),
                $this->shop_bank_name->getValue(),
                $this->shop_bic->getValue(),
                $this->shop_iban->getValue()
        );
        
    }

}

