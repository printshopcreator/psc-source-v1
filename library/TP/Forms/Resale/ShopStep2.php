<?php


class TP_Forms_Resale_ShopStep2 extends Zend_Form {

    protected $design;

    protected $submit;

    public function __construct($options) {

        $wizard = new TP_ResaleWizard();

        $this->setAction('/resale/shopstep2');

        $this->setAttribs(array('class' => 'niceform'));
        
        $this->design = new Zend_Form_Element_Radio('design');
        $this->design->setAttrib('size', 30)
                    ->setRequired(true)
                    ->setLabel('Design')
                    ->addMultiOptions($options)
                    ->setValue($wizard->getShopName());


        $this->submit = new Zend_Form_Element_Submit('Weiter');


        $this->addElements(array(
            $this->design,
            $this->submit
        ));

        $this->addDecorators(array(
            'FormElements',
                array('HtmlTag', array('tag' => 'dl')),
            'Form'
        ));
    }

    public function storeInSession() {
        $wizard = new TP_ResaleWizard();
        $wizard->setStep2(
            $this->design->getValue()
        );
    }

}