<?php

class TP_Forms_Resale_ShopStep3 extends Zend_Form {

    protected $agb;
    protected $submit;
    protected $captcha;

    public function __construct() {



        $wizard = new TP_ResaleWizard();

        $this->setAttribs(array('class' => 'niceform', 'id' => 'shopstep3'));

        $this->setAction('/resale/shopstep3');

        $install = Zend_Registry::get('install');
        $i = 1;
        foreach (explode('<br />', nl2br($install['check_agb'])) as $row) {
            if ($row != "") {
                $row = str_replace('\r\n', '', $row);
                $row = str_replace('\n', '', $row);
                $row = str_replace('\r', '', $row);

                $name = 'agb' . $i;

                $this->$name = new Zend_Form_Element_Checkbox('agb' . $i);
                $this->$name->setAttrib('size', 30)
                        ->setRequired(true)
                        ->setAttrib('class', 'required')
                        ->setUncheckedValue('')
                        ->setLabel($row);
                $this->addElement($this->$name);
                $i++;
            }
        }



        $this->captcha = new Zend_Form_Element_Captcha('cp', array(
                    'label' => "Bitte die Buchstaben aus dem Bild eingeben",
                    'captcha' => array(
                        'captcha' => 'Image',
                        'font' => APPLICATION_PATH . '/fonts/verdana.ttf',
                        'imgDir' => PUBLIC_PATH . '/temp/thumb/',
                        'imgUrl' => '/temp/thumb/',
                        'wordLen' => '4'
                    ),
                ));

        $this->submit = new Zend_Form_Element_Submit('Weiter');


        $this->addElements(array(
            $this->captcha,
            $this->submit
        ));

        $this->addDecorators(array(
            'FormElements',
            array('HtmlTag', array('tag' => 'dl')),
            'Form'
        ));
    }

    public function storeInSession() {
        $wizard = new TP_ResaleWizard();
        $wizard->setStep3(
                $this->agb1->getValue()
        );
    }

}