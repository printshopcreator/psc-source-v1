<?php


class TP_Forms_Setting_Aboutus extends Zend_Form {

    protected $betreiber_company;
    protected $betreiber_name;
    protected $betreiber_rechtsform;
    protected $betreiber_address;
    protected $betreiber_street;
    protected $betreiber_email;
    protected $betreiber_tel;
    protected $betreiber_sid;
    protected $betreiber_uid;
    protected $betreiber_hid;
    protected $betreiber_register;
    protected $betreiber_vb;
    protected $betreiber_web;

    protected $beschreibung;

    protected $submit;

    protected $shopconf;

    protected $shop;

    public function __construct($shop) {

        $this->shop = $shop;

        if($this->shop->layout_settings != "") {
            $this->shopconf = Zend_Json::decode($this->shop->layout_settings);
        }else{
            $this->shopconf = array();
        }

        $wizard = new TP_ResaleWizard();

        $this->setAction('/myshops/configaboutus');

        $this->setAttribs(array('class' => 'niceform', 'id' => 'shop_settings'));       

        $this->submit = new Zend_Form_Element_Submit('save');
        $this->submit->setLabel('Speichern');

        $this->betreiber_company = new Zend_Form_Element_Text('betreiber_company');
        $this->betreiber_company->setLabel('Firma');
        $this->betreiber_company->setValue($this->shop->betreiber_company);
        $this->addElement($this->betreiber_company);

        $this->betreiber_rechtsform = new Zend_Form_Element_Text('betreiber_rechtsform');
        $this->betreiber_rechtsform->setLabel('Rechtsform');
        $this->betreiber_rechtsform->setValue($this->shop->betreiber_rechtsform);
        $this->addElement($this->betreiber_rechtsform);

        $this->betreiber_name = new Zend_Form_Element_Text('betreiber_name');
        $this->betreiber_name->setLabel('Name');
        $this->betreiber_name->setValue($this->shop->betreiber_name);
        $this->addElement($this->betreiber_name);

        $this->betreiber_address = new Zend_Form_Element_Text('betreiber_address');
        $this->betreiber_address->setLabel('PLZ/Ort');
        $this->betreiber_address->setValue($this->shop->betreiber_address);
        $this->addElement($this->betreiber_address);

        $this->betreiber_street = new Zend_Form_Element_Text('betreiber_street');
        $this->betreiber_street->setLabel('Straße/Hausnummer');
        $this->betreiber_street->setValue($this->shop->betreiber_street);
        $this->addElement($this->betreiber_street);

        $this->betreiber_email = new Zend_Form_Element_Text('betreiber_email');
        $this->betreiber_email->setLabel('E-Mail');
        $this->betreiber_email->setValue($this->shop->betreiber_email);
        $this->addElement($this->betreiber_email);

        $this->betreiber_tel = new Zend_Form_Element_Text('betreiber_tel');
        $this->betreiber_tel->setLabel('Tel');
        $this->betreiber_tel->setValue($this->shop->betreiber_tel);
        $this->addElement($this->betreiber_tel);

        $this->betreiber_sid = new Zend_Form_Element_Text('betreiber_sid');
        $this->betreiber_sid->setLabel('Steuernummer');
        $this->betreiber_sid->setValue($this->shop->betreiber_sid);
        $this->addElement($this->betreiber_sid);

        $this->betreiber_uid = new Zend_Form_Element_Text('betreiber_uid');
        $this->betreiber_uid->setLabel('Umsatzsteuer Id');
        $this->betreiber_uid->setValue($this->shop->betreiber_uid);
        $this->addElement($this->betreiber_uid);

        $this->betreiber_hid = new Zend_Form_Element_Text('betreiber_hid');
        $this->betreiber_hid->setLabel('Registernummer');
        $this->betreiber_hid->setValue($this->shop->betreiber_hid);
        $this->addElement($this->betreiber_hid);

        $this->betreiber_register = new Zend_Form_Element_Text('betreiber_register');
        $this->betreiber_register->setLabel('Betreiberstand');
        $this->betreiber_register->setValue($this->shop->betreiber_register);
        $this->addElement($this->betreiber_register);

        $this->betreiber_vb = new Zend_Form_Element_Text('betreiber_vb');
        $this->betreiber_vb->setLabel('Vertretungsberechtigter');
        $this->betreiber_vb->setValue($this->shop->betreiber_vb);
        $this->addElement($this->betreiber_vb);

        $this->betreiber_web = new Zend_Form_Element_Text('betreiber_web');
        $this->betreiber_web->setLabel('Homepage');
        $this->betreiber_web->setValue($this->shop->betreiber_web);
        $this->addElement($this->betreiber_web);

        $this->beschreibung = new Zend_Form_Element_Textarea('beschreibung');
        $this->beschreibung->setLabel('Beschreibung');
        $this->beschreibung->setAttribs(array('cols' => 40, 'rows' => 40));
        $this->beschreibung->setValue($this->shop->betreiber_description);

        $this->addElement($this->beschreibung);
        $this->addElement($this->submit);

        $this->addDecorators(array(
            'FormElements',
                array('HtmlTag', array('tag' => 'dl')),
            'Form'
        ));
    }

    public function storeInSession() {
        $this->shop->betreiber_company = $this->betreiber_company->getValue();
        $this->shop->betreiber_name = $this->betreiber_name->getValue();
        $this->shop->betreiber_address = $this->betreiber_address->getValue();
        $this->shop->betreiber_street = $this->betreiber_street->getValue();
        $this->shop->betreiber_email = $this->betreiber_email->getValue();
        $this->shop->betreiber_tel = $this->betreiber_tel->getValue();
        $this->shop->betreiber_uid = $this->betreiber_uid->getValue();
        $this->shop->betreiber_sid = $this->betreiber_sid->getValue();
        $this->shop->betreiber_hid = $this->betreiber_hid->getValue();
        $this->shop->betreiber_register = $this->betreiber_register->getValue();
        $this->shop->betreiber_vb = $this->betreiber_vb->getValue();
        $this->shop->betreiber_rechtsform = $this->betreiber_rechtsform->getValue();
        $this->shop->betreiber_web = $this->betreiber_web->getValue();
        $this->shop->betreiber_description = $this->beschreibung->getValue();
        $this->shop->save();
    }

}