<?php


class TP_Forms_Setting_Article extends Zend_Form {

    protected $title;

    protected $pos;

    protected $description;

    protected $prodpreis;

    protected $resale_price;

    protected $allow_layouter;

    protected $display_market;

    protected $enable;

    protected $submit;

    protected $tags;

    protected $theme;

    protected $articlegroup;

    protected $market_theme;

    protected $parent;

    protected $seitenanzahl;

    protected $image;

    protected $image_remove;

    protected $image2;

    protected $image2_remove;

    protected $row;

    protected $highRole;


    protected $text_art;

    protected $text_format;

    protected $article_nr_intern;

    protected $article_nr_extern;

    protected $meta_keywords;
    protected $meta_description;
    protected $meta_author;
    protected $meta_custom_title;

    protected $vorlage_file;
    protected $vorlage_file_remove;

    protected $vorlage_info;

    public function __construct($row) {

        $user = Zend_Auth::getInstance()->getIdentity();
        $user = Doctrine_Query::create()->from('Contact m')->useQueryCache(true)->where('id = ?')->fetchOne(array(
            $user['id']));
        if ($user != false) {

            foreach ($user->Roles as $rol) {
                if ($this->highRole == null || $rol->level > $this->highRole->level)
                    $this->highRole = $rol;
            }
        }

        $this->addPrefixPath('TP_Form', 'TP/Form/Element/', 'Element');
        $this->row = $row;

        $wizard = new TP_ResaleWizard();

        $this->setAction('/myshops/config');

        $this->setAttribs(array('class' => 'niceform', 'id' => 'article_settings'));       

        $this->submit = new Zend_Form_Element_Submit('save');
        $this->submit->setLabel('Speichern');

        $this->title = new Zend_Form_Element_Text('title');
        $this->title->setAttrib('size', 37)
            ->setRequired(true)
            ->setLabel('Titel')
            ->addFilter(new TP_Filter_Badwords())
            ->setAttrib('class', 'required')
            ->setValue($row->title);


        if($this->highRole->level > 25) {
            $this->text_art = new Zend_Form_Element_Text('text_art');
            $this->text_art->setAttrib('size', 37)
                ->setLabel('Art')
                ->addFilter(new TP_Filter_Badwords())
                ->setAttrib('class', 'required')
                ->setValue($row->text_art);

            $this->text_format = new Zend_Form_Element_Text('text_format');
            $this->text_format->setAttrib('size', 37)
                ->setLabel('Format')
                ->addFilter(new TP_Filter_Badwords())
                ->setAttrib('class', 'required')
                ->setValue($row->text_format);

            $this->article_nr_extern = new Zend_Form_Element_Text('article_nr_extern');
            $this->article_nr_extern->setAttrib('size', 37)
                ->setLabel('Nr Extern')
                ->addFilter(new TP_Filter_Badwords())
                ->setAttrib('class', 'required')
                ->setValue($row->article_nr_extern);

            $this->article_nr_intern = new Zend_Form_Element_Text('article_nr_intern');
            $this->article_nr_intern->setAttrib('size', 37)
                ->setLabel('Nr Intern')
                ->addFilter(new TP_Filter_Badwords())
                ->setAttrib('class', 'required')
                ->setValue($row->article_nr_intern);


            $this->pos = new Zend_Form_Element_Text('pos');
            $this->pos->setAttrib('size', 37)
                ->setRequired(true)
                ->setLabel('Position')
                ->addFilter(new TP_Filter_Badwords())
                ->setAttrib('class', 'required')
                ->setValue($row->pos);

            $this->description = new Zend_Form_Element_Textarea('description');
            $this->description->setAttrib('rows', 6);
            $this->description->setAttrib('cols', 50);
            $this->description->setLabel('Beschreibung')
                ->setRequired(true)
                ->addFilter(new TP_Filter_Badwords())
                ->setAttrib('class', 'required')
                ->setValue($row->info);

            $obj = TP_Mongo::getInstance();
            $obj = $obj->articles->findOne(array('_id' => $this->row->uuid));
            $temp = array();
            $searchtags = array();
            if($obj) {
                foreach($obj['tags'] as $row) {
                    $temp[] = $row['name'];
                }
                $searchtags = $temp;
                $temp = implode(',', $temp);
            }

            $this->tags = new Zend_Form_Element_Text('tags');
            $this->tags->setAttrib('size', 37)
                        ->setLabel('Tags (Tag1,Tag2)')
                        ->addFilter(new TP_Filter_Badwords())
                        ->setValue($temp);
        }

        $this->enable = new Zend_Form_Element_Checkbox('enable');
        $this->enable->setLabel('Produkt aktiv')
            ->setValue($this->row->enable);

        $image_path_1 = false;

        if($row->file != "") {
            $image = Doctrine_Query::create()->from('Image c')->where('c.id = ?', array($row->file))->fetchOne();
            require_once(APPLICATION_PATH . '/helpers/Image.php');
            $img = new TP_View_Helper_Image();
            $image_path_1 = $img->image()->thumbnailImage($image['id'], 'articlelist', $image['id'], true, null, false);
        }

        $this->image = new TP_Form_Element_Image('image', array(), $image_path_1);
        $this->image->setLabel('Bild 1');
        $this->image->addValidator('Extension', false, 'jpg,gif,png,jpeg,tiff');

        $id = TP_Util::uuid();

        $this->image->setDestination('temp/');

        $this->image->addFilter('Rename', 'temp/' . $id . '_' . $this->image->getFileName(null, false));

        $this->image_remove = new Zend_Form_Element_Checkbox('image_remove');
        $this->image_remove->clearDecorators();

        if($this->highRole->level > 25) {
            $image_path_2 = false;

            if($row->file1 != "") {
                $image = Doctrine_Query::create()->from('Image c')->where('c.id = ?', array($row->file1))->fetchOne();
                require_once(APPLICATION_PATH . '/helpers/Image.php');
                $img = new TP_View_Helper_Image();
                $image_path_2 = $img->image()->thumbnailImage($image['id'], 'articlelist', $image['id'], true, null, false);
            }

            $this->image2 = new TP_Form_Element_Image('image2', array(), $image_path_2);
            $this->image2->setLabel('Bild 2');
            $this->image2->addValidator('Extension', false, 'jpg,gif,png,jpeg,tiff');

            $id = TP_Util::uuid();

            $this->image2->setDestination('temp/');

            $this->image2->addFilter('Rename', 'temp/' . $id . '_' . $this->image2->getFileName(null, false));

            $this->image2_remove = new Zend_Form_Element_Checkbox('image2_remove');
            $this->image2_remove->clearDecorators();
        }

        if($this->highRole->level > 25) {

            $this->addElements(array(
                $this->title,
                $this->text_art,
                $this->text_format,
                $this->article_nr_extern,
                $this->article_nr_intern,
                $this->pos,
                $this->description,
                $this->tags,
                $this->enable,
                $this->image,
                $this->image_remove,
                $this->image2,
                $this->image2_remove
            ));
        }else{
            $this->addElements(array(
                $this->title,
                $this->enable,
                $this->image,
                $this->image_remove
            ));
        }


        $cur = new Zend_Currency( Zend_Registry::get( 'locale' )->getLanguage() . '_' . Zend_Registry::get( 'locale' )->getRegion() );
        $settings = new TP_Shopverwaltung();
        if($this->row->Shop->market){

            $this->prodpreis = new TP_Form_Element_Textdisplay('prodpreis');
            $this->prodpreis->setValue($cur->toCurrency($row->a4_abpreis))->setLabel('Produktionspreis');

            $this->resale_price = new TP_Form_Element_Price('resale_price');
            $this->resale_price->setAttrib('size', 30)
                        ->setLabel('Resale Price')
                        ->addValidator('Between', false, array('min' => -1, 'max' => 1000))
                        ->setValue(str_replace(".", ",", round($this->row->a6_resale_price*1.19, 2)));



            $this->allow_layouter = new Zend_Form_Element_Checkbox('allow_layouter');
            $this->allow_layouter->setLabel('Als Produkt verkaufen')
                    ->setValue(!$this->row->not_edit);

            $this->display_market = new Zend_Form_Element_Checkbox('resale');
            $this->display_market->setLabel('Im Marktplatz anzeigen')
                        ->setValue($this->row->display_market);



            $selectedthemesm = array();
            foreach($this->row->ArticleThemeMarketArticle as $tp) {
                $selectedthemesm[] = $tp->theme_id;
            }



            $this->market_theme = new Zend_Form_Element_Multiselect('market_theme');
            $this->market_theme->setLabel('Marktplatzthemen')
                             ->setAttrib('size', 10)
                             ->setValue($selectedthemesm);




            $install = Zend_Registry::get('install');
            $article_themesm = Doctrine_Query::create ()->select('a.title as title, a.id as id')->from('ArticleTheme as a')->where('a.shop_id = ? AND a.parent_id = 0', array($install['defaultmarket']))->execute();

            $tmpm = array();
            foreach($article_themesm as $row) {
                $tmpm[$row->id] = $row->title;
                $article_themes_sub = Doctrine_Query::create ()->select('a.title as title, a.id as id')->from('ArticleTheme as a')->where('a.shop_id = ? AND a.parent_id = ?', array($install['defaultmarket'], $row->id))->execute();
                foreach($article_themes_sub as $row_sub) {
                    $tmpm[$row_sub->id] = '---' . $row_sub->title;
                }
            }

            $this->market_theme->addMultiOptions($tmpm);

            $this->addElements(array(
                $this->prodpreis,
                $this->resale_price,
                $this->enable,
                $this->allow_layouter,
                $this->display_market,
                $this->market_theme
            ));

        }

        if($this->highRole->level > 25) {
            $article_groups = Doctrine_Query::create ()->select('a.title as title, a.id as id')->from('ArticleGroup as a')->where('a.shop_id = ? AND a.parent = 0', array($settings->getCurrentShop()))->execute();

            $article_themes = Doctrine_Query::create ()->select('a.title as title, a.id as id')->from('ArticleTheme as a')->where('a.shop_id = ? AND a.parent_id = 0', array($settings->getCurrentShop()))->execute();

            $selectedgroups = array();
            foreach($this->row->ArticleGroupArticle as $tp) {
                $selectedgroups[] = $tp->articlegroup_id;
            }

            $this->articlegroup = new Zend_Form_Element_Multiselect('articlegroup');
            $this->articlegroup->setLabel('Artikelgruppen')
                ->setAttrib('size', 10)
                ->setValue($selectedgroups);

            $tmp = array();
            foreach($article_groups as $rows) {
                $tmp[$rows->id] = $rows->title;
                $article_themes_sub = Doctrine_Query::create ()->select('a.title as title, a.id as id')->from('ArticleGroup as a')->where('a.shop_id = ? AND a.parent = ?', array($this->row->shop_id, $rows->id))->execute();
                foreach($article_themes_sub as $row_sub) {
                    $tmp[$row_sub->id] = '---' . $row_sub->title;

                    $article_themes_sub_sub = Doctrine_Query::create ()->select('a.title as title, a.id as id')->from('ArticleGroup as a')->where('a.shop_id = ? AND a.parent = ?', array($this->row->shop_id, $row_sub->id))->execute();

                    foreach($article_themes_sub_sub as $row_sub_sub) {
                        $tmp[$row_sub_sub->id] = '------' . $row_sub_sub->title;

                        $article_themes_sub_sub_sub = Doctrine_Query::create ()->select('a.title as title, a.id as id')->from('ArticleGroup as a')->where('a.shop_id = ? AND a.parent = ?', array($this->row->shop_id, $row_sub_sub->id))->execute();

                        foreach($article_themes_sub_sub_sub as $row_sub_sub_sub) {
                            $tmp[$row_sub_sub_sub->id] = '---------' . $row_sub_sub_sub->title;

                        }
                    }
                }
            }

            $this->articlegroup->addMultiOptions($tmp);

            $selectedthemes = array();
            foreach($this->row->ArticleThemeArticle as $tp) {
                $selectedthemes[] = $tp->theme_id;
            }

            $this->theme = new Zend_Form_Element_Multiselect('theme');
            $this->theme->setLabel('Shopthemen')
                ->setAttrib('size', 10)
                ->setValue($selectedthemes);

            $tmp = array();
            foreach($article_themes as $rows) {
                $tmp[$rows->id] = $rows->title;
                $article_themes_sub = Doctrine_Query::create ()->select('a.title as title, a.id as id')->from('ArticleTheme as a')->where('a.shop_id = ? AND a.parent_id = ?', array($this->row->shop_id, $rows->id))->execute();
                foreach($article_themes_sub as $row_sub) {
                    $tmp[$row_sub->id] = '---' . $row_sub->title;
                }
            }

            $this->theme->addMultiOptions($tmp);

            $this->meta_keywords = new Zend_Form_Element_Textarea('meta_keywords');
            $this->meta_keywords->setAttrib('rows', 6);
            $this->meta_keywords->setAttrib('cols', 50);
            $this->meta_keywords->setLabel('Meta Keywords')
                ->addFilter(new TP_Filter_Badwords())
                ->setAttrib('class', 'required')
                ->setValue($row->meta_keywords);

            $this->meta_description = new Zend_Form_Element_Textarea('meta_description');
            $this->meta_description->setAttrib('rows', 6);
            $this->meta_description->setAttrib('cols', 50);
            $this->meta_description->setLabel('Meta Beschreibung')
                ->addFilter(new TP_Filter_Badwords())
                ->setAttrib('class', 'required')
                ->setValue($row->meta_description);

            $this->meta_author = new Zend_Form_Element_Textarea('meta_author');
            $this->meta_author->setAttrib('rows', 6);
            $this->meta_author->setAttrib('cols', 50);
            $this->meta_author->setLabel('Meta Author')
                ->addFilter(new TP_Filter_Badwords())
                ->setAttrib('class', 'required')
                ->setValue($row->meta_author);

            $this->meta_custom_title = new Zend_Form_Element_Textarea('meta_custom_title');
            $this->meta_custom_title->setAttrib('rows', 6);
            $this->meta_custom_title->setAttrib('cols', 50);
            $this->meta_custom_title->setLabel('Meta Title')
                ->addFilter(new TP_Filter_Badwords())
                ->setAttrib('class', 'required')
                ->setValue($row->meta_custom_title);

            $this->vorlage_info = new Zend_Form_Element_Textarea('vorlage_info');
            $this->vorlage_info->setAttrib('rows', 6);
            $this->vorlage_info->setAttrib('cols', 50);
            $this->vorlage_info->setLabel('Datenhinweise')
                ->addFilter(new TP_Filter_Badwords())
                ->setAttrib('class', 'required')
                ->setValue($row->vorlage_info);

            $vorlage_file_path_2 = false;

            if($row->vorlage_file != "") {
                $image = Doctrine_Query::create()->from('Image c')->where('c.id = ?', array($row->vorlage_file))->fetchOne();
                require_once(APPLICATION_PATH . '/helpers/Image.php');
                $img = new TP_View_Helper_Image();
                $vorlage_file_path_2 = $img->image()->thumbnailImage($image['id'], 'articlelist', $image['id'], true, null, false);
            }

            $this->vorlage_file = new TP_Form_Element_Image('vorlage_file', array(), $vorlage_file_path_2);
            $this->vorlage_file->setLabel('Datenhinweis Datei');
            $this->vorlage_file->addValidator('Extension', false, 'jpg,gif,png,jpeg,tiff,pdf,doc');

            $id = TP_Util::uuid();

            $this->vorlage_file->setDestination('temp/');

            $this->vorlage_file->addFilter('Rename', 'temp/' . $id . '_' . $this->vorlage_file->getFileName(null, false));

            $this->vorlage_file_remove = new Zend_Form_Element_Checkbox('vorlage_file_remove');
            $this->vorlage_file_remove->clearDecorators();

            $this->addElements(array(
                $this->articlegroup,
                $this->theme,
                $this->meta_author,
                $this->meta_custom_title,
                $this->meta_description,
                $this->meta_keywords,
                $this->vorlage_info,
                $this->vorlage_file,
                $this->vorlage_file_remove,
                $this->submit
            ));
        }else{
            if($this->highRole->level == 25) {

                $xml = simplexml_load_string($this->row->a1_xml);
                $node = $xml->xpath('//option[@id="seitenanzahl"]');
                $this->seitenanzahl = new Zend_Form_Element_Text('seitenanzahl');
                $this->seitenanzahl->setAttrib('size', 37)
                    ->setRequired(true)
                    ->setLabel('Seitenanzahl')
                    ->addFilter(new TP_Filter_Badwords())
                    ->setAttrib('class', 'required')
                    ->setValue($node[0]['default']);

                $this->addElements(array(
                    $this->seitenanzahl
                ));
            }

            $this->addElements(array(
                $this->submit
            ));
        }

        $this->addDecorators(array(
            'FormElements',
                array('HtmlTag', array('tag' => 'dl')),
            'Form'
        ));
    }

    public function storeInSession() {
        $this->row->title = $this->title->getValue();
        if($this->highRole->level > 25) {
            $this->row->text_art = $this->text_art->getValue();
            $this->row->text_format = $this->text_format->getValue();
            $this->row->article_nr_extern = $this->article_nr_extern->getValue();
            $this->row->article_nr_intern = $this->article_nr_intern->getValue();
            $this->row->meta_keywords = $this->meta_keywords->getValue();
            $this->row->meta_description = $this->meta_description->getValue();
            $this->row->meta_author = $this->meta_author->getValue();
            $this->row->meta_custom_title = $this->meta_custom_title->getValue();
            $this->row->vorlage_info = $this->vorlage_info->getValue();

            $this->row->info = $this->description->getValue();
            $this->row->pos = $this->pos->getValue();
        }
        if($this->row->Shop->market){

            $this->row->a6_resale_price = (str_replace(',', '.', $this->resale_price->getValue())/1.19);
            $this->row->display_market = $this->display_market->getValue();
            $this->row->not_edit = !$this->allow_layouter->getValue();
        }

        $this->row->enable = $this->enable->getValue();

        if($this->image->getValue() && $this->image_remove->getValue() != 1){
            $file = new Image ( );
            $file->id = TP_Util::uuid ();
            $file->path = $this->image->getFileName(null, true);
            $file->name = $this->image->getValue();

            $finfo = new finfo(FILEINFO_MIME_TYPE);
            $finfo = $finfo->file($file->path);
            $file->imagetype = $finfo;
            $file->save();

            $this->row->file = $file->id;
        }

        if($this->image_remove->getValue() == 1) {
            $this->row->file = "";
        }
        if($this->highRole->level > 25) {
            if($this->image2->getValue() && $this->image2_remove->getValue() != 1){
                $file = new Image ( );
                $file->id = TP_Util::uuid ();
                $file->path = $this->image2->getFileName(null, true);
                $file->name = $this->image2->getValue();

                $finfo = new finfo(FILEINFO_MIME_TYPE);
                $finfo = $finfo->file($file->path);
                $file->imagetype = $finfo;
                $file->save();

                $this->row->file1 = $file->id;
            }

            if($this->image2_remove->getValue() == 1) {
                $this->row->file1 = "";
            }

            if($this->vorlage_file->getValue() && $this->vorlage_file_remove->getValue() != 1){
                $file = new Image ( );
                $file->id = TP_Util::uuid ();
                $file->path = $this->vorlage_file->getFileName(null, true);
                $file->name = $this->vorlage_file->getValue();

                $finfo = new finfo(FILEINFO_MIME_TYPE);
                $finfo = $finfo->file($file->path);
                $file->imagetype = $finfo;
                $file->save();

                $this->row->vorlage_file = $file->id;
            }

            if($this->vorlage_file_remove->getValue() == 1) {
                $this->row->vorlage_file = "";
            }
        }

        if($this->highRole->level == 25) {
            $xml = simplexml_load_string($this->row->a1_xml);

            $node = $xml->xpath('//option[@id="seitenanzahl"]');

            $node[0]['default'] = $this->seitenanzahl->getValue();

            $node[0]->opt['id'] = $this->seitenanzahl->getValue();
            $node[0]->opt['name'] = $this->seitenanzahl->getValue().' Seiten';


            $this->row->a1_xml = $xml->asXML();
        }

        $this->row->save();
        if($this->highRole->level > 25) {
            Doctrine_Query::create()
                ->delete()
                ->from('ArticleGroupArticle as a')
                ->where('a.article_id = ?', array(intval($this->row->id)))
                ->execute();

            if($this->articlegroup->getValue()) {
                foreach ($this->articlegroup->getValue() as $key) {
                    $group = new ArticleGroupArticle();
                    $group->articlegroup_id = $key;
                    $group->article_id = $this->row->id;
                    $group->save();
                }
            }

            Doctrine_Query::create()
                        ->delete()
                        ->from('ArticleThemeArticle as a')
                        ->where('a.article_id = ?', array(intval($this->row->id)))
                        ->execute();

            if($this->theme->getValue()) {
                foreach ($this->theme->getValue() as $key) {
                    $group = new ArticleThemeArticle();
                    $group->theme_id = $key;
                    $group->article_id = $this->row->id;
                    $group->save();
                }
            }
        }

        if($this->row->Shop->market){

            Doctrine_Query::create()
                ->delete()
                ->from('ArticleThemeMarketArticle as a')
                ->where('a.article_id = ?', array(intval($this->row->id)))
                ->execute();

            if($this->market_theme->getValue()) {
                foreach ($this->market_theme->getValue() as $key) {
                    $group = new ArticleThemeMarketArticle();
                    $group->theme_id = $key;
                    $group->article_id = $this->row->id;
                    $group->save();
                }
            }

        }
        if($this->highRole->level > 25) {

            $filter = new TP_Filter_Badwords();

            $tags = $this->tags->getValue();

            if (isset($tags) && trim($tags) != "") {
                $tagsTemp = array();
                foreach (explode(',', $tags) as $key) {
                    $tag = strtolower(trim($filter->filter($key)));
                    if ($tag) {
                        $tagsTemp[] = array('name' => $tag, 'url' => Doctrine_Inflector::urlize($tag));
                    }
                }

                $obj = TP_Mongo::getInstance();
                $collection = $obj->articles;
                $collection->save(array_merge($this->row->getRiakData(), array('tags' => $tagsTemp, '_id' => $this->row->uuid)));
            }

            $client = new Elastica_Client();
            $index = $client->getIndex('psc');
            $type = $index->getType('product');

            $doc = new Elastica_Document($this->row->uuid, $this->row->getRiakData(), 'product', 'psc');
            $doc->add('tags', $tagsTemp);
            $type->addDocument($doc);

            $index->refresh();
        }
    }

}