<?php


class TP_Forms_Setting_Articlegroup extends Zend_Form {

    protected $title;

    protected $text;

    protected $image;

    protected $pos;

    protected $submit;

    protected $parent;

    protected $image_remove;

    protected $row;

    public function __construct($row, $rows) {

        $this->addPrefixPath('TP_Form', 'TP/Form/Element/', 'Element');
        $this->row = $row;

        $wizard = new TP_ResaleWizard();

        $this->setAction('/myshops/config');

        $this->setAttribs(array('class' => 'niceform', 'id' => 'articlegroup_settings'));       

        $this->submit = new Zend_Form_Element_Submit('save');
        $this->submit->setLabel('Speichern');

        $this->title = new Zend_Form_Element_Text('title');
        $this->title->setAttrib('size', 37)
            ->setRequired(true)
            ->setLabel('Titel')
            ->addFilter(new TP_Filter_Badwords())
            ->setAttrib('class', 'required')
            ->setValue($row->title);

        $this->pos = new Zend_Form_Element_Text('pos');
        $this->pos->setAttrib('size', 37)
            ->setLabel('Position')
            ->setValue($row->pos);

        $groups = array();
        $groups[0] = 'Keiner';
        foreach($rows as $a) {
            $groups[$a->id] = $a->title;
        }

        $this->parent = new Zend_Form_Element_Select('parent');
        $this->parent->setLabel('Vorgänger')
                        ->addMultiOptions($groups)
                        ->setValue($row->parent);

        $this->text = new Zend_Form_Element_Textarea('text');
        $this->text->setAttrib('rows', 6);
        $this->text->setAttrib('cols', 50);
        $this->text->setLabel('Beschreibung')
            ->addFilter(new TP_Filter_Badwords())
            ->setValue($row->text);

        $image_path = false;

        if($row->image != "") {
            $image = Doctrine_Query::create()->from('Image c')->where('c.id = ?', array($row->image))->fetchOne();
            require_once(APPLICATION_PATH . '/helpers/Image.php');
            $img = new TP_View_Helper_Image();
            $image_path = $img->image()->thumbnailImage($image['id'], 'articlelist', $image['id'], true, null, false);
        }

        $this->image = new TP_Form_Element_Image('image', array(), $image_path);
        $this->image->setLabel('Logo');
        $this->image->addValidator('Extension', false, 'jpg,gif,png,jpeg,tiff');

        $id = TP_Util::uuid();

        $this->image->setDestination('temp/');

        $this->image->addFilter('Rename', 'temp/' . $id . '_' . $this->image->getFileName(null, false));

        $this->image_remove = new Zend_Form_Element_Checkbox('image_remove');
        $this->image_remove->clearDecorators();


        $this->addElements(array(
            $this->title,
            $this->pos,
            $this->parent,
            $this->text,
            $this->image,
            $this->image_remove,
            $this->submit
        ));

        $this->addDecorators(array(
            'FormElements',
                array('HtmlTag', array('tag' => 'dl')),
            'Form'
        ));
    }

    public function storeInSession() {
        $this->row->title = $this->title->getValue();
        $this->row->text = $this->text->getValue();
        $this->row->pos = $this->pos->getValue();
        $this->row->parent = $this->parent->getValue();

        if($this->image->getValue() && $this->image_remove->getValue() != 1){
            $file = new Image ( );
            $file->id = TP_Util::uuid ();
            $file->path = $this->image->getFileName(null, true);
            $file->name = $this->image->getValue();

            $finfo = new finfo(FILEINFO_MIME_TYPE);
            $finfo = $finfo->file($file->path);
            $file->imagetype = $finfo;
            $file->save();

            $this->row->image = $file->id;
        }

        if($this->image_remove->getValue() == 1) {
            $this->row->image = "";
        }

        $this->row->save();
    }

}