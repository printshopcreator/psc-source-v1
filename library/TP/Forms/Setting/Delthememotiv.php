<?php


class TP_Forms_Setting_Delthememotiv extends Zend_Form {

    protected $submit;

    protected $row;

    public function __construct($row) {

        $this->addPrefixPath('TP_Form', 'TP/Form/Element/', 'Element');
        $this->row = $row;

        
        $this->setAction('/myshops/config');

        $this->setAttribs(array('class' => 'niceform', 'id' => 'theme_motiv_settings'));       

        $this->submit = new Zend_Form_Element_Submit('save');
        $this->submit->setLabel('Löschen');


        $this->addElements(array(
            $this->submit
        ));

        $this->addDecorators(array(
            'FormElements',
                array('HtmlTag', array('tag' => 'dl')),
            'Form'
        ));
    }

    public function storeInSession() {
        $this->row->delete();
    }

}