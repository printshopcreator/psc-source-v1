<?php


class TP_Forms_Setting_Domain extends Zend_Form {

    protected $name;

    protected $submit;

    protected $row;

    public function __construct($row) {

        $this->addPrefixPath('TP_Form', 'TP/Form/Element/', 'Element');
        $this->row = $row;

        $wizard = new TP_ResaleWizard();

        $this->setAction('/myshops/editdomain');

        $this->setAttribs(array('class' => 'niceform', 'id' => 'domain_settings'));       

        $this->submit = new Zend_Form_Element_Submit('save');
        $this->submit->setLabel('Speichern');

        $this->name = new Zend_Form_Element_Text('name');
        $this->name->setAttrib('size', 37)
            ->setRequired(true)
            ->setLabel('Titel')
            ->addFilter(new TP_Filter_Badwords())
            ->setAttrib('class', 'required')
            ->setValue($row->name);


        $this->addElements(array(
            $this->name,
            $this->submit
        ));

        $this->addDecorators(array(
            'FormElements',
                array('HtmlTag', array('tag' => 'dl')),
            'Form'
        ));
    }

    public function storeInSession() {
        $this->row->name = $this->name->getValue();

        $this->row->save();
    }

}