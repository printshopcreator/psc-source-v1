<?php


class TP_Forms_Setting_Motiv extends Zend_Form {

    protected $title;

    protected $copyright;

    protected $info;

    protected $tags;

    protected $replace;

    protected $resale_shop;

    protected $resale_market;

    protected $price1;

    protected $price2;

    protected $price3;

    protected $theme;

    protected $market_theme;

    protected $submit;

    protected $parent;

    protected $status;

    protected $hightRole;

    protected $row;

    protected $shop;

    public function __construct($row, $hightRole, $shop) {

        $this->shop = $shop;
        $this->hightRole = $hightRole;
        $this->addPrefixPath('TP_Form', 'TP/Form/Element/', 'Element');
        $this->row = $row;

        $wizard = new TP_ResaleWizard();

        $this->setAction('/myshops/config');

        $this->setAttribs(array('class' => 'niceform', 'id' => 'motiv_settings'));       

        $this->submit = new Zend_Form_Element_Submit('save');
        $this->submit->setLabel('Speichern');

        $this->title = new Zend_Form_Element_Text('title');
        $this->title->setAttrib('size', 37)
            ->setRequired(true)
            ->setLabel('Titel')
            ->addFilter(new TP_Filter_Badwords())
            ->setAttrib('class', 'required')
            ->setValue($row->title);

        $this->copyright = new Zend_Form_Element_Text('copyright');
        $this->copyright->setLabel('Copyright')
            ->addFilter(new TP_Filter_Badwords())
            ->setAttrib('class', 'required')
            ->setValue($row->copyright);

        $this->info = new Zend_Form_Element_Textarea('info');
        $this->info->setLabel('Info')
            ->setAttrib("rows", 5)
            ->addFilter(new TP_Filter_Badwords())
            ->setValue($row->info);

        $obj = TP_Mongo::getInstance();
        $obj = $obj->motive->findOne(array('_id' => $this->row->uuid));
        $temp = array();
        $searchtags = array();
        if($obj) {
            foreach($obj['tags'] as $row) {
                $temp[] = $row['name'];
            }
            $searchtags = $temp;
            $temp = implode(',', $temp);
        }

        $this->replace = new Zend_Form_Element_File('replace');
        $this->replace->setLabel('Bild ersetzen mit');
        $this->replace->addValidator('Extension', false, 'jpg,gif,png,jpeg,tiff');

        $this->tags = new Zend_Form_Element_Text('tags');
        $this->tags->setAttrib('size', 37)
                    ->setLabel('Tags (Tag1,Tag2)')
                    ->addFilter(new TP_Filter_Badwords())
                    ->setValue($temp);

        $this->resale_shop = new Zend_Form_Element_Checkbox('resale_shop');
        $this->resale_shop->setLabel('Im Shop bereitstellen?')
                    ->setValue($this->row->resale_shop);

        $this->resale_market = new Zend_Form_Element_Checkbox('resale_market');
        $this->resale_market->setLabel('Im Marktplatz bereitstellen?')
                    ->setValue($this->row->resale_market);

        $this->price1 = new TP_Form_Element_Motivprice('price1');
        $this->price1->setAttrib('size', 4)
                    ->setLabel('Preis Staffel 1')
                    ->addValidator('Between', false, array('min' => -1, 'max' => 1000))
                    ->setValue(str_replace(".", ",", $this->row->price1));
        $this->price2 = new TP_Form_Element_Motivprice('price2');
        $this->price2->setAttrib('size', 4)
                    ->setLabel('Preis Staffel 2')
                    ->addValidator('Between', false, array('min' => -1, 'max' => 1000))
                    ->setValue(str_replace(".", ",", $this->row->price2));
        $this->price3 = new TP_Form_Element_Motivprice('price3');
        $this->price3->setAttrib('size', 4)
                    ->setLabel('Preis Staffel 3')
                    ->addValidator('Between', false, array('min' => -1, 'max' => 1000))
                    ->setValue(str_replace(".", ",", $this->row->price3));

        $selectedthemes = array();
        foreach($this->row->MotivThemeMotiv as $tp) {
            $selectedthemes[] = $tp->theme_id;
        }

        $selectedthemesm = array();
        foreach($this->row->MotivThemeMarketMotiv as $tp) {
            $selectedthemesm[] = $tp->theme_id;
        }

        $this->theme = new Zend_Form_Element_Multiselect('theme');
        $this->theme->setLabel('Shopthemen')
                         ->setAttrib('size', 10)
                         ->setValue($selectedthemes);

        if($this->shop->market) {
            $this->market_theme = new Zend_Form_Element_Multiselect('market_theme');
            $this->market_theme->setLabel('Marktplatzthemen')
                             ->setAttrib('size', 10)
                         ->setValue($selectedthemesm);
        }
        $settings = new TP_Shopverwaltung();
        $motivthemes = Doctrine_Query::create ()->select('a.title as title, a.id as id')->from('Motivtheme as a')->where('a.shop_id = ? AND a.parent_id = 0', array($settings->getCurrentShop()))->execute();
        
        $tmp = array();
        foreach($motivthemes as $row) {
            $tmp[$row->id] = $row->title;
            $motivthemes_sub = Doctrine_Query::create ()->select('a.title as title, a.id as id')->from('Motivtheme as a')->where('a.shop_id = ? AND a.parent_id = ?', array($this->row->shop_id, $row->id))->execute();
            foreach($motivthemes_sub as $row_sub) {
                $tmp[$row_sub->id] = '---' . $row_sub->title;
            }
        }

        $this->theme->addMultiOptions($tmp);

        $install = Zend_Registry::get('install');
        $motivthemesm = Doctrine_Query::create ()->select('a.title as title, a.id as id')->from('Motivtheme as a')->where('a.shop_id = ? AND a.parent_id = 0', array($install['defaultmarket']))->execute();
        
        $tmpm = array();
        foreach($motivthemesm as $row) {
            $tmpm[$row->id] = $row->title;
            $motivthemes_sub = Doctrine_Query::create ()->select('a.title as title, a.id as id')->from('Motivtheme as a')->where('a.shop_id = ? AND a.parent_id = ?', array($install['defaultmarket'], $row->id))->execute();
            foreach($motivthemes_sub as $row_sub) {
                $tmpm[$row_sub->id] = '---' . $row_sub->title;
            }
        }

        if($this->shop->market) {
            $this->market_theme->addMultiOptions($tmpm);
        }

        $this->addElements(array(
            $this->title,
            $this->copyright,
            $this->info,
            $this->replace,
            $this->tags,
            $this->resale_shop
        ));

        if($this->shop->market) {
            $this->market_theme->addMultiOptions($tmpm);
            $this->addElements(array(
                $this->resale_market
            ));
        }

        $this->addElements(array(
            $this->price1,
            $this->price2,
            $this->price3,
            $this->theme,
            $this->market_theme
        ));

        if($hightRole->level >= 40) {
            $this->status = new Zend_Form_Element_Select('status');
            $this->status->setLabel('Status')
                ->setAttrib('size', 10)
                ->setValue($this->row->status);

            $config = new Zend_Config_Ini(APPLICATION_PATH . '/configs/' . '/systemdefaults.ini', 'motivstatus');
            $config = $config->toArray();
            $this->status->addMultiOptions($config);

            $this->addElement($this->status);
        }

        $this->addElement($this->submit);

        $this->addDecorators(array(
            'FormElements',
                array('HtmlTag', array('tag' => 'dl')),
            'Form'
        ));
    }

    public function storeInSession() {
        $this->row->title = $this->title->getValue();
        $this->row->copyright = $this->copyright->getValue();
        $this->row->info = $this->info->getValue();
        $this->row->resale_shop = $this->resale_shop->getValue();
        $this->row->resale_market = $this->resale_market->getValue();
        $this->row->price1 = (str_replace(',', '.', $this->price1->getValue()));
        $this->row->price2 = (str_replace(',', '.', $this->price2->getValue()));
        $this->row->price3 = (str_replace(',', '.', $this->price3->getValue()));
        if($this->hightRole->level >= 40) {
             $this->row->status = $this->status->getValue();
        }
        $this->row->save();

        if($this->replace->getValue()) {

            $im = new imagick();
            $im->readImage( $this->replace->getFileName(null, true));
            $res = $im->getImageResolution();
            $res['x'] = intval($res['x']);
            if($res['x'] != '300') {
                $im->setImageResolution(300, 300);
            }

            if($im->getImageUnits() == 2) {
                $im->setImageUnits(1);
                $res['x'] = $res['x'] * 2.54;
            }


            copy($this->replace->getFileName(null, true), APPLICATION_PATH . '/../market/motive/' . $this->row->file_orginal);
            $imgwork = $im->clone();
            $imgwork->setImageCompressionQuality(70);
            $imgwork->writeImage(APPLICATION_PATH . '/../market/motive/' . $this->row->file_work);
            $imgmid = $im->clone();
            $imgmid->thumbnailImage(500,null);
            $imgmid->writeImage(APPLICATION_PATH . '/../market/motive/' . $this->row->file_mid);
            $imgthumb = $im->clone();
            $imgthumb->thumbnailImage(null,110);
            $imgthumb->writeImage(APPLICATION_PATH . '/../market/motive/' . $this->row->file_thumb);

        }


        Doctrine_Query::create()
                    ->delete()
                    ->from('MotivThemeMotiv as a')
                    ->where('a.motiv_id = ?', array(intval($this->row->id)))
                    ->execute();

        if($this->theme->getValue()) {
            foreach ($this->theme->getValue() as $key) {
                $group = new MotivThemeMotiv();
                $group->theme_id = $key;
                $group->motiv_id = $this->row->id;
                $group->save();
            }
        }

        if($this->shop->market) {
            Doctrine_Query::create()
                        ->delete()
                        ->from('MotivThemeMarketMotiv as a')
                        ->where('a.motiv_id = ?', array(intval($this->row->id)))
                        ->execute();

            if($this->market_theme->getValue()) {
                foreach ($this->market_theme->getValue() as $key) {
                    $group = new MotivThemeMarketMotiv();
                    $group->theme_id = $key;
                    $group->motiv_id = $this->row->id;
                    $group->save();
                }
            }
        }

        $filter = new TP_Filter_Badwords();

        $tags = $this->tags->getValue();


        if (isset($tags) && trim($tags) != "") {
            $tagsTemp = array();
            foreach (explode(',', $tags) as $key) {
                $tag = strtolower(trim($filter->filter($key)));
                if ($tag) {
                    $tagsTemp[] = array('name' => $tag, 'url' => Doctrine_Inflector::urlize($tag));
                }
            }

            $obj = TP_Mongo::getInstance();
            $collection = $obj->motive;
            $collection->save(array_merge($this->row->getRiakData(), array('tags' => $tagsTemp, '_id' => $this->row->uuid)));
        }

    }

}