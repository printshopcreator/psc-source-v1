<?php


class TP_Forms_Setting_Shop extends Zend_Form {

    protected $name;

    protected $description;

    protected $logo1;

    protected $logo1_remove;

    protected $logo2;

    protected $logo2_remove;

    protected $background_image;

    protected $background_image_remove;

    protected $keywords;

    protected $submit;

    protected $private;

    protected $register;

    protected $theme;

    protected $subtitle;

    protected $shop;

    public function __construct($shop) {

        $this->shop = $shop;


        $wizard = new TP_ResaleWizard();

        $this->setAction('/myshops/config');

        $this->setAttribs(array('class' => 'niceform', 'id' => 'shop_settings'));       

        $this->submit = new Zend_Form_Element_Submit('save');
        $this->submit->setLabel('Speichern');

        $this->subtitle = new Zend_Form_Element_Text('subtitle');
        $this->subtitle->setLabel('Untertitel');
        $this->subtitle->setAttrib('size', 37)
            ->addFilter(new TP_Filter_Badwords())
            ->setAttrib('class', 'required')
            ->setValue($shop->subtitle);

        $this->description = new Zend_Form_Element_Textarea('description');
        $this->description->setAttrib('rows', 6);
        $this->description->setAttrib('cols', 50);
        $this->description->setLabel('Beschreibung')
            ->addFilter(new TP_Filter_Badwords())
            ->setAttrib('class', 'required')
            ->setValue($shop->description);

        $this->keywords = new Zend_Form_Element_Text('keywords');
        $this->keywords->setLabel('Keywords');
        $this->keywords->setAttrib('size', 37)
            ->addFilter(new TP_Filter_Badwords())
            ->setAttrib('class', 'required')
            ->setValue($shop->keywords);

        $this->private = new Zend_Form_Element_Checkbox('private');
        $this->private->setLabel('Privater Shop')
                    ->setValue($shop->private);

        $this->register = new Zend_Form_Element_Checkbox('register');
        $this->register->setLabel('Registrierung möglich')
                    ->setValue($shop->registration);

        $image_path = false;
        if($shop->logo1 != "") {
            $image = Doctrine_Query::create()->from('Image c')->where('c.id = ?', array($shop->logo1))->fetchOne();
            require_once(APPLICATION_PATH . '/helpers/Image.php');
            $img = new TP_View_Helper_Image();
            $image_path = $img->image()->thumbnailImage($image['id'], 'articlelist', $image['id'], true, null, false);
        }

        $image_path2 = false;
        if($shop->logo2 != "") {
            $image = Doctrine_Query::create()->from('Image c')->where('c.id = ?', array($shop->logo2))->fetchOne();
            require_once(APPLICATION_PATH . '/helpers/Image.php');
            $img = new TP_View_Helper_Image();
            $image_path2 = $img->image()->thumbnailImage($image['id'], 'articlelist', $image['id'], true, null, false);
        }

        $image_fullbackground = false;
        if($shop->fullbackground != "") {
            $image = Doctrine_Query::create()->from('Image c')->where('c.id = ?', array($shop->fullbackground))->fetchOne();
            require_once(APPLICATION_PATH . '/helpers/Image.php');
            $img = new TP_View_Helper_Image();
            $image_fullbackground = $img->image()->thumbnailImage($image['id'], 'articlelist', $image['id'], true, null, false);
        }

        $this->logo1 = new TP_Form_Element_Image('logo1', array(), $image_path);
        $this->logo1->setLabel('Logo (960x120px)');
        $this->logo1->addValidator('Extension', false, 'jpg,gif,png,jpeg,tiff');

        $id = TP_Util::uuid();

        $this->logo1->setDestination('temp/');

        $this->logo1->addFilter('Rename', 'temp/' . $id . '_' . $this->logo1->getFileName(null, false));

        $this->logo1_remove = new Zend_Form_Element_Checkbox('logo1_remove');
        $this->logo1_remove->clearDecorators();

        $this->logo2 = new TP_Form_Element_Image('logo2', array(), $image_path2);
        $this->logo2->setLabel('Banner (960x250px)');
        $this->logo2->addValidator('Extension', false, 'jpg,gif,png,jpeg,tiff');

        $id = TP_Util::uuid();

        $this->logo2->setDestination('temp/');

        $this->logo2->addFilter('Rename', 'temp/' . $id . '_' . $this->logo2->getFileName(null, false));

        $this->logo2_remove = new Zend_Form_Element_Checkbox('logo2_remove');
        $this->logo2_remove->clearDecorators();

        $this->background_image = new TP_Form_Element_Image('background_image', array(), $image_fullbackground);
        $this->background_image->setLabel('Background');
        $this->background_image->addValidator('Extension', false, 'jpg,gif,png,jpeg,tiff');

        $id = TP_Util::uuid();

        $this->background_image->setDestination('temp/');

        $this->background_image->addFilter('Rename', 'temp/' . $id . '_' . $this->background_image->getFileName(null, false));

        $this->background_image_remove = new Zend_Form_Element_Checkbox('background_image_remove');
        $this->background_image_remove->clearDecorators();

        $this->addElements(array(
            $this->subtitle,
            $this->description,
            $this->keywords,
            $this->private,
            $this->register,
            $this->logo1,
            $this->logo1_remove,
            $this->logo2,
            $this->logo2_remove,
            $this->background_image,
            $this->background_image_remove
        ));

        if($this->shop->market) {

            $selectedthemes = array();
            foreach($this->shop->ShopThemeShop as $tp) {
                $selectedthemes[] = $tp->theme_id;
            }

            $this->theme = new Zend_Form_Element_Multiselect('theme');
            $this->theme->setLabel('Thema')
                             ->setAttrib('size', 10)
                             ->setValue($selectedthemes);

            $install = Zend_Registry::get('install');
            $article_themes = Doctrine_Query::create ()->select('a.title as title, a.id as id')->from('ShopTheme as a')->where('a.shop_id = ? AND a.parent_id = 0', array($install['defaultmarket']))->execute();

            $tmp = array();
            foreach($article_themes as $row) {
                $tmp[$row->id] = $row->title;
                $article_themes_sub = Doctrine_Query::create ()->select('a.title as title, a.id as id')->from('ShopTheme as a')->where('a.shop_id = ? AND a.parent_id = ?', array($install['defaultmarket'], $row->id))->execute();
                foreach($article_themes_sub as $row_sub) {
                    $tmp[$row_sub->id] = '---' . $row_sub->title;
                }
            }

            $this->theme->addMultiOptions($tmp);
            $this->addElement($this->theme);
        }

        $this->addElement($this->submit);

        $this->addDecorators(array(
            'FormElements',
                array('HtmlTag', array('tag' => 'dl')),
            'Form'
        ));
    }

    public function storeInSession() {
        $this->shop->subtitle = $this->subtitle->getValue();
        $this->shop->description = $this->description->getValue();
        $this->shop->keywords = $this->keywords->getValue();
        $this->shop->private = $this->private->getValue();
        $this->shop->registration = $this->register->getValue();

        if($this->logo1->getValue() && $this->logo1_remove->getValue() != 1){
            $file = new Image ( );
            $file->id = TP_Util::uuid ();
            $file->path = $this->logo1->getFileName(null, true);
            $file->name = $this->logo1->getValue();

            $finfo = new finfo(FILEINFO_MIME_TYPE);
            $finfo = $finfo->file($file->path);
            $file->imagetype = $finfo;
            $file->save();

            $this->shop->logo1 = $file->id;

        }
        if($this->logo2->getValue() && $this->logo2_remove->getValue() != 1){
            $file = new Image ( );
            $file->id = TP_Util::uuid ();
            $file->path = $this->logo2->getFileName(null, true);
            $file->name = $this->logo2->getValue();

            $finfo = new finfo(FILEINFO_MIME_TYPE);
            $finfo = $finfo->file($file->path);
            $file->imagetype = $finfo;
            $file->save();

            $this->shop->logo2 = $file->id;
        }

        if($this->background_image->getValue() && $this->background_image_remove->getValue() != 1){
            $file = new Image ( );
            $file->id = TP_Util::uuid ();
            $file->path = $this->background_image->getFileName(null, true);
            $file->name = $this->background_image->getValue();

            $finfo = new finfo(FILEINFO_MIME_TYPE);
            $finfo = $finfo->file($file->path);
            $file->imagetype = $finfo;
            $file->save();

            $this->shop->fullbackground = $file->id;
        }

        if($this->logo1_remove->getValue() == 1) {
            $this->shop->logo1 = "";
        }

        if($this->logo2_remove->getValue() == 1) {
            $this->shop->logo2 = "";
        }

        if($this->background_image_remove->getValue() == 1) {
            $this->shop->fullbackground = "";
        }

        $this->shop->save();

        if($this->shop->market) {
            Doctrine_Query::create()
                        ->delete()
                        ->from('ShopThemeShop as a')
                        ->where('a.shop_id = ?', array(intval($this->shop->id)))
                        ->execute();

            if($this->theme->getValue()) {
                foreach ($this->theme->getValue() as $key) {
                    $group = new ShopThemeShop();
                    $group->theme_id = $key;
                    $group->shop_id = $this->shop->id;
                    $group->save();
                }
            }
        }
    }

}