<?php


class TP_Forms_Setting_Slide extends Zend_Form {

    protected $shop;

    protected $title;

    protected $row;

    protected $image;

    protected $link;

    protected $text;

    protected $submit;

    public function __construct($shop, $row) {

        $this->addPrefixPath('TP_Form', 'TP/Form/Element/', 'Element');
        $this->shop = $shop;
        $this->row = $row;

        $data = Zend_Json::decode($row->value);

        $wizard = new TP_ResaleWizard();

        $this->setAction('/myshops/editslide');

        $this->setAttribs(array('class' => 'niceform', 'id' => 'slide_settings'));

        $this->submit = new Zend_Form_Element_Submit('save');
        $this->submit->setLabel('Speichern');

        $this->title = new Zend_Form_Element_Text('title');
        $this->title->setLabel('Titel')
            ->addFilter(new TP_Filter_Badwords())
            ->setValue($data['title']);

        $this->link = new Zend_Form_Element_Text('link');
        $this->link->setLabel('Link')
            ->addFilter(new TP_Filter_Badwords())
            ->setValue($data['link']);

        $this->text = new Zend_Form_Element_Textarea('text');
        $this->text->setLabel('Text')
            ->addFilter(new TP_Filter_Badwords())
            ->setAttribs(array('cols' => 40, 'rows' => 40))
            ->setValue($data['text']);

        $image_path = false;

        if($data['image'] != "") {
            $image = Doctrine_Query::create()->from('Image c')->where('c.id = ?', array($data['image']))->fetchOne();
            require_once(APPLICATION_PATH . '/helpers/Image.php');
            $img = new TP_View_Helper_Image();
            $image_path = $img->image()->thumbnailImage($image['id'], 'articlelist', $image['id'], true, null, false);
        }

        $this->image = new TP_Form_Element_Image('image', array(), $image_path);
        $this->image->setLabel('Bild (opt. Breite 700px)');
        $this->image->addValidator('Extension', false, 'jpg,gif,png,jpeg,tiff');

        $id = TP_Util::uuid();

        $this->image->setDestination('temp/');

        $this->image->addFilter('Rename', 'temp/' . $id . '_' . $this->image->getFileName(null, false));

        $this->image_remove = new Zend_Form_Element_Checkbox('image_remove');
        $this->image_remove->clearDecorators();

        $this->addElements(array(
            $this->title,
            $this->link,
            $this->image,
            $this->image_remove,
            $this->text,
            $this->submit
        ));

        $this->addDecorators(array(
            'FormElements',
                array('HtmlTag', array('tag' => 'dl')),
            'Form'
        ));
    }

    public function storeInSession() {
        $data = Zend_Json::decode($this->row->value);
        if($this->image->getValue() && $this->image_remove->getValue() != 1){
            $file = new Image ( );
            $file->id = TP_Util::uuid ();
            $file->path = $this->image->getFileName(null, true);
            $file->name = $this->image->getValue();

            $finfo = new finfo(FILEINFO_MIME_TYPE);
            $finfo = $finfo->file($file->path);
            $file->imagetype = $finfo;
            $file->save();

            $this->row->value = Zend_Json::encode(array(
                'link' => $this->link->getValue(),
                'title' => $this->title->getValue(),
                'text' => $this->text->getValue(),
                'image' => $file->id
            ));
        }else{
            $this->row->value = Zend_Json::encode(array(
                'link' => $this->link->getValue(),
                'title' => $this->title->getValue(),
                'text' => $this->text->getValue(),
                'image' => $data['image']
            ));
        }

        if($this->image_remove->getValue() == 1) {
            $this->row->value = Zend_Json::encode(array(
                'link' => $this->link->getValue(),
                'title' => $this->title->getValue(),
                'text' => $this->text->getValue(),
                'image' => ''
            ));
        }

        $this->row->save();

    }

}