<?php


class TP_Forms_Setting_Startseite extends Zend_Form {

    protected $mode;

    protected $slogan;

    protected $submit;

    protected $motive;

    protected $products;

    protected $productgroups;

    protected $slider_interval;

    protected $shopconf;

    protected $shop;

    public function __construct($shop) {

        $this->shop = $shop;

        if($this->shop->layout_settings != "") {
            $this->shopconf = Zend_Json::decode($this->shop->layout_settings);
        }else{
            $this->shopconf = array();
        }

        $wizard = new TP_ResaleWizard();

        $this->setAction('/myshops/configstart');

        $this->setAttribs(array('class' => 'niceform', 'id' => 'shop_settings'));       

        $this->submit = new Zend_Form_Element_Submit('save');
        $this->submit->setLabel('Speichern');

        $this->mode = new Zend_Form_Element_Select('mode');
        $this->mode->setLabel('Startseite');
        $this->mode->addMultiOption('standard', 'Willkommensseite');
        $this->mode->addMultiOption('sliderproducts', 'Slider + Produktliste o. Produktgruppen');
        $this->mode->addMultiOption('sliderproductsnosidenav', 'Slider + Produktliste o. Produktgruppen ohne Seitennavi');
        $this->mode->addMultiOption('articles', 'Produktliste');
        $this->mode->addMultiOption('sliderproductstop', 'Slider + Produktliste (Topseller) o. Produktgruppen');

        if(file_exists(Zend_Registry::get('layout_path') . '/templates/index/ms.phtml')) {
            $this->mode->addMultiOption('ms', 'Motiv Start');
        }

        $this->addElement($this->mode);

        if(strtolower($this->shop->defaultfunc) == "index") {

            $row = Doctrine_Query::create()->select()->from('Shopsetting s')->where('s.shop_id = ? AND s.keyid = ?',array($this->shop['id'], 'index_index_layout'))->fetchOne();

            if($row && (strtolower($row->value) == "psc" || strtolower($row->value) == "psctopseller" || strtolower($row->value) == "sliderproductsnosidenav")) {
                $this->slider_interval = new Zend_Form_Element_Select('slider_interval');
                $this->slider_interval->setLabel('Slidergeschwindigkeit');
                $this->slider_interval->addMultiOption('6000', 'Langsam');
                $this->slider_interval->addMultiOption('4000', 'Normal');
                $this->slider_interval->addMultiOption('2000', 'Schnell');
                if(isset($this->shopconf[$this->shop->layout]['slider_interval'])) {
                    $this->slider_interval->setValue($this->shopconf[$this->shop->layout]['slider_interval']);
                }
                $this->addElement($this->slider_interval);
                $this->mode->setValue("sliderproducts");
                if(strtolower($row->value) == "sliderproductsnosidenav") {
                    $this->mode->setValue("sliderproductsnosidenav");
                }


                $products = Doctrine_Query::create ()->select('a.title as title, a.id as id')->from('Article as a')->where('a.shop_id = ? AND a.enable = 1', array($this->shop->id))->orderBy('a.pos ASC')->execute();
                $tmpp = array();
                foreach($products as $rowp) {
                    $tmpp[$rowp->id] = $rowp->title;
                }
                $this->products = new Zend_Form_Element_Multiselect('products');
                $this->products->setLabel('Produkte')
                    ->setAttrib('size', 10)
                    ->addMultiOptions($tmpp);
                if(isset($this->shopconf[$this->shop->layout]['mc_start_products'])) {
                    $this->products->setValue($this->shopconf[$this->shop->layout]['mc_start_products']);
                }
                if(strtolower($row->value) == "psc" || strtolower($row->value) == "sliderproductsnosidenav") {
                    $this->addElement($this->products);
                }
                $productgroups = Doctrine_Query::create ()->select('a.title as title, a.id as id')->from('ArticleGroup as a')->where('a.shop_id = ? AND a.enable = 1', array($this->shop->id))->orderBy('a.pos ASC')->execute();
                $tmpg = array();
                foreach($productgroups as $rowp) {
                    $tmpg[$rowp->id] = $rowp->title;
                }
                $this->productgroups = new Zend_Form_Element_Multiselect('productgroups');
                $this->productgroups->setLabel('Produktgruppen')
                    ->setAttrib('size', 10)
                    ->addMultiOptions($tmpg);
                if(isset($this->shopconf[$this->shop->layout]['mc_start_productgroups'])) {
                    $this->productgroups->setValue($this->shopconf[$this->shop->layout]['mc_start_productgroups']);
                }
                $this->addElement($this->productgroups);

            }

            if($row && strtolower($row->value) == "ms") {
                $this->mode->setValue("ms");
            }
            if($row && strtolower($row->value) == "index") {

                $this->slogan = new Zend_Form_Element_Text('slogan');
                $this->slogan->setLabel('Slogan');

                if(isset($this->shopconf[$this->shop->layout]['index_slogan'])) {
                    $this->slogan->setValue($this->shopconf[$this->shop->layout]['index_slogan']);
                }
                $this->addElement($this->slogan);
                $this->mode->setValue("standard");
            }
        }

        if(strtolower($this->shop->defaultfunc) == "overview") {
            $this->mode->setValue("articles");
        }

        if(strtolower($this->shop->defaultfunc) == "cms") {
            $this->mode->setValue("cms");
        }


        $this->addElement($this->submit);

        $this->addDecorators(array(
            'FormElements',
                array('HtmlTag', array('tag' => 'dl')),
            'Form'
        ));
    }

    public function storeInSession() {
        $setting = "index";
        if($this->mode->getValue() == "standard") {
            $this->shop->defaultfunc = "Index";
            $setting = "index";
        }
        if($this->mode->getValue() == "ms") {
            $this->shop->defaultfunc = "Index";
            $setting = "ms";
        }
        if($this->mode->getValue() == "sliderproducts") {
            $this->shop->defaultfunc = "Index";
            $setting = "psc";
        }
        if($this->mode->getValue() == "sliderproductsnosidenav") {
            $this->shop->defaultfunc = "Index";
            $setting = "sliderproductsnosidenav";
        }

        if($this->mode->getValue() == "sliderproductstop") {
            $this->shop->defaultfunc = "Index";
            $setting = "psctopseller";
        }

        if($this->mode->getValue() == "sliderproductgroups") {
            $this->shop->defaultfunc = "Index";
            $setting = "pscgroups";
        }
        if($this->mode->getValue() == "articles") {
            $this->shop->defaultfunc = "Overview";
        }
        if($this->mode->getValue() == "cms") {
            $this->shop->defaultfunc = "Cms";
        }

        $row = Doctrine_Query::create()->select()->from('Shopsetting s')->where('s.shop_id = ? AND s.keyid = ?',array($this->shop['id'], 'index_index_layout'))->fetchOne();
        if($row) {
            $row->value = $setting;
            $row->save();
        }else{
            $row = new Shopsetting();
            $row->shop_id = $this->shop->id;
            $row->keyid = "index_index_layout";
            $row->value = $setting;
            $row->save();
        }

        if(isset($this->slogan)) {
            $this->shopconf[$this->shop->layout]['index_slogan'] = $this->slogan->getValue();

            $this->shop->layout_settings = Zend_Json::encode($this->shopconf);
        }
        if(isset($this->slider_interval)) {
            $this->shopconf[$this->shop->layout]['slider_interval'] = $this->slider_interval->getValue();

            $this->shop->layout_settings = Zend_Json::encode($this->shopconf);
        }

        if(isset($this->motive)) {
            $this->shopconf[$this->shop->layout]['mc_start_motive'] = $this->motive->getValue();

            $this->shop->layout_settings = Zend_Json::encode($this->shopconf);
        }

        if(isset($this->products)) {
            $this->shopconf[$this->shop->layout]['mc_start_products'] = $this->products->getValue();

            $this->shop->layout_settings = Zend_Json::encode($this->shopconf);
        }
        if(isset($this->productgroups)) {
            $this->shopconf[$this->shop->layout]['mc_start_productgroups'] = $this->productgroups->getValue();

            $this->shop->layout_settings = Zend_Json::encode($this->shopconf);
        }


        $this->shop->save();
    }

}