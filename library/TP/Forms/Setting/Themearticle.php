<?php


class TP_Forms_Setting_Themearticle extends Zend_Form {

    protected $title;

    protected $submit;

    protected $parent;

    protected $row;

    public function __construct($row, $rows) {

        $this->addPrefixPath('TP_Form', 'TP/Form/Element/', 'Element');
        $this->row = $row;

        $wizard = new TP_ResaleWizard();

        $this->setAction('/myshops/config');

        $this->setAttribs(array('class' => 'niceform', 'id' => 'theme_article_settings'));       

        $this->submit = new Zend_Form_Element_Submit('save');
        $this->submit->setLabel('Speichern');

        $this->title = new Zend_Form_Element_Text('title');
        $this->title->setAttrib('size', 37)
            ->setRequired(true)
            ->setLabel('Titel')
            ->addFilter(new TP_Filter_Badwords())
            ->setAttrib('class', 'required')
            ->setValue($row->title);

        $groups = array();
        $groups[0] = 'Keiner';
        foreach($rows as $a) {
            $groups[$a->id] = $a->title;
        }

        $this->parent = new Zend_Form_Element_Select('parent');
        $this->parent->setLabel('Vorgänger')
                        ->addMultiOptions($groups)
                        ->setValue($row->parent_id);


        $this->addElements(array(
            $this->title,
            $this->parent,
            $this->submit
        ));

        $this->addDecorators(array(
            'FormElements',
                array('HtmlTag', array('tag' => 'dl')),
            'Form'
        ));
    }

    public function storeInSession() {
        $this->row->title = $this->title->getValue();
        $this->row->parent_id = $this->parent->getValue();

        $this->row->save();
    }

}