<?php


class TP_Forms_Shop_Contact extends EasyBib_Form {

    protected $name;

    protected $email;

    protected $tel;

    protected $text;

    protected $captcha;

    protected $submit;

    protected $shop;

    public function __construct($shop) {

        $this->shop = $shop;

        $this->setAction('/index/contact');

        $this->setAttribs(array('class' => 'form-horizontal', 'id' => 'shop_contact'));

        $this->submit = new Zend_Form_Element_Submit('send');
        $this->submit->setLabel('Senden');

        $this->name = new Zend_Form_Element_Text('name');
        $this->name->setLabel('Name*');
        $this->name->setRequired(true);

        $this->addElement($this->name);

        $this->email = new Zend_Form_Element_Text('email');
        $this->email->setLabel('E-Mail*');
        $this->email->setRequired(true);

        $this->addElement($this->email);

        $this->tel = new Zend_Form_Element_Text('tel');
        $this->tel->setLabel('Tel');

        $this->addElement($this->tel);

        $this->text = new Zend_Form_Element_Textarea('text');
        $this->text->setLabel('Text*');
        $this->text->setRequired(true);
        $this->text->setAttribs(array('cols' => 50, 'rows' => 10));
        $this->addElement($this->text);

        if(!$shop->private) {
            $this->captcha = $element = new Zend_Form_Element_Captcha('cp', array(
                'label' => "",
                'captcha' => array(
                    'captcha' => 'Image',
                    'font' => PUBLIC_PATH . '/layouter/fonts/verdana.ttf',
                    'imgDir' => PUBLIC_PATH . '/temp/thumb/',
                    'imgUrl' => '/temp/thumb/',
                    'wordLen' => '4'
                ),
            ));

            $this->addElement($this->captcha);
        }

        $this->addElement($this->submit);

        EasyBib_Form_Decorator::setFormDecorator($this, EasyBib_Form_Decorator::BOOTSTRAP);
    }

    public function storeInSession() {

    }

}