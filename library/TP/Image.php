<?php

class TP_Image {

    const LAYOUTERPREVIEW = 0;

    const LAYOUTERPREVIEWBIG = 1;
	
    protected  $settings = null;
    protected  $converter = null;

    protected static $_instance = null;

    public static function getInstance()
    {
        if (null === self::$_instance) {
            self::$_instance = new self();
    }

    return self::$_instance;
    }

    public function __construct() {

    }

    public static function generateWidthImgTag($mode, $file, $modus = false, $class = "", $onlyPath = false, $clearCache = false) {
        if(file_exists($file) || strpos($file, "apps") !== false) {
            if($onlyPath) {
                return self::generate($mode, $file, $modus);
            }
            return '<img id="generated_image" src="'.self::generate($mode, $file, $modus).'?time='.time().'" class="'.$class.'"/>';
        }
    }

    public static function generate($mode, $file, $modus = false) {

        switch($mode) {
            case TP_Image::LAYOUTERPREVIEW:
                if($modus) {
                    $file = self::generateThumb($modus, $file);
                }else{
                    $file = self::generateThumb('layouter', $file);
                }

            break;
            case TP_Image::LAYOUTERPREVIEWBIG:
                if($modus) {
                    $file = self::generateThumb($modus, $file);
                }else{
                    $file = self::generateThumb('layouterbig', $file);
                }
            break;
        }

        return $file;
    }

    public static function generateThumb($mode = 'admin', $path) {

        $shop = Zend_Registry::get('shop');

        if(!file_exists('temp/thumb/'.$shop['uid'])) mkdir('temp/thumb/'.$shop['uid'], 0777, true);

        if(isset($_REQUEST['testMode']) && $_REQUEST['testMode'] == 'q3o847tqcp236r') {
            $config = new Zend_Config_Ini(Zend_Registry::get('shop_path')  . '/'.  $_REQUEST['testLayout'] .'/config/images.ini', APPLICATION_ENV);
        }elseif($shop['customtemplates'] == 1) {
            $config = new Zend_Config_Ini(APPLICATION_PATH  . '/design/vorlagen/'. $shop['layout'] .'/config/images.ini', APPLICATION_ENV);
        }else{
        	if(file_exists(Zend_Registry::get('shop_path')  . '/'. $shop['layout'] .'/config/images.ini')) {
            	$config = new Zend_Config_Ini(Zend_Registry::get('shop_path')  . '/'. $shop['layout'] .'/config/images.ini', APPLICATION_ENV);
        	}else{
        		if(file_exists(Zend_Registry::get('shop_path')  . '/config/images.ini')) {
                            $config = new Zend_Config_Ini(Zend_Registry::get('shop_path')  . '/config/images.ini', APPLICATION_ENV);
                        }else{
                            $config = new Zend_Config_Ini(APPLICATION_PATH  . '/design/vorlagen/'. $shop['layout'] .'/config/images.ini', APPLICATION_ENV);
                        }
        	}
        }

        try
        {
            $filters = array();

            $im = new imagick($path);
            $im->setImageCompressionQuality(98);
            $sizes = $im->getImageGeometry();

            if($config->images->$mode->scale->data->direction == 'SCALE_NOT') {

            }elseif((int)$config->images->$mode->scale->data->width != 0 && $sizes['width'] >= $sizes['height'] && isset($config->images->$mode->scale->data->height)) {
                $im->thumbnailImage( (int)$config->images->$mode->scale->data->width, null);
            }elseif((int)$config->images->$mode->scale->data->height != 0 && $sizes['width'] <= $sizes['height'] && isset($config->images->$mode->scale->data->width)) {
                $im->thumbnailImage( null, (int)$config->images->$mode->scale->data->height );
            }elseif(isset($config->images->$mode->scale->data->height) && !isset($config->images->$mode->scale->data->width)) {
                $im->thumbnailImage( null, (int)$config->images->$mode->scale->data->height );
            }elseif(isset($config->images->$mode->scale->data->width) && !isset($config->images->$mode->scale->data->height)) {
                $im->thumbnailImage( (int)$config->images->$mode->scale->data->width, null );
            }else{
                $im->thumbnailImage( 200, null );
            }
            $im->writeImage(PUBLIC_PATH.'/temp/thumb/'.$shop['uid'].'/'.md5($path).'_'.$mode.'.png');
            $im->clear();
            $im->destroy();

        }catch ( Exception $e){
            Zend_Registry::get('log')->debug('CREATE THUMB:'.$e->getMessage());
        }

        return '/temp/thumb/'.$shop['uid'].'/'.md5($path).'_'.$mode.'.png';
    }
    
    public static function createThumb($mode ='admin', $image, $overwrite = false) {

        $shop = Zend_Registry::get('shop');

        if((!file_exists('temp/thumb/'.$shop['uid'].'/'.$image->id.'_'.$mode.'.png') || $overwrite) && file_exists($image->path)) {

            if(!file_exists('temp/thumb/'.$shop['uid'])) mkdir('temp/thumb/'.$shop['uid'], 0777, true);

            $finfo = new finfo(FILEINFO_MIME_TYPE);
            $finfo = $finfo->file($image->path);

            if($finfo != 'application/pdf' && $finfo != 'image/svg+xml' && $finfo != 'image/tiff' && $finfo != 'image/jpeg' && $finfo != 'application/postscript' && $finfo != 'image/gif' && $finfo != 'image/png' && $finfo != 'image/jpg') return false;



            if(isset($_REQUEST['testMode']) && $_REQUEST['testMode'] == 'q3o847tqcp236r') {
                $config = new Zend_Config_Ini(Zend_Registry::get('shop_path')  . '/'.  $_REQUEST['testLayout'] .'/config/images.ini', APPLICATION_ENV);
            }elseif($shop['customtemplates'] == 1) {
                $config = new Zend_Config_Ini(APPLICATION_PATH  . '/design/vorlagen/'. $shop['layout'] .'/config/images.ini', APPLICATION_ENV);
            }else{
	            if(file_exists(Zend_Registry::get('shop_path')  . '/'. $shop['layout'] .'/config/images.ini')) {
	            	$config = new Zend_Config_Ini(Zend_Registry::get('shop_path')  . '/'. $shop['layout'] .'/config/images.ini', APPLICATION_ENV);
                    }else{
                        if(file_exists(Zend_Registry::get('shop_path')  . '/config/images.ini')) {
                            $config = new Zend_Config_Ini(Zend_Registry::get('shop_path')  . '/config/images.ini', APPLICATION_ENV);
                        }else{
                            $config = new Zend_Config_Ini(APPLICATION_PATH  . '/design/vorlagen/'. $shop['layout'] .'/config/images.ini', APPLICATION_ENV);
                        }
                    }
            }
            if(true) {

                try
                {
                    $filters = array();

                  	if($finfo == 'application/pdf') {

                        $resize = "-resize ";
                        if((int)$config->images->$mode->scale->data->width != 0) {
                            $resize = $resize . (int)$config->images->$mode->scale->data->width;
                        }else{
                            $resize = $resize . 0;
                        }
                        $resize = $resize . "x";
                        if((int)$config->images->$mode->scale->data->height != 0) {
                            $resize = $resize . (int)$config->images->$mode->scale->data->height;
                        }else{
                            $resize = $resize . (int)$config->images->$mode->scale->data->width;
                        }

                        exec("convert '".$image->path."[0]' ".$resize." -quality 75 -background white -alpha remove -density 140 -resample 100 -colorspace sRGB ". PUBLIC_PATH.'/temp/thumb/'.$shop['uid'].'/'.$image->id.'_'.$mode.'.png');
                       // var_dump("convert -density 140 -resample 100 -colorspace sRGB '".$image->path."[0]' ".$resize." -quality 75 ". PUBLIC_PATH.'/temp/thumb/'.$shop['uid'].'/'.$image->id.'_'.$mode.'.jpg');
                        return '/temp/thumb/'.$shop['uid'].'/'.$image->id.'_'.$mode.'.png';
                    }elseif($finfo == 'image/png') {

                        $resize = "-resize ";
                        if((int)$config->images->$mode->scale->data->width != 0) {
                            $resize = $resize . (int)$config->images->$mode->scale->data->width;
                        }else{
                            $resize = $resize . 0;
                        }
                        $resize = $resize . "x";
                        if((int)$config->images->$mode->scale->data->height != 0) {
                            $resize = $resize . (int)$config->images->$mode->scale->data->height;
                        }else{
                            $resize = $resize . (int)$config->images->$mode->scale->data->width;
                        }

                        exec("convert '".$image->path."' ".$resize." -quality 75 ". PUBLIC_PATH.'/temp/thumb/'.$shop['uid'].'/'.$image->id.'_'.$mode.'.png');
                       // var_dump("convert -density 140 -resample 100 -colorspace sRGB '".$image->path."[0]' ".$resize." -quality 75 ". PUBLIC_PATH.'/temp/thumb/'.$shop['uid'].'/'.$image->id.'_'.$mode.'.jpg');
                        return '/temp/thumb/'.$shop['uid'].'/'.$image->id.'_'.$mode.'.png';

                    }else{
                        $im = new imagick($image->path);
                          //$im->setStrokeAntialias(false);
                    	//$im->setImageCompressionQuality(98);
                    }

                    $sizes = $im->getImageGeometry();

                    if($config->images->$mode->scale->data->direction == 'SCALE_NOT') {

                    }elseif($config->images->$mode->scale->data->direction == 'SCALE_BOTH_MIN' && (int)$config->images->$mode->scale->data->width != 0 && (int)$config->images->$mode->scale->data->height != 0){

                        if($sizes['width'] < (int)$config->images->$mode->scale->data->width) {

                        }else{
                            if($sizes['width'] >= $sizes['height']) {
                                $im->thumbnailImage( (int)$config->images->$mode->scale->data->width, null);
                                $sizes = $im->getImageGeometry();
                                if($sizes['height'] > (int)$config->images->$mode->scale->data->height) {
                                    $im->thumbnailImage( null, (int)$config->images->$mode->scale->data->height );
                                }
                            }else{
                                $im->thumbnailImage( null, (int)$config->images->$mode->scale->data->height );
                                $sizes = $im->getImageGeometry();
                                if($sizes['width'] > (int)$config->images->$mode->scale->data->width) {
                                    $im->thumbnailImage( (int)$config->images->$mode->scale->data->width, null);
                                }
                            }
                        }

                    }elseif($config->images->$mode->scale->data->direction == 'SCALE_BOTH' && (int)$config->images->$mode->scale->data->width != 0 && (int)$config->images->$mode->scale->data->height != 0){


                        if($sizes['width'] >= $sizes['height']) {
                            $im->thumbnailImage( (int)$config->images->$mode->scale->data->width, null);
                            $sizes = $im->getImageGeometry();
                            if($sizes['height'] > (int)$config->images->$mode->scale->data->height) {
                                $im->thumbnailImage( null, (int)$config->images->$mode->scale->data->height );
                            }
                        }else{
                            $im->thumbnailImage( null, (int)$config->images->$mode->scale->data->height );
                            $sizes = $im->getImageGeometry();
                            if($sizes['width'] > (int)$config->images->$mode->scale->data->width) {
                                $im->thumbnailImage( (int)$config->images->$mode->scale->data->width, null);
                            }
                        }


                    }elseif((int)$config->images->$mode->scale->data->width != 0 && $sizes['width'] >= $sizes['height'] && isset($config->images->$mode->scale->data->height)) {

                        $im->thumbnailImage( (int)$config->images->$mode->scale->data->width, null);

                    }elseif((int)$config->images->$mode->scale->data->height != 0 && $sizes['width'] <= $sizes['height'] && isset($config->images->$mode->scale->data->width)) {

                        $im->thumbnailImage( null, (int)$config->images->$mode->scale->data->height );

                    }elseif(isset($config->images->$mode->scale->data->height) && !isset($config->images->$mode->scale->data->width)) {

                        $im->thumbnailImage( null, (int)$config->images->$mode->scale->data->height );

                    }elseif(isset($config->images->$mode->scale->data->width) && !isset($config->images->$mode->scale->data->height)) {

                        $im->thumbnailImage( (int)$config->images->$mode->scale->data->width, null );

                    }else{

                        $im->thumbnailImage( 200, null );
                    }

                    if(isset($config->images->$mode->render_copyright) && $config->images->$mode->render_copyright) {

                        $height = $im->getimageheight();

                        $draw = new ImagickDraw();
                        $draw->setFillColor('#ffffff');
                        $draw->setFont('layouter/fonts/verdana.ttf');
                        $draw->setFontSize(10);
                        if(Zend_Registry::isRegistered('render_copyright_shop_id')) {
                            $shop = Doctrine_Query::create()->from('Shop c')->where('c.id = ?', array(Zend_Registry::get('render_copyright_shop_id', false)))->fetchOne();
                            if($shop->betreiber_name == "") {
                            	$im->annotateImage($draw,5,$height-10,0,'©'.$shop->name);
                            }else{
                            	$im->annotateImage($draw,5,$height-10,0,'©'.$shop->betreiber_name);
                            }
                        }elseif(Zend_Registry::isRegistered('render_copyright_text')){
                            $im->annotateImage($draw,5,$height-10,0,'©'.Zend_Registry::get('render_copyright_text',""));
                        }
                    }

                    $im->writeImage(PUBLIC_PATH.'/temp/thumb/'.$shop['uid'].'/'.$image->id.'_'.$mode.'.png');
                    $im->clear();
                    $im->destroy();

                }catch ( Exception $e){
                    Zend_Registry::get('log')->debug('CREATE THUMB:'.$e->getMessage());
                }

            } elseif($config->mode == "gd") {
                $filters = array();

                $im = new TP_Thumbnail($image->path);
                $im->resize((int)$config->images->$mode->scale->data->width, (int)$config->images->$mode->scale->data->height );
                $im->save(PUBLIC_PATH.'/temp/thumb/'.$shop['uid'].'/'.$image->id.'_'.$mode.'.png');
            }
        }

        return '/temp/thumb/'.$shop['uid'].'/'.$image->id.'_'.$mode.'.png';
    }
}
