<?php

class TP_Layouter_Item
{

    private $ref = '';
    private $kst = '';
    private $templatePrintContactId = 0;

    private $_config_xml = "";
    
    private $_page_templates = "";
    
    private $_pages_xml = "";

    private $_page_objects = "";

    private $_title = "";

    private $_xslfo = "";

    private $_preview_xslfo = "";
    
    private $_extend_preview_xslfo = "";

    private $_article_id = "";

    private $_org_article_id = "";
    
    private $is_edit = false;

    private $preview_path = "";

    private $designer_xml = "";
    
    private $template_print_id = "";

    private $layouter_modus = 1;

    private $updated;
    
    public function setTemplatePrintId($var) {
        $this->template_print_id = $var;
    }
    
    public function getTemplatePrintId() {
        return $this->template_print_id;
    }

    public function setPreviewPath($xml) {
        $this->preview_path = $xml;
    }

    public function getPreviewPath() {
        return $this->preview_path;
    }

    public function setDesignerXML($xml) {
        $this->designer_xml = $xml;
        $this->updated = date('Y-m-d H:i:s');
    }

    public function getDesignerXML() {
        return $this->designer_xml;
    }

    public function saveMongo() {

    }

    public function __construct($id) {
        $this->updated = date('Y-m-d H:i:s');

        $article = Doctrine_Query::create()
            ->from('Article c')
            ->where('c.uuid = ?', array($id))
            ->fetchOne();

        if($article == false) {
            throw new Zend_Amf_Exception;
        }

        if($article->typ == 9) {
            $this->preview_path = $article->mercendise_thumbnail;
            $this->designer_xml = $article->mercendise_design;
            $this->_title = $article->title;
        }else{
            if(!file_exists(APPLICATION_PATH . '/../market/products/'. $article->a6_directory . '/' . $article->a6_xmlpagesfile)) {

                $path = str_split($article->id);

                    if(!file_exists(APPLICATION_PATH . '/../market/products/' . implode('/', $path) . '')) {
                        $an = '';
                        foreach ($path as $pt) {
                            $an.=$pt.'/';
                            if(!file_exists(APPLICATION_PATH . '/../market/products/' . $an))
                                mkdir(APPLICATION_PATH . '/../market/products/' . $an);
                            if(!file_exists(APPLICATION_PATH . '/../market/preview/' . $an))
                                mkdir(APPLICATION_PATH . '/../market/preview/' . $an);
                        }
                    }


                    $article->a6_directory =  implode('/', $path) . '/';

                    copy(APPLICATION_PATH . '/../market/products/configdummy.xml', APPLICATION_PATH . '/../market/products/' . implode('/', $path) . '/config.xml');
                    copy(APPLICATION_PATH . '/../market/products/pagesdummy.xml', APPLICATION_PATH . '/../market/products/' . implode('/', $path) . '/pages.xml');
                    copy(APPLICATION_PATH . '/../market/products/pagetemplatesdummy.xml', APPLICATION_PATH . '/../market/products/' . implode('/', $path) . '/pagetemplates.xml');
                    copy(APPLICATION_PATH . '/../market/products/pageobjectsdummy.xml', APPLICATION_PATH . '/../market/products/' . implode('/', $path) . '/pageobjects.xml');
                    copy(APPLICATION_PATH . '/../market/products/xslfodummy.fop', APPLICATION_PATH . '/../market/products/' . implode('/', $path) . '/xslfo.fop');
                    copy(APPLICATION_PATH . '/../market/products/previewxslfodummy.fop', APPLICATION_PATH . '/../market/products/' . implode('/', $path) . '/previewxslfo.fop');
                    copy(APPLICATION_PATH . '/../market/products/extendpreviewxslfodummy.fop', APPLICATION_PATH . '/../market/products/' . implode('/', $path) . '/extendpreviewxslfo.fop');

                    $article->a6_xmlconfigfile = 'config.xml';
                    $article->a6_xmlpagesfile = 'pages.xml';
                    $article->a6_xmlpagetemplatesfile = 'pagetemplates.xml';
                    $article->a6_xmlpageobjectsfile = 'pageobjects.xml';
                    $article->a6_xmlxslfofile = 'xslfo.fop';
                    $article->a6_xmlpreviewxslfofile = 'previewxslfo.fop';
                    $article->a6_xmlextendpreviewxslfofile = 'extendpreviewxslfo.fop';
                    $article->save();
            }

            $this->_pages_xml = file_get_contents(APPLICATION_PATH . '/../market/products/'. $article->a6_directory . '/' . $article->a6_xmlpagesfile);
            $this->_page_templates = file_get_contents(APPLICATION_PATH . '/../market/products/'. $article->a6_directory . '/' . $article->a6_xmlpagetemplatesfile);
            $this->_config_xml = file_get_contents(APPLICATION_PATH . '/../market/products/'. $article->a6_directory . '/' . $article->a6_xmlconfigfile);
            $this->_page_objects = file_get_contents(APPLICATION_PATH . '/../market/products/'. $article->a6_directory . '/' . $article->a6_xmlpageobjectsfile);
            $this->_xslfo = file_get_contents(APPLICATION_PATH . '/../market/products/'. $article->a6_directory . '/' . $article->a6_xmlxslfofile);
            $this->_preview_xslfo = file_get_contents(APPLICATION_PATH . '/../market/products/'. $article->a6_directory . '/' . $article->a6_xmlpreviewxslfofile);
            $this->_extend_preview_xslfo = file_get_contents(APPLICATION_PATH . '/../market/products/'. $article->a6_directory . '/' . $article->a6_xmlextendpreviewxslfofile);
            $this->_title = $article->title;
        }
    }
    
    public function setIsEdit() {
        $this->is_edit = true;
    }
    
    public function getIsEdit() {
        return $this->is_edit;
    }

    public function getArticleId() {
        return $this->_article_id;
    }

    public function setArticleId($id) {
        $this->_article_id = $id;
    }

    public function getOrgArticleId() {
        return $this->_org_article_id;
    }

    public function setOrgArticleId($id) {
        $this->_org_article_id = $id;
    }

    public function getPageObjects() {
        return $this->_page_objects;
    }

    public function setPageObjects($xml) {
        $this->_page_objects = $xml;
    }

    public function setTitle($xml) {
        $this->_title = $xml;
    }

    public function getTitle() {
        return $this->_title;
    }

    public function getPageTemplates() {
        return $this->_page_templates;
    }

    public function setPageTemplates($xml) {
        $this->_page_templates = $xml;
    }

    public function getConfigXml() {
        return $this->_config_xml;
    }

    public function setConfigXml($xml) {
        $this->_config_xml = $xml;
    }

    public function getPagesXml() {
        return $this->_pages_xml;
    }

    public function setPagesXml($xml) {
        $this->_pages_xml = $xml;
    }

    public function getXslFo() {
        return $this->_xslfo;
    }

    public function setXslFo($xml) {
        $this->_xslfo = $xml;
    }

    public function getPreviewXslFo() {
        return $this->_preview_xslfo;
    }

    public function setPreviewXslFo($xml) {
        $this->_preview_xslfo = $xml;
    }

    public function getExtendPreviewXslFo() {
        return $this->_extend_preview_xslfo;
    }

    public function setExtendPreviewXslFo($xml) {
        $this->_extend_preview_xslfo = $xml;
    }

    public function getLayouterModus()
    {
        return $this->layouter_modus;
    }

    public function setLayouterModus($layouter_modus)
    {
        $this->layouter_modus = $layouter_modus;
    }

    /**
     * @return bool|string
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * @param bool|string $updated
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;
    }

    /**
     * @return int
     */
    public function getTemplatePrintContactId()
    {
        return $this->templatePrintContactId;
    }

    /**
     * @param int $templatePrintContactId
     */
    public function setTemplatePrintContactId($templatePrintContactId)
    {
        $this->templatePrintContactId = $templatePrintContactId;
    }

    /**
     * @return string
     */
    public function getRef()
    {
        return $this->ref;
    }

    /**
     * @param string $ref
     */
    public function setRef($ref)
    {
        $this->ref = $ref;
    }

    /**
     * @return string
     */
    public function getKst()
    {
        return $this->kst;
    }

    /**
     * @param string $kst
     */
    public function setKst($kst)
    {
        $this->kst = $kst;
    }
}