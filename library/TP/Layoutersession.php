<?php

class TP_Layoutersession
{

    /**
     * Layoutersession
     *
     * @var Zend_Session_Namespace
     */
    private $_Items;

    /**
     * Konstruiert das Model für die Suche
     *
     * @return void
     */
    public function __construct()
    {

        $this->_Items = new Zend_Session_Namespace ('Layoutersession');

        $this->_Items->unlock();

        if (Zend_Session::namespaceIsset('Layoutersession') === false) {
            $this->initSession();
        }

    }

    public function copySession($id)
    {

        if (Zend_Auth::getInstance()->hasIdentity()) {

            $user = Zend_Auth::getInstance()->getIdentity();

            $row = Doctrine_Query::create()->from('LayouterSession c')->where('c.contact_id = ? AND c.uuid = ?', array($user ['id'], $id))->fetchOne();

            if ($row) {
                $row = $row->copy();
                return $row->getArticleId();
            }
        }

        if (isset ($this->_Items->data [$id])) {
            $item = clone($this->_Items->data [$id]);
            $item->setArticleId(TP_Util::uuid());
            $this->_Items->unlock();
            $this->_Items->data [$item->getArticleId()] = $item;
            $this->_Items->lock();
            return $item->getArticleId();

        }
    }

    /**
     * Gibt das Item aus der Session zurück
     *
     * @param string $articleId
     * @param string $layouterId
     * @return TP_Layouter_Item
     */

    public function getLayouterArticle($articleId, $layouterId = false)
    {

        if (Zend_Auth::getInstance()->hasIdentity()) {

            $user = Zend_Auth::getInstance()->getIdentity();


            if($layouterId) {
                $row = Doctrine_Query::create()->from('LayouterSession c')->where('c.contact_id = ? AND c.uuid = ?', array($user ['id'], $layouterId))->fetchOne();
            }else{
                $row = Doctrine_Query::create()->from('LayouterSession c')->where('c.contact_id = ? AND c.uuid = ?', array($user ['id'], $articleId))->fetchOne();
            }

            if (!$row) {

                $article = Doctrine_Query::create()
                    ->from('Article c')
                    ->where('c.uuid = ?', array($articleId))
                    ->fetchOne();

                if ($article == false) {
                    throw new Zend_Amf_Exception;
                }

                $row = new LayouterSession ();
                $row->contact_id = $user ['id'];
                $row->setOrgArticleId($articleId);
                if ($layouterId != false) {
                    $row->setArticleId($layouterId);
                } else {
                    $row->setArticleId(TP_Util::uuid());
                }
                $row->title = $article->title;
                if ($article->typ != 9) {
                    $row->setPageTemplates(file_get_contents(APPLICATION_PATH . '/../market/products/' . $article->a6_directory . '/' . $article->a6_xmlpagetemplatesfile));
                }
                $row->save();
                return $row;
            } else {
                return $row;
            }
        }

        if (!isset ($this->_Items->data [$articleId])) {
            $item = new TP_Layouter_Item ($articleId);
            if ($layouterId != false) {
                $item->setArticleId($layouterId);
            } else {
                $item->setArticleId(TP_Util::uuid());
            }
            $item->setOrgArticleId($articleId);
            $this->_Items->unlock();
            $this->_Items->data [$item->getArticleId()] = $item;
            $this->_Items->lock();
            return $this->_Items->data [$item->getArticleId()];
        } else {
            return $this->_Items->data [$articleId];
        }
    }

    public function getAllLayouterArticle()
    {

        $basket = new TP_Basket();
        $layouterIds = array();
        foreach ($basket->getAllArtikel() as $art) {
            if ($art->getLayouterId() != "") {
                $layouterIds[] = $art->getLayouterId();
            }
        }

        if (Zend_Auth::getInstance()->hasIdentity()) {

            $user = Zend_Auth::getInstance()->getIdentity();

            return Doctrine_Query::create()->from('LayouterSession c')->whereNotIn('c.uuid', $layouterIds)->andWhere('c.contact_id = ?', array($user ['id']))->orderBy('c.title ASC')->execute();

        } else {
            $tmp = array();
            $tempCount = array();
            foreach ($this->_Items->data as $row) {
                if ($row->getIsEdit()) {
                    $tmp [$row->getArticleId()] = $row;
                }
                if ($row->getIsEdit() && !in_array($row->getArticleId(), $layouterIds)) {
                    $tempCount [$row->getArticleId()] = $row;
                }
            }
            $this->_Items->unlock();
            $this->_Items->data = $tmp;
            $this->_Items->lock();
            return $tempCount;
        }
    }

    public function getLayouterSessionForLogin()
    {
        return $this->_Items->data;
    }

    public function deleteLayouterArticle($ArticleId)
    {
        if (Zend_Auth::getInstance()->hasIdentity()) {

            $user = Zend_Auth::getInstance()->getIdentity();

            Doctrine_Query::create()->delete('LayouterSession c')->where('c.contact_id = ? AND c.uuid = ?', array($user ['id'], $ArticleId))->execute();

        }

        if (isset ($this->_Items->data [$ArticleId])) {
            unset ($this->_Items->data [$ArticleId]);
        }
    }

    public function deleteAllLayouterArticle($shopId)
    {

        $basket = new TP_Basket();
        $layouterIds = array();
        foreach ($basket->getAllArtikel() as $art) {
            if ($art->getLayouterId() != "") {
                $layouterIds[] = $art->getLayouterId();
            }
        }

        if (Zend_Auth::getInstance()->hasIdentity()) {

            $user = Zend_Auth::getInstance()->getIdentity();

            Doctrine_Query::create()->delete('LayouterSession c')->whereNotIn('c.uuid', $layouterIds)->andWhere('c.contact_id = ?', array($user ['id']))->execute();

        }

        $this->_Items->unlock();
        $tmp = array();

        /** @var TP_Layouter_Item $item */
        foreach($this->_Items->data as $key => $item) {
            if(in_array($key, $layouterIds)) {
                $tmp[$key] = $item;
            }
        }
        $this->_Items->data = $tmp;
        $this->_Items->lock();
    }

    private function initSession()
    {
        $this->_Items->data = array();
    }

    public function clearSession()
    {
        $this->initSession();
    }

}
