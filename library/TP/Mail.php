<?php

class TP_Mail extends Zend_Mail {

    protected static $_instance = null;

    public static function getInstance() {
        if (null === self::$_instance) {
            self::$_instance = new self();
        }

        return self::$_instance;
    }

    public function __construct() {
        parent::__construct ();
    }

    public static function sendRegistrationMail(Contact $contact, $view) {
        $translate = Zend_Registry::get('translate');
        $config = new Zend_Config_Ini(Zend_Registry::get('shop_path') . '/config/mail.ini', APPLICATION_ENV);
        $view->contact = $contact;
        $mail = new Zend_Mail('UTF-8');
        try {
            $mail->setBodyText($view->render('registrationMailPlain.php'))
                    ->setBodyHtml($view->render('registrationMailHtml.php'))
                    ->setFrom($config->registration->frommail, $config->registration->fromname)
                    ->addTo($contact->self_email, $contact->self_firstname . ' ' . $contact->self_lastname)
                    ->setSubject($translate->translate($config->registration->subject))
                    ->send(self::checkTransport($config->registration));
        } catch (Exception $e) {

        }
    }

    public static function sendTextMail($mode, $text, $texthtml, $subject, $to, $fromname = "", $frommail = "", $attach = array(), $bcc = '') {
        


        $mail = new Zend_Mail('UTF-8');
        $mail->setType(Zend_Mime::MULTIPART_MIXED);
        if($bcc != "") {
            $bccs = explode(',', $bcc);
            $mail->addBcc($bccs);
        }

        foreach ($attach as $at) {
            $mail->addAttachment($at);
        }

        $shop = Zend_Registry::get('shop');
        if ($shop['smtpusethis'] == 1) {

            try {
                if ($text != "") {
                    $mail->setBodyText($text, 'utf-8');
                }
                if ($texthtml != "") {
                    $mail->setBodyHtml($texthtml, 'utf-8');
                }
                $mail->setFrom($frommail, $fromname)
                        ->addTo($to)
                        ->setSubject($subject)
                        ->send(self::checkTransport($shop['smtpusername'], $shop['smtppassword'], $shop['smtphostname']));
            } catch (Exception $e) {
                Zend_Registry::get('log')->debug('MAIL EXCEPTION: ' . $e->getMessage(). $e->getTraceAsString());
            }
        } else {

            if (file_exists(Zend_Registry::get('shop_path') . '/config/mail.ini')) {
                $config = new Zend_Config_Ini(Zend_Registry::get('shop_path') . '/config/mail.ini', APPLICATION_ENV);
            } elseif (file_exists(APPLICATION_PATH . '/configs/mail.ini')) {
                $config = new Zend_Config_Ini(APPLICATION_PATH . '/configs/mail.ini', APPLICATION_ENV);
            } else {
                return;
            }


            try {
                if($text != "") {
                    $mail->setBodyText($text, 'utf-8');
                }
                if ($texthtml != "") {
                    $mail->setBodyHtml($texthtml, 'utf-8');
                }
                if(isset($config->$mode->port)) {
                    $mail->setFrom($frommail, $fromname)
                        ->addTo($to)
                        ->setSubject($subject)
                        ->send(self::checkTransport($config->$mode->username, $config->$mode->password, $config->$mode->hostname, $config->$mode->port));
                }else{
                    $mail->setFrom($frommail, $fromname)
                        ->addTo($to)
                        ->setSubject($subject)
                        ->send(self::checkTransport($config->$mode->username, $config->$mode->password, $config->$mode->hostname));
                }
            } catch (Exception $e) {
                Zend_Registry::get('log')->debug('MAIL EXCEPTION: ' . $e->getMessage(). $e->getTraceAsString());
            }
        }
    }

    protected static function checkTransport($username, $password, $hostname, $port = 25) {

        switch ('smtp') {
            case "smtp":
                $configMail = array('auth' => 'login',
                    'username' => $username,
                    'port' => $port,
                    'password' => $password);
                $transport = new Zend_Mail_Transport_Smtp($hostname, $configMail);
                return $transport;
                break;
            case "sendmail":
                $transport = new Zend_Mail_Transport_Sendmail();
                return $transport;
                break;
            default:
                break;
        }

        return null;
    }

}
?>
