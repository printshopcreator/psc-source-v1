<?php

require_once('/data/www/new/vendor/mongodb/mongodb/src/functions.php');
require_once('/data/www/new/vendor/mongodb/mongodb/src/Client.php');
require_once('/data/www/new/vendor/mongodb/mongodb/src/Database.php');
require_once('/data/www/new/vendor/mongodb/mongodb/src/Collection.php');
require_once('/data/www/new/vendor/mongodb/mongodb/src/UpdateResult.php');
require_once('/data/www/new/vendor/mongodb/mongodb/src/InsertOneResult.php');
require_once('/data/www/new/vendor/mongodb/mongodb/src/Operation/Executable.php');
require_once('/data/www/new/vendor/mongodb/mongodb/src/Operation/Explainable.php');
require_once('/data/www/new/vendor/mongodb/mongodb/src/Operation/FindOne.php');
require_once('/data/www/new/vendor/mongodb/mongodb/src/Operation/Find.php');
require_once('/data/www/new/vendor/mongodb/mongodb/src/Operation/InsertOne.php');
require_once('/data/www/new/vendor/mongodb/mongodb/src/Operation/Update.php');
require_once('/data/www/new/vendor/mongodb/mongodb/src/Operation/UpdateOne.php');

require_once('/data/www/new/vendor/mongodb/mongodb/src/Model/BSONArray.php');
require_once('/data/www/new/vendor/mongodb/mongodb/src/Model/BSONDocument.php');
require_once('/data/www/new/vendor/mongodb/mongodb/src/Exception/Exception.php');
require_once('/data/www/new/vendor/mongodb/mongodb/src/Exception/InvalidArgumentException.php');


class TP_Mongo {

    protected static $_instance = null;

    public static function getInstance() {
        
        if (null === self::$_instance) {
            $mongo = new MongoDB\Client("mongodb://mongodb:27017");
            self::$_instance = $mongo->psc;
        }

        return self::$_instance;
    }
}

