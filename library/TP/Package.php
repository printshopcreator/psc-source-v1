<?php

class TP_Package {

    private $uuid = null;
    private $order = null;
    private $orderpos = null;
    private $file = null;

    public function __construct($uuid = null)
    {
        if ($uuid) {
            $this->uuid = $uuid;
            $this->loadOrder();
        }
    }

    public function loadByPos($uuid = null, $id = null)
    {
        
        $this->order = Doctrine_Query::create()
                        ->from('Orders a')
                        ->leftJoin('a.Orderspos p')
                        ->where('a.uuid = ? AND p.id = ?', array($uuid, $id))->fetchOne();

        $this->orderpos = Doctrine_Query::create()
                        ->from('Orderspos a')
                        ->where('a.orders_id = ? AND a.id = ?', array($this->order->id, $id))->fetchOne();

        if (!$this->order) {
            throw new Exception('Order not found', '404');
        }
    }

    public function loadOrder(Orders $order = null)
    {

        if ($order) {
            $this->order = $order;
        } else {

            $this->order = Doctrine_Query::create()
                            ->from('Orders a')
                            ->where('a.uuid = ?', array($this->uuid))->fetchOne();
        }
        if (!$this->order) {
            throw new Exception('Order not found', '404');
        }
    }

    public function download()
    {


        header('Content-Type: application/octet-stream');
        // Es wird downloaded.pdf benannt
        header('Content-Disposition: attachment; filename="' . $this->order->alias . '.zip"');
        header('Content-Transfer-Encoding: binary');
        $this->readfile_chunked($this->file);
    }

    public function downloadPos()
    {


        header('Content-Type: application/octet-stream');
        // Es wird downloaded.pdf benannt
        header('Content-Disposition: attachment; filename="' . $this->order->alias . '_' . $this->orderpos->id . '.zip"');
        header('Content-Transfer-Encoding: binary');
        $this->readfile_chunked($this->file);
    }

    public function createZip($subZips = false)
    {

        if (!$this->order) {
            throw new Exception('Order not found', '404');
        }

        if (!file_exists(APPLICATION_PATH . '/../data/customerdocs'))
            mkdir(APPLICATION_PATH . '/../data/customerdocs');

        if (!file_exists(APPLICATION_PATH . '/../data/export'))
            mkdir(APPLICATION_PATH . '/../data/export');

        if (!file_exists(APPLICATION_PATH . '/../data/customerdocs/' . $this->order->Shop->id))
            mkdir(APPLICATION_PATH . '/../data/customerdocs/' . $this->order->Shop->id);

        $archive = new ZipArchive();
        if(file_exists(APPLICATION_PATH . '/../data/customerdocs/' . $this->order->Shop->id . "/" . $this->order->alias . '.zip')) {
            $archive->open(APPLICATION_PATH . '/../data/customerdocs/' . $this->order->Shop->id . "/" . $this->order->alias . '.zip', ZIPARCHIVE::OVERWRITE);
        }else{
            $archive->open(APPLICATION_PATH . '/../data/customerdocs/' . $this->order->Shop->id . "/" . $this->order->alias . '.zip', ZIPARCHIVE::CREATE);
        }
        if (!$this->order->Shop->getDocInvoiceNotInPackage()) {
            $file = TP_Util::writeReport($this->order->id, 'invoice');
            if ($file && $file != "Kein Report") {
                $archive->addFile($file, $this->order->alias . '_RE.pdf');
            }
        }
        if(!$this->order->Shop->getDocDeliveryNotInPackage()) {
            $file = TP_Util::writeReport($this->order->id, 'delivery');
            if ($file && $file != "Kein Report") {
                $archive->addFile($file, $this->order->alias . '_LS.pdf');
            }
        }
        if(!$this->order->Shop->getDocOfferNotInPackage()) {
            $file = TP_Util::writeReport($this->order->id, 'offer');
            if ($file && $file != "Kein Report") {
                $archive->addFile($file, $this->order->alias . '_OF.pdf');
            }
        }
        if(!$this->order->Shop->getDocOrderNotInPackage()) {
            $file = TP_Util::writeReport($this->order->id, 'order');
            if ($file && $file != "Kein Report") {
                $archive->addFile($file, $this->order->alias . '_OR.pdf');
            }
        }
        if(!$this->order->Shop->getDocLabelNotInPackage()) {
            $file = TP_Util::writeReport($this->order->id, 'label');
            if ($file && $file != "Kein Report") {
                $archive->addFile($file, $this->order->alias . '_LB.pdf');
            }
        }
        if(!$this->order->Shop->getDocJobticketNotInPackage()) {
            $file = TP_Util::writeReport($this->order->id, 'jobtiket');
            if ($file && $file != "Kein Report") {
                $archive->addFile($file, $this->order->alias . '_JOB.pdf');
            }
        }
        if(!$this->order->Shop->isDocStornoNotInPackage()) {
            $file = TP_Util::writeReport($this->order->id, 'storno');
            if ($file && $file != "Kein Report") {
                $archive->addFile($file, $this->order->alias . '_STORNO.pdf');
            }
        }

        $i = 1;

        Zend_Registry::set('xmlexport', array());

        TP_Queue::process('packagedownloadorder', 'global', $this->order);

        $xmlexport = Zend_Registry::get('xmlexport');

        foreach ($xmlexport as $export) {
            $archive->addFile($export['value'], $export['name']);
        }

        $basePath = APPLICATION_PATH . '/../market/basket/' .$this->order->id. '/';

        if($this->order->file1 != "") {
            $archive->addFile($basePath.$this->order->file1, $this->order->file1);
        }

        foreach ($this->order->Orderspos as $pos) {

            Zend_Registry::set('xmlexport', array());

            TP_Queue::process('packagedownloadorderpos', 'global', $pos);

            $article_position = Doctrine_Query::create()
                            ->from('Article m')
                            ->where('m.id = ?', array($pos->article_id))->fetchOne();

            $art_id = $article_position->id;
            if($article_position->a6_org_article != 0) {
                $art_id = $article_position->a6_org_article;
            }

            $xmlexport = Zend_Registry::get('xmlexport');

            foreach ($xmlexport as $export) {
                $addzip = true;
                $archive->addFile($export['value'], $this->order->alias . '_' . $i . '/' . $export['name']);
            }

            $addzip = false;
            $uploads = Doctrine_Query::create()
                            ->from('Upload m')
                            ->where('orderpos_id = ?', array($pos->id))->execute();


            if($subZips) {
                $archivepos = new ZipArchive();

                if (file_exists(APPLICATION_PATH . '/../data/customerdocs/' . $this->order->Shop->id . "/" . $this->order->alias . '_' . $art_id . '_' . $i . '.zip')) {
                    $archivepos->open(APPLICATION_PATH . '/../data/customerdocs/' . $this->order->Shop->id . "/" . $this->order->alias . '_' . $art_id . '_' . $i . '.zip', ZIPARCHIVE::OVERWRITE);
                } else {
                    $archivepos->open(APPLICATION_PATH . '/../data/customerdocs/' . $this->order->Shop->id . "/" . $this->order->alias . '_' . $art_id . '_' . $i . '.zip', ZIPARCHIVE::CREATE);
                }
            }else{
                $archivepos = $archive;
            }

            if($this->order->install_id != 7) {
                $archivepos->addEmptyDir($this->order->alias . '_' . $art_id . '_' . $i);
            }
            foreach ($uploads as $upload) {
                $addzip = true;
                $archivepos->addFile(PUBLIC_PATH . '/uploads/' . $this->order->Shop->uid . '/article/' . $upload->path, $this->order->alias . '_' . $art_id . '_' . $i . '/' . $this->order->alias . '_' . $i . '_' . $art_id . "_" . $upload->name);
            }

            $path = str_split($pos->id);

            if (file_exists(APPLICATION_PATH . '/../market/templateprint/basket/' . $this->order->id . '/' . $pos->pos . '/final.pdf')) {
                $archivepos->addFile(APPLICATION_PATH . '/../market/templateprint/basket/' . $this->order->id . '/' . $pos->pos . '/final.pdf', $this->order->alias . '_' . $art_id . '_' . $i . '/' . $this->order->alias . '_' . $pos->pos .'_' . str_replace(" ", "-", $pos->ref) . '_final.pdf');

                $addzip = true;
            }elseif(file_exists(APPLICATION_PATH . '/../market/templateprint/basket/' . $this->order->id . '/' . $pos->pos . '/preview.pdf')){
                $archivepos->addFile(APPLICATION_PATH . '/../market/templateprint/basket/' . $this->order->id . '/' . $pos->pos . '/preview.pdf', $this->order->alias . '_' . $art_id . '_' . $i . '/' . $this->order->alias . '_' . $pos->pos .'_' . str_replace(" ", "-", $pos->ref) . '_final.pdf');

                $addzip = true;
            }

            if (file_exists(APPLICATION_PATH . '/../market/steplayouter/basket/' . $this->order->id . '/' . $pos->pos . '/preview.pdf')) {

                $archivepos->addFile(APPLICATION_PATH . '/../market/steplayouter/basket/' . $this->order->id . '/' . $pos->pos . '/preview.pdf', $this->order->alias . '_' . $art_id . '_' . $i . '/' . $this->order->alias . '_' . $pos->pos . '_' . $art_id . '_preview.pdf');

                $addzip = true;
            }

            if (file_exists(APPLICATION_PATH . '/../market/steplayouter/basket/' . $this->order->id . '/' . $pos->pos . '/production.pdf')) {

                $archivepos->addFile(APPLICATION_PATH . '/../market/steplayouter/basket/' . $this->order->id . '/' . $pos->pos . '/production.pdf', $this->order->alias . '_' . $art_id . '_' . $i . '/' . $this->order->alias . '_' . $pos->pos . '_' . $art_id . '.pdf');

                $addzip = true;
            }

            if (file_exists(APPLICATION_PATH . '/../market/steplayouter/basket/' . $this->order->id . '/' . $pos->pos . '/'.$this->order->id . '_' . $pos->pos.'.pdf')) {

                $archivepos->addFile(APPLICATION_PATH . '/../market/steplayouter/basket/' . $this->order->id . '/' . $pos->pos . '/'.$this->order->id . '_' . $pos->pos.'.pdf', $this->order->alias . '_' . $art_id . '_' . $i . '/' . $this->order->alias . '_' . $pos->pos . '_' . $art_id . '.pdf');

                $addzip = true;
            }

            if (file_exists(APPLICATION_PATH . '/../market/steplayouter/basket/' . $this->order->id . '/' . $pos->pos . '/'.$this->order->alias . '_' . $pos->pos.'.pdf')) {

                $archivepos->addFile(APPLICATION_PATH . '/../market/steplayouter/basket/' . $this->order->id . '/' . $pos->pos . '/'.$this->order->alias . '_' . $pos->pos.'.pdf', $this->order->alias . '_' . $art_id . '_' . $i . '/' . $this->order->alias . '_' . $pos->pos . '_' . $art_id . '.pdf');

                $addzip = true;
            }

            if ($pos->xmlxslfofile != "" && !$pos->render_print && $article_position->upload_weblayouter == 1) {
                $addzip = true;
                $path = implode('/', str_split($pos->id));
                try{
                    $dir = new RecursiveIteratorIterator(new RecursiveDirectoryIterator(APPLICATION_PATH . '/../data/renderings/' . $path));

                    $ii = 0;
                    foreach ($dir as $fileinfo) {
                        if ($fileinfo->isDir()) {
                            $ii++;
                        }
                        if ($fileinfo->isFile() && strpos($fileinfo->getFilename(), 'pdf') && !strpos($fileinfo->getFilename(), '_preview')) {
                            $archivepos->addFile($fileinfo->getRealPath(), $this->order->alias . '_' . $art_id . '_' . $i . '/' . $this->order->alias . '_' . $ii . '_' . $art_id . ".pdf");
                        }
                    }

                    if(file_exists(APPLICATION_PATH . '/../data/renderings/' . $path . '/print_preview.pdf')) {
                        $archivepos->addFile(APPLICATION_PATH . '/../data/renderings/' . $path . '/print_preview.pdf', 'print_preview.pdf');
                    }
                }catch(Exception $e) {}
            }
            if($subZips) {
                if ($addzip) {
                    $archivepos->close();
                    $archive->addFile(APPLICATION_PATH . '/../data/customerdocs/' . $this->order->Shop->id . "/" . $this->order->alias . '_' . $art_id . '_' . $i . '.zip', $this->order->alias . '_' . $art_id . '_' . $i . '.zip');
                } else {
                    $archivepos->close();
                }
            }

            $i++;
        }


        $archive->close();

        $this->file = APPLICATION_PATH . '/../data/customerdocs/' . $this->order->Shop->id . "/" . $this->order->alias . '.zip';
    }

    public function createPosZip()
    {

        if (!$this->order) {
            throw new Exception('Order not found', '404');
        }

        if (!file_exists(APPLICATION_PATH . '/../data/customerdocs/' . $this->order->Shop->id))
            mkdir(APPLICATION_PATH . '/../data/customerdocs/' . $this->order->Shop->id);

        $archive = new ZipArchive();
        if(file_exists(APPLICATION_PATH . '/../data/customerdocs/' . $this->order->Shop->id . "/" . $this->order->alias . '_' . $this->orderpos->id . '.zip')) {
            $archive->open(APPLICATION_PATH . '/../data/customerdocs/' . $this->order->Shop->id . "/" . $this->order->alias . '_' . $this->orderpos->id . '.zip', ZIPARCHIVE::OVERWRITE);
        }else{
            $archive->open(APPLICATION_PATH . '/../data/customerdocs/' . $this->order->Shop->id . "/" . $this->order->alias . '_' . $this->orderpos->id . '.zip', ZIPARCHIVE::CREATE);
        }


        $file = TP_Util::writeReport($this->order->id, 'delivery', $this->orderpos);
        if ($file && $file != "Kein Report") {
            $archive->addFile($file, $this->order->alias . '_LS.pdf');
        }
        $file = TP_Util::writeReport($this->order->id, 'invoice', $this->orderpos);
        if ($file && $file != "Kein Report") {
            $archive->addFile($file, $this->order->alias . '_RE.pdf');
        }
        $file = TP_Util::writeReport($this->order->id, 'label', $this->orderpos);
        if ($file && $file != "Kein Report") {
            $archive->addFile($file, $this->order->alias . '_LB.pdf');
        }
        $file = TP_Util::writeReport($this->order->id, 'jobtiket', $this->orderpos);
        if ($file && $file != "Kein Report") {
            $archive->addFile($file, $this->order->alias . '_JOB.pdf');
        }
        $file = TP_Util::writeReport($this->order->id, 'storno', $this->orderpos);
        if ($file && $file != "Kein Report") {
            $archive->addFile($file, $this->order->alias . '_STORNO.pdf');
        }

        $i = 1;

        Zend_Registry::set('xmlexport', array());

        TP_Queue::process('packagedownloadorder', 'global', $this->order);

        $xmlexport = Zend_Registry::get('xmlexport');

        foreach ($xmlexport as $export) {
            $archive->addFile($export['value'], $export['name']);
        }

        $pos = $this->orderpos;
        

        Zend_Registry::set('xmlexport', array());

        TP_Queue::process('packagedownloadorderpos', 'global', $pos);

        $article_position = Doctrine_Query::create()
                        ->from('Article m')
                        ->where('m.id = ?', array($pos->article_id))->fetchOne();
        
        $art_id = $article_position->id;
        if($article_position->a6_org_article != 0) {
            $art_id = $article_position->a6_org_article;
        }

        $xmlexport = Zend_Registry::get('xmlexport');

        foreach ($xmlexport as $export) {
            $addzip = true;
            $archive->addFile($export['value'], $this->order->alias . '_' . $i . '/' . $export['name']);
        }

        $addzip = false;
        $uploads = Doctrine_Query::create()
                        ->from('Upload m')
                        ->where('orderpos_id = ?', array($pos->id))->execute();

        
        foreach ($uploads as $upload) {
            $archive->addFile(PUBLIC_PATH . '/uploads/' . $this->order->Shop->uid . '/article/' . $upload->path, $this->order->alias . '_' . $i . '_' . $art_id . "_" . $upload->name);
        }

        $path = str_split($pos->id);

        if ($pos->Article->not_edit && $pos->Article->mercendise_design != "") {
            $des = Zend_Json::decode($pos->Article->mercendise_design);
        } elseif ($pos->a9_designer_data != "") {
            $des = Zend_Json::decode($pos->a9_designer_data);
        } else {
            $des = false;
        }

        if ($des) {

            if ($pos->Article->not_edit) {
                $archive->addFile($pos->Article->mercendise_thumbnail, $this->order->alias . '_' . $i . '_' . $art_id . "_image.jpg");
            } else {
                if (file_exists(APPLICATION_PATH . '/../market/designer/basket/' . implode('/', $path) . '/image.jpg')) {
                    $archive->addFile(APPLICATION_PATH . '/../market/designer/basket/' . implode('/', $path) . '/image.jpg', $this->order->alias . '_' . $i . '_' . $art_id . "_image.jpg");
                }
            }

            $xmldes = simplexml_load_string('<?xml version="1.0"?><root>' .
                    $des['design_config']
                    . '</root>');
            Zend_Registry::get('log')->debug($des['design_config']);
            require_once('PHPExcel/PHPExcel.php');
            $objPHPExcel = new PHPExcel();
            $objPHPExcel->setActiveSheetIndex(0);
            $objPHPExcel->getActiveSheet()->SetCellValue('A1', 'Side');
            $objPHPExcel->getActiveSheet()->SetCellValue('B1', 'Type');
            $objPHPExcel->getActiveSheet()->SetCellValue('C1', 'String');
            $objPHPExcel->getActiveSheet()->SetCellValue('D1', 'Face');
            $objPHPExcel->getActiveSheet()->SetCellValue('E1', 'Size');
            $objPHPExcel->getActiveSheet()->SetCellValue('F1', 'Bold');
            $objPHPExcel->getActiveSheet()->SetCellValue('G1', 'Italy');
            $objPHPExcel->getActiveSheet()->SetCellValue('H1', 'Underline');
            $objPHPExcel->getActiveSheet()->SetCellValue('I1', 'Color');
            $objPHPExcel->getActiveSheet()->SetCellValue('J1', 'Colorname');
            $objPHPExcel->getActiveSheet()->SetCellValue('K1', 'Spacing');
            $objPHPExcel->getActiveSheet()->SetCellValue('L1', 'Radius');

            $exceli = 2;
            foreach ($xmldes->clip as $el) {


                if ($el['type'] == 'img') {

                    if (strpos($el['path_str'], "readusermotivebig")) {
                        $match = str_replace("/service/designer/readusermotivebig?id=", "", str_replace(",undefined,undefined", "", $el['path_str']));
                        $motiv = Doctrine_Query::create()
                                        ->from('Motiv a')
                                        ->where('a.uuid = ?', array($match))->fetchOne();
                        $path_parts = pathinfo($motiv['file_work']);
                        $archive->addFile(APPLICATION_PATH . '/../market/motive/' . $motiv['file_work'], $path_parts['basename']);

                        $objPHPExcel->getActiveSheet()->SetCellValue('A' . $exceli, $el['side']);
                        $objPHPExcel->getActiveSheet()->SetCellValue('B' . $exceli, $el['type']);
                        $objPHPExcel->getActiveSheet()->SetCellValue('C' . $exceli, $el['path_str']);
                    }
                    $exceli++;
                }
                if ($el['type'] == 'txt') {
                    if ($el['path_str'] != "") {
                        $objPHPExcel->getActiveSheet()->SetCellValue('A' . $exceli, $el['side']);
                        $objPHPExcel->getActiveSheet()->SetCellValue('B' . $exceli, $el['type']);
                        $objPHPExcel->getActiveSheet()->SetCellValue('C' . $exceli, $el['path_str']);
                        $objPHPExcel->getActiveSheet()->SetCellValue('D' . $exceli, $el['fntface']);
                        $objPHPExcel->getActiveSheet()->SetCellValue('E' . $exceli, $el['fntsize']);
                        $objPHPExcel->getActiveSheet()->SetCellValue('F' . $exceli, $el['fntbld']);
                        $objPHPExcel->getActiveSheet()->SetCellValue('G' . $exceli, $el['fntitl']);
                        $objPHPExcel->getActiveSheet()->SetCellValue('H' . $exceli, $el['fntund']);
                        $objPHPExcel->getActiveSheet()->SetCellValue('I' . $exceli, $el['col']);
                        $objPHPExcel->getActiveSheet()->SetCellValue('J' . $exceli, $el['colname']);
                        $objPHPExcel->getActiveSheet()->SetCellValue('K' . $exceli, $el['spacing']);
                        $objPHPExcel->getActiveSheet()->SetCellValue('L' . $exceli, $el['radius']);
                    }
                    $exceli++;
                }
            }

            $writer = new PHPExcel_Writer_Excel2007($objPHPExcel);
            $writer->save(APPLICATION_PATH . '/../data/customerdocs/' . $this->order->Shop->id . "/" . $this->order->alias . '_' . $i . '.xlsx');
            $archive->addFile(APPLICATION_PATH . '/../data/customerdocs/' . $this->order->Shop->id . "/" . $this->order->alias . '_' . $i . '.xlsx', 'data.xlsx');

        }
        if (file_exists(APPLICATION_PATH . '/../market/templateprint/basket/' . $this->order->id . '/' . $pos->pos . '/final.pdf')) {
            $archive->addFile(APPLICATION_PATH . '/../market/templateprint/basket/' . $this->order->id . '/' . $pos->pos . '/final.pdf', $this->order->alias . '_' . $pos->pos .'_' . str_replace(" ", "-", $pos->ref) . '_final.pdf');

            $addzip = true;
        }

        if ($pos->xmlxslfofile != "" && !$pos->render_print) {
            $path = implode('/', str_split($pos->id));

            $dir = new RecursiveIteratorIterator(new RecursiveDirectoryIterator(APPLICATION_PATH . '/../data/renderings/' . $path));

            $ii = 0;
            foreach ($dir as $fileinfo) {
                if ($fileinfo->isDir()) {
                    $ii++;
                }
                if ($fileinfo->isFile() && strpos($fileinfo->getFilename(), 'pdf')) {
                    $archive->addFile($fileinfo->getRealPath(), $this->order->alias . '_' . $ii . '_' . $art_id . ".pdf");
                }
            }

            if(file_exists(APPLICATION_PATH . '/../data/renderings/' . $path . '/print_preview.pdf')) {
                $archive->addFile(APPLICATION_PATH . '/../data/renderings/' . $path . '/print_preview.pdf', 'print_preview.pdf');
            }
        } elseif ($pos->xmlxslfofile != "" && $pos->render_print) {
            echo "Noch kein PDF gerendert. Bitte warten!!!";
            die();
        }


        $archive->close();

        $this->file = APPLICATION_PATH . '/../data/customerdocs/' . $this->order->Shop->id . "/" . $this->order->alias . '_' . $this->orderpos->id . '.zip';
    }

    public function getFileName() {
        return $this->file;
    }

    // Fuktion readfile_chunked();
    protected function readfile_chunked($filename)
    {
        $chunksize = 1 * (1024 * 1024); // how many bytes per chunk
        $buffer = '';
        $handle = fopen($filename, 'rb');
        if ($handle === false) {
            return false;
        }
        while (!feof($handle)) {
            $buffer = fread($handle, $chunksize);
            print $buffer;
            ob_flush();
            flush();
        }
        return fclose($handle);
        die();
    }

}