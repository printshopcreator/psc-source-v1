<?php

class TP_Payment
{

    public static function process(TP_Basket $obj)
    {
        $payment = Doctrine_Query::create()->from('Paymenttype p')->where('id = ?')->fetchOne(array(
            $obj->getPaymenttype()
        ));

        if($payment && $payment->getPaymentGateway() != '') {

            if($obj->getToken() == "") {
                $obj->setToken(TP_Util::uuid());
            }

            if((round($obj->getPreisBrutto(), 2)) <= 0) {
                return true;
            }
            $params = Zend_Registry::get('params');

            $user = Zend_Auth::getInstance()->getIdentity();
            $user = Doctrine_Query::create()
                ->from('Contact m')
                ->where('id = ?')->fetchOne(array($user['id']));

            if(isset($params['paymentRef'])) {
                $obj->setPaymentRef($params['paymentRef']);
            }

            if(isset($params['paymentId'])) {
                $shop = Zend_Registry::get('shop');
                $shop = Doctrine_Query::create()->from('Shop c')->where('c.id = ?', array(
                    $shop['id']))->fetchOne();

                header('Location: /apps/payment/do/'.$shop->getApiKey().'/' . $payment->getPaymentGateway() . '?&hash='.urlencode($obj->getToken()).'&paymentId='.$params['paymentId'].'&PayerID='.$params['PayerID'].'&contact='.$user->uuid.'&invoiceAddress='.$obj->getInvoice().'&deliveryAddress='.$obj->getDelivery().'&amount='.round($obj->getPreisBrutto(),2));
                die();
            }

            if((!isset($params['hash']) || 'Ja' != $params['hash']) && !isset($params['token'])) {
                $shop = Zend_Registry::get('shop');
                $shop = Doctrine_Query::create()->from('Shop c')->where('c.id = ?', array(
                    $shop['id']))->fetchOne();

                header('Location: /apps/payment/pay/'.$shop->getApiKey().'/' . $payment->getPaymentGateway() . '?&hash='.urlencode($obj->getToken()).'&contact='.$user->uuid.'&invoiceAddress='.$obj->getInvoice().'&deliveryAddress='.$obj->getDelivery().'&amount='.round($obj->getPreisBrutto(),2));

                die();
            }


        }
    }

}