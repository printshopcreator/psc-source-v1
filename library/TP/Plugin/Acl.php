<?php

class TP_Plugin_Acl extends Zend_Acl {
    /*
     * Roles
     * @var array
     */

    protected $_rolesArray = array();

    /*
     * Resources
     * @var array
     */
    protected $_resourcesArray = array();

    /*
     * Privileges
     * @var array
     */
    protected $_privilegesArray = array();

    /*
     * ResourcePrivileges
     * @var array
     */
    protected $_resourcePrivilegesArray = array();

    /*
     * Constructor
     */

    public function __construct() {
        $this->_loadRoles();
        $this->_loadResources();
        $this->_loadPrivileges();

        $this->_loadResourcePrivileges();
        $this->_loadRolePrivileges();
    }

    public function isAllowed($role = null, $resource = null, $privilege = null) {
        if($resource == 'default_uploadcenter') {
            return true;
        }
        return parent::isAllowed($role, $resource, $privilege);
    }

    /*
     * Loading Roles
     * 
     * @model RolesModel
     */

    protected function _loadRoles() {

        if(!file_exists("/data/www/old/cache/role.ser")) {

            $dataRoles = Doctrine_Query::create()
                ->from('Role m')
                ->orderBy('m.level DESC')
                ->execute();

            foreach ($dataRoles as $dataRole) {
                // Add Role to internal Array
                $this->_rolesArray[$dataRole->id] = $dataRole->name;
            }

            file_put_contents("/data/www/old/cache/role.ser", serialize($this->_rolesArray));

        }else{
            $this->_rolesArray = unserialize(file_get_contents("/data/www/old/cache/role.ser"));
        }


        foreach ($this->_rolesArray  as $dataRole) {

            $this->addRole(new Zend_Acl_Role($dataRole));
        }
    }

    /*
     * Loading Resources
     * 
     * @model ResourcesModel
     */

    protected function _loadResources() {
        // Load Model

        if(!file_exists("/data/www/old/cache/ac.ser")) {
            $dataResources = Doctrine_Query::create()
                ->from('Resource m')
                ->execute();

            foreach ($dataResources as $dataResource) {

                // Add Resources to internal Array
                $this->_resourcesArray[$dataResource->id] = $dataResource->modul . '_' . $dataResource->name;
            }

            file_put_contents("/data/www/old/cache/ac.ser", serialize($this->_resourcesArray));
        }else{

            $this->_resourcesArray = unserialize(file_get_contents("/data/www/old/cache/ac.ser"));

        }

        foreach ($this->_resourcesArray as $dataResource) {

            $this->add(new Zend_Acl_Resource($dataResource));
        }
    }

    /*
     * Loading Privileges
     * 
     * @model PrivilegesModel
     */

    protected function _loadPrivileges() {

        if(!file_exists("/data/www/old/cache/privilege.ser")) {


        $dataPrivileges = Doctrine_Query::create()
                        ->from('Privilege m')
                        ->execute();

            foreach ($dataPrivileges as $dataPrivilege) {
                // Add Privileges to internal Array
                $this->_privilegesArray[$dataPrivilege->id] = $dataPrivilege->name;
            }

            file_put_contents("/data/www/old/cache/privilege.ser", serialize($this->_privilegesArray));
        }else{

            $this->_privilegesArray = unserialize(file_get_contents("/data/www/old/cache/privilege.ser"));

        }
    }

    /*
     * Loading ResourcePrivileges
     * 
     * @model ResourcePrivilegesModel
     */

    protected function _loadResourcePrivileges() {
        // Load Model


        if(!file_exists("/data/www/old/cache/rep.ser")) {

            $dataResourcePrivileges = Doctrine_Query::create()
                ->from('ResourcePrivilege m')
                ->orderBy('m.sort ASC')
                ->execute();

            foreach ($dataResourcePrivileges as $dataResourcePrivilege) {
                $this->_resourcePrivilegesArray[$dataResourcePrivilege->id] = array(
                    'resource_id' => $dataResourcePrivilege->resource_id,
                    'privilege_id' => $dataResourcePrivilege->privilege_id
                );
            }

            file_put_contents("/data/www/old/cache/rep.ser", serialize($this->_resourcePrivilegesArray));
        }else{

            $this->_resourcePrivilegesArray = unserialize(file_get_contents("/data/www/old/cache/rep.ser"));

        }
    }

    /*
     * Loading RolePrivileges
     * 
     * @model RolePrivilegesModel
     */

    protected function _loadRolePrivileges() {
        // Load Model
        if(!file_exists("/data/www/old/cache/rp.ser")) {

            $dataRolePrivileges = Doctrine_Query::create()
                ->from('RolePrivilege m')
                ->execute();

            $tmp = [];

            foreach ($dataRolePrivileges as $dataRolePrivilege) {
                $tmp[] = [
                    'name' => $dataRolePrivilege->ResourcePrivilege->Privilege->name,
                    'role_id' => $dataRolePrivilege->role_id,
                    'resource_privilege_id' => $dataRolePrivilege->resource_privilege_id
                ];
            }

            file_put_contents("/data/www/old/cache/rp.ser", serialize($tmp));
        }else{

            $tmp = unserialize(file_get_contents("/data/www/old/cache/rp.ser"));

        }

        foreach ($tmp as $row) {
            if ($row['name'] == 'all') {
                $this->allow(
                        $this->_rolesArray[$row['role_id']],
                        $this->_resourcesArray[$this->_resourcePrivilegesArray[$row['resource_privilege_id']]['resource_id']],
                        null
                );
            } else {
                $this->allow(
                        $this->_rolesArray[$row['role_id']],
                        $this->_resourcesArray[$this->_resourcePrivilegesArray[$row['resource_privilege_id']]['resource_id']],
                        $this->_privilegesArray[$this->_resourcePrivilegesArray[$row['resource_privilege_id']]['privilege_id']]
                );
            }

        }
    }

}

