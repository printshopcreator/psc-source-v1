<?php
class TP_Plugin_Auth extends Zend_Controller_Plugin_Abstract
{
    private $_auth;
    private $_acl;
    
    private $_noauth = array('module' => 'admin',
                             'controller' => 'login',
                             'action' => 'index');
                             
    private $_noacl = array('module' => 'admin',
                            'controller' => 'login',
                            'action' => 'index');
                            
    private $_nosite = array('module' => 'default',
                             'controller' => 'error',
                             'action' => 'error');
                            
    public function __construct($auth, $acl)
    {
        $this->_auth = $auth;
        $this->_acl = $acl;
    }
    
    public function preDispatch(Zend_Controller_Request_Abstract $request)
    {
    	
    	 if ($request->getParam('pass') && $request->getParam('user') != 'Benutzername') {
            // TODO input validation...
            $_authAdapter = new TP_Plugin_AuthAdapter(); // put this in a constructor?
            $_authAdapter->setIdentity($request->getParam('user'))
                         ->setCredential($request->getParam('pass'));
            $result = Zend_Auth::getInstance()->authenticate($_authAdapter);

            if($result->isValid()){
            	echo json_encode(array('success' => true));
	       		die();
            }else{
            	echo json_encode(array('success' => false, 'errors' => array('reason' => $result->getMessages())));
       			die();
            }
            // TODO restore parameters of the called page
        }
    	
    	if ($this->_auth->hasIdentity())
        {
            $user = $this->_auth->getIdentity();
        	$user = Doctrine_Query::create() 
                    ->from('Contact m')
                    ->where('id = ?')->fetchOne(array($user['id']));

        }else{
        	$role = 'guest';
        }
        
        $controller = $request->getControllerName();
        $action = $request->getActionName();
        $module = $request->getModuleName();
        
        $resource = $controller;

        if (!$this->_acl->has($module.'_'.$resource)) {
            $this->_request->setModuleName($this->_nosite['module']);
            $this->_request->setControllerName($this->_nosite['controller']);
            $this->_request->setActionName($this->_nosite['action']);
        }else{

	        if(isset($user)) {
			        if (!$this->isAllowed($user, $module.'_'.$resource, $action))
			        {
			            if (!$this->_auth->hasIdentity())
			            {
			                $this->_request->setModuleName($this->_noauth['module']);
			                $this->_request->setControllerName($this->_noauth['controller']);
			                $this->_request->setActionName($this->_noauth['action']);
			            }
			            else
			            {
			                $this->_request->setModuleName($this->_noacl['module']);
			                $this->_request->setControllerName($this->_noacl['controller']);
			                $this->_request->setActionName($this->_noacl['action']);
			            }
			        }
	        }else{
	        	if (!$this->_acl->isAllowed($role, $module.'_'.$resource, $action))
			        {
			            if (!$this->_auth->hasIdentity())
			            {
			                $this->_request->setModuleName($this->_noauth['module']);
			                $this->_request->setControllerName($this->_noauth['controller']);
			                $this->_request->setActionName($this->_noauth['action']);
			            }
			            else
			            {
			                $this->_request->setModuleName($this->_noacl['module']);
			                $this->_request->setControllerName($this->_noacl['controller']);
			                $this->_request->setActionName($this->_noacl['action']);
			            }
			        }
	        }
        }
    }
    
    private function isAllowed($user, $res, $action) {
    	
    	foreach ($user->Roles as $role) {
      		
    			if($this->_acl->isAllowed($role->name, $res, $action)) return true;
      		
    	}
    	if($this->_acl->isAllowed('guest', $res, $action)) return true;
    	return false;
    }
}  
