<?php

class TP_Plugin_AuthAdapter implements Zend_Auth_Adapter_Interface
{

    /**
     * $_identity - Identity value
     *
     * @var string
     */
    protected $_identity = null;

    /**
     * $_credential - Credential values
     *
     * @var string
     */
    protected $_credential = null;

    /**
     * $_apiLogin - ApiLogin
     *
     * @var boolean
     */
    protected $_apiLogin = false;


    protected $_idLogin = false;

    /**
     * setIdentity() - set the value to be used as the identity
     *
     * @param  string $value
     * @return Zend_Auth_Adapter_DbTable Provides a fluent interface
     */
    public function setIdentity($value)
    {
        $this->_identity = $value;
        return $this;
    }

    /**
     * setCredential() - set the credential value to be used, optionally can specify a treatment
     * to be used, should be supplied in parameterized form, such as 'MD5(?)' or 'PASSWORD(?)'
     *
     * @param  string $credential
     * @return Zend_Auth_Adapter_DbTable Provides a fluent interface
     */
    public function setCredential($credential)
    {
        $this->_credential = $credential;
        return $this;
    }

    public function authenticate()
    {
        $_identity = null;

        if($this->_idLogin !== false) {
            $row = Doctrine_Query::create()
                ->from('Contact m')
                ->where('m.id= ?', array($this->_idLogin))
                ->execute()->getFirst();
            $_identity = $row->toArray(true);
            return new Zend_Auth_Result(true, $_identity, array('Erfolgreich authentifiziert an der Datendank'));
        }

        $shop = Zend_Registry::get('shop');

        if($shop['install_id'] == 4999) {
            $row = Doctrine_Query::create()
                ->from('Contact m')
                ->where('m.self_email = ? AND m.enable = 1 AND m.virtual = 0 AND m.install_id = ?', array($this->_identity, $shop['install_id']))
                ->execute()->getFirst();
        }elseif($this->_apiLogin) {
            $row = Doctrine_Query::create()
                ->from('Contact m')
                ->leftJoin('m.Shops as s')
                ->where('m.id = ? AND m.enable = 1 AND s.id = ?', array($this->_identity, $shop['id']))
                ->execute()->getFirst();
        }else{
            if($shop['useemailaslogin'] == true) {
                $row = Doctrine_Query::create()
                    ->from('Contact m')
                    ->leftJoin('m.Shops as s')
                    ->where('m.self_email = ? AND m.enable = 1 AND m.virtual = 0 AND s.id = ?', array($this->_identity, $shop['id']))
                    ->execute()->getFirst();
            }else{
                $row = Doctrine_Query::create()
                    ->from('Contact m')
                    ->leftJoin('m.Shops as s')
                    ->where('m.name = ? AND m.enable = 1 AND m.virtual = 0 AND s.id = ?', array($this->_identity, $shop['id']))
                    ->execute()->getFirst();
            }
        }

        // Es darf diesen Benutzer nur genau 1x geben
        if (!isset($row->id)) {
            return new Zend_Auth_Result(false, $_identity, array('Login failure'));
        }
        if($row->locked || $row->Account->locked) {
            return new Zend_Auth_Result(false, $_identity, array('Sie wurden gesperrt'));
        }

        if ($row->id != '' && $row->password == $this->_credential && !$this->_apiLogin) {
            $row->password = password_hash($row->password, PASSWORD_DEFAULT);
            $row->save();
            $_identity = $row->toArray(true);
            $_identity['name1'] = $_identity['self_firstname'].' '.$_identity['self_lastname'];
            return new Zend_Auth_Result(true, $_identity, array('Erfolgreich authentifiziert an der Datendank'));

        }elseif(function_exists('password_verify') && password_verify ($this->_credential, $row->password) && !$this->_apiLogin) {
            $_identity = $row->toArray(true);
            $_identity['name1'] = $_identity['self_firstname'].' '.$_identity['self_lastname'];
            return new Zend_Auth_Result(true, $_identity, array('Erfolgreich authentifiziert an der Datendank'));
        }elseif($this->_apiLogin) {
            $_identity = $row->toArray(true);
            $_identity['name1'] = $_identity['self_firstname'].' '.$_identity['self_lastname'];
            return new Zend_Auth_Result(true, $_identity, array('Erfolgreich authentifiziert an der Datendank'));
        }else{
            return new Zend_Auth_Result(false, $_identity, array('Passwort falsch!'));
        }
    }

    /**
     * @return boolean
     */
    public function isApiLogin()
    {
        return $this->_apiLogin;
    }

    /**
     * @param boolean $apiLogin
     */
    public function setApiLogin($apiLogin)
    {
        $this->_apiLogin = $apiLogin;
    }

    /**
     * @return bool
     */
    public function isIdLogin()
    {
        return $this->_idLogin;
    }

    /**
     * @param bool $idLogin
     */
    public function setIdLogin($idLogin)
    {
        $this->_idLogin = $idLogin;
    }
}
