<?php

class TP_Preflight {

    protected $article = null;
    protected $basket = null;
    protected $basketItem = null;
    protected $isValid = true;

    protected $errors = array();

    public function __construct(Article $article, TP_Basket $basket, TP_Basket_Item $basketItem) {
        $this->article = $article;
        $this->basket = $basket;
        $this->basketItem = $basketItem;
    }

    public function validate() {
        if($this->article->simple_preflight) {
            return $this->quickValidate();
        }
    }

    public function isValid() {
        return $this->isValid;
    }

    public function getErrors() {
        return $this->errors;
    }

    protected function quickValidate() {

        $count = 0;
        foreach($this->basketItem->getFiles() as $key => $file) {

            $finfo = new finfo(FILEINFO_MIME_TYPE);
            $finfo = $finfo->file(PUBLIC_PATH . '/uploads/'. $this->article->Shop->uid. '/article/'. $file['value']);

            if($finfo != 'application/pdf') {
                $this->setIsNotValid();
                $this->errors[] = array('type' => 'format', 'mode' => 'noteqal', 'key' => $key , 'must' => 'pdf', 'act' => str_replace('/', '', strrchr($finfo, '/')));
            }

            if($finfo == 'application/pdf') {

                $im = new imagick(PUBLIC_PATH . '/uploads/'. $this->article->Shop->uid. '/article/'. $file['value']);
                $count += $im->getNumberImages();

            }else{
                $count += 1;
            }

        }

        if($this->basketItem->getDesignerSeiten() > $count) {
            $this->setIsNotValid();
            $this->errors[] = array('type' => 'count', 'mode' => 'equal', 'must' => $this->basketItem->getDesignerSeiten(), 'act' => $count);
        }

    }

    protected function setIsNotValid() {
        $this->isValid = false;
    }

}