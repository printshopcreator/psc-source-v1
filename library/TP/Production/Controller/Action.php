<?php

class TP_Production_Controller_Action extends TP_Admin_Controller_Action {

    protected $translate = null;
    protected $shop = null;
    protected $shopIds = array();
    protected $user = null;
    protected $highRole = null;
    protected $highRoleName = null;
    public $install = null;

    public function init() {
        parent::init();

        $this->_helper->layout->setLayout('default')
                ->setLayoutPath(APPLICATION_PATH . '/design/production/layout')
                ->setContentKey('CONTENT');
        $this->view->setScriptPath(APPLICATION_PATH . '/design/production/templates');

    }

}