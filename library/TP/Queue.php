<?php

class TP_Queue {
	
    function __construct() {
    }

    public static function process($cmd, $mode, $obj) {

        $shop = Zend_Registry::get('shop');
        $install = Zend_Registry::get('install');

        if($obj instanceof Orders) {
            $shop = $obj->Shop;
        }
        if($obj instanceof Orderspos) {
            $shop = $obj->Orders->Shop;
        }



        if($shop['market'] == true) {

            $records = Doctrine_Query::create()
                      ->from('Queues as q')
                      ->where('q.shop_id = ? AND q.action = ? AND shop = ? AND enable = 1', array($install['defaultmarket'], $cmd, $shop['id']))
                      ->orderBy('pos ASC')
                      ->execute();

            if(count($records) == 0) {
                $records = Doctrine_Query::create()
                    ->from('Queues as q')
                    ->where('q.shop_id = ? AND q.action = ? AND shop = 0 AND enable = 1', array($install['defaultmarket'], $cmd))
                    ->orderBy('pos ASC')
                    ->execute();
            }

        }else{

            $records = Doctrine_Query::create()
                      ->from('Queues as q')
                      ->where('q.shop_id = ? AND q.action = ? AND shop = ? AND enable = 1', array($shop['id'], $cmd, $shop['id']))
                      ->orderBy('pos ASC')
                      ->execute();

            if(count($records) == 0) {
                $records = Doctrine_Query::create()
                    ->from('Queues as q')
                    ->where('q.shop_id = ? AND q.action = ? AND shop = 0 AND enable = 1', array($shop['id'], $cmd))
                    ->orderBy('pos ASC')
                    ->execute();
            }

        }
        Zend_Registry::get('log')->debug("QUEUE".count($records));
        foreach($records as $record) {
           $object = unserialize($record->data);
           if($object->mode != $mode) continue;
           if(!is_object($obj)) {
               $object->process($object, $obj);
           }else{
               $object->process($object, $obj);
           }

        }

    }
}