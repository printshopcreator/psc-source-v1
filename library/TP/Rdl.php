<?php

class TP_Rdl {


    private $_templates = array();

    private $_submittals = array();

    private $_reportPath = '';

    private $_agb = false;

    function __construct($reportPath, $file, $site, $params = array(), $data = array()) {

        $this->_reportPath = $reportPath;

		try {

            if(file_exists($reportPath.$file) && $file != null)
			    $this->_templates[$site] = new TP_Rdl_Template($reportPath.$file, $params, $data);
		    
		}catch(Exception $e) {
            echo $e->getMessage();
		}
	}

    protected function addSite($file, $site) {

        
    }


    public function export($renderer) {
        $renderer->setSubmittals($this->_submittals);
        $renderer->setAgb($this->_agb);
        $renderer->export($this->_templates);
    }

    public function getReportPath() {
        return $this->_reportPath;
    }

    public function addSubmittals($var) {
        $this->_submittals[] = $var;
    }

    public function setAgb($var) {
        $this->_agb = $var;
    }

}