<?php

class TP_Rdl_Band {

    protected $_height = 0;
    
    protected $_elements = array();


    function __construct($height) {
        $this->_height = $height;
    }

    public function loadElementsXml($xml) {

        foreach($xml as $key => $elementXml) {
            switch($key) {
                case 'staticText':
                    $element = new TP_Rdl_Element_Statictext( (int)$elementXml->reportElement['x'], (int)$elementXml->reportElement['y'], (int)$elementXml->reportElement['width'], (int)$elementXml->reportElement['height']);
                    $element->setText( (string)$elementXml->text);
                    if(isset($elementXml->reportElement->printWhenExpression)) $element->setPrintWhen((string)$elementXml->reportElement->printWhenExpression);
                    if(isset($elementXml->textElement) && isset($elementXml->textElement->font['size'])) $element->setFontSize((int)$elementXml->textElement->font['size']);
                    if(isset($elementXml->textElement) && isset($elementXml->textElement->font['fontName'])) $element->setFontName((string)$elementXml->textElement->font['fontName']);
                    if(isset($elementXml->textElement['textAlignment']) && isset($elementXml->textElement['textAlignment'])) $element->setAlign((string)$elementXml->textElement['textAlignment']);
                    if(isset($elementXml->textElement) && isset($elementXml->textElement->font['pdfFontName'])) $element->setFontFile((string)$elementXml->textElement->font['pdfFontName']);
                    if(isset($elementXml->textElement) && isset($elementXml->textElement->font['isBold']) && $elementXml->textElement->font['isBold'] == 'true') $element->setBold((bool)$elementXml->textElement->font['isBold']);
                    if(isset($elementXml->textElement) && isset($elementXml->textElement->font['isItalic']) && $elementXml->textElement->font['isItalic'] == 'true') $element->setItalic((bool)$elementXml->textElement->font['isItalic']);
                    if(isset($elementXml->reportElement) && isset($elementXml->reportElement['forecolor'])) $element->setColor((string)$elementXml->reportElement['forecolor']);
                    $this->_elements[] = $element;
                break;
                case 'textField':
                    $element = new TP_Rdl_Element_Textfield( (int)$elementXml->reportElement['x'], (int)$elementXml->reportElement['y'], (int)$elementXml->reportElement['width'], (int)$elementXml->reportElement['height']);
                    $element->setExpression( (string)$elementXml->textFieldExpression);
                    if(isset($elementXml->reportElement->printWhenExpression)) $element->setPrintWhen((string)$elementXml->reportElement->printWhenExpression);
                    if(isset($elementXml->textFieldExpression) && isset($elementXml->textFieldExpression['class'])) $element->setClass((string)$elementXml->textFieldExpression['class']);
                    if(isset($elementXml->textElement) && isset($elementXml->textElement->font['size'])) $element->setFontSize((int)$elementXml->textElement->font['size']);
                    if(isset($elementXml->textElement) && isset($elementXml->textElement->font['fontName'])) $element->setFontName((string)$elementXml->textElement->font['fontName']);
                    if(isset($elementXml->textElement['textAlignment']) && isset($elementXml->textElement['textAlignment'])) $element->setAlign((string)$elementXml->textElement['textAlignment']);
                    if(isset($elementXml->textElement) && isset($elementXml->textElement->font['pdfFontName'])) $element->setFontFile((string)$elementXml->textElement->font['pdfFontName']);
                    if(isset($elementXml->textElement) && isset($elementXml->textElement->font['isBold']) && $elementXml->textElement->font['isBold'] == 'true') $element->setBold((bool)$elementXml->textElement->font['isBold']);
                    if(isset($elementXml->textElement) && isset($elementXml->textElement->font['isItalic']) && $elementXml->textElement->font['isItalic'] == 'true') $element->setItalic((bool)$elementXml->textElement->font['isItalic']);
                    if(isset($elementXml['pattern'])) $element->setPattern((string)$elementXml['pattern']);
                    if(isset($elementXml['isStretchWithOverflow'])) $element->setMoveNext(true);
                    if(isset($elementXml->reportElement['key'])) $element->setKey((string)$elementXml->reportElement['key']);
                    
                    if(isset($elementXml->reportElement) && isset($elementXml->reportElement['forecolor'])) $element->setColor((string)$elementXml->reportElement['forecolor']);
                    $this->_elements[] = $element;
                break;
                case 'line':
                    $element = new TP_Rdl_Element_Line( (int)$elementXml->reportElement['x'], (int)$elementXml->reportElement['y'], (int)$elementXml->reportElement['width'], (int)$elementXml->reportElement['height']);
                    if(isset($elementXml->reportElement->printWhenExpression)) $element->setPrintWhen((string)$elementXml->reportElement->printWhenExpression);
                    if(isset($elementXml->graphicElement) && isset($elementXml->graphicElement->pen['lineWidth'])) $element->setSize((string)$elementXml->graphicElement->pen['lineWidth']);
                    if(isset($elementXml->reportElement) && isset($elementXml->reportElement['forecolor'])) $element->setColor((string)$elementXml->reportElement['forecolor']);
                    if(isset($elementXml->graphicElement) && isset($elementXml->graphicElement->pen['lineColor'])) $element->setColor((string)$elementXml->graphicElement->pen['lineColor']);

                    $this->_elements[] = $element;
                break;
                case 'break':
                    $element = new TP_Rdl_Element_Break( (int)$elementXml->reportElement['x'], (int)$elementXml->reportElement['y'], 0, (int)$elementXml->reportElement['height']);
                    if(isset($elementXml->reportElement->printWhenExpression)) $element->setPrintWhen((string)$elementXml->reportElement->printWhenExpression);
                    
                    $this->_elements[] = $element;
                break;
                case 'rectangle':
                    $element = new TP_Rdl_Element_Rectangle( (int)$elementXml->reportElement['x'], (int)$elementXml->reportElement['y'], (int)$elementXml->reportElement['width'], (int)$elementXml->reportElement['height']);
                    if(isset($elementXml->reportElement->printWhenExpression)) $element->setPrintWhen((string)$elementXml->reportElement->printWhenExpression);
                    if(isset($elementXml->graphicElement) && isset($elementXml->graphicElement->pen['lineWidth'])) $element->setSize((string)$elementXml->graphicElement->pen['lineWidth']);
                    if(isset($elementXml->reportElement) && isset($elementXml->reportElement['forecolor'])) $element->setColor((string)$elementXml->reportElement['forecolor']);
                    if(isset($elementXml->graphicElement) && isset($elementXml->graphicElement->pen['lineColor'])) $element->setColor((string)$elementXml->graphicElement->pen['lineColor']);
                    if(isset($elementXml->reportElement) && isset($elementXml->reportElement['backcolor'])) $element->setBackColor((string)$elementXml->reportElement['backcolor']);
                    if(isset($elementXml['radius'])) $element->setRadius((int)$elementXml['radius']);


                    $this->_elements[] = $element;
                break;
                case 'image':
                    $element = new TP_Rdl_Element_Image( (int)$elementXml->reportElement['x'], (int)$elementXml->reportElement['y'], (int)$elementXml->reportElement['width'], (int)$elementXml->reportElement['height']);
                    if(isset($elementXml->reportElement->printWhenExpression)) $element->setPrintWhen((string)$elementXml->reportElement->printWhenExpression);
                    if(isset($elementXml->imageExpression)) $element->setSource((string)$elementXml->imageExpression);
                    if(isset($elementXml->reportElement['key'])) $element->setKey((string)$elementXml->reportElement['key']);
                    if(isset($elementXml->reportElement['key']) && ($elementXml->reportElement['key'] == 'barcodePost'||$elementXml->reportElement['key'] == 'barcode'||$elementXml->reportElement['key'] == 'barcode128') && isset($elementXml->imageExpression)) $element->setExpression((string)$elementXml->imageExpression);
                    $this->_elements[] = $element;
                break;
                case 'componentElement':
                    switch((string)$elementXml->reportElement['key']) {
                        case 'table':
                            $elementT = new TP_Rdl_Element_Table( (int)$elementXml->reportElement['x'], (int)$elementXml->reportElement['y'], (int)$elementXml->reportElement['width'], (int)$elementXml->reportElement['height']);
                            $elementXml->registerXPathNamespace('jr', 'http://jasperreports.sourceforge.net/jasperreports/components');
                            $elementT->setExpression( (string)$elementXml->children('http://jasperreports.sourceforge.net/jasperreports/components')->table->children()->datasetRun->dataSourceExpression);
                            if(isset($elementXml['isStretchWithOverflow'])) {

                                $elementT->setMoveNext((int)$elementXml['isStretchWithOverflow']);
                            }
                            foreach($elementXml->children('http://jasperreports.sourceforge.net/jasperreports/components')->table->column as $elm) {
                                if(isset($elm->detailCell->children()->textField)) {
                                    $elmText = $elm->detailCell->children()->textField;
                                    $element = new TP_Rdl_Element_Textfield( (int)$elmText->reportElement['x'], (int)$elmText->reportElement['y'], (int)$elmText->reportElement['width'], (int)$elmText->reportElement['height']);
                                    $element->setExpression( (string)$elmText->textFieldExpression);
                                    if(isset($elmText->textElement) && isset($elmText->textElement->font['size'])) $element->setFontSize((int)$elmText->textElement->font['size']);
                                    if(isset($elmText->textElement) && isset($elmText->textElement->font['fontName'])) $element->setFontName((string)$elmText->textElement->font['fontName']);
                                    if(isset($elmText->textElement['textAlignment']) && isset($elmText->textElement['textAlignment'])) $element->setAlign((string)$elmText->textElement['textAlignment']);
                                    if(isset($elmText->textElement) && isset($elmText->textElement->font['pdfFontName'])) $element->setFontFile((string)$elmText->textElement->font['pdfFontName']);
                                    if(isset($elmText->textElement) && isset($elmText->textElement->font['isBold']) && $elmText->textElement->font['isBold'] == 'true') $element->setBold((bool)$elmText->textElement->font['isBold']);
                                    if(isset($elmText->textElement) && isset($elmText->textElement->font['isItalic']) && $elmText->textElement->font['isItalic'] == 'true') $element->setItalic((bool)$elmText->textElement->font['isItalic']);
                                    if(isset($elmText->reportElement) && isset($elmText->reportElement['forecolor'])) $element->setColor((string)$elmText->reportElement['forecolor']);
                                    $elementT->addColumn($element);
                                    
                                }
                            }
                            $this->_elements[] = $elementT;
                        break;
                    }
                break;    
                default:
                break;
                
            }
        }

    }

    public function getElements() {
        return $this->_elements;
    }

    public function getHeight() {
        return $this->_height;
    }

}
