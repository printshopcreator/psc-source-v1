<?php

class TP_Rdl_Element {


    protected $_left = 0;

    protected $_top = 0;

    protected $_width = 0;

    protected $_height = 0;

    protected $_color = '#000000';

    protected $_key = '';
    
    protected $_printwhen = '';

    function __construct($left = 0, $top = 0, $width = 0, $height = 0, $color = '#000000') {
        $this->_left = $left;
        $this->_top = $top;
        $this->_width = $width;
        $this->_height = $height;
        $this->_color = $color;

    }

    public function setKey($val) {
        $this->_key = $val;
    }
    
    public function setPrintWhen($val) {
        $this->_printwhen = $val;
    }
    
    public function isPrintable($P,$V,$F) {
    
        if($this->_printwhen == '') return true;
        $st = array();
        preg_match('/(?<name>[$A-Z{A-Za-z0-9_}]+)\.(?<struct>[$a-z]+)\((?<val>[0-9]+)\)/i', $this->_printwhen, $st);
        eval('$name = '.str_replace('{', '[\'', str_replace('}', '\']', $st['name'])).';');
        switch($st['struct']) {
            case 'equals':
                if($name == $st['val'])  return true;
            break;
            case 'equalsNot':
                if($name != $st['val'])  return true;
                break;
            case 'isNotEmpty':
            	if(is_array($name)) {
            		if(count($name) > 0) return true;
            	}
                if(!is_array($name) && $name != '')  return true;
            break;
            case 'isEmpty':
                if($name == '')  return true;
            break;
            case 'greater':
                if($name > $st['val'])  return true;
            break;

        }
        
        return false;
    }

    public function getPrintWhen() {
        return $this->_printwhen;
    }
    
    public function setLeft($val) {
        $this->_left = intval($val);
    }

    public function setTop($val) {
        $this->_top = intval($val);
    }

    public function setWidth($val) {
        $this->_width = intval($val);
    }

    public function setHeight($val) {
        $this->_height = intval($val);
    }

    public function setColor($val) {
        $this->_color = $val;
    }

    public function getKey() {
        return $this->_key;
    }

    public function getColor() {
        return $this->_color;
    }

    public function getLeft() {
        return $this->_left;
    }

    public function getTop() {
        return $this->_top;
    }

    public function getWidth() {
        return $this->_width;
    }

    public function getHeight() {
        return $this->_height;
    }

}