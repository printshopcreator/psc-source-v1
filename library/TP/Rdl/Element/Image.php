<?php

class TP_Rdl_Element_Image extends TP_Rdl_Element {

    protected $_source = '1';

    protected $_expression = '';

    public function setSource($val) {
        $this->_source = $val;
    }

    public function getSource() {
        return $this->_source;
    }

    public function setExpression($val) {

        $this->_expression = str_replace('{', '[\'', str_replace('}', '\']', $val));
    }

    public function getExpression() {
        return $this->_expression;
    }
}