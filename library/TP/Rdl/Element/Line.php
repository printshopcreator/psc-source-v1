<?php

class TP_Rdl_Element_Line extends TP_Rdl_Element {

    protected $_size = 1;

    public function setSize($val) {
        $this->_size = $val;
    }

    public function getSize() {
        return $this->_size;
    }
}