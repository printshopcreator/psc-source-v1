<?php

class TP_Rdl_Element_Rectangle extends TP_Rdl_Element {

    protected $_size = 1;

    protected $_backColor = '#ffffff';

    protected $_radius = 0;

    public function setSize($val) {
        $this->_size = $val;
    }

    public function getSize() {
        return $this->_size;
    }

    public function setBackColor($val) {
        $this->_backColor = $val;
    }

    public function getBackColor() {
        return $this->_backColor;
    }

    public function setRadius($val) {
        $this->_radius = $val;
    }

    public function getRadius() {
        return $this->_radius;
    }
}