<?php

class TP_Rdl_Element_Statictext extends TP_Rdl_Element {

    protected $_text = '';

    protected $_fontSize = 10;

    protected $_fontName = '';

    protected $_fontFile = '';

    protected $_bold = false;

    protected $_italic = false;
    protected $_align = "Left";

    public function setAlign($var) {
        $this->_align = $var;
    }

    public function getAlign() {
        return $this->_align;
    }

    public function setText($val) {

        $this->_text = $val;
    }

    public function getText() {
        return $this->_text;
    }

    public function setFontSize($val) {
        $this->_fontSize = $val;
    }

    public function getFontSize() {
        return $this->_fontSize;
    }

    public function setFontName($val) {
        $this->_fontName = $val;
    }

    public function getFontName() {
        return $this->_fontName;
    }

    public function setFontFile($val) {
        $this->_fontFile = $val;
    }

    public function getFontFile() {
        return $this->_fontFile;
    }
    
    public function setBold($value) {
    	$this->_bold = $value;
    }
    
    public function getBold() {
    	return $this->_bold;
    }

    public function setItalic($value) {
    	$this->_italic = $value;
    }

    public function getItalic() {
    	return $this->_italic;
    }
}