<?php

class TP_Rdl_Element_Table extends TP_Rdl_Element {

    protected $_expression = '';
    protected $_columns = array();
    protected $_move_next = true;

    public function setExpression($val) {
        $this->_expression = str_replace('{', '[\'', str_replace('}', '\']', $val));
    }

    public function getExpression() {
        return $this->_expression;
    }

    public function addColumn($field) {
        $this->_columns[] = $field;
    }

    public function getColumn($index) {
        return $this->_columns[$index];
    }

    /**
     * @return boolean
     */
    public function getMoveNext()
    {
        return $this->_move_next;
    }

    /**
     * @param boolean $move_next
     */
    public function setMoveNext($move_next)
    {
        $this->_move_next = $move_next;
    }
}