<?php

class TP_Rdl_Element_Textfield extends TP_Rdl_Element {

    protected $_expression = '';
    protected $_fontSize = 10;
    protected $_class = '';
    protected $_fontName = '';
    protected $_fontFile = '';
    protected $_pattern = '';
    protected $_bold = false;
    protected $_italic = false;
    protected $_align = "Left";
    protected $_move_next = false;

    public function setAlign($var) {
        $this->_align = $var;
    }

    public function getAlign() {
        return $this->_align;
    }

    public function setExpression($val) {

        $this->_expression = str_replace('{', '[\'', str_replace('}', '\']', $val));
    }

    public function getExpression() {
        return $this->_expression;
    }

    public function setFontSize($val) {
        $this->_fontSize = $val;
    }

    public function getFontSize() {
        return $this->_fontSize;
    }
    
    public function setMoveNext($val) {
        $this->_move_next = $val;
    }

    public function getMoveNext() {
        return $this->_move_next;
    }

    public function setClass($val) {
        $this->_class = $val;
    }

    public function getClass() {
        return $this->_class;
    }
    
    public function setPattern($val) {
        $this->_pattern = $val;
    }

    public function getPattern() {
        return $this->_pattern;
    }

    public function setFontName($val) {
        $this->_fontName = $val;
    }

    public function getFontName() {
        return $this->_fontName;
    }

    public function setFontFile($val) {
        $this->_fontFile = $val;
    }

    public function getFontFile() {
        return $this->_fontFile;
    }

    public function setBold($value) {
        $this->_bold = $value;
    }

    public function getBold() {
        return $this->_bold;
    }

    public function setItalic($value) {
        $this->_italic = $value;
    }

    public function getItalic() {
        return $this->_italic;
    }

}