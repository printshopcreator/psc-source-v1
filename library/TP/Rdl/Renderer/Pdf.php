<?php

class TP_Rdl_Renderer_Pdf {

    protected $_file = '';
    protected $_actpdfsite = null;
    protected $_actheight = null;
    protected $_actparams = null;
    protected $_actvars = null;
    protected $_data = null;
    protected $_acttemplate = null;
    protected $_site1rows = 1;
    protected $_site1rowswl = 1;
    protected $_pdf = null;
    protected $_siterows = 1;
    protected $_lastsiterows = 1;
    protected $_submittals = array();
    protected $_agb = false;
    protected $_nextDataBreak = false;
    protected $_renderbarcode = false;
    protected $_renderbarcodePost = false;

    function __construct($file) {
        $this->_file = $file;
    }

    public function setSubmittals($var) {
        $this->_submittals = $var;
    }

    public function setAgb($var) {
        $this->_agb = $var;
    }

    protected function getWrappedText($string, Zend_Pdf_Resource_Font $font, $size, $max_width) {
        $wrappedText = '';



        $lines = explode("\n", $string);
        foreach ($lines as $line) {
            $words = explode(' ', $line);
            $word_count = count($words);
            $i = 0;
            $wrappedLine = '';
            while ($i < $word_count) {
                /* if adding a new word isn't wider than $max_width,
                  we add the word */
                if ($this->widthForStringUsingFontSize($wrappedLine . ' ' . $words[$i]
                                , $font
                                , $size) < $max_width) {
                    if (!empty($wrappedLine)) {
                        $wrappedLine .= ' ';
                    }
                    $wrappedLine .= $words[$i];
                } else {
                    $wrappedText .= $wrappedLine . "\n";
                    $wrappedLine = $words[$i];
                }
                $i++;
            }
            $wrappedText .= $wrappedLine . "\n";
        }
        return $wrappedText;
    }

    /**
     * found here, not sure of the author :
     * http://devzone.zend.com/article/2525-Zend_Pdf-tutorial#comments-2535
     */
    protected function widthForStringUsingFontSize($string, Zend_Pdf_Resource_Font $font, $fontSize) {
        $drawingString = iconv('UTF-8', 'UTF-16BE//IGNORE', $string);
        $characters = array();
        for ($i = 0; $i < strlen($drawingString); $i++) {
            $characters[] = (ord($drawingString[$i++]) << 8 ) | ord($drawingString[$i]);
        }
        $glyphs = $font->glyphNumbersForCharacters($characters);
        $widths = $font->widthsForGlyphs($glyphs);
        $stringWidth = (array_sum($widths) / $font->getUnitsPerEm()) * $fontSize;
        return $stringWidth;
    }

    public function export($templates) {

        $pdf = new Zend_Pdf();
        $this->_pdf = $pdf;
        foreach ($templates as $template) {
            $this->_acttemplate = $template;
            $this->_actvars['PAGE_COUNT'] = 1;

            $dataCount = count($template->getData());

            $this->_actparams = $template->getParams();
            $this->_actpdfsite = $pdf->newPage($template->getWidth(), $template->getHeight());
            if (isset($this->_submittals[0]) && intval($this->_actvars['PAGE_COUNT']) == 1 && file_exists($this->_submittals[0])) {
                $image = Zend_Pdf_Image::imageWithPath($this->_submittals[0]);

                $install = Zend_Registry::get("install");

                if(false && $install['id'] == 7) {
                    $this->_actpdfsite->drawImage($image, 0, 0, $template->getHeight()*($image->getPixelWidth()/$image->getPixelHeight()), $template->getHeight());
                }else{
                    $this->_actpdfsite->drawImage($image, $this->_acttemplate->getBackroundX(), $this->_acttemplate->getBackroundY(), $template->getWidth(), $template->getHeight());
                }


            } elseif (isset($this->_submittals[1]) && intval($this->_actvars['PAGE_COUNT']) > 1 && file_exists($this->_submittals[1])) {
                $image = Zend_Pdf_Image::imageWithPath($this->_submittals[1]);
                $this->_actpdfsite->drawImage($image, 0, 0, $template->getWidth(), $template->getHeight());
            }
            $this->_actheight = $template->getHeight() - $this->_acttemplate->getTopMargin();

            $this->renderSection($template->getTitle());
            if($this->_actvars['PAGE_COUNT'] == 1) {
                $this->renderSection($template->getPageHeader());
            }
            $this->renderSection($template->getColumnHeader());

            $newSite = true;

            foreach ($template->getData() as $key => $data) {
                $this->_data = $data;
                $nextElementHeight = $this->getNextHeight($template->getDetail());
 
                if($this->_actvars['PAGE_COUNT'] == 1) {

                    $height = $this->_actheight - $this->_acttemplate->getColumnFooterHeight(0) - $this->_acttemplate->getLastPageFooterHeight(0);

                    if($height >= $nextElementHeight && $this->_nextDataBreak == false) {
                        $this->renderSection($template->getDetail());
                    }else{

                        $height = $this->_actheight - $this->_acttemplate->getColumnFooterHeight(0)-$this->_acttemplate->getPageFooterHeight(0);

                        if($height >= $nextElementHeight && $this->_nextDataBreak == false) {
                            $this->renderSection($template->getDetail());
                        }else{
                            
                                $this->_actvars['PAGE_COUNT']++;
                                $this->renderSection($template->getColumnFooter());
                                $this->renderSection($template->getPageFooter());
                                $pdf->pages[] = clone($this->_actpdfsite);

                        }

                    }
                    
                }
                if($this->_actvars['PAGE_COUNT'] > 1) {

                    if($newSite) {

                        $this->_actpdfsite = $pdf->newPage($template->getWidth(), $template->getHeight());

                        if (isset($this->_submittals[0]) && intval($this->_actvars['PAGE_COUNT']) == 1 && file_exists($this->_submittals[0])) {
                            $image = Zend_Pdf_Image::imageWithPath($this->_submittals[0]);

                            $this->_actpdfsite->drawImage($image, 0, 0, $template->getWidth(), $template->getHeight());
                        } elseif (isset($this->_submittals[1]) && intval($this->_actvars['PAGE_COUNT']) > 1 && file_exists($this->_submittals[1])) {
                            $image = Zend_Pdf_Image::imageWithPath($this->_submittals[1]);

                            $this->_actpdfsite->drawImage($image, 0, 0, $template->getWidth(), $template->getHeight());
                        }

                        $this->_actheight = $template->getHeight() - $this->_acttemplate->getTopMargin();

                        $this->renderSection($template->getTitle());

                        $this->renderSection($template->getColumnHeader());

                        $newSite = false;
                        
                        $this->renderSection($template->getDetail());
                        
                    }else{
                    
                        $height = $this->_actheight - $this->_acttemplate->getColumnFooterHeight(0) - $this->_acttemplate->getLastPageFooterHeight(0)-$this->_acttemplate->getSummaryHeight(0);

                        if($height >= $nextElementHeight && $this->_nextDataBreak == false) {
                            $this->renderSection($template->getDetail());
                        }else{

                            $height = $this->_actheight - $this->_acttemplate->getColumnFooterHeight(0)-$this->_acttemplate->getPageFooterHeight(0);

                            if($height >= $nextElementHeight && $this->_nextDataBreak == false) {
                                $this->renderSection($template->getDetail());
                            }else{

                                    $this->_actvars['PAGE_COUNT']++;
                                    $this->renderSection($template->getColumnFooter());
                                    $this->renderSection($template->getPageFooter());
                                    $pdf->pages[] = clone($this->_actpdfsite);


                            $this->_actpdfsite = $pdf->newPage($template->getWidth(), $template->getHeight());

                            if (isset($this->_submittals[0]) && intval($this->_actvars['PAGE_COUNT']) == 1 && file_exists($this->_submittals[0])) {
                                $image = Zend_Pdf_Image::imageWithPath($this->_submittals[0]);

                                $this->_actpdfsite->drawImage($image, 0, 0, $template->getWidth(), $template->getHeight());
                            } elseif (isset($this->_submittals[1]) && intval($this->_actvars['PAGE_COUNT']) > 1 && file_exists($this->_submittals[1])) {
                                $image = Zend_Pdf_Image::imageWithPath($this->_submittals[1]);

                                $this->_actpdfsite->drawImage($image, 0, 0, $template->getWidth(), $template->getHeight());
                            }

                            $this->_actheight = $template->getHeight() - $this->_acttemplate->getTopMargin();

                            $this->renderSection($template->getTitle());

                            $this->renderSection($template->getColumnHeader());

                            $newSite = false;

                            $this->renderSection($template->getDetail());


                            }

                        }
                    }
                    
                }
                
            }

            if($this->_actvars['PAGE_COUNT'] == 1) {

                $height = $this->_acttemplate->getColumnFooterHeight(0)+$this->_acttemplate->getLastPageFooterHeight(0)+$this->_acttemplate->getSummaryHeight(0);

                if($this->_actheight >= $height) {
                    $this->renderSection($template->getColumnFooter());

                    $this->renderSection($template->getLastPageFooter());

                    $this->renderSection($template->getSummary(), true, $template);

                    $this->_actvars['PAGE_COUNT']++;
                    $pdf->pages[] = clone($this->_actpdfsite);
                }else{

                    $height = $this->_acttemplate->getColumnFooterHeight(0)+$this->_acttemplate->getLastPageFooterHeight(0)+$this->_acttemplate->getPageFooterHeight(0);

                    if($this->_actheight >= $height) {

                        $this->renderSection($template->getColumnFooter());

                        $this->renderSection($template->getLastPageFooter());

                        $this->_actvars['PAGE_COUNT']++;
                        $pdf->pages[] = clone($this->_actpdfsite);


                    }else{

                            $this->renderSection($template->getColumnFooter());

                            $this->_actvars['PAGE_COUNT']++;
                            $pdf->pages[] = clone($this->_actpdfsite);


                            $this->_actpdfsite = $pdf->newPage($template->getWidth(), $template->getHeight());

                            if (isset($this->_submittals[0]) && intval($this->_actvars['PAGE_COUNT']) == 1 && file_exists($this->_submittals[0])) {
                                $image = Zend_Pdf_Image::imageWithPath($this->_submittals[0]);

                                $this->_actpdfsite->drawImage($image, 0, 0, $template->getWidth(), $template->getHeight());
                            } elseif (isset($this->_submittals[1]) && intval($this->_actvars['PAGE_COUNT']) > 1 && file_exists($this->_submittals[1])) {
                                $image = Zend_Pdf_Image::imageWithPath($this->_submittals[1]);

                                $this->_actpdfsite->drawImage($image, 0, 0, $template->getWidth(), $template->getHeight());
                            }

                            $this->_actheight = $template->getHeight() - $this->_acttemplate->getTopMargin();

                            //$this->renderSection($template->getTitle());

                            //$this->renderSection($template->getColumnHeader());

                            $this->renderSection($template->getLastPageFooter());

                            $this->renderSection($template->getSummary(), true, $template);

                            $pdf->pages[] = clone($this->_actpdfsite);

                    }


                }

            }else{

                $height = $this->_acttemplate->getColumnFooterHeight(0)+$this->_acttemplate->getLastPageFooterHeight(0)+$this->_acttemplate->getSummaryHeight(0);

                if($this->_actheight >= $height) {
                    $this->renderSection($template->getColumnFooter());

                    $this->renderSection($template->getLastPageFooter());

                    $this->renderSection($template->getSummary(), true, $template);

                    $this->_actvars['PAGE_COUNT']++;
                    $pdf->pages[] = clone($this->_actpdfsite);
                }else{

                    $height = $this->_acttemplate->getColumnFooterHeight(0)+$this->_acttemplate->getLastPageFooterHeight(0)+$this->_acttemplate->getPageFooterHeight(0);

                    if($this->_actheight >= $height) {
                            $this->renderSection($template->getColumnFooter());

                            $this->renderSection($template->getLastPageFooter());

                            $this->_actvars['PAGE_COUNT']++;
                            $pdf->pages[] = clone($this->_actpdfsite);
                    }else{

                            $this->renderSection($template->getColumnFooter());

                            $this->_actvars['PAGE_COUNT']++;
                            $pdf->pages[] = clone($this->_actpdfsite);


                            $this->_actpdfsite = $pdf->newPage($template->getWidth(), $template->getHeight());

                            if (isset($this->_submittals[0]) && intval($this->_actvars['PAGE_COUNT']) == 1 && file_exists($this->_submittals[0])) {
                                $image = Zend_Pdf_Image::imageWithPath($this->_submittals[0]);

                                $this->_actpdfsite->drawImage($image, 0, 0, $template->getWidth(), $template->getHeight());
                            } elseif (isset($this->_submittals[1]) && intval($this->_actvars['PAGE_COUNT']) > 1 && file_exists($this->_submittals[1])) {
                                $image = Zend_Pdf_Image::imageWithPath($this->_submittals[1]);

                                $this->_actpdfsite->drawImage($image, 0, 0, $template->getWidth(), $template->getHeight());
                            }

                            $this->_actheight = $template->getHeight() - $this->_acttemplate->getTopMargin();

                            $this->renderSection($template->getTitle());

                            $this->renderSection($template->getColumnHeader());

                            $this->renderSection($template->getLastPageFooter());

                            $this->renderSection($template->getSummary(), true, $template);

                            $pdf->pages[] = $this->_actpdfsite;

                    }


                }

            }

            if($this->_agb != false && file_exists($this->_agb)) {

                $this->_actpdfsite = $pdf->newPage($template->getWidth(), $template->getHeight());
                $image = Zend_Pdf_Image::imageWithPath($this->_agb);

                $this->_actpdfsite->drawImage($image, 0, 0, $template->getWidth(), $template->getHeight());

                $pdf->pages[] = $this->_actpdfsite;
            }

        }
        try {

            if($this->_renderbarcode) {
                $barcodeOptions = array('factor' => 1.5, 'text' => $this->_renderbarcode['text'], 'font' => APPLICATION_PATH . "/fonts/verdana.ttf");
                $rendererOptions = array('topOffset' => $this->_renderbarcode['top'], 'leftOffset' => $this->_renderbarcode['left']);
                $pdfWithBarcode = Zend_Barcode::factory('code39', 'pdf',
                    $barcodeOptions, $rendererOptions)->setResource($pdf, 0)->draw();
            }
            if($this->_renderbarcodePost) {
                /*$barcodeOptions = array('factor' => 1.45, 'text' => $this->_renderbarcodePost['text'], 'font' => APPLICATION_PATH . "/fonts/verdana.ttf", 'barHeight' => 26, 'drawtext' => false);
                $rendererOptions = array('topOffset' => $this->_renderbarcodePost['top'], 'leftOffset' => $this->_renderbarcodePost['left']);
                $pdfWithBarcode = Zend_Barcode::factory('code128', 'pdf',
                    $barcodeOptions, $rendererOptions)->setResource($pdf, 0)->draw();*/
            }

            $pdf->save($this->_file);

        } catch (Exception $e) {
            Zend_Registry::get('log')->debug(PUBLIC_PATH . "/layouter/fonts/verdana.ttf");
            Zend_Registry::get('log')->debug($e->getMessage());
            Zend_Registry::get('log')->debug($e->getTraceAsString());
        }
    }

    protected function getFont($elm) {

        if($elm->getFontFile() != "") {
            $fontPath = Zend_Registry::get('shop_path') . "/reports/" . $elm->getFontFile();
            if(!file_exists($fontPath)) {
                $elm->setFontFile("");
            }else{
                return Zend_Pdf_Font::fontWithPath($fontPath, Zend_Pdf_Font::EMBED_SUPPRESS_EMBED_EXCEPTION);
            }
        }

        if ($elm->getBold() == true && $elm->getItalic() == true) {
            $font = Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_HELVETICA_BOLD_ITALIC);
        } elseif ($elm->getBold() == true) {
            $font = Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_HELVETICA_BOLD);
        } elseif ($elm->getItalic() == true) {
            $font = Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_HELVETICA_ITALIC);
        } else {
            $font = Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_HELVETICA);
        }

        return $font;
    }

    protected function getNextHeight($elements, $summary = false, $template = null) {
        $P = $this->_actparams;
        $V = $this->_actvars;
        $F = null;

        $actheight = 0;


        if ($this->_data != null)
            $F = $this->_data;


        foreach ($elements as $element) {
            $linesHeight = 0;
            foreach ($element->getElements() as $elm) {

                if (!$elm->isPrintable($P, $V, $F))
                    continue;

                $lineht = 0;
                switch (get_class($elm)) {

                    case 'TP_Rdl_Element_Statictext':
                        break;
                    case 'TP_Rdl_Element_Line':
                        break;
                    case 'TP_Rdl_Element_Textfield':
                        $font = $this->getFont($elm);

                        eval('$exp = ' . $elm->getExpression() . ';');
                        if ($elm->getClass() == 'java.util.Date') {
                            if($exp == '0000-00-00' || $exp == '0000-00-00 00:00:00') {
                                $exp = '';
                            }else{
                                $exp = new Zend_Date(Zend_Locale_Format::getDate($exp,
                                                    array('date_format' => 'yyyy-MM-dd hh:mm:ss')));
                                $exp = $exp->get(Zend_Date::DATE_MEDIUM);
                            }
                        }
                        if ($elm->getClass() == 'java.util.DateLong') {
                            if($exp == '0000-00-00' || $exp == '0000-00-00 00:00:00') {
                                $exp = '';
                            }else{
                                $exp = new Zend_Date(Zend_Locale_Format::getDate($exp,
                                    array('date_format' => 'yyyy-MM-dd hh:mm:ss')));
                                $exp = $exp->get(Zend_Date::DATETIME_SHORT);
                            }
                        }

                        $lines = explode("\n", $this->getWrappedText($exp, $font, $elm->getFontSize(), $elm->getWidth()));
                        $y = $this->_actheight - $elm->getTop() - $elm->getHeight();
                        $lineht = 0 - $elm->getTop() - $elm->getHeight();

                        foreach ($lines as $line) {
                            $y-=$elm->getFontSize() + 4;
                            $lineht = $lineht + $elm->getFontSize() + 4;
                        }

                        break;
                    case 'TP_Rdl_Element_Image':

                        
                        break;
                }

                if ($lineht > $linesHeight)
                    $linesHeight = $lineht;
            }

            $actheight += $element->getHeight();
            $actheight += $linesHeight;
        }

        return $actheight;
    }

    protected function renderSection($elements, $summary = false, $template = null) {
        $P = $this->_actparams;
        $V = $this->_actvars;
        $V['PAGE_NUMBER'] = $V['PAGE_COUNT'];
        $additionalHeigh = 0;
        $F = null;
        if ($this->_data != null)
            $F = $this->_data;

        
        foreach ($elements as $element) {
            $linesHeight = 0;
            foreach ($element->getElements() as $elm) {
      
                if (!$elm->isPrintable($P, $V, $F))
                    continue;

                $lineht = 0;
                switch (get_class($elm)) {

                    case 'TP_Rdl_Element_Statictext':
                        $this->_actpdfsite->setFillColor(Zend_Pdf_Color_Html::color($elm->getColor()));
                        $this->_actpdfsite->setFont($this->getFont($elm), $elm->getFontSize());
                        
                        if ($summary == true) {
                            $this->_actpdfsite->drawText($elm->getText(), $this->_acttemplate->getLeftMargin() + $elm->getLeft(), $this->_acttemplate->getButtomMargin() + ($element->getHeight() - $elm->getTop() - $elm->getHeight()) - $additionalHeigh, 'UTF-8');
                        } else {
                            $this->_actpdfsite->drawText($elm->getText(), $this->_acttemplate->getLeftMargin() + $elm->getLeft(), $this->_actheight - $elm->getTop() - $elm->getHeight() - $additionalHeigh, 'UTF-8');
                        }

                        break;
                    case 'TP_Rdl_Element_Break':
                        $this->_nextDataBreak = true;
                        break;
                    case 'TP_Rdl_Element_Table':
                        
                        eval('$exp = ' . $elm->getExpression() . ';');
                        
                        $height = 0;
                        $cell1 = $elm->getColumn(0);
                        $cell2 = $elm->getColumn(1);

                        foreach($exp as $key => $wert) {
                            
                            eval('$exp1 = ' . $cell1->getExpression() . ';');
                            $font = $this->getFont($cell1);
                            $this->_actpdfsite->setFillColor(Zend_Pdf_Color_Html::color($cell1->getColor()));
                            $this->_actpdfsite->setFont($font, $cell1->getFontSize());

                            if($elm->getMoveNext()) {
                                $y = $this->_actheight - $elm->getTop() - $cell1->getHeight() - $additionalHeigh;
                            }else{
                                $y = $this->_actheight - $elm->getTop() - $cell1->getHeight() - $height;
                            }

                            $x = $this->_acttemplate->getLeftMargin() + $elm->getLeft();
                            if($cell1->getAlign() == 'Right') {
                                $text_width = $this->getTextWidth($exp1, $font, $cell1->getFontSize());
			                    $x = $x + ($cell1->getWidth() - $text_width);
                            }

                            $this->_actpdfsite->drawText($exp1, $x, $y, 'UTF-8');
                            if($cell2) {
                                eval('$exp2 = ' . $cell2->getExpression() . ';');
                                $font = $this->getFont($cell2);
                                $this->_actpdfsite->setFillColor(Zend_Pdf_Color_Html::color($cell2->getColor()));
                                $this->_actpdfsite->setFont($font, $cell2->getFontSize());
                                if($elm->getMoveNext()) {
                                    $y = $this->_actheight - $elm->getTop() - $elm->getHeight() - $additionalHeigh;
                                }else{
                                    $y = $this->_actheight - $elm->getTop() - $elm->getHeight() - $height;
                                }

                                $x = $this->_acttemplate->getLeftMargin() + $elm->getLeft() + $cell1->getWidth();
                                if($cell2->getAlign() == 'Right') {
                                    $text_width = $this->getTextWidth($exp2, $font, $cell2->getFontSize());
                                    $x = $x + ($cell2->getWidth() - $text_width);
                                }

                                $this->_actpdfsite->drawText($exp2, $x, $y, 'UTF-8');
                            }
                            if($elm->getMoveNext()) {
                                $additionalHeigh = $additionalHeigh + $elm->getHeight();
                            }else{
                                $height += $elm->getHeight();
                            }
                        }

                        if($elm->getMoveNext()) {
                            $additionalHeigh = $additionalHeigh - $elm->getHeight();
                        }

                        break;
                    case 'TP_Rdl_Element_Line':

                        $this->_actpdfsite->setFillColor(Zend_Pdf_Color_Html::color($elm->getColor()));
                        $this->_actpdfsite->setLineColor(Zend_Pdf_Color_Html::color($elm->getColor()));
                        $this->_actpdfsite->setLineWidth($elm->getSize());
                        if ($summary == true) {
                            $this->_actpdfsite->drawLine($this->_acttemplate->getLeftMargin() + $elm->getLeft(),
                                    $this->_acttemplate->getButtomMargin() + $elm->getTop() + $elm->getHeight() - $additionalHeigh,
                                    $this->_acttemplate->getLeftMargin() + $elm->getLeft() + $elm->getWidth(),
                                    $this->_acttemplate->getButtomMargin() + $elm->getTop() + $elm->getHeight() - $additionalHeigh);
                        } else {
                            $this->_actpdfsite->drawLine($this->_acttemplate->getLeftMargin() + $elm->getLeft(), $this->_actheight - $elm->getTop() - $elm->getHeight() - $additionalHeigh, $this->_acttemplate->getLeftMargin() + $elm->getLeft() + $elm->getWidth(), $this->_actheight - $elm->getTop() - $elm->getHeight() - $additionalHeigh);
                        }

                        break;
                    case 'TP_Rdl_Element_Rectangle':
                        $this->_actpdfsite->setFillColor(Zend_Pdf_Color_Html::color($elm->getBackColor()));
                        $this->_actpdfsite->setLineColor(Zend_Pdf_Color_Html::color($elm->getColor()));
                        $this->_actpdfsite->setLineWidth($elm->getSize());
                        if ($summary == true) {
                            $this->_actpdfsite->drawRectangle($this->_acttemplate->getLeftMargin() + $elm->getLeft(),
                                    $this->_acttemplate->getButtomMargin() + $elm->getTop() + $elm->getHeight() - $additionalHeigh,
                                    $this->_acttemplate->getLeftMargin() + $elm->getLeft() + $elm->getWidth(),
                                    $this->_acttemplate->getButtomMargin() + $elm->getTop() - $elm->getHeight() - $additionalHeigh);
                        } else {
                            if($elm->getRadius() != 0) {
                                $this->_actpdfsite->drawRoundedRectangle($this->_acttemplate->getLeftMargin() + $elm->getLeft(),
                                    $this->_actheight - $elm->getTop() - $elm->getHeight() - $additionalHeigh,
                                    $this->_acttemplate->getLeftMargin() + $elm->getLeft() + $elm->getWidth(),
                                    $this->_actheight - $elm->getTop() - $additionalHeigh,
                                    $elm->getRadius());
                            }else{
                                $this->_actpdfsite->drawRectangle($this->_acttemplate->getLeftMargin() + $elm->getLeft(),
                                    $this->_actheight - $elm->getTop() - $elm->getHeight() - $additionalHeigh,
                                    $this->_acttemplate->getLeftMargin() + $elm->getLeft() + $elm->getWidth(),
                                    $this->_actheight - $elm->getTop() - $additionalHeigh);
                            }
                        }

                        break;
                    case 'TP_Rdl_Element_Textfield':
                        $font = $this->getFont($elm);
                        $this->_actpdfsite->setFillColor(Zend_Pdf_Color_Html::color($elm->getColor()));
                        $this->_actpdfsite->setFont($font, $elm->getFontSize());
                        eval('$exp = ' . $elm->getExpression() . ';');
                        if ($elm->getClass() == 'java.util.Date') {
                            if($exp == '0000-00-00' || $exp == '0000-00-00 00:00:00') {
                                $exp = '';
                            }else{
                                $exp = new Zend_Date(Zend_Locale_Format::getDate($exp,
                                                        array('date_format' => 'yyyy-MM-dd hh:mm:ss')));
                                $exp = $exp->get(Zend_Date::DATE_MEDIUM);
                            }
                        }
                        if ($elm->getClass() == 'java.util.DateLong') {
                            if($exp == '0000-00-00' || $exp == '0000-00-00 00:00:00') {
                                $exp = '';
                            }else{
                                $exp = new Zend_Date(Zend_Locale_Format::getDate($exp,
                                    array('date_format' => 'yyyy-MM-dd hh:mm:ss')));
                                $exp = $exp->get(Zend_Date::DATETIME_SHORT);
                            }
                        }
                        if($elm->getKey() == 'barcodePostKey') {
                            $exp = substr($exp, 0, 2) . ' ' . substr($exp, 2, 2)  . ' ' . substr($exp, 4, 3)  . ' ' . substr($exp, 7, 3)  . ' ' . substr($exp, 10, 3);
                        }

                        $lines = explode("\n", $this->getWrappedText($exp, $font, $elm->getFontSize(), $elm->getWidth()));
                        $y = $this->_actheight - $elm->getTop() - $elm->getHeight() - $additionalHeigh;
                        $lineht = 0 - $elm->getTop() - $elm->getHeight();
                        $addHeight = $y;
                        foreach ($lines as $line) {
                            if($line == "") {
                                continue;
                            }
                            $x = $this->_acttemplate->getLeftMargin() + $elm->getLeft();
                            if($elm->getAlign() == 'Right') {
                                $text_width = $this->getTextWidth(str_replace(array("ü","Ü","ö","Ö","ä","Ä","²","Â²"), array("u","U","o","O","a","A","2",""), $line), $font, $elm->getFontSize());
			                    $x = $x + ($elm->getWidth() - $text_width);
                            }

                            if ($summary == true) {
                                $this->_actpdfsite->drawText($line, $x, $this->_acttemplate->getButtomMargin() + ($element->getHeight() - $elm->getTop() - $elm->getHeight()) - $additionalHeigh, 'UTF-8');
                            } else {
                                $this->_actpdfsite->drawText($line, $x, $y, 'UTF-8');
                            }
                            $y-=$elm->getFontSize() + 4;
                            $lineht = $lineht + $elm->getFontSize() + 4;
                            
                        }

                        if($elm->getMoveNext() && count($lines) > 2) {
                        	$additionalHeigh = $additionalHeigh +  (count($lines) - 2) * ($elm->getFontSize() + 2);
                        }

                        break;
                    case 'TP_Rdl_Element_Image':


                        if ($elm->getKey() == 'barcode') {
                            eval('$exp = ' . $elm->getExpression() . ';');
                            TP_QRCode::generate($exp, null, null, PUBLIC_PATH . '/barcodes/' . $exp . '.png');
                            $file = PUBLIC_PATH . '/barcodes/' . $exp . '.png';

                        }elseif($elm->getKey() == 'barcode128') {
                            eval('$exp = ' . $elm->getExpression() . ';');
                            // Nur der zu zeichnende Text wird ben�tigt

                            $this->_renderbarcode = array('text' => $exp, 'left' => $this->_acttemplate->getLeftMargin() + $elm->getLeft(), 'top' => $elm->getTop());


                        }elseif($elm->getKey() == 'barcodePost') {
                            eval('$exp = ' . $elm->getExpression() . ';');
                            // Nur der zu zeichnende Text wird ben�tigt

                            $this->_renderbarcodePost = array('text' => $exp, 'left' => $this->_acttemplate->getLeftMargin() + $elm->getLeft(), 'top' => $elm->getTop());


                        }elseif($elm->getKey() == 'product_image') {
                            if($F['org_article']->file != "") {

                                $image = Doctrine_Query::create()
                                        ->from('Image as i')
                                        ->where('i.id = ?')->fetchOne(array($F['org_article']->file));

                                if($image && file_exists(PUBLIC_PATH.'/'.$image->path)) {
                                    if(strpos($image->path, "pdf")) {
                                        $image = new gmagick(PUBLIC_PATH.'/'.$image->path."[0]");
                                        $image->thumbnailimage(200, 200, true);
                                        $image->writeimage(PUBLIC_PATH . '/temp/'.md5($image->path).".png");
                                        $file = PUBLIC_PATH .'/temp/'.md5($image->path).".png";
                                    }else{
                                        $image = new gmagick(PUBLIC_PATH.'/'.$image->path);

                                        $image->thumbnailimage(200, 200, true);

                                        $image->writeimage(PUBLIC_PATH . '/temp/'.md5($image->path).".png");
                                        $file = PUBLIC_PATH .'/temp/'.md5($image->path).".png";

                                    }
                                }elseif($image){
                                    $file = PUBLIC_PATH .'/temp/'.md5($image->path).".png";
                                }

                            }elseif($F['org_article']->file1 != "") {
                                $image = Doctrine_Query::create()
                                        ->from('Image as i')
                                        ->where('i.id = ?')->fetchOne(array($F['org_article']->file1));
                                if($image) {
                                    $file = PUBLIC_PATH .'/'.$image->path;
                                }
                            }elseif(TP_Templateprint::isTemplateExists($F['org_article'])) {


                                $file = TP_Templateprint::generatePreview($F['org_article']);


                            }elseif($F['org_article']->a6_org_article != 0) {
                                $orgarticle = Doctrine_Query::create()
                                    ->from('Article as i')
                                    ->where('i.id = ?')->fetchOne(array($F['org_article']->a6_org_article));
                                if($orgarticle->file != "") {
                                    $image = Doctrine_Query::create()
                                        ->from('Image as i')
                                        ->where('i.id = ?')->fetchOne(array($orgarticle->file));
                                    if($image) {
                                        $file = PUBLIC_PATH .'/'.$image->path;
                                    }
                                }elseif($orgarticle->file1 != "") {
                                    $image = Doctrine_Query::create()
                                        ->from('Image as i')
                                        ->where('i.id = ?')->fetchOne(array($orgarticle->file1));
                                    if($image) {
                                        $file = PUBLIC_PATH .'/'.$image->path;
                                    }
                                }else{
                                    $file = false;
                                }

                            }else{
                                $file = false;
                            }

                        }else{
                            $file = $this->_acttemplate->getReportPath() . '/' . $elm->getSource();
                        }
                        if ($file != false && file_exists($file) && $elm->getKey() != 'barcode128') {

                            if($elm->getHeight() == 0) {
                                $im = new imagick($file);

                                $sizes = $im->getImageGeometry();
                                $widthScale = $sizes['width'] / $elm->getWidth();
                                $elm->setHeight(round($sizes['height']/$widthScale));

                            }

                            if($elm->getWidth() == 0) {
                                $im = new imagick($file);

                                $sizes = $im->getImageGeometry();
                                $widthScale = $sizes['height'] / $elm->getHeight();
                                $elm->setWidth(round($sizes['width']/$widthScale));

                            }

                            if ($summary == true) {
                                $this->_actpdfsite->drawImage(Zend_Pdf_Image::imageWithPath($file),
                                        $this->_acttemplate->getLeftMargin() + $elm->getLeft(),
                                        $this->_acttemplate->getButtomMargin() + ($element->getHeight() - $elm->getTop() - $elm->getHeight()),
                                        $this->_acttemplate->getLeftMargin() + $elm->getLeft() + $elm->getWidth(),
                                        $this->_acttemplate->getButtomMargin() + ($element->getHeight() - $elm->getTop()));
                            } else {
                                Zend_Registry::get("log")->debug($file);
                                $this->_actpdfsite->drawImage(Zend_Pdf_Image::imageWithPath($file),
                                        $this->_acttemplate->getLeftMargin() + $elm->getLeft(),
                                        $this->_actheight - $elm->getTop() - $elm->getHeight(),
                                        $this->_acttemplate->getLeftMargin() + $elm->getLeft() + $elm->getWidth(),
                                        $this->_actheight - $elm->getTop());
                            }
                        }
                        break;
                }

                if ($lineht > $linesHeight)
                    $linesHeight = $lineht;
            }

            $this->_actheight -= $element->getHeight();
            $this->_actheight -= ( $linesHeight + $additionalHeigh );
        }
    }

    /**
	 * Return length of generated string in points
	 *
	 * @param string $string
	 * @param Zend_Pdf_Resource_Font $font
	 * @param int $font_size
	 * @return double
	 */
	public function getTextWidth($text, Zend_Pdf_Resource_Font $font, $font_size)
	{
		$drawing_text = @iconv('', 'UTF-8//IGNORE', $text);
		$characters    = array();
		for ($i = 0; $i < strlen($drawing_text); $i++) {
			$characters[] = (ord($drawing_text[$i++]) << 8) | ord ($drawing_text[$i]);
		}
		$glyphs        = $font->glyphNumbersForCharacters($characters);
		$widths        = $font->widthsForGlyphs($glyphs);
		$text_width   = (array_sum($widths) / $font->getUnitsPerEm()) * $font_size;
		return $text_width;
	}

    protected function translate($var) {
        $translate = Zend_Registry::get('Zend_Translate');
        $translation = Zend_Registry::get('layout_path') . '/locale';
        $translate->addTranslation($translation, Zend_Registry::get('locale'));
        Zend_Registry::get('log')->debug($var);
        return $translate->translate($var);
    }

}
