<?php

class TP_Rdl_Template {


    protected $_pageHeight = 0;

    protected $_pageWidth = 0;

    protected $_topMargin = 0;

    protected $_buttomMargin = 0;

    protected $_leftMargin = 0;

    protected $_rightMargin = 0;

    protected $_backroundX = 0;
    protected $_backroundY = 0;
    protected $_backroundWidth = 0;
    protected $_backroundHeight = 0;

    protected $_title;

    protected $_pageHeader;

    protected $_columnHeader;

    protected $_detail;

    protected $_columnFooter;

    protected $_pageFooter;

    protected $_lastPageFooter;

    protected $_summary;

    protected $_params = array();

    protected $_data = array();

    protected $_file = '';

    function __construct($file, $params, $data) {

        try{
            $this->_file = $file;
            $file = new SimpleXMLElement($file, LIBXML_NOCDATA, true);
        }catch(Exception $e) {
            
        }

        $this->_params = $params;
        $this->_data = $data;

        $this->_pageHeight = (int)$file['pageHeight'];
        $this->_pageWidth = (int)$file['pageWidth'];
        $this->_topMargin = (int)$file['topMargin'];
        $this->_buttomMargin = (int)$file['buttomMargin'];
        $this->_leftMargin = (int)$file['leftMargin'];
        if(isset($file['backroundX'])) {
            $this->_backroundX = (float)$file['backroundX'];
        }
        if(isset($file['backroundY'])) {
            $this->_backroundY = (float)$file['backroundY'];
        }
        if(isset($file['backroundWidth'])) {
            $this->_backroundWidth = (float)$file['backroundWidth'];
        }
        if(isset($file['backroundHeight'])) {
            $this->_backroundHeight = (float)$file['backroundHeight'];
        }

        foreach($file->title->children() as $elements) {
            $band = new TP_Rdl_Band( (int)$elements['height']);
            $band->loadElementsXml($elements->children());

            $this->_title[] = $band;
            
        }

        foreach($file->pageHeader->children() as $elements) {
            $band = new TP_Rdl_Band( (int)$elements['height']);
            $band->loadElementsXml($elements->children());
            $this->_pageHeader[] = $band;
        }

        foreach($file->columnHeader->children() as $elements) {
            $band = new TP_Rdl_Band( (int)$elements['height']);
            $band->loadElementsXml($elements->children());
            $this->_columnHeader[] = $band;
        }

        foreach($file->detail->children() as $elements) {
            $band = new TP_Rdl_Band( (int)$elements['height']);
            $band->loadElementsXml($elements->children());
            $this->_detail[] = $band;
        }

        foreach($file->columnFooter->children() as $elements) {
            $band = new TP_Rdl_Band( (int)$elements['height']);
            $band->loadElementsXml($elements->children());
            $this->_columnFooter[] = $band;
        }

        foreach($file->pageFooter->children() as $elements) {
            $band = new TP_Rdl_Band( (int)$elements['height']);
            $band->loadElementsXml($elements->children());
            $this->_pageFooter[] = $band;
        }

        foreach($file->lastPageFooter->children() as $elements) {
            $band = new TP_Rdl_Band( (int)$elements['height']);
            $band->loadElementsXml($elements->children());
            $this->_lastPageFooter[] = $band;
        }

        foreach($file->summary->children() as $elements) {
            $band = new TP_Rdl_Band( (int)$elements['height']);
            $band->loadElementsXml($elements->children());
            $this->_summary[] = $band;
        }
    }

    public function getReportPath() {
        return realpath(dirname($this->_file));
    }

    public function getWidth() {
        return $this->_pageWidth;
    }

    public function getHeight() {
        return $this->_pageHeight;
    }

    public function getTitle()
    {
        return $this->_title;
    }

    public function getTitleHeight($index)
    {
        return $this->_title[$index]->getHeight();
    }

    public function getPageHeader()
    {
        return $this->_pageHeader;
    }

    public function getPageHeaderHeight($index)
    {
        return $this->_pageHeader[$index]->getHeight();
    }

    public function getColumnHeader()
    {
        return $this->_columnHeader;
    }

    public function getColumnHeaderHeight($index)
    {
        return $this->_columnHeader[$index]->getHeight();
    }

    public function getDetail()
    {
        return $this->_detail;
    }

    public function getDetailHeight($index)
    {
        return $this->_detail[$index]->getHeight();
    }

    public function getColumnFooter()
    {
        return $this->_columnFooter;
    }

    public function getColumnFooterHeight($index)
    {
        return $this->_columnFooter[$index]->getHeight();
    }

    public function getPageFooter()
    {
        return $this->_pageFooter;
    }

    public function getPageFooterHeight($index)
    {
        return $this->_pageFooter[$index]->getHeight();
    }

    public function getLastPageFooter()
    {
        return $this->_lastPageFooter;
    }

    public function getLastPageFooterHeight($index)
    {
        return $this->_lastPageFooter[$index]->getHeight();
    }

    public function getSummary()
    {
        return $this->_summary;
    }

    public function getSummaryHeight($index)
    {
        return $this->_summary[$index]->getHeight();
    }

    public function getParams() {
        return $this->_params;
    }

    public function getData() {
        return $this->_data;
    }

    public function getTopMargin() {
        return $this->_topMargin;
    }

    public function getLeftMargin() {
        return $this->_leftMargin;
    }

    public function getButtomMargin() {
        return $this->_buttomMargin;
    }

    public function getRightMargin() {
        return $this->_rightMargin;
    }

    /**
     * @return int
     */
    public function getBackroundHeight()
    {
        return $this->_backroundHeight;
    }

    /**
     * @param int $backroundHeight
     */
    public function setBackroundHeight($backroundHeight)
    {
        $this->_backroundHeight = $backroundHeight;
    }

    /**
     * @return int
     */
    public function getBackroundX()
    {
        return $this->_backroundX;
    }

    /**
     * @param int $backroundX
     */
    public function setBackroundX($backroundX)
    {
        $this->_backroundX = $backroundX;
    }

    /**
     * @return int
     */
    public function getBackroundY()
    {
        return $this->_backroundY;
    }

    /**
     * @param int $backroundY
     */
    public function setBackroundY($backroundY)
    {
        $this->_backroundY = $backroundY;
    }

    /**
     * @return int
     */
    public function getBackroundWidth()
    {
        return $this->_backroundWidth;
    }

    /**
     * @param int $backroundWidth
     */
    public function setBackroundWidth($backroundWidth)
    {
        $this->_backroundWidth = $backroundWidth;
    }

}