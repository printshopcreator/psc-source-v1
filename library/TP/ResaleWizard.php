<?php

class TP_ResaleWizard
{

    /**
     * Filter-Sessionnamespace
     *
     * @var Zend_Session_Namespace
     */
    private $_data;

    /**
     * Konstruiert das Model für die Suche
     *
     * @return void
     */
    public function __construct()
    {

        $this->_data = new Zend_Session_Namespace('Resale_Wiz');

        $this->_data->unlock();

        if (Zend_Session::namespaceIsset('Resale_Wiz') === false) {
            $this->initWizard();
        }
        //die(var_dump($this->_data->shop));
    }

    public function setStep1($name, $description, $keywords, $shoptheme1, $shoptheme2, $shoptheme3)
    {

        $install = Zend_Registry::get('install');

        $this->_data->shop->name = $name;
        $this->_data->shop->description = $description;
        $this->_data->shop->keywords = $keywords;
        $this->_data->shop->shoptheme1 = $shoptheme1;
        $this->_data->shop->shoptheme2 = $shoptheme2;
        $this->_data->shop->shoptheme3 = $shoptheme3;
        $this->_data->shop->created = false;
        $filter = new Zend_Filter_Alnum ( );
        $filter2 = new TP_Filter_Umlaut();
        $this->_data->shop->domain = $filter->filter($filter2->filter($name)) . $install ['marketdomain'];
    }

    public function setStep1Simple($name, $description, $keywords)
    {

        $install = Zend_Registry::get('install');

        $this->_data->shop->name = $name;
        $this->_data->shop->description = $description;
        $this->_data->shop->keywords = $keywords;
        $this->_data->shop->created = false;
        $filter = new Zend_Filter_Alnum ( );
        $this->_data->shop->domain = $filter->filter($name) . $install ['marketdomain'];
    }

    public function setArticleId($id)
    {
        $this->_data->shop->articleid = $id;
    }

    public function getArticleId()
    {
        return $this->_data->shop->articleid;
    }

    public function setLayouterId($id)
    {
        $this->_data->shop->layouterid = $id;
    }

    public function getLayouterId()
    {
        return $this->_data->shop->layouterid;
    }

    public function setStep2($design)
    {
        $this->_data->shop->design = $design;
    }

    public function setStep3($agb)
    {
        $this->_data->shop->agb = $agb;
    }

    public function setArticleStep5($agb)
    {
        $this->_data->shop->agb = $agb;
    }

    public function setArticleStep4($private)
    {
        $this->_data->shop->articleprivate = $private;
    }

    public function getResalePrice()
    {
        return $this->_data->shop->resale_price;
    }

    public function setResalePrice($var)
    {
        return $this->_data->shop->resale_price = $var;
    }

    public function setShopCreated($var)
    {
        $this->_data->shop->created = $var;
    }

    public function getShopName()
    {
        return $this->_data->shop->name;
    }

    public function getShopTheme1()
    {
        return $this->_data->shop->shoptheme1;
    }

    public function getShopTheme2()
    {
        return $this->_data->shop->shoptheme2;
    }

    public function getShopTheme3()
    {
        return $this->_data->shop->shoptheme3;
    }

    public function getArticleName()
    {
        return $this->_data->shop->articlename;
    }

    public function getShopDesign()
    {
        return $this->_data->shop->design;
    }

    public function setArticleShop($id)
    {
        $this->_data->unlock();
        $this->_data->shop->articleshop = $id;
    }

    public function setDisplayMarket($value)
    {
        $this->_data->shop->display_market = $id;
    }

    public function getAll()
    {
        return $this->_data->shop;
    }

    public function getDisplayMarket()
    {
        return $this->_data->shop->display_market;
    }

    public function getArticleShop()
    {
        return $this->_data->shop->articleshop;
    }

    public function getArticleResale()
    {
        return $this->_data->shop->resale;
    }

    public function getShopCreated()
    {
        return $this->_data->shop->created;
    }

    public function getShopDescription()
    {
        return $this->_data->shop->description;
    }

    public function getArticleDescription()
    {
        return $this->_data->shop->articledescription;
    }

    public function getArticleEinleitung()
    {
        return $this->_data->shop->articleeinleitung;
    }

    public function setArticleStep1($name, $description, $einleitung, $shop)
    {
        $this->_data->shop->articlename = $name;
        $this->_data->shop->articledescription = $description;
        $this->_data->shop->articleeinleitung = $einleitung;
        $this->_data->shop->articleshop = $shop;
    }

    public function setArticleStep2($articlegroup1, $articlegroup2, $articlegroup3, $articletheme1, $articletheme2, $articletheme3)
    {

        $this->_data->shop->articlegroup1 = $articlegroup1;
        $this->_data->shop->articlegroup2 = $articlegroup2;
        $this->_data->shop->articlegroup3 = $articlegroup3;

        $this->_data->shop->articletheme1 = $articletheme1;
        $this->_data->shop->articletheme2 = $articletheme2;
        $this->_data->shop->articletheme3 = $articletheme3;
    }

    public function setArticleStep3($display_market, $articlethememarket1, $articlethememarket2, $articlethememarket3)
    {

        $this->_data->shop->display_market = $display_market;

        $this->_data->shop->articlethememarket1 = $articlethememarket1;
        $this->_data->shop->articlethememarket2 = $articlethememarket2;
        $this->_data->shop->articlethememarket3 = $articlethememarket3;
    }

    public function getResaleMotivUri()
    {
        return $this->_data->shop->resaleMotiv;
    }

    public function getArticle()
    {
        return Doctrine_Query::create()->from('Article c')->where('c.uuid = ?', array($this->getArticleId()))->fetchOne();
    }

    public function createArticle()
    {

        $this->_data->unlock();

        $shop = Doctrine_Query::create()->from('Shop c')->where('c.id = ?', array($this->getArticleShop()))->fetchOne();
        Zend_Registry::set('target_shop', $shop);
        $this->_data->shop->resaleMotiv = 'http://' . $shop->Domain [0]->name;

        $article = Doctrine_Query::create()->from('Article c')->where('c.uuid = ?', array($this->getArticleId()))->fetchOne();
        $user = Zend_Auth::getInstance()->getIdentity();
        //ToDo: So lassen?
        if (true || $user ['id'] != $article->contact_id || $this->getLayouterId() != "") {

            $orgid = $article->id;

            $article = $article->copy();
            $article->uuid = "";
            $article->contact_id = $user ['id'];
            $article->private = true;
            $article->a6_org_article = $orgid;
            $article->save();

            $articles = Zend_Registry::get('articles');

            $articleObj = new $articles [$article->typ] ( );

            if (method_exists($articleObj, 'copyPreDispatch'))
                $articleObj->copyPreDispatch($article, $this->getLayouterId());

            $this->setArticleId($article->id);
        }

        $contact = $article->Contact;
        $contact->article_count = $contact->article_count + 1;
        $contact->save();

        $article->title = $this->getArticleName();
        $article->info = $this->getArticleDescription();
        $article->einleitung = $this->getArticleEinleitung();
        $article->private = $this->getArticlePrivate();
        $article->shop_id = $this->getArticleShop();
        $article->a6_resale_price = (str_replace(',', '.', $this->getResalePrice())/1.19);
        $article->display_market = $this->getDisplayMarket();
        $article->resale = false;
        $article->resale_design = false;
        $article->a4_abpreis_calc = true;
        $article->a4_abpreis_must_recalc = true;

        $article->not_edit = $this->getArticleNotEdit();
        $article->resale_design = $this->getArticleResaleDesign();
        $article->file = "";
        $article->file1 = "";
        $article->custom_1_title = "";
        $article->custom_1_text = "";
        $article->visits = 0;
        $article->confirm = false;
        $article->confirmone = false;
        $article->used = 0;
        $article->pos = 0;

        $article->rate = 0;
        $article->rate_count = 0;

        if($this->getAllowLayouter()) {
            $article->not_edit = false;
            $article->resale_design = false;
        }else{
            $article->not_edit = true;
            $article->resale_design = false;
        }

        $article->render_new_preview_image = true;
        $article->render_new_preview_pdf = true;
        $article->render_new_preview_gallery = true;

        $article->save();

        if ($this->getArticlePrivate()) {
            foreach ($this->getArticlePrivateContact() as $key => $row) {
                $articlecontact = new ContactArticle();
                $articlecontact->article_id = $article->id;
                $articlecontact->contact_id = $row;
                $articlecontact->save();
            }
        }


        $tags = $this->getArticleTags();
        $filter = new TP_Filter_Badwords();

        if (isset($tags) && trim($tags) != "") {
            $tagsTemp = array();
            foreach (explode(',', $tags) as $key) {
                $tag = strtolower(trim($filter->filter($key)));
                if ($tag) {
                    $tagsTemp[] = array('name' => $tag, 'url' => Doctrine_Inflector::urlize($tag));
                }
            }

            $obj = TP_Mongo::getInstance();
            $collection = $obj->articles;
            $collection->save(array_merge($article->getRiakData(), array('tags' => $tagsTemp, '_id' => $article->uuid)));
        }

        $this->setArticleId($article->uuid);

        $orgarticlegroups = $article->OrgArticle->ArticleGroupMarketArticle->toArray();

        foreach ($orgarticlegroups as $groups) {

            $articlecon = Doctrine_Query::create()->from('ArticleGroupMarketArticle c')->where('c.article_id = ? AND c.articlegroup_id = ?', array($article->id, $groups ['articlegroup_id']))->fetchOne();

            if ($articlecon == false) {
                $a = new ArticleGroupMarketArticle ( );
                $a->article_id = $article->id;
                $a->articlegroup_id = $groups ['articlegroup_id'];
                $a->save();
            }
        }

        if ($this->getArticleTheme1() != "" && $this->getArticleTheme1() != "%" && $this->getArticleTheme1() != "*") {

            $articlecon = Doctrine_Query::create()->from('ArticleThemeArticle c')->where('c.article_id = ? AND c.theme_id = ?', array($article->id, $this->getArticleTheme1()))->fetchOne();

            if ($articlecon == false) {
                $a = new ArticleThemeArticle ( );
                $a->article_id = $article->id;
                $a->theme_id = $this->getArticleTheme1();
                $a->save();
            }
        } elseif ($this->getArticleTheme1() != "" && $this->getArticleTheme1() != "%" && $this->getArticleTheme1() == "*" && $this->getArticleThemeInput1() != "") {

            $b = new ArticleTheme ( );
            $b->parent_id = 0;
            $b->shop_id = $this->getArticleShop();
            $b->title = $this->getArticleThemeInput1();
            $b->install_id = $article->install_id;
            $b->save();

            $a = new ArticleThemeArticle ( );
            $a->article_id = $article->id;
            $a->theme_id = $b->id;
            $a->save();
        }

        if ($this->getArticleTheme2() != "" && $this->getArticleTheme2() != "%" && $this->getArticleTheme2() != "*") {
            $articlecon = Doctrine_Query::create()->from('ArticleThemeArticle c')->where('c.article_id = ? AND c.theme_id = ?', array($article->id, $this->getArticleTheme2()))->fetchOne();

            if ($articlecon == false) {
                $a = new ArticleThemeArticle ( );
                $a->article_id = $article->id;
                $a->theme_id = $this->getArticleTheme2();
                $a->save();
            }
        } elseif ($this->getArticleTheme2() != "" && $this->getArticleTheme2() != "%" && $this->getArticleTheme2() == "*" && $this->getArticleThemeInput2() != "") {

            $b = new ArticleTheme ( );
            $b->parent_id = 0;
            $b->shop_id = $this->getArticleShop();
            $b->title = $this->getArticleThemeInput2();
            $b->install_id = $article->install_id;
            $b->save();

            $a = new ArticleThemeArticle ( );
            $a->article_id = $article->id;
            $a->theme_id = $b->id;
            $a->save();
        }

        if ($this->getArticleTheme3() != "" && $this->getArticleTheme3() != "%" && $this->getArticleTheme3() != "*") {
            $articlecon = Doctrine_Query::create()->from('ArticleThemeArticle c')->where('c.article_id = ? AND c.theme_id = ?', array($article->id, $this->getArticleTheme3()))->fetchOne();

            if ($articlecon == false) {
                $a = new ArticleThemeArticle ( );
                $a->article_id = $article->id;
                $a->theme_id = $this->getArticleTheme3();
                $a->save();
            }
        } elseif ($this->getArticleTheme3() != "" && $this->getArticleTheme3() != "%" && $this->getArticleTheme3() == "*" && $this->getArticleThemeInput3() != "") {

            $b = new ArticleTheme ( );
            $b->parent_id = 0;
            $b->shop_id = $this->getArticleShop();
            $b->title = $this->getArticleThemeInput3();
            $b->install_id = $article->install_id;
            $b->save();

            $a = new ArticleThemeArticle ( );
            $a->article_id = $article->id;
            $a->theme_id = $b->id;
            $a->save();
        }

        if ($this->getArticleGroup1() != "" && $this->getArticleGroup1() != "%" && $this->getArticleGroup1() != '*') {

            $articlecon = Doctrine_Query::create()->from('ArticleGroupArticle c')->where('c.article_id = ? AND c.articlegroup_id = ?', array($article->id, $this->getArticleGroup1()))->fetchOne();

            if ($articlecon == false) {
                $a = new ArticleGroupArticle ( );
                $a->article_id = $article->id;
                $a->articlegroup_id = $this->getArticleGroup1();
                $a->save();
            }
        } elseif ($this->getArticleGroup1() != "" && $this->getArticleGroup1() != "%" && $this->getArticleGroup1() == '*' && $this->getArticleGroupInput1()) {

            $b = new ArticleGroup ( );
            $b->parent = 0;
            $b->shop_id = $this->getArticleShop();
            $b->title = $this->getArticleGroupInput1();
            $b->install_id = $article->install_id;
            $b->language = 'all';
            $b->save();

            $a = new ArticleGroupArticle ( );
            $a->article_id = $article->id;
            $a->articlegroup_id = $b->id;
            $a->save();
        }

        if ($this->getArticleGroup2() != "" && $this->getArticleGroup2() != "%" && $this->getArticleGroup2() != '*') {
            $articlecon = Doctrine_Query::create()->from('ArticleGroupArticle c')->where('c.article_id = ? AND c.articlegroup_id = ?', array($article->id, $this->getArticleGroup2()))->fetchOne();

            if ($articlecon == false) {
                $a = new ArticleGroupArticle ( );
                $a->article_id = $article->id;
                $a->articlegroup_id = $this->getArticleGroup2();
                $a->save();
            }
        } elseif ($this->getArticleGroup2() != "" && $this->getArticleGroup2() != "%" && $this->getArticleGroup2() == '*' && $this->getArticleGroupInput2()) {

            $b = new ArticleGroup ( );
            $b->parent = 0;
            $b->shop_id = $this->getArticleShop();
            $b->title = $this->getArticleGroupInput2();
            $b->install_id = $article->install_id;
            $b->language = 'all';
            $b->save();

            $a = new ArticleGroupArticle ( );
            $a->article_id = $article->id;
            $a->articlegroup_id = $b->id;
            $a->save();
        }

        if ($this->getArticleGroup3() != "" && $this->getArticleGroup3() != "%" && $this->getArticleGroup3() != '*') {
            $articlecon = Doctrine_Query::create()->from('ArticleGroupArticle c')->where('c.article_id = ? AND c.articlegroup_id = ?', array($article->id, $this->getArticleGroup3()))->fetchOne();

            if ($articlecon == false) {
                $a = new ArticleGroupArticle ( );
                $a->article_id = $article->id;
                $a->articlegroup_id = $this->getArticleGroup3();
                $a->save();
            }
        } elseif ($this->getArticleGroup3() != "" && $this->getArticleGroup3() != "%" && $this->getArticleGroup3() == '*' && $this->getArticleGroupInput3()) {

            $b = new ArticleGroup ( );
            $b->parent = 0;
            $b->shop_id = $this->getArticleShop();
            $b->title = $this->getArticleGroupInput3();
            $b->install_id = $article->install_id;
            $b->language = 'all';
            $b->save();

            $a = new ArticleGroupArticle ( );
            $a->article_id = $article->id;
            $a->articlegroup_id = $b->id;
            $a->save();
        }

        if ($this->getArticleThemeMarket1() != "" && $this->getArticleThemeMarket1() != "%") {
            $articlecon = Doctrine_Query::create()->from('ArticleThemeMarketArticle c')->where('c.article_id = ? AND c.theme_id = ?', array($article->id, $this->getArticleThemeMarket1()))->fetchOne();

            if ($articlecon == false) {
                $a = new ArticleThemeMarketArticle ( );
                $a->article_id = $article->id;
                $a->theme_id = $this->getArticleThemeMarket1();
                $a->save();
            }
        }
        if ($this->getArticleThemeMarket2() != "" && $this->getArticleThemeMarket2() != "%") {
            $articlecon = Doctrine_Query::create()->from('ArticleThemeMarketArticle c')->where('c.article_id = ? AND c.theme_id = ?', array($article->id, $this->getArticleThemeMarket2()))->fetchOne();

            if ($articlecon == false) {
                $a = new ArticleThemeMarketArticle ( );
                $a->article_id = $article->id;
                $a->theme_id = $this->getArticleThemeMarket2();
                $a->save();
            }
        }
        if ($this->getArticleThemeMarket3() != "" && $this->getArticleThemeMarket3() != "%") {
            $articlecon = Doctrine_Query::create()->from('ArticleThemeMarketArticle c')->where('c.article_id = ? AND c.theme_id = ?', array($article->id, $this->getArticleThemeMarket3()))->fetchOne();

            if ($articlecon == false) {
                $a = new ArticleThemeMarketArticle ( );
                $a->article_id = $article->id;
                $a->theme_id = $this->getArticleThemeMarket3();
                $a->save();
            }
        }

        TP_Queue::process('articlemarketcreated', 'global', array('article' => $article));
    }

    public function createShop()
    {

        if (!$this->_data->shop->created) {

            $baseShop = Zend_Registry::get('shop');
            $baseInstall = Zend_Registry::get('install');

            $user = Zend_Auth::getInstance()->getIdentity();

            $shop = new Shop ( );
            $shop->install_id = $baseShop ['install_id'];
            $shop->customtemplates = true;
            $shop->market = true;
            $shop->display_market = true;
            $shop->private = false;
            $shop->name = $this->_data->shop->name;
            $shop->subtitle = $this->_data->shop->description;
            $shop->description = $this->_data->shop->description;
            $shop->layout = 'bootstrap';
            if($baseShop ['install_id'] == 34) {
                $shop->customtemplates = false;
                $shop->layout = "../0001-5dd94c57-510a3727-7223-a8ad7e21/subshopdesigns/public/ligakalender";
            }
            $shop->keywords = $this->_data->shop->keywords;
            $shop->author = $user ['self_firstname'] . ' ' . $user ['self_lastname'];
            $shop->copyright = "";
            $shop->defaultfunc = 'Index';
            $shop->defaultparam = 'all';
            $shop->css = $baseShop['css'];
            $shop->creator_id = $user['id'];

            $shop->betreiber_name = $user ['self_firstname'] . ' ' . $user ['self_lastname'];
            $shop->betreiber_address = $user ['self_zip'] . ' ' . $user ['self_city'];
            $shop->betreiber_street = $user ['self_street'] . ' ' . $user ['self_house_number'];
            $shop->betreiber_email = $user ['self_email'];

            $shop->service_value = $baseInstall['service_value'];

            $shop->layouter_presets = $baseShop['layouter_presets'];

            $shop->logo1 = $this->getLogo();

            $shop->template_display_products = true;
            $shop->template_display_motive = true;

            $shop->template_display_motive_all = true;
            $shop->template_display_motive_fav = true;
            $shop->template_display_motive_my = true;

            $shop->template_display_products_all = true;
            $shop->template_display_products_product_templates = true;
            $shop->template_display_products_products = true;
            $shop->template_display_products_templates = true;
            $shop->template_display_products_own_products = true;
            $shop->template_display_products_my_products = true;
            $shop->template_display_products_custom_layouter = false;
            $shop->template_display_products_calc = false;
            $shop->template_display_products_depot = false;

            $shop->template_display_products_navi = true;
            $shop->template_display_theme_navi = true;
            $shop->template_display_help = true;
            $shop->template_display_help_cms = true;
            $shop->template_display_news = true;

            if ($this->_data->shop->type == 2) {
                $shop->private = true;
                $shop->registration = false;
            }

            if ($this->_data->shop->type == 3) {
                $shop->template_display_products_custom_layouter = true;
                $shop->template_display_products_calc = true;
                $shop->template_display_products_product_templates = false;
                $shop->template_display_products_templates = false;
                $shop->market = false;
            }

            if ($this->_data->shop->type == 4) {
                $shop->private = true;
                $shop->registration = false;
                $shop->template_display_products_custom_layouter = true;
                $shop->template_display_products_calc = true;
                $shop->template_display_products_product_templates = false;
                $shop->template_display_products_templates = false;
                $shop->market = false;
            }

            $shop->save();

            $account = new Account ( );
            $account->company = "No Account";
            $account->install_id = $shop->install_id;

            $account->save();

            $accountshop = new ShopAccount ( );
            $accountshop->account_id = $account->id;
            $accountshop->shop_id = $shop->id;

            $accountshop->save();

            $shop->default_account = $account->id;

            $shop->save();

            $this->setShopId($shop->id);


            if ($this->getShopKto() != "") {

                $contact = Doctrine_Query::create()->from('Contact as c')->where('c.id = ?', array($user ['id']))->fetchOne();
                $contact->shop_kto = $this->getShopKto();
                $contact->shop_blz = $this->getShopBlz();
                $contact->shop_bank_name = $this->getShopBankName();
                $contact->shop_bic = $this->getShopBic();
                $contact->shop_iban = $this->getShopIban();
                $contact->save();
            }

            $shopContact = new ShopContact ( );
            $shopContact->contact_id = $user ['id'];
            $shopContact->shop_id = $shop->id;
            $shopContact->admin = true;
            $shopContact->login = true;
            $shopContact->save();

            if ($shopContact->contact_id != $baseInstall ['admincontact']) {
                $shopContact = new ShopContact ( );
                $shopContact->contact_id = $baseInstall ['admincontact'];
                $shopContact->shop_id = $shop->id;
                $shopContact->admin = true;
                $shopContact->login = true;
                $shopContact->save();
            }

            $domain = new Domain ( );
            $domain->name = $this->_data->shop->domain;
            $domain->shop_id = $shop ['id'];

            $domain->save();

            $temp = array(8, 9, 10, 13, 23, 28, 31, 32, 33, 34, 35, 38, 46, 48, 49, 50, 51);
            foreach ($temp as $key) {
                $resprevshop = new ResourcePrivilegeShop ( );
                $resprevshop->shop_id = $shop->id;
                $resprevshop->resourceprivilege_id = $key;
                $resprevshop->save();
            }

            $role = Doctrine_Query::create()->from('ContactRole m')->where('m.contact_id = ? AND m.role_id = 4')->fetchOne(array($user ['id']));
            if ($role == false) {
                $role = new ContactRole ( );
                $role->contact_id = $user ['id'];
                $role->role_id = 4;
                $role->save();
            }

            $cms = Doctrine_Query::create()->from('Cms m')->where('m.shop_id = ? AND m.copy_market = 1', array($baseInstall ['defaultmarket']))->execute();

            foreach ($cms as $cm) {
                $a = $cm->copy();
                $a->shop_id = $shop->id;
                $a->save();
            }

            if ($this->getShopTheme1() != "") {
                $a = new ShopThemeShop ( );
                $a->shop_id = $shop->id;
                $a->theme_id = $this->getShopTheme1();
                $a->save();
            }

            if ($this->getShopTheme2() != "") {
                $a = new ShopThemeShop ( );
                $a->shop_id = $shop->id;
                $a->theme_id = $this->getShopTheme2();
                $a->save();
            }

            if ($this->getShopTheme3() != "") {
                $a = new ShopThemeShop ( );
                $a->shop_id = $shop->id;
                $a->theme_id = $this->getShopTheme3();
                $a->save();
            }

            TP_Queue::process('shopmarketcreated', 'global', $shop);
        }

        $this->_data->shop->created = true;
    }

    public function hasArticleId()
    {
        if ($this->_data->shop->articleid != "")
            return true;
        return false;
    }

    public function getShopKeywords()
    {
        return $this->_data->shop->keywords;
    }

    public function setLogo($var)
    {
        $this->_data->shop->logo = $var;
    }

    public function getLogo()
    {
        return $this->_data->shop->logo;
    }

    public function setArticlePrivate($var)
    {
        $this->_data->shop->articleprivate = $var;
    }

    public function getArticlePrivate()
    {
        return $this->_data->shop->articleprivate;
    }

    public function setArticlePrivateAccount($var)
    {
        $this->_data->shop->privateaccount = $var;
    }

    public function getArticlePrivateAccount()
    {
        return $this->_data->shop->privateaccount;
    }

    public function setArticlePrivateContact($var)
    {
        $this->_data->shop->privatecontact = $var;
    }

    public function getArticlePrivateContact()
    {
        return $this->_data->shop->privatecontact;
    }

    private function initWizard()
    {
        $this->_data->shop->name = "";
        $this->_data->shop->description = "";
        $this->_data->shop->keywords = "";
        $this->_data->shop->shoptheme1 = "";
        $this->_data->shop->shoptheme2 = "";
        $this->_data->shop->shoptheme3 = "";
        $this->_data->shop->design = "";
        $this->_data->shop->created = false;
        $this->_data->shop->articleprivate = false;

        $this->_data->shop->privateaccount = false;
        $this->_data->shop->privatecontact = array();

        $this->_data->shop->resale = false;
        $this->_data->shop->display_market = false;
        $this->_data->shop->agb = false;
        $this->_data->shop->domain = "";
        $this->_data->shop->articleid = "";
        $this->_data->shop->layouterid = "";
        $this->_data->shop->articlename = "";
        $this->_data->shop->articledescription = "";
        $this->_data->shop->articleeinleitung = "";
        $this->_data->shop->articleshop = "";

        $this->_data->shop->shopid = "";

        $this->_data->shop->logo = "";

        $this->_data->shop->articletags = "";

        $this->_data->shop->resale_price = 0;

        $this->_data->shop->articlegroup1 = "";
        $this->_data->shop->articlegroup2 = "";
        $this->_data->shop->articlegroup3 = "";

        $this->_data->shop->articlegroupinput1 = "";
        $this->_data->shop->articlegroupinput2 = "";
        $this->_data->shop->articlegroupinput3 = "";

        $this->_data->shop->articlegroupmarket1 = "";
        $this->_data->shop->articlegroupmarket2 = "";
        $this->_data->shop->articlegroupmarket3 = "";

        $this->_data->shop->articletheme1 = "";
        $this->_data->shop->articletheme2 = "";
        $this->_data->shop->articletheme3 = "";

        $this->_data->shop->articlethemeinput1 = "";
        $this->_data->shop->articlethemeinput2 = "";
        $this->_data->shop->articlethemeinput3 = "";

        $this->_data->shop->articlethememarket1 = "";
        $this->_data->shop->articlethememarket2 = "";
        $this->_data->shop->articlethememarket3 = "";

        $this->_data->shop->shop_kto = "";
        $this->_data->shop->shop_blz = "";
        $this->_data->shop->shop_bic = "";
        $this->_data->shop->shop_iban = "";
        $this->_data->shop->shop_bank_name = "";

        $this->_data->shop->resaleMotiv = '';

        $this->_data->shop->motiveresale = array(
            'resale_shop' => 0,
            'resale_market' => 1,
            'resale_download' => 0,
            'price1' => 0,
            'price2' => 0,
            'price3' => 0,
            'download_price' => 0,
            'title' => '',
            'copyright' => '',
            'tags' => '',
            'motivtheme1' => '',
            'motivtheme2' => '',
            'motivtheme3' => '',
            'markettheme1' => '',
            'markettheme2' => '',
            'markettheme3' => '',
            'motivnames' => array()
        );

        $this->_data->shop->type = 1;

        $this->_data->shop->resale_design = false;
        $this->_data->shop->allow_layouter = false;
        $this->_data->shop->not_edit = false;
    }

    public function setBank($kto, $blz, $bank_name, $bic, $iban)
    {
        $this->_data->shop->shop_kto = $kto;
        $this->_data->shop->shop_blz = $blz;
        $this->_data->shop->shop_bank_name = $bank_name;
        $this->_data->shop->shop_bic = $bic;
        $this->_data->shop->shop_iban = $iban;
    }

    public function setAllowLayouter($value) {
        $this->_data->shop->allow_layouter = $value;
    }

    public function getAllowLayouter() {
        return $this->_data->shop->allow_layouter;
    }

    public function getShopKto()
    {
        return $this->_data->shop->shop_kto;
    }

    public function getShopBlz()
    {
        return $this->_data->shop->shop_blz;
    }

    public function getShopBankName()
    {
        return $this->_data->shop->shop_bank_name;
    }

    public function getShopBic()
    {
        return $this->_data->shop->shop_bic;
    }

    public function getShopIban()
    {
        return $this->_data->shop->shop_iban;
    }

    public function setArticleNotEdit($var)
    {
        $this->_data->shop->not_edit = $var;
    }

    public function setArticleResaleDesign($var)
    {
        $this->_data->shop->resale_design = $var;
    }

    public function getArticleNotEdit()
    {
        return $this->_data->shop->not_edit;
    }

    public function setArticleTags($var)
    {
        $this->_data->shop->articletags = $var;
    }

    public function getArticleTags()
    {
        return $this->_data->shop->articletags;
    }

    public function getArticleResaleDesign()
    {
        return $this->_data->shop->resale_design;
    }

    public function setShopId($var)
    {
        $this->_data->shop->shopid = $var;
    }

    public function getShopId()
    {
        return $this->_data->shop->shopid;
    }

    public function setMotivStep1($value)
    {
        $this->_data->shop->motiveresale = $value;
    }

    public function setResaleMotive($value)
    {
        $this->_data->shop->motiveresale['motivnames'] = $value;
    }

    public function getResaleMotive()
    {
        return $this->_data->shop->motiveresale;
    }

    public function getArticleGroup1()
    {
        return $this->_data->shop->articlegroup1;
    }

    public function getArticleGroup2()
    {
        return $this->_data->shop->articlegroup2;
    }

    public function setStep0($value)
    {
        $this->_data->shop->type = $value;
    }

    public function getType()
    {
        return $this->_data->shop->type;
    }

    public function getArticleGroup3()
    {
        return $this->_data->shop->articlegroup3;
    }

    public function getArticleGroupInput1()
    {
        return $this->_data->shop->articlegroupinput1;
    }

    public function getArticleGroupInput2()
    {
        return $this->_data->shop->articlegroupinput2;
    }

    public function getArticleGroupInput3()
    {
        return $this->_data->shop->articlegroupinput3;
    }

    public function getArticleGroupMarket1()
    {
        return $this->_data->shop->articlegroupmarket1;
    }

    public function getArticleGroupMarket2()
    {
        return $this->_data->shop->articlegroupmarket2;
    }

    public function getArticleGroupMarket3()
    {
        return $this->_data->shop->articlegroupmarket3;
    }

    public function getArticleTheme1()
    {
        return $this->_data->shop->articletheme1;
    }

    public function getArticleTheme2()
    {
        return $this->_data->shop->articletheme2;
    }

    public function getArticleTheme3()
    {
        return $this->_data->shop->articletheme3;
    }

    public function getArticleThemeInput1()
    {
        return $this->_data->shop->articlethemeinput1;
    }

    public function getArticleThemeInput2()
    {
        return $this->_data->shop->articlethemeinput2;
    }

    public function getArticleThemeInput3()
    {
        return $this->_data->shop->articlethemeinput3;
    }

    public function getArticleThemeMarket1()
    {
        return $this->_data->shop->articlethememarket1;
    }

    public function getArticleThemeMarket2()
    {
        return $this->_data->shop->articlethememarket2;
    }

    public function getArticleThemeMarket3()
    {
        return $this->_data->shop->articlethememarket3;
    }

    public function clearWizard()
    {
        $this->initWizard();
    }

}
