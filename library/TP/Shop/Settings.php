<?php

class TP_Shop_Settings
{

	

	
	public static function getSetting($standard = true, $value = false) {
		
		if(!$value) return false;
		
		$shop = Zend_Registry::get('shop');
	
		$row = Doctrine_Query::create()->select()->from('Shopsetting s')->where('s.shop_id = ? AND s.keyid = ?',array($shop['id'], $value))->fetchOne();
		
		if($row) {
			return $row->value;
		}
		
		return $standard;
	
	}
	
}