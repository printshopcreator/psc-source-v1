<?php

class TP_Shopverwaltung {

    /**
     * Filter-Sessionnamespace
     *
     * @var Zend_Session_Namespace
     */
    private $_Settings;

    /**
     * Konstruiert das Model für die Suche
     *
     * @return void
     */
    public function __construct()
    {

        $this->_Settings = new Zend_Session_Namespace('Shopverwaltung');

        $this->_Settings->unlock();

        if(Zend_Session::namespaceIsset ('Shopverwaltung') === false)
        {
            $this->initSettings();
        }

        $this->_Settings->lock();
    }


    public function setCurrentShop($id) {
        $this->_Settings->unlock();
        $this->_Settings->current_shop = $id;
        $this->_Settings->lock();
    }

    public function getCurrentShop() {
        return $this->_Settings->current_shop;
    }

    private function initSettings() {
        $this->_Settings->current_shop = false;
    }


    public function clearSettings() {
        $this->initSettings();
    }

}
