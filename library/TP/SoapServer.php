<?php

class LoginResult {
    
    /**
     * @var integer
     */
    public $code;
    
    /**
     * @var string
     */
    public $uuid;
    
    /**
     * @var string
     */
    public $message;
}

class SoapContact {
    
    /**
     * @var string
     */
    public $username;
}

class SoapAddress {
    
    /**
     * @var string
     */
    public $firstname;
    
    /**
     * @var string
     */
    public $lastname;
    
    /**
     * @var string
     */
    public $street;
    
    /**
     * @var string
     */
    public $house_number;
    
    /**
     * @var string
     */
    public $zip;
    
    /**
     * @var string
     */
    public $city;
    
    /**
     * @var string
     */
    public $email;
    
    /**
     * @var string
     */
    public $phone;
}

class SoapOrder {
    
    /**
     * @var string
     */
    public $uuid;
    
    /**
     * @var string
     */
    public $alias;
    
    /**
     * @var string
     */
    public $created;
    
    /**
     * @var string
     */
    public $updated;
    
    /**
     * @var string
     */
    public $status;
    
    /**
     * @var SoapContact
     */
    public $contact;

    /**
     * @var int
     */
    public $contact_id;

    /**
     * @var string
     */
    public $gutschein;

    /**
     * @var float
     */
    public $gutschein_abzug;

    /**
     * @var int
     */
    public $gutschein_typ;
    
    /**
     * @var SoapAddress
     */
    public $deliveryAddress;
    
    /**
     * @var SoapAddress
     */
    public $invoiceAddress;
    
    /**
     * @var SoapAddress
     */
    public $senderAddress;
    
    /**
     * @var float
     */
    public $netto;
    
    /**
     * @var float
     */
    public $mwert;
    
    /**
     * @var float
     */
    public $brutto;
    
    /**
     * @var float
     */
    public $payment_price;
    
    /**
     * @var string
     */
    public $payment_name;
    
    /**
     * @var float
     */
    public $shipping_price;
    
    /**
     * @var string
     */
    public $shipping_name;
    
    /**
     * @var float
     */
    public $coupon_price_netto;
    
    /**
     * @var float
     */
    public $coupon_price_brutto;
    
    /**
     * @var string
     */
    public $coupon_name;
}

class SoapOrderposCalcOption {
    
    /**
     * @var string
     */
    public $name;
    
    /**
     * @var string
     */
    public $value;
    
}

class SoapOrderpos {
    
    /**
     * @var string
     */
    public $product_name;

    /**
     * @var string
     */
    public $article_nr_extern;

    /**
     * @var string
     */
    public $text_art;

    /**
     * @var string
     */
    public $text_format;
    
    /**
     * @var int
     */
    public $product_count;
    
    /**
     * @var float
     */
    public $netto;
    
    /**
     * @var float
     */
    public $mwert;
    
    /**
     * @var float
     */
    public $brutto;
    
    /**
     * @var SoapOrderposCalcOption[]
     */
    public $calc_options;
    
}

class getOrdersResult {
    
    /**
     * @var integer
     */
    public $code;
    
    /**
     * @var string
     */
    public $uuid;
    
    /**
     * @var string
     */
    public $message;
    
    /**
     * @var SoapOrder[]
     */
    public $orders;
}

class getOrderResult {
    
    /**
     * @var integer
     */
    public $code;
    
    /**
     * @var string
     */
    public $uuid;
    
    /**
     * @var string
     */
    public $message;
    
    /**
     * @var SoapOrder
     */
    public $order;
    
    
    /**
     * @var SoapOrderpos[]
     */
    public $orderpos;
}

class setOrderStatusResult {
    
    /**
     * @var integer
     */
    public $code;
    
    /**
     * @var string
     */
    public $uuid;
    
    /**
     * @var string
     */
    public $message;
}

class TP_SoapServer {
    
    /**
     * log
     *
     * @return string
     */
    public function getSessionId() {
        return Zend_Session::getId();
    }
    
    /**
     * login
     * 
     * @param string $username
     * @param string $password
     * @return LoginResult
     */
    public function login($username, $password) {
        
        $shop = Zend_Registry::get('shop');
        
        $_authAdapter = new TP_Plugin_AuthAdapter(); // put this in a constructor?
        $_authAdapter->setIdentity($username)
                     ->setCredential($password);
        $result = Zend_Auth::getInstance()->authenticate($_authAdapter);
        
        $res = new LoginResult();

        if($result->isValid()){
            
            $user = Zend_Auth::getInstance()->getIdentity();
            
            $user = Doctrine_Query::create()->from('Contact m')->where('id = ?')->fetchOne(array(
                        $user['id']));
            $highRole = null;    
            if ($user != false) {
                foreach ($user->Roles as $rol) {
                    if ($highRole == null || $rol->level > $highRole->level)
                        $highRole = $rol;
                }
            }

            if($highRole->level >= 40) {
            
                $res->code = 200;
                $res->uuid = Zend_Session::getId();
                $res->message = "Login successful";
                try {
                    $api = new Api();
                    $api->uuid = Zend_Session::getId();
                    $api->shop_id = $shop['id'];
                    $api->install_id = $shop['install_id'];
                    $api->contact_id = $user['id'];
                    $api->save();
                }catch (\Exception $e) {
                    Zend_Registry::get('log')->debug($e->getMessage());
                }

            }else{
                $res->code = 202;
                $res->uuid = "";
                $res->message = "Not enough Rights";    
            }
            
            return $res;
        }else{
            $res->code = 203;
            $res->uuid = "";
            $res->message = "Bad Username or Password";
            

        }

        return $res;
        
    }
    
    /**
     * Get Orders
     * 
     * @param string $apiKey ApiKey
     * @param string $createdTs Timestamp wann erzeugt
     * @return getOrdersResult
     */
    public function getOrders($apiKey, $createdTs = false) {
        
        $key = Doctrine_Query::create()->select()->from('Api a')->where('a.uuid = ?')->fetchOne(array($apiKey));

        if(!$createdTs) {
            $createdTs = date("Y-m-d 00:00:00");
        }

        if(!$key) {
            $res = new getOrdersResult();
            $res->code = 303;
            $res->uuid = "";
            $res->message = "Bad Key";
            
            return $res;
        }
        
        
        $orders = Doctrine_Query::create()->select()
                    ->from('Orders o')->where('o.install_id = ? AND o.created >= ? ', array($key->install_id, $createdTs))->orderBy('o.id DESC')->execute();
        
        
        $res = new getOrdersResult();
        $res->code = 300;
        $res->uuid = $key->uuid;
        $res->message = "Success";

        foreach($orders as $order) {
            
            $ord = new SoapOrder();
            $ord->uuid = $order->id;
            $ord->alias = $order->alias;
            $ord->created = $order->created;
            $ord->updated = $order->updated;
            $ord->status = $order->status;
            $res->orders[] = $ord;
        }
        
        
        return $res;
        
    }
    
    /**
     * Get Order
     * 
     * @param string $apiKey ApiKey
     * @param int $orderId OrderId
     * @return getOrderResult
     */
    public function getOrder($apiKey, $orderId) {
        
        $key = Doctrine_Query::create()->select()->from('Api a')->where('a.uuid = ?')->fetchOne(array($apiKey));
        
        if(!$key) {
            $res = new getOrdersResult();
            $res->code = 403;
            $res->uuid = "";
            $res->message = "Bad Key";
            
            return $res;
        }
        
        
        $order = Doctrine_Query::create()->select()
                    ->from('Orders o')->where('o.install_id = ? AND o.id = ?', array($key->install_id, $orderId))->fetchOne();
        
        if(!$order) {
            $res = new getOrderResult();
            $res->code = 404;
            $res->uuid = "";
            $res->message = "Bad OrderId";
            
            return $res;
        }
        
        
        $res = new getOrderResult();
        $res->code = 400;
        $res->uuid = $key->uuid;
        $res->message = "Success";
        
        $ord = new SoapOrder();
        $ord->uuid = $order->id;
        $ord->alias = $order->alias;
        $ord->created = $order->created;
        $ord->updated = $order->updated;
        $ord->status = $order->status;
        $ord->contact_id = $order->contact_id;
        $ord->gutschein = $order->gutschein;
        $ord->gutschein_abzug = $order->gutscheinabzug;
        $ord->gutschein_typ = $order->gutscheinabzugtyp;

        $invoiceAd = new SoapAddress();
        $invoiceAd->firstname = $order->getInvoiceAddressSaved()['firstname'];
        $invoiceAd->lastname = $order->getInvoiceAddressSaved()['lastname'];
        $invoiceAd->street = $order->getInvoiceAddressSaved()['street'];
        $invoiceAd->house_number = $order->getInvoiceAddressSaved()['house_number'];
        $invoiceAd->zip = $order->getInvoiceAddressSaved()['zip'];
        $invoiceAd->city = $order->getInvoiceAddressSaved()['city'];
        $invoiceAd->email = $order->getInvoiceAddressSaved()['email'];
        $invoiceAd->phone = $order->getInvoiceAddressSaved()['phone'];
        
        $ord->invoiceAddress = $invoiceAd;

        $invoiceDel = new SoapAddress();
        $invoiceDel->firstname = $order->getDeliveryAddressSaved()['firstname'];
        $invoiceDel->lastname = $order->getDeliveryAddressSaved()['lastname'];
        $invoiceDel->street = $order->getDeliveryAddressSaved()['street'];
        $invoiceDel->house_number = $order->getDeliveryAddressSaved()['house_number'];
        $invoiceDel->zip = $order->getDeliveryAddressSaved()['zip'];
        $invoiceDel->city = $order->getDeliveryAddressSaved()['city'];
        $invoiceDel->email = $order->getDeliveryAddressSaved()['email'];
        $invoiceDel->phone = $order->getDeliveryAddressSaved()['phone'];

        $ord->deliveryAddress = $invoiceDel;

        $invoiceSd = new SoapAddress();
        $invoiceSd->firstname = $order->getSenderAddressSaved()['firstname'];
        $invoiceSd->lastname = $order->getSenderAddressSaved()['lastname'];
        $invoiceSd->street = $order->getSenderAddressSaved()['street'];
        $invoiceSd->house_number = $order->getSenderAddressSaved()['house_number'];
        $invoiceSd->zip = $order->getSenderAddressSaved()['zip'];
        $invoiceSd->city = $order->getSenderAddressSaved()['city'];
        $invoiceSd->email = $order->getSenderAddressSaved()['email'];
        $invoiceSd->phone = $order->getSenderAddressSaved()['phone'];

        $ord->senderAddress = $invoiceSd;
        
        $datas = array();
        $mwert = array();
        $prodpreisbrutto = 0;
        $summewarenwert = array();
        $prodpreis = 0;
        $versandMwert = 0;
        $versand = 0;
        
        foreach ($order->Orderspos as $pos) {
            $pro = unserialize($pos->data);
            $proa = $pro->getArticle();
            $proo = $pro->getOptions();



            $article = Doctrine_Query::create()
                            ->from('Article c')
                            ->where('c.id = ?', array($pos->article_id))
                            ->fetchOne();

            if($article->a6_org_article != 0) {
                $article = Doctrine_Query::create()
                    ->from('Article c')
                    ->where('c.id = ?', array($article->a6_org_article))
                    ->fetchOne();
            }
            $options = $article->getOptArray($pos->data);
            $rawoptions = $article->getRawOptArray($pos->data);
            
            $files = array();
            
            if($pos->getPostionData('auflage')) {
                $pos->count = $pos->getPostionData('auflage');
            }
            
            
            
            $orderpos = new SoapOrderpos();
            $orderpos->product_name = $proa['title'];
            $orderpos->product_count = $pos->count;
            $orderpos->brutto = $pos->priceallbrutto;
            $orderpos->mwert = $pos->priceallsteuer;
            $orderpos->netto = $pos->priceall;
            $orderpos->article_nr_extern = $article->article_nr_extern;
            $orderpos->text_art = $article->text_art;
            $orderpos->text_format = $article->text_format;

            foreach($rawoptions as $key => $item) {
                
                $option = new SoapOrderposCalcOption();
                $option->name = $key;
                $option->value = $item;
                
                $orderpos->calc_options[] = $option;
            }
            
            foreach(Zend_Json::decode($pos->calc_values) as $key => $item) {
                
                $option = new SoapOrderposCalcOption();
                $option->name = $key;
                $option->value = $item;
                
                $orderpos->calc_options[] = $option;
            }
            
            $prodpreisbrutto = $prodpreisbrutto + $pos->priceallbrutto;
            $prodpreis = $prodpreis + $pos->priceall;
            if(!isset($mwert[$article->mwert])) {
                $mwert[$article->mwert] = 0;
            }
            if(!isset($summewarenwert[$article->mwert])) {
                $summewarenwert[$article->mwert] = 0;
            }
            $mwert[$article->mwert] = ($mwert[$article->mwert]*1) + $pos->priceallsteuer;
            $summewarenwert[$article->mwert] = $summewarenwert[$article->mwert] + $pos->priceallbrutto;
            $versandMwert = $versandMwert + $pos->priceallsteuer;
            if ($article->versand == 'Fix') {
                $versand = $versand + $article->versandwert;
            } elseif ($article->versand == 'Position') {

            }
            
            $res->orderpos[] = $orderpos;
        }
        
        $tempMwert = array();
        $params = array();
        $params['gutscheinname'] = '';

        $nettogutschein = 0;
        if($order->gutscheinabzug != "" && $order->gutscheinabzug > 0) {
            $tempMwert = array();
            $versandMwert = 0;
            foreach($mwert as $key => $mw) {
				
                $anteil = (((($summewarenwert[$key] / $prodpreisbrutto) * $order->gutscheinabzug)*100)/(100+$key))*($key*0.01);
                $tempMwert[$key] = $tempMwert[$key] + ($mw - $anteil);
                $nettogutschein += (((($summewarenwert[$key] / $prodpreisbrutto) * $order->gutscheinabzug)*100)/(100+$key));
                $versandMwert += ($mw - $anteil);
            }
            
            $ord->coupon_name = Doctrine_Query::create()->from('CreditSystemDetail c')->where('c.uuid = ?', array($order->gutschein))->fetchOne()->CreditSystem->title;
            $ord->coupon_price_netto = $nettogutschein;
            $ord->coupon_price_brutto = $order->gutscheinabzug;
        }
        $mwert = Zend_Json::decode($order->mwertalle);

        $tmpWert = array();
/*
        foreach($mwert as $key => $mw) {
            $tmpWert[$key] = $currency->toCurrency($mw);
        }
        
  
        $params['mwerttable'] = $tmpWert;
        $params['prodbrutto'] = $currency->toCurrency($prodpreisbrutto);
        $params['prodnetto'] = $currency->toCurrency($prodpreis);
        $params['gutscheincode'] = $data->gutschein;
        $params['gutscheinabzug'] = $currency->toCurrency($data->gutscheinabzug);
        $params['gutscheinabzugnetto'] = $currency->toCurrency($nettogutschein);
        */
        
        $ord->brutto = $order->preisbrutto;
        $ord->netto = $order->preisbrutto - $order->preissteuer;
        $ord->mwert = $order->preissteuer;
        
        
        $shippingtype = Doctrine_Query::create()
                        ->from('Shippingtype c')
                        ->where('c.id = ?', array(intval($order->shippingtype_id)))
                        ->fetchOne();
        $paymenttype = Doctrine_Query::create()
                        ->from('Paymenttype c')
                        ->where('c.id = ?', array(intval($order->paymenttype_id)))
                        ->fetchOne();
        if ($shippingtype != false) {
            $ord->shipping_name = $shippingtype->title;
            $ord->shipping_id = $shippingtype->id;
            $ord->shipping_price = $versand + $shippingtype->kosten_fix * 1;
        }
        if ($paymenttype != false) {
            $ord->payment_name = $paymenttype->title;
            $ord->payment_id = $paymenttype->id;
            if ($paymenttype->prozent == 1) {
                $ord->payment_price = ($prodpreis) / 100 * $paymenttype->wert * 1;
               
            }else{
                $ord->payment_price = $paymenttype->wert * 1;
                
            }
        }
        
        $res->order = $ord;
        
        return $res;
        
    }

    /**
     * Set OrderStatus
     * 
     * @param string $apiKey ApiKey
     * @param int $orderId OrderId
     * @param int $statusId StatusId
     * @return setOrderStatusResult
     */
    public function setOrderStatus($apiKey, $orderId, $statusId) {
        
        $key = Doctrine_Query::create()->select()->from('Api a')->where('a.uuid = ?')->fetchOne(array($apiKey));
        
        if(!$key) {
            $res = new setOrderStatusResult();
            $res->code = 503;
            $res->uuid = "";
            $res->message = "Bad Key";
            
            return $res;
        }
        
        
        $order = Doctrine_Query::create()->select()
                    ->from('Orders o')->where('o.install_id = ? AND o.id = ?', array($key->install_id, $orderId))->fetchOne();
        
        if(!$order) {
            $res = new setOrderStatusResult();
            $res->code = 504;
            $res->uuid = "";
            $res->message = "Bad OrderId";
            
            return $res;
        }
        
        
        $order->status = $statusId;
        $order->save();

        if($statusId >= 140 && $statusId != 170 ) {

            $produktionszeit = 0;
            foreach($order->Orderspos as $pos) {
                $pro = unserialize($pos->data);
                $proo = $pro->getOptions();

                if(isset($proo['produktionszeit']) && $proo['produktionszeit'] > $produktionszeit) {
                    $produktionszeit = $proo['produktionszeit'];
                }
                if(isset($proo['produktionszeit'])) {
                    $obj = new TP_Delivery(date('Y-m-d'));
                    $result = $obj->getDeliveryDate($proo['produktionszeit']);
                    $pos->delivery_date = $result->format('Y-m-d');
                    $pos->save();
                }
            }

            if($produktionszeit > 0) {
                $obj = new TP_Delivery(date('Y-m-d'));
                $result = $obj->getDeliveryDate($produktionszeit);
                $order->delivery_date = $result->format('Y-m-d');
                $order->save();
            }

        }
        TP_Queue::process('orderstatuschange', 'global', $order);
        
        $res = new setOrderStatusResult();
        $res->code = 500;
        $res->uuid = Zend_Session::getId();
        $res->message = "Success";

        return $res;
    }
    
}