<?php

class TP_Status {

    public static $InternalName = 1;
    public static $ExternalName = 2;

    public static function getStatusText($key, $mode = 2) {

        $statusCollection = TP_Mongo::getInstance()->Status;

        $doc = $statusCollection->findOne(array('code' => (int)$key, 'typ' => 1));

        if($mode === 1) {
            return $doc['internalName'];
        }
        return $doc['externalName'];

    }

    public static function getStatusAsArray() {

        $statusCollection = TP_Mongo::getInstance()->Status;

        $docs = $statusCollection->find(array('typ' => 1));

        return $docs;

    }

    public static function getPositionStatusText($key, $mode = 2) {

        $statusCollection = TP_Mongo::getInstance()->Status;

        $doc = $statusCollection->findOne(array('code' => (int)$key, 'typ' => 2));

        if($mode === 1) {
            return $doc['internalName'];
        }
        return $doc['externalName'];

    }

    public static function getPositionStatusAsArray() {

        $statusCollection = TP_Mongo::getInstance()->Status;

        $docs = $statusCollection->find(array('typ' => 2));

        return $docs;

    }

}