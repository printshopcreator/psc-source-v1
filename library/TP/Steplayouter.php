<?php

class TP_Steplayouter {


    private $_config = array();

    private $_save_data = array();

    private $_save_layer_data = array();

    private $_data = array();

    private $_article = array();

    private $_save_create_data = array();

    private $_sites = array();

    private $_pdf = false;

    private $_layers = array();

    /*
     * @param array $config
     * @param array $data
     * @param Article $article
     * @return TP_Steplayouter
     */

    function __construct($save_data ,Article $article, $save_layer_data = array(), $save_create_data = array()) {

        $this->_save_data = $save_data;
        $this->_save_layer_data = $save_layer_data;
        $this->_article = $article;
        $this->_save_create_data = $save_create_data;
        $this->_sites = new TP_Steplayouter_Collection();

        return $this;
    }

    public function buildRenderStack($width = 0, $height = 0) {


        $conf = simplexml_load_string($this->_article->upload_steplayouter_xml);
        foreach($conf->rendering->sites->site as $site_config) {

            $site = new TP_Steplayouter_Site();
            $site = $site->createFromXml($site_config, $conf->steps, $this->_config, $this->_save_data, $this->_save_layer_data, $this->_save_create_data,$width, $height);

            $this->_sites->addItem($site);
        }
        foreach($conf->rendering->layers->layer as $layer) {
            if((int)$layer['scale'] == 0) {
                $layer['scale'] = 1;
            }
            if((int)$layer['scale'] == 0) {
                $layer['scale'] = 1;
            }
            if(!isset($layer['ispassepartout'])) {
                $layer['ispassepartout'] = 0;
            }
            $this->_layers[(string)$layer['id']] = array(
                'name' => (string)$layer['name'],
                'dragable' => (bool)intval($layer['dragable']),
                'scaleable' => (bool)intval($layer['scaleable']),
                'rotateable' => (bool)intval($layer['rotateable']),
                'clipbox' => (string)$layer['clipbox'],
                'rotate' => (int)$layer['rotate'],
                'scale' => (int)$layer['scale'],
                'fit' => (int)$layer['fit'],
                'hidden' => (bool)intval($layer['hidden']),
                'ispassepartout' => (bool)intval($layer['ispassepartout']),
                'onlypdf' => (bool)intval($layer['onlypdf'])
            );
        }


        foreach ($this->_save_create_data ['elements'] as $child) {
            if(isset($child['render-pos-layer']) &&
               !isset($this->_layers[$child['render-pos-layer']]) &&
                isset($child['render-pos-orglayer']) &&
                isset($this->_layers[$child['render-pos-orglayer']])) {
                $this->_layers[$child['render-pos-layer']] = $this->_layers[$child['render-pos-orglayer']];
            }
        }

    }

    public function exportPNG($filename, $width, $height, $site = 1) {

        if(!$this->_pdf) {
            $this->buildPDFLib($filename, $site);
        }
        exec('gs -dNOPAUSE -dMaxBitmap=2147483647 -r300 -dBATCH -sDEVICE=pngalpha -sOutputFile="'.$filename.'.png" "'.$filename.'.pdf"');
        exec('/usr/bin/convert -resize '.($width).'x'.($height).' "'.$filename.'.png" "'.$filename.'.png"');

        if($this->_sites->getIterator()->current()->getPassepartout() != "") {



            $im = new imagick($filename.'.png');
            $res = $im->getImagePage();

            $canvas = new Imagick();
            if(strpos($this->_sites->getIterator()->current()->getPassepartout(), "PuzzleMaske")) {
                $canvas->newImage( $res['width'], ($res['height']), 'transparent', 'png' );

                if(file_exists(APPLICATION_PATH . '/../'.$this->_sites->getIterator()->current()->getPassepartout())) {
                    $imcom = new imagick(APPLICATION_PATH . '/../'.$this->_sites->getIterator()->current()->getPassepartout());
                }
                if(file_exists(APPLICATION_PATH . '/../public/'.$this->_sites->getIterator()->current()->getPassepartout())) {
                    $imcom = new imagick(APPLICATION_PATH . '/../public/'.$this->_sites->getIterator()->current()->getPassepartout());
                }
                if($width > $height) {
                    $imcom->thumbnailImage($res['width'], false);
                }else{
                    $imcom->thumbnailImage($res['width'], false);
                }


                $canvas->compositeImage($im, imagick::COMPOSITE_DEFAULT, 0, 0);
                $canvas->compositeImage($imcom, imagick::COMPOSITE_DEFAULT, 0, 0);
                $canvas->writeimage($filename.'.png');
            }else{
                $canvas->newImage( $res['width'], ($res['height']), 'transparent', 'png' );

                if(file_exists(APPLICATION_PATH . '/../'.$this->_sites->getIterator()->current()->getPassepartout())) {
                    $imcom = new imagick(APPLICATION_PATH . '/../'.$this->_sites->getIterator()->current()->getPassepartout());
                }
                if(file_exists(APPLICATION_PATH . '/../public/'.$this->_sites->getIterator()->current()->getPassepartout())) {
                    $imcom = new imagick(APPLICATION_PATH . '/../public/'.$this->_sites->getIterator()->current()->getPassepartout());
                }
                if($width > $height) {
                    $imcom->thumbnailImage($res['width'], false);
                }else{
                    $imcom->thumbnailImage($res['width'], false);
                }


                $canvas->compositeImage($im, imagick::COMPOSITE_DEFAULT, 0, 0);
                $canvas->compositeImage($imcom, imagick::COMPOSITE_DEFAULT, 0, 0);
                $canvas->writeimage($filename.'.png');
            }
        }


        /*$image = new Imagick($filename.'.pdf['.($site-1).']');
        $image->setimageresolution(300,300);
        $image->thumbnailimage($width, $height, true);
        $image->writeimage($filename.'.png');
         */
        //Zend_Registry::get('log')->debug('gs -dNOPAUSE -dMaxBitmap=2147483647 -r300 -dBATCH -sDEVICE=pngalpha -sOutputFile="'.$filename.'.png" "'.$filename.'.pdf"');
        //Zend_Registry::get('log')->debug('/usr/bin/convert -resize '.($width).'x'.($height).' "'.$filename.'.png" "'.$filename.'.png"');
        //Zend_Registry::get('log')->debug();

        return $filename.'.png';

    }

    public function exportPNGS($filename, $width, $height, $site = 1) {

        if(!$this->_pdf) {
            $this->buildPDFLib($filename, false);
        }

        $images = array();
            Zend_Registry::get('log')->debug($filename.'.pdf');

        exec('gs -dNOPAUSE -dMaxBitmap=2147483647 -r300 -dBATCH -sDEVICE=pngalpha -sOutputFile="'.$filename.'%d.png" "'.$filename.'.pdf"');
        $i=1;
        foreach($this->_sites as $site)
        {
            exec('/usr/bin/convert -resize '.($width).'x'.($height).' "'.$filename.$i.'.png" "'.$filename.$i.'.png"');
            $image = new imagick($filename.$i.'.png');
            $width = $image->getImageWidth();
            $images[] = array('file' => $i.'.png', 'passepartout' => $site->getPassepartout(), 'passepartoutwidth' => $width);
            $i++;
        }
        return $images;
    }

    /**
     * @param $id
     * @return TP_Steplayouter_Site
     */
    public function getSite($id) {
        $i=1;
        foreach($this->_sites as $site) {

            if($i == $id) {
                return $site;
            }
            $i++;
        }
    }

    public function exportLayerPNGS($filename, $width, $height, $whichSite = 1, $layerId, $images_uuid) {

        $i=1;

        foreach($this->_sites as $site) {

            if($i == $whichSite) {
                break;
            }
            $i++;
        }

        $shop = Zend_Registry::get('shop');
        $searchpath = APPLICATION_PATH . '/design/clients/' . $shop['uid'] . '/fonts';
        if($shop['market']) {
            $market = Zend_Registry::get('market_shop');
            $marketSearchPath = APPLICATION_PATH . '/design/clients/' . $market['uid'] . '/fonts';
        }



        $layer = $site->getLayer();

        $images = array();
        $first = true;
        $firstImage = array();


        if($first && $layerId == 0) {
            $rgb = $site->hex2rgb($site->getBackgroundColor());

            $p = new PDFlib();
            $p->set_option("license=L900102-010014-133986-3NWC62-C4U942");
            $p->set_option("errorpolicy=return");

            $p->set_option("SearchPath=".$searchpath);
            if($shop['market']) {
                $p->set_option("SearchPath=".$marketSearchPath);
            }

            if ($p->begin_document($filename.'_back.pdf', "") == 0) {
                die("Error: " . $p->get_errmsg());
            }

            $p->set_info("Creator", "Printshop Creator");
            $p->set_info("Title", "PDF Vorschau");
            $p->set_option("textformat=utf8");

            $p->set_option("topdown=true");
            $p->begin_page_ext($site->getWidth(), $site->getHeight(), "");

            $p->setcolor("fill", "rgb", $rgb['r'], $rgb['g'], $rgb['b'], 0.0);
            /* Define a rectangle */
            $p->rect(0, $site->getHeight(), $site->getWidth(), $site->getHeight());
            $p->fill();
            $p->end_page_ext("");
            $p->end_document("");

            exec('gs -dNOPAUSE -dMaxBitmap=2147483647 -r300 -dBATCH -sDEVICE=pngalpha -sOutputFile="'.$filename.'_back.png" "'.$filename.'_back.pdf"');
            //$image = new imagick($filename.'_back.png');
            //$image->paintTransparentImage('#FEFEFE', 0.0, 10);
            //$image->resizeImage(($width*2), ($height*2), Imagick::FILTER_LANCZOS,0,true);
            exec('/usr/bin/convert -transparent "#FEFEFE" -resize '.($width*2).'x'.($height*2).' "'.$filename.'_back.png" "'.$filename.'_back.png"');
            //$image->writeimage($filename.'_back.png');
            $image = new imagick($filename.'_back.png');
            $cor = $image->getImagePage();


            $displayLayouterTrim = array();
            $displayLayouterTrim['left'] = 0;
            $displayLayouterTrim['top'] = 0;
            $displayLayouterTrim['right'] = 0;
            $displayLayouterTrim['bottom'] = 0;
            if($site->getRenderLayouterTrim()) {
                $displayLayouterTrim['left'] = $cor['width']/2/100*($site->getTrimLeft()*100/$site->getWidth());
                $displayLayouterTrim['top'] = $cor['height']/2/100*($site->getTrimTop()*100/$site->getHeight());
                $displayLayouterTrim['right'] = $cor['width']/2/100*($site->getTrimRight()*100/$site->getWidth());
                $displayLayouterTrim['bottom'] = $cor['height']/2/100*($site->getTrimBottom()*100/$site->getHeight());
            }

            $images[] = array('key' => 0, 'src' => '_back.png', 'scale' => 1, 'rotate' => 0, 'onlypdf' => 0, 'ispassepartout' => false, 'fit' => 0, 'isclip' => false, 'editable' => false, 'isresizeable' => false, 'iszoomable' => false, 'ismoveable' => false, 'name' => '', 'basewidth' => ($cor['width']/2), 'baseheight' => ($cor['height']/2), 'x' => $cor['x'], 'y' => $cor['y'], 'width' => ($image->getImageWidth()/2), 'height' => ($image->getImageHeight()/2), 'displayTrim' => $displayLayouterTrim);


            $first = false;
        }else{
            $first = false;
        }

        $hasText = false;

        foreach($layer as $key => $rows) {

            if($key != $layerId) {
                continue;
            }

            $p = new PDFlib();
            $p->set_option("license=L900102-010014-133986-3NWC62-C4U942");
            $p->set_option("errorpolicy=return");

            $p->set_option("SearchPath=".$searchpath);
            if($shop['market']) {
                $p->set_option("SearchPath=".$marketSearchPath);
            }

            if ($p->begin_document($filename.'_'.$key.'.pdf', "") == 0) {
                die("Error: " . $p->get_errmsg());
            }

            $p->set_info("Creator", "Printshop Creator");
            $p->set_info("Title", "PDF Vorschau");
            $p->set_option("textformat=utf8");

            $sites = $site->buildPDFLayer($p, $rows, $first);

            $p->end_document("");
            $p->delete();
            unset($p);

            exec('gs -dNOPAUSE -dMaxBitmap=2147483647 -r300 -dBATCH -sDEVICE=png16m -sOutputFile="'.$filename.'_'.$key.'.png" "'.$filename.'_'.$key.'.pdf"');
            exec('/usr/bin/convert -transparent "#fffffd" -crop 100%x100%+0+1 -resize '.($width*2).'x'.($height*2).' "'.$filename.'_'.$key.'.png" "'.$filename.$key.'.png"');

            if(!$first) {
                exec('/usr/bin/convert -background transparent -border 1 -bordercolor transparent -trim "'.$filename.$key.'.png" "'.$filename.$key.'.png"');
            }
            $first=false;
            $image = new imagick($filename.$key.'.png');
            $cor = $image->getImagePage();

            $x = $cor['x']/2-1;
            $y = $cor['y']/2-1;
            $width = $cor['width']/2;
            $height = $cor['height']/2;

            $displayLayouterTrim = array();
            $displayLayouterTrim['left'] = 0;
            $displayLayouterTrim['top'] = 0;
            $displayLayouterTrim['right'] = 0;
            $displayLayouterTrim['bottom'] = 0;
            if($site->getRenderLayouterTrim()) {
                $displayLayouterTrim['left'] = $width/100*($site->getTrimLeft()*100/$site->getWidth());
                $displayLayouterTrim['top'] = $height/100*($site->getTrimTop()*100/$site->getHeight());
                $displayLayouterTrim['right'] = $width/100*($site->getTrimRight()*100/$site->getWidth());
                $displayLayouterTrim['bottom'] = $height/100*($site->getTrimBottom()*100/$site->getHeight());
            }

            if(isset($this->_layers[$key])) {

                $clip = false;
                $cbx = 0;
                $cby = 0;
                $cbw = 0;
                $cbh = 0;
                if(isset($this->_layers[$key]['clipbox']) && $this->_layers[$key]['clipbox'] != "") {
                    $clipbox = explode(" ", $this->_layers[$key]['clipbox']);

                    $cbx = ($cor['width']/2/100)*($clipbox[0]*100/$site->getWidth());
                    $cby = ($cor['height']/2/100)*($clipbox[1]*100/$site->getHeight());
                    $cbw = ($cor['width']/2/100)*($clipbox[2]*100/$site->getWidth());
                    $cbh = ($cor['height']/2/100)*($clipbox[3]*100/$site->getHeight());

                    $clip = true;
                }
                $editable = false;
                if($this->_layers[$key]['scaleable'] || $this->_layers[$key]['dragable'] || $this->_layers[$key]['rotateable']) {
                    $editable = true;
                }
                if(Zend_Registry::isRegistered("steplayouter_rte")) {
                    $this->_layers[$key]['scaleable'] = false;
                }
                $images[] = array('key' => (int)$key, 'src' => $key.'.png', 'scale' => $this->_layers[$key]['scale'], 'onlypdf' => $this->_layers[$key]['onlypdf'], 'ispassepartout' => $this->_layers[$key]['ispassepartout'], 'rotate' => $this->_layers[$key]['rotate'], 'fit' => $this->_layers[$key]['fit'], 'isclip' => $clip, 'clipx' => $cbx, 'clipy' => $cby, 'clipw' => $cbw, 'cliph' => $cbh, 'editable' => $editable, 'isscaleable' => $this->_layers[$key]['scaleable'], 'isrotateable' => $this->_layers[$key]['rotateable'], 'isdragable' => $this->_layers[$key]['dragable'], 'name' => $this->_layers[$key]['name'], 'basewidth' => $width, 'baseheight' => $height, 'x' => $x, 'y' => $y, 'width' => ($image->getImageWidth()/2), 'height' => ($image->getImageHeight()/2), 'displayTrim' => $displayLayouterTrim);
            }else{
                $images[] = array('key' => (int)$key, 'src' => $key.'.png', 'scale' => 1, 'rotate' => 0, 'fit' => 0, 'onlypdf' => 0, 'ispassepartout' => false,  'isclip' => false, 'editable' => false, 'isscaleable' => false, 'isrotateable' => false, 'isdragable' => false, 'name' => '', 'basewidth' => $width, 'baseheight' => $height, 'x' => $x, 'y' => $y, 'width' => ($image->getImageWidth()/2), 'height' => ($image->getImageHeight()/2));
            }

        }


        usort($images, function($a,$b) {
            return ($a < $b) ? -1 : 1;
        });


        return $images;
        /*$images = array();

        for($i = 0; $i < count($this->_sites); $i++)
        {
            $image = new Imagick($filename.'.pdf['.$i.']');
            $image->thumbnailimage($width, $height, true);
            $image->writeimage($filename.$i.'.png');

            $images[] = $i.'.png';
        }
        return $images;
        */
    }

    public function exportLayerImageNames($filename, $width, $height, $whichSite = 1) {

        $i=1;

        foreach($this->_sites as $site) {

            if($i == $whichSite) {
                break;
            }
            $i++;
        }

        $shop = Zend_Registry::get('shop');

        if($shop['market']) {
            $shop = Zend_Registry::get('market_shop');
        }
        $searchpath = APPLICATION_PATH . '/design/clients/' . $shop['uid'] . '/fonts';
        $layer = $site->getLayer();

        $images = array();
        $first = true;
        $firstImage = array();


        if($first) {

            $images[] = array('key' => 0, 'src' => '_back.png');


            $first = false;
        }

        $hasText = false;

        foreach($layer as $key => $rows) {
            $images[] = array('key' => (int)$key, 'src' => $key.'.png');
        }


        usort($images, function($a,$b) {
            return ($a < $b) ? -1 : 1;
        });


        return $images;
        /*$images = array();

        for($i = 0; $i < count($this->_sites); $i++)
        {
            $image = new Imagick($filename.'.pdf['.$i.']');
            $image->thumbnailimage($width, $height, true);
            $image->writeimage($filename.$i.'.png');

            $images[] = $i.'.png';
        }
        return $images;
        */
    }

    public function buildProductionPDF($filename, $whichSite = 2) {
        $shop = Zend_Registry::get('shop');

        Zend_Registry::set("production", 1);

        $searchpath = APPLICATION_PATH . '/design/clients/' . $shop['uid'] . '/fonts';
        if($shop['market']) {
            $market = Zend_Registry::get('market_shop');
            $marketSearchPath = APPLICATION_PATH . '/design/clients/' . $market['uid'] . '/fonts';
        }

        if(!$marketSearchPath) {
            $marketSearchPath = APPLICATION_PATH . '/design/clients/0001-4d1421cd-4f8ec189-8e3d-041a0e2f/fonts';
        }




        $p = new PDFlib();
        $p->set_parameter("license", "L900102-010014-133986-3NWC62-C4U942");
        $p->set_parameter("errorpolicy", "return");


        if ($p->begin_document($filename.'.pdf', "") == 0) {
            die("Error: " . $p->get_errmsg());
        }

        $p->set_info("Creator", "Printshop Creator");
        $p->set_info("Title", "PDF Vorschau");
        $p->set_parameter("textformat", "utf8");

        $p->set_parameter("SearchPath", APPLICATION_PATH . '/design/clients/0001-4d1421cd-4f8ec189-8e3d-041a0e2f/fonts');
        $p->set_parameter("SearchPath", APPLICATION_PATH . '/design/clients/3214981973/fonts');

        $p->set_parameter("SearchPath", $searchpath);



        $i = 1;
        foreach($this->_sites as $site) {
            if($i == $whichSite || $whichSite == false) {
                $site->buildPDFLibProduction($p, $this->_layers);
            }

            $i++;

        }

        $p->end_document("");

        if($site->getCutX()) {
            exec('java -jar /opt/lampp/htdocs/trunk/library/croppages/CropPages.jar cut "' . $filename.'.pdf" "' . $filename.'_cut.pdf" "' . ($site->getCutWidth()-4) . '" "' . ($site->getCutHeight()-4) . '" "' . ($site->getCutX()) . '" "' . ($site->getCutY()*-1) . '" 0');
            rename($filename.'_cut.pdf', $filename.'.pdf');
        }
    }

    public function buildPreviewPDF($filename) {
        $shop = Zend_Registry::get('shop');
        $searchpath = APPLICATION_PATH . '/design/clients/' . $shop['uid'] . '/fonts';
        if($shop['market']) {
            $market = Zend_Registry::get('market_shop');
            $marketSearchPath = APPLICATION_PATH . '/design/clients/' . $market['uid'] . '/fonts';
        }

        Zend_Registry::set("preview", 1);

        $p = new PDFlib();
        $p->set_parameter("license", "L900102-010014-133986-3NWC62-C4U942");
        $p->set_parameter("errorpolicy", "return");

        $optlist = "masterpassword=BoonkerzWichtig1 permissions={" .
            "nocopy noaccessible noassemble}";

        if ($p->begin_document($filename.'.pdf', $optlist) == 0) {
            die("Error: " . $p->get_errmsg());
        }

        $p->set_info("Creator", "Printshop Creator");
        $p->set_info("Title", "PDF Vorschau");
        $p->set_parameter("textformat", "utf8");

        $p->set_parameter("SearchPath", $searchpath);
        $p->set_parameter("SearchPath", APPLICATION_PATH . '/design/clients/0001-4d1421cd-4f8ec189-8e3d-041a0e2f/fonts');
        $p->set_parameter("SearchPath", APPLICATION_PATH . '/design/clients/3214981973/fonts');


        foreach($this->_sites as $site) {
            $site->buildPDFLibPreview($p, $this->_layers);
        }

        $p->end_document("");
    }

    public function buildPDFLib($filename, $whichSite = 1) {
        $shop = Zend_Registry::get('shop');

        if($shop['market']) {
            $shop = Zend_Registry::get('market_shop');
        }
        if(Zend_Registry::isRegistered("font_path")) {
            $searchpath = Zend_Registry::get('font_path');
        }else{
            $searchpath = APPLICATION_PATH . '/design/clients/' . $shop['uid'] . '/fonts';
        }


        $p = new PDFlib();
        $p->set_parameter("license", "L900102-010014-133986-3NWC62-C4U942");
        $p->set_parameter("errorpolicy", "return");


        if ($p->begin_document($filename.'.pdf', "") == 0) {
            die("Error: " . $p->get_errmsg());
        }

        $p->set_info("Creator", "Printshop Creator");
        $p->set_info("Title", "PDF Vorschau");
        $p->set_parameter("textformat", "utf8");

        $p->set_parameter("SearchPath", $searchpath);
        $p->set_parameter("SearchPath", APPLICATION_PATH . '/design/clients/0001-4d1421cd-4f8ec189-8e3d-041a0e2f/fonts');
        $p->set_parameter("SearchPath", APPLICATION_PATH . '/design/clients/3214981973/fonts');

        $i = 1;


        foreach($this->_sites as $site) {
            if($i == $whichSite || $whichSite == false) {
                $site->buildPDFLib($p, $this->_layers);
            }
            $i++;
        }

        $p->end_document("");
    }
}