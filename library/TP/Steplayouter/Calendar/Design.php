<?php

class TP_Steplayouter_Calendar_Design {

    private $_font_family = "Helvetica";

    private $_x = 0;

    private $_y = 0;

    private $monthColor = "white";

    private $monthSize = false;

    private $daySize = false;

    private $siteBackground = false;

    private $monthBackColor = "#e7a020";

    private $dayNamesColor = "black";

    private $weekNamesColor = "black";

    private $dayColor = "black";

    private $holidayColor = "black";

    private $vacationColor = "red";

    private $holidayBackColor = "red";

    private $sunBackColor = "red";

    private $sunColor = "red";

    private $samColor = "red";

    private $_start_month = 1;

    private $_start_year = 2014;

    private $_height = 400;

    private $_width = 400;

    private $_year = 2012;

    private $_month = 12;

    private $_print_holiday = false;

    private $_print_vacation = false;

    private $_print_region = false;

    private $_font_family_days = false;

    public function getDayNamesColor()
    {
        return $this->dayNamesColor;
    }

    public function setDayNamesColor($dayNamesColor)
    {
        $this->dayNamesColor = $dayNamesColor;
    }

    public function getDayColor()
    {
        return $this->dayColor;
    }

    public function setDayColor($dayColor)
    {
        $this->dayColor = $dayColor;
    }

    public function getVacationColor()
    {
        return $this->vacationColor;
    }

    public function setVacationColor($vacationColor)
    {
        $this->vacationColor = $vacationColor;
    }

    public function getHolidayColor()
    {
        return $this->holidayColor;
    }

    public function setHolidayColor($holidayColor)
    {
        $this->holidayColor = $holidayColor;
    }

    public function getSunBackColor()
    {
        return $this->sunBackColor;
    }

    public function setSunBackColor($sunBackColor)
    {
        $this->sunBackColor = $sunBackColor;
    }

    public function getHolidayBackColor()
    {
        return $this->holidayBackColor;
    }

    public function setHolidayBackColor($holidayBackColor)
    {
        $this->holidayBackColor = $holidayBackColor;
    }

    public function getMonthColor()
    {
        return $this->monthColor;
    }

    public function setMonthColor($monthColor)
    {
        $this->monthColor = $monthColor;
    }

    public function getSunColor()
    {
        return $this->sunColor;
    }

    public function setSunColor($sunColor)
    {
        $this->sunColor = $sunColor;
    }

    public function getFontFamily()
    {
        return $this->_font_family;
    }

    public function setFontFamily($font_family)
    {
        $this->_font_family = $font_family;
    }

    public function getStartMonth()
    {
        return $this->_start_month;
    }

    public function setStartMonth($start_month)
    {
        $this->_start_month = $start_month;
    }

    public function getStartYear()
    {
        return $this->_start_year;
    }

    public function setStartYear($start_year)
    {
        $this->_start_year = $start_year;
    }

    public function getHeight()
    {
        return $this->_height;
    }

    public function setHeight($height)
    {
        $this->_height = $height;
    }

    public function getWidth()
    {
        return $this->_width;
    }

    public function setWidth($width)
    {
        $this->_width = $width;
    }

    public function getYear()
    {
        return $this->_year;
    }

    public function setYear($year)
    {
        $this->_year = $year;
    }

    public function getMonth()
    {
        return $this->_month;
    }

    public function setMonth($month)
    {
        $this->_month = $month;
    }

    public function getPrintHoliday()
    {
        return $this->_print_holiday;
    }

    public function setPrintHoliday($print_holiday)
    {
        $this->_print_holiday = $print_holiday;
    }

    public function getPrintVacation()
    {
        return $this->_print_vacation;
    }

    public function setPrintVacation($print_vacation)
    {
        $this->_print_vacation = $print_vacation;
    }

    public function getPrintRegion()
    {
        return $this->_print_region;
    }

    public function setPrintRegion($print_region)
    {
        $this->_print_region = $print_region;
    }

    public function getY()
    {
        return $this->_y;
    }

    public function setY($y)
    {
        $this->_y = $y;
    }

    public function getX()
    {
        return $this->_x;
    }

    public function setX($x)
    {
        $this->_x = $x;
    }

    public function getWeekNamesColor()
    {
        return $this->weekNamesColor;
    }

    public function setWeekNamesColor($weekNamesColor)
    {
        $this->weekNamesColor = $weekNamesColor;
    }

    public function getMonthBackColor()
    {
        return $this->monthBackColor;
    }

    public function setMonthBackColor($monthBackColor)
    {
        $this->monthBackColor = $monthBackColor;
    }

    public function getSamColor()
    {
        return $this->samColor;
    }

    public function setSamColor($samColor)
    {
        $this->samColor = $samColor;
    }

    /**
     * @return string
     */
    public function getSiteBackground()
    {
        return $this->siteBackground;
    }

    /**
     * @param string $siteBackground
     */
    public function setSiteBackground($siteBackground)
    {
        $this->siteBackground = $siteBackground;
    }

    /**
     * @return boolean
     */
    public function getMonthSize()
    {
        return $this->monthSize;
    }

    /**
     * @param boolean $monthSize
     */
    public function setMonthSize($monthSize)
    {
        $this->monthSize = $monthSize;
    }

    /**
     * @return boolean
     */
    public function getDaySize()
    {
        return $this->daySize;
    }

    /**
     * @param boolean $daySize
     */
    public function setDaySize($daySize)
    {
        $this->daySize = $daySize;
    }

    /**
     * @return boolean
     */
    public function getFontFamilyDays()
    {
        if(!$this->_font_family_days) {
            return $this->_font_family;
        }
        return $this->_font_family_days;
    }

    /**
     * @param boolean $font_family_days
     */
    public function setFontFamilyDays($font_family_days)
    {
        $this->_font_family_days = $font_family_days;
    }
}