<?php

class TP_Steplayouter_Calendar_Template_5 {

    protected $_pdf = false;

    /**
     * @var TP_Steplayouter_Calendar_Design
     */
    protected $_design = false;

    /**
     * @var bool
     */
    protected $_layers = false;

    public function __construct($pdf, $design, $layers) {
        $this->_pdf = $pdf;
        $this->_design = $design;
        $this->_layers = $layers;
    }


    public function render() {

        $install = Zend_Registry::get('install');
        if($install['vacation'] != "") {
            $ferien = simplexml_load_string($install['vacation']);
        }
        require_once("Date/Holidays.php");

        switch($this->getDesign()->getPrintRegion()) {
            case "BW":
                require_once("Date/Holidays/Filter/Germany/BadenWuerttemberg.php");
                $region = new Date_Holidays_Filter_Germany_BadenWuerttemberg();
                $ferien = $ferien->bw;
                break;
            case "BY":
                require_once("Date/Holidays/Filter/Germany/Bavaria.php");
                $region = new Date_Holidays_Filter_Germany_Bavaria();
                $ferien = $ferien->by;
                break;
            case "BE":
                require_once("Date/Holidays/Filter/Germany/Berlin.php");
                $region = new Date_Holidays_Filter_Germany_Berlin();
                $ferien = $ferien->be;
                break;
            case "BB":
                require_once("Date/Holidays/Filter/Germany/Brandenburg.php");
                $region = new Date_Holidays_Filter_Germany_Brandenburg();
                $ferien = $ferien->bb;
                break;
            case "HB":
                require_once("Date/Holidays/Filter/Germany/Bremen.php");
                $region = new Date_Holidays_Filter_Germany_Bremen();
                $ferien = $ferien->hb;
                break;
            case "HH":
                require_once("Date/Holidays/Filter/Germany/Hamburg.php");
                $region = new Date_Holidays_Filter_Germany_Hamburg();
                $ferien = $ferien->hh;
                break;
            case "HE":
                require_once("Date/Holidays/Filter/Germany/Hesse.php");
                $region = new Date_Holidays_Filter_Germany_Hesse();
                $ferien = $ferien->he;
                break;
            case "MV":
                require_once("Date/Holidays/Filter/Germany/MecklenburgWesternPomerania.php");
                $region = new Date_Holidays_Filter_Germany_MecklenburgWesternPomerania();
                $ferien = $ferien->mv;
                break;
            case "NI":
                require_once("Date/Holidays/Filter/Germany/LowerSaxony.php");
                $region = new Date_Holidays_Filter_Germany_LowerSaxony();
                $ferien = $ferien->ni;
                break;
            case "NW":
                require_once("Date/Holidays/Filter/Germany/NorthRhineWestphalia.php");
                $region = new Date_Holidays_Filter_Germany_NorthRhineWestphalia();
                $ferien = $ferien->nw;
                break;
            case "RP":
                require_once("Date/Holidays/Filter/Germany/RhinelandPalatinate.php");
                $region = new Date_Holidays_Filter_Germany_RhinelandPalatinate();
                $ferien = $ferien->rp;
                break;
            case "SL":
                require_once("Date/Holidays/Filter/Germany/Saarland.php");
                $region = new Date_Holidays_Filter_Germany_Saarland();
                $ferien = $ferien->sl;
                break;
            case "SN":
                require_once("Date/Holidays/Filter/Germany/Saxony.php");
                $region = new Date_Holidays_Filter_Germany_Saxony();
                $ferien = $ferien->sn;
                break;
            case "ST":
                require_once("Date/Holidays/Filter/Germany/SaxonyAnhalt.php");
                $region = new Date_Holidays_Filter_Germany_SaxonyAnhalt();
                $ferien = $ferien->st;
                break;
            case "SH":
                require_once("Date/Holidays/Filter/Germany/SchleswigHolstein.php");
                $region = new Date_Holidays_Filter_Germany_SchleswigHolstein();
                $ferien = $ferien->sh;
                break;
            case "TH":
                require_once("Date/Holidays/Filter/Germany/Thuringia.php");
                $region = new Date_Holidays_Filter_Germany_Thuringia();
                $ferien = $ferien->th;
                break;
            case "0":
            default:
                require_once("Date/Holidays/Filter/Germany/Official.php");
                $region = new Date_Holidays_Filter_Germany_Official();
                break;
        }

        $font = $this->_pdf->load_font($this->getDesign()->getFontFamilyDays(), "unicode", "embedding fontstyle=normal");

        $fontBold = $this->_pdf->load_font($this->getDesign()->getFontFamily(), "unicode", "embedding fontstyle=normal");

        $startDate = new DateTime($this->getDesign()->getStartYear(). '-' . $this->getDesign()->getStartMonth() . '-01', new DateTimeZone('Europe/Berlin'));
        $this->getDesign()->setMonth(($this->getDesign()->getMonth()-1));
        if($this->getDesign()->getMonth() > 0) {
            $startDate->modify("+".$this->getDesign()->getMonth()." month");
        }
        $maxdays = cal_days_in_month(CAL_GREGORIAN, $startDate->format("m"), $startDate->format("Y"));

        $row = 1;

        $tbl = 0;

        $colWidth = round($this->getDesign()->getWidth() / 10);
        Zend_Registry::get('log')->debug($colWidth);

        setlocale(LC_TIME, "de_DE.UTF8");


        $holidays = &Date_Holidays::factory('Germany', $this->getDesign()->getStartYear(), 'de_DE');

        $enddate = clone($startDate);
        $enddate->modify("+1month");
        $enddate->modify("-1day");

        if($this->getDesign()->getMonthColor() == "white") {
            $this->getDesign()->setMonthColor("black");
        }

        $holidays_titles = $holidays->getHolidaysForDatespan($startDate->getTimestamp(), $enddate->getTimestamp(), $region);

        $tlcell_opts = "fittextline={font=" . $fontBold ." fillcolor={".$this->getDesign()->getMonthColor()."} fontsize=20 position={center center}} " .
            " rowheight=20" .
            " margin=4";

        $tbl = $this->_pdf->add_table_cell($tbl, 1, $row, "", $tlcell_opts);

        $tlcell_opts = "fittextline={font=" . $fontBold ." fillcolor={".$this->getDesign()->getMonthColor()."} fontsize=20 position={center center}} " .
            " rowheight=20" .
            " margin=4";

        $tbl = $this->_pdf->add_table_cell($tbl, 2, $row, "", $tlcell_opts);

        $tlcell_opts = "fittextline={font=" . $fontBold ." fillcolor={".$this->getDesign()->getMonthColor()."} fontsize=35 position={center center}} " .
            " rowheight=20 colspan=8" .
            " margin=4 matchbox={fillcolor={".$this->getDesign()->getSiteBackground()."}}";


        $tbl = $this->_pdf->add_table_cell($tbl, 3, $row, strftime("%B", $startDate->getTimestamp()), $tlcell_opts);

        $tlcell_opts = "fittextline={font=" . $fontBold ." fillcolor={".$this->getDesign()->getMonthColor()."} fontsize=20 position={center center}} " .
            " rowheight=20" .
            " margin=4";

        $tbl = $this->_pdf->add_table_cell($tbl, 11, $row, "", $tlcell_opts);

        $tlcell_opts = "fittextline={font=" . $fontBold ." fillcolor={".$this->getDesign()->getMonthColor()."} fontsize=20 position={center center}} " .
            " rowheight=20" .
            " margin=4";

        $tbl = $this->_pdf->add_table_cell($tbl, 12, $row, "", $tlcell_opts);

        $row++;

        $tlcell_opts = "fittextline={font=" . $fontBold ." fillcolor={".$this->getDesign()->getMonthColor()."} fontsize=20 position={center center}} " .
            " rowheight=20 colspan=12" .
            " margin=4";

        $tbl = $this->_pdf->add_table_cell($tbl, 1, $row, "", $tlcell_opts);

        $row++;

         $stroke_opts = " fill={";

        for ($i = 1; $i <= ($maxdays); $i++) {

            $color = " fillcolor={".$this->getDesign()->getDayColor()."}";

            $fillColor ="";
            if($i % 2 == 0) {


            }

            if(strftime("%a", $startDate->getTimestamp()) == "So") {
                $color = " fillcolor={".$this->getDesign()->getSunColor()."}";
                $stroke_opts .= " {area=row" . ($row) . " fillcolor={#eeeeee}}";
            }

            $tlcell_opts = "fittextline={font=" . $font . $color . " fontsize=20 position={center center}} " .
            " colwidth=" . $colWidth . " rowheight=18" .
            " margin=4 rowjoingroup=group".$i." ".$fillColor;


            $tbl = $this->_pdf->add_table_cell($tbl, 1, $row, $i, $tlcell_opts);

            $tlcell_opts = "fittextline={font=" . $fontBold . $color . " fontsize=20 position={center center}} " .
                " colwidth=" . $colWidth . " rowheight=18" .
                " margin=4 rowjoingroup=group".$i." ".$fillColor;


            $tbl = $this->_pdf->add_table_cell($tbl, 2, $row, strftime("%a", $startDate->getTimestamp()), $tlcell_opts);

            $tlcell_opts = "fittextline={font=" . $font . $color . " fontsize=20 position={center center}} " .
                " colwidth=" . $colWidth . " rowheight=18" .
                " margin=4 rowjoingroup=group".$i." ".$fillColor;


            $tbl = $this->_pdf->add_table_cell($tbl, 3, $row, "", $tlcell_opts);

            $tbl = $this->_pdf->add_table_cell($tbl, 4, $row, "", $tlcell_opts);
            $tbl = $this->_pdf->add_table_cell($tbl, 5, $row, "", $tlcell_opts);
            $tbl = $this->_pdf->add_table_cell($tbl, 6, $row, "", $tlcell_opts);
            $tbl = $this->_pdf->add_table_cell($tbl, 7, $row, "", $tlcell_opts);
            $tbl = $this->_pdf->add_table_cell($tbl, 8, $row, "", $tlcell_opts);
            $tbl = $this->_pdf->add_table_cell($tbl, 9, $row, "", $tlcell_opts);
            $tbl = $this->_pdf->add_table_cell($tbl, 10, $row, "", $tlcell_opts);
            $tbl = $this->_pdf->add_table_cell($tbl, 11, $row, "", $tlcell_opts);

            if(strftime("%a", $startDate->getTimestamp()) == "Mo") {
                $tlcell_opts = "fittextline={font=" . $font . $color . " fontsize=20 position={right center}} " .
                    " colwidth=" . $colWidth . " rowheight=18" .
                    " margin=4 rowjoingroup=group".$i." ".$fillColor;

                $tbl = $this->_pdf->add_table_cell($tbl, 12, $row, strftime("%W", $startDate->getTimestamp()), $tlcell_opts);
            }else{
                $tbl = $this->_pdf->add_table_cell($tbl, 12, $row, "", $tlcell_opts);
            }


            $startDate->modify("+1day");
            $row++;

            $tlcell_opts = "" .
                " rowheight=0.5" .
                " colspan=12 matchbox={fillcolor={#444444}} rowjoingroup=group".$i;

            $tbl = $this->_pdf->add_table_cell($tbl, 1, $row, "", $tlcell_opts);

            $row++;
        }

        $tlcell_opts = "fittextline={font=" . $fontBold ." fillcolor={".$this->getDesign()->getMonthColor()."} fontsize=20 position={center center}} " .
            " rowheight=18 colspan=12" .
            " margin=4";

        $tbl = $this->_pdf->add_table_cell($tbl, 1, $row, "", $tlcell_opts);

        $row++;

        $tlcell_opts = "fittextline={font=" . $fontBold ." fillcolor={#555555} fontsize=20 position={left center}} " .
            "rowheight=18 colspan=12" .
            " margin=2";

        foreach($holidays_titles as $holiday) {
            $tbl = $this->_pdf->add_table_cell($tbl, 1, $row, $holiday->getDate()->format("%d.%m") ." " . $holiday->getTitle(), $tlcell_opts);
            $row++;
        }

        $stroke_opts .= "}";

        $fittab_opts = "header=0 " .$stroke_opts;

        $result = $this->_pdf->fit_table($tbl, $this->getDesign()->getX(), $this->getDesign()->getY(), $this->getDesign()->getWidth(), $this->getDesign()->getHeight()+$this->getDesign()->getY(), $fittab_opts);

    }

    public function getDesign()
    {
        return $this->_design;
    }

    public function setDesign($design)
    {
        $this->_design = $design;
    }

}