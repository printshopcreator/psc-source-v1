<?php

class TP_Steplayouter_Calendar_Template_6 {

    protected $_pdf = false;

    /**
     * @var TP_Steplayouter_Calendar_Design
     */
    protected $_design = false;

    /**
     * @var bool
     */
    protected $_layers = false;

    public function __construct($pdf, $design, $layers) {
        $this->_pdf = $pdf;
        $this->_design = $design;
        $this->_layers = $layers;
    }


    public function render() {

        $install = Zend_Registry::get('install');
        if($install['vacation'] != "") {
            $ferien = simplexml_load_string($install['vacation']);
        }
        require_once("Date/Holidays.php");

        switch($this->getDesign()->getPrintRegion()) {
            case "BW":
                require_once("Date/Holidays/Filter/Germany/BadenWuerttemberg.php");
                $region = new Date_Holidays_Filter_Germany_BadenWuerttemberg();
                $ferien = $ferien->bw;
                break;
            case "BY":
                require_once("Date/Holidays/Filter/Germany/Bavaria.php");
                $region = new Date_Holidays_Filter_Germany_Bavaria();
                $ferien = $ferien->by;
                break;
            case "BE":
                require_once("Date/Holidays/Filter/Germany/Berlin.php");
                $region = new Date_Holidays_Filter_Germany_Berlin();
                $ferien = $ferien->be;
                break;
            case "BB":
                require_once("Date/Holidays/Filter/Germany/Brandenburg.php");
                $region = new Date_Holidays_Filter_Germany_Brandenburg();
                $ferien = $ferien->bb;
                break;
            case "HB":
                require_once("Date/Holidays/Filter/Germany/Bremen.php");
                $region = new Date_Holidays_Filter_Germany_Bremen();
                $ferien = $ferien->hb;
                break;
            case "HH":
                require_once("Date/Holidays/Filter/Germany/Hamburg.php");
                $region = new Date_Holidays_Filter_Germany_Hamburg();
                $ferien = $ferien->hh;
                break;
            case "HE":
                require_once("Date/Holidays/Filter/Germany/Hesse.php");
                $region = new Date_Holidays_Filter_Germany_Hesse();
                $ferien = $ferien->he;
                break;
            case "MV":
                require_once("Date/Holidays/Filter/Germany/MecklenburgWesternPomerania.php");
                $region = new Date_Holidays_Filter_Germany_MecklenburgWesternPomerania();
                $ferien = $ferien->mv;
                break;
            case "NI":
                require_once("Date/Holidays/Filter/Germany/LowerSaxony.php");
                $region = new Date_Holidays_Filter_Germany_LowerSaxony();
                $ferien = $ferien->ni;
                break;
            case "NW":
                require_once("Date/Holidays/Filter/Germany/NorthRhineWestphalia.php");
                $region = new Date_Holidays_Filter_Germany_NorthRhineWestphalia();
                $ferien = $ferien->nw;
                break;
            case "RP":
                require_once("Date/Holidays/Filter/Germany/RhinelandPalatinate.php");
                $region = new Date_Holidays_Filter_Germany_RhinelandPalatinate();
                $ferien = $ferien->rp;
                break;
            case "SL":
                require_once("Date/Holidays/Filter/Germany/Saarland.php");
                $region = new Date_Holidays_Filter_Germany_Saarland();
                $ferien = $ferien->sl;
                break;
            case "SN":
                require_once("Date/Holidays/Filter/Germany/Saxony.php");
                $region = new Date_Holidays_Filter_Germany_Saxony();
                $ferien = $ferien->sn;
                break;
            case "ST":
                require_once("Date/Holidays/Filter/Germany/SaxonyAnhalt.php");
                $region = new Date_Holidays_Filter_Germany_SaxonyAnhalt();
                $ferien = $ferien->st;
                break;
            case "SH":
                require_once("Date/Holidays/Filter/Germany/SchleswigHolstein.php");
                $region = new Date_Holidays_Filter_Germany_SchleswigHolstein();
                $ferien = $ferien->sh;
                break;
            case "TH":
                require_once("Date/Holidays/Filter/Germany/Thuringia.php");
                $region = new Date_Holidays_Filter_Germany_Thuringia();
                $ferien = $ferien->th;
                break;
            case "0":
            default:
                $region = null;
                break;
        }

        $font = $this->_pdf->load_font($this->getDesign()->getFontFamilyDays(), "unicode", "embedding fontstyle=normal");

        $fontBold = $this->_pdf->load_font($this->getDesign()->getFontFamily(), "unicode", "embedding fontstyle=normal");

        $startDate = new DateTime($this->getDesign()->getStartYear(). '-' . $this->getDesign()->getStartMonth() . '-01', new DateTimeZone('Europe/Berlin'));
        $this->getDesign()->setMonth(($this->getDesign()->getMonth()-1));
        if($this->getDesign()->getMonth() > 0) {
            $startDate->modify("+".$this->getDesign()->getMonth()." month");
        }
        $maxdays = cal_days_in_month(CAL_GREGORIAN, $startDate->format("m"), $startDate->format("Y"));

        $row = 1;

        $tf = 0;
        $tbl = 0;
        $capheight = 8.5;



        $colWidth = round($this->getDesign()->getWidth()/ 8);

        setlocale(LC_TIME, "de_DE.UTF8");


        $holidays = &Date_Holidays::factory('Germany', $this->getDesign()->getStartYear(), 'de_DE');

        $enddate = clone($startDate);
        $enddate->modify("+1month");
        $enddate->modify("-1day");

        $holidays_titles = $holidays->getHolidaysForDatespan($startDate->getTimestamp(), $enddate->getTimestamp(), $region);
        $titles = array();

        foreach($holidays_titles as $holiday) {
            $titles[] = $holiday->getDate()->format("%d.%m") ." " . $holiday->getTitle();
        }

       

        $tlcell_opts = "fittextline={font=" . $fontBold ." fillcolor={".$this->getDesign()->getMonthColor()."} fontsize=".($this->getDesign()->getMonthSize()?$this->getDesign()->getMonthSize():60)." position={left center}} " .
            " colwidth=" . round($this->getDesign()->getWidth() / 2) . " rowheight=30 colspan=7" .
            " margin=4";

        /* Add the text line cell */
        $tbl = $this->_pdf->add_table_cell($tbl, 1, $row, strftime("%B", $startDate->getTimestamp()), $tlcell_opts);

        $titles = array();

        foreach($ferien->children() as $f) {
            $from = new DateTime($f['from']);
            $to = new DateTime($f['to']);
            if($from >= $startDate && $to <= $enddate) {
                $titles[] = $from->format("d.m") . "-" . $to->format("d.m"). " " . $f['de'];
            }

        }


        $row++;

        $tlcell_opts = "fittextline={font=" . $fontBold ." fillcolor={".$this->getDesign()->getMonthColor()."} fontsize=16 position={center center}} " .
            " colwidth=" . round($this->getDesign()->getWidth() / 2) . " rowheight=10 colspan=8" .
            " margin=2";

        $tbl = $this->_pdf->add_table_cell($tbl, 1, $row, "", $tlcell_opts);

        $row++;

        $color = " fillcolor={".$this->getDesign()->getWeekNamesColor()."}";
        $tlcell_opts = "fittextline={font=" . $font . $color." fontsize=11 position={center center}} " .
            " colwidth=" . $colWidth . " rowheight=20" .
            " margin=2";

        $color = " fillcolor={".$this->getDesign()->getDayColor()."}";
        $tlcell_opts = "fittextline={font=" . $font . $color." fontsize=".($this->getDesign()->getDaySize()?$this->getDesign()->getDaySize():28)." position={center center}} " .
            " colwidth=" . $colWidth . " rowheight=30" .
            " margin=4";
        $tbl = $this->_pdf->add_table_cell($tbl, 1, $row, "Mo", $tlcell_opts);
        $tbl = $this->_pdf->add_table_cell($tbl, 2, $row, "Di", $tlcell_opts);
        $tbl = $this->_pdf->add_table_cell($tbl, 3, $row, "Mi", $tlcell_opts);
        $tbl = $this->_pdf->add_table_cell($tbl, 4, $row, "Do", $tlcell_opts);
        $tbl = $this->_pdf->add_table_cell($tbl, 5, $row, "Fr", $tlcell_opts);
        $tbl = $this->_pdf->add_table_cell($tbl, 6, $row, "Sa", $tlcell_opts);
        $color = " fillcolor={".$this->getDesign()->getSunColor()."}";
        $tlcell_opts = "fittextline={font=" . $font . $color." fontsize=".($this->getDesign()->getDaySize()?$this->getDesign()->getDaySize():28)." position={center center}} " .
            " colwidth=" . $colWidth . " rowheight=30" .
            " margin=4";
        $tbl = $this->_pdf->add_table_cell($tbl, 7, $row, "So", $tlcell_opts);

        $row++;

        $column = 1;

        $dayoftheweek = $startDate->format("w");
        if($dayoftheweek == 0) {
            $dayoftheweek = 7;
        }
        $so = array();
        for ($i = 1; $i <= ($maxdays); $i++) {
            if($row == 4 && $i == 1) {
                $column = $dayoftheweek;
            }

            $color = " fillcolor={".$this->getDesign()->getDayColor()."}";

            if(strftime("%a", $startDate->getTimestamp()) == "So") {
                $so[] = $i;
                $color = " fillcolor={".$this->getDesign()->getSunColor()."}";
            }
            if(($holidays->isHoliday($startDate->getTimestamp(), $region) && $this->getDesign()->getPrintHoliday())) {
                $color = " fillcolor={".$this->getDesign()->getHolidayColor()."}";
            }
            $tlcell_opts = "fittextline={font=" . $font . $color . " fontsize=".($this->getDesign()->getDaySize()?$this->getDesign()->getDaySize():28)." position={center top}} " .
                " colwidth=" . $colWidth . " rowheight=".($this->getDesign()->getDaySize()?($this->getDesign()->getDaySize()):28)."" .
                " margin=6";

            /* Add the text line cell */
            if($maxdays >= $i) {
                $tbl = $this->_pdf->add_table_cell($tbl, $column, $row, $i, $tlcell_opts);
            }
            $column++;
            $startDate->modify("+1day");
            if($column >= 8) {
                $column = 1;
                $row++;
            }
        }



        $fittab_opts = "header=0";

        $result = $this->_pdf->fit_table($tbl, $this->getDesign()->getX(), $this->getDesign()->getY(), $this->getDesign()->getWidth()+$this->getDesign()->getX(), $this->getDesign()->getHeight()+$this->getDesign()->getY(), $fittab_opts);

    }

    public function getDesign()
    {
        return $this->_design;
    }

    public function setDesign($design)
    {
        $this->_design = $design;
    }

}