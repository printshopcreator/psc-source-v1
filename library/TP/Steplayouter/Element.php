<?php

abstract class TP_Steplayouter_Element {

    private $_id = "";

    private $_x = 0;

    private $_y = 0;

    private $_layer = 0;

    private $_org_layer = 0;

    private $_trim_left = 0;

    private $_trim_top = 0;

    private $_sort = 0;

    private $page_height = 0;

    private $page_width = 0;

    private $ref = false;

    private $ref_value = false;

    private $is_active = false;

    private $dirty = false;

    private $production_pdf = true;

    private $rel_y = 0;

    private $rel_x = 0;

    private $_render_success = true;

    private $_render_elements = array();

    private $_render_size_fixed = false;

    private $_render_debug = false;

    public function getId()
    {
        return $this->_id;
    }

    public function setId($id)
    {
        $this->_id = $id;
    }

    public function getX()
    {
        return $this->_x;
    }

    public function setX($x)
    {
        $this->_x = $x;
    }

    public function getY()
    {
        return $this->_y;
    }

    public function setY($y)
    {
        $this->_y = $y;
    }

    public function buildPDFLib($p, $layers = array())
    {

    }

    public function getLayer()
    {
        return $this->_layer;
    }

    public function setLayer($layer)
    {
        $this->_layer = $layer;
    }

    public function getSort() {
        return $this->_sort;
    }

    public function setSort($sort) {
        $this->_sort = $sort;
    }

    public function getRef()
    {
        return $this->ref;
    }

    public function setRef($ref)
    {
        $this->ref = $ref;
    }

    public function getRefValue()
    {
        return $this->ref_value;
    }

    public function setRefValue($ref_value)
    {
        $this->ref_value = $ref_value;
    }

    public function getDirty()
    {
        return $this->dirty;
    }

    public function setDirty($dirty)
    {
        $this->dirty = $dirty;
    }

    public function getOrgLayer()
    {
        return $this->_org_layer;
    }

    public function setOrgLayer($org_layer)
    {
        $this->_org_layer = $org_layer;
    }

    /**
     * @return boolean
     */
    public function getProductionPdf()
    {
        return $this->production_pdf;
    }

    /**
     * @param boolean $production_pdf
     */
    public function setProductionPdf($production_pdf)
    {
        $this->production_pdf = $production_pdf;
    }

    /**
     * @return int
     */
    public function getRelY()
    {
        return $this->rel_y;
    }

    /**
     * @param int $rel_y
     */
    public function setRelY($rel_y)
    {
        $this->rel_y = $rel_y;
    }

    /**
     * @return int
     */
    public function getRelX()
    {
        return $this->rel_x;
    }

    /**
     * @param int $rel_x
     */
    public function setRelX($rel_x)
    {
        $this->rel_x = $rel_x;
    }

    /**
     * @return boolean
     */
    public function getRenderSuccess()
    {
        return $this->_render_success;
    }

    /**
     * @param boolean $render_success
     */
    public function setRenderSuccess($render_success)
    {
        $this->_render_success = $render_success;
    }

    /**
     * @return array
     */
    public function getRenderElements()
    {
        return $this->_render_elements;
    }

    /**
     * @param array $render_elements
     */
    public function setRenderElements($render_elements)
    {
        $this->_render_elements = $render_elements;
    }

    /**
     * @return boolean
     */
    public function getIsActive()
    {
        return $this->is_active;
    }

    /**
     * @param boolean $is_active
     */
    public function setIsActive($is_active)
    {
        $this->is_active = $is_active;
    }

    /**
     * @return boolean
     */
    public function getRenderSizeFixed()
    {
        return $this->_render_size_fixed;
    }

    /**
     * @param boolean $render_size_fixed
     */
    public function setRenderSizeFixed($render_size_fixed)
    {
        $this->_render_size_fixed = $render_size_fixed;
    }

    /**
     * @return boolean
     */
    public function getRenderDebug()
    {
        return $this->_render_debug;
    }

    /**
     * @param boolean $render_debug
     */
    public function setRenderDebug($render_debug)
    {
        $this->_render_debug = $render_debug;
    }

    protected function hex2rgb($hex) {
        $color = array();
        $color['r'] = hexdec(substr($hex, 1, 2))/255;
        $color['g'] = hexdec(substr($hex, 3, 2))/255;
        $color['b'] = hexdec(substr($hex, 5, 2))/255;
        return $color;
    }

    public function getPageHeight() {
        return $this->page_height;
    }

    public function setPageHeight($page_height) {
        $this->page_height = $page_height;
    }

    public function getPageWidth() {
        return $this->page_width;
    }

    public function setPageWidth($page_width) {
        $this->page_width = $page_width;
    }

    public function getTrimLeft()
    {
        return $this->_trim_left;
    }

    public function setTrimLeft($trim_left)
    {
        $this->_trim_left = $trim_left;
    }

    public function getTrimTop()
    {
        return $this->_trim_top;
    }

    public function setTrimTop($trim_top)
    {
        $this->_trim_top = $trim_top;
    }

}