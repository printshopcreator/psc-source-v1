<?php

class TP_Steplayouter_Element_Calender extends TP_Steplayouter_Element
{
    private $_template = 1;

    private $_calendar_design = false;

    public function buildPDFLib($p, $layers = array()) {

        $this->getCalendarDesign()->setY($this->getY());
        $this->getCalendarDesign()->setX($this->getX());
        switch($this->getTemplate()) {
            case 1:
                $cal = new TP_Steplayouter_Calendar_Template_1($p, $this->getCalendarDesign(), $this->getLayer());
            break;
            case 2:
                $cal = new TP_Steplayouter_Calendar_Template_2($p, $this->getCalendarDesign(), $this->getLayer());
            break;
            case 3:
                $cal = new TP_Steplayouter_Calendar_Template_3($p, $this->getCalendarDesign(), $this->getLayer());
            break;
            case 4:
                $cal = new TP_Steplayouter_Calendar_Template_4($p, $this->getCalendarDesign(), $this->getLayer());
            break;
            case 5:
                $cal = new TP_Steplayouter_Calendar_Template_5($p, $this->getCalendarDesign(), $this->getLayer());
            break;
            case 6:
                $cal = new TP_Steplayouter_Calendar_Template_6($p, $this->getCalendarDesign(), $this->getLayer());
            break;
            case 7:
                $cal = new TP_Steplayouter_Calendar_Template_7($p, $this->getCalendarDesign(), $this->getLayer());
            break;
        }

        $cal->render();


    }

    /**
     * @return TP_Steplayouter_Calendar_Design
     */
    public function getCalendarDesign()
    {
        return $this->_calendar_design;
    }

    public function setCalendarDesign($calendar_design)
    {
        $this->_calendar_design = $calendar_design;
    }

    public function getTemplate()
    {
        return $this->_template;
    }

    public function setTemplate($template)
    {
        $this->_template = $template;
    }
}