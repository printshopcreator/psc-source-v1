<?php

class TP_Steplayouter_Element_Imageselect extends TP_Steplayouter_Element {


    private $_file = false;

    private $_base_dir = false;

    private $_width = 400;

    private $_height = 400;

    public function getFile()
    {
        return $this->_file;
    }

    public function setFile($file)
    {
        $this->_file = $file;
    }

    public function buildPDFLib($p, $layers = array())
    {
        Zend_Registry::get('log')->debug($this->getFile());
        if($this->getFile()) {
            $image = $p->load_image("auto", PUBLIC_PATH . '/' . $this->getBaseDir() . $this->getFile(), "honoriccprofile=false");
            if ($image == 0) {
                return;
            }
            $p->fit_image($image, $this->getX(), $this->getY()+$this->getHeight(),
                "position={top left} boxsize={".$this->getWidth()." ".$this->getHeight()."} fitmethod=meet");

            $p->close_image($image);
        }
    }

    public function getWidth()
    {
        return $this->_width;
    }

    public function setWidth($width)
    {
        $this->_width = $width;
    }

    public function getHeight()
    {
        return $this->_height;
    }

    public function setHeight($height)
    {
        $this->_height = $height;
    }

    public function getBaseDir()
    {
        return $this->_base_dir;
    }

    public function setBaseDir($base_dir)
    {
        $this->_base_dir = $base_dir;
    }

}