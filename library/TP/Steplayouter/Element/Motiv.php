<?php

class TP_Steplayouter_Element_Motiv extends TP_Steplayouter_Element {


    private $_motiv_uuid = "";

    private $_width = 400;

    private $_height = 400;

    private $_rotate = 0;

    private $_scale = 1;

    private $_crop = false;

    private $_render_print_adjust = true;

    public function getMotivUUID()
    {
        return $this->_motiv_uuid;
    }

    public function getValue() {
        return $this->getMotivUUID();
    }

    public function setMotivUUID($motiv_uuid)
    {
        $this->_motiv_uuid = $motiv_uuid;
    }

    public function buildPDFLib($p, $layers = array())
    {

        //return ;

        /* Apply the extended graphics state */


        if(!$this->getMotivUUID() || (isset($layers[$this->getLayer()]) && $layers[$this->getLayer()]['onlypdf'] == 1 && Zend_Registry::isRegistered("steplayout_preview"))) {
            return;
        }

        if($this->getCrop()) {
            $imageContainer = new TP_Uploadsession();
            $motiv = $imageContainer->getImage($this->getMotivUUID());
            $image = $p->load_image("auto", PUBLIC_PATH . '/' . $motiv['dir'], "honoriccprofile=false");
        }else{

            if(Zend_Auth::getInstance()->hasIdentity()) {
                $motiv = Doctrine_Query::create ()->from('Motiv c')->where('c.uuid = ?', array($this->getMotivUUID()))->fetchOne();

                if(Zend_Registry::isRegistered("production") && Zend_Registry::get("production") == 1) {
                    $image = $p->load_image("auto", APPLICATION_PATH . '/../market/motive/' .$motiv->file_work, "");
                }else{
                    $image = $p->load_image("auto", APPLICATION_PATH . '/../market/motive/' .$motiv->file_mid, "");
                }

            }else{
                $images = new Zend_Session_Namespace('amfmotive');

                if(!isset($images->motive[$this->getMotivUUID()])) {
                    $motiv = Doctrine_Query::create ()->from('Motiv c')->where('c.uuid = ?', array($this->getMotivUUID()))->fetchOne();
                    if(Zend_Registry::isRegistered("production") && Zend_Registry::get("production") == 1) {
                        $image = $p->load_image("auto", APPLICATION_PATH . '/../market/motive/' .$motiv->file_work, "");
                    }else{
                        $image = $p->load_image("auto", APPLICATION_PATH . '/../market/motive/' .$motiv->file_mid, "");
                    }

                }else{
                    if(Zend_Registry::isRegistered("production") && Zend_Registry::get("production") == 1) {
                        $image = $p->load_image("auto", APPLICATION_PATH . '/../market/guest/' . $images->motive[$this->getMotivUUID()]['file_work'], "");
                    }else{
                        $image = $p->load_image("auto", APPLICATION_PATH . '/../market/guest/' . $images->motive[$this->getMotivUUID()]['file_mid'], "");
                    }

                }


            }

        }
        if ($image == 0) {
            return;
        }
        $orgX = $this->getX();       //-123pt
        $orgY = $this->getY();
        $orgW = $this->getWidth();  // 595pt
        $orgH = $this->getHeight();

        $imagewidth = $p->info_image($image, "imagewidth", "");
        $imageheight = $p->info_image($image, "imageheight", "" );

        $scale = "slice";

        if($this->getWidth() > $this->getHeight() && $imagewidth < $imageheight) {
            $this->setWidth($imagewidth/$imageheight*$this->getHeight());
        }

        if($this->getWidth() < $this->getHeight() && $imagewidth > $imageheight) {
            $this->setWidth($imageheight/$imagewidth*$this->getWidth());
        }

        Zend_Registry::get("log")->debug("L:".$this->getLayer()."X:".($this->getX()));
        if($this->getScale() != 1) {


            $orgwidth = $this->getWidth();


            $orgheight = $this->getHeight(); // 400
            $this->setWidth($this->getWidth()*$this->getScale());  //833pt (1.4)
            $this->setHeight($this->getHeight()*$this->getScale()); // 560
            //$this->setX($this->getX()*$this->getScale());
            //$this->setY($this->getY()*$this->getScale());
            $orgwidth = $this->getWidth()-$orgwidth;             // 238pt
            $orgheight = $this->getHeight()-$orgheight;
            $this->setX(($this->getX())-(($orgwidth)/2)+($this->getRelX()/2));           // -242pt
            $this->setY($this->getY()-(($orgheight)/2)+($this->getRelY()/2));
            //$this->setY(($this->getY()/$this->getScale())-($orgheight/$this->getScale()/2));
            $scale = "meet";

        }


        if($this->getLayer() != 0 && isset($layers[$this->getLayer()]) && $layers[$this->getLayer()]['clipbox'] != "") {

            $clipbox = explode(" ", $layers[$this->getLayer()]['clipbox']);
            $p->save();


            $p->rect(
                $clipbox[0],
                ($clipbox[1]+$clipbox[3]),
                $clipbox[2],
                ($clipbox[3]));

            $p->clip ();


            $gstate = $p->create_gstate("opacityfill=1");
            $p->set_gstate($gstate);
            $p->translate($this->getX() + ($this->_width/2), $this->getY()+$this->_height/2);    //   174pt

            if($this->getRotate() == 0 && $this->getLayer() != 0 && isset($layers[$this->getLayer()]) && $layers[$this->getLayer()]['rotate'] != "") {
                if($layers[$this->getLayer()]['rotate'] < 0) {
                    $this->setRotate(360-$layers[$this->getLayer()]['rotate']);
                }
            }
            $p->rotate($this->getRotate());

            if(false && $this->getDirty() && $layers[$this->getLayer()]['fit'] == 1 && $this->getScale() != 1) {
                $p->fit_image($image, ($this->getWidth()/2)*-1, $this->getHeight()/2,
                    "position {center top} boxsize={".$this->getWidth()." ".($this->getHeight())."} fitmethod=".$scale);

            }else{
                
                $p->fit_image($image, ($this->getWidth()/2)*-1, $this->getHeight()/2,
                    "position {center top} boxsize={".$this->getWidth()." ".($this->getHeight())."} fitmethod=".$scale);

            }

            $p->close_image($image);

            $p->end_layer( );

            $p->restore();
        }else{
            if(Zend_Registry::isRegistered("renderpreviewlayer")) {

                if($this->getX() < 0) {
                    $this->setX(0);
                }
                if($this->getY() < 0) {
                    $this->setY(0);
                }
            }
            $p->translate($this->getX() + ($this->_width/2), $this->getY()+$this->_height/2);    //   174pt

            if($this->getRotate() == 0 && $this->getLayer() != 0 && isset($layers[$this->getLayer()]) && $layers[$this->getLayer()]['rotate'] != "") {
                if($layers[$this->getLayer()]['rotate'] < 0) {
                    $this->setRotate(360-$layers[$this->getLayer()]['rotate']);
                }
            }
            $p->rotate($this->getRotate());

            if($this->getDirty() && isset($layers[$this->getLayer()]) && $layers[$this->getLayer()]['fit'] == 1) {
                $p->fit_image($image, ($this->_width/2)*-1, round($this->_height/2),
                    "position {center top} boxsize={".$this->getWidth()." ".$this->getHeight()."} fitmethod=".$scale);
            }else{
                $p->fit_image($image, ($this->_width/2)*-1, round($this->_height/2),
                "position {center top} boxsize={".$this->getWidth()." ".$this->getHeight()."} fitmethod=meet");
            }

            $p->close_image($image);
            $p->end_layer( );
            $p->rotate($this->getRotate()*-1);
            $p->translate(($this->getX()+($this->_width/2))*-1, ($this->getY()+($this->_height/2))*-1);

        }


    }

    public function getWidth()
    {
        return $this->_width;
    }

    public function setWidth($width)
    {
        $this->_width = $width;
    }

    public function getHeight()
    {
        return $this->_height;
    }

    public function setHeight($height)
    {
        $this->_height = $height;
    }

    public function getRotate()
    {
        return $this->_rotate;
    }

    public function setRotate($rotate)
    {
        $this->_rotate = $rotate;
    }

    public function getScale() {
        return $this->_scale;
    }

    public function setScale($scale) {
        $this->_scale = $scale;
    }

    public function getCrop() {
        return $this->_crop;
    }

    public function setCrop($crop) {
        $this->_crop = $crop;
    }

    /**
     * @return boolean
     */
    public function getRenderPrintAdjust()
    {
        return $this->_render_print_adjust;
    }

    /**
     * @param boolean $render_print_adjust
     */
    public function setRenderPrintAdjust($render_print_adjust)
    {
        $this->_render_print_adjust = $render_print_adjust;
    }

}