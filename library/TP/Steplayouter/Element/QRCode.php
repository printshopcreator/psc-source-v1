<?php

class TP_Steplayouter_Element_QRCode extends TP_Steplayouter_Element {


    private $_text = false;

    private $_width = 400;

    private $_height = 400;

    private $_dynconfig = "";

    public function buildPDFLib($p, $layers = array())
    {

        //return ;

        /* Apply the extended graphics state */


        if(!$this->getText()) {
            return;
        }

        $loader = new Twig_Loader_String();
        $twig = new Twig_Environment($loader);
        $template = $twig->loadTemplate($this->getText());
        $data = $template->render(array(
            'data' => $this->_dynconfig
        ));

        TP_QRCode::generate($data, null, null, PUBLIC_PATH . '/barcodes/' . md5($data) . '.png');
        $file = PUBLIC_PATH . '/barcodes/' . md5($data) . '.png';

        $image = $p->load_image("auto", $file, "");
        $p->translate($this->getX() + ($this->_width/2), $this->getY()+$this->_height/2);

        $p->fit_image($image, ($this->_width/2)*-1, round($this->_height/2),
            "position {center top} boxsize={".$this->getWidth()." ".$this->getHeight()."} fitmethod=meet");

        $p->close_image($image);
        $p->end_layer( );
        $p->translate(($this->getX()+($this->_width/2))*-1, ($this->getY()+($this->_height/2))*-1);
    }

    public function getWidth()
    {
        return $this->_width;
    }

    public function setWidth($width)
    {
        $this->_width = $width;
    }

    public function getHeight()
    {
        return $this->_height;
    }

    public function setHeight($height)
    {
        $this->_height = $height;
    }

    /**
     * @return string
     */
    public function getText()
    {
        return $this->_text;
    }

    /**
     * @param string $text
     */
    public function setText($text)
    {
        $this->_text = $text;
    }

    public function setDynConfig($dyn_config)
    {
        $this->_dynconfig = $dyn_config;
    }

}