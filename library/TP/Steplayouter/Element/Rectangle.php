<?php

class TP_Steplayouter_Element_Rectangle extends TP_Steplayouter_Element {

    private $_width = 400;

    private $_height = 400;

    private $_fill_color = "#000000";

    public function buildPDFLib($p, $layers = array())
    {
        $rgb = $this->hex2rgb($this->getFillColor());
        $p->setcolor("fill", "rgb", $rgb['r'], $rgb['g'], $rgb['b'], 0.0);
        $p->rect($this->getX(), $this->getY(), $this->getWidth(), $this->getHeight());
        $p->fill();
    }

    public function getWidth()
    {
        return $this->_width;
    }

    public function setWidth($width)
    {
        $this->_width = $width;
    }

    public function getHeight()
    {
        return $this->_height;
    }

    public function setHeight($height)
    {
        $this->_height = $height;
    }

    public function getFillColor() {
        return $this->_fill_color;
    }

    public function setFillColor($fill_color) {
        $this->_fill_color = $fill_color;
    }

}