<?php

class TP_Steplayouter_Element_Text extends TP_Steplayouter_Element {

    private $_data = false;

    private $_font = false;

    private $_width = false;

    private $_height = false;

    private $_align = "left";

    private $_font_size = 12;

    private $_font_style = "0";

    private $_font_bold = "0";

    private $_font_italic = "0";

    private $_font_underline = "0";

    private $_font_family = "Helvetica";

    private $_font_color = 'rgb 0 0 0';

    private $_rotate = false;

    private $_lineHeight = false;

    private $_auto_break = false;

    private $_char_spacing = 0;

    private $_template = false;

    public function __construct() {
    }

    public function getData()
    {
        return $this->_data;
    }

    public function setData($data)
    {
        $this->_data = $data;
    }

    public function getFont()
    {
        return $this->_font;
    }

    public function setFont($font)
    {
        $this->_font = $font;
    }

    public function buildPDFLib($p, $layers = array())
    {
        // Fonts laden

        $loader = new Twig_Loader_String();
        $twig = new Twig_Environment($loader);

        Zend_Registry::get('log')->debug($this->getIsActive());

        if($this->getIsActive()) {
            $template = $twig->loadTemplate($this->getIsActive());
            $this->setIsActive($template->render(array(
                'value' => $this->getData(),
                'elements' => $this->getRenderElements()
            )));
            Zend_Registry::get('log')->debug($this->getIsActive());
            if(!$this->getIsActive()) {
                return;
            }

        }


        if($this->_font) {

        }else{
            if($this->getFontBold() && $this->getFontItalic()) {
                $font = $p->load_font($this->getFontFamily(), "unicode", "fontstyle=bolditalic");
            }elseif($this->getFontItalic()) {
                $font = $p->load_font($this->getFontFamily(), "unicode", "fontstyle=italic");
            }elseif($this->getFontBold()) {
                $font = $p->load_font($this->getFontFamily(), "unicode", "fontstyle=bold");
            }else{
                $font = $p->load_font($this->getFontFamily(), "unicode", "fontstyle=normal");
            }

            if( $font == -1 || $font == 0 ) {
                $font = $p->load_font("Helvetica", "unicode", "fontstyle=normal");
            }
        }

        $template = $twig->loadTemplate($this->getFontColor());
        $this->setFontColor($template->render(array(
            'elements' => $this->getRenderElements()
        )));

        $template = $twig->loadTemplate($this->getY());
        $this->setY($template->render(array(
            'elements' => $this->getRenderElements()
        )));

        if($this->getTemplate() && $this->getData() != "") {
            $template = $twig->loadTemplate($this->getTemplate());
            $this->setData($template->render(array(
                'value' => $this->getData()
            )));
        }

        $this->setY($this->getY()+$this->getTrimTop());
        $this->setX($this->getX()+$this->getTrimLeft());

        if($this->getRef()) {
            $motiv = Doctrine_Query::create ()->from('Motiv c')->where('c.uuid = ?', array($this->getRefValue()))->fetchOne();

            $loader = new Twig_Loader_String();
            $twig = new Twig_Environment($loader);
            $template = $twig->loadTemplate($this->_data);
            $this->_data = $template->render(array(
                'motiv' => $motiv,
                'elements' => $this->getRenderElements()
            ));
        }
        $elm = $this->getRenderElements();

        if($this->_data == "") {
            $this->setRenderSuccess(false);
            return;
        }

        $size = "";
        if($this->getFontUnderline()) {
            $size = "underline underlinewidth=3 underlineposition=-40% strokecolor={".$this->getFontColor()."} ";
        }

        if($this->getRotate() == 0 && $this->getLayer() != 0 && isset($layers[$this->getLayer()]) && $layers[$this->getLayer()]['rotate'] != "") {
            if($layers[$this->getLayer()]['rotate'] < 0) {
                $this->setRotate(360-$layers[$this->getLayer()]['rotate']);
            }
        }

        $rotate = "";
        if($this->getRotate()) {
            $rotate = " position 50 rotate=".$this->getRotate();

            $p->fit_textline($this->_data, $this->getX()+($this->_width/2), $this->getY(), $size . "fillcolor={".$this->getFontColor()."} font=".$font." charspacing=".$this->getCharSpacing()." fontsize=" . $this->getFontSize() . $rotate);
            return;
        }

        if($this->getWidth()) {
            $size = $size . "boxsize={" . $this->getWidth() . " " . $this->getFontSize() . "} position={center ".$this->getAlign()."} ";
        }

        if($this->_auto_break && strrpos($this->_data, $this->_auto_break)) {
            $this->_data = str_replace($this->_auto_break, $this->_auto_break."<nextline>", $this->_data);
            $optlist = " charspacing=".$this->getCharSpacing();
            if($this->getLineHeight()) {
                $optlist .=" leading=".$this->getLineHeight();
            }

            $tf_avoid = $p->create_textflow($this->_data, "fillcolor={".$this->getFontColor()."} font=".$font." fontsize=" . $this->getFontSize().$optlist);
            if ($tf_avoid == 0) {
                throw new Exception("Error: " . $p->get_errmsg());
            }

            $result = $p->fit_textflow($tf_avoid, $this->getX(), $this->getY()+$this->getHeight(), $this->getX()+$this->getWidth(),$this->getY() ,"fitmethod=auto");
            if (!$result == "_stop")
            {
                throw new Exception("Error: " . $p->get_errmsg());
            }

            $p->delete_textflow($tf_avoid);
            return;
        }

        $p->fit_textline($this->_data, $this->getX(), $this->getY(), $size . "fillcolor={".$this->getFontColor()."} font=".$font." charspacing=".$this->getCharSpacing()." fontsize=" . $this->getFontSize());


    }

    public function getFontSize()
    {
        return $this->_font_size;
    }

    public function setFontSize($font_size)
    {
        $this->_font_size = $font_size;
    }

    public function getFontColor()
    {
        return $this->_font_color;
    }

    public function setFontColor($font_color)
    {
        $this->_font_color = $font_color;
    }

    public function getFontStyle()
    {
        return $this->_font_style;
    }

    public function setFontStyle($font_style)
    {
        $this->_font_style = $font_style;
    }

    public function getRotate()
    {
        return $this->_rotate;
    }

    public function setRotate($rotate)
    {
        $this->_rotate = $rotate;
    }

    public function getWidth()
    {
        return $this->_width;
    }

    public function setWidth($width)
    {
        $this->_width = $width;
    }

    public function getAlign()
    {
        return $this->_align;
    }

    public function setAlign($align)
    {
        $this->_align = $align;
    }

    public function getFontBold() {
        return $this->_font_bold;
    }

    public function setFontBold($font_bold) {
        $this->_font_bold = $font_bold;
    }

    public function getFontItalic() {
        return $this->_font_italic;
    }

    public function setFontItalic($font_italic) {
        $this->_font_italic = $font_italic;
    }

    public function getFontUnderline() {
        return $this->_font_underline;
    }

    public function setFontUnderline($font_underline) {
        $this->_font_underline = $font_underline;
    }

    public function getFontFamily() {
        return $this->_font_family;
    }

    public function setFontFamily($font_family) {
        $this->_font_family = $font_family;
    }

    /**
     * @return boolean
     */
    public function getAutoBreak()
    {
        return $this->_auto_break;
    }

    /**
     * @param boolean $auto_break
     */
    public function setAutoBreak($auto_break)
    {
        $this->_auto_break = $auto_break;
    }

    /**
     * @return boolean
     */
    public function getHeight()
    {
        return $this->_height;
    }

    /**
     * @param boolean $height
     */
    public function setHeight($height)
    {
        $this->_height = $height;
    }

    /**
     * @return boolean
     */
    public function getLineHeight()
    {
        return $this->_lineHeight;
    }

    /**
     * @param boolean $lineHeight
     */
    public function setLineHeight($lineHeight)
    {
        $this->_lineHeight = $lineHeight;
    }

    /**
     * @return int
     */
    public function getCharSpacing()
    {
        return $this->_char_spacing;
    }

    /**
     * @param int $char_spacing
     */
    public function setCharSpacing($char_spacing)
    {
        $this->_char_spacing = $char_spacing;
    }

    /**
     * @return boolean
     */
    public function getTemplate()
    {
        return $this->_template;
    }

    /**
     * @param boolean $template
     */
    public function setTemplate($template)
    {
        $this->_template = $template;
    }
}