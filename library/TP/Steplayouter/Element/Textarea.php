<?php

class TP_Steplayouter_Element_Textarea extends TP_Steplayouter_Element_Text {

    private $_height = 400;

    private $_rotate = 0;

    private $_align = "left";

    private $_lineHeight = false;

    public function buildPDFLib($p, $layers = array())
    {

        try{
            $this->setY($this->getY()+$this->getTrimTop());
            $this->setX($this->getX()+$this->getTrimLeft());

            $value = $p->utf8_to_utf16(str_replace("\r\n", "<nextline>", $this->getData()),'');

            $debug = "";
            if($this->getRenderDebug()) {
                $debug = " showborder";
            }


            $shop = Zend_Registry::get('shop');

            $searchpath = APPLICATION_PATH . '/design/clients/' . $shop['uid'] . '/fonts';
            if($shop['market']) {
                $market = Zend_Registry::get('market_shop');
                $marketSearchPath = APPLICATION_PATH . '/design/clients/' . $market['uid'] . '/fonts';
            }

            if($this->getFontBold() && $this->getFontItalic()) {
                if(file_exists($searchpath . str_replace(" ", "-", $this->getFontFamily()).'-Bold-Italic.ttf') || file_exists($marketSearchPath . str_replace(" ", "-", $this->getFontFamily()).'-Bold-Italic.ttf')) {
                    $this->setFontFamily($this->getFontFamily() . ' Bold-Italic');
                    $optlist = "fontname={".str_replace("|", " ", str_replace(" ", "-", $this->getFontFamily()))."} fontstyle=normal fillcolor={".$this->getFontColor()."} fontsize=" . $this->getFontSize() ." encoding=unicode ";
                }else{
                    $optlist = "fontname={".str_replace("|", " ", str_replace(" ", "-", $this->getFontFamily()))."} fontstyle=bolditalic fillcolor={".$this->getFontColor()."} fontsize=" . $this->getFontSize() ." encoding=unicode ";
                }
            }elseif($this->getFontItalic()) {
                if(file_exists($searchpath . str_replace(" ", "-", $this->getFontFamily()).'-Italic.ttf') || file_exists($marketSearchPath . str_replace(" ", "-", $this->getFontFamily()).'-Italic.ttf')) {
                    $this->setFontFamily($this->getFontFamily() . ' Italic');
                    $optlist = "fontname={".str_replace("|", " ", str_replace(" ", "-", $this->getFontFamily()))."} fontstyle=normal fillcolor={".$this->getFontColor()."} fontsize=" . $this->getFontSize() ." encoding=unicode ";
                }else{
                    $optlist = "fontname={".str_replace("|", " ", str_replace(" ", "-", $this->getFontFamily()))."} fontstyle=italic fillcolor={".$this->getFontColor()."} fontsize=" . $this->getFontSize() ." encoding=unicode ";
                }
            }elseif($this->getFontBold()) {
                if(file_exists($searchpath . str_replace(" ", "-", $this->getFontFamily()).'-Bold.ttf') || file_exists($marketSearchPath . str_replace(" ", "-", $this->getFontFamily()).'-Bold.ttf')) {
                    $this->setFontFamily($this->getFontFamily() . ' Bold');
                    $optlist = "fontname={".str_replace("|", " ", str_replace(" ", "-", $this->getFontFamily()))."} fontstyle=normal fillcolor={".$this->getFontColor()."} fontsize=" . $this->getFontSize() ." encoding=unicode ";
                }else{
                    $optlist = "fontname={".str_replace("|", " ", str_replace(" ", "-", $this->getFontFamily()))."} fontstyle=bold fillcolor={".$this->getFontColor()."} fontsize=" . $this->getFontSize() ." encoding=unicode ";
                }
            }else{

                $optlist = "fontname={".str_replace("|", " ", $this->getFontFamily())."} fontstyle=normal fillcolor={".$this->getFontColor()."} fontsize=" . $this->getFontSize() ." encoding=unicode ";
            }

            if($this->getLineHeight()) {
                $optlist .=" leading=".$this->getLineHeight();
            }

            if($this->getAlign() != "left") {
                $optlist .= " alignment=".$this->getAlign();
            }

            if($this->getFontUnderline()) {
                $tf_avoid = $p->create_textflow("<underline>" . str_replace("|", "<nextline>", $this->getData()), $optlist . " underlinewidth=3 underlineposition=-40% strokecolor={".$this->getFontColor()."}");
            }else{
                $tf_avoid = $p->create_textflow(str_replace("|", "<nextline>", $this->getData()), $optlist);
            }
            if ($tf_avoid == 0) {
                throw new Exception("Error: " . $p->get_errmsg());
            }


            if($this->getRenderSizeFixed()) {
                $result = $p->fit_textflow($tf_avoid, 0, 0, 900, 900,"blind fitmethod=nofit");
                $width = $this->getWidth();
                $height = $this->getHeight();
                $widthB = $p->info_textflow($tf_avoid, "textwidth");
                $heightB = $p->info_textflow($tf_avoid, "textheight");


                if(($this->getX()+$widthB) > $this->getPageWidth() && !Zend_Registry::isRegistered("production") && !Zend_Registry::isRegistered("preview")) {
                    $this->setX($this->getPageWidth()-$widthB-100);
                }
            }else{
                $result = $p->fit_textflow($tf_avoid, 0, 0, 900, 900, "blind fitmethod=nofit");
                $width = $p->info_textflow($tf_avoid, "textwidth");
                $height = $p->info_textflow($tf_avoid, "textheight");

                if(($this->getX()+$width) > $this->getPageWidth() && !Zend_Registry::isRegistered("production") && !Zend_Registry::isRegistered("preview")) {
                    $this->setX($this->getPageWidth()-$width-100);
                }
            }


            $p->translate($this->getX() + ($width/2), $this->getY()+($this->getHeight()/2));
            if($this->getRotate() == 0 && $this->getLayer() != 0 && isset($layers[$this->getLayer()]) && $layers[$this->getLayer()]['rotate'] != "") {
                if($layers[$this->getLayer()]['rotate'] < 0) {
                    $this->setRotate(360-$layers[$this->getLayer()]['rotate']);
                }
            }
            $p->rotate($this->getRotate());
            if($this->getAlign() != "center") {
                if($this->getAlign() == "right" && !$this->getRenderSizeFixed()) {
                    if(substr_count( $this->getData(), "\n" ) == 0 && substr_count( $this->getData(), " " ) >= 1) {
                        $result = $p->fit_textflow($tf_avoid, ($width/2)*-1, ($this->getHeight()/2)*-1, ($width/2), ($this->getHeight()),"rewind=-1 fitmethod=nofit".$debug);
                    }else{
                        $result = $p->fit_textflow($tf_avoid, ($width/2)*-1, ($this->getHeight()/2)*-1, ($width/2), ($this->getHeight()),"rewind=-1 fitmethod=nofit".$debug);
                    }

                }else{
                    $result = $p->fit_textflow($tf_avoid, ($width/2)*-1, ($this->getHeight()/2)*-1, ($width), ($this->getHeight()),"rewind=-1 fitmethod=nofit".$debug);
                }

            }else{
                if($this->getRenderSizeFixed()) {
                    $result = $p->fit_textflow($tf_avoid, ($width/2)*-1, ($this->getHeight()/2)*-1, ($width), ($this->getHeight()),"rewind=-1 fitmethod=nofit".$debug);
                }else{
                    $result = $p->fit_textflow($tf_avoid, ($width)*-1, ($this->getHeight()/2)*-1, ($width), ($this->getHeight()),"rewind=-1 fitmethod=nofit".$debug);
                }

            }

            if (!$result == "_stop")
            {
                throw new Exception("Error: " . $p->get_errmsg());
            }

            $p->delete_textflow($tf_avoid);
            $p->rotate($this->getRotate()*-1);
            $p->translate(($this->getX()+($width/2))*-1, ($this->getY()+($this->getHeight()/2))*-1);
        }catch (PDFlibException $e){
            die("TEXT PDFlib exception occurred:\n" .
                "[" . $e->get_errnum() . "] " . $e->get_apiname() .
                ": " . $e->get_errmsg() . "\n".$this->getId().$optlist.$e->getTraceAsString());
        }catch (Exception $e) {
            die($e->getMessage().$e->getTraceAsString());
        }

    }

    public function getHeight()
    {
        return $this->_height;
    }

    public function setHeight($height)
    {
        $this->_height = $height;
    }

    public function getRotate() {
        return $this->_rotate;
    }

    public function setRotate($rotate) {
        $this->_rotate = $rotate;
    }

    public function getLineHeight()
    {
        return $this->_lineHeight;
    }

    public function setLineHeight($lineHeight)
    {
        $this->_lineHeight = $lineHeight;
    }

    public function getAlign()
    {
        return $this->_align;
    }

    public function setAlign($align)
    {
        $this->_align = $align;
    }

}