<?php

class TP_Steplayouter_Element_Upload extends TP_Steplayouter_Element {


    private $_file = false;

    private $_width = 400;

    private $_height = 400;

    private $_rotate = 0;

    private $_value = false;

    private $_render_print_adjust = true;

    public function getFile()
    {
        return $this->_file;
    }

    public function setFile($file)
    {
        $this->_file = $file;
    }

    public function buildPDFLib($p, $layers = array())
    {
        if($this->getFile()) {
            $image = $p->load_image("auto", $this->getFile(), "");
            if ($image == 0) {
                return;
            }
            $p->translate($this->getX() + ($this->_width/2), $this->getY()+($this->_height/2));
            $p->rotate($this->getRotate());
            if($this->getLayer() != 0 && isset($layers[$this->getLayer()]) && $layers[$this->getLayer()]['clipbox'] != "") {
                $clipbox = explode(" ", $layers[$this->getLayer()]['clipbox']);
                $optlist = "matchbox={clipping={" . $clipbox[0] . " " . $clipbox[1] . " " . $clipbox[2] . " " . $clipbox[3] . "}}";
                $p->fit_image($image, ($this->_width/2)*-1, round($this->_height/2),
                    "position {center top} boxsize={".$this->getWidth()." ".$this->getHeight()."} fitmethod=meet " . $optlist);
            }else{
                $p->fit_image($image, ($this->_width/2)*-1, round($this->_height/2),
                    "position {center top} boxsize={".$this->getWidth()." ".$this->getHeight()."} fitmethod=meet");
            }


            $p->close_image($image);
            $p->end_layer( );
            $p->rotate($this->getRotate()*-1);
            $p->translate(($this->getX()+($this->_width/2))*-1, ($this->getY()+($this->_height/2))*-1);
        }elseif($this->getValue()) {
            $motiv = Doctrine_Query::create ()->from('Motiv c')->where('c.uuid = ?', array($this->getValue()))->fetchOne();

            $image = $p->load_image("auto", APPLICATION_PATH . '/../market/motive/' .$motiv->file_thumb, "");
            if ($image == 0) {
                return;
            }
            $p->translate($this->getX() + ($this->_width/2), $this->getY()+($this->_height/2));
            $p->rotate($this->getRotate());

            if($this->getLayer() != 0 && isset($layers[$this->getLayer()]) && $layers[$this->getLayer()]['clipbox'] != "") {
                $clipbox = explode(" ", $layers[$this->getLayer()]['clipbox']);
                $optlist = "matchbox={clipping={" . $clipbox[0] . " " . $clipbox[1] . " " . $clipbox[2] . " " . $clipbox[3] . "}}";
                $p->fit_image($image, ($this->_width/2)*-1, round($this->_height/2),
                    "position {center top} boxsize={".$this->getWidth()." ".$this->getHeight()."} fitmethod=meet " . $optlist);
            }else{
                $p->fit_image($image, ($this->_width/2)*-1, round($this->_height/2),
                    "position {center top} boxsize={".$this->getWidth()." ".$this->getHeight()."} fitmethod=meet");
            }


            $p->close_image($image);
            $p->end_layer( );
            $p->rotate($this->getRotate()*-1);
            $p->translate(($this->getX()+($this->_width/2))*-1, ($this->getY()+($this->_height/2))*-1);


        }

    }

    public function getWidth()
    {
        return $this->_width;
    }

    public function setWidth($width)
    {
        $this->_width = $width;
    }

    public function getHeight()
    {
        return $this->_height;
    }

    public function setHeight($height)
    {
        $this->_height = $height;
    }

    public function getValue() {
        return $this->_value;
    }

    public function setValue($value) {
        $this->_value = $value;
    }

    public function getRotate() {
        return $this->_rotate;
    }

    public function setRotate($rotate) {
        $this->_rotate = $rotate;
    }

    /**
     * @return boolean
     */
    public function getRenderPrintAdjust()
    {
        return $this->_render_print_adjust;
    }

    /**
     * @param boolean $render_print_adjust
     */
    public function setRenderPrintAdjust($render_print_adjust)
    {
        $this->_render_print_adjust = $render_print_adjust;
    }

}