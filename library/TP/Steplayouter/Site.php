<?php

class TP_Steplayouter_Site
{

    private $_width = 0;

    private $_height = 0;

    private $_trim_left = false;

    private $_trim_right = false;

    private $_trim_top = false;

    private $_trim_bottom = false;

    private $_fold_mark_x = false;

    private $_fold_mark_y = false;

    private $_cut_x = false;

    private $_cut_y = false;

    private $_cut_width = false;

    private $_cut_height = false;

    private $_render_preview_trim = false;

    private $_render_layouter_trim = false;

    private $_render_preview_fold_mark = false;

    private $_passepartout = "";

    private $_id = "";

    private $_elements = null;

    private $_background_color = "#ffffff";

    private $_ref_layers = array();

    private $_ref_layers_elements = array();

    private $_base_width = 0;

    private $_base_height = 0;

    public function __construct()
    {
        $this->_elements = new TP_Steplayouter_Element_Collection();
    }

    /*
     * @param SimpleXMLElement $config
     * @return SimpleXMLElement
     */
    public function createFromXml(\SimpleXMLElement $config, \SimpleXMLElement $steps, $fix_config, $dyn_config, $layer_data = array(), $create_data = array(), $width = 0, $height = 0)
    {

        $this->_id = (int)$config['id'];
        $this->_width = (int)$config['width'];
        $this->_height = (int)$config['height'];

        $this->_base_width = $width;
        $this->_base_height = $height;

        if (isset($config['trim-left'])) {
            $this->setTrimLeft((float)$config['trim-left']);
        }
        if (isset($config['site-background-color'])) {
            $this->setBackgroundColor((string)$config['site-background-color']);
        }
        if (isset($dyn_config['site-background-color']) && $dyn_config['site-background-color'] != "") {
            $this->setBackgroundColor($dyn_config['site-background-color']);
        }
        if (isset($config['trim-right'])) {
            $this->setTrimRight((float)$config['trim-right']);
        }
        if (isset($config['trim-top'])) {
            $this->setTrimTop((float)$config['trim-top']);
        }
        if (isset($config['trim-bottom'])) {
            $this->setTrimBottom((float)$config['trim-bottom']);
        }
        if (isset($config['passepartout'])) {
            $this->setPassepartout((string)$config['passepartout']);
        }

        if (isset($config['fold-mark-x'])) {
            $this->setFoldMarkX((float)$config['fold-mark-x']);
        }
        if (isset($config['fold-mark-y'])) {
            $this->setFoldMarkY((float)$config['fold-mark-y']);
        }
        if (isset($config['cut-x'])) {
            $this->setCutX((float)$config['cut-x']);
        }
        if (isset($config['cut-y'])) {
            $this->setCutY((float)$config['cut-y']);
        }
        if (isset($config['cut-width'])) {
            $this->setCutWidth((float)$config['cut-width']);
        }
        if (isset($config['cut-height'])) {
            $this->setCutHeight((float)$config['cut-height']);
        }
        if (isset($config['render-preview-trim'])) {
            $this->setRenderPreviewTrim((int)$config['render-preview-trim']);
        }
        if (isset($config['render-layouter-trim'])) {
            $this->setRenderLayouterTrim((int)$config['render-layouter-trim']);
        }
        if (isset($config['render-preview-fold-mark'])) {
            $this->setRenderPreviewFoldMark((int)$config['render-preview-fold-mark']);
        }

        foreach ($config->elements->children() as $key => $child) {
            switch ((string)$child['render-mode']) {
                case "rectangle":
                    $element = new TP_Steplayouter_Element_Rectangle();
                    $element->setFillColor((string)$child['value']);
                    if (isset($dyn_config[(string)$child['id']]) && $dyn_config[(string)$child['id']] != "") {
                        $element->setFillColor($dyn_config[(string)$child['id']]);
                    }

                    $element->setWidth((int)$child['render-pos-width']);
                    $element->setHeight((int)$child['render-pos-height']);
                    break;
                case "qrcode":
                    $element = new TP_Steplayouter_Element_QRCode();
                    $element->setText((string)$child);
                    $element->setDynConfig($dyn_config);
                    $element->setWidth((int)$child['render-pos-width']);
                    $element->setHeight((int)$child['render-pos-height']);
                    break;
                case "image":
                case "motiv":
                    $element = new TP_Steplayouter_Element_Motiv();
                    $element->setMotivUUID((string)$child['value']);

                    $element->setWidth((int)$child['render-pos-width']);
                    $element->setHeight((int)$child['render-pos-height']);
                    if (isset($child['render-rotate'])) {
                        $element->setRotate((int)$child['render-rotate']);
                    }
                    break;
                case "textarea":
                    $element = new TP_Steplayouter_Element_Textarea();
                    if (isset($dyn_config[(string)$child['id']])) {
                        $element->setData($dyn_config[(string)$child['id']]);
                    } else {
                        $element->setData((string)$child['value']);
                    }
                    $element->setFontSize((int)$child['render-font-size']);
                    if (isset($dyn_config[(string)$child['id'] . '_size']) && $dyn_config[(string)$child['id'] . '_size'] != "") {
                        $element->setFontSize($dyn_config[(string)$child['id'] . '_size']);
                    }
                    $element->setWidth((int)$child['render-pos-width']);
                    $element->setHeight((int)$child['render-pos-height']);
                    if (isset($child['render-font-color'])) {
                        $element->setFontColor((string)$child['render-font-color']);
                    }
                    if (isset($dyn_config[(string)$child['id'] . '_color']) && $dyn_config[(string)$child['id'] . '_color'] != "") {
                        $element->setFontColor($dyn_config[(string)$child['id'] . '_color']);
                    }
                    if (isset($child['render-font-bold'])) {
                        $element->setFontBold((string)$child['render-font-bold']);
                    }
                    if (isset($child['render-font-line-height'])) {
                        $element->setLineHeight((int)$child['render-font-line-height']);
                    }
                    if (isset($dyn_config[(string)$child['id'] . '_bold']) && $dyn_config[(string)$child['id'] . '_bold'] != "") {
                        $element->setFontBold($dyn_config[(string)$child['id'] . '_bold']);
                    }
                    if (isset($child['render-font-italic'])) {
                        $element->setFontItalic((string)$child['render-font-italic']);
                    }
                    if (isset($dyn_config[(string)$child['id'] . '_italic']) && $dyn_config[(string)$child['id'] . '_italic'] != "") {
                        $element->setFontItalic($dyn_config[(string)$child['id'] . '_italic']);
                    }
                    if (isset($child['render-font-underline'])) {
                        $element->setFontUnderline((string)$child['render-font-underline']);
                    }
                    if (isset($child['render-font-align'])) {
                        $element->setAlign((string)$child['render-font-align']);
                    }
                    if (isset($dyn_config[(string)$child['id'] . '_underline']) && $dyn_config[(string)$child['id'] . '_underline'] != "") {
                        $element->setFontUnderline($dyn_config[(string)$child['id'] . '_underline']);
                    }
                    break;
                case "formtext":
                case "text":
                case "richtext":
                    if((string)$child['id'] == "sponsord-by-text" && (!isset($dyn_config["sponsord-by"]) || $dyn_config["sponsord-by"] == "remove"  || $dyn_config["sponsord-by"] == "")) {
                        continue;
                    }
                    $element = new TP_Steplayouter_Element_Text();
                    if (isset($dyn_config[(string)$child['id']])) {
                        $element->setData($dyn_config[(string)$child['id']]);
                    } else {
                        $element->setData((string)$child['value']);
                    }
                    if (isset($child['template'])) {
                        $element->setTemplate((string)$child['template']);
                    }
                    if (isset($child['render-rotate'])) {
                        $element->setRotate((int)$child['render-rotate']);
                    }
                    if (isset($dyn_config[(string)$child['id'] . '_rotate']) && $dyn_config[(string)$child['id'] . '_rotate'] != "") {
                        $element->setRotate($dyn_config[(string)$child['id'] . '_rotate']);
                    }
                    if (isset($child['render-font-color'])) {
                        $element->setFontColor((string)$child['render-font-color']);
                    }
                    if (isset($dyn_config[(string)$child['id'] . '_color']) && $dyn_config[(string)$child['id'] . '_color'] != "") {
                        $element->setFontColor($dyn_config[(string)$child['id'] . '_color']);
                    }
                    $element->setFontSize((int)$child['render-font-size']);
                    if (isset($dyn_config[(string)$child['id'] . '_size']) && $dyn_config[(string)$child['id'] . '_size'] != "") {
                        $element->setFontSize($dyn_config[(string)$child['id'] . '_size']);
                    }
                    if (isset($child['render-font-bold'])) {
                        $element->setFontBold((string)$child['render-font-bold']);
                    }
                    if (isset($dyn_config[(string)$child['id'] . '_bold']) && $dyn_config[(string)$child['id'] . '_bold'] != "") {
                        $element->setFontBold($dyn_config[(string)$child['id'] . '_bold']);
                    }
                    if (isset($child['render-font-italic'])) {
                        $element->setFontItalic((string)$child['render-font-italic']);
                    }
                    if (isset($dyn_config[(string)$child['id'] . '_italic']) && $dyn_config[(string)$child['id'] . '_italic'] != "") {
                        $element->setFontItalic($dyn_config[(string)$child['id'] . '_italic']);
                    }
                    if (isset($child['render-font-underline'])) {
                        $element->setFontUnderline((string)$child['render-font-underline']);
                    }
                    if (isset($dyn_config[(string)$child['id'] . '_underline']) && $dyn_config[(string)$child['id'] . '_underline'] != "") {
                        $element->setFontUnderline($dyn_config[(string)$child['id'] . '_underline']);
                    }
                    if (isset($child['render-pos-width'])) {
                        $element->setWidth((int)$child['render-pos-width']);
                    }
                    if (isset($child['render-font-align'])) {
                        $element->setAlign((string)$child['render-font-align']);
                    }
                    if (isset($child['render-font-family'])) {
                        $element->setFontFamily((string)$child['render-font-family']);
                    }
                    if (isset($dyn_config[(string)$child['id'] . '_font']) && $dyn_config[(string)$child['id'] . '_font'] != "") {
                        $element->setFontFamily($dyn_config[(string)$child['id'] . '_font']);
                    }
                    break;
            }

            $element->setId((string)$child['id']);
            $element->setX((int)$child['render-pos-x']);
            $element->setY((int)$child['render-pos-y']);
            if (isset($child['is-active'])) {
                $element->setIsActive((string)$child['is-active']);
            }
            if (isset($child['render-pos-layer'])) {
                $element->setLayer((int)$child['render-pos-layer']);
            } else {
                $element->setLayer(1);
            }

            if (isset($child['render-pos-sort'])) {
                $element->setSort((int)$child['render-pos-sort']);
            }

            $this->_elements->addItem($element);

        }

        foreach ($create_data['elements'] as $child) {
                $element = false;
            if ($this->_id == (int)$child['render-site']) {
                switch ($child['type']) {
                    case "formuploadmotiv":
                        $element = new TP_Steplayouter_Element_Motiv();
                        $element->setMotivUUID((string)$child['value']);
                        if (isset($dyn_config[(string)$child['id']]) && $dyn_config[(string)$child['id']] != "") {
                            $element->setMotivUUID($dyn_config[(string)$child['id']]);
                        }
                        if (isset($dyn_config[(string)$child['id']]) && $dyn_config[(string)$child['id']] == "remove") {
                            $element->setMotivUUID(false);
                        }
                        $element->setWidth(100);
                        $element->setHeight(100);
                        if (isset($child['render-rotate'])) {
                            $element->setRotate((int)$child['render-rotate']);
                        }
                        if (isset($layer_data[(int)$child['render-site']][(int)$child['render-pos-layer']]['rotate'])) {
                            $element->setRotate($layer_data[(int)$child['render-site']][(int)$child['render-pos-layer']]['rotate'] * -1);
                        }
                        if (isset($dyn_config[(string)$child['id'] . '_crop']) && $dyn_config[(string)$child['id'] . '_crop'] != "") {
                            $element->setCrop($dyn_config[(string)$child['id'] . '_crop']);
                        }
                        if (isset($dyn_config[(string)$child['id'] . '_rotate']) && $dyn_config[(string)$child['id'] . '_rotate'] != "") {
                            $element->setRotate($dyn_config[(string)$child['id'] . '_rotate']);
                        }
                        if (isset($layer_data[(int)$child['render-site']][(int)$child['render-pos-layer']]['scale'])) {
                            $element->setScale($layer_data[(int)$child['render-site']][(int)$child['render-pos-layer']]['scale']);
                        }
                        if (isset($dyn_config[(string)$child['id'] . '_scale']) && $dyn_config[(string)$child['id'] . '_scale'] != "") {
                            $element->setScale($dyn_config[(string)$child['id'] . '_scale']);
                        }
                        break;
                    case "formtext":
                    case "text":
                    case "richtext":
                        if((string)$child['id'] == "sponsord-by-text" && (!isset($dyn_config["sponsord-by"]) || $dyn_config["sponsord-by"] == "remove"  || $dyn_config["sponsord-by"] == "")) {
                            continue;
                        }
                        $element = new TP_Steplayouter_Element_Text();
                        if (isset($dyn_config[(string)$child['id']])) {
                            $element->setData($dyn_config[(string)$child['id']]);
                        } else {
                            $element->setData((string)$child['value']);
                        }
                        if (isset($child['template'])) {
                            $element->setTemplate((string)$child['template']);
                        }
                        if (isset($child['render-rotate'])) {
                            $element->setRotate((int)$child['render-rotate']);
                        }
                        if (isset($child['render-font-line-height'])) {
                            $element->setLineHeight((int)$child['render-font-line-height']);
                        }
                        if (isset($child['render-font-char-spacing'])) {
                            $element->setCharSpacing((string)$child['render-font-char-spacing']);
                        }
                        if (isset($layer_data[(int)$child['render-site']][(int)$child['render-pos-layer']]['rotate'])) {
                            $element->setRotate($layer_data[(int)$child['render-site']][(int)$child['render-pos-layer']]['rotate'] * -1);
                        }
                        if (isset($dyn_config[(string)$child['id'] . '_rotate']) && $dyn_config[(string)$child['id'] . '_rotate'] != "") {
                            $element->setRotate($dyn_config[(string)$child['id'] . '_rotate']);
                        }
                        if (isset($child['render-font-color'])) {
                            $element->setFontColor((string)$child['render-font-color']);
                        }
                        if (isset($dyn_config[(string)$child['id'] . '_color']) && $dyn_config[(string)$child['id'] . '_color'] != "") {
                            $element->setFontColor($dyn_config[(string)$child['id'] . '_color']);
                        }
                        $element->setFontSize((int)$child['render-font-size']);
                        if (isset($dyn_config[(string)$child['id'] . '_size']) && $dyn_config[(string)$child['id'] . '_size'] != "") {
                            $element->setFontSize($dyn_config[(string)$child['id'] . '_size']);
                        }
                        if (isset($child['render-font-bold'])) {
                            $element->setFontBold((string)$child['render-font-bold']);
                        }
                        if (isset($child['render-auto-break'])) {
                            $element->setAutoBreak((string)$child['render-auto-break']);
                        }
                        if (isset($dyn_config[(string)$child['id'] . '_bold']) && $dyn_config[(string)$child['id'] . '_bold'] != "") {
                            $element->setFontBold($dyn_config[(string)$child['id'] . '_bold']);
                        }
                        if (isset($child['render-font-italic'])) {
                            $element->setFontItalic((string)$child['render-font-italic']);
                        }
                        if (isset($dyn_config[(string)$child['id'] . '_italic']) && $dyn_config[(string)$child['id'] . '_italic'] != "") {
                            $element->setFontItalic($dyn_config[(string)$child['id'] . '_italic']);
                        }
                        if (isset($child['render-font-underline'])) {
                            $element->setFontUnderline((string)$child['render-font-underline']);
                        }
                        if (isset($dyn_config[(string)$child['id'] . '_underline']) && $dyn_config[(string)$child['id'] . '_underline'] != "") {
                            $element->setFontUnderline($dyn_config[(string)$child['id'] . '_underline']);
                        }
                        if (isset($child['render-pos-width'])) {
                            $element->setWidth((int)$child['render-pos-width']);
                        }
                        if (isset($child['render-pos-height'])) {
                            $element->setHeight((int)$child['render-pos-height']);
                        }
                        if (isset($child['render-font-align'])) {
                            $element->setAlign((string)$child['render-font-align']);
                        }
                        if (isset($dyn_config[(string)$child['id'] . '_align']) && $dyn_config[(string)$child['id'] . '_align'] != "") {
                            $element->setAlign($dyn_config[(string)$child['id'] . '_align']);
                        }
                        if (isset($child['render-font-family'])) {
                            $element->setFontFamily((string)$child['render-font-family']);
                        }
                        if (isset($dyn_config[(string)$child['id'] . '_font']) && $dyn_config[(string)$child['id'] . '_font'] != "") {
                            $element->setFontFamily($dyn_config[(string)$child['id'] . '_font']);
                        }
                        break;
                    case "formrte":
                        $element = new TP_Steplayouter_Element_Rte();

                        if (isset($dyn_config[(string)$child['id']])) {
                            $element->setData($dyn_config[(string)$child['id']]);
                        } else {
                            if((string)$child != "") {
                                $element->setData((string)$child);
                            }else{
                                $element->setData((string)$child['value']);
                            }
                        }
                        if (isset($layer_data[(int)$child['render-site']][(int)$child['render-pos-layer']]['rotate'])) {
                            $element->setRotate($layer_data[(int)$child['render-site']][(int)$child['render-pos-layer']]['rotate'] * -1);
                        }
                        if (isset($dyn_config[(string)$child['id'] . '_rotate']) && $dyn_config[(string)$child['id'] . '_rotate'] != "") {
                            $element->setRotate($dyn_config[(string)$child['id'] . '_rotate']);
                        }
                        if (isset($child['data-render-font-line-height'])) {
                            $element->setLineHeight((string)$child['data-render-font-line-height']);
                        }

                        $element->setWidth((int)$child['render-pos-width']);
                        $element->setHeight((int)$child['render-pos-height']);

                        break;
                }

                if ($element) {
                    $element->setId((string)$child['id']);
                    $element->setX((int)$child['render-pos-x']);

                    $element->setY((int)$child['render-pos-y']);

                    if (isset($child['render-pos-layer'])) {
                        $element->setLayer((int)$child['render-pos-layer']);
                        if (isset($layer_data[(int)$child['render-site']][(int)$child['render-pos-layer']])) {
                            if (isset($layer_data[(int)$child['render-site']][(int)$child['render-pos-layer']]['x'])) {
                                $rel = $this->_width / $layer_data[(int)$child['render-site']][(int)$child['render-pos-layer']]['width'] * $layer_data[(int)$child['render-site']][(int)$child['render-pos-layer']]['x'];
                                $element->setX((int)$child['render-pos-x'] + $rel);
                            }
                            if (isset($layer_data[(int)$child['render-site']][(int)$child['render-pos-layer']]['y'])) {
                                $rel = $this->_height / $layer_data[(int)$child['render-site']][(int)$child['render-pos-layer']]['height'] * $layer_data[(int)$child['render-site']][(int)$child['render-pos-layer']]['y'];
                                $element->setY((int)$child['render-pos-y'] + ($rel));

                            }
                        }else{
                            $element->setDirty(true);
                        }
                    } else {
                        $element->setLayer(1);
                    }
                    if (isset($child['render-pos-orglayer'])) {
                        $element->setOrgLayer($child['render-pos-orglayer']);
                    }

                    if (isset($child['render-pos-sort'])) {
                        $element->setSort((int)$child['render-pos-sort']);
                    }

                    if (isset($child['render-size-fixed'])) {
                        $element->setRenderSizeFixed((int)$child['render-size-fixed']);
                    }

                    if (isset($child['render-debug'])) {
                        $element->setRenderDebug((int)$child['render-debug']);
                    }

                    $element->setPageHeight($this->_height);
                    $element->setPageWidth($this->_width);
                    $this->_elements->addItem($element);
                }
            }
        }


        foreach ($steps->step as $childs) {
            foreach ($childs->elements->children() as $child) {
                if ($this->_id == (int)$child['render-site'] || (string)$child['render-mode'] == "substeps" || (string)$child['render-mode'] == "site-layout") {
                    $element = false;

                    switch ((string)$child['render-mode']) {
                        case "calender":
                            $element = new TP_Steplayouter_Element_Calender();
                            $element->setTemplate((int)$child['calender-template']);

                            $calDesign = new TP_Steplayouter_Calendar_Design();
                            $calDesign->setWidth((int)$child['render-pos-width']);
                            $calDesign->setHeight((int)$child['render-pos-height']);
                            $calDesign->setMonth((int)$child['calender-month']);
                            $calDesign->setX((int)$child['render-pos-x']);
                            $calDesign->setSiteBackground($this->getBackgroundColor());

                            $calDesign->setY((int)$child['render-pos-y']);
                            if (isset($dyn_config['calender-year']) && $dyn_config['calender-year'] != "") {
                                $calDesign->setStartYear($dyn_config['calender-year']);
                            }
                            if (isset($dyn_config['calender-month']) && $dyn_config['calender-month'] != "") {
                                $calDesign->setStartMonth($dyn_config['calender-month']);
                            }
                            if (isset($dyn_config['calender-region']) && $dyn_config['calender-region'] != "") {
                                $calDesign->setPrintRegion($dyn_config['calender-region']);
                            }
                            if (isset($dyn_config['calender-holiday']) && $dyn_config['calender-holiday'] != "") {
                                $calDesign->setPrintHoliday($dyn_config['calender-holiday']);
                            }
                            if (isset($dyn_config['calender-vacation']) && $dyn_config['calender-vacation'] != "") {
                                $calDesign->setPrintVacation($dyn_config['calender-vacation']);
                            }
                            if (isset($child['render-font-family'])) {
                                $calDesign->setFontFamily((string)$child['render-font-family']);
                            }
                            if (isset($child['calender-monthsize'])) {
                                $calDesign->setMonthSize((int)$child['calender-monthsize']);
                            }
                            if (isset($child['calender-daysize'])) {
                                $calDesign->setDaySize((int)$child['calender-daysize']);
                            }
                            if (isset($child['calender-monthcolor'])) {
                                $calDesign->setMonthColor((string)$child['calender-monthcolor']);
                            }
                            if (isset($child['calender-font-family-days'])) {
                                $calDesign->setFontFamilyDays((string)$child['calender-font-family-days']);
                            }
                            if (isset($dyn_config['calender-monthcolor']) && $dyn_config['calender-monthcolor'] != "") {
                                $calDesign->setMonthColor($dyn_config['calender-monthcolor']);
                            }
                            if (isset($dyn_config['calender-daycolor']) && $dyn_config['calender-daycolor'] != "") {
                                $calDesign->setDayColor($dyn_config['calender-daycolor']);
                            }
                            if (isset($dyn_config['calender-daynamescolor']) && $dyn_config['calender-daynamescolor'] != "") {
                                $calDesign->setDayNamesColor($dyn_config['calender-daynamescolor']);
                            }
                            if (isset($dyn_config['calender-weeknamescolor']) && $dyn_config['calender-weeknamescolor'] != "") {
                                $calDesign->setWeekNamesColor($dyn_config['calender-weeknamescolor']);
                            }
                            if (isset($dyn_config['calender-holidaybackcolor']) && $dyn_config['calender-holidaybackcolor'] != "") {
                                $calDesign->setHolidayBackColor($dyn_config['calender-holidaybackcolor']);
                            }
                            if (isset($dyn_config['calender-holidaycolor']) && $dyn_config['calender-holidaycolor'] != "") {
                                $calDesign->setHolidayColor($dyn_config['calender-holidaycolor']);
                            }
                            if (isset($dyn_config['calender-sunbackcolor']) && $dyn_config['calender-sunbackcolor'] != "") {
                                $calDesign->setSunBackColor($dyn_config['calender-sunbackcolor']);
                            }
                            if (isset($dyn_config['calender-samcolor']) && $dyn_config['calender-samcolor'] != "") {
                                $calDesign->setSamColor($dyn_config['calender-samcolor']);
                            }
                            if (isset($dyn_config['calender-suncolor']) && $dyn_config['calender-suncolor'] != "") {
                                $calDesign->setSunColor($dyn_config['calender-suncolor']);
                            }
                            if (isset($dyn_config['calender-vacationcolor']) && $dyn_config['calender-vacationcolor'] != "") {
                                $calDesign->setVacationColor($dyn_config['calender-vacationcolor']);
                            }
                            $element->setCalendarDesign($calDesign);


                            break;
                        case "rectangle":
                            $element = new TP_Steplayouter_Element_Rectangle();
                            $element->setFillColor((string)$child['value']);
                            if (isset($dyn_config[(string)$child['id']]) && $dyn_config[(string)$child['id']] != "") {
                                $element->setFillColor($dyn_config[(string)$child['id']]);
                            }

                            $element->setWidth((int)$child['render-pos-width']);
                            $element->setHeight((int)$child['render-pos-height']);
                            break;
                        case "image":
                        case "motiv":
                        case "formuploadmotiv":
                            $element = new TP_Steplayouter_Element_Motiv();
                            $element->setMotivUUID((string)$child['value']);
                            if (isset($dyn_config[(string)$child['id']]) && $dyn_config[(string)$child['id']] != "") {
                                $element->setMotivUUID($dyn_config[(string)$child['id']]);
                            }
                            if (isset($dyn_config[(string)$child['id']]) && $dyn_config[(string)$child['id']] == "remove") {
                                $element->setMotivUUID(false);
                            }
                            $element->setWidth((int)$child['render-pos-width']);
                            $element->setHeight((int)$child['render-pos-height']);
                            if (isset($child['render-rotate'])) {
                                $element->setRotate((int)$child['render-rotate']);
                            }
                            if (isset($child['render-print-adjust'])) {
                                $element->setRenderPrintAdjust((int)$child['render-print-adjust']);
                            }
                            if (isset($layer_data[(int)$child['render-site']][(int)$child['render-pos-layer']]['rotate'])) {
                                $element->setRotate($layer_data[(int)$child['render-site']][(int)$child['render-pos-layer']]['rotate'] * -1);
                            }
                            if (isset($dyn_config[(string)$child['id'] . '_crop']) && $dyn_config[(string)$child['id'] . '_crop'] != "") {
                                $element->setCrop($dyn_config[(string)$child['id'] . '_crop']);
                            }
                            if (isset($dyn_config[(string)$child['id'] . '_rotate']) && $dyn_config[(string)$child['id'] . '_rotate'] != "") {
                                $element->setRotate($dyn_config[(string)$child['id'] . '_rotate']);
                            }
                            if (isset($layer_data[(int)$child['render-site']][(int)$child['render-pos-layer']]['scale'])) {
                                $element->setScale($layer_data[(int)$child['render-site']][(int)$child['render-pos-layer']]['scale']);
                            }
                            if (isset($dyn_config[(string)$child['id'] . '_scale']) && $dyn_config[(string)$child['id'] . '_scale'] != "") {
                                $element->setScale($dyn_config[(string)$child['id'] . '_scale']);
                            }
                            break;
                        case "rte":
                            $element = new TP_Steplayouter_Element_Rte();

                            if (isset($dyn_config[(string)$child['id']])) {
                                $element->setData($dyn_config[(string)$child['id']]);
                            } else {
                                if((string)$child != "") {
                                    $element->setData((string)$child);
                                }else{
                                    $element->setData((string)$child['value']);
                                }
                            }
                            if (isset($layer_data[(int)$child['render-site']][(int)$child['render-pos-layer']]['rotate'])) {
                                $element->setRotate($layer_data[(int)$child['render-site']][(int)$child['render-pos-layer']]['rotate'] * -1);
                            }
                            if (isset($dyn_config[(string)$child['id'] . '_rotate']) && $dyn_config[(string)$child['id'] . '_rotate'] != "") {
                                $element->setRotate($dyn_config[(string)$child['id'] . '_rotate']);
                            }
                            if (isset($child['render-font-line-height'])) {
                                $element->setLineHeight((string)$child['render-font-line-height']);
                            }

                            $element->setWidth((int)$child['render-pos-width']);
                            $element->setHeight((int)$child['render-pos-height']);

                            break;
                        case "textarea":
                            $element = new TP_Steplayouter_Element_Textarea();

                            if (isset($dyn_config[(string)$child['id']])) {
                                $element->setData($dyn_config[(string)$child['id']]);
                            } else {
                                $element->setData((string)$child['value']);
                            }
                            if (isset($layer_data[(int)$child['render-site']][(int)$child['render-pos-layer']]['rotate'])) {
                                $element->setRotate($layer_data[(int)$child['render-site']][(int)$child['render-pos-layer']]['rotate'] * -1);
                            }
                            if (isset($dyn_config[(string)$child['id'] . '_rotate']) && $dyn_config[(string)$child['id'] . '_rotate'] != "") {
                                $element->setRotate($dyn_config[(string)$child['id'] . '_rotate']);
                            }
                            if (isset($child['render-font-color'])) {
                                $element->setFontColor((string)$child['render-font-color']);
                            }
                            if (isset($dyn_config[(string)$child['id'] . '_color']) && $dyn_config[(string)$child['id'] . '_color'] != "") {
                                $element->setFontColor($dyn_config[(string)$child['id'] . '_color']);
                            }
                            if (isset($child['render-font-line-height'])) {
                                $element->setLineHeight((int)$child['render-font-line-height']);
                            }
                            $element->setFontSize((int)$child['render-font-size']);
                            if (isset($dyn_config[(string)$child['id'] . '_size']) && $dyn_config[(string)$child['id'] . '_size'] != "" && $dyn_config[(string)$child['id'] . '_size'] != "0") {
                                $element->setFontSize($dyn_config[(string)$child['id'] . '_size']);
                            }
                            $element->setWidth((int)$child['render-pos-width']);
                            $element->setHeight((int)$child['render-pos-height']);
                            if (isset($child['render-font-bold'])) {
                                $element->setFontBold((string)$child['render-font-bold']);
                            }
                            if (isset($dyn_config[(string)$child['id'] . '_bold']) && $dyn_config[(string)$child['id'] . '_bold'] != "") {
                                $element->setFontBold($dyn_config[(string)$child['id'] . '_bold']);
                            }
                            if (isset($child['render-font-italic'])) {
                                $element->setFontItalic((string)$child['render-font-italic']);
                            }
                            if (isset($dyn_config[(string)$child['id'] . '_italic']) && $dyn_config[(string)$child['id'] . '_italic'] != "") {
                                $element->setFontItalic($dyn_config[(string)$child['id'] . '_italic']);
                            }
                            if (isset($child['render-font-underline'])) {
                                $element->setFontUnderline((string)$child['render-font-underline']);
                            }
                            if (isset($dyn_config[(string)$child['id'] . '_underline']) && $dyn_config[(string)$child['id'] . '_underline'] != "") {
                                $element->setFontUnderline($dyn_config[(string)$child['id'] . '_underline']);
                            }
                            if (isset($child['render-font-family'])) {
                                $element->setFontFamily((string)$child['render-font-family']);
                            }
                            if (isset($child['render-font-align'])) {
                                $element->setAlign((string)$child['render-font-align']);
                            }
                            if (isset($dyn_config[(string)$child['id'] . '_align']) && $dyn_config[(string)$child['id'] . '_align'] != "") {
                                $element->setAlign($dyn_config[(string)$child['id'] . '_align']);
                            }
                            if (isset($dyn_config[(string)$child['id'] . '_font']) && $dyn_config[(string)$child['id'] . '_font'] != "") {
                                $element->setFontFamily($dyn_config[(string)$child['id'] . '_font']);
                            }
                            break;
                        case "formtext":
                        case "text":
                        case "richtext":
                            if((string)$child['id'] == "sponsord-by-text" && (!isset($dyn_config["sponsord-by"]) || $dyn_config["sponsord-by"] == "remove"  || $dyn_config["sponsord-by"] == "")) {
                                continue;
                            }
                            $element = new TP_Steplayouter_Element_Text();
                            if (isset($dyn_config[(string)$child['id']])) {
                                $element->setData($dyn_config[(string)$child['id']]);
                            } else {
                                $element->setData((string)$child['value']);
                            }
                            if (isset($child['template'])) {
                                $element->setTemplate((string)$child['template']);
                            }
                            if (isset($child['render-rotate'])) {
                                $element->setRotate((int)$child['render-rotate']);
                            }
                            if (isset($child['render-font-line-height'])) {
                                $element->setLineHeight((int)$child['render-font-line-height']);
                            }
                            if (isset($child['render-font-char-spacing'])) {
                                $element->setCharSpacing((string)$child['render-font-char-spacing']);
                            }
                            if (isset($layer_data[(int)$child['render-site']][(int)$child['render-pos-layer']]['rotate'])) {
                                $element->setRotate($layer_data[(int)$child['render-site']][(int)$child['render-pos-layer']]['rotate'] * -1);
                            }
                            if (isset($dyn_config[(string)$child['id'] . '_rotate']) && $dyn_config[(string)$child['id'] . '_rotate'] != "") {
                                $element->setRotate($dyn_config[(string)$child['id'] . '_rotate']);
                            }
                            if (isset($child['render-font-color'])) {
                                $element->setFontColor((string)$child['render-font-color']);
                            }
                            if (isset($dyn_config[(string)$child['id'] . '_color']) && $dyn_config[(string)$child['id'] . '_color'] != "") {
                                $element->setFontColor($dyn_config[(string)$child['id'] . '_color']);
                            }
                            $element->setFontSize((int)$child['render-font-size']);
                            if (isset($dyn_config[(string)$child['id'] . '_size']) && $dyn_config[(string)$child['id'] . '_size'] != "") {
                                $element->setFontSize($dyn_config[(string)$child['id'] . '_size']);
                            }
                            if (isset($child['render-font-bold'])) {
                                $element->setFontBold((string)$child['render-font-bold']);
                            }
                            if (isset($child['render-auto-break'])) {
                                $element->setAutoBreak((string)$child['render-auto-break']);
                            }
                            if (isset($dyn_config[(string)$child['id'] . '_bold']) && $dyn_config[(string)$child['id'] . '_bold'] != "") {
                                $element->setFontBold($dyn_config[(string)$child['id'] . '_bold']);
                            }
                            if (isset($child['render-font-italic'])) {
                                $element->setFontItalic((string)$child['render-font-italic']);
                            }
                            if (isset($dyn_config[(string)$child['id'] . '_italic']) && $dyn_config[(string)$child['id'] . '_italic'] != "") {
                                $element->setFontItalic($dyn_config[(string)$child['id'] . '_italic']);
                            }
                            if (isset($child['render-font-underline'])) {
                                $element->setFontUnderline((string)$child['render-font-underline']);
                            }
                            if (isset($dyn_config[(string)$child['id'] . '_underline']) && $dyn_config[(string)$child['id'] . '_underline'] != "") {
                                $element->setFontUnderline($dyn_config[(string)$child['id'] . '_underline']);
                            }
                            if (isset($child['render-pos-width'])) {
                                $element->setWidth((int)$child['render-pos-width']);
                            }
                            if (isset($child['render-pos-height'])) {
                                $element->setHeight((int)$child['render-pos-height']);
                            }
                            if (isset($child['render-font-align'])) {
                                $element->setAlign((string)$child['render-font-align']);
                            }
                            if (isset($dyn_config[(string)$child['id'] . '_align']) && $dyn_config[(string)$child['id'] . '_align'] != "") {
                                $element->setAlign($dyn_config[(string)$child['id'] . '_align']);
                            }
                            if (isset($child['render-font-family'])) {
                                $element->setFontFamily((string)$child['render-font-family']);
                            }
                            if (isset($dyn_config[(string)$child['id'] . '_font']) && $dyn_config[(string)$child['id'] . '_font'] != "") {
                                $element->setFontFamily($dyn_config[(string)$child['id'] . '_font']);
                            }
                            break;
                        case "formupload":
                            $element = new TP_Steplayouter_Element_Upload();
                            if (isset($dyn_config[(string)$child['id']])) {
                                $upload = new TP_Uploadsession();
                                $image = $upload->getImage($dyn_config[(string)$child['id']]);
                                if ($image['crop']) {
                                    $element->setFile($image['crop']);
                                } else {
                                    $element->setFile($image['dir']);
                                }

                            } elseif (isset($child['value'])) {
                                $element->setValue((string)$child['value']);
                            }
                            if (isset($layer_data[(int)$child['render-site']][(int)$child['render-pos-layer']]['rotate'])) {
                                $element->setRotate($layer_data[(int)$child['render-site']][(int)$child['render-pos-layer']]['rotate'] * -1);
                            }
                            if (isset($dyn_config[(string)$child['id'] . '_rotate']) && $dyn_config[(string)$child['id'] . '_rotate'] != "") {
                                $element->setRotate($dyn_config[(string)$child['id'] . '_rotate']);
                            }
                            if (isset($child['render-print-adjust'])) {
                                $element->setRenderPrintAdjust((int)$child['render-print-adjust']);
                            }
                            $element->setWidth((int)$child['render-pos-width']);
                            $element->setHeight((int)$child['render-pos-height']);
                            break;
                        case "formimageselect":
                            $element = new TP_Steplayouter_Element_Imageselect();
                            $element->setBaseDir((string)$child['base-dir']);

                            foreach ($child->options->option as $option) {
                                if (isset($dyn_config[(string)$child['id']]) && $dyn_config[(string)$child['id']] == (string)$option['id']) {
                                    $element->setFile((string)$option['file']);
                                } elseif (!isset($dyn_config[(string)$child['id']]) && (string)$child['value'] == (string)$option['id']) {
                                    $element->setFile((string)$option['file']);
                                }
                            }

                            $element->setWidth((int)$child['render-pos-width']);
                            $element->setHeight((int)$child['render-pos-height']);
                            break;
                        case "formmotivselect":
                            $element = new TP_Steplayouter_Element_Imageselect();
                            $element->setBaseDir((string)$child['base-dir']);

                            foreach ($child->options->option as $option) {
                                Zend_Registry::get('log')->debug(print_r($option, true));
                                if (isset($dyn_config[(string)$child['id']]) && $dyn_config[(string)$child['id']] == (string)$option['id'] && (string)$option['id'] != "remove") {
                                    $motiv = Doctrine_Query::create ()->from('Motiv c')->where('c.uuid = ?', array($option['motiv']))->fetchOne();

                                    $element->setFile('market/motive/'.$motiv->file_mid);
                                } elseif (!isset($dyn_config[(string)$child['id']]) && (string)$child['value'] == (string)$option['id'] && (string)$option['id'] != "remove") {
                                    $motiv = Doctrine_Query::create ()->from('Motiv c')->where('c.uuid = ?', array($option['motiv']))->fetchOne();
                                    $element->setFile('market/motive/'.$motiv->file_mid);
                                }
                            }

                            $element->setWidth((int)$child['render-pos-width']);
                            $element->setHeight((int)$child['render-pos-height']);
                            break;
                        case "substeps":
                            $this->renderStepElements($child, $dyn_config, $layer_data);
                            break;
                        case "site-layout":
                            $value = (int)$child['value'];
                            if (isset($dyn_config[(string)$child['id']])) {
                                $value = $dyn_config[(string)$child['id']];
                            }
                            foreach ($child->options->option as $option) {
                                if((int)$option['id'] == $value) {
                                    $this->renderElements($option, $dyn_config, $layer_data);
                                }
                            }

                            break;
                    }

                    if ($element) {
                        $element->setId((string)$child['id']);
                        $element->setX((string)$child['render-pos-x']);
                        $element->setY((string)$child['render-pos-y']);
                        if (isset($child['ref'])) {
                            $element->setRef((string)$child['ref']);
                            $element->setRefValue($dyn_config[(string)$child['ref']]);

                        }
                        if (isset($child['is-active'])) {
                            $element->setIsActive((string)$child['is-active']);

                        }
                        if (isset($child['production_pdf'])) {
                            $element->setProductionPdf(intval($child['production_pdf']));
                        }
                        if (isset($child['render-pos-layer'])) {

                            if (!isset($this->_ref_layers_elements[(string)$child['id']])) {
                                $this->_ref_layers_elements[(string)$child['id']] = (int)$child['render-pos-layer'];
                            }
                            if (isset($child['ref']) && $this->_ref_layers_elements[(string)$child['ref']]) {
                                if(!isset($this->_ref_layers[$this->_ref_layers_elements[(string)$child['ref']]])) {
                                    $this->_ref_layers[$this->_ref_layers_elements[(string)$child['ref']]] = array();
                                }
                                if((int)$child['render-pos-layer'] != $this->_ref_layers_elements[(string)$child['ref']]) {
                                    $this->_ref_layers[$this->_ref_layers_elements[(string)$child['ref']]][] = (int)$child['render-pos-layer'];
                                }
                            }

                            $element->setLayer((int)$child['render-pos-layer']);
                            if (isset($layer_data[(int)$child['render-site']][(int)$child['render-pos-layer']])) {
                                if (isset($layer_data[(int)$child['render-site']][(int)$child['render-pos-layer']]['x'])) {
                                    $rel = $this->_width / $this->_base_width * $layer_data[(int)$child['render-site']][(int)$child['render-pos-layer']]['x'];
                                    Zend_Registry::get('log')->debug("REL".(int)$child['render-pos-x']."/".$rel."/".$layer_data[(int)$child['render-site']][(int)$child['render-pos-layer']]['x']."/".$this->_width."/".$this->_base_width);

                                    $element->setX((int)$child['render-pos-x'] + $rel);
                                    //$element->setRelX($rel);
                                }
                                if (isset($layer_data[(int)$child['render-site']][(int)$child['render-pos-layer']]['y'])) {
                                    $rel = $this->_height / $this->_base_height * $layer_data[(int)$child['render-site']][(int)$child['render-pos-layer']]['y'];
                                    $element->setY((int)$child['render-pos-y'] + $rel);
                                    //$element->setRelY($rel);

                                }
                            }else{
                                $element->setDirty(true);
                            }
                        } else {
                            $element->setLayer(1);
                        }

                        if (isset($child['render-pos-sort'])) {
                            $element->setSort((int)$child['render-pos-sort']);
                        }

                        if (isset($child['render-size-fixed'])) {
                            $element->setRenderSizeFixed((int)$child['render-size-fixed']);
                        }

                        if (isset($child['render-debug'])) {
                            $element->setRenderDebug((int)$child['render-debug']);
                        }

                        $element->setPageHeight($this->_height);
                        $element->setPageWidth($this->_width);
                        $this->_elements->addItem($element);
                    }
                }
            }

        }
        return $this;
    }

    public function renderElements($elements, $dyn_config, $layer_data)
    {
        foreach ($elements->elements->children() as $child) {

            if ($this->_id == (int)$child['render-site'] || (string)$child['render-mode'] == "site-layout") {
                $element = false;

                switch ((string)$child['render-mode']) {
                    case "calender":
                        $element = new TP_Steplayouter_Element_Calender();
                        $element->setTemplate((int)$child['calender-template']);

                        $calDesign = new TP_Steplayouter_Calendar_Design();
                        $calDesign->setWidth((int)$child['render-pos-width']);
                        $calDesign->setHeight((int)$child['render-pos-height']);
                        $calDesign->setMonth((int)$child['calender-month']);
                        $calDesign->setX((int)$child['render-pos-x']);

                        $calDesign->setY((int)$child['render-pos-y']);
                        $calDesign->setSiteBackground($this->getBackgroundColor());
                        if (isset($dyn_config['calender-year']) && $dyn_config['calender-year'] != "") {
                            $calDesign->setStartYear($dyn_config['calender-year']);
                        }
                        if (isset($dyn_config['calender-month']) && $dyn_config['calender-month'] != "") {
                            $calDesign->setStartMonth($dyn_config['calender-month']);
                        }
                        if (isset($dyn_config['calender-region']) && $dyn_config['calender-region'] != "") {
                            $calDesign->setPrintRegion($dyn_config['calender-region']);
                        }
                        if (isset($dyn_config['calender-holiday']) && $dyn_config['calender-holiday'] != "") {
                            $calDesign->setPrintHoliday($dyn_config['calender-holiday']);
                        }
                        if (isset($dyn_config['calender-vacation']) && $dyn_config['calender-vacation'] != "") {
                            $calDesign->setPrintVacation($dyn_config['calender-vacation']);
                        }
                        if (isset($child['render-font-family'])) {
                            $calDesign->setFontFamily((string)$child['render-font-family']);
                        }
                        if (isset($child['calender-monthsize'])) {
                            $calDesign->setMonthSize((int)$child['calender-monthsize']);
                        }
                        if (isset($child['calender-daysize'])) {
                            $calDesign->setDaySize((int)$child['calender-daysize']);
                        }
                        if (isset($child['calender-font-family-days'])) {
                            $calDesign->setFontFamilyDays((string)$child['calender-font-family-days']);
                        }
                        if (isset($child['calender-monthcolor'])) {
                            $calDesign->setMonthColor((string)$child['calender-monthcolor']);
                        }
                        if (isset($dyn_config['calender-monthcolor']) && $dyn_config['calender-monthcolor'] != "") {
                            $calDesign->setMonthColor($dyn_config['calender-monthcolor']);
                        }
                        if (isset($dyn_config['calender-daycolor']) && $dyn_config['calender-daycolor'] != "") {
                            $calDesign->setDayColor($dyn_config['calender-daycolor']);
                        }
                        if (isset($dyn_config['calender-daynamescolor']) && $dyn_config['calender-daynamescolor'] != "") {
                            $calDesign->setDayNamesColor($dyn_config['calender-daynamescolor']);
                        }
                        if (isset($dyn_config['calender-weeknamescolor']) && $dyn_config['calender-weeknamescolor'] != "") {
                            $calDesign->setWeekNamesColor($dyn_config['calender-weeknamescolor']);
                        }
                        if (isset($dyn_config['calender-holidaybackcolor']) && $dyn_config['calender-holidaybackcolor'] != "") {
                            $calDesign->setHolidayBackColor($dyn_config['calender-holidaybackcolor']);
                        }
                        if (isset($dyn_config['calender-holidaycolor']) && $dyn_config['calender-holidaycolor'] != "") {
                            $calDesign->setHolidayColor($dyn_config['calender-holidaycolor']);
                        }
                        if (isset($dyn_config['calender-sunbackcolor']) && $dyn_config['calender-sunbackcolor'] != "") {
                            $calDesign->setSunBackColor($dyn_config['calender-sunbackcolor']);
                        }
                        if (isset($dyn_config['calender-samcolor']) && $dyn_config['calender-samcolor'] != "") {
                            $calDesign->setSamColor($dyn_config['calender-samcolor']);
                        }
                        if (isset($dyn_config['calender-suncolor']) && $dyn_config['calender-suncolor'] != "") {
                            $calDesign->setSunColor($dyn_config['calender-suncolor']);
                        }
                        if (isset($dyn_config['calender-vacationcolor']) && $dyn_config['calender-vacationcolor'] != "") {
                            $calDesign->setVacationColor($dyn_config['calender-vacationcolor']);
                        }
                        $element->setCalendarDesign($calDesign);

                        break;
                    case "rte":
                        $element = new TP_Steplayouter_Element_Rte();

                        if (isset($dyn_config[(string)$child['id']])) {
                            $element->setData($dyn_config[(string)$child['id']]);
                        } else {
                            if((string)$child != "") {
                                $element->setData((string)$child);
                            }else{
                                $element->setData((string)$child['value']);
                            }
                        }
                        if (isset($layer_data[(int)$child['render-site']][(int)$child['render-pos-layer']]['rotate'])) {
                            $element->setRotate($layer_data[(int)$child['render-site']][(int)$child['render-pos-layer']]['rotate'] * -1);
                        }
                        if (isset($dyn_config[(string)$child['id'] . '_rotate']) && $dyn_config[(string)$child['id'] . '_rotate'] != "") {
                            $element->setRotate($dyn_config[(string)$child['id'] . '_rotate']);
                        }
                        if (isset($child['render-font-line-height'])) {
                            $element->setLineHeight((string)$child['render-font-line-height']);
                        }

                        $element->setWidth((int)$child['render-pos-width']);
                        $element->setHeight((int)$child['render-pos-height']);

                        break;
                    case "rectangle":
                        $element = new TP_Steplayouter_Element_Rectangle();
                        $element->setFillColor((string)$child['value']);
                        if (isset($dyn_config[(string)$child['id']]) && $dyn_config[(string)$child['id']] != "") {
                            $element->setFillColor($dyn_config[(string)$child['id']]);
                        }

                        $element->setWidth((int)$child['render-pos-width']);
                        $element->setHeight((int)$child['render-pos-height']);
                        break;
                    case "image":
                    case "motiv":
                    case "formuploadmotiv":
                        $element = new TP_Steplayouter_Element_Motiv();
                        $element->setMotivUUID((string)$child['value']);
                        if (isset($dyn_config[(string)$child['id']]) && $dyn_config[(string)$child['id']] != "") {
                            $element->setMotivUUID($dyn_config[(string)$child['id']]);
                        }
                        if (isset($dyn_config[(string)$child['id']]) && $dyn_config[(string)$child['id']] == "remove") {
                            $element->setMotivUUID(false);
                        }
                        $element->setWidth((int)$child['render-pos-width']);
                        $element->setHeight((int)$child['render-pos-height']);
                        if (isset($child['render-rotate'])) {
                            $element->setRotate((int)$child['render-rotate']);
                        }
                        if (isset($layer_data[(int)$child['render-site']][(int)$child['render-pos-layer']]['rotate'])) {
                            $element->setRotate($layer_data[(int)$child['render-site']][(int)$child['render-pos-layer']]['rotate'] * -1);
                        }
                        if (isset($dyn_config[(string)$child['id'] . '_crop']) && $dyn_config[(string)$child['id'] . '_crop'] != "") {
                            $element->setCrop($dyn_config[(string)$child['id'] . '_crop']);
                        }
                        if (isset($dyn_config[(string)$child['id'] . '_rotate']) && $dyn_config[(string)$child['id'] . '_rotate'] != "") {
                            $element->setRotate($dyn_config[(string)$child['id'] . '_rotate']);
                        }
                        if (isset($layer_data[(int)$child['render-site']][(int)$child['render-pos-layer']]['scale'])) {
                            $element->setScale($layer_data[(int)$child['render-site']][(int)$child['render-pos-layer']]['scale']);
                        }
                        if (isset($dyn_config[(string)$child['id'] . '_scale']) && $dyn_config[(string)$child['id'] . '_scale'] != "") {
                            $element->setScale($dyn_config[(string)$child['id'] . '_scale']);
                        }
                        if (isset($child['render-print-adjust'])) {
                            $element->setRenderPrintAdjust((int)$child['render-print-adjust']);
                        }
                        break;
                    case "textarea":
                        $element = new TP_Steplayouter_Element_Textarea();
                        if (isset($dyn_config[(string)$child['id']])) {
                            $element->setData($dyn_config[(string)$child['id']]);
                        } else {
                            $element->setData((string)$child['value']);
                        }
                        if (isset($layer_data[(int)$child['render-site']][(int)$child['render-pos-layer']]['rotate'])) {
                            $element->setRotate($layer_data[(int)$child['render-site']][(int)$child['render-pos-layer']]['rotate'] * -1);
                        }
                        if (isset($dyn_config[(string)$child['id'] . '_rotate']) && $dyn_config[(string)$child['id'] . '_rotate'] != "") {
                            $element->setRotate($dyn_config[(string)$child['id'] . '_rotate']);
                        }
                        if (isset($child['render-font-color'])) {
                            $element->setFontColor((string)$child['render-font-color']);
                        }
                        if (isset($child['render-font-line-height'])) {
                            $element->setLineHeight((int)$child['render-font-line-height']);
                        }
                        if (isset($dyn_config[(string)$child['id'] . '_color']) && $dyn_config[(string)$child['id'] . '_color'] != "") {
                            $element->setFontColor($dyn_config[(string)$child['id'] . '_color']);
                        }
                        $element->setFontSize((int)$child['render-font-size']);
                        if (isset($dyn_config[(string)$child['id'] . '_size']) && $dyn_config[(string)$child['id'] . '_size'] != "" && $dyn_config[(string)$child['id'] . '_size'] != "0") {
                            $element->setFontSize($dyn_config[(string)$child['id'] . '_size']);
                        }
                        $element->setWidth((int)$child['render-pos-width']);
                        $element->setHeight((int)$child['render-pos-height']);
                        if (isset($child['render-font-bold'])) {
                            $element->setFontBold((string)$child['render-font-bold']);
                        }
                        if (isset($dyn_config[(string)$child['id'] . '_bold']) && $dyn_config[(string)$child['id'] . '_bold'] != "") {
                            $element->setFontBold($dyn_config[(string)$child['id'] . '_bold']);
                        }
                        if (isset($child['render-font-italic'])) {
                            $element->setFontItalic((string)$child['render-font-italic']);
                        }
                        if (isset($dyn_config[(string)$child['id'] . '_italic']) && $dyn_config[(string)$child['id'] . '_italic'] != "") {
                            $element->setFontItalic($dyn_config[(string)$child['id'] . '_italic']);
                        }
                        if (isset($child['render-font-underline'])) {
                            $element->setFontUnderline((string)$child['render-font-underline']);
                        }
                        if (isset($dyn_config[(string)$child['id'] . '_underline']) && $dyn_config[(string)$child['id'] . '_underline'] != "") {
                            $element->setFontUnderline($dyn_config[(string)$child['id'] . '_underline']);
                        }
                        if (isset($child['render-font-family'])) {
                            $element->setFontFamily((string)$child['render-font-family']);
                        }
                        if (isset($dyn_config[(string)$child['id'] . '_font']) && $dyn_config[(string)$child['id'] . '_font'] != "") {
                            $element->setFontFamily($dyn_config[(string)$child['id'] . '_font']);
                        }
                        if (isset($child['render-font-align'])) {
                            $element->setAlign((string)$child['render-font-align']);
                        }
                        if (isset($dyn_config[(string)$child['id'] . '_align']) && $dyn_config[(string)$child['id'] . '_align'] != "") {
                            $element->setAlign($dyn_config[(string)$child['id'] . '_align']);
                        }
                        break;
                    case "formtext":
                    case "text":
                    case "richtext":
                        if((string)$child['id'] == "sponsord-by-text" && (!isset($dyn_config["sponsord-by"]) || $dyn_config["sponsord-by"] == "remove"  || $dyn_config["sponsord-by"] == "")) {
                            continue;
                        }
                        $element = new TP_Steplayouter_Element_Text();
                        if (isset($dyn_config[(string)$child['id']])) {
                            $element->setData($dyn_config[(string)$child['id']]);
                        } else {
                            $element->setData((string)$child['value']);
                        }
                        if (isset($child['template'])) {
                            $element->setTemplate((string)$child['template']);
                        }
                        if (isset($child['render-rotate'])) {
                            $element->setRotate((int)$child['render-rotate']);
                        }
                        if (isset($layer_data[(int)$child['render-site']][(int)$child['render-pos-layer']]['rotate'])) {
                            $element->setRotate($layer_data[(int)$child['render-site']][(int)$child['render-pos-layer']]['rotate'] * -1);
                        }
                        if (isset($dyn_config[(string)$child['id'] . '_rotate']) && $dyn_config[(string)$child['id'] . '_rotate'] != "") {
                            $element->setRotate($dyn_config[(string)$child['id'] . '_rotate']);
                        }
                        if (isset($child['render-font-color'])) {
                            $element->setFontColor((string)$child['render-font-color']);
                        }
                        if (isset($dyn_config[(string)$child['id'] . '_color']) && $dyn_config[(string)$child['id'] . '_color'] != "") {
                            $element->setFontColor($dyn_config[(string)$child['id'] . '_color']);
                        }
                        $element->setFontSize((int)$child['render-font-size']);
                        if (isset($dyn_config[(string)$child['id'] . '_size']) && $dyn_config[(string)$child['id'] . '_size'] != "") {
                            $element->setFontSize($dyn_config[(string)$child['id'] . '_size']);
                        }
                        if (isset($child['render-font-bold'])) {
                            $element->setFontBold((string)$child['render-font-bold']);
                        }
                        if (isset($dyn_config[(string)$child['id'] . '_bold']) && $dyn_config[(string)$child['id'] . '_bold'] != "") {
                            $element->setFontBold($dyn_config[(string)$child['id'] . '_bold']);
                        }
                        if (isset($child['render-font-italic'])) {
                            $element->setFontItalic((string)$child['render-font-italic']);
                        }
                        if (isset($dyn_config[(string)$child['id'] . '_italic']) && $dyn_config[(string)$child['id'] . '_italic'] != "") {
                            $element->setFontItalic($dyn_config[(string)$child['id'] . '_italic']);
                        }
                        if (isset($child['render-font-underline'])) {
                            $element->setFontUnderline((string)$child['render-font-underline']);
                        }
                        if (isset($dyn_config[(string)$child['id'] . '_underline']) && $dyn_config[(string)$child['id'] . '_underline'] != "") {
                            $element->setFontUnderline($dyn_config[(string)$child['id'] . '_underline']);
                        }
                        if (isset($child['render-pos-width'])) {
                            $element->setWidth((int)$child['render-pos-width']);
                        }
                        if (isset($child['render-font-align'])) {
                            $element->setAlign((string)$child['render-font-align']);
                        }
                        if (isset($dyn_config[(string)$child['id'] . '_align']) && $dyn_config[(string)$child['id'] . '_align'] != "") {
                            $element->setAlign($dyn_config[(string)$child['id'] . '_align']);
                        }
                        if (isset($child['render-font-family'])) {
                            $element->setFontFamily((string)$child['render-font-family']);
                        }
                        if (isset($dyn_config[(string)$child['id'] . '_font']) && $dyn_config[(string)$child['id'] . '_font'] != "") {
                            $element->setFontFamily($dyn_config[(string)$child['id'] . '_font']);
                        }
                        break;
                    case "formupload":
                        $element = new TP_Steplayouter_Element_Upload();
                        if (isset($dyn_config[(string)$child['id']])) {
                            $upload = new TP_Uploadsession();
                            $image = $upload->getImage($dyn_config[(string)$child['id']]);
                            if ($image['crop']) {
                                $element->setFile($image['crop']);
                            } else {
                                $element->setFile($image['dir']);
                            }

                        } elseif (isset($child['value'])) {
                            $element->setValue((string)$child['value']);
                        }
                        if (isset($layer_data[(int)$child['render-site']][(int)$child['render-pos-layer']]['rotate'])) {
                            $element->setRotate($layer_data[(int)$child['render-site']][(int)$child['render-pos-layer']]['rotate'] * -1);
                        }
                        if (isset($dyn_config[(string)$child['id'] . '_rotate']) && $dyn_config[(string)$child['id'] . '_rotate'] != "") {
                            $element->setRotate($dyn_config[(string)$child['id'] . '_rotate']);
                        }
                        $element->setWidth((int)$child['render-pos-width']);
                        $element->setHeight((int)$child['render-pos-height']);
                        if (isset($child['render-print-adjust'])) {
                            $element->setRenderPrintAdjust((int)$child['render-print-adjust']);
                        }
                        break;
                    case "formmotivselect":
                        $element = new TP_Steplayouter_Element_Imageselect();
                        $element->setBaseDir((string)$child['base-dir']);

                        foreach ($child->options->option as $option) {
                            if (isset($dyn_config[(string)$child['id']]) && $dyn_config[(string)$child['id']] == (string)$option['id'] && (string)$option['id'] != "remove") {
                                $motiv = Doctrine_Query::create ()->from('Motiv c')->where('c.uuid = ?', array($option['motiv']))->fetchOne();
                                $element->setFile('market/motive/'.$motiv->file_mid);
                            } elseif (!isset($dyn_config[(string)$child['id']]) && (string)$child['value'] == (string)$option['id'] && (string)$option['id'] != "remove") {
                                $motiv = Doctrine_Query::create ()->from('Motiv c')->where('c.uuid = ?', array($option['motiv']))->fetchOne();
                                $element->setFile('market/motive/'.$motiv->file_mid);
                            }
                        }

                        $element->setWidth((int)$child['render-pos-width']);
                        $element->setHeight((int)$child['render-pos-height']);
                        break;
                    case "formimageselect":
                        $element = new TP_Steplayouter_Element_Imageselect();
                        $element->setBaseDir((string)$child['base-dir']);

                        foreach ($child->options->option as $option) {
                            if (isset($dyn_config[(string)$child['id']]) && $dyn_config[(string)$child['id']] == (string)$option['id']) {
                                $element->setFile((string)$option['file']);
                            } elseif (!isset($dyn_config[(string)$child['id']]) && (string)$child['value'] == (string)$option['id']) {
                                $element->setFile((string)$option['file']);
                            }
                        }

                        $element->setWidth((int)$child['render-pos-width']);
                        $element->setHeight((int)$child['render-pos-height']);
                        break;

                }

                if ($element) {
                    $element->setId((string)$child['id']);
                    $element->setX((int)$child['render-pos-x']);

                    $element->setY((int)$child['render-pos-y']);
                    if (isset($child['ref'])) {
                        $element->setRef((string)$child['ref']);
                        $element->setRefValue($dyn_config[(string)$child['ref']]);
                    }
                    if (isset($child['is-active'])) {
                        $element->setIsActive((string)$child['is-active']);

                    }
                    if (isset($child['production_pdf'])) {
                        $element->setProductionPdf(intval($child['production_pdf']));
                    }

                    if (isset($child['render-pos-layer'])) {

                        if (!isset($this->_ref_layers_elements[(string)$child['id']])) {
                            $this->_ref_layers_elements[(string)$child['id']] = (int)$child['render-pos-layer'];
                        }
                        if (isset($child['ref']) && $this->_ref_layers_elements[(string)$child['ref']]) {
                            if(!isset($this->_ref_layers[$this->_ref_layers_elements[(string)$child['ref']]])) {
                                $this->_ref_layers[$this->_ref_layers_elements[(string)$child['ref']]] = array();
                            }
                            if((int)$child['render-pos-layer'] != $this->_ref_layers_elements[(string)$child['ref']]) {
                                $this->_ref_layers[$this->_ref_layers_elements[(string)$child['ref']]][] = (int)$child['render-pos-layer'];
                            }
                        }

                        $element->setLayer((int)$child['render-pos-layer']);
                        if (isset($layer_data[(int)$child['render-site']][(int)$child['render-pos-layer']])) {
                            if (isset($layer_data[(int)$child['render-site']][(int)$child['render-pos-layer']]['x'])) {
                                $rel = $this->_width / $this->_base_width * $layer_data[(int)$child['render-site']][(int)$child['render-pos-layer']]['x'];
                                $element->setX((int)$child['render-pos-x'] + $rel);
                                //$element->setRelX($rel);
                            }
                            if (isset($layer_data[(int)$child['render-site']][(int)$child['render-pos-layer']]['y'])) {
                                $rel = $this->_height / $this->_base_height * $layer_data[(int)$child['render-site']][(int)$child['render-pos-layer']]['y'];
                                $element->setY((int)$child['render-pos-y'] + $rel);
                                //$element->setRelY($rel);
                            }
                        }else{
                            $element->setDirty(true);
                        }
                    } else {
                        $element->setLayer(1);
                    }

                    if (isset($child['render-pos-sort'])) {
                        $element->setSort((int)$child['render-pos-sort']);
                    }

                    if (isset($child['render-size-fixed'])) {
                        $element->setRenderSizeFixed((int)$child['render-size-fixed']);
                    }

                    if (isset($child['render-debug'])) {
                        $element->setRenderDebug((int)$child['render-debug']);
                    }

                    $element->setPageHeight($this->_height);
                    $element->setPageWidth($this->_width);
                    $this->_elements->addItem($element);
                }
            }
        }
    }

    public function renderStepElements($step, $dyn_config, $layer_data)
    {

        foreach ($step->steps->children() as $childs) {

            foreach ($childs->elements->children() as $child) {

                if ($this->_id == (int)$child['render-site'] || (string)$child['render-mode'] == "site-layout") {
                    $element = false;

                    switch ((string)$child['render-mode']) {
                        case "calender":
                            $element = new TP_Steplayouter_Element_Calender();
                            $element->setTemplate((int)$child['calender-template']);

                            $calDesign = new TP_Steplayouter_Calendar_Design();
                            $calDesign->setWidth((int)$child['render-pos-width']);
                            $calDesign->setHeight((int)$child['render-pos-height']);
                            $calDesign->setMonth((int)$child['calender-month']);
                            $calDesign->setX((int)$child['render-pos-x']);

                            $calDesign->setY((int)$child['render-pos-y']);
                            $calDesign->setSiteBackground($this->getBackgroundColor());
                            if (isset($child['calender-year']) && $child['calender-year'] != "") {
                                $calDesign->setStartYear(intval($child['calender-year']));
                            }
                            if (isset($child['calender-month']) && $child['calender-month'] != "") {
                                $calDesign->setMonth(intval($child['calender-month']));
                            }
                            if (isset($child['calender-region']) && $child['calender-region'] != "") {
                                $calDesign->setPrintRegion($child['calender-region']);
                            }
                            if (isset($child['calender-holiday']) && $child['calender-holiday'] != "") {
                                $calDesign->setPrintHoliday($child['calender-holiday']);
                            }
                            if (isset($child['calender-vacation']) && $child['calender-vacation'] != "") {
                                $calDesign->setPrintVacation($child['calender-vacation']);
                            }
                            if (isset($child['render-font-family'])) {
                                $calDesign->setFontFamily((string)$child['render-font-family']);
                            }
                            if (isset($child['calender-monthsize'])) {
                                $calDesign->setMonthSize((int)$child['calender-monthsize']);
                            }
                            if (isset($child['calender-daysize'])) {
                                $calDesign->setDaySize((int)$child['calender-daysize']);
                            }
                            if (isset($child['calender-monthcolor'])) {
                                $calDesign->setMonthColor((string)$child['calender-monthcolor']);
                            }
                            if (isset($child['calender-font-family-days'])) {
                                $calDesign->setFontFamilyDays((string)$child['calender-font-family-days']);
                            }
                            if (isset($dyn_config['calender-monthcolor']) && $dyn_config['calender-monthcolor'] != "") {
                                $calDesign->setMonthColor($dyn_config['calender-monthcolor']);
                            }
                            if (isset($dyn_config['calender-daycolor']) && $dyn_config['calender-daycolor'] != "") {
                                $calDesign->setDayColor($dyn_config['calender-daycolor']);
                            }
                            if (isset($dyn_config['calender-daynamescolor']) && $dyn_config['calender-daynamescolor'] != "") {
                                $calDesign->setDayNamesColor($dyn_config['calender-daynamescolor']);
                            }
                            if (isset($dyn_config['calender-weeknamescolor']) && $dyn_config['calender-weeknamescolor'] != "") {
                                $calDesign->setWeekNamesColor($dyn_config['calender-weeknamescolor']);
                            }
                            if (isset($dyn_config['calender-holidaybackcolor']) && $dyn_config['calender-holidaybackcolor'] != "") {
                                $calDesign->setHolidayBackColor($dyn_config['calender-holidaybackcolor']);
                            }
                            if (isset($dyn_config['calender-holidaycolor']) && $dyn_config['calender-holidaycolor'] != "") {
                                $calDesign->setHolidayColor($dyn_config['calender-holidaycolor']);
                            }
                            if (isset($dyn_config['calender-sunbackcolor']) && $dyn_config['calender-sunbackcolor'] != "") {
                                $calDesign->setSunBackColor($dyn_config['calender-sunbackcolor']);
                            }
                            if (isset($dyn_config['calender-samcolor']) && $dyn_config['calender-samcolor'] != "") {
                                $calDesign->setSamColor($dyn_config['calender-samcolor']);
                            }
                            if (isset($dyn_config['calender-suncolor']) && $dyn_config['calender-suncolor'] != "") {
                                $calDesign->setSunColor($dyn_config['calender-suncolor']);
                            }
                            if (isset($dyn_config['calender-vacationcolor']) && $dyn_config['calender-vacationcolor'] != "") {
                                $calDesign->setVacationColor($dyn_config['calender-vacationcolor']);
                            }
                            $element->setCalendarDesign($calDesign);

                            break;
                        case "rte":
                            $element = new TP_Steplayouter_Element_Rte();

                            if (isset($dyn_config[(string)$child['id']])) {
                                $element->setData($dyn_config[(string)$child['id']]);
                            } else {
                                if((string)$child != "") {
                                    $element->setData((string)$child);
                                }else{
                                    $element->setData((string)$child['value']);
                                }
                            }
                            if (isset($layer_data[(int)$child['render-site']][(int)$child['render-pos-layer']]['rotate'])) {
                                $element->setRotate($layer_data[(int)$child['render-site']][(int)$child['render-pos-layer']]['rotate'] * -1);
                            }
                            if (isset($dyn_config[(string)$child['id'] . '_rotate']) && $dyn_config[(string)$child['id'] . '_rotate'] != "") {
                                $element->setRotate($dyn_config[(string)$child['id'] . '_rotate']);
                            }
                            if (isset($child['render-font-line-height'])) {
                                $element->setLineHeight((string)$child['render-font-line-height']);
                            }

                            $element->setWidth((int)$child['render-pos-width']);
                            $element->setHeight((int)$child['render-pos-height']);

                            break;
                        case "rectangle":
                            $element = new TP_Steplayouter_Element_Rectangle();
                            $element->setFillColor((string)$child['value']);
                            if (isset($dyn_config[(string)$child['id']]) && $dyn_config[(string)$child['id']] != "") {
                                $element->setFillColor($dyn_config[(string)$child['id']]);
                            }

                            $element->setWidth((int)$child['render-pos-width']);
                            $element->setHeight((int)$child['render-pos-height']);
                            break;
                        case "image":
                        case "motiv":
                        case "formuploadmotiv":
                            $element = new TP_Steplayouter_Element_Motiv();
                            $element->setMotivUUID((string)$child['value']);
                            if (isset($dyn_config[(string)$child['id']]) && $dyn_config[(string)$child['id']] != "") {
                                $element->setMotivUUID($dyn_config[(string)$child['id']]);
                            }
                            if (isset($dyn_config[(string)$child['id']]) && $dyn_config[(string)$child['id']] == "remove") {
                                $element->setMotivUUID(false);
                            }
                            $element->setWidth((int)$child['render-pos-width']);
                            $element->setHeight((int)$child['render-pos-height']);
                            if (isset($child['render-rotate'])) {
                                $element->setRotate((int)$child['render-rotate']);
                            }
                            if (isset($layer_data[(int)$child['render-site']][(int)$child['render-pos-layer']]['rotate'])) {
                                $element->setRotate($layer_data[(int)$child['render-site']][(int)$child['render-pos-layer']]['rotate'] * -1);
                            }
                            if (isset($dyn_config[(string)$child['id'] . '_crop']) && $dyn_config[(string)$child['id'] . '_crop'] != "") {
                                $element->setCrop($dyn_config[(string)$child['id'] . '_crop']);
                            }
                            if (isset($dyn_config[(string)$child['id'] . '_rotate']) && $dyn_config[(string)$child['id'] . '_rotate'] != "") {
                                $element->setRotate($dyn_config[(string)$child['id'] . '_rotate']);
                            }
                            if (isset($layer_data[(int)$child['render-site']][(int)$child['render-pos-layer']]['scale'])) {
                                $element->setScale($layer_data[(int)$child['render-site']][(int)$child['render-pos-layer']]['scale']);
                            }
                            if (isset($dyn_config[(string)$child['id'] . '_scale']) && $dyn_config[(string)$child['id'] . '_scale'] != "") {
                                $element->setScale($dyn_config[(string)$child['id'] . '_scale']);
                            }
                            if (isset($child['render-print-adjust'])) {
                                $element->setRenderPrintAdjust((int)$child['render-print-adjust']);
                            }
                            break;
                        case "textarea":
                            $element = new TP_Steplayouter_Element_Textarea();
                            if (isset($dyn_config[(string)$child['id']])) {
                                $element->setData($dyn_config[(string)$child['id']]);
                            } else {
                                $element->setData((string)$child['value']);
                            }
                            if (isset($layer_data[(int)$child['render-site']][(int)$child['render-pos-layer']]['rotate'])) {
                                $element->setRotate($layer_data[(int)$child['render-site']][(int)$child['render-pos-layer']]['rotate'] * -1);
                            }
                            if (isset($dyn_config[(string)$child['id'] . '_rotate']) && $dyn_config[(string)$child['id'] . '_rotate'] != "") {
                                $element->setRotate($dyn_config[(string)$child['id'] . '_rotate']);
                            }
                            if (isset($child['render-font-color'])) {
                                $element->setFontColor((string)$child['render-font-color']);
                            }
                            if (isset($dyn_config[(string)$child['id'] . '_color']) && $dyn_config[(string)$child['id'] . '_color'] != "") {
                                $element->setFontColor($dyn_config[(string)$child['id'] . '_color']);
                            }
                            $element->setFontSize((int)$child['render-font-size']);
                            if (isset($dyn_config[(string)$child['id'] . '_size']) && $dyn_config[(string)$child['id'] . '_size'] != "" && $dyn_config[(string)$child['id'] . '_size'] != "0") {
                                $element->setFontSize($dyn_config[(string)$child['id'] . '_size']);
                            }
                            $element->setWidth((int)$child['render-pos-width']);
                            $element->setHeight((int)$child['render-pos-height']);
                            if (isset($child['render-font-bold'])) {
                                $element->setFontBold((string)$child['render-font-bold']);
                            }
                            if (isset($dyn_config[(string)$child['id'] . '_bold']) && $dyn_config[(string)$child['id'] . '_bold'] != "") {
                                $element->setFontBold($dyn_config[(string)$child['id'] . '_bold']);
                            }
                            if (isset($child['render-font-italic'])) {
                                $element->setFontItalic((string)$child['render-font-italic']);
                            }
                            if (isset($dyn_config[(string)$child['id'] . '_italic']) && $dyn_config[(string)$child['id'] . '_italic'] != "") {
                                $element->setFontItalic($dyn_config[(string)$child['id'] . '_italic']);
                            }
                            if (isset($child['render-font-underline'])) {
                                $element->setFontUnderline((string)$child['render-font-underline']);
                            }
                            if (isset($dyn_config[(string)$child['id'] . '_underline']) && $dyn_config[(string)$child['id'] . '_underline'] != "") {
                                $element->setFontUnderline($dyn_config[(string)$child['id'] . '_underline']);
                            }
                            if (isset($child['render-font-family'])) {
                                $element->setFontFamily((string)$child['render-font-family']);
                            }
                            if (isset($dyn_config[(string)$child['id'] . '_font']) && $dyn_config[(string)$child['id'] . '_font'] != "") {
                                $element->setFontFamily($dyn_config[(string)$child['id'] . '_font']);
                            }
                            if (isset($child['render-font-line-height'])) {
                                $element->setLineHeight((int)$child['render-font-line-height']);
                            }
                            if (isset($child['render-font-align'])) {
                                $element->setAlign((string)$child['render-font-align']);
                            }
                            if (isset($dyn_config[(string)$child['id'] . '_align']) && $dyn_config[(string)$child['id'] . '_align'] != "") {
                                $element->setAlign($dyn_config[(string)$child['id'] . '_align']);
                            }
                            break;
                        case "formtext":
                        case "text":
                        case "richtext":
                            if((string)$child['id'] == "sponsord-by-text" && (!isset($dyn_config["sponsord-by"]) || $dyn_config["sponsord-by"] == "remove"  || $dyn_config["sponsord-by"] == "")) {
                                continue;
                            }
                            $element = new TP_Steplayouter_Element_Text();
                            if (isset($dyn_config[(string)$child['id']])) {
                                $element->setData($dyn_config[(string)$child['id']]);
                            } else {
                                $element->setData((string)$child['value']);
                            }
                            if (isset($child['render-rotate'])) {
                                $element->setRotate((int)$child['render-rotate']);
                            }
                            if (isset($layer_data[(int)$child['render-site']][(int)$child['render-pos-layer']]['rotate'])) {
                                $element->setRotate($layer_data[(int)$child['render-site']][(int)$child['render-pos-layer']]['rotate'] * -1);
                            }
                            if (isset($dyn_config[(string)$child['id'] . '_rotate']) && $dyn_config[(string)$child['id'] . '_rotate'] != "") {
                                $element->setRotate($dyn_config[(string)$child['id'] . '_rotate']);
                            }
                            if (isset($child['render-font-color'])) {
                                $element->setFontColor((string)$child['render-font-color']);
                            }
                            if (isset($dyn_config[(string)$child['id'] . '_color']) && $dyn_config[(string)$child['id'] . '_color'] != "") {
                                $element->setFontColor($dyn_config[(string)$child['id'] . '_color']);
                            }
                            $element->setFontSize((int)$child['render-font-size']);
                            if (isset($dyn_config[(string)$child['id'] . '_size']) && $dyn_config[(string)$child['id'] . '_size'] != "") {
                                $element->setFontSize($dyn_config[(string)$child['id'] . '_size']);
                            }
                            if (isset($child['render-font-bold'])) {
                                $element->setFontBold((string)$child['render-font-bold']);
                            }
                            if (isset($dyn_config[(string)$child['id'] . '_bold']) && $dyn_config[(string)$child['id'] . '_bold'] != "") {
                                $element->setFontBold($dyn_config[(string)$child['id'] . '_bold']);
                            }
                            if (isset($child['render-font-italic'])) {
                                $element->setFontItalic((string)$child['render-font-italic']);
                            }
                            if (isset($dyn_config[(string)$child['id'] . '_italic']) && $dyn_config[(string)$child['id'] . '_italic'] != "") {
                                $element->setFontItalic($dyn_config[(string)$child['id'] . '_italic']);
                            }
                            if (isset($child['render-font-underline'])) {
                                $element->setFontUnderline((string)$child['render-font-underline']);
                            }
                            if (isset($dyn_config[(string)$child['id'] . '_underline']) && $dyn_config[(string)$child['id'] . '_underline'] != "") {
                                $element->setFontUnderline($dyn_config[(string)$child['id'] . '_underline']);
                            }
                            if (isset($child['render-pos-width'])) {
                                $element->setWidth((int)$child['render-pos-width']);
                            }
                            if (isset($child['render-font-align'])) {
                                $element->setAlign((string)$child['render-font-align']);
                            }
                            if (isset($dyn_config[(string)$child['id'] . '_align']) && $dyn_config[(string)$child['id'] . '_align'] != "") {
                                $element->setAlign($dyn_config[(string)$child['id'] . '_align']);
                            }
                            if (isset($child['render-font-family'])) {
                                $element->setFontFamily((string)$child['render-font-family']);
                            }
                            if (isset($dyn_config[(string)$child['id'] . '_font']) && $dyn_config[(string)$child['id'] . '_font'] != "") {
                                $element->setFontFamily($dyn_config[(string)$child['id'] . '_font']);
                            }
                            break;
                        case "formupload":
                            $element = new TP_Steplayouter_Element_Upload();
                            if (isset($dyn_config[(string)$child['id']])) {
                                $upload = new TP_Uploadsession();
                                $image = $upload->getImage($dyn_config[(string)$child['id']]);
                                if ($image['crop']) {
                                    $element->setFile($image['crop']);
                                } else {
                                    $element->setFile($image['dir']);
                                }

                            } elseif (isset($child['value'])) {
                                $element->setValue((string)$child['value']);
                            }
                            if (isset($layer_data[(int)$child['render-site']][(int)$child['render-pos-layer']]['rotate'])) {
                                $element->setRotate($layer_data[(int)$child['render-site']][(int)$child['render-pos-layer']]['rotate'] * -1);
                            }
                            if (isset($dyn_config[(string)$child['id'] . '_rotate']) && $dyn_config[(string)$child['id'] . '_rotate'] != "") {
                                $element->setRotate($dyn_config[(string)$child['id'] . '_rotate']);
                            }
                            $element->setWidth((int)$child['render-pos-width']);
                            $element->setHeight((int)$child['render-pos-height']);
                            if (isset($child['render-print-adjust'])) {
                                $element->setRenderPrintAdjust((int)$child['render-print-adjust']);
                            }
                            break;
                        case "formmotivselect":
                            $element = new TP_Steplayouter_Element_Imageselect();
                            $element->setBaseDir((string)$child['base-dir']);

                            foreach ($child->options->option as $option) {
                                if (isset($dyn_config[(string)$child['id']]) && $dyn_config[(string)$child['id']] == (string)$option['id'] && (string)$option['id'] != "remove") {
                                    $motiv = Doctrine_Query::create ()->from('Motiv c')->where('c.uuid = ?', array($option['motiv']))->fetchOne();
                                    $element->setFile('market/motive/'.$motiv->file_mid);
                                } elseif (!isset($dyn_config[(string)$child['id']]) && (string)$child['value'] == (string)$option['id'] && (string)$option['id'] != "remove") {
                                    $motiv = Doctrine_Query::create ()->from('Motiv c')->where('c.uuid = ?', array($option['motiv']))->fetchOne();
                                    $element->setFile('market/motive/'.$motiv->file_mid);
                                }
                            }

                            $element->setWidth((int)$child['render-pos-width']);
                            $element->setHeight((int)$child['render-pos-height']);
                            break;
                        case "formimageselect":
                            $element = new TP_Steplayouter_Element_Imageselect();
                            $element->setBaseDir((string)$child['base-dir']);

                            foreach ($child->options->option as $option) {
                                if (isset($dyn_config[(string)$child['id']]) && $dyn_config[(string)$child['id']] == (string)$option['id']) {
                                    $element->setFile((string)$option['file']);
                                } elseif (!isset($dyn_config[(string)$child['id']]) && (string)$child['value'] == (string)$option['id']) {
                                    $element->setFile((string)$option['file']);
                                }
                            }

                            $element->setWidth((int)$child['render-pos-width']);
                            $element->setHeight((int)$child['render-pos-height']);
                            break;
                        case "site-layout":
                            $value = (int)$child['value'];
                            if (isset($dyn_config[(string)$child['id']])) {
                                $value = $dyn_config[(string)$child['id']];
                            }
                            foreach ($child->options->option as $option) {
                                if((int)$option['id'] == $value) {
                                    $this->renderElements($option, $dyn_config, $layer_data);
                                }
                            }

                            break;

                    }

                    if ($element) {
                        $element->setId((string)$child['id']);
                        $element->setX((int)$child['render-pos-x']);

                        $element->setY((int)$child['render-pos-y']);
                        if (isset($child['ref'])) {
                            $element->setRef((string)$child['ref']);
                            if(!isset($dyn_config[(string)$child['ref']])) {
                                $element->setRefValue($this->_elements->getItemByObjectId((string)$child['ref'])->getValue());
                            }else{
                                $element->setRefValue($dyn_config[(string)$child['ref']]);
                            }
                        }
                        if (isset($child['production_pdf'])) {
                            $element->setProductionPdf(intval($child['production_pdf']));
                        }
                        if (isset($child['render-pos-layer'])) {

                            if (!isset($this->_ref_layers_elements[(string)$child['id']])) {
                                $this->_ref_layers_elements[(string)$child['id']] = (int)$child['render-pos-layer'];
                            }
                            if (isset($child['ref']) && $this->_ref_layers_elements[(string)$child['ref']]) {
                                if(!isset($this->_ref_layers[$this->_ref_layers_elements[(string)$child['ref']]])) {
                                    $this->_ref_layers[$this->_ref_layers_elements[(string)$child['ref']]] = array();
                                }
                                if((int)$child['render-pos-layer'] != $this->_ref_layers_elements[(string)$child['ref']]) {
                                    $this->_ref_layers[$this->_ref_layers_elements[(string)$child['ref']]][] = (int)$child['render-pos-layer'];
                                }
                            }

                            $element->setLayer((int)$child['render-pos-layer']);
                            if (isset($layer_data[(int)$child['render-site']][(int)$child['render-pos-layer']])) {
                                if (isset($layer_data[(int)$child['render-site']][(int)$child['render-pos-layer']]['x'])) {
                                    $rel = $this->_width / $this->_base_width * $layer_data[(int)$child['render-site']][(int)$child['render-pos-layer']]['x'];
                                    $element->setX((int)$child['render-pos-x'] + $rel);
                                    //$element->setRelX($rel);
                                }
                                if (isset($layer_data[(int)$child['render-site']][(int)$child['render-pos-layer']]['y'])) {
                                    $rel = $this->_height / $this->_base_height * $layer_data[(int)$child['render-site']][(int)$child['render-pos-layer']]['y'];
                                    $element->setY((int)$child['render-pos-y'] + $rel);
                                    //$element->setRelY($rel);
                                }
                            }else{
                                $element->setDirty(true);
                            }
                        } else {
                            $element->setLayer(1);
                        }

                        if (isset($child['render-pos-sort'])) {
                            $element->setSort((int)$child['render-pos-sort']);
                        }

                        if (isset($child['render-size-fixed'])) {
                            $element->setRenderSizeFixed((int)$child['render-size-fixed']);
                        }

                        if (isset($child['render-debug'])) {
                            $element->setRenderDebug((int)$child['render-debug']);
                        }

                        $element->setPageHeight($this->_height);
                        $element->setPageWidth($this->_width);
                        $this->_elements->addItem($element);
                    }
                }
            }

        }
    }

    public function buildPDFLibProduction($p, $layers = array())
    {
        $p->set_parameter("topdown", "true");

         $p->begin_page_ext(0, 0, "width=" . ($this->getWidth() + $this->getTrimLeft() + $this->getTrimRight()) . " height="
             . ($this->getHeight() + $this->getTrimBottom() + $this->getTrimTop()) . " trimbox={" . $this->getTrimLeft() . " "
             . $this->getTrimTop() . " " . ($this->getWidth() + $this->getTrimLeft()) . " " . ($this->getHeight() + $this->getTrimTop()) . "}");


        //$p->begin_page_ext($this->_width, $this->_height, "");

        if($this->getBackgroundColor() != "#ffffff") {
            $rgb = $this->hex2rgb($this->getBackgroundColor());
            $p->setcolor("fill", "rgb", $rgb['r'], $rgb['g'], $rgb['b'], 0.0);
            /* Define a rectangle */
            $p->rect(0, $this->getHeight() + $this->getTrimTop() + $this->getTrimBottom(), $this->getWidth() + $this->getTrimLeft() + $this->getTrimRight(), $this->getHeight() + $this->getTrimTop() + $this->getTrimBottom());
            $p->fill();
        }

        $layerTemp = array();
        foreach($layers as $key => $layer) {
            if($layer['clipbox'] != "") {
                $cp = explode(" ", $layer['clipbox']);
                var_dump($cp);
               //

                $cp[2] = $cp[2] + $this->getTrimLeft() + $this->getTrimRight();
                $cp[3] = $cp[3] + $this->getTrimTop() + $this->getTrimBottom();
                $layer['clipbox'] = implode(" ", $cp);

            }

            $layerTemp[$key] = $layer;
        }

        $this->_elements->sortBySort();
        foreach ($this->_elements as $element) {
            $element->setRenderElements($this->_elements->getArray());
            if($element->getProductionPdf() == 0) {
                continue;
            }
            if($element instanceof TP_Steplayouter_Element_Motiv || $element instanceof TP_Steplayouter_Element_Upload) {

            }
            if(($element instanceof TP_Steplayouter_Element_Motiv || $element instanceof TP_Steplayouter_Element_Upload) && $element->getRenderPrintAdjust()) {

                $element->setWidth($element->getWidth()+$this->getTrimLeft()+$this->getTrimRight());
                $element->setHeight($element->getHeight()+$this->getTrimTop()+$this->getTrimBottom());
                $element->setPageHeight($element->getPageHeight() + $this->getTrimTop()+$this->getTrimBottom());

                if(round($element->getX(),0) == 0) {
                    $element->setX($element->getX()-$this->getTrimLeft());
                }
                if(round($element->getY(),0) > -5 && round($element->getY(),0) < 5) {
                    $element->setY(0);
                }



            }elseif($element instanceof TP_Steplayouter_Element_Rectangle) {
                if(round($element->getX(),0) == 0) {
                    $element->setWidth($element->getWidth()+$this->getTrimLeft());
                }else{
                    $element->setX($element->getX()+$this->getTrimLeft());
                }

                $element->setY($element->getY()+$this->getTrimTop());
            }elseif($element instanceof TP_Steplayouter_Element_Text || $element instanceof TP_Steplayouter_Element_Textarea) {
                $element->setTrimLeft($this->getTrimLeft());
                $element->setTrimTop($this->getTrimTop());
            }else{
                $element->setX($element->getX()+$this->getTrimLeft());
                $element->setY($element->getY()+$this->getTrimTop());
            }
            $element->buildPDFLib($p, $layerTemp);
        }

        if ($this->getRenderPreviewFoldMark() && ($this->getFoldMarkX() || $this->getFoldMarkY())) {
            $this->buildPDFRenderFold($p);
        }

        $p->end_page_ext("");
    }



    public function buildPDFLibPreview($p, $layers = array())
    {
        $p->set_parameter("topdown", "true");
        Zend_Registry::get('log')->debug("PDF PREVIEW: w:".$this->_width ." h:". $this->_height);
        $p->begin_page_ext($this->_width, $this->_height, "");

        $rgb = $this->hex2rgb($this->getBackgroundColor());
        $p->setcolor("fill", "rgb", $rgb['r'], $rgb['g'], $rgb['b'], 0.0);
        /* Define a rectangle */
        $p->rect(0, $this->getHeight(), $this->getWidth(), $this->getHeight());
        $p->fill();

        $this->_elements->sortBySort();
        foreach ($this->_elements as $element) {
            $element->setRenderElements($this->_elements->getArray());
            $element->buildPDFLib($p, $layers);
        }

        if ($this->getRenderPreviewFoldMark() && ($this->getFoldMarkX() || $this->getFoldMarkY())) {
            //$this->buildPDFRenderFold($p);
        }
        $font = $p->load_font("Helvetica-Bold", "unicode", "");
        $p->fit_textline("---MUSTER---", 0, $this->getHeight(), "font=".$font.
            " fontsize=1 textrendering=1 boxsize={".$this->getWidth()." ".$this->getHeight()."} stamp=ul2lr" .
                " strokecolor=red strokewidth=1");

        $p->end_page_ext("");
    }

    public function buildPDFLib($p, $layers = array())
    {
        $p->set_parameter("topdown", "true");
       /* Zend_Registry::get('log')->debug("width=" . ($this->getWidth() + $this->getTrimLeft() + $this->getTrimRight()) . " height="
            . ($this->getHeight() + $this->getTrimBottom() + $this->getTrimTop()) . " trimbox={" . $this->getTrimLeft() . " "
            . $this->getTrimTop() . " " . ($this->getWidth() + $this->getTrimLeft()) . " " . ($this->getHeight() + $this->getTrimTop()) . "}");

        $p->begin_page_ext(0, 0, "width=" . ($this->getWidth() + $this->getTrimLeft() + $this->getTrimRight()) . " height="
            . ($this->getHeight() + $this->getTrimBottom() + $this->getTrimTop()) . " trimbox={" . $this->getTrimLeft() . " "
            . $this->getTrimTop() . " " . ($this->getWidth() + $this->getTrimLeft()) . " " . ($this->getHeight() + $this->getTrimTop()) . "}");
         */

        $p->begin_page_ext($this->_width, $this->_height, "");

        $rgb = $this->hex2rgb($this->getBackgroundColor());
        $p->setcolor("fill", "rgb", $rgb['r'], $rgb['g'], $rgb['b'], 0.0);
        /* Define a rectangle */
        $p->rect(0, $this->getHeight(), $this->getWidth(), $this->getHeight());
        $p->fill();

        $this->_elements->sortBySort();
        foreach ($this->_elements as $element) {
            $element->setRenderElements($this->_elements->getArray());
            $element->buildPDFLib($p, $layers);
        }

        if ($this->getRenderPreviewFoldMark() && ($this->getFoldMarkX() || $this->getFoldMarkY())) {
            $this->buildPDFRenderFold($p);
        }

        $p->end_page_ext("");
    }

    public function buildPDFLayer($p, $rows, $first)
    {

        $p->set_parameter("topdown", "true");
        $p->begin_page_ext($this->_width, $this->_height, "");

        $this->_elements->sortBySort();
        $p->save();
        $rgb = $this->hex2rgb('#fffffd');
        $p->setcolor("fill", "rgb", $rgb['r'], $rgb['g'], $rgb['b'], 0.0);
        $sh = $p->shading("axial",  -2, -2, $this->_width+2, $this->_height+2, $rgb['r'], $rgb['g'], $rgb['b'], 0.0, "");
        $p->shfill($sh);
        //$p->rect(0, $this->_height, $this->_width, $this->_height+10);
        //$p->fill();
        $p->restore();
        foreach ($rows as $element) {
            $element->setRenderElements($this->_elements->getArray());
            $element->buildPDFLib($p);
        }
        $p->end_page_ext("");

        return $this;
    }

    public function hex2rgb($hex)
    {
        $color = array();
        $color['r'] = hexdec(substr($hex, 1, 2)) / 255;
        $color['g'] = hexdec(substr($hex, 3, 2)) / 255;
        $color['b'] = hexdec(substr($hex, 5, 2)) / 255;
        return $color;
    }

    public function getLayer()
    {
        $this->_elements->sortBySort();
        return $this->_elements->getLayer();
    }

    protected function buildPDFRenderFold($p)
    {
        $p->setcolor("stroke", "rgb", 0.4, 0.5, 0.2, 0);
        if ($this->getFoldMarkX()) {

            $p->setlinewidth(0.6);
            $p->setdashpattern("dasharray={3 3}");
            $p->moveto($this->getFoldMarkX(), $this->getTrimTop());
            $p->lineto($this->getFoldMarkX(), ($this->_height - $this->getTrimBottom()));
            $p->stroke();

        }

        if ($this->getFoldMarkY()) {

            $p->setlinewidth(0.6);
            $p->setdashpattern("dasharray={3 3}");
            $p->moveto($this->getTrimLeft(), $this->getFoldMarkY());
            $p->lineto($this->getTrimLeft(), ($this->_width - $this->getTrimRight()));
            $p->stroke();

        }
    }

    public function getPassepartout()
    {
        return $this->_passepartout;
    }

    public function setPassepartout($passepartout)
    {
        $this->_passepartout = $passepartout;
    }

    /**
     * @return array
     */
    public function getRefLayers()
    {
        return $this->_ref_layers;
    }

    /**
     * @param array $ref_layers
     */
    public function setRefLayers($ref_layers)
    {
        $this->_ref_layers = $ref_layers;
    }

    /**
     * @return array
     */
    public function getRefLayersElements()
    {
        return $this->_ref_layers_elements;
    }

    /**
     * @param array $ref_layers_elements
     */
    public function setRefLayersElements($ref_layers_elements)
    {
        $this->_ref_layers_elements = $ref_layers_elements;
    }

    /**
     * @return boolean
     */
    public function getRenderLayouterTrim()
    {
        return $this->_render_layouter_trim;
    }

    /**
     * @param boolean $render_layouter_trim
     */
    public function setRenderLayouterTrim($render_layouter_trim)
    {
        $this->_render_layouter_trim = $render_layouter_trim;
    }

    /**
     * @return boolean
     */
    public function getCutX()
    {
        return $this->_cut_x;
    }

    /**
     * @param boolean $cut_x
     */
    public function setCutX($cut_x)
    {
        $this->_cut_x = $cut_x;
    }

    /**
     * @return boolean
     */
    public function getCutY()
    {
        return $this->_cut_y;
    }

    /**
     * @param boolean $cut_y
     */
    public function setCutY($cut_y)
    {
        $this->_cut_y = $cut_y;
    }

    /**
     * @return boolean
     */
    public function getCutWidth()
    {
        return $this->_cut_width;
    }

    /**
     * @param boolean $cut_width
     */
    public function setCutWidth($cut_width)
    {
        $this->_cut_width = $cut_width;
    }

    /**
     * @return boolean
     */
    public function getCutHeight()
    {
        return $this->_cut_height;
    }

    /**
     * @param boolean $cut_height
     */
    public function setCutHeight($cut_height)
    {
        $this->_cut_height = $cut_height;
    }

    protected function buildPDFRenderTrim($p)
    {
        $p->setcolor("stroke", "rgb", 0.4, 0.5, 0.2, 0);
        if ($this->getTrimLeft()) {
            $p->setlinewidth(0.6);
            $p->setdashpattern("dasharray={3 5}");
            $p->moveto($this->getTrimLeft(), $this->getTrimTop());
            $p->lineto($this->getTrimLeft(), ($this->_height - $this->getTrimBottom()));
            $p->stroke();
        }

        if ($this->getTrimRight()) {
            $p->setlinewidth(0.6);
            $p->setdashpattern("dasharray={3 5}");
            $p->moveto($this->_width + $this->getTrimLeft(), $this->getTrimTop());
            $p->lineto($this->_width + $this->getTrimLeft(), ($this->_height - $this->getTrimBottom()));
            $p->stroke();
        }

        if ($this->getTrimTop()) {
            $p->setlinewidth(0.6);
            $p->setdashpattern("dasharray={3 5}");
            $p->moveto($this->getTrimLeft(), $this->getTrimTop());
            $p->lineto($this->_width + $this->getTrimLeft(), $this->getTrimTop());
            $p->stroke();
        }

        if ($this->getTrimBottom()) {
            $p->setlinewidth(0.6);
            $p->setdashpattern("dasharray={3 5}");
            $p->moveto($this->getTrimLeft(), $this->_height + $this->getTrimTop());
            $p->lineto($this->_width + $this->getTrimLeft(), $this->_height + $this->getTrimTop());
            $p->stroke();
        }
    }

    public function getFoldMarkY()
    {
        return $this->_fold_mark_y;
    }

    public function setFoldMarkY($fold_mark_y)
    {
        $this->_fold_mark_y = $fold_mark_y;
    }

    public function getFoldMarkX()
    {
        return $this->_fold_mark_x;
    }

    public function setFoldMarkX($fold_mark_x)
    {
        $this->_fold_mark_x = $fold_mark_x;
    }

    public function getTrimBottom()
    {
        return $this->_trim_bottom;
    }

    public function setTrimBottom($trim_bottom)
    {
        $this->_trim_bottom = $trim_bottom;
    }

    public function getTrimTop()
    {
        return $this->_trim_top;
    }

    public function setTrimTop($trim_top)
    {
        $this->_trim_top = $trim_top;
    }

    public function getTrimRight()
    {
        return $this->_trim_right;
    }

    public function setTrimRight($trim_right)
    {
        $this->_trim_right = $trim_right;
    }

    public function getTrimLeft()
    {
        return $this->_trim_left;
    }

    public function setTrimLeft($trim_left)
    {
        $this->_trim_left = $trim_left;
    }

    public function getRenderPreviewTrim()
    {
        return $this->_render_preview_trim;
    }

    public function setRenderPreviewTrim($render_preview_trim)
    {
        $this->_render_preview_trim = $render_preview_trim;
    }

    public function getRenderPreviewFoldMark()
    {
        return $this->_render_preview_fold_mark;
    }

    public function setRenderPreviewFoldMark($render_preview_fold_mark)
    {
        $this->_render_preview_fold_mark = $render_preview_fold_mark;
    }

    public function getBackgroundColor()
    {
        return $this->_background_color;
    }

    public function setBackgroundColor($background_color)
    {
        $this->_background_color = $background_color;
    }

    public function getWidth()
    {
        return $this->_width;
    }

    public function setWidth($width)
    {
        $this->_width = $width;
    }

    public function getHeight()
    {
        return $this->_height;
    }

    public function setHeight($height)
    {
        $this->_height = $height;
    }

}