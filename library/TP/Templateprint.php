<?php

class TP_Templateprint {

    public static function generatePreviewLink(Article $article, $layouterId = false, $order_id = false, $pos = false) {
        if($layouterId) {

            $articleSession = new TP_Layoutersession();
            $articleSess = $articleSession->getLayouterArticle($layouterId);

            if(Zend_Auth::getInstance()->hasIdentity()) {
                if(file_exists('/data/www/new/web/apps/market/templateprint/user/' . $articleSess->getTemplatePrintId() . '/preview.pdf')) {
                    return 'apps/market/templateprint/user/' . $articleSess->getTemplatePrintId() . '/preview.pdf';
                }else{
                    return 'apps/market/templateprint/user/' . $articleSess->getTemplatePrintId() . '/final.pdf';
                }
            }else{
                if(file_exists('/data/www/new/web/apps/market/templateprint/guest/' . $articleSess->getTemplatePrintId() . '/preview.pdf')) {
                    return 'apps/market/templateprint/guest/' . $articleSess->getTemplatePrintId() . '/preview.pdf';
                }else{
                    return 'apps/market/templateprint/guest/' . $articleSess->getTemplatePrintId() . '/final.pdf';
                }
            }
        }
    }

    public static function generatePreview(Article $article, $layouterId = false, $order_id = false, $pos = false, $site = 1) {
        $site = $site-1;
        if($order_id && $pos) {
            if(file_exists(APPLICATION_PATH . '/../market/templateprint/basket/' . $order_id . '/' . $pos . '/preview.pdf')) {
                exec("convert -density 140 -resample 100 -colorspace sRGB ".APPLICATION_PATH . "/../market/templateprint/basket/" . $order_id . "/" . $pos . "/preview.pdf -quality 75 ". APPLICATION_PATH . "/../market/templateprint/basket/" . $order_id . "/" . $pos . "/preview.png");
                return '../market/templateprint/basket/' . $order_id . '/' . $pos . '/preview.png';
            }elseif(file_exists(APPLICATION_PATH . '/../market/templateprint/basket/' . $order_id . '/' . $pos . '/final.pdf')) {
                exec("convert -density 140 -resample 100 -colorspace sRGB ".APPLICATION_PATH . "/../market/templateprint/basket/" . $order_id . "/" . $pos . "/final.pdf -quality 75 ". APPLICATION_PATH . "/../market/templateprint/basket/" . $order_id . "/" . $pos . "/final.png");
                return '../market/templateprint/basket/' . $order_id . '/' . $pos . '/final.png';
            }

        }

        if($layouterId) {
            
            $articleSession = new TP_Layoutersession();
            $articleSess = $articleSession->getLayouterArticle($layouterId);

            if(Zend_Auth::getInstance()->hasIdentity()) {
                if(file_exists(APPLICATION_PATH . "/../market/templateprint/user/" . $articleSess->getTemplatePrintId() . "/preview.pdf")) {
                    exec("convert -density 140 -resample 100 -colorspace sRGB ".APPLICATION_PATH . "/../market/templateprint/user/" . $articleSess->getTemplatePrintId() . "/preview.pdf[".$site."] -quality 75 ". APPLICATION_PATH . "/../market/templateprint/user/" . $articleSess->getTemplatePrintId() . "/preview".$site.".png");
                }else{
                    exec("convert -density 140 -resample 100 -colorspace sRGB ".APPLICATION_PATH . "/../market/templateprint/user/" . $articleSess->getTemplatePrintId() . "/final.pdf[".$site."] -quality 75 ". APPLICATION_PATH . "/../market/templateprint/user/" . $articleSess->getTemplatePrintId() . "/preview".$site.".png");
                }

                $article->render_new_preview_image = true;
                $article->save();
                Zend_Registry::get('log')->debug('/apps/market/templateprint/user/' . $articleSess->getTemplatePrintId() . '/preview'.$site.'.png');
                return '../market/templateprint/user/' . $articleSess->getTemplatePrintId() . '/preview'.$site.'.png';
            }else{

                if(file_exists(APPLICATION_PATH . "/../market/templateprint/guest/" . $articleSess->getTemplatePrintId() . "/preview.pdf")) {
                    exec("convert -density 140 -resample 100 -colorspace sRGB ".APPLICATION_PATH . "/../market/templateprint/guest/" . $articleSess->getTemplatePrintId() . "/preview.pdf[".$site."] -quality 75 ". APPLICATION_PATH . "/../market/templateprint/guest/" . $articleSess->getTemplatePrintId() . "/preview".$site.".png");
                }else{
                    exec("convert -density 140 -resample 100 -colorspace sRGB ".APPLICATION_PATH . "/../market/templateprint/guest/" . $articleSess->getTemplatePrintId() . "/final.pdf[".$site."] -quality 75 ". APPLICATION_PATH . "/../market/templateprint/guest/" . $articleSess->getTemplatePrintId() . "/preview".$site.".png");
                }
                
                $article->render_new_preview_image = true;
                $article->save();

                return '../market/templateprint/guest/' . $articleSess->getTemplatePrintId() . '/preview'.$site.'.png';
            }
            
        }else{
            
            $path = str_split($article->id);
            

            if(file_exists(APPLICATION_PATH . "/../market/templateprint/orginal/" . implode('/', $path) . "/preview'.$site.'.png")) {
                return '../market/templateprint/orginal/' . implode('/', $path) . '/preview'.$site.'.png';
            }
            exec("convert -density 140 -resample 100 -colorspace sRGB ".APPLICATION_PATH . "/../market/templateprint/orginal/" . implode('/', $path) . "/preview.pdf[".$site."] -quality 75 ". APPLICATION_PATH . "/../market/templateprint/orginal/" . implode('/', $path) . "/preview".$site.".png");

            $article->render_new_preview_image = true;
            $article->save();

            return '../market/templateprint/orginal/' . implode('/', $path) . '/preview'.$site.'.png';
            
        }
        
    }
    
    public static function isTemplateExists(Article $article) {
        $path = str_split($article->id);
        
        if(file_exists(APPLICATION_PATH . '/../market/templateprint/orginal/' . implode('/', $path) . '/product.zip')) {
            return "&w2pproductid=".$article->uuid;
        }
        return "";
    }

    public static function isTemplateExistsInBasket(Orderspos $pos)
    {
        if(file_exists(APPLICATION_PATH . '/../market/templateprint/basket/' . $pos->Orders->id . '/' . $pos->pos . '/preview.pdf')) {
            return true;
        }

        return false;
    }

    public static function generatePreviewFromBasket(Orderspos $pos)
    {
        if(!file_exists(APPLICATION_PATH . '/../market/templateprint/basket/' . $pos->Orders->id . '/' . $pos->pos . '/preview.png')) {
            exec("mutool draw -r 300 -o ". APPLICATION_PATH . "/../market/templateprint/basket/" . $pos->Orders->id . '/' . $pos->pos . "/preview.png ".APPLICATION_PATH . "/../market/templateprint/basket/" . $pos->Orders->id . '/' . $pos->pos . "/preview.pdf 1");
        }

        return '/apps/market/templateprint/basket/' . $pos->Orders->id . '/' . $pos->pos . '/preview.png';
    }

    public static function siteExists(Article $article, $layouterId = false, $order_id = false, $pos = false, $site = 1)
    {
        if($layouterId) {

            $articleSession = new TP_Layoutersession();
            $articleSess = $articleSession->getLayouterArticle($layouterId);

            if (Zend_Auth::getInstance()->hasIdentity()) {

                $image = new Imagick();
                if(file_exists(APPLICATION_PATH . "/../market/templateprint/user/" . $articleSess->getTemplatePrintId() . "/preview.pdf")) {
                    $image->pingImage(APPLICATION_PATH . "/../market/templateprint/user/" . $articleSess->getTemplatePrintId() . "/preview.pdf");
                }else{
                    $image->pingImage(APPLICATION_PATH . "/../market/templateprint/user/" . $articleSess->getTemplatePrintId() . "/final.pdf");
                }

                if($site <= $image->getNumberImages()) {
                    return true;
                }

            } else {

                $image = new Imagick();
                if(file_exists(APPLICATION_PATH . "/../market/templateprint/guest/" . $articleSess->getTemplatePrintId() . "/preview.pdf")) {
                    $image->pingImage(APPLICATION_PATH . "/../market/templateprint/guest/" . $articleSess->getTemplatePrintId() . "/preview.pdf");
                }else{
                    $image->pingImage(APPLICATION_PATH . "/../market/templateprint/guest/" . $articleSess->getTemplatePrintId() . "/final.pdf");
                }

                if($site <= $image->getNumberImages()) {
                    return true;
                }

            }
        }else{

            $path = str_split($article->id);

            $image = new Imagick();
            if(file_exists(APPLICATION_PATH . "/../market/templateprint/orginal/" . implode('/', $path) . "/preview.pdf")) {
                $image->pingImage(APPLICATION_PATH . "/../market/templateprint/orginal/" . implode('/', $path) . "/preview.pdf");
            }else{
                $image->pingImage(APPLICATION_PATH . "/../market/templateprint/orginal/" . implode('/', $path) . "/final.pdf");
            }

            if($site <= $image->getNumberImages()) {
                return true;
            }

        }

        return false;
    }

}
