<?php

class TP_Themesfilter {

    /**
     * Filter-Sessionnamespace
     *
     * @var Zend_Session_Namespace
     */
    private $_Filter;

    /**
     * Konstruiert das Model für die Suche
     *
     * @return void
     */
    public function __construct()
    {

        $this->_Filter = new Zend_Session_Namespace('Themesfilter');

        $this->_Filter->unlock();

        if(Zend_Session::namespaceIsset ('Themesfilter') === false)
        {
            $this->initFilter();
        }

        $this->_Filter->lock();
    }


    public function setArticleFilterId($id, $name) {
        $this->_Filter->unlock();
        if(!isset($this->_Filter->article[$id])) {
            $this->_Filter->article[$id] = $name;
        }
        $this->_Filter->lock();
    }

    public function delArticleFilterId($id) {
        $this->_Filter->unlock();
        if(isset($this->_Filter->article[$id])) {
            unset($this->_Filter->article[$id]);
        }
        $this->_Filter->lock();
    }
    
    public function delAllArticleFilter() {
        $this->_Filter->unlock();
        $this->_Filter->article = array();
        $this->_Filter->lock();
    }

    public function getArticleFilter() {
        return $this->_Filter->article;
    }

    public function hasArticleFilter() {
        return count($this->_Filter->article);
    }

    public function setMotivFilterId($id, $name) {
        $this->_Filter->unlock();
        if(!isset($this->_Filter->motiv[$id])) {
            $this->_Filter->motiv = array();
            $this->_Filter->motiv[$id] = $name;
        }
        $this->_Filter->lock();
    }

    public function delMotivFilterId($id) {
        $this->_Filter->unlock();
        if(isset($this->_Filter->motiv[$id])) {
            unset($this->_Filter->motiv[$id]);
        }
        $this->_Filter->lock();
    }
    
    public function delAllMotivFilter() {
        $this->_Filter->unlock();
        $this->_Filter->motiv = array();
        $this->_Filter->lock();
    }

    public function getMotivFilter() {
        return $this->_Filter->motiv;
    }

    public function setShopFilterId($id, $name) {
        $this->_Filter->unlock();
        if(!isset($this->_Filter->shop[$id])) {
            $this->_Filter->shop = array();
            $this->_Filter->shop[$id] = $name;
        }
        $this->_Filter->lock();
    }

    public function delShopFilterId($id) {
        $this->_Filter->unlock();
        if(isset($this->_Filter->shop[$id])) {
            unset($this->_Filter->shop[$id]);
        }
        $this->_Filter->lock();
    }
    
    public function delAllShopFilter() {
        $this->_Filter->unlock();
        $this->_Filter->shop = array();
        $this->_Filter->lock();
    }

    public function getShopFilter() {
        return $this->_Filter->shop;
    }

    private function initFilter() {
        $this->_Filter->article = array();
        $this->_Filter->motiv = array();
        $this->_Filter->shop = array();
        $this->_Filter->article_url = false;
    }

    public function clearArticleFilter() {
        $this->_Filter->unlock();
        $this->_Filter->article = array();
        $this->_Filter->lock();
    }

    public function clearFilter() {
        $this->initFilter();
    }

    /**
     * @return mixed
     */
    public function getArticleUrl()
    {
        return $this->_Filter->article_url;
    }

    /**
     * @param mixed $article_url
     */
    public function setArticleUrl($article_url)
    {
        $this->_Filter->unlock();
        $this->_Filter->article_url = $article_url;
        $this->_Filter->lock();
    }

}
