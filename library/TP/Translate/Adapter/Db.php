<?php

class TP_Translate_Adapter_Db extends Zend_Translate_Adapter
{
    private $_data = array();

	public function __construct($data = null, $locale = null, array $options = array())
    {
    	
        $data['de'] = array();
        //parent::__construct($data, $locale, $options);
    }
    /**
     * Load translation data
     *
     * @param  string|array  $data
     * @param  string        $locale  Locale/Language to add data for, identical with locale identifier,
     *                                see Zend_Locale for more information
     * @param  array         $options OPTIONAL Options to use
     * @return array
     */
    protected function _loadTranslationData($data, $locale, array $options = array())
    {
        
        $data = Doctrine_Query::CREATE()->from('Admintranslation')->fetchArray();
		if(strlen($locale) == 5) {
			$locale = substr($locale, 0, 2);
		}
        foreach ($data as $item) {
        	if(!isset($this->_data[$locale])) $this->_data[$locale] = array();
            $this->_data[$locale][$item['id']] = $item[$locale];
        }

        return $this->_data;
    }
    
	public function translate($messageId, $locale = null)
    {
        $message = parent::translate($messageId, $locale);
        
        $temp = Zend_Registry::get('locale')->getLanguage();
        if($message == $messageId && !isset($this->_translate[$temp][$messageId])) {
        	//$messaged = new Admintranslation();
        	//$messaged->id = $messageId;
        	//$messaged->$temp = $message;
			//$messaged->save();
        
        }
        
        return $message;
    }

    /**
     * returns the adapters name
     *
     * @return string
     */
    public function toString()
    {
        return "Db";
    }
}