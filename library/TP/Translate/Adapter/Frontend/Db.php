<?php

class TP_Translate_Adapter_Frontend_Db extends Zend_Translate_Adapter_Csv
{
    
	/**
     * Load translation data
     *
     * @param  string|array  $data
     * @param  string        $locale  Locale/Language to add data for, identical with locale identifier,
     *                                see Zend_Locale for more information
     * @param  array         $options OPTIONAL Options to use
     * @return array
     */
    protected function _loadTranslationData($filename, $locale, array $options = array())
    {
    	
    	
    	$this->_data = parent::_loadTranslationData($filename, $locale, $options);
        
    	if(strlen($locale) == 5) {
			$locale = substr($locale, 0, 2);
		}
		
		$shop = Zend_Registry::get('shop');
		
        $data = Doctrine_Query::CREATE()->from('Frontendtranslation')->where('locale = ? AND shop_id = ?',array($locale, $shop['id']))->fetchArray();
		
        foreach ($data as $item) {
        	//if(!isset($this->_data[$locale])) $this->_data[$locale] = array();
            $this->_data[$locale][$item['message_id']] = $item['text'];
        }

        return $this->_data;
    }
	
}