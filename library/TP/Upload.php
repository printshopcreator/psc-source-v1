<?php

class TP_Upload {

    public static function getLastUploadForUser(Article $article) {
        if(Zend_Auth::getInstance()->hasIdentity()) {
            $user = Zend_Auth::getInstance()->getIdentity();

            $prof = Doctrine_Query::create()
                ->from('Orderspos p')
                ->leftJoin('p.Orders o')
                ->where('o.contact_id = ? AND p.article_id = ?', array($user['id'], $article->id))->orderBy('p.id DESC')->fetchOne();

            return $prof->Upload;
        }

        return [];
    }



}