<?php

class TP_Uploadsession
{

    /**
     * Layoutersession
     *
     * @var Zend_Session_Namespace
     */
    private $_Items;

    /**
     * Konstruiert das Model für die Suche
     *
     * @return void
     */
    public function __construct()
    {

        $this->_Items = new Zend_Session_Namespace ('Uploadsession');

        $this->_Items->unlock();

        if (Zend_Session::namespaceIsset('Uploadsession') === false) {
            $this->initSession();
        }

    }

    private function initSession()
    {
        $this->_Items->data = array();
    }

    public function addImage($dir, $motiv_id, $key, $name)
    {
        if(Zend_Auth::getInstance()->hasIdentity()) {
            $entry = new Steplayouterdata();
            $entry->uuid = $key;
            $entry->data = json_encode(array('name' => $name, 'key' => $key, 'motiv_id' => $motiv_id, 'dir' => $dir, 'crop' => false));
            $entry->save();
            return;
        }
        $this->_Items->unlock();
        $this->_Items->data[$key] = array('name' => $name, 'key' => $key, 'motiv_id' => $motiv_id, 'dir' => $dir, 'crop' => false);
    }

    public function hasImage($key) {
        if(Zend_Auth::getInstance()->hasIdentity()) {
            $entry = Doctrine_Query::create ()->from ( 'Steplayouterdata c' )->where ( 'c.uuid = ?', array ($key ) )->fetchOne ();
            if($entry) {
                return true;
            }else{
                return false;
            }
        }

        if(isset($this->_Items->data[$key])) {
            return true;
        }

        return false;
    }

    public function getImage($key)
    {

        if(Zend_Auth::getInstance()->hasIdentity()) {
            $entry = Doctrine_Query::create ()->from ( 'Steplayouterdata c' )->where ( 'c.uuid = ?', array ($key ) )->fetchOne ();
            if($entry) {

                return json_decode($entry->data, true);
            }
        }

        return $this->_Items->data[$key];
    }

    public function updateImage($key, $data)
    {
        if(Zend_Auth::getInstance()->hasIdentity()) {
            $entry = Doctrine_Query::create ()->from ( 'Steplayouterdata c' )->where ( 'c.uuid = ?', array ($key ) )->fetchOne ();
            if($entry) {
                $entry->data = json_encode($data);
                $entry->save();
            }
        }

        $this->_Items->unlock();
        $this->_Items->data[$key] = $data;
    }

    public function clearSession()
    {
        $this->initSession();
    }

    public function copyToDatabase() {

        foreach($this->_Items->data as $key => $row) {

            $entry = new Steplayouterdata();
            $entry->uuid = $key;
            $entry->data = json_encode($row);
            $entry->save();
        }
        $this->initSession();
    }

}
