<?php

/**
 * Util Class
 *
 * Util Class
 *
 * PHP versions 4 and 5
 *
 * @category   Library
 * @package    TP
 * @subpackage Util
 * @author     Thomas petersomn <tpeterson@printshopcreator.de>
 * @copyright  1997-2009 Thomas Peterson
 * @license    Not Public
 * @version    SVN: $Id: Util.php 7312 2012-02-13 21:16:33Z boonkerz $
 * @link       http://www.printshopcreator.de
 */

/**
 * Util Class
 *
 * Util Class
 *
 * PHP versions 4 and 5
 *
 * @category   Library
 * @package    TP
 * @subpackage Util
 * @author     Thomas petersomn <tpeterson@printshopcreator.de>
 * @copyright  1997-2009 Thomas Peterson
 * @license    Not Public
 * @version    Release: $Id: Util.php 7312 2012-02-13 21:16:33Z boonkerz $
 * @link       http://www.printshopcreator.de
 */
class TP_Util {
    const CLEAR_ALL = 1;
    const CLEAR_APC = 2;

	public static function encode($str)
    {
		$table = str_split("1bcd2fgh3jklmn4pqrstAvwxyz567890", 1);

        $n = strlen($str) * 8 / 5;
        $arr = str_split($str, 1);
        $m = "";
        foreach ($arr as $c) {
            $m .= str_pad(decbin(ord($c)), 8, "0", STR_PAD_LEFT);
        }
        $p = ceil(strlen($m) / 5) * 5;
        $m = str_pad($m, $p, "0", STR_PAD_RIGHT);
        $newstr = "";
        for ($i = 0; $i < $n; $i++) {
            $newstr .= $table[bindec(substr($m, $i * 5, 5))];
        }
        return $newstr;
    }

    public static function strip($text) {

        $table = array(
            'Š' => 'S', 'š' => 's', 'Đ' => 'Dj', 'đ' => 'dj', 'Ž' => 'Z', 'ž' => 'z', 'Č' => 'C', 'č' => 'c', 'Ć' => 'C', 'ć' => 'c',
            'À' => 'A', 'Á' => 'A', 'Â' => 'A', 'Ã' => 'A', 'Ä' => 'Ae', 'Å' => 'A', 'Æ' => 'A', 'Ç' => 'C', 'È' => 'E', 'É' => 'E',
            'Ê' => 'E', 'Ë' => 'E', 'Ì' => 'I', 'Í' => 'I', 'Î' => 'I', 'Ï' => 'I', 'Ñ' => 'N', 'Ò' => 'O', 'Ó' => 'O', 'Ô' => 'O',
            'Õ' => 'O', 'Ö' => 'Oe', 'Ø' => 'O', 'Ù' => 'U', 'Ú' => 'U', 'Û' => 'U', 'Ü' => 'Ue', 'Ý' => 'Y', 'Þ' => 'B', 'ß' => 'Ss',
            'à' => 'a', 'á' => 'a', 'â' => 'a', 'ã' => 'a', 'ä' => 'ae', 'å' => 'a', 'æ' => 'a', 'ç' => 'c', 'è' => 'e', 'é' => 'e',
            'ê' => 'e', 'ë' => 'e', 'ì' => 'i', 'í' => 'i', 'î' => 'i', 'ï' => 'i', 'ð' => 'o', 'ñ' => 'n', 'ò' => 'o', 'ó' => 'o',
            'ô' => 'o', 'õ' => 'o', 'ö' => 'oe', 'ø' => 'o', 'ù' => 'u', 'ú' => 'u', 'û' => 'u', 'ü' => 'ue', 'ý' => 'y', 'ý' => 'y', 'þ' => 'b',
            'ÿ' => 'y', 'Ŕ' => 'R', 'ŕ' => 'r'
        );

        $text = strtr($text, $table);

        $text = preg_replace('/\W/', ' ', $text);

        $text = preg_replace('/\ +/', '-', $text);

        $text = preg_replace('/\-$/', '', $text);
        $text = preg_replace('/^\-/', '', $text);

        return $text;
    }

    public static function uuid($serverID = 1) {

        $guid = '';
        $namespace = rand(11111, 99999);
        $uid = uniqid('', true);
        $data = $namespace;
        $data .= $_SERVER['REQUEST_TIME'];
        $data .= $_SERVER['HTTP_USER_AGENT'];
        $data .= $_SERVER['REMOTE_ADDR'];
        $data .= $_SERVER['REMOTE_PORT'];
        $hash = strtolower(hash('ripemd128', $uid . $guid . md5($data)));
        $guid = substr($hash,  0,  8) . '-' .
            substr($hash,  8,  4) . '-' .
            substr($hash, 12,  4) . '-' .
            substr($hash, 16,  4) . '-' .
            substr($hash, 20, 12);
        return $guid;
    }

    public static function uuidCredit($serverID = 1) {

        $t = explode(" ", microtime());
        return sprintf('%04x%04x',
                mt_rand(0, 0xffff), mt_rand(0, 0xffff));
    }

    public static function uuidDecode($uuid) {
        $rez = Array();
        $u = explode("-", $uuid);
        if (is_array($u) && count($u) == 5) {
            $rez = Array(
                'serverID' => $u[0],
                'ip' => self::clientIPFromHex($u[1]),
                'unixtime' => hexdec($u[2]),
                'micro' => (hexdec($u[3]) / 65536)
            );
        }
        return $rez;
    }

    public static function clientIPToHex($ip="") {
        $hex = "";
        if ($ip == "")
            $ip = getEnv("REMOTE_ADDR");
        $part = explode('.', $ip);
        for ($i = 0; $i <= count($part) - 1; $i++) {
            $hex.=substr("0" . dechex($part[$i]), -2);
        }
        return $hex;
    }

    public static function clientIPFromHex($hex) {
        $ip = "";
        if (strlen($hex) == 8) {
            $ip.=hexdec(substr($hex, 0, 2)) . ".";
            $ip.=hexdec(substr($hex, 2, 2)) . ".";
            $ip.=hexdec(substr($hex, 4, 2)) . ".";
            $ip.=hexdec(substr($hex, 6, 2));
        }
        return $ip;
    }

    public static function writeReport($id, $what, $pos = false) {

        $host = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] && !in_array(strtolower($_SERVER['HTTPS']),array('off','no'))) ? 'https' : 'http';
        $host .= '://'.$_SERVER['HTTP_HOST'];

        $currency = new Zend_Currency(Zend_Registry::get('locale')->getLanguage() . '_' . Zend_Registry::get('locale')->getRegion());

        if (intval($id) != 0) {

            $data = Doctrine_Query::create()
                            ->from('Orders a')
                            ->where('a.id = ?')->fetchOne(array(intval($id)));
        } else {
            return false;
        }
        
        
        $reportPath = Zend_Registry::get('shop_path') . "/reports/";
        
        $shop = $data->Shop;
        if(file_exists(APPLICATION_PATH . '/design/clients/' . $shop->uid . '/reports/')) {
            $reportPath = APPLICATION_PATH . '/design/clients/' . $shop->uid . '/reports/';
        }
        if($data->Shop->market == 1 && $shop->reportsite1 == "") {

            $shop = Doctrine_Query::create()->select()->from('Shop s')->where('s.id = ?', array($data->Install->defaultmarket))->fetchOne();

            $reportPath = APPLICATION_PATH . "/design/clients/" . $shop->uid . "/reports/";

        }elseif (!file_exists($reportPath . $what . ".jrxml")) {
            if (!file_exists($reportPath. "/reports"))
                mkdir($reportPath . "/reports", 0777, true);

            if (!file_exists(Zend_Registry::get('layout_path') . "/reports/" . $what . ".jrxml")) {
                return false;
            }
            copy(Zend_Registry::get('layout_path') . "/reports/" . $what . ".jrxml", $reportPath . $what . ".jrxml");
        }


        $params = $data->Contact->toArray();
        $params['contactCustom1'] = $data->Contact->getCustom1();
        $params['contactCustom2'] = $data->Contact->getCustom2();
        $params['contactCustom3'] = $data->Contact->getCustom3();
        $params['contactCustom4'] = $data->Contact->getCustom4();
        $params['contactCustom5'] = $data->Contact->getCustom5();
        $params['contactCustom6'] = $data->Contact->getCustom6();
        $params['contactCustom7'] = $data->Contact->getCustom7();
        $params['contactCustom8'] = $data->Contact->getCustom8();
        $params['contactCustom9'] = $data->Contact->getCustom9();
        $params['contactCustom10'] = $data->Contact->getCustom10();
        $params['contactCustom11'] = $data->Contact->getCustom11();
        $params['contactCustom12'] = $data->Contact->getCustom12();
        $params['contactCustom13'] = $data->Contact->getCustom13();
        $params['contactCustom14'] = $data->Contact->getCustom14();
        $params['contactCustom15'] = $data->Contact->getCustom15();
        $params['contactCustom16'] = $data->Contact->getCustom16();
        $params['contactCustom17'] = $data->Contact->getCustom17();
        $params['contactCustom18'] = $data->Contact->getCustom18();
        $params['contactCustom19'] = $data->Contact->getCustom19();
        $params['contactCustom20'] = $data->Contact->getCustom20();
        $params['contactCustom21'] = $data->Contact->getCustom21();
        $params['contactCustom22'] = $data->Contact->getCustom22();
        $params['contactCustom23'] = $data->Contact->getCustom23();
        $params['contactCustom24'] = $data->Contact->getCustom24();

        $params['name'] = $params['self_email'];

        if ($params['self_anrede'] == 1) {
            $params['self_anrede'] = "Herr";
        }
        if ($params['self_anrede'] == 2) {
            $params['self_anrede'] = "Frau";
        }
        if ($params['self_anrede'] == 3) {
            $params['self_anrede'] = "Firma";
        }
        if ($params['self_anrede'] == 4) {
            $params['self_anrede'] = "Herr Dr.";
        }
        if ($params['self_anrede'] == 5) {
            $params['self_anrede'] = "Frau Dr.";
        }
        if ($params['self_anrede'] == 6) {
            $params['self_anrede'] = "Herr Prof.";
        }
        if ($params['self_anrede'] == 7) {
            $params['self_anrede'] = "Frau Prof.";
        }
        if ($params['self_anrede'] == 8) {
            $params['self_anrede'] = "Herr Prof. Dr.";
        }
        if ($params['self_anrede'] == 9) {
            $params['self_anrede'] = "Frau Prof. Dr.";
        }
        if ($params['self_anrede'] == 10) {
            $params['self_anrede'] = "Schwester";
        }

        if ($data->delivery_same == false) {

            $liefer = Doctrine_Query::create()
                            ->from('ContactAddress a')
                            ->where('a.id = ?')->fetchOne(array(intval($data->delivery_address)));


            $params['liefer'] = $liefer->toArray();

            if ($params['liefer']['anrede'] == 1) {
                $params['liefer']['anrede'] = "Herr";
            }
            if ($params['liefer']['anrede'] == 2) {
                $params['liefer']['anrede'] = "Frau";
            }
            if ($params['liefer']['anrede'] == 3) {
                $params['liefer']['anrede'] = "Firma";
            }
            if ($params['liefer']['anrede'] == 4) {
                $params['liefer']['anrede'] = "Herr Dr.";
            }
            if ($params['liefer']['anrede'] == 5) {
                $params['liefer']['anrede'] = "Frau Dr.";
            }
            if ($params['liefer']['anrede'] == 6) {
                $params['liefer']['anrede'] = "Herr Prof.";
            }
            if ($params['liefer']['anrede'] == 7) {
                $params['liefer']['anrede'] = "Frau Prof.";
            }
            if ($params['liefer']['anrede'] == 8) {
                $params['liefer']['anrede'] = "Herr Prof. Dr.";
            }
            if ($params['liefer']['anrede'] == 9) {
                $params['liefer']['anrede'] = "Frau Prof. Dr.";
            }
            if ($params['liefer']['anrede'] == 10) {
                $params['liefer']['anrede'] = "Schwester";
            }

            $params['lieferissame'] = 1;
        } else {
            $params['lieferissame'] = 0;
        }

        
        if($data->delivery_same == true && $data->invoice_address == 0 && $data->delivery_address != 0) {
            $invoiceAddress = Doctrine_Query::create()->select()
                    ->from('ContactAddress o')->where('o.id = ?', array($data->delivery_address))->fetchOne();
        }else{
            $invoiceAddress = Doctrine_Query::create()->select()
                    ->from('ContactAddress o')->where('o.id = ?', array($data->invoice_address))->fetchOne();
        }
        
        if($invoiceAddress) {
            $params['invoiceAddress'] = $invoiceAddress->toArray();


            if($params['invoiceAddress']['company'] == "" && $params['invoiceAddress']['street'] == $params['self_street']) {
                $params['invoiceAddress']['company'] = $params['self_department'];
            }
            if($data->delivery_address != 0) {

                $invoiceAddDel = Doctrine_Query::create()->select()
                        ->from('ContactAddress o')->where('o.id = ?', array($data->delivery_address))->fetchOne();


                $params['deliveryAddress'] = $invoiceAddDel->toArray();

            }else{
                $params['deliveryAddress'] = $invoiceAddress->toArray();
            }

            if($data->sender_address != 0) {

                $invoiceAddSend = Doctrine_Query::create()->select()
                        ->from('ContactAddress o')->where('o.id = ?', array($data->sender_address))->fetchOne();

                $params['senderAddress'] = $invoiceAddSend->toArray();

            }else{
                $params['senderAddress'] = $invoiceAddress->toArray();
            }
        }
        
        $params['deliveryissame'] = $data->delivery_same;
        $params['senderissame'] = $data->sender_same;
        $datas = array();
        $mwert = array();
        $prodpreisbrutto = 0;
        $summewarenwert = array();
        $prodpreis = 0;
        $versandMwert = 0;
        $versand = 0;
        require_once(APPLICATION_PATH . '/helpers/Article.php');
        $helper = new TP_View_Helper_Article();
        $weight = 0;
        $tempTitle = '';
        $optionsAsArray = array();
        if($pos != false) {
            $pro = unserialize($pos->data);
            $proa = $pro->getArticle();
            $proo = $pro->getOptions();

            $article = Doctrine_Query::create()
                            ->from('Article c')
                            ->where('c.id = ?', array($pos->article_id))
                            ->fetchOne();


            if($article->typ == 9) {
                $options = $article->getMarketOptArray($pos->data, true, $pos->a9_designer_data);
                $options_name2 = $article->getMarketOptArray($pos->data, true, $pos->a9_designer_data);
                $rawoptions = $article->getMarketRawOptArray($pos->data);
            }else{
                $options = $article->getOptArray($pos->data);
                $optionsAsArray = $article->getOptArray($pos->data, true, true);
                $options_name2 = $article->getOptArrayName2($pos->data);
                $rawoptions = $article->getRawOptArray($pos->data);
            }

            $files = array();
            foreach ($pos->Upload as $upload) {
                array_push($files, $upload->name);
            }
            if($pos->getPostionData('auflage')) {
                $pos->count = $pos->getPostionData('auflage');
            }
            $tempTitle = $proa['title'];
            array_push($datas,
                    array(
                        'name' => $proa['title']
                        , 'count' => $pos->count
                        , 'pos' => $pos->pos
                        , 'articlegroup' => $article->getArticlegroupAsRootline()
                        , 'articletype' => $helper->getNearest($article, array(431))->title
                        , 'priceone' => $currency->toCurrency($pos->priceone)
                        , 'priceonesteuer' => $currency->toCurrency($pos->priceonesteuer)
                        , 'priceonebrutto' => $currency->toCurrency($pos->priceonebrutto)
                        , 'priceall' => $currency->toCurrency($pos->priceall)
                        , 'priceallsteuer' => $currency->toCurrency($pos->priceallsteuer)
                        , 'priceallbrutto' => $currency->toCurrency($pos->priceallbrutto)
                        , 'optionsAsArray' => $optionsAsArray
                        , 'mwert' => $article->mwert.'%'
                        , 'subdata' => array(array('name' => 'test'), array('name' => 'test2'))
                        , 'options' => implode(' | ', $options)
                        , 'options_name2' => implode(' | ', $options_name2)
                        , 'options_array' => $options
                        , 'rawoptions' => $rawoptions
                        , 'calcvalues' => Zend_Json::decode($pos->calc_values)
                        , 'files' => implode(' | ', $files)
                        , 'weight' => $pos->weight
                        , 'ref' => $pos->ref
                        , 'org_article' => $article
                        , 'delivery_date' => $pos->delivery_date
                    )
            );
            $prodpreisbrutto = $prodpreisbrutto + $pos->priceallbrutto;
            $prodpreis = $prodpreis + $pos->priceall;
            if(!isset($mwert[$article->mwert])) {
                $mwert[$article->mwert] = 0;
            }
            if(!isset($summewarenwert[$article->mwert])) {
                $summewarenwert[$article->mwert] = 0;
            }
            $mwert[$article->mwert] = ($mwert[$article->mwert]*1) + $pos->priceallsteuer;
            $summewarenwert[$article->mwert] = $summewarenwert[$article->mwert] + $pos->priceallbrutto;
            $versandMwert = $versandMwert + $pos->priceallsteuer;
            $versand = $versand + $pos->shipping_price;
            if ($article->versand == 'Fix') {
                $versand = $versand + $article->versandwert + $pos->shipping_price;
            } elseif ($article->versand == 'Position') {

            }
        }else{
            /** @var Orderspos $pos */
            foreach ($data->Orderspos as $pos) {
                $pro = unserialize($pos->data);
                $proa = $pro->getArticle();
                $proo = $pro->getOptions();
                $optionsAsArray = array();
                $article = Doctrine_Query::create()
                                ->from('Article c')
                                ->where('c.id = ?', array($pos->article_id))
                                ->fetchOne();

                if(!$article) {
                    continue;
                }
                if($article->typ == 9) {
                    $options = $article->getMarketOptArray($pos->data, true, $pos->a9_designer_data);
                    $options_name2 = $article->getMarketOptArray($pos->data, true, $pos->a9_designer_data);
                    $rawoptions = $article->getMarketRawOptArray($pos->data);
                }else{

                    if($article->a6_org_article != 0) {
                        $articleOrd = Doctrine_Query::create()
                            ->from('Article c')
                            ->where('c.id = ?', array($article->a6_org_article))
                            ->fetchOne();

                        $options = $articleOrd->getOptArray($pos->data);
                        $optionsAsArray = $articleOrd->getOptArray($pos->data, true, true);
                        $options_name2 = $articleOrd->getOptArrayName2($pos->data);
                        $rawoptions = $articleOrd->getRawOptArray($pos->data);
                    }else {
                        $options = $article->getOptArray($pos->data);
                        $optionsAsArray = $article->getOptArray($pos->data, true, true);
                        $options_name2 = $article->getOptArrayName2($pos->data);
                        $rawoptions = $article->getRawOptArray($pos->data);
                    }
                }
                $files = array();
                foreach ($pos->Upload as $upload) {
                    array_push($files, $upload->name);
                }
                if($pos->getPostionData('auflage')) {
                    $pos->count = $pos->getPostionData('auflage');
                }

                if($pos->shipping_type > 0) {
                    $shippingtype = Doctrine_Query::create()
                        ->from('Shippingtype c')
                        ->where('c.id = ?', array(intval($pos->shipping_type)))
                        ->fetchOne();

                    if ($shippingtype != false) {
                        $versand = $versand + ($pos->shipping_price);
                    }else{
                        $shippingtype = new Shippingtype();
                    }
                }else{
                    $shippingtype = new Shippingtype();
                }

                $steplayouter = array();

                if($pos->a9_designer_data != "") {
                    try{
                        $stepl = json_decode($pos->a9_designer_data, true);

                        if(isset($stepl['jsonSaveData']['foto-back_1'])) {
                            $fotoback1 = Doctrine_Query::create()->from('Motiv c')->where('c.uuid = ?', array(
                                $stepl['jsonSaveData']['foto-back_1']))->fetchOne();
                            if($fotoback1) {
                                $steplayouter['foto-back_1'] = $fotoback1->toArray();
                            }
                            $fotoback2 = Doctrine_Query::create()->from('Motiv c')->where('c.uuid = ?', array(
                                $stepl['jsonSaveData']['foto-back_2']))->fetchOne();
                            if($fotoback1) {
                                $steplayouter['foto-back_2'] = $fotoback2->toArray();
                            }
                            $fotoback3 = Doctrine_Query::create()->from('Motiv c')->where('c.uuid = ?', array(
                                $stepl['jsonSaveData']['foto-back_3']))->fetchOne();
                            if($fotoback1) {
                                $steplayouter['foto-back_3'] = $fotoback3->toArray();
                            }
                            $fotoback4 = Doctrine_Query::create()->from('Motiv c')->where('c.uuid = ?', array(
                                $stepl['jsonSaveData']['foto-back_4']))->fetchOne();
                            if($fotoback1) {
                                $steplayouter['foto-back_4'] = $fotoback4->toArray();
                            }

                        }

                    }catch(Exception $e) {

                    }
                }

                $templateprintContactObj = false;
                if($pos->getTemplatePrintContactId() != 0 && $pos->getTemplatePrintContactId() != "") {
                    $templateprintContactObj = Doctrine_Query::create()
                        ->from('Contact c')
                        ->where('c.uuid = ?', array($pos->getTemplatePrintContactId()))
                        ->fetchOne();
                }

                $tempTitle = $proa['title'];
                $tempDatas =
                        array(
                            'name' => $proa['title']
                            , 'count' => $pos->count
                            , 'pos' => $pos->pos
                            , 'articlegroup' => $article->getArticlegroupAsRootline()
                            , 'articletype' => $helper->getNearest($article, array(431))->title
                            , 'priceone' => $currency->toCurrency($pos->priceone)
                            , 'priceonesteuer' => $currency->toCurrency($pos->priceonesteuer)
                            , 'priceonebrutto' => $currency->toCurrency($pos->priceonebrutto)
                            , 'priceall' => $currency->toCurrency($pos->priceall)
                            , 'priceonecalc' => $currency->toCurrency($pos->priceall/$pos->count*1)
                            , 'priceallsteuer' => $currency->toCurrency($pos->priceallsteuer)
                            , 'priceallbrutto' => $currency->toCurrency($pos->priceallbrutto)
                            , 'mwert' => $article->mwert.'%'
                            , 'subdata' => array(array('name' => 'test'), array('name' => 'test2'))
                            , 'options' => implode('   |   ', $options)
                            , 'optionsAsArray' => $optionsAsArray
                            , 'options_name2' => implode('   |   ', $options_name2)
                            , 'options_array' => $options
                            , 'rawoptions' => $rawoptions
                            , 'calcvalues' => Zend_Json::decode($pos->calc_values)
                            , 'files' => implode('   |   ', $files)
                            , 'files_array' => $files
                            , 'weight' => $pos->weight
                            , 'steplayouter' => $steplayouter
                            , 'ref' => $pos->ref
                            , 'kst' => $pos->kst
                            , 'org_article' => $article
                            , 'shippingtype' => $shippingtype
                            , 'article_nr_extern' => $article->article_nr_extern
                            , 'article_nr_intern' => $article->article_nr_intern
                            , 'stock_place' => $article->stock_place
                            , 'stock_count' => $article->stock_count
                            , 'delivery_date' => $pos->delivery_date
                            , 'templateprintContact' => $templateprintContactObj
                        );

                if(!isset($params['kst']) || $params['kst'] == '') {
                    $params['kst'] = $pos->kst;
                }

                if(!isset($params['ref']) || $params['ref'] == '') {
                    $params['ref'] = $pos->ref;
                }

                if($article->ArticleGroup->count() && $article->ArticleGroup->getFirst()) {
                    $tempDatas['firstArticleGroup'] = $article->ArticleGroup->getFirst();
                }

                $datas[] = $tempDatas;

                $prodpreisbrutto = $prodpreisbrutto + $pos->priceallbrutto;
                $prodpreis = $prodpreis + $pos->priceall;
                if(!isset($mwert[$article->mwert])) {
                    $mwert[$article->mwert] = 0;
                }
                if(!isset($summewarenwert[$article->mwert])) {
                    $summewarenwert[$article->mwert] = 0;
                }
                $mwert[$article->mwert] = ($mwert[$article->mwert]*1) + $pos->priceallsteuer;
                $summewarenwert[$article->mwert] = $summewarenwert[$article->mwert] + $pos->priceallbrutto;
                $versandMwert = $versandMwert + $pos->priceallsteuer;
                

                if ($article->versand == 'Fix') {
                    $versand = $versand + $article->versandwert;
                } elseif ($article->versand == 'Position') {

                }
                $weight = $weight + $pos->weight;
            }
        }

        $tempMwert = array();
        
        $params['gutscheinname'] = '';
        $params['product_title'] = $tempTitle;

        $nettogutschein = 0;
        if($data->gutscheinabzug != "" && $data->gutscheinabzug > 0) {
            $tempMwert = array();
            $versandMwert = 0;
            foreach($mwert as $key => $mw) {
				
                $anteil = (((($summewarenwert[$key] / $prodpreisbrutto) * $data->gutscheinabzug)*100)/(100+$key))*($key*0.01);
                $tempMwert[$key] = $tempMwert[$key] + ($mw - $anteil);
                $nettogutschein += (((($summewarenwert[$key] / $prodpreisbrutto) * $data->gutscheinabzug)*100)/(100+$key));
                $versandMwert += ($mw - $anteil);
            }
            
            $params['gutscheinname'] = Doctrine_Query::create()->from('CreditSystemDetail c')->where('c.uuid = ?', array($data->gutschein))->fetchOne()->CreditSystem->title;
            
        }
        
        
        
        $mwert = Zend_Json::decode($data->mwertalle);

        $tmpWert = array();

        foreach($mwert as $key => $mw) {
            if($mw > 0) {
                $tmpWert[$key] = $currency->toCurrency($mw);
            }
        }
        
        $params['mwerttable'] = $tmpWert;
        $params['contactNr'] = $data->Contact->getKundenNr();
        $params['prodbrutto'] = $currency->toCurrency($prodpreisbrutto);
        $params['prodnetto'] = $currency->toCurrency($prodpreis);
        $params['netto'] = $currency->toCurrency($data->preisbrutto - $data->preissteuer);
        $params['steuer'] = $currency->toCurrency($data->preissteuer);
        $params['brutto'] = $currency->toCurrency($data->preisbrutto);
        $date = new Zend_Date($data->created , Zend_Date::ISO_8601);
        $params['created_date'] = $date->toString(Zend_Date::DATE_SHORT);
        $params['gutscheincode'] = $data->gutschein;
        $params['weight'] = $weight;
        $params['gutscheinabzug'] = $currency->toCurrency($data->gutscheinabzug);
        $params['shippingtype_extra_label'] = $data->shippingtype_extra_label;
        $params['gutscheinabzugnetto'] = $currency->toCurrency($nettogutschein);
        if($params['gutscheincode'] != "") {
            $params['gutschein'] = 1;
        }else{
            $params['gutschein'] = 0;
        }
        $params['paymentRef'] = $data->getPaymentRef();
        $params['order'] = $data->toArray(false);
        
        
        $params['sp'] = 0;

        $shippingtype = Doctrine_Query::create()
                        ->from('Shippingtype c')
                        ->where('c.id = ?', array(intval($data->shippingtype_id)))
                        ->fetchOne();
        $paymenttype = Doctrine_Query::create()
                        ->from('Paymenttype c')
                        ->where('c.id = ?', array(intval($data->paymenttype_id)))
                        ->fetchOne();
        if ($shippingtype != false) {
            $params['shippingtype'] = $shippingtype->toArray(false);
            $params['shippingprice'] = $currency->toCurrency($versand + $shippingtype->calcVersand($data->preis, $weight, $params['deliveryAddress']['country'], false, $params['deliveryAddress']['zip']) * 1);
            $params['sp'] = $versand + $shippingtype->calcVersand($data->preis, $weight, $params['deliveryAddress']['country'], false, $params['deliveryAddress']['zip']) * 1;
            $params['shippingpriceBrutto'] = $currency->toCurrency($shippingtype->calcVersand($data->preis, $weight, $params['deliveryAddress']['country'], false, $params['deliveryAddress']['zip']) * ($shippingtype->mwert*0.01+1));
        }


        if(($prodpreisbrutto - $data->gutscheinabzug) <= 0) {
            $prodpreis = 0;
        }



        
        if ($paymenttype != false) {
            $params['paymenttype'] = $paymenttype->toArray(false);
            if ($paymenttype->prozent == 1) {
                $params['paymentprice'] = $currency->toCurrency( ($prodpreis) / 100 * $paymenttype->wert * 1);
                $params['sp'] = $params['sp'] + ( ($prodpreis) / 100 * $paymenttype->wert * 1);
            }else{
                $params['paymentprice'] = $currency->toCurrency($paymenttype->wert * 1);
                $params['sp'] = $params['sp'] + $paymenttype->wert * 1;
            }
        }

        $params['spraw'] = $params['sp']*1;
        $params['sp'] = $currency->toCurrency($params['sp']);
        $params['POS_COUNT'] = count($datas);


        try {
            $file = new TP_Rdl(
                            $reportPath
                            , $what . ".jrxml"
                            , 1
                            , $params
                            , $datas
            );
            
            $background = false;
            if($what == "label" && $shop->report_background_label) {
                $background = true;
            }elseif($what == "jobtiket" && $shop->report_background_job) {
                $background = true;
            }elseif($what == "invoice" && $shop->report_background_invoice) {
                $background = true;
            }elseif($what == "delivery" && $shop->report_background_delivery) {
                $background = true;
            }elseif($what == "offer" && $shop->report_background_offer) {
                $background = true;
            }elseif($what == "order" && $shop->report_background_order) {
                $background = true;
            }elseif($what == "storno" && $shop->report_background_invoice) {
                $background = true;
            }

            if(!file_exists(PUBLIC_PATH . '/temp/thumb/' . $shop->uid)) {
                mkdir(PUBLIC_PATH . '/temp/thumb/' . $shop->uid, 0777, true);
            }

            if ($shop->reportsite1 != "" && $background) {
                $pdf = Doctrine_Query::create()
                                ->from('Image as i')
                                ->where('i.id = ?')->fetchOne(array($shop->reportsite1));

                if (!file_exists(PUBLIC_PATH . '/temp/thumb/' . $shop->uid . '/'. $shop->reportsite1 . '.jpg')) {
                    $fileDownload = 'https://cdn.shopsrv.de/' . TP_Util::encode($host . '/' . $pdf->path) . '?w=2480&fm=jpg';

                    file_put_contents(PUBLIC_PATH . '/temp/thumb/' . $shop->uid . '/'. $shop->reportsite1 . '.jpg', file_get_contents($fileDownload));
                }


                $file->addSubmittals(PUBLIC_PATH . '/temp/thumb/' . $shop->uid . '/'. $shop->reportsite1 . '.jpg');
            }
            if ($shop->reportsite2 != "" && $background) {
                $pdf = Doctrine_Query::create()
                                ->from('Image as i')
                                ->where('i.id = ?')->fetchOne(array($shop->reportsite2));

                if (!file_exists(PUBLIC_PATH . '/temp/thumb/' . $shop->uid . '/'. $shop->reportsite2 . '.jpg')) {
                    $fileDownload = 'https://cdn.shopsrv.de/' . TP_Util::encode($host . '/' . $pdf->path) . '?w=2480&fm=jpg';

                    file_put_contents(PUBLIC_PATH . '/temp/thumb/' . $shop->uid . '/'. $shop->reportsite2 . '.jpg', file_get_contents($fileDownload));
                }


                $file->addSubmittals(PUBLIC_PATH . '/temp/thumb/' . $shop->uid . '/'. $shop->reportsite2 . '.jpg');

            }

            if ($shop->report_agb != "" && $background) {
                $pdf = Doctrine_Query::create()
                                ->from('Image as i')
                                ->where('i.id = ?')->fetchOne(array($shop->report_agb));


                if (!file_exists(PUBLIC_PATH . '/temp/thumb/' . $shop->uid . '/'. $shop->report_agb . '.jpg')) {
                    $fileDownload = 'https://cdn.shopsrv.de/' . TP_Util::encode($host . '/' . $pdf->path) . '?w=2480&fm=jpg';



                    file_put_contents(PUBLIC_PATH . '/temp/thumb/' . $shop->uid . '/'. $shop->report_agb . '.jpg', file_get_contents($fileDownload));
                }


                 $file->setAgb(PUBLIC_PATH . '/temp/thumb/' . $shop->uid . '/'. $shop->report_agb . '.jpg');

            }

            if (!file_exists(APPLICATION_PATH . '/../data/customerdocs')) {
                mkdir(APPLICATION_PATH . '/../data/customerdocs', 0777);
            }
            if (!file_exists(APPLICATION_PATH . '/../data/customerdocs/' . $shop->id)) {
                mkdir(APPLICATION_PATH . '/../data/customerdocs/' . $shop->id, 0777);
            }

            $file->export(new TP_Rdl_Renderer_Pdf(APPLICATION_PATH . '/../data/customerdocs/' . $shop->id . "/" . $data->alias . '_' . $what . '.pdf'));
            Zend_Registry::get('log')->info(APPLICATION_PATH . '/../data/customerdocs/' . $shop->id . "/" . $data->alias . '_' . $what . '.pdf');
        } catch (Exception $e) {
            Zend_Registry::get('log')->info($e->getMessage());
            Zend_Registry::get('log')->info($e->getTraceAsString());
            return false;
        }

        return APPLICATION_PATH . '/../data/customerdocs/' . $shop->id . '/' . $data->alias . '_' . $what . '.pdf';
    }

    public static function clearCache($mode = TP_Util::CLEAR_APC) {

        /* $memcache_obj = new Memcache();
          $memcache_obj->connect('localhost', 11211);
          $memcache_obj->flush();
         */
        if(!Zend_Registry::isRegistered('shop')) {
            return;
        }
        $shop = Zend_Registry::get('shop');
        if ($mode == TP_Util::CLEAR_ALL && file_exists(PUBLIC_PATH . '/temp/thumb/' . $shop['uid'])) {
            
            foreach (new DirectoryIterator(PUBLIC_PATH . '/temp/thumb/' . $shop['uid']) as $fileInfo) {
                if ($fileInfo->isDot() || $fileInfo->getFilename() == ".svn")
                    continue;
                unlink($fileInfo->getPathname());
            }
        }
        if ($mode == TP_Util::CLEAR_ALL && file_exists(APPLICATION_PATH . '/../cache/' . $shop['uid'])) {

            foreach (new DirectoryIterator(APPLICATION_PATH . '/../cache/' . $shop['uid']) as $fileInfo) {
                if ($fileInfo->isDot() || $fileInfo->getFilename() == ".svn")
                    continue;
                unlink($fileInfo->getPathname());
            }
        }

    }
    
    public static function clearShop($shop) {
    
            if(file_exists(APPLICATION_PATH . '/../cache/' . $shop['uid'])) {
                foreach (new DirectoryIterator(APPLICATION_PATH . '/../cache/' . $shop['uid']) as $fileInfo) {
                    if ($fileInfo->isDot() || $fileInfo->getFilename() == ".svn")
                        continue;
                    unlink($fileInfo->getPathname());
                }
            }
    }

    public static function clearParams($params) {
        unset($params['config']);
        unset($params['save']);
        unset($params['module']);
        unset($params['controller']);
        unset($params['action']);
        unset($params['sid']);
        unset($params['uid']);
        unset($params['format']);
        unset($params['json']);
        unset($params['headingsize']);

        return $params;
    }

    public static function cleanArray($data, $fields) {
        $temp = array();

        foreach ($data as $key => $value) {
            if (in_array($key, $fields))
                $temp[$key] = $value;
        }
        return $temp;
    }

}
