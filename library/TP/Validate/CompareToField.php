<?php 


/**
 * Klasse zum Vergleichen zweier Felder auf gleiche Werte<br /<br />
 *
 * <code>
 * $form = new Zend_Form();
 *
 * $element_1 = $form->createElement('text', 'f_name_element_1');
 * $element_2 = $form->createElement('text', 'f_name_element_2');
 * $element_2->addValidator('CompareToField', false, array('f_name_element_1'));
 * </code>
 *
 *
 * @author
 * @date     $Date: 2010-11-06 02:43:11 +0100 (Sat, 06 Nov 2010) $
 * @version  $Revision: 5163 $
 * @package
 */
class TP_Validate_CompareToField extends Zend_Validate_Abstract
{
    /**
     * Validation failure message key for when ...
     */
    const NOT_MATCH = 'notMatch';

    /**
     * Validation failure message template definitions
     *
     * @var     array
     * @access  protected
     */
    protected $_messageTemplates = array(
        self::NOT_MATCH => 'Die eingebenen Werte in den Felder stimmen nicht überein'
        );

    /**
     * Feldname des zweiten Feldes für die Überprüfung
     *
     * @var     string
     * @access  protected
     */
    protected $_field;


    /**
     * Konstruktor, setzt den Namen des zweiten, zu vergleichenden Feldes
     *
     * @param   string $field Feldname
     * @return  void
     * @access  public
     */
    public function __construct($field = null)
    {
        $this->setField($field);
    }

    /**
     * Liefert den zweiten, zu vergleichenden Feldnamen
     *
     * @return  string Feldname
     * @access  public
     */
    public function getField()
    {
        return $this->_field;
    }

    /**
     * Setzt den zweiten, zu vergleichenden Feldnamen
     *
     * @param   string $field Feldname
     * @return  VDB_Validate_CompareToField für die Nutzung als Fluent Interface
     * @access  public
     */
    public function setField($field)
    {
        $this->_field = $field;
        return $this;
    }

    /**
     * Definiert bei Zend_Validate_Interface<br /><br />
     *
     * Liefert false zurück wenn die Felder gleiche Werte haben, sonst true
     *
     * @param   string $value Wert des Feldes, dem der Validator zugeordnet ist
     * @param   mixed $context string oder array der übertragenen Formularwerte
     * @return  boolean
     */
    public function isValid($value, $context = null)
    {
        $value = (string) $value;
        $this->_setValue($value);

        if (is_array($context))
        {
            if (isset($context[$this->_field])
                && ($value != $context[$this->_field]))
            {
                $this->_error(self::NOT_MATCH);
                return false;
            }
        }
        elseif (is_string($context) && ($value != $context))
        {
            $this->_error(self::NOT_MATCH);
            return false;
        }

        return true;
    }
}