<?php

/**
 * Zend Framework
 *
 * LICENSE
 *
 * This source file is subject to the new BSD license that is bundled
 * with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://framework.zend.com/license/new-bsd
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@zend.com so we can send you a copy immediately.
 *
 * @category   Zend
 * @package    Zend_Validate
 * @copyright  Copyright (c) 2005-2009 Zend Technologies USA Inc. (http://www.zend.com)
 * @license    http://framework.zend.com/license/new-bsd     New BSD License
 * @version    $Id: NoRecordExistsDoctrine.php 5163 2010-11-06 01:43:11Z boonkerz $
 */

/**
 * @see Zend_Validate_Db_Abstract
 */
require_once 'Zend/Validate/Db/Abstract.php';

/**
 * Confirms a record does not exist in a table.
 *
 * @category   Zend
 * @package    Zend_Validate
 * @uses       Zend_Validate_Db_Abstract
 * @copyright  Copyright (c) 2005-2009 Zend Technologies USA Inc. (http://www.zend.com)
 * @license    http://framework.zend.com/license/new-bsd     New BSD License
 */
class TP_Validate_Db_NoRecordExistsDoctrine extends Zend_Validate_Db_Abstract {
	
	public function isValid($value) {
		$shop = Zend_Registry::get ( 'shop' );
		$valid = true;
		$this->_setValue ( $value );
		$result = Doctrine_Query::create ()->from ( $this->_table . ' m' );
		if($this->_field == 'self_email') {
			$result = $result->leftJoin('m.Shops as s')->where ( 'm.' . $this->_field . ' = ? AND m.virtual = 0 AND m.enable = 1 AND s.id = ?', array ($value, $shop ['id'] ) )->fetchOne ();
		}else{
			$result = $result->where ( 'm.' . $this->_field . ' = ? AND m.enable = 1 AND m.virtual = 0 AND m.install_id = ?', array ($value, $shop ['install_id'] ) )->fetchOne ();
		}
		if ($result != false) {
			$valid = false;
			$this->_error ( self::ERROR_RECORD_FOUND );
		}
		return $valid;
	}
}
