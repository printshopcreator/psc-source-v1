<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of WebdavAuth
 *
 * @author boonkerz
 */
class TP_WebdavAuth extends ezcWebdavDigestAuthenticatorBase implements ezcWebdavAuthorizer {

    protected $highRole;
    
    public function authenticateAnonymous( ezcWebdavAnonymousAuth $data )
    {
        return false;
    }

    public function authenticateBasic( ezcWebdavBasicAuth $data )
    {
        $_authAdapter = new TP_Plugin_AuthAdapter(); // put this in a constructor?
        $_authAdapter->setIdentity($data->username)
                     ->setCredential($data->password);
        $result = Zend_Auth::getInstance()->authenticate($_authAdapter);

 
        $row = Doctrine_Query::create()
                    ->from('Contact m')
                    ->where('m.name = ? AND m.enable = 1', array($data->username))
                    ->fetchOne();
                   
        
        if ($row['id'] != '' && $row['password'] == $data->password) {
            //$this->auth = $row;
           
            return true;
        }
        return false;
    }

    public function authenticateDigest( ezcWebdavDigestAuth $data )
    {
        

  
        $row = Doctrine_Query::create()
                    ->from('Contact m')
                    ->where('m.name = ? AND m.enable = 1', array($data->username))
                    ->fetchOne()->toArray(false);

        

        if ($this->checkDigest($data, $row['password'])) {
            //$this->auth = $row;
            $_authAdapter = new TP_Plugin_AuthAdapter(); // put this in a constructor?
            $_authAdapter->setIdentity($data->username)
                         ->setCredential($row['password']);
            $result = Zend_Auth::getInstance()->authenticate($_authAdapter);
            return true;
        }
        return false;
    }

    public function authorize( $user, $path, $access = ezcWebdavAuthorizer::ACCESS_READ )
    {
        $user = Zend_Auth::getInstance()->getIdentity();
        $user = Doctrine_Query::create()
                ->from('Contact m')
                ->where('id = ?')->fetchOne(array($user['id']));

        if(!file_exists(PATHWEBDAV . '/contacts/' . $user['id'])) {
            mkdir(PATHWEBDAV . '/contacts/' . $user['id']);
        }

        if($user!=false) {
	        foreach($user->Roles as $rol) {
	             if($this->highRole == null || $rol->level > $this->highRole->level) $this->highRole = $rol;
	        }
        }

        if($this->highRole->level >= 30 && $this->highRole->level <= 50) {
            foreach($user->Shops as $shop) {
                if(strpos($path, $shop->uid) != false) {
                    return true;
                }
            }
        }

        if (strpos($path, 'contacts/'.$user['id']) || $path == '/shops' || $path == '/uploads' || $path == '//shops' || $path == '//uploads' || $path == '//contacts' || $path == '/contacts' || $path == '/')
        {
            return true;
        }
        
        return false;
    }
}

