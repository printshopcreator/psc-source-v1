<?php return array (
    'sans-serif' =>
    array (
        'normal' => DOMPDF_FONT_DIR . 'Helvetica',
        'bold' => DOMPDF_FONT_DIR . 'Helvetica-Bold',
        'italic' => DOMPDF_FONT_DIR . 'Helvetica-Oblique',
        'bold_italic' => DOMPDF_FONT_DIR . 'Helvetica-BoldOblique',
    ),
    'times' =>
    array (
        'normal' => DOMPDF_FONT_DIR . 'Times-Roman',
        'bold' => DOMPDF_FONT_DIR . 'Times-Bold',
        'italic' => DOMPDF_FONT_DIR . 'Times-Italic',
        'bold_italic' => DOMPDF_FONT_DIR . 'Times-BoldItalic',
    ),
    'times-roman' =>
    array (
        'normal' => DOMPDF_FONT_DIR . 'Times-Roman',
        'bold' => DOMPDF_FONT_DIR . 'Times-Bold',
        'italic' => DOMPDF_FONT_DIR . 'Times-Italic',
        'bold_italic' => DOMPDF_FONT_DIR . 'Times-BoldItalic',
    ),
    'courier' =>
    array (
        'normal' => DOMPDF_FONT_DIR . 'Courier',
        'bold' => DOMPDF_FONT_DIR . 'Courier-Bold',
        'italic' => DOMPDF_FONT_DIR . 'Courier-Oblique',
        'bold_italic' => DOMPDF_FONT_DIR . 'Courier-BoldOblique',
    ),
    'helvetica' =>
    array (
        'normal' => DOMPDF_FONT_DIR . 'Helvetica',
        'bold' => DOMPDF_FONT_DIR . 'Helvetica-Bold',
        'italic' => DOMPDF_FONT_DIR . 'Helvetica-Oblique',
        'bold_italic' => DOMPDF_FONT_DIR . 'Helvetica-BoldOblique',
    ),
    'verdana' =>
    array (
        'normal' => DOMPDF_FONT_DIR . 'verdana',
        'bold' => DOMPDF_FONT_DIR . 'verdanab',
        'italic' => DOMPDF_FONT_DIR . 'verdanai',
        'bold_italic' => DOMPDF_FONT_DIR . 'verdanaz',
    ),
    'zapfdingbats' =>
    array (
        'normal' => DOMPDF_FONT_DIR . 'ZapfDingbats',
        'bold' => DOMPDF_FONT_DIR . 'ZapfDingbats',
        'italic' => DOMPDF_FONT_DIR . 'ZapfDingbats',
        'bold_italic' => DOMPDF_FONT_DIR . 'ZapfDingbats',
    ),
    'symbol' =>
    array (
        'normal' => DOMPDF_FONT_DIR . 'Symbol',
        'bold' => DOMPDF_FONT_DIR . 'Symbol',
        'italic' => DOMPDF_FONT_DIR . 'Symbol',
        'bold_italic' => DOMPDF_FONT_DIR . 'Symbol',
    ),
    'serif' =>
    array (
        'normal' => DOMPDF_FONT_DIR . 'Times-Roman',
        'bold' => DOMPDF_FONT_DIR . 'Times-Bold',
        'italic' => DOMPDF_FONT_DIR . 'Times-Italic',
        'bold_italic' => DOMPDF_FONT_DIR . 'Times-BoldItalic',
    ),
    'monospace' =>
    array (
        'normal' => DOMPDF_FONT_DIR . 'Courier',
        'bold' => DOMPDF_FONT_DIR . 'Courier-Bold',
        'italic' => DOMPDF_FONT_DIR . 'Courier-Oblique',
        'bold_italic' => DOMPDF_FONT_DIR . 'Courier-BoldOblique',
    ),
    'fixed' =>
    array (
        'normal' => DOMPDF_FONT_DIR . 'Courier',
        'bold' => DOMPDF_FONT_DIR . 'Courier-Bold',
        'italic' => DOMPDF_FONT_DIR . 'Courier-Oblique',
        'bold_italic' => DOMPDF_FONT_DIR . 'Courier-BoldOblique',
    ),
    'dejavu sans' =>
    array (
        'bold' => DOMPDF_FONT_DIR . 'DejaVuSans-Bold',
        'bold_italic' => DOMPDF_FONT_DIR . 'DejaVuSans-BoldOblique',
        'italic' => DOMPDF_FONT_DIR . 'DejaVuSans-Oblique',
        'normal' => DOMPDF_FONT_DIR . 'DejaVuSans',
    ),
    'dejavu sans light' =>
    array (
        'normal' => DOMPDF_FONT_DIR . 'DejaVuSans-ExtraLight',
    ),
    'dejavu sans condensed' =>
    array (
        'bold' => DOMPDF_FONT_DIR . 'DejaVuSansCondensed-Bold',
        'bold_italic' => DOMPDF_FONT_DIR . 'DejaVuSansCondensed-BoldOblique',
        'italic' => DOMPDF_FONT_DIR . 'DejaVuSansCondensed-Oblique',
        'normal' => DOMPDF_FONT_DIR . 'DejaVuSansCondensed',
    ),
    'dejavu sans mono' =>
    array (
        'bold' => DOMPDF_FONT_DIR . 'DejaVuSansMono-Bold',
        'bold_italic' => DOMPDF_FONT_DIR . 'DejaVuSansMono-BoldOblique',
        'italic' => DOMPDF_FONT_DIR . 'DejaVuSansMono-Oblique',
        'normal' => DOMPDF_FONT_DIR . 'DejaVuSansMono',
    ),
    'dejavu serif' =>
    array (
        'bold' => DOMPDF_FONT_DIR . 'DejaVuSerif-Bold',
        'bold_italic' => DOMPDF_FONT_DIR . 'DejaVuSerif-BoldItalic',
        'italic' => DOMPDF_FONT_DIR . 'DejaVuSerif-Italic',
        'normal' => DOMPDF_FONT_DIR . 'DejaVuSerif',
    ),
    'dejavu serif condensed' =>
    array (
        'bold' => DOMPDF_FONT_DIR . 'DejaVuSerifCondensed-Bold',
        'bold_italic' => DOMPDF_FONT_DIR . 'DejaVuSerifCondensed-BoldItalic',
        'italic' => DOMPDF_FONT_DIR . 'DejaVuSerifCondensed-Italic',
        'normal' => DOMPDF_FONT_DIR . 'DejaVuSerifCondensed',
    ),'audiowide' =>
  array (
    'normal' => DOMPDF_FONT_DIR . '63eaae0c038b2abfeaf4261a6a42222a',
  ),
  'cinzel decorative' => 
  array (
    'normal' => DOMPDF_FONT_DIR . '976abbed8428944352b8e6ce21864253',
    'bold' => DOMPDF_FONT_DIR . 'Cinzel-Decorative-Bold',
  ),
  'ewert' => 
  array (
    'normal' => DOMPDF_FONT_DIR . '4a2936d6b1f690bdae89f1835f2f0af6',
  ),
  'great vibes' => 
  array (
    'normal' => DOMPDF_FONT_DIR . '2e9c2ee5ab60fe9e9c230394753f3c78',
  ),
    'greatvibes' =>
    array (
        'normal' => DOMPDF_FONT_DIR . '2e9c2ee5ab60fe9e9c230394753f3c78',
    ),
  'kaushan script' => 
  array (
    'normal' => DOMPDF_FONT_DIR . '1fc8d467d6c94c330351b23c39e5a098',
  ),
  'mr bedfort' => 
  array (
    'normal' => DOMPDF_FONT_DIR . 'c4170e571bb3c3d506a9db2a1ddb9e59',
  ),
  'seaweed script' => 
  array (
    'normal' => DOMPDF_FONT_DIR . 'e7673fd7508386066da10bccd7484262',
  ),
  'unifrakturcook' => 
  array (
    'normal' => DOMPDF_FONT_DIR . 'e62d986e5ef4f7b2765a82e3e8c23cbe',
  ),
  'unifrakturmaguntia' =>
  array (
    'normal' => DOMPDF_FONT_DIR . 'UnifrakturMaguntia',
  ),
    'euphoria script' =>
    array (
        'normal' => DOMPDF_FONT_DIR . 'Euphoria-Script',
    ),
    'codystar' =>
    array (
        'normal' => DOMPDF_FONT_DIR . 'Codystar-Regular',
    ),
    'ewert' =>
    array (
        'normal' => DOMPDF_FONT_DIR . 'Ewert-Regular',
    ),
    'faster one' =>
    array (
        'normal' => DOMPDF_FONT_DIR . 'FasterOne-Regular',
    ),
    'rye' =>
    array (
        'normal' => DOMPDF_FONT_DIR . 'Rye-Regular',
    ),



) ?>