<?php

require_once ('/data/www/new/src/PSC/Libraries/CalcBundle/Session/Calc.php');
require_once ('/data/www/new/src/PSC/Shop/ContactBundle/Session/AddressImport.php');
if(file_exists('/data/www/new/var/plugins/System/PSC/FormLayouter/Session/FormLayouter.php')) {
    require_once ('/data/www/new/var/plugins/System/PSC/FormLayouter/Session/FormLayouter.php');
}


define('APPLICATION_PATH',
              realpath(dirname(__FILE__) . '/../application'));

define('PUBLIC_PATH',
              realpath(dirname(__FILE__)));

set_include_path(implode(PATH_SEPARATOR, array(
    realpath(APPLICATION_PATH . '/../library'),
    get_include_path(),
)));

defined('APPLICATION_ENV')
    || define('APPLICATION_ENV',
              (getenv('APPLICATION_ENV') ? getenv('APPLICATION_ENV')
                                         : 'production'));
require_once 'Zend/Application.php';

$application = new Zend_Application(
    APPLICATION_ENV,
    APPLICATION_PATH . '/configs/application.ini'
);
if (strrpos(strtolower(PHP_SAPI), 'cli') !== false) {
    $application->bootstrap(array('Autoload', 'Db', 'Cli', 'ArticleQueues'))->run();
}else{
    try{
        $application->bootstrap()->run();
    }  catch (Exception $e) {
        Zend_Registry::get('log')->debug($e->getMessage());
    }
}
