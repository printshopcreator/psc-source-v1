Ext.onReady(function() {


    Ext.QuickTips.init();

    var translation = new Locale.Gettext();
    translation.textdomain('Contact');

    var win = new Ext.Window({
         id:'metaform-win'
        ,layout:'fit'
        ,width: 800
        ,scope:this
        ,height: 600
        ,title: translation._('account_edit_js_Edit Account')
        ,items:{
             xtype:'metaform'
            ,url:'/admin/account/edit?config=5&sid='+ sid + '&uid=' + uid
            ,buttons:
            [{
                 text: translation._('account_edit_js_Save')
                ,iconCls: 'action_applyChanges'
                ,listeners:{
                   click:{scope:this, buffer:200, fn:function(btn) {
                           if(Ext.getCmp('metaform-win').items.get(0).getForm().isValid()) {
                                Ext.getCmp('metaform-win').items.get(0).getForm().submit({waitMsg: translation._('account_edit_js_Saving')
                                    ,url: '/admin/account/edit?config=5&sid='+ sid + '&uid=' + uid + '&save=1'
                                    ,success: function(form, action){
                                      if(action.result.isNew) {
                                            window.location = '/admin/account/edit?sid='+ sid + '&uid=' + action.result.uid;
                                      }
                                    }});
                           }else{

                                Ext.getCmp('metaform-win').items.get(0).getForm().items.find(function(f) {

                                    if(!f.isValid()) {
                                        ParentTab = f.findParentByType('tabpanel');
                                        TabPanel = f.findParentByType('panel');
                                        if(TabPanel.iconCls != 'required')
                                            TabPanel.setIconClass( 'required' );

                                    };

                                });

                                Ext.MessageBox.alert(translation._('Validation_Failed'), translation._('Validation_Failed_Message'));
                            }
                }}}
            },{
                  text: translation._('account_edit_js_Save and Close')
                ,iconCls: 'action_applyChangesClose'
                ,listeners:{
                   click:{scope:this, buffer:200, fn:function(btn) {
                           if(Ext.getCmp('metaform-win').items.get(0).getForm().isValid()) {
                               Ext.getCmp('metaform-win').items.get(0).getForm().submit({waitMsg: translation._('account_edit_js_Saving')
                               , url: '/admin/account/edit?config=5&sid='+ sid + '&uid=' + uid + '&save=1'
                               ,success: function(form, action){
                                                      window.close();
                                                    }
                               });
                           }else{

                                Ext.getCmp('metaform-win').items.get(0).getForm().items.find(function(f) {

                                    if(!f.isValid()) {
                                        ParentTab = f.findParentByType('tabpanel');
                                        TabPanel = f.findParentByType('panel');
                                        if(TabPanel.iconCls != 'required')
                                            TabPanel.setIconClass( 'required' );

                                    };

                                });

                                Ext.MessageBox.alert(translation._('Validation_Failed'), translation._('Validation_Failed_Message'));
                            }
                }}}
            },{
                text: translation._('account_edit_js_Close'),
                listeners:{
                   click:{scope:this, buffer:200, fn:function(btn) {
                        window.close();
                }}},
                iconCls: 'action_cancel'
            }]
        }
    });

    win.show();
});
