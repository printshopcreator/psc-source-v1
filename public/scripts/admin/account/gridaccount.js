Ext.ns('PrintShopCreator.Admin.Account');

var translation = new Locale.Gettext();
translation.textdomain('Contact');

PrintShopCreator.Admin.Account.Grid = Ext.extend(Ext.grid.GridPanel, {
	 
	border:false
	,layout: 'fit'
	,stateful:true
	,url:'/admin/account/all?config=2&sid=' + sid + '&market=' + market 
	,objName:'account'
	,idName:'id'
	,frame: false
	,autoHeight: true
	,autoWidth: true
	,id: 'accountgrid'
	,title: translation._('account_gridaccount_js_title_Accounts')
	,initComponent:function() {

		// create row actions
		this.rowActions = new Ext.ux.grid.RowActions({
			 actions:[{
				 iconCls:'icon-go-tab'
				,qtip: translation._('account_gridaccount_js_Filter')
			},{
				 iconCls:'icon-edit-record'
				,qtip: translation._('account_gridaccount_js_Edit')
			}]
			,widthIntercept:Ext.isSafari ? 4 : 2
			,id:'actions'
		});
		this.rowActions.on('action', this.onRowAction, this);
		
		var filters = new Ext.ux.grid.GridFilters({
		  filters:[
			{type: 'string',  dataIndex: 'company'}
		]});
		
		Ext.apply(this, {
			// {{{
			store:new Ext.data.Store({
				reader:new Ext.data.JsonReader({
					 id:'id'
					,totalProperty:'totalCount'
					,root:'rows'
					,fields:[
						 {name:'id', type:'int'}
						,{name:'company', type:'string'}
                        ,{name:'appendix', type:'string'}
						,{name:'zip', type:'string'}
						,{name:'city', type:'string'}
						,{name:'phone', type:'string'}
						,{name:'email', type:'string'}
					]
				})
				,proxy:new Ext.data.HttpProxy({url:this.url})
				,baseParams:{cmd:'getData', objName:this.objName}
				,sortInfo:{field:'company', direction:'ASC'}
				,remoteSort:true
			})
			// }}}
			// {{{
			,columns:[{
				 header: translation._('account_gridaccount_js_Company')
				,id:'company'
				,dataIndex:'company'
				,width:160
				,sortable:true
				,editor:new Ext.form.TextField({
					allowBlank:false
				})
			},{
                header: translation._('Zusatz/Filiale')
                ,id:'appendix'
                ,dataIndex:'appendix'
                ,width:160
                ,sortable:true
                ,editor:new Ext.form.TextField({
                    allowBlank:false
                })
            },{
				 header: translation._('account_gridaccount_js_Zip')
				,id:'zip'
				,dataIndex:'zip'
				,width:100
				,sortable:true
				,editor:new Ext.form.TextField({
					allowBlank:false
				})
			},{
				 header: translation._('account_gridaccount_js_City')
				,id:'city'
				,dataIndex:'city'
				,width:160
				,sortable:true
				,editor:new Ext.form.TextField({
					allowBlank:false
				})
			},{
				 header: translation._('account_gridaccount_js_Phone')
				,id:'phone'
				,dataIndex:'phone'
				,width:160
				,sortable:true
				,editor:new Ext.form.TextField({
					allowBlank:false
				})
			},{
				 header: translation._('account_gridaccount_js_Email')
				,id:'Email'
				,dataIndex:'email'
				,width:160
				,sortable:true
				,editor:new Ext.form.TextField({
					allowBlank:false
				})
				,renderer: function(val) { return '<a href="mailto:'+val+'">'+val+'</a>'; }
			}, this.rowActions]
			// }}}
			,plugins:[ this.rowActions, filters]
			,viewConfig:{forceFit:true}
			,tbar:[{
				 text: translation._('account_gridaccount_js_Add Account')
				,iconCls:'icon-form-add'
				,listeners:{
					click:{scope:this, buffer:200, fn:function(btn) {
						PrintShopCreator.Base.Common.openWindow('accountWindow', '/admin/account/edit?sid='+ sid, 800, 600);
					}}
				}
			}]
		}); // eo apply

		this.bbar = new Ext.PagingToolbar({
			 store:this.store
			,pageSize:10
			,displayInfo: true
			,plugins: [filters,new Ext.ux.ProgressBarPager()]
		});

		// call parent
		PrintShopCreator.Admin.Account.Grid.superclass.initComponent.apply(this, arguments);
	} // eo function initComponent
	// {{{
	,onRender:function() {
		// call parent
		PrintShopCreator.Admin.Account.Grid.superclass.onRender.apply(this, arguments);

		// load store
		this.store.load({params:{start:0,limit:10}});

	} // eo function onRender
	// }}}

	,addRecord:function() {
		var store = this.store;
		if(store.recordType) {
			var rec = new store.recordType({newRecord:true});
			
			return rec;
		}
		return false;
	} // eo function addRecord

	,onRowAction:function(grid, record, action, row, col) {
		switch(action) {
			case 'icon-go-tab':
				var grid2 = Ext.getCmp('PrintShopCreatorAdminContactGrid');
				grid2.store.reload({params:{start:0,limit:10, account_id: record.id}});
				var grid3 = Ext.getCmp('PrintShopCreatorAdminOrderGrid');
				grid3.store.reload({params:{start:0,limit:10, account_id: record.id}});
				
			break;
			
			case 'icon-minus':
				this.deleteRecord(record);
			break;

			case 'icon-edit-record':
				PrintShopCreator.Base.Common.openWindow('accountWindow', '/admin/account/edit?sid='+ sid + '&uid=' + record.id, 800, 600);
			break;
		}
	} // eo onRowAction

	,commitChanges:function() {
		var records = this.store.getModifiedRecords();
		if(!records.length) {
			return;
		}
		var data = [];
		Ext.each(records, function(r, i) {
			var o = r.getChanges();
			if(r.data.newRecord) {
				o.newRecord = true;
			}
			o[this.idName] = r.get(this.idName);
			data.push(o);
		}, this);
		var o = {
			 url:this.url
			,method:'post'
			,callback:this.requestCallback
			,scope:this
			,params:{
				 cmd:'saveData'
				,objName:this.objName
				,data:Ext.encode(data)
			}
		};
		Ext.Ajax.request(o);
	} // eo function commitChanges

	,requestCallback:function(options, success, response) {
		if(true !== success) {
			this.showError(response.responseText);
			return;
		}
		try {
			var o = Ext.decode(response.responseText);
		}
		catch(e) {
			this.showError(response.responseText, 'Cannot decode JSON object');
			return;
		}
		if(true !== o.success) {
			this.showError(o.error || 'Unknown error');
			return;
		}

		switch(options.params.cmd) {
			case 'saveData':
				var records = this.store.getModifiedRecords();
				Ext.each(records, function(r, i) {
					if(o.insertIds && o.insertIds[i]) {
						r.set(this.idName, o.insertIds[i]);
						delete(r.data.newRecord);
					}
				});
				this.store.commitChanges();
			break;

			case 'deleteData':
			break;
		}
	} // eo function requestCallback

	,showError:function(msg, title) {
		Ext.Msg.show({
			 title:title || 'Error'
			,msg:Ext.util.Format.ellipsis(msg, 2000)
			,icon:Ext.Msg.ERROR
			,buttons:Ext.Msg.OK
			,minWidth:1200 > String(msg).length ? 360 : 600
		});
	} // eo function showError

	,deleteRecord:function(record) {
		Ext.Msg.show({
			 title: translation._('msg_delete')
			,msg: translation._('msg_delete_confirm_beforetext') + ':<br><b>' + record.get('company') + '</b><br>' + translation._('msg_delete_confirm_aftertext')
			,icon:Ext.Msg.QUESTION
			,buttons:Ext.Msg.YESNO
			,scope:this
			,fn:function(response) {
				if('yes' !== response) {
					return;
				}
//				console.info('Deleting record');
			}
		});
	} // eo function deleteRecord

}); // eo extend

// register xtype
Ext.reg('PrintShopCreatorAdminAccountGrid', PrintShopCreator.Admin.Account.Grid);
Ext.QuickTips.init();
	
	var account = new Ext.FormPanel({
		               
        renderTo: 'accountindex',
        closable: false,
        resizable: false,
        layout: 'fit',
        border: false,
        items:[{xtype:'PrintShopCreatorAdminAccountGrid', id:'PrintShopCreatorAdminAccountGrid'}]
});
account.show();
