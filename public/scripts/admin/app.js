Ext.BLANK_IMAGE_URL = '/scripts/extjs/resources/images/default/s.gif';


var translation = new Locale.Gettext();
translation.textdomain('App');

DocPanel = Ext.extend(Ext.Panel, {
    closable: true,
    autoScroll:true,

    initComponent : function(){
        var ps = this.cclass.split('.');
        this.title = ps[ps.length-1];

        DocPanel.superclass.initComponent.call(this);
    },

    scrollToMember : function(member){
        var el = Ext.fly(windowwindowthis.cclass + '-' + member);
        if(el){
            var top = (el.getOffsetsTo(this.body)[1]) + this.body.dom.scrollTop;
            this.body.scrollTo('top', top-25, {
                duration:.75,
                callback: this.hlMember.createDelegate(this, [member])
            });
        }
    },

    scrollToSection : function(id){
        var el = Ext.getDom(id);
        if(el){
            var top = (Ext.fly(el).getOffsetsTo(this.body)[1]) + this.body.dom.scrollTop;
            this.body.scrollTo('top', top-25, {
                duration:.5,
                callback: function(){
                    Ext.fly(el).next('h2').pause(.2).highlight('#8DB2E3', {
                        attr:'color'
                    });
                }
            });
        }
    },

    hlMember : function(member){
        var el = Ext.fly(this.cclass + '-' + member);
        if(el){
            el.up('tr').highlight('#cadaf9');
        }
    }
});

MainPanel = function(){
	
    if(role == 30) {
        MainPanel.superclass.constructor.call(this, {
            region:'center',
	   
            margins:'0 5 5 0',
            deferredRender: true,
            require   : ['@mif', 'ux/crc32'],
            resizeTabs: true,
            minTabWidth: 135,
            tabWidth: 135,
            //plugins: new Ext.ux.TabCloseMenu(),
            //
            enableTabScroll: true,
            activeTab: 0,
            items: [{
                id:'welcome-panel-shop',
                title: shopname,
                iconCls: 'tabs',
	
	   
                constrain     : true,
                scope: this,
                xtype: 'iframepanel',
                defaultSrc: opendomain + '/user/myoverview',
                autoScroll: true,
                closable:false
            }]
        });
    };
    if(role >= 40) {
        MainPanel.superclass.constructor.call(this, {
            region:'center',
	   
            margins:'0 5 5 0',
            deferredRender: true,
            require   : ['@mif', 'ux/crc32'],
            resizeTabs: true,
            minTabWidth: 135,
            tabWidth: 135,
            //plugins: new Ext.ux.TabCloseMenu(),
            //
            enableTabScroll: true,
            activeTab: 0,
            items: []
        });

    }
};

Ext.extend(MainPanel, Ext.TabPanel, {
    stateful: true,
    stateEvents: ['add', 'remove', 'tabchange', 'tabsort'],
    stateId: 'admin_tabpanel_display',
    currentTabs: [],


    initComponent: function() {

        Ext.apply(this, Ext.state.Manager.get(this.stateId));

        this.items.concat(this.getDefaultTabItems());

        this.on('tabsort', this.onTabChange, this);
        this.on('add', this.onTabChange, this);
        this.on('remove', this.onTabChange, this);
        //this.on('afterrender', this.getDefaultTabItems, this);

        this.supr().initComponent.call(this);

        this.setActiveTab(this.activeTab);

    },

    getDefaultTabItems: function() {

        var tabItems = [];

        Ext.each(this.currentTabs, function(tab) {
            this.items.push(new DocPanel({
                id: tab.id,
                cclass : tab.cclass,
                autoLoad: tab.autoLoad,
                iconCls: tab.icon,
                autoScroll: true
            }));
        }, this);

        

        return tabItems;

    },

    onTabChange: function() {
        var tabCount = this.items.getCount();
        
        this.currentTabs = [];

        if(role == 30) {
            var ii = 1;
        }
        if(role >= 40) {
            var ii = 2;
        }

        for (var i=ii, tab; i<tabCount; i++) {
            tab = this.items.get(i);
            // update currentTabs
            this.currentTabs.push({
                'id': tab.id,
                'cclass': tab.cclass,
                'iconCls': tab.iconCls,
                'autoLoad': tab.autoLoad
                });
            
        }
    },

    getState: function() {
        return {
            currentTabs: this.currentTabs,
            activeTab: Ext.isNumber(this.activeTab) ? this.activeTab : this.items.indexOf(this.activeTab)
        };
    },

    initEvents : function(){
        MainPanel.superclass.initEvents.call(this);
    //this.body.on('click', this.onClick, this);
    },

    onClick: function(e, target){
        if(target = e.getTarget('a:not(.exi)', 3)){
            var cls = Ext.fly(target).getAttributeNS('ext', 'cls');
            e.stopEvent();
            if(cls){
                var member = Ext.fly(target).getAttributeNS('ext', 'member');
                this.loadClass(target.href, cls, member);
            }else if(target.className == 'inner-link'){
                this.getActiveTab().scrollToSection(target.href.split('#')[1]);
            }else{
                var id = 'docs-' + cls.replace('&', '').replace(';', '');
                var p = this.add({
                    id:id,
                    title: translation._('app_js_Overview'),
                    iconCls: 'tabs',


                    constrain     : true,
                    scope: this,
                    xtype: 'iframepanel',
                    defaultSrc: 'http://' + target.href,
                    autoScroll: false,
                    closable:false
                })

            //window.open(target.href);
            }
        }else if(target = e.getTarget('.micon', 2)){
            e.stopEvent();
            var tr = Ext.fly(target.parentNode);
            if(tr.hasClass('expandable')){
                tr.toggleClass('expanded');
            }
        }
    },

    loadClass : function(hreff, cls, member, icon){
        var id = 'docs-' + cls.replace('&', '').replace(';', '');
        var tab = this.getComponent(id);
        if(tab){
            this.setActiveTab(tab);
            if(member){
                tab.scrollToMember(member);
            }
        }else{
            var autoLoad = {
                url: hreff,
                loadScripts: true,
                scripts: true
            };
            if(member){
                autoLoad.callback = function(){
                    Ext.getCmp(id).scrollToMember(member);
                }
            }
            var p = this.add(new DocPanel({
                id: id,
                cclass : cls,
                autoLoad: autoLoad,
                iconCls: icon,
                autoScroll: true
            }));
            this.setActiveTab(p);
        }
    }
	
});


Ext.onReady(function(){

    Ext.QuickTips.init();
	
    var mainPanel = new MainPanel();
    if(role >= 40) {
        var hd = new Ext.Panel({
            border: false,
            layout:'anchor',
            region:'north',
            cls: 'docs-header',
            autoHeight: true,
            items: [{
                xtype:'box',
                el:'header',
                border:false,
                height: 80
            },
            new Ext.Toolbar({
                cls:'top-toolbar',
                items:[
                    {
                        text: 'HTML Backend',
                        iconCls: 'icon-wiki',
                        handler: function(e) {
                            window.open('/apps/backend');
                        }
                    }, '-',
                    {
                        text: 'Helpdesk',
                        iconCls: 'icon-dt',
                        handler: function(e) {
                            window.open('https://printshopcreator.ladesk.com/');
                        }
                    },
                '->', translation._('app_js_User') +  ': ' + username + ', ' + firstname + ' ' + lastname
                , '-' , {
	                        
                    tooltip: translation._('app_js_Logout'),
                    iconCls: 'icon-logout',
                    handler: function(e) {
                        Ext.Ajax.request(
                        {
                            waitMsg: 'logout ...',
                            url: '/admin/user/index',
                            params: {
                                logout: "1"
                            },
                            failure:function(response,options){
                                Ext.MessageBox.alert('Warning','Oops...');
                            },
                            success:function(response,options){
                                window.location.replace("/");
                            }
                        }
                        );
                    }
                }]
            })]
        });
    };
    if(role == 30) {
        var hd = new Ext.Panel({
            border: false,
            layout:'anchor',
            region:'north',
            cls: 'docs-header',
            autoHeight: true,
            items: [{
                xtype:'box',
                el:'header',
                border:false,
                height: 80
            },
            new Ext.Toolbar({
                cls:'top-toolbar',
                items:[
                '->', translation._('app_js_User') +  ': ' + username + ', ' + firstname + ' ' + lastname
                , '-' , {
	                        
                    tooltip: translation._('app_js_Logout'),
                    iconCls: 'icon-logout',
                    handler: function(e) {
                        Ext.Ajax.request(
                        {
                            waitMsg: 'logout ...',
                            url: '/admin/user/index',
                            params: {
                                logout: "1"
                            },
                            failure:function(response,options){
                                Ext.MessageBox.alert('Warning','Oops...');
                            },
                            success:function(response,options){
                                window.location.replace("/admin/app");
                            }
                        }
                        );
                    }
                }]
            })]
        });
    }

    
    function change(val){
        if(val == "" || val == 0){
            return '<span style="color:green;"><img src="/images/admin/oxygen/16x16/status/dialog-information.png" /></span>';
        }else if(val == 9){
            return '<span style="color:red;"><img src="/images/admin/oxygen/16x16/status/dialog-error.png" /></span>';
        }
        return val;
    }

    
    var root = new Ext.tree.AsyncTreeNode({
        text: 'Ext JS',
        draggable:false, // disable root node dragging
        id:'source'
    });
   
    var adminnewsstore = new Ext.data.JsonStore({
        fields: [
        {
            name: 'id'
        },

        {
            name: 'message'
        },

        {
            name: 'created'
        },

        {
            name: 'status'
        }
        ]
    });

    var adminnewsgrid = new Ext.grid.GridPanel({
        store: adminnewsstore,
        columns: [
        {
            id:'message',
            header: 'Text',
            dataIndex: 'message'
        },

        {
            id:'created',
            header: 'Datum',
            dataIndex: 'created'
        },

        {
            id:'status',
            header: '',
            dataIndex: 'status',
            renderer: change,
            width: 25
        }
        ],
        title: 'Systemnews',
        stripeRows: true,
        autoExpandColumn: 'message'
    });

    
    var viewport = new Ext.Viewport({
        layout: 'border',
            
        defaults: {
        //activeItem: 0
        },
        items: [hd , {
            region: 'west',
            collapsible: true,
            title: translation._('app_js_Menu'),
            xtype: 'treepanel',
            width: 200,
            autoScroll: true,
            id: 'admin_tree_display',
            split: true,
            plugins:[new Ext.ux.state.TreePanel()],
            stateId: 'admin_tree_display',
            animate:true,
            containerScroll: true,
            loader: new Ext.tree.TreeLoader({
                dataUrl:'/admin/app/tree'
            }),
            root: root,
            
            rootVisible: false,
            listeners: {
                click: function(n, e) {
                    
                    if(n.attributes.cls == 'window' && n.isLeaf()) {
                        e.stopEvent();
                        window.open(n.attributes.href);
                        return;
                    }else{
                        if(n.isLeaf()){
                            e.stopEvent();
                            mainPanel.loadClass(n.attributes.href, n.text, null, n.icon);
                            mainPanel.doLayout();
                        }
                    }
                }
            }
        }, mainPanel]
    });
    
    Ext.Direct.addProvider(
    {
        url: '/admin/news/systemnews/format/json',
        "type":"remoting",
        "actions":{
            "TestAction":[{
                "name":"doEcho",
                "len":1
            }]
        }
    }
    ,
    {
        type:'polling',
        baseParams: {
            id: 0
        },
        interval: 600000,
        url: '/admin/news/systemnews/format/json'
    }
    );
    
    Ext.Direct.on('alert', function(e){
        adminnewsstore.loadData(e.rows);
        if(e.popup) {
            Ext.getCmp('newswindow').expand();
        }
    });


    
    root.expand(false, /*no anim*/ false);
	
    viewport.doLayout();
	
    setTimeout(function(){
        Ext.get('loading').remove();
        Ext.get('loading-mask').fadeOut({
            remove:true
        });
    }, 250);

});
