/*
 * Ext JS Library 2.1
 * Copyright(c) 2006-2008, Ext JS, LLC.
 * licensing@extjs.com
 * 
 * http://extjs.com/license
 */

ContactOverviewGrid = function(limitColumns){

	var url = '/admin/account/all?config=3';
	var objName = 'contact';
	
    function italic(value){
        return '<i>' + value + '</i>';
    }
    

    function change(val){
        if(val > 0){
            return '<span style="color:green;">' + val + '</span>';
        }else if(val < 0){
            return '<span style="color:red;">' + val + '</span>';
        }
        return val;
    }

    function pctChange(val){
        if(val > 0){
            return '<span style="color:green;">' + val + '%</span>';
        }else if(val < 0){
            return '<span style="color:red;">' + val + '%</span>';
        }
        return val;
    }
    
    var store = new Ext.data.Store({
            reader:new Ext.data.JsonReader({
					 id:'id'
					,totalProperty:'totalCount'
					,root:'rows'
					,fields:[
						 {name:'id', type:'int'}
						,{name:'self_firstname', type:'string'}
						,{name:'self_lastname', type:'string'}
						,{name:'self_zip', type:'string'}
						,{name:'self_city', type:'string'}
						,{name:'self_phone', type:'string'}
						,{name:'self_email', type:'string'}
					]
				})
				,proxy:new Ext.data.HttpProxy({url:url})
				,baseParams:{cmd:'getData', objName:objName}
				,sortInfo:{field:'self_firstname', direction:'ASC'}
				,remoteSort:true
        });
    
    ContactOverviewGrid.superclass.constructor.call(this, {
        store: store,
        columns:[{
				 header: translation._('contactoverviewgrid_js_firstname')
				,id:'firstname'
				,dataIndex:'self_firstname'
				,width:195
				,sortable:true
				,editor:new Ext.form.TextField({
					allowBlank:false
				})
			},{
				 header: translation._('contactoverviewgrid_js_lastname')
				,id:'lastname'
				,dataIndex:'self_lastname'
				,width:195
				,sortable:true
				,editor:new Ext.form.TextField({
					allowBlank:false
				})
		}],
        autoExpandColumn: 'firstname',
        height:470,
        width:600,
        bbar: new Ext.PagingToolbar({
	        	store: store,
				displayInfo:true
				,pageSize:20
			}),
        onRender:function() {
			// call parent
			ContactOverviewGrid.superclass.onRender.apply(this, arguments);
	
			// load store
			this.store.load({params:{start:0,limit:20}});

		}
    });


}

Ext.extend(ContactOverviewGrid, Ext.grid.GridPanel);
