/*
 * Ext JS Library 2.1
 * Copyright(c) 2006-2008, Ext JS, LLC.
 * licensing@extjs.com
 * 
 * http://extjs.com/license
 */

OrdersOverviewGrid = function(limitColumns){

	var url = '/admin/account/all?config=1';
	var objName = 'order';
	
	function renderStatus(data, cell, record, rowIndex, columnIndex, store){
         var value=record.get('status')
         //cell.css = "UnStyleCss"; //défini dans style.css
         cell.css = "status" + value; //Color's set here
         return data; //data = text
}
	
    function italic(value){
        return '<i>' + value + '</i>';
    }
    

    function change(val){
        if(val > 0){
            return '<span style="color:green;">' + val + '</span>';
        }else if(val < 0){
            return '<span style="color:red;">' + val + '</span>';
        }
        return val;
    }

    function pctChange(val){
        if(val > 0){
            return '<span style="color:green;">' + val + '%</span>';
        }else if(val < 0){
            return '<span style="color:red;">' + val + '%</span>';
        }
        return val;
    }
    
    var store = new Ext.data.Store({
            reader:new Ext.data.JsonReader({
					 id:'id'
					,totalProperty:'totalCount'
					,root:'rows'
					,fields:[
						 {name:'id', type:'int'}
						,{name:'alias', type:'string'}
						,{name:'account', type:'string'}
                        ,{name:'c/self_firstname', type:'string'}
						,{name:'status', type:'string'}
					]
				})
				,proxy:new Ext.data.HttpProxy({url:url})
				,baseParams:{cmd:'getData', objName:objName}
				,sortInfo:{field:'status', direction:'ASC'}
				,remoteSort:true
        });
    
    OrdersOverviewGrid.superclass.constructor.call(this, {
        store: store,
        columns:[{
				 header: translation._('ordersoverviewgrid_js_alias')
				,id:'alias'
				,dataIndex:'alias'
				,width:50
				,sortable:true
				,editor:new Ext.form.TextField({
					allowBlank:false
				})
			},{
                 header: translation._('orders_gridorder_js_Contact')
                ,id:'contact'
                ,dataIndex:'c/self_firstname'
                ,width:140
                ,sortable:true

            },{
				 header: translation._('ordersoverviewgrid_js_account')
				,id:'account'
				,dataIndex:'account'
				,width:140
				,sortable:true
				,editor:new Ext.form.TextField({
					allowBlank:false
				})
			},{
				 header: translation._('ordersoverviewgrid_js_status')
				,id:'status'
				,dataIndex:'status'
				,width:140
				,sortable:true
				,renderer: renderStatus   // This line is important 
				
			}],
        autoExpandColumn: 'alias',
        height:470,
        width:600,
        bbar: new Ext.PagingToolbar({
	        	store: store,
				displayInfo:true
				,pageSize:20
			}),
		
        onRender:function() {
			// call parent
			OrdersOverviewGrid.superclass.onRender.apply(this, arguments);
			
			// load store
			this.store.load({params:{start:0,limit:20}});
		
		}
    });

	
}

Ext.extend(OrdersOverviewGrid, Ext.grid.GridPanel);
