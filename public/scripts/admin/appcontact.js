Ext.BLANK_IMAGE_URL = '/scripts/extjs/resources/images/default/s.gif';


var translation = new Locale.Gettext();
translation.textdomain('App');

DocPanel = Ext.extend(Ext.Panel, {
    closable: true,
    autoScroll:true,

    initComponent : function(){
        var ps = this.cclass.split('.');
        this.title = ps[ps.length-1];

        DocPanel.superclass.initComponent.call(this);
    },

    scrollToMember : function(member){
        var el = Ext.fly(this.cclass + '-' + member);
        if(el){
            var top = (el.getOffsetsTo(this.body)[1]) + this.body.dom.scrollTop;
            this.body.scrollTo('top', top-25, {duration:.75, callback: this.hlMember.createDelegate(this, [member])});
        }
    },

	scrollToSection : function(id){
		var el = Ext.getDom(id);
		if(el){
			var top = (Ext.fly(el).getOffsetsTo(this.body)[1]) + this.body.dom.scrollTop;
			this.body.scrollTo('top', top-25, {duration:.5, callback: function(){
                Ext.fly(el).next('h2').pause(.2).highlight('#8DB2E3', {attr:'color'});
            }});
        }
	},

    hlMember : function(member){
        var el = Ext.fly(this.cclass + '-' + member);
        if(el){
            el.up('tr').highlight('#cadaf9');
        }
    }
});

MainPanel = function(){
    MainPanel.superclass.constructor.call(this, {
        id:'doc-body',
        region:'center',
        margins:'0 5 5 0',
        resizeTabs: true,
        minTabWidth: 135,
        tabWidth: 135,
        plugins: new Ext.ux.TabCloseMenu(),
        enableTabScroll: true,
        activeTab: 0,

        items: {
            id: 'docs-' + translation._('appcontacts_js_MyOrders').replace('&', '').replace(';', '') ,
            title: translation._('appcontacts_js_MyOrders'),
            autoLoad: {url: '/admin/myorders/all', callback: this.initSearch, scope: this, loadScripts: true, scripts: true},
            iconCls:'icon-docs',
            autoScroll: true			
        }
    });
};

Ext.extend(MainPanel, Ext.TabPanel, {
	
    initEvents : function(){
        MainPanel.superclass.initEvents.call(this);
        this.body.on('click', this.onClick, this);
    },

    onClick: function(e, target){
        if(target = e.getTarget('a:not(.exi)', 3)){
            var cls = Ext.fly(target).getAttributeNS('ext', 'cls');
            e.stopEvent();
            if(cls){
                var member = Ext.fly(target).getAttributeNS('ext', 'member');
                this.loadClass(target.href, cls, member);
            }else if(target.className == 'inner-link'){
                this.getActiveTab().scrollToSection(target.href.split('#')[1]);
            }else{
                window.open(target.href);
            }
        }else if(target = e.getTarget('.micon', 2)){
            e.stopEvent();
            var tr = Ext.fly(target.parentNode);
            if(tr.hasClass('expandable')){
                tr.toggleClass('expanded');
            }
        }
    },

    loadClass : function(hreff, cls, member, icon){
        var id = 'docs-' + cls.replace('&', '').replace(';', '');
        var tab = this.getComponent(id);
        if(tab){
            this.setActiveTab(tab);
            if(member){
                tab.scrollToMember(member);
            }
        }else{
            var autoLoad = {url: hreff, loadScripts: true, scripts: true};
            if(member){
                autoLoad.callback = function(){
                    Ext.getCmp(id).scrollToMember(member);
                }
            }
            var p = this.add(new DocPanel({
                id: id,
                cclass : cls,
                autoLoad: autoLoad,
                iconCls: icon
            }));
            this.setActiveTab(p);
        }
    }
	
});


Ext.onReady(function(){

    Ext.QuickTips.init();
	
    var mainPanel = new MainPanel();
    
    var hd = new Ext.Panel({
        border: false,
        layout:'anchor',
        region:'north',
        cls: 'docs-header',
        autoHeight: true,
        items: [{
            xtype:'box',
            el:'header',
            border:false,
            anchor: 'none -25'
        },
         new Ext.Toolbar({
            cls:'top-toolbar',
            items:[ {
                        text: 'Wiki',
                        iconCls: 'icon-wiki',
                        handler: function(e) {
                            window.open('http://wiki.printshopcreator.de');
                        }
                    }, '-',
                    {
                        text: 'Forum',
                        iconCls: 'icon-forum',
                        handler: function(e) {
                            window.open('http://forum.printshopcreator.de');
                        }
                    }, '-',
                    {
                        text: 'Tiketsystem',
                        iconCls: 'icon-tiket',
                        handler: function(e) {
                            window.open('http://ticket.printshopcreator.de');
                        }
                    },
			'->', {
                tooltip: translation._('app_js_Logout'),
                iconCls: 'icon-logout',
                handler: function(e) {
	                 Ext.Ajax.request(
		                {
		                    waitMsg: 'logout ...',
		                    url: '/admin/user/index',
		                    params: {
		                        logout: "1"
		                    },
		                    failure:function(response,options){
		                        Ext.MessageBox.alert('Warning','Oops...');
		                    },
		                    success:function(response,options){
		                            window.location.replace("/admin/app");
		                    }
		                 }
		            );
	            }
            }]
        })]
    });
    
    var root = new Ext.tree.AsyncTreeNode({
                text: 'Ext JS', 
                draggable:false, // disable root node dragging
                id:'source'
            });
   
    
    

    var viewport = new Ext.Viewport({
	    layout: 'border',
	    defaults: {
	        //activeItem: 0
	    },
	    items: [hd , {
	        region: 'west',
	        collapsible: true,
	        title: translation._('appcontacts_js_Menu'),
	        xtype: 'treepanel',
	        width: 200,
	        autoScroll: true,
	        split: true,
	        animate:true, 
            enableDD:true,
            containerScroll: true,
	        loader: new Ext.tree.TreeLoader({dataUrl:'/admin/app/tree'}),
	        root: root,
            
	        rootVisible: false,
	        listeners: {
	            click: function(n, e) {
	                 if(n.isLeaf()){
			            e.stopEvent();
			            mainPanel.loadClass(n.attributes.href, n.text, null, n.icon);
			            mainPanel.doLayout(); 
			         }
	            }
	        }
	    }, mainPanel]
	});
    
	root.expand(true, /*no anim*/ true);
	
    viewport.doLayout();
	
    setTimeout(function(){
        Ext.get('loading').remove();
        Ext.get('loading-mask').fadeOut({remove:true});
    }, 250);
});
