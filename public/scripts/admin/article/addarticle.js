Ext.onReady(function(){

    var wizard = new Ext.ux.Wiz({

        title : 'Create an Article',
        renderTo: document.body,
        headerConfig : {
            title : 'Create an Article'
        },

        cardPanelConfig : {
            defaults : {
                baseCls    : 'x-small-editor',
                bodyStyle  : 'padding:40px 15px 5px 120px;background-color:#F6F6F6;',
                border     : false
            }
        },

        cards : [

            // first card with welcome message
            new Ext.ux.Wiz.Card({
                title : 'Welcome',
                items : [{
                    border    : false,
                    bodyStyle : 'background:none;',
                    html      : 'Create your own Article, '+
                                'but before we can start fill out the Wizard.<br/><br/>'
                }]
            }),

            // second card with input fields last/firstname
            new Ext.ux.Wiz.Card({
                title        : 'Your name',
                monitorValid : true,
                defaults     : {
                    labelStyle : 'font-size:11px'
                },
                items : [{
                        border    : false,
                        bodyStyle : 'background:none;padding-bottom:30px;',
                        html      : 'Please enter the Articlename.'
                    },
                    new Ext.form.TextField({
                        name       : 'title',
                        fieldLabel : 'title',
                        allowBlank : false,
                        validator  : function(v){
                            var t = /^[a-zA-Z_\- ]+$/;
                            return t.test(v);
                        }
                    })

                ]
            }),

            // third card with input field email-address
            new Ext.ux.Wiz.Card({
                title        : 'Your email-address',
                monitorValid : true,
                defaults     : {
                    labelStyle : 'font-size:11px'
                },
                items : [{
                        border    : false,
                        bodyStyle : 'background:none;padding-bottom:30px;',
                        html      : ' Please enter your email-address.'
                    },
                    new Ext.form.TextField({
                        name       : 'email',
                        fieldLabel : 'Email-Address',
                        allowBlank : false,
                        vtype      : 'email'
                    })
                ]
            }),

            // fourth card with finish-message
            new Ext.ux.Wiz.Card({
                title        : 'Finished!',
                monitorValid : true,
                items : [{
                    border    : false,
                    bodyStyle : 'background:none;',
                    html      : 'Thank you for testing this wizard. Your data has been collected '+
                                'and can be accessed via a call to <pre><code>this.getWizardData</code></pre>'+
                                'When you click on the "finish"-button, the "finish"-event will be fired.<br/>'+
                                'If no attached listener for this event returns "false", this dialog will be '+
                                'closed. <br />'
                }]
            })


        ]
    });

    wizard.show();
});