Ext.onReady(function() {


    Ext.QuickTips.init();

    Ext.form.TextAreaXML = Ext.extend(Ext.form.TextField,  {

        onRender : function(ct, position){
            
            Ext.form.TextAreaXML.superclass.onRender.call(this, ct, position);


            var editor = CodeMirror.fromTextArea('a3_xml', {
                height: "350px",
                parserfile: "/scripts/vendor/codemirror/js/parsexml.js",
                stylesheet: "/scripts/vendor/codemirror/css/xmlcolors.css",
                path: "/scripts/vendor/codemirror/js/",
                continuousScanning: 500,
                lineNumbers: true,
                textWrapping: false
              });
        }

    });

    Ext.reg('textareaxml', Ext.form.TextAreaXML);

    var translation = new Locale.Gettext();
    translation.textdomain('App');

    var win = new Ext.Window({
         id:'metaform-win'
        ,layout:'fit'
        ,width: 800
        ,scope:this
        ,height: 600
        ,layout: 'form'
        ,title: translation._('article_edit_js_Edit Product')
        ,items:[ {
             xtype:'metaform'
             ,id:"articleform"
            ,url:'/admin/article/edit?config=3&sid='+ sid + '&type=' + type + '&uid=' + uid
            ,buttons:[{
                  text: translation._('article_edit_js_Save')
                ,iconCls: 'action_applyChanges'
                ,listeners:{
                   click:{scope:this, buffer:200, fn:function(btn) {
                       Ext.getCmp('metaform-win').items.get(0).getForm().submit(
                            {waitMsg: translation._('article_edit_js_Saving')
                            ,url: '/admin/article/edit?config=3&sid='+ sid + '&type=' + type + '&uid=' + uid + '&save=1'
                            ,success: function(form, action){
                                  if(action.result.isNew) {
                                    window.location = '/admin/article/edit?sid='+ sid + '&uid=' + action.result.uid + '&type=' +type;
                                  }
                            }
                        });
                        opener.Ext.getCmp('PrintShopCreatorAdminArticleLIST').store.reload();

                }}}
            },{
                  text: translation._('article_edit_js_Save and Close')
                ,iconCls: 'action_applyChangesClose'
                ,listeners:{
                   click:{scope:this, buffer:200, fn:function(btn) {
	                   Ext.getCmp('metaform-win').items.get(0).getForm().submit(
                               {waitMsg: translation._('article_edit_js_Saving')
                               ,url: '/admin/article/edit?config=3&sid='+ sid + '&type=' + type + '&uid=' + uid + '&save=1'
                               ,success: function(form, action){
                                                      window.close();
                                            }
                           });
                           opener.Ext.getCmp('PrintShopCreatorAdminArticleLIST').store.reload();
                }}}
            },{
                text: translation._('article_edit_js_Close'),
                listeners:{
                   click:{scope:this, buffer:200, fn:function(btn) {
                        window.close();
                }}},
                iconCls: 'action_cancel'
            }]
        }]
    });

    win.show();

    Ext.get('file').on('change', function() {
      document.getElementById('uuid_upload').value = uid;
      document.getElementById('file_upload_form').submit();
    })

});
