Ext.ns('PrintShopCreator.Admin.Article');

function renderWebsite(value, metaData, record, rowIndex, colIndex, store) {
	return '<a href="' + value + '" target="_blank">' + translation._('article_index_js_Productlink') + '</a>';
};

PrintShopCreator.Admin.Article.LIST = Ext.extend(Ext.grid.GridPanel, {
	 
	border:false
	,layout: 'fit'
	,stateful:true
	,url:'/admin/article/all?config=1&sid=' + sid
	,objName:'article'
	,idName:'id'
	,stripeRows: true
	,frame: false
	,autoHeight: true
	,autoWidth: true
        ,id: 'articlegrid'
	,initComponent:function() {
		
		
		// create row actions
		this.rowActions = new Ext.ux.grid.RowActions({
			 actions:[{
				 iconCls:'icon-minus'
				,qtip: translation._('article_index_js_Delete Products')
			},{
				 iconCls:'icon-copy'
				,qtip: translation._('article_index_js_Copy Products')
			},{
				 iconCls:'icon-edit-record'
				,qtip: translation._('article_index_js_Edit Product')
			}]
		});
		this.rowActions.on('action', this.onRowAction, this);
		
		var filters = new Ext.ux.grid.GridFilters({
		  filters:[
			{type: 'string',  dataIndex: 'title'},
			{type: 'date',  dataIndex: 'created'},
			{type: 'date',  dataIndex: 'updated'},
			{type: 'boolean',  dataIndex: 'enable'}
		]});
		
		Ext.apply(this, {
			// {{{
			store:new Ext.data.Store({
				reader:new Ext.data.JsonReader({
					 id:'id'
					,totalProperty:'totalCount'
					,root:'rows'
					,fields:[
						 {name:'id', type:'int'}
						,{name:'uuid', type:'string'}
						,{name:'title', type:'string'}
						,{name:'typ', type:'string'}
						,{name:'link', type:'string'}
                                                ,{name:'used', type:'string'}
						,{name:'created', type:'date', dateFormat: 'Y-m-d H:i:s'}
						,{name:'updated', type:'date', dateFormat: 'Y-m-d H:i:s'}
						,{name:'typName', type:'string'}
						,{name:'enable', type:'boolean'}
					]
				})
				,proxy:new Ext.data.HttpProxy({url:this.url})
				,sortInfo:{field:'title', direction:'ASC'}
				,remoteSort:true
                                ,params:{start: 0, limit: 30}
			})
			// }}}
			// {{{
			,columns:[{
				 header: 'Id'
				,id:'id'
				,dataIndex:'id'
				,width:20
				,sortable:true
				
			},{
				 header: translation._('article_index_js_Productname')
				,id:'title'
				,dataIndex:'title'
				,width:160
				,sortable:true
			},{
				 header: translation._('article_index_js_Used')
				,id:'used'
				,dataIndex:'used'
				,width:40
				,sortable:true
			},{
				 header: translation._('article_index_js_Producttyp')
				,id:'typ'
				,dataIndex:'typName'
				,width:100
				,sortable:true
			},{
				 header: translation._('article_index_js_Created')
				,id:'created'
				,dataIndex:'created'
				,width:100
				,sortable:true
				,renderer: Ext.util.Format.dateRenderer('d.m.Y H:i')
			},{
				 header: translation._('article_index_js_Updated')
				,id:'updated'
				,dataIndex:'updated'
				,width:100
				,sortable:true
				,renderer: Ext.util.Format.dateRenderer('d.m.Y H:i')
			},{
				 header: translation._('article_index_js_Enable')
				,id:'enable'
				,dataIndex:'enable'
				,width:100
				,sortable:true
				,renderer: PrintShopCreator.Base.Common.renderBool
			},{
				 header: translation._('article_index_js_Productlink')
				,id:'link'
				,dataIndex:'link'
				,width:100
				,sortable:true
				,renderer: renderWebsite
			}, this.rowActions]
			// }}}
			,plugins:[this.rowActions, filters]
			,viewConfig:{forceFit:true}

		}); // eo apply
		
		if(role<40) {
			this.tbar = [];
		}
		
		this.bbar = new Ext.PagingToolbar({
			 store:this.store
			,pageSize:30
                        ,displayInfo: true

			,plugins: [filters,new Ext.ux.ProgressBarPager()]
		});

		// call parent
		PrintShopCreator.Admin.Article.LIST.superclass.initComponent.apply(this, arguments);
	} // eo function initComponent
	// {{{
	,onRender:function() {
		// call parent
		PrintShopCreator.Admin.Article.LIST.superclass.onRender.apply(this, arguments);

		// load store
		this.store.load({params:{start:0,limit:30}});

	} // eo function onRender
	// }}}

	,addRecord:function() {
		var store = this.store;
		if(store.recordType) {
			var rec = new store.recordType({newRecord:true});
			
			return rec;
		}
		return false;
	} // eo function addRecord

	,onRowAction:function(grid, record, action, row, col) {
		switch(action) {
			
			case 'icon-minus':
				this.deleteRecord(record);
			break;
			case 'icon-copy':
				this.copyRecord(record);
			break;
			case 'icon-edit-record':
			
			    PrintShopCreator.Base.Common.openWindow('articleWindow', '/admin/article/edit?sid='+ sid + '&uid=' + record.id + '&type=' + record.data.typ, 800, 600);
				
			break;
		}
	} // eo onRowAction

	

	,showError:function(msg, title) {
		Ext.Msg.show({
			 title:title || 'Error'
			,msg:Ext.util.Format.ellipsis(msg, 2000)
			,icon:Ext.Msg.ERROR
			,buttons:Ext.Msg.OK
			,minWidth:1200 > String(msg).length ? 360 : 600
		});
	} // eo function showError

	,deleteRecord:function(record) {
		Ext.Msg.show({
			 title: translation._('msg_delete')
			,msg: translation._('msg_delete_confirm_beforetext') + ':<br><b>' + record.get('title') + '</b><br>' + translation._('msg_delete_confirm_aftertext')
			,icon:Ext.Msg.QUESTION
			,buttons:Ext.Msg.YESNO
			,scope:this
			,fn:function(response) {
				if('yes' == response) {
					 try {
					    var req = new XMLHttpRequest;
					
					    req.open('POST', '/admin/article/edit?uuid=' + record.data.uuid, false);
					    req.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
					    req.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
					    req.send('&delete=1');
					    if (req.status == 200 || req.status == 304 || req.status == 0 || req.status == null) {
                            this.store.reload();
                            var message = Ext.util.JSON.decode(req.responseText);
                            if(message.error) {
                            	Ext.MessageBox.show({
                                    title: 'Fehler',
                                    msg: message.message,
                                    buttons: Ext.MessageBox.OK,
                                    animEl: 'mb9',
                                    icon: Ext.MessageBox.ERROR
                                });

                            }
					    }
					  } catch (e) {
					    return '';
					  }
				}else{
				    return;
				}
//				console.info('Deleting record');
			}
		});
	} // eo function deleteRecord
	,copyRecord:function(record) {

                combo = new Ext.form.ComboBox({
                                        fieldLabel: 'Shop',
                                        hiddenName:'shopsel',
                                        store: new Ext.data.JsonStore({
                                            url: '/service/?type=get&mode=system&service=shops',
                                            root: 'items',
                                            fields: ['name', 'label']
                                        }),
                                        valueField:'name',
                                        displayField:'label',
                                        mode: 'remote',
                                        triggerAction: 'all',
                                        forceSelection: true,
                                        width:190,
                                        emptyText:translation._('Select a Shop')
                            });

		var tabs = new Ext.FormPanel({
                    labelWidth: 120,
                    width: 600,
                    frame: true,
                    id: 'copyshopform',
                    items: [{
                            xtype: 'fieldset',
                            title: translation._('Mode'),
                            items: [{
                                xtype: 'radiogroup',
                                fieldLabel: translation._('Mode'),
                                height: 90,
                                id: 'modes',
                                columns: 1,
                                    items: [
                                    { boxLabel: translation._('ThisShop'), name: 'radBreakType', inputValue: 1, checked: true  },
                                    { boxLabel: translation._('OtherShop'), name: 'radBreakType', inputValue: 2}
                                    ]
                            }]
                    },{
                            xtype: 'fieldset',
                            border: true,
                            title: translation._('Settings for Other Shop'),
                            items: [combo]
                    }]
                });

                var win = new Ext.Window({
                    title: translation._('Copy Article'),
                    closable: true,
                    id: 'copyshop',
                    modal: true,
                    closeAction: 'hide',
                    width: 500,
                    height: 350,
                    plain: true,
                    layout: 'fit',
                    border: false,
                    items: tabs,
                    buttons: [{
                      text: translation._('Execute'),
                      iconCls: 'action_applyChanges',
                      id: 'insert-btn',
                      scope: this,
                      handler: function() {

                        var radio = Ext.getCmp('modes');
                        
                        if(radio.items.items[0].getGroupValue() == 1) {
                            try {
                                var req = new XMLHttpRequest;

                                req.open('POST', '/admin/article/edit?uuid=' + record.data.uuid, false);
                                req.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
                                req.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
                                req.send('&copy=1');
                                if (req.status == 200 || req.status == 304 || req.status == 0 || req.status == null) {
                                        this.store.load({params:{start:0,limit:20}});
                                        win.close();
                                        return Ext.util.JSON.decode(req.responseText);
                                }
                          } catch (e) {
                                win.close();
                          }

                        }
                        if(radio.items.items[0].getGroupValue() == 2) {
                             try {
                                var req = new XMLHttpRequest;

                                req.open('POST', '/admin/article/edit?uuid=' + record.data.uuid, false);
                                req.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
                                req.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
                                req.send('&copymove=1&target=' + combo.getValue());
                                if (req.status == 200 || req.status == 304 || req.status == 0 || req.status == null) {
                                        this.store.load({params:{start:0,limit:20}});
                                        win.close();
                                }
                                win.hide();
                          } catch (e) {
                                win.close();
                          }
                        }
                      }
                    }, {
                      text: translation._('Close'),
                      iconCls: 'action_cancel',
                      handler: function() {
                        win.close();
                      }
                    }]
                  });
                  win.show();
	} // eo function deleteRecord
}); // eo extend

// register xtype
Ext.reg('PrintShopCreatorAdminArticleLIST', PrintShopCreator.Admin.Article.LIST);
Ext.QuickTips.init();
	
	var articleList = new Ext.FormPanel({
		               
        renderTo: 'articlelist',
        closable: false,
        resizable: false,
        layout: 'fit',
        border: false,
        items:[{xtype:'PrintShopCreatorAdminArticleLIST', id:'PrintShopCreatorAdminArticleLIST'}]
});
articleList.show();
