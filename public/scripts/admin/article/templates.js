Ext.ns('PrintShopCreator.Admin.Article');

function renderWebsite(value, metaData, record, rowIndex, colIndex, store) {
	return '<a href="' + value + '" target="_blank">' + translation._('article_index_js_Productlink') + '</a>';
};

PrintShopCreator.Admin.Article.TemplateLIST = Ext.extend(Ext.grid.GridPanel, {
	 
	border:false
	,layout: 'fit'
	,stateful:true
	,url:'/admin/article/fetcharticletemplates/format/json'
	,objName:'article'
	,idName:'id'
	,frame: false
	,autoHeight: true
	,autoWidth: true
        ,id: 'articlegrid'
	,initComponent:function() {
		
		
		// create row actions
                if(role == 50) {
                    this.rowActions = new Ext.ux.grid.RowActions({
                             actions:[{
                                     iconCls:'icon-copy'
                                    ,qtip: translation._('article_index_js_Copy Products')
                            },{
                                     iconCls:'icon-edit-record'
                                    ,qtip: translation._('article_index_js_Edit Product')
                            }]
                    });
                }else{
                    this.rowActions = new Ext.ux.grid.RowActions({
                             actions:[{
                                     iconCls:'icon-copy'
                                    ,qtip: translation._('article_index_js_Copy Products')
                            }]
                    });
                }
		this.rowActions.on('action', this.onRowAction, this);
		
		var filters = new Ext.ux.grid.GridFilters({
		  filters:[
			{type: 'string',  dataIndex: 'title'},
			{type: 'date',  dataIndex: 'created'},
			{type: 'date',  dataIndex: 'updated'},
			{
				type: 'list',
				dataIndex: 'typName',
				store : new Ext.data.Store({
					proxy:  new Ext.data.HttpProxy({ url: '/admin/article/all?config=2', method: 'POST' }),
					reader: new Ext.data.JsonReader({ totalProperty: 'count', root: 'rows' },
					[
						{ name: 'id', type: 'string', mapping: 'id' },
						{ name: 'label', type: 'string', mapping: 'title' }
					])
					,labelField: 'id'
				})
				,phpMode: true
				,labelField: 'label'
			}

		]});
		
		Ext.apply(this, {
			// {{{
			store:new Ext.data.Store({
				reader:new Ext.data.JsonReader({
					 id:'id'
					,totalProperty:'totalCount'
					,root:'rows'
					,fields:[
						 {name:'id', type:'int'}
						,{name:'uuid', type:'string'}
						,{name:'title', type:'string'}
						,{name:'typ', type:'string'}
						,{name:'link', type:'string'}
						,{name:'created', type:'date', dateFormat: 'Y-m-d H:i:s'}
						,{name:'updated', type:'date', dateFormat: 'Y-m-d H:i:s'}
						,{name:'typName', type:'string'}
					]
				})
				,proxy:new Ext.data.HttpProxy({url:this.url})
				,sortInfo:{field:'title', direction:'ASC'}
				,remoteSort:true
                                ,params:{start: 0, limit: 20}
			})
			// }}}
			// {{{
			,columns:[{
				 header: 'Id'
				,id:'id'
				,dataIndex:'id'
				,width:20
				,sortable:true
				
			},{
				 header: translation._('article_index_js_Productname')
				,id:'title'
				,dataIndex:'title'
				,width:160
				,sortable:true
			},{
				 header: translation._('article_index_js_Producttyp')
				,id:'typ'
				,dataIndex:'typName'
				,width:100
				,sortable:true
			},{
				 header: translation._('article_index_js_Created')
				,id:'created'
				,dataIndex:'created'
				,width:100
				,sortable:true
				,renderer: Ext.util.Format.dateRenderer('d.m.Y H:i')
			},{
				 header: translation._('article_index_js_Updated')
				,id:'updated'
				,dataIndex:'updated'
				,width:100
				,sortable:true
				,renderer: Ext.util.Format.dateRenderer('d.m.Y H:i')
			},{
				 header: translation._('article_index_js_Productlink')
				,id:'link'
				,dataIndex:'link'
				,width:100
				,sortable:true
				,renderer: renderWebsite
			}, this.rowActions]
			// }}}
			,plugins:[this.rowActions, filters]
			,viewConfig:{forceFit:true}
			
		}); // eo apply
		
		if(role<40) {
			this.tbar = [];
		}
		
		this.bbar = new Ext.PagingToolbar({
			 store:this.store
			,pageSize:20
                        ,displayInfo: true

			,plugins: [filters,new Ext.ux.ProgressBarPager()]
		});

		// call parent
		PrintShopCreator.Admin.Article.TemplateLIST.superclass.initComponent.apply(this, arguments);
	} // eo function initComponent
	// {{{
	,onRender:function() {
		// call parent
		PrintShopCreator.Admin.Article.TemplateLIST.superclass.onRender.apply(this, arguments);

		// load store
		this.store.load({params:{start:0,limit:20}});

	} // eo function onRender
	// }}}

	,addRecord:function() {
		var store = this.store;
		if(store.recordType) {
			var rec = new store.recordType({newRecord:true});
			
			return rec;
		}
		return false;
	} // eo function addRecord

	,onRowAction:function(grid, record, action, row, col) {
		switch(action) {
			
			case 'icon-copy':
				this.copyRecord(record);
			break;
			case 'icon-edit-record':
			
			    PrintShopCreator.Base.Common.openWindow('articleWindow', '/admin/article/edit?uid=' + record.id + '&type=' + record.data.typ, 800, 600);
				
			break;
		}
	} // eo onRowAction

	

	,showError:function(msg, title) {
		Ext.Msg.show({
			 title:title || 'Error'
			,msg:Ext.util.Format.ellipsis(msg, 2000)
			,icon:Ext.Msg.ERROR
			,buttons:Ext.Msg.OK
			,minWidth:1200 > String(msg).length ? 360 : 600
		});
	} // eo function showError

	,copyRecord:function(record) {

                combo = new Ext.form.ComboBox({
                                        fieldLabel: 'Shop',
                                        hiddenName:'shopsel',
                                        store: new Ext.data.JsonStore({
                                            url: '/service/?type=get&mode=system&service=shops',
                                            root: 'items',
                                            fields: ['name', 'label']
                                        }),
                                        valueField:'name',
                                        displayField:'label',
                                        mode: 'remote',
                                        triggerAction: 'all',
                                        forceSelection: true,
                                        width:190,
                                        emptyText:translation._('Select a Shop')
                            });

		var tabs = new Ext.FormPanel({
                    labelWidth: 120,
                    width: 600,
                    frame: true,
                    id: 'copyshopform',
                    items: [{
                            xtype: 'fieldset',
                            title: translation._('Mode'),
                            items: [{
                                xtype: 'radiogroup',
                                fieldLabel: translation._('Mode'),
                                height: 90,
                                id: 'modes',
                                columns: 1,
                                    items: [
                                    { boxLabel: translation._('OtherShop'), name: 'radBreakType', inputValue: 2, checked: true  }
                                    ]
                            }]
                    },{
                            xtype: 'fieldset',
                            border: true,
                            title: translation._('Settings for Other Shop'),
                            items: [combo]
                    }]
                });

                var win = new Ext.Window({
                    title: translation._('Copy Article'),
                    closable: true,
                    id: 'copyshop',
                    modal: true,
                    closeAction: 'hide',
                    width: 500,
                    height: 350,
                    plain: true,
                    layout: 'fit',
                    border: false,
                    items: tabs,
                    buttons: [{
                      text: translation._('Execute'),
                      iconCls: 'action_applyChanges',
                      id: 'insert-btn',
                      scope: this,
                      handler: function() {

                        var radio = Ext.getCmp('modes');
                        
                        if(radio.items.items[0].getGroupValue() == 1) {
                            try {
                                var req = new XMLHttpRequest;

                                req.open('POST', '/admin/article/edit?uuid=' + record.data.uuid, false);
                                req.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
                                req.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
                                req.send('&copy=1');
                                if (req.status == 200 || req.status == 304 || req.status == 0 || req.status == null) {
                                        this.store.reload();
                                        win.close();
                                        return Ext.util.JSON.decode(req.responseText);
                                }
                          } catch (e) {
                                win.close();
                          }

                        }
                        if(radio.items.items[0].getGroupValue() == 2) {
                             try {
                                var req = new XMLHttpRequest;

                                req.open('POST', '/admin/article/edit?uuid=' + record.data.uuid, false);
                                req.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
                                req.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
                                req.send('&copymove=1&target=' + combo.getValue());
                                if (req.status == 200 || req.status == 304 || req.status == 0 || req.status == null) {
                                        this.store.reload();
                                        win.close();
                                }
                                win.hide();
                          } catch (e) {
                                win.close();
                          }
                        }
                      }
                    }, {
                      text: translation._('Close'),
                      iconCls: 'action_cancel',
                      handler: function() {
                        win.close();
                      }
                    }]
                  });
                  win.show();
	} // eo function deleteRecord
}); // eo extend

// register xtype
Ext.reg('PrintShopCreatorAdminArticleTemplateLIST', PrintShopCreator.Admin.Article.TemplateLIST);
Ext.QuickTips.init();
	
	var articleTemplateList = new Ext.FormPanel({
		               
        renderTo: 'articlelisttemplates',
        closable: false,
        resizable: false,
        layout: 'fit',
        border: false,
        items:[{xtype:'PrintShopCreatorAdminArticleTemplateLIST', id:'PrintShopCreatorAdminArticleTemplateLIST'}]
});
articleTemplateList.show();
