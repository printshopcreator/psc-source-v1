Ext.ns('PrintShopCreator.Admin.Articlegroup');

PrintShopCreator.Admin.Articlegroup.LIST = Ext.extend(Ext.grid.GridPanel, {
	 
	border:false
	,layout: 'fit'
	,stateful:true
	,url:'/admin/articlegroup/all?config=1&sid=' + sid
	,objName:'articlegroup'
	,idName:'id'
	,frame: false
	,autoHeight: true
	,autoWidth: true
	,id: 'articlegroupgrid'
	,initComponent:function() {
		
		
		// create row actions
		this.rowActions = new Ext.ux.grid.RowActions({
			 actions:[{
				 iconCls:'icon-minus'
				,qtip: translation._('articlegroup_all_js_Delete Productgroup')
			},{
				 iconCls:'icon-edit-record'
				,qtip: translation._('articlegroup_all_js_Edit Productgroup')
			}]
		});
		this.rowActions.on('action', this.onRowAction, this);

                var filters = new Ext.ux.grid.GridFilters({
		  filters:[
			{type: 'string',  dataIndex: 'title'}

		]});

		Ext.apply(this, {
			// {{{
			store:new Ext.data.Store({
				reader:new Ext.data.JsonReader({
					 id:'id'
					,totalProperty:'totalCount'
					,root:'rows'
					,fields:[
						 {name:'id', type:'int'}
						,{name:'title', type:'string'}
						,{name:'parent', type:'string'}
						,{name:'enable', type:'boolean'}
					]
				})
				,proxy:new Ext.data.HttpProxy({url:this.url})
				,sortInfo:{field:'title', direction:'ASC'}
				,remoteSort:true
			})
			// }}}
			// {{{
			,columns:[{
				 header: 'Id'
				,id:'id'
				,dataIndex:'id'
				,width:20
				,sortable:true

			},{
				 header: translation._('articlegroup_all_js_Productgroupname')
				,id:'title'
				,dataIndex:'title'
				,width:160
				,sortable:true
				
			},{
				 header: translation._('articlegroup_all_js_Parent')
				,id:'parent'
				,dataIndex:'parent'
				,width:100
				,sortable:true
				
			},{
				 header: translation._('articlegroup_all_js_Enable')
				,id:'enable'
				,dataIndex:'enable'
				,width:160
				,sortable:true
				,renderer: PrintShopCreator.Base.Common.renderBool
			}, this.rowActions]
			// }}}
			,plugins:[this.rowActions, filters]
			,viewConfig:{forceFit:true}
			,tbar:[{
				 text: translation._('articlegroup_all_js_Add Productgroup')
				,iconCls:'icon-form-add'
				,handler: function(e) {
                		PrintShopCreator.Base.Common.openWindow('articlegroupWindow', '/admin/articlegroup/edit?sid='+ sid, 800, 600);
                }
			}]
		}); // eo apply

		this.bbar = new Ext.PagingToolbar({
			 store:this.store
			,displayInfo: true
			,plugins: [filters,new Ext.ux.ProgressBarPager()]
			,pageSize:20
		});

		// call parent
		PrintShopCreator.Admin.Articlegroup.LIST.superclass.initComponent.apply(this, arguments);
	} // eo function initComponent
	// {{{
	,onRender:function() {
		// call parent
		PrintShopCreator.Admin.Articlegroup.LIST.superclass.onRender.apply(this, arguments);

		// load store
		this.store.load({params:{start:0,limit:20}});

	} // eo function onRender
	// }}}

	

	,onRowAction:function(grid, record, action, row, col) {
		switch(action) {
			
			case 'icon-minus':
				this.deleteRecord(record);
			break;

			case 'icon-edit-record':
			
			    PrintShopCreator.Base.Common.openWindow('articleWindow', '/admin/articlegroup/edit?sid='+ sid + '&uid=' + record.id, 800, 600);
				
			break;
		}
	} // eo onRowAction

	

	
	,showError:function(msg, title) {
		Ext.Msg.show({
			 title:title || 'Error'
			,msg:Ext.util.Format.ellipsis(msg, 2000)
			,icon:Ext.Msg.ERROR
			,buttons:Ext.Msg.OK
			,minWidth:1200 > String(msg).length ? 360 : 600
		});
	} // eo function showError

	,deleteRecord:function(record) {
		Ext.Msg.show({
			 title: translation._('msg_delete')
			,msg: translation._('msg_delete_confirm_beforetext') + ':<br><b>' + record.get('title') + '</b><br>' + translation._('msg_delete_confirm_aftertext')
			,icon:Ext.Msg.QUESTION
			,buttons:Ext.Msg.YESNO
			,scope:this
			,fn:function(response) {
				if('yes' == response) {
					 try {
					    var req = new XMLHttpRequest;
					
					    req.open('POST', '/admin/articlegroup/edit?uid=' + record.id, false);
					    req.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
					    req.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
					    req.send('&delete=1');
					    if (req.status == 200 || req.status == 304 || req.status == 0 || req.status == null) {
                            this.store.load({params:{start:0,limit:20}});
					      return Ext.util.JSON.decode(req.responseText);
					    }
					  } catch (e) {
					    return '';
					  }
				}else{
				    return;
				}
//				console.info('Deleting record');
			}
		});
	} // eo function deleteRecord

}); // eo extend

// register xtype
Ext.reg('PrintShopCreatorAdminArticlegroupLIST', PrintShopCreator.Admin.Articlegroup.LIST);
Ext.QuickTips.init();
	
	var articlegroupList = new Ext.FormPanel({
		               
        renderTo: 'articlegrouplist',
        closable: false,
        resizable: false,
        layout: 'fit',
        border: false,
        items:[{xtype:'PrintShopCreatorAdminArticlegroupLIST', id:'PrintShopCreatorAdminArticlegroupLIST'}]
});
articlegroupList.show();