/*!
 * Ext JS Library 3.1.0
 * Copyright(c) 2006-2009 Ext JS, LLC
 * licensing@extjs.com
 * http://www.extjs.com/license
 */
Ext.ns('Ext.ux.grid');

Ext.ns('PrintShopCreator.Admin.Articlegroup');

var translation = new Locale.Gettext();
translation.textdomain('Article');

var editor = new Ext.ux.grid.RowEditor();

PrintShopCreator.Admin.Articlegroup.LangGrid = Ext.extend(Ext.grid.EditorGridPanel, {

	border:false
    ,stateful:true
	,layout: 'fit'
	,url:'/admin/articlegroup/langfetch/format/json?uid=' + uid
	,objName:'contact'
	,idName:'id'
	,frame: false
	,autoHeight: false
    ,height: 500
	,autoWidth: true
	,id: 'articlegroupgrid'
	,initComponent:function() {

		Ext.apply(this, {
			// {{{
			store:new Ext.data.Store({
				reader:new Ext.data.JsonReader({
					 id:'uuid'
					,totalProperty:'totalCount'
					,root:'results'
					,fields:[
                        {name:'lang', type:'string'}
                        ,{name:'title', type:'string'}
                        ,{name:'uuid', type:'string'}
					]
				})
                ,proxy:new Ext.data.HttpProxy({
                    api: {
                        read    : this.url,
                        create  : this.url + '&mode=create',
                        update  : this.url + '&mode=update',
                        destroy : this.url
                    }})
                ,writer: new Ext.data.JsonWriter({
                    encode: true,
                    writeAllFields: true
                })
				,proxy:new Ext.data.HttpProxy({url:this.url})
				,baseParams:{cmd:'getData', objName:this.objName}
				,remoteSort:true
			})

			,columns:[{
				 header: translation._('Language')
				,id:'lang'
				,dataIndex:'lang'
				,width:20
				,sortable:true
                ,editable:false
                ,editor: new Ext.form.TextField({
                    allowBlank: false
                })
			},{
                    header: translation._('Name')
                    ,id:'title'
                    ,dataIndex:'title'
                    ,width:50
                    ,sortable:true
                    ,editor:new Ext.form.TextField({
                    allowBlank:false
                })
                }]
			// }}}
			,plugins:[editor]
			,viewConfig:{forceFit:true}

			,tbar:[{
				xtype: 'combo'

				,emptyText: translation._('Language')
                                ,store: new Ext.data.JsonStore({
						    url: '/admin/articlegroup/langselect/format/json',
						    root: 'results',
						    fields: ['name', 'id']
				})
                                ,mode: 'remote'
                                ,id: 'langselect'
                                ,triggerAction: 'all'
                                        ,selectOnFocus:true
                                        ,valueField:'id'
                                        ,displayField:'name'
                                        ,listeners:{
                                                select:{scope:this, buffer:200, fn:function(btn) {

                                                try {
                                                    var req = new XMLHttpRequest;

                                                    req.open('POST', '/admin/articlegroup/langadd/format/json?value=' + btn.value + '&articlegroup=' + uid, false);
                                                    req.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
                                                    req.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
                                                    req.send('&delete=1');
                                                    if (req.status == 200 || req.status == 304 || req.status == 0 || req.status == null) {
                                                        this.store.load({params:{start:0,limit:10}});
                                                      return Ext.util.JSON.decode(req.responseText);
                                                    }
                                                  } catch (e) {
                                                    return '';
                                                  }
														}}
                                            }

                                        }]
		}); // eo apply
        editor.on({
            scope: this,
            afteredit: function(roweditor, changes, record, rowIndex) {
                //your save logic here - might look something like this:
                this.store.save();
            }
        });
		// call parent
        PrintShopCreator.Admin.Articlegroup.LangGrid.superclass.initComponent.apply(this, arguments);
	} // eo function initComponent
	// {{{
	,onRender:function() {
		// call parent
        PrintShopCreator.Admin.Articlegroup.LangGrid.superclass.onRender.apply(this, arguments);

		// load store
		this.store.load({params:{start:0,limit:10}});

	} // eo function onRender

	,showError:function(msg, title) {
		Ext.Msg.show({
			 title:title || 'Error'
			,msg:Ext.util.Format.ellipsis(msg, 2000)
			,icon:Ext.Msg.ERROR
			,buttons:Ext.Msg.OK
			,minWidth:1200 > String(msg).length ? 360 : 600
		});
	} // eo function showError

}); // eo extend