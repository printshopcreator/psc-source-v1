// vim: sw=4:ts=4:nu:nospell:fdc=4
/**
 * Ext.ux.grid.RecordForm Plugin Example Application
 *
 * @author    Ing. Jozef Sakáloš
 * @copyright (c) 2008, by Ing. Jozef Sakáloš
 * @date      31. March 2008
 * @version   $Id: recordform.js 187 2008-04-16 00:03:34Z jozo $

 * @license recordform.js is licensed under the terms of
 * the Open Source LGPL 3.0 license.  Commercial use is permitted to the extent
 * that the code/component(s) do NOT become part of another Open Source or Commercially
 * licensed development library or toolkit without explicit permission.
 * 
 * License details: http://www.gnu.org/licenses/lgpl.html
 */

/*global Ext, Web, Example */

Ext.ns('PrintShopCreator');

var translation = new Locale.Gettext();
translation.textdomain('App');

function renderStatus(data, cell, record, rowIndex, columnIndex, store){
         if(data == 1) {
            return 'Zugang';
         }
         if(data == 2) {
             return 'Abgang';
         }
         if(data == 3) {
             return 'Stonierung';
         }
         if(data == 4) {
             return 'Manuelle Buchung';
         }
}

function renderType(data, cell, record, rowIndex, columnIndex, store){
         if(data == 1) {
             return 'Product';
         }
         if(data == 2) {
             return 'Motiv';
         }
}

function renderWebsite(value, metaData, record, rowIndex, colIndex, store) {
	return '<a href="http://' + value + '" target="_blank">' + translation._('shops_gridshops_js_Shoplink') + '</a>';
};

PrintShopCreator.BookingGrid = Ext.extend(Ext.grid.GridPanel, {
	 
	border:false
	,layout: 'fit'
	,stateful:true
	,url:'/admin/booking/fetchbooking/format/json'
	,objName:'booking'
	,idName:'id'
	,frame: true
	,autoHeight: true
	,autoWidth: true
,id: 'bookinggrid'
,title: translation._('booking_grid_js_title')
	,initComponent:function() {
		

		// create row actions
		this.rowActions = new Ext.ux.grid.RowActions({
			 actions:[{
				 iconCls:'icon-edit-record'
				,qtip: translation._('booking_grid_js_Edit_Booking')
			}]
			,widthIntercept:Ext.isSafari ? 4 : 2
			,id:'actions'
		});
		this.rowActions.on('action', this.onRowAction, this);

		Ext.apply(this, {
			// {{{
			store:new Ext.data.Store({
				reader:new Ext.data.JsonReader({
					 id:'id'
					,totalProperty:'totalCount'
					,root:'rows'
					,fields:[
						 {name:'id', type:'int'}
						,{name:'shop_name', type:'string'}
						,{name:'product_contact_creator_name', type:'string'}
                                                ,{name:'product_contact_buyer_name', type:'string'}
                                                ,{name:'type', type:'string'}
                                                ,{name:'product_name', type:'string'}
                                                ,{name:'booking_typ', type:'string'}
                                                ,{name:'booking_text', type:'string'}
                                                ,{name:'product_resale', type:'string'}
                                                ,{name:'payment_value', type:'string'}
					]
				})
				,proxy:new Ext.data.HttpProxy({url:this.url})
				,baseParams:{cmd:'getData', objName:this.objName}
				,sortInfo:{field:'id', direction:'DESC'}
				,remoteSort:true
			})
			// }}}
			// {{{
			,columns:[{
				 header: translation._('booking_gridshops_js_Name')
				,id:'shop_name'
				,dataIndex:'shop_name'
				,width:160
				,sortable:true
				,editor:new Ext.form.TextField({
					allowBlank:false
				})
			},{
				 header: translation._('booking_gridshops_js_Buyer')
				,id:'product_contact_buyer_name'
				,dataIndex:'product_contact_buyer_name'
				,width:160
				,sortable:true
				,editor:new Ext.form.TextField({
					allowBlank:false
				})
			},{
				 header: translation._('booking_gridshops_js_Creator')
				,id:'product_contact_creator_name'
				,dataIndex:'product_contact_creator_name'
				,width:160
				,sortable:true
				,editor:new Ext.form.TextField({
					allowBlank:false
				})
			},{
				 header: translation._('booking_gridshops_js_type')
				,id:'type'
				,dataIndex:'type'
				,width:160
                                ,renderer: renderType
				,sortable:true
				,editor:new Ext.form.TextField({
					allowBlank:false
				})
			},{
				 header: translation._('booking_gridshops_js_product_name')
				,id:'product_name'
				,dataIndex:'product_name'
				,width:160
				,sortable:true
				,editor:new Ext.form.TextField({
					allowBlank:false
				})
			},{
				 header: translation._('booking_gridshops_js_booking_typ')
				,id:'booking_typ'
				,dataIndex:'booking_typ'
				,width:160
                                ,renderer: renderStatus
				,sortable:true
				,editor:new Ext.form.TextField({
					allowBlank:false
				})
			},{
				 header: translation._('booking_gridshops_js_booking_text')
				,id:'booking_text'
				,dataIndex:'booking_text'
				,width:160
				,sortable:true
				,editor:new Ext.form.TextField({
					allowBlank:false
				})
			},{
				 header: translation._('booking_gridshops_js_product_resale')
				,id:'product_resale'
				,dataIndex:'product_resale'
				,width:160
				,sortable:true
				,editor:new Ext.form.TextField({
					allowBlank:false
				})
			},{
				 header: translation._('booking_gridshops_js_payment_value')
				,id:'payment_value'
				,dataIndex:'payment_value'
				,width:160
				,sortable:true
				,editor:new Ext.form.TextField({
					allowBlank:false
				})
			}, this.rowActions]
			// }}}
			,plugins:[this.rowActions]
			,viewConfig:{forceFit:true}
                        ,tbar:[{
				 text: translation._('booking_grid_js_Add Booking')
				,iconCls:'icon-form-add'
				,listeners:{
					click:{scope:this, buffer:200, fn:function(btn) {
						PrintShopCreator.Base.Common.openWindow('bookingWindow', '/admin/booking/edit', 800, 600);
					}}
				}
			},{
				 text: translation._('booking_grid_js_DTAUS Export')
				,iconCls:'icon-export'
                                ,disabled: true
				,listeners:{
					click:{scope:this, buffer:200, fn:function(btn) {
						PrintShopCreator.Base.Common.openWindow('bookingexportWindow', '/admin/booking/export', 800, 600);
					}}
				}
			}]
		}); // eo apply

		this.bbar = new Ext.PagingToolbar({
			 store:this.store
			,displayInfo:true
			,pageSize:20
		});

		// call parent
		PrintShopCreator.BookingGrid.superclass.initComponent.apply(this, arguments);
	} // eo function initComponent
	// {{{
	,onRender:function() {
		// call parent
		PrintShopCreator.BookingGrid.superclass.onRender.apply(this, arguments);
                this.view.getRowClass = function(record, index){
                     if(record.data.booking_typ == 1) {
                        return 'status2';
                     }
                     if(record.data.booking_typ == 2) {
                         return 'status1';
                     }
                     if(record.data.booking_typ == 3) {
                         return 'status3';
                     }
                     if(record.data.booking_typ == 4) {
                         return 'status4';
                     }
                };
		// load store
		this.store.load({params:{start:0,limit:20}});

	} // eo function onRender
	// }}}

	,addRecord:function() {
		var store = this.store;
		if(store.recordType) {
			var rec = new store.recordType({newRecord:true});
			
			return rec;
		}
		return false;
	} // eo function addRecord

	,onRowAction:function(grid, record, action, row, col) {
		switch(action) {
			case 'icon-copy':
				this.copyRecord(record);
			break;

			case 'icon-edit-record':
				
				PrintShopCreator.Base.Common.openWindow('bookingWindow', '/admin/booking/edit?uid=' + record.id, 800, 600);
			break;
		}
	} // eo onRowAction

	,showError:function(msg, title) {
		Ext.Msg.show({
			 title:title || 'Error'
			,msg:Ext.util.Format.ellipsis(msg, 2000)
			,icon:Ext.Msg.ERROR
			,buttons:Ext.Msg.OK
			,minWidth:1200 > String(msg).length ? 360 : 600
		});
	} // eo function showError

	,copyRecord:function(record) {
		Ext.Msg.show({
			 title: translation._('msg_copy')
			,msg: translation._('msg_copy_confirm_beforetext') + ':<br><b>' + record.get('name') + '</b><br>' + translation._('msg_copy_confirm_aftertext')
			,icon:Ext.Msg.QUESTION
			,buttons:Ext.Msg.YESNO
			,scope:this
			,fn:function(response) {
				if('yes' == response) {
					 try {
					    var req = new XMLHttpRequest;

					    req.open('POST', '/admin/system/shops?uid=' + record.id, false);
					    req.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
					    req.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
					    //req.send('&copy=1');
					    if (req.status == 200 || req.status == 304 || req.status == 0 || req.status == null) {
                          this.store.load({params:{start:0,limit:20}});
					      return Ext.util.JSON.decode(req.responseText);
					    }
					  } catch (e) {
					    return '';
					  }
				}else{
				    return;
				}
//				console.info('Deleting record');
			}
		});
	} // eo function deleteRecord

}); // eo extend

// register xtype
Ext.reg('bookinggrid', PrintShopCreator.BookingGrid);

Ext.QuickTips.init();
	
	var booking = new Ext.FormPanel({
		               
        renderTo: 'bookingindex',
        closable: false,
        resizable: false,
        layout: 'fit',
        border: false,
        items:[{xtype:'bookinggrid', id:'bookinggrid'}]
});
booking.show();
