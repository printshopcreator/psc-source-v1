Ext.QuickTips.init();

Ext.onReady(function() {

    var translation = new Locale.Gettext();
    translation.textdomain('App');

    var win = new Ext.Window({
         id:'metaform-win'
        ,layout:'fit'
        ,width: 800
        ,scope:this
        ,height: 600
        ,title: translation._('cms_edit_js_CMS')
        ,items:{
             xtype:'metaform'
            ,url:'/admin/cms/edit?config=3&sid='+ sid + '&uid=' + uid
            ,buttons:
            [{
                 text: translation._('cms_edit_js_Save')
                ,iconCls: 'action_applyChanges'
                ,listeners:{
                   click:{scope:this, buffer:200, fn:function(btn) {
    
	                   Ext.getCmp('metaform-win').items.get(0).getForm().submit({waitMsg: translation._('cms_edit_js_Saving')
                               ,url: '/admin/cms/edit?config=3&sid='+ sid + '&uid=' + uid + '&save=1'
                               ,success: function(form, action){
                                      if(action.result.isNew) {
                                        window.location = '/admin/cms/edit?sid='+ sid + '&uid=' + action.result.uid;
                                      }
                                }});
                            opener.Ext.getCmp('cmsgrid').store.reload();
                }}}
            },{
                  text: translation._('cms_edit_js_Save and Close')
                ,iconCls: 'action_applyChangesClose'
                ,listeners:{
                   click:{scope:this, buffer:200, fn:function(btn) {
	                   Ext.getCmp('metaform-win').items.get(0).getForm().submit({waitMsg: translation._('cms_edit_js_Saving')
	                   , url: '/admin/cms/edit?config=3&sid='+ sid + '&uid=' + uid + '&save=1'
	                   ,success: function(form, action){
				                  window.close();  
				                }
	                   
	                   });
                           opener.Ext.getCmp('cmsgrid').store.reload();
                }}}
            },{
                text: translation._('cms_edit_js_Close'),
                listeners:{
                   click:{scope:this, buffer:200, fn:function(btn) {
                        window.close();
                }}},
                iconCls: 'action_cancel'
            }]
        }
    });

    win.show();
});