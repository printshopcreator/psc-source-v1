Ext.ns('PrintShopCreator');

var translation = new Locale.Gettext();
translation.textdomain('App');

function renderStatus(data, cell, record, rowIndex, columnIndex, store){
         var value=record.get('status');
         //cell.css = "UnStyleCss"; //défini dans style.css
         cell.css = "status" + value; //Color's set here
         return data; //data = text
}
PrintShopCreator.CmsGrid = Ext.extend(Ext.grid.GridPanel, {
	 
	border:false
	,layout: 'fit'
	,stateful:true
	,url:'/admin/cms/all?config=1&sid=' + sid
	,objName:'cms'
	,idName:'id'
	,frame: false
	,autoHeight: true
	,autoWidth: true
,id: 'cmsgrid'
	,initComponent:function() {
		

		// create row actions
		this.rowActions = new Ext.ux.grid.RowActions({
			 actions:[{
				 iconCls:'icon-minus'
				,qtip: translation._('cms_gridcms_js_Delete Page')
			},{
				 iconCls:'icon-edit-record'
				,qtip: translation._('cms_gridcms_js_Edit Page')
			}]
			,widthIntercept:Ext.isSafari ? 4 : 2
			,id:'actions'
		});
		this.rowActions.on('action', this.onRowAction, this);

                var filters = new Ext.ux.grid.GridFilters({
		  filters:[
			{type: 'string',  dataIndex: 'title'},
                        {type: 'string',  dataIndex: 'menu'},
                        {type: 'string',  dataIndex: 'url'},
                        {type: 'string',  dataIndex: 'pos'}

		]});

		Ext.apply(this, {
			// {{{
			store:new Ext.data.Store({
                                id: 'store_cms_index',
				reader:new Ext.data.JsonReader({
					 id:'id'
					,totalProperty:'totalCount'
					,root:'rows'
					,fields:[
						 {name:'id', type:'int'}
						,{name:'title', type:'string'}
						,{name:'parent', type:'string'}
						,{name:'menu', type:'string'}
                        ,{name:'pos', type:'string'}
						,{name:'url', type:'string'}
					]
				})
				,proxy:new Ext.data.HttpProxy({url:this.url})
				,baseParams:{cmd:'getData', objName:this.objName}
				,sortInfo:{field:'title', direction:'ASC'}
				,remoteSort:true
			})
			// }}}
			// {{{
			,columns:[{
				 header: 'Id'
				,id:'id'
				,dataIndex:'id'
				,width:20
				,sortable:true

			},{
				 header: translation._('cms_gridcms_js_Name')
				,id:'title'
				,dataIndex:'title'
				,width:160
				,sortable:true
				,editor:new Ext.form.TextField({
					allowBlank:false
				})
			},{
				 header: translation._('articlegroup_all_js_Parent')
				,id:'parent'
				,dataIndex:'parent'
				,width:100
				,sortable:true
					
			},{
				 header: translation._('cms_gridcms_js_Menu')
				,id:'menu'
				,dataIndex:'menu'
				,width:160
				,sortable:true
				,editor:new Ext.form.TextField({
					allowBlank:false
				})
			},{
                header: translation._('Pos')
                ,id:'pos'
                ,dataIndex:'pos'
                ,width:160
                ,sortable:true
                ,editor:new Ext.form.TextField({
                    allowBlank:false
                })
            },{
				 header: translation._('cms_gridcms_js_URL')
				,id:'url'
				,dataIndex:'url'
				,width:160
				,sortable:true
				,editor:new Ext.form.TextField({
					allowBlank:false
				})
			}, this.rowActions]
			// }}}
			,plugins:[this.rowActions, filters]
			,viewConfig:{forceFit:true}			
			,tbar:[{
			//,plugins:[new Ext.ux.grid.Search({
			//	iconCls:'icon-zoom'
			//	,readonlyIndexes:['note']
			//	,disableIndexes:['pctChange']
			//}), this.rowActions]
			//,viewConfig:{forceFit:true}
			//,tbar:[{
				 text: translation._('cms_gridcms_js_Add Site')
				,iconCls:'icon-form-add'
				,listeners:{
					click:{scope:this, buffer:200, fn:function(btn) {
						PrintShopCreator.Base.Common.openWindow('cmsWindow', '/admin/cms/edit?sid='+ sid, 800, 600);
					}}
				}
			}]
		}); // eo apply

		this.bbar = new Ext.PagingToolbar({
			 store:this.store
			,displayInfo: true
			,plugins: [filters, new Ext.ux.ProgressBarPager()]
			,pageSize:20
		});

		// call parent
		PrintShopCreator.CmsGrid.superclass.initComponent.apply(this, arguments);
	} // eo function initComponent
	// {{{
	,onRender:function() {
		// call parent
		PrintShopCreator.CmsGrid.superclass.onRender.apply(this, arguments);

		// load store
		this.store.load({params:{start:0,limit:20}});

	} // eo function onRender
	// }}}

	,addRecord:function() {
		var store = this.store;
		if(store.recordType) {
			var rec = new store.recordType({newRecord:true});
			
			return rec;
		}
		return false;
	} // eo function addRecord

	,onRowAction:function(grid, record, action, row, col) {
		switch(action) {
			case 'icon-minus':
				this.deleteRecord(record);
			break;

			case 'icon-edit-record':
				
				PrintShopCreator.Base.Common.openWindow('articleWindow', '/admin/cms/edit?sid='+ sid + '&uid=' + record.id, 800, 600);
			break;
		}
	} // eo onRowAction

	,showError:function(msg, title) {
		Ext.Msg.show({
			 title:title || 'Error'
			,msg:Ext.util.Format.ellipsis(msg, 2000)
			,icon:Ext.Msg.ERROR
			,buttons:Ext.Msg.OK
			,minWidth:1200 > String(msg).length ? 360 : 600
		});
	} // eo function showError

	,deleteRecord:function(record) {
		Ext.Msg.show({
			 title: translation._('msg_delete')
			,msg: translation._('msg_delete_confirm_beforetext') + ':<br><b>' + record.get('title') + '</b><br>' + translation._('msg_delete_confirm_aftertext')
			,icon:Ext.Msg.QUESTION
			,buttons:Ext.Msg.YESNO
			,scope:this
			,fn:function(response) {
				if('yes' == response) {
					 try {
					    var req = new XMLHttpRequest;

					    req.open('POST', '/admin/cms/edit?uid=' + record.id, false);
					    req.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
					    req.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
					    req.send('&delete=1');
					    if (req.status == 200 || req.status == 304 || req.status == 0 || req.status == null) {
                          this.store.load({params:{start:0,limit:20}});
					      return Ext.util.JSON.decode(req.responseText);
					    }
					  } catch (e) {
					    return '';
					  }
				}else{
				    return;
				}
//				console.info('Deleting record');
			}
		});
	} // eo function deleteRecord

}); // eo extend

// register xtype
Ext.reg('cmsgrid', PrintShopCreator.CmsGrid);

Ext.QuickTips.init();
	
	var order = new Ext.FormPanel({
		               
        renderTo: 'cmsindex',
        closable: false,
        resizable: false,
        layout: 'fit',
        border: false,
        items:[{xtype:'cmsgrid', id:'cmsgrid'}]
});
order.show();
