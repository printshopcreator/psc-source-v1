/*!
 * Ext JS Library 3.1.0
 * Copyright(c) 2006-2009 Ext JS, LLC
 * licensing@extjs.com
 * http://www.extjs.com/license
 */
Ext.ns('Ext.ux.grid');

/**
 * @class Ext.ux.grid.CheckColumn
 * @extends Object
 * GridPanel plugin to add a column with check boxes to a grid.
 * <p>Example usage:</p>
 * <pre><code>
// create the column
var checkColumn = new Ext.grid.CheckColumn({
   header: 'Indoor?',
   dataIndex: 'indoor',
   id: 'check',
   width: 55
});

// add the column to the column model
var cm = new Ext.grid.ColumnModel([{
       header: 'Foo',
       ...
    },
    checkColumn
]);

// create the grid
var grid = new Ext.grid.EditorGridPanel({
    ...
    cm: cm,
    plugins: [checkColumn], // include plugin
    ...
});
 * </code></pre>
 * In addition to storing a Boolean value within the record data, this
 * class toggles a css class between <tt>'x-grid3-check-col'</tt> and
 * <tt>'x-grid3-check-col-on'</tt> to alter the background image used for
 * a column.
 */
Ext.ux.grid.CheckColumn = function(config){
    Ext.apply(this, config);
    if(!this.id){
        this.id = Ext.id();
    }
    this.renderer = this.renderer.createDelegate(this);
};

Ext.ux.grid.CheckColumn.prototype ={
    init : function(grid){
        this.grid = grid;
        this.grid.on('render', function(){
            var view = this.grid.getView();
            view.mainBody.on('mousedown', this.onMouseDown, this);
        }, this);
    },

    onMouseDown : function(e, t){
        if(t.className && t.className.indexOf('x-grid3-cc-'+this.id) != -1){
            e.stopEvent();
            var index = this.grid.getView().findRowIndex(t);
            var record = this.grid.store.getAt(index);
            record.set(this.dataIndex, !record.data[this.dataIndex]);
            try {
			    var req = new XMLHttpRequest;

			    req.open('POST', '/admin/contact/contactshopchangeadmin/format/json?uuid=' + record.data.uuid , false);
			    req.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
			    req.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
			    req.send('&changeadmin=1');
			    if (req.status == 200 || req.status == 304 || req.status == 0 || req.status == null) {
			    	this.grid.store.reload();
			    }
			  } catch (e) {
			    return '';
			  }
            
        }
    },

    renderer : function(v, p, record){
        p.css += ' x-grid3-check-col-td'; 
        return '<div class="x-grid3-check-col'+(v?'-on':'')+' x-grid3-cc-'+this.id+'">&#160;</div>';
    }
};

// register ptype
Ext.preg('checkcolumn', Ext.ux.grid.CheckColumn);

// backwards compat
Ext.grid.CheckColumn = Ext.ux.grid.CheckColumn;

Ext.ns('PrintShopCreator.Admin.Contact');

var translation = new Locale.Gettext();
translation.textdomain('Contact');

PrintShopCreator.Admin.Contact.GridPanel = Ext.extend(Ext.grid.EditorGridPanel, {

	border:false
	,layout: 'fit'
	,stateful:true
	,url:'/admin/contact/contactshop/format/json?uid=' + uid
	,objName:'contact'
	,idName:'id'
	,frame: false
	,autoHeight: true
	,autoWidth: true
	,id: 'contactgrid'
	,initComponent:function() {

		// create row actions
		this.rowActions = new Ext.ux.grid.RowActions({
			 actions:[{
				 iconCls:'icon-minus'
				,qtip: translation._('contact_gridcontact_js_Delete')
			}]
			,widthIntercept:Ext.isSafari ? 4 : 2
			,id:'actions'
		});
		this.rowActions.on('action', this.onRowAction, this);
		
		var checkColumn = new Ext.grid.CheckColumn({
		       header: translation._('article_index_js_Admin'),
		       dataIndex: 'admin',
		       width: 55
	    });

		Ext.apply(this, {
			// {{{
			store:new Ext.data.Store({
				reader:new Ext.data.JsonReader({
					 id:'id'
					,totalProperty:'totalCount'
					,root:'results'
					,fields:[
						{name:'shop', type:'string'}
						,{name:'admin', type:'bool'}
                                                ,{name:'uuid', type:'string'}
					]
				})
				,proxy:new Ext.data.HttpProxy({url:this.url})
				,baseParams:{cmd:'getData', objName:this.objName}
				,remoteSort:true
			})
			// }}}
			// {{{
			,columns:[{
				 header: translation._('contact_gridcontact_js_Shop')
				,id:'shop'
				,dataIndex:'shop'
				,width:160
				,sortable:true
				,editor:new Ext.form.TextField({
					allowBlank:false
				})
			}, checkColumn, this.rowActions]
			// }}}
			,plugins:[this.rowActions, checkColumn]
			,viewConfig:{forceFit:true}
			,tbar:[{
				xtype: 'combo'

				,emptyText: translation._('article_index_js_Add Shop')
                                ,store: new Ext.data.JsonStore({
						    url: '/admin/contact/contactshopselect/format/json',
						    root: 'results',
						    fields: ['shop', 'shop_id']
				})
                                ,mode: 'remote'
                                ,id: 'shopselect'
                                ,triggerAction: 'all'
                                        ,selectOnFocus:true
                                        ,valueField:'shop_id'
                                        ,displayField:'shop'
                                        ,listeners:{
                                                select:{scope:this, buffer:200, fn:function(btn) {

                                                        try {
														    var req = new XMLHttpRequest;

														    req.open('POST', '/admin/contact/contactshopadd/format/json?value=' + btn.value + '&contact=' + uid, false);
														    req.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
														    req.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
														    req.send('&delete=1');
														    if (req.status == 200 || req.status == 304 || req.status == 0 || req.status == null) {
									                            this.store.load({params:{start:0,limit:10}});
														      return Ext.util.JSON.decode(req.responseText);
														    }
														  } catch (e) {
														    return '';
														  }
														}}
                                            }

                                        }]
		}); // eo apply

		// call parent
		PrintShopCreator.Admin.Contact.GridPanel.superclass.initComponent.apply(this, arguments);
	} // eo function initComponent
	// {{{
	,onRender:function() {
		// call parent
		PrintShopCreator.Admin.Contact.GridPanel.superclass.onRender.apply(this, arguments);

		// load store
		this.store.load({params:{start:0,limit:10}});

	} // eo function onRender
	// }}}

	,onRowAction:function(grid, record, action, row, col) {
		switch(action) {

                        case 'icon-minus':
				this.deleteRecord(record);
		}
	} // eo onRowAction

	,showError:function(msg, title) {
		Ext.Msg.show({
			 title:title || 'Error'
			,msg:Ext.util.Format.ellipsis(msg, 2000)
			,icon:Ext.Msg.ERROR
			,buttons:Ext.Msg.OK
			,minWidth:1200 > String(msg).length ? 360 : 600
		});
	} // eo function showError

	,deleteRecord:function(record) {
		Ext.Msg.show({
			 title: translation._('msg_delete')
			,msg: translation._('msg_delete_confirm_beforetext') + ':<br><b>' + record.get('shop') + '</b><br>' + translation._('msg_delete_confirm_aftertext')
			,icon:Ext.Msg.QUESTION
			,buttons:Ext.Msg.YESNO
			,scope:this
			,fn:function(response) {
				if('yes' == response) {
					 try {
					    var req = new XMLHttpRequest;

					    req.open('POST', '/admin/contact/contactshopdelete/format/json?uuid=' + record.data.uuid, false);
					    req.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
					    req.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
					    req.send('&delete=1');
					    if (req.status == 200 || req.status == 304 || req.status == 0 || req.status == null) {
                            this.store.load({params:{start:0,limit:10}});
					      return Ext.util.JSON.decode(req.responseText);
					    }
					  } catch (e) {
					    return '';
					  }
				}else{
				    return;
				}
//				console.info('Deleting record');
			}
		});
	} // eo function deleteRecord

}); // eo extend