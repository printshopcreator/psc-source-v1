Ext.ns('Ext.ux.grid');

Ext.ux.grid.CheckColumnLiefer = function(config){
    Ext.apply(this, config);
    if(!this.id){
        this.id = Ext.id();
    }
    this.renderer = this.renderer.createDelegate(this);
};

Ext.ux.grid.CheckColumnLiefer.prototype ={
    init : function(grid){
        this.grid = grid;
        this.grid.on('render', function(){
            var view = this.grid.getView();
            view.mainBody.on('mousedown', this.onMouseDown, this);
        }, this);
    },

    onMouseDown : function(e, t){
        if(t.className && t.className.indexOf('x-grid3-cc-'+this.id) != -1){
            e.stopEvent();
            var index = this.grid.getView().findRowIndex(t);
            var record = this.grid.store.getAt(index);
            record.set(this.dataIndex, !record.data[this.dataIndex]);
            try {
			    var req = new XMLHttpRequest;

			    req.open('POST', '/admin/contact/contactaddresschangedisplay/format/json?uuid=' + record.data.uuid , false);
			    req.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
			    req.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
			    req.send('&changeadmin=1');
			    if (req.status == 200 || req.status == 304 || req.status == 0 || req.status == null) {
			    	this.grid.store.reload();
			    }
			  } catch (e) {
			    return '';
			  }
            
        }
    },

    renderer : function(v, p, record){
        p.css += ' x-grid3-check-col-td'; 
        return '<div class="x-grid3-check-col'+(v?'-on':'')+' x-grid3-cc-'+this.id+'">&#160;</div>';
    }
};

// register ptype
Ext.preg('checkcolumnliefer', Ext.ux.grid.CheckColumnLiefer);

// backwards compat
Ext.grid.CheckColumnLiefer = Ext.ux.grid.CheckColumnLiefer;

Ext.ns('PrintShopCreator.Admin.Contact');

var translation = new Locale.Gettext();
translation.textdomain('Contact');

PrintShopCreator.Admin.Contact.GridLieferPanel = Ext.extend(Ext.grid.EditorGridPanel, {

	border:false
	,layout: 'fit'
	,stateful:true
	,url:'/admin/contact/contactaddress/format/json?uid=' + uid
	,objName:'contact'
	,idName:'id'
	,frame: false
	,autoHeight: true
	,autoWidth: true
	,id: 'contactliefer'
	,initComponent:function() {

		// create row actions
		this.rowActions = new Ext.ux.grid.RowActions({
			 actions:[{
				 iconCls:'icon-minus'
				,qtip: translation._('contact_gridcontact_js_Delete')
			}]
			,widthIntercept:Ext.isSafari ? 4 : 2
			,id:'actions'
		});
		this.rowActions.on('action', this.onRowAction, this);
		
		var checkColumn = new Ext.grid.CheckColumnLiefer({
		       header: translation._('Display'),
		       dataIndex: 'display',
		       width: 55
	    });

		Ext.apply(this, {
			// {{{
			store:new Ext.data.Store({
				reader:new Ext.data.JsonReader({
					 id:'id'
					,totalProperty:'totalCount'
					,root:'results'
					,fields:[
						{name:'shop', type:'string'}
						,{name:'firstname', type:'string'}
						,{name:'lastname', type:'string'}
						,{name:'company', type:'string'}
                        ,{name:'street', type:'string'}
                        ,{name:'zip', type:'string'}
						,{name:'city', type:'string'}
                        ,{name:'type', type:'string'}
						,{name:'display', type:'bool'}
                        ,{name:'uuid', type:'string'}
					]
				})
				,proxy:new Ext.data.HttpProxy({url:this.url})
				,baseParams:{cmd:'getData', objName:this.objName}
				,remoteSort:true
			})
			// }}}
			// {{{
			,columns:[{
				 header: translation._('contact_settings_fieldLabel_Firstname')
				,id:'firstname'
				,dataIndex:'firstname'
				,width:160
				,sortable:true
				,editor:new Ext.form.TextField({
					allowBlank:false
				})
			},{
				 header: translation._('contact_settings_fieldLabel_Lastname')
					,id:'lastname'
					,dataIndex:'lastname'
					,width:160
					,sortable:true
					,editor:new Ext.form.TextField({
						allowBlank:false
					})
				},{
					 header: translation._('contact_settings_fieldLabel_Company')
						,id:'company'
						,dataIndex:'company'
						,width:160
						,sortable:true
						,editor:new Ext.form.TextField({
							allowBlank:false
						})
					},{
						 header: translation._('contact_settings_fieldLabel_Street')
							,id:'street'
							,dataIndex:'street'
							,width:160
							,sortable:true
							,editor:new Ext.form.TextField({
								allowBlank:false
							})
						},{
						 header: translation._('contact_settings_fieldLabel_Zip')
							,id:'zip'
							,dataIndex:'zip'
							,width:160
							,sortable:true
							,editor:new Ext.form.TextField({
								allowBlank:false
							})
						},{
						 header: translation._('Typ')
							,id:'type'
							,dataIndex:'type'
							,width:160
							,sortable:true
							,editor:new Ext.form.TextField({
								allowBlank:false
							})
						},{
                header: translation._('contact_settings_fieldLabel_City')
                ,id:'city'
                ,dataIndex:'city'
                ,width:160
                ,sortable:true
                ,editor:new Ext.form.TextField({
                    allowBlank:false
                })
            }, checkColumn]
			// }}}
			,plugins:[checkColumn]
			,viewConfig:{forceFit:true}
			
		}); // eo apply

		// call parent
		PrintShopCreator.Admin.Contact.GridLieferPanel.superclass.initComponent.apply(this, arguments);
	} // eo function initComponent
	// {{{
	,onRender:function() {
		// call parent
		PrintShopCreator.Admin.Contact.GridLieferPanel.superclass.onRender.apply(this, arguments);

		// load store
		this.store.load({params:{start:0,limit:10}});

	} // eo function onRender
	// }}}

	,onRowAction:function(grid, record, action, row, col) {
		switch(action) {

                        case 'icon-minus':
				this.deleteRecord(record);
		}
	} // eo onRowAction

	,showError:function(msg, title) {
		Ext.Msg.show({
			 title:title || 'Error'
			,msg:Ext.util.Format.ellipsis(msg, 2000)
			,icon:Ext.Msg.ERROR
			,buttons:Ext.Msg.OK
			,minWidth:1200 > String(msg).length ? 360 : 600
		});
	} // eo function showError

}); // eo extend