Ext.ns('PrintShopCreator.Admin.Contact');

var translation = new Locale.Gettext();
translation.textdomain('Contact');

PrintShopCreator.Admin.Contact.Grid = Ext.extend(Ext.grid.GridPanel, {
	 
    border:false
    ,
    layout: 'fit'
    ,
    stateful:true
    ,
    url:'/admin/account/all?config=3&sid=' + sid + '&market=' + market
    ,
    objName:'contact'
    ,
    idName:'id'
    ,
    frame: false
    ,
    autoHeight: true
    ,
    autoWidth: true
    ,
    id: 'contactgrid'
    ,
    title: translation._('Personen/Kunden')
    ,
    initComponent:function() {
		
        // create row actions
        this.rowActions = new Ext.ux.grid.RowActions({
            actions:[{
                iconCls:'icon-go-tab'
                ,
                qtip: translation._('contact_gridcontact_js_Filter')
            },{
                iconCls:'icon-edit-record'
                ,
                qtip: translation._('contact_gridcontact_js_Edit')
            }]
            ,
            widthIntercept:Ext.isSafari ? 4 : 2
            ,
            id:'actions'
        });
        this.rowActions.on('action', this.onRowAction, this);

        var filters = new Ext.ux.grid.GridFilters({
            filters:[
            {
                type: 'string',
                dataIndex: 'id'
            },
            {
                type: 'string',
                dataIndex: 'name'
            },

            {
                type: 'string',
                dataIndex: 'self_firstname'
            },

            {
                type: 'string',
                dataIndex: 'self_lastname'
            },

            {
                type: 'string',
                dataIndex: 'self_zip'
            },

            {
                type: 'string',
                dataIndex: 'self_city'
            },

            {
                type: 'string',
                dataIndex: 'self_email'
            },

            {
                type: 'string',
                dataIndex: 'self_department'
            },

            {
                type: 'boolean',
                dataIndex: 'enable',
                defaultValue: true,
                value: true
            }


            ]
            });


        Ext.apply(this, {
            // {{{
            store:new Ext.data.Store({
                reader:new Ext.data.JsonReader({
                    id:'id'
                    ,
                    totalProperty:'totalCount'
                    ,
                    root:'rows'
                    ,
                    fields:[
                    {
                        name:'id',
                        type:'int'
                    }
                    ,{
                        name:'name',
                        type:'string'
                    }

                    ,{
                        name:'self_firstname',
                        type:'string'
                    }
                    ,{
                        name:'self_lastname',
                        type:'string'
                    }
                    ,{
                        name:'self_zip',
                        type:'string'
                    }
                    ,{
                        name:'self_city',
                        type:'string'
                    }
                    ,{
                        name:'self_phone',
                        type:'string'
                    }
                    ,{
                        name:'self_email',
                        type:'string'
                    }
                    ,{
                        name:'self_department',
                        type:'string'
                    }
                    ,{
                        name:'enable',
                        type:'boolean'
                    }
                    ]
                })
                ,
                proxy:new Ext.data.HttpProxy({
                    url:this.url
                    })
                ,
                baseParams:{
                    cmd:'getData',
                    objName:this.objName
                    }
                ,
                sortInfo:{
                    field:'self_firstname',
                    direction:'ASC'
                }
                ,
                remoteSort:true
            })
            // }}}
            // {{{
            ,
            columns:[{
                header: translation._('contact_settings_fieldLabel_KundenNr')
                ,
                id:'id'
                ,
                dataIndex:'id'
                ,
                width:160
                ,
                sortable:true
               
            },{
                header: translation._('Username')
                ,
                id:'name'
                ,
                dataIndex:'name'
                ,
                width:160
                ,
                sortable:true
                
            },{
                header: translation._('contact_gridcontact_js_Firstname')
                ,
                id:'firstname'
                ,
                dataIndex:'self_firstname'
                ,
                width:160
                ,
                sortable:true
                
            },{
                header: translation._('contact_gridcontact_js_Lastname')
                ,
                id:'lastname'
                ,
                dataIndex:'self_lastname'
                ,
                width:160
                ,
                sortable:true
                
            },{
                header: translation._('contact_gridcontact_js_Company')
                ,
                id:'company'
                ,
                dataIndex:'self_department'
                ,
                width:160
                ,
                sortable:true
                
            },{
                header: translation._('contact_gridcontact_js_Zip')
                ,
                id:'zip'
                ,
                dataIndex:'self_zip'
                ,
                width:100
                ,
                sortable:true
                
            },{
                header: translation._('contact_gridcontact_js_City')
                ,
                id:'city'
                ,
                dataIndex:'self_city'
                ,
                width:160
                ,
                sortable:true
                
            },{
                header: translation._('contact_gridcontact_js_Phone')
                ,
                id:'phone'
                ,
                dataIndex:'self_phone'
                ,
                width:160
                ,
                sortable:true
                
            },{
                header: translation._('contact_gridcontact_js_Email')
                ,
                id:'Email'
                ,
                dataIndex:'self_email'
                ,
                width:160
                ,
                sortable:true
                ,
                renderer: function(val) {
                    return '<a href="mailto:'+val+'">'+val+'</a>';
                }
            },{
                header: translation._('article_index_js_Enable')
                ,
                id:'enable'
                ,
                dataIndex:'enable'
                ,
                width:160
                ,
                sortable:true
                ,
                renderer: PrintShopCreator.Base.Common.renderBool
            }, this.rowActions]
            // }}}
            ,
            plugins:[this.rowActions, filters]
            ,
            viewConfig:{
                forceFit:true
            }
            ,
            tbar:[{
                text: translation._('contact_gridcontact_js_Add Contact')
                ,
                iconCls:'icon-form-add'
                ,
                listeners:{
                    click:{
                        scope:this,
                        buffer:200,
                        fn:function(btn) {
					
                            PrintShopCreator.Base.Common.openWindow('contactWindow', '/admin/contact/edit?sid='+ sid, 800, 700);
						
                        }
                    }
            }
                },'-',{
                text: translation._('contact_gridcontact_js_Export Contact')
                ,
                iconCls:'icon-export'
                ,
                listeners:{
                    click:{
                        scope:this,
                        buffer:200,
                        fn:function(btn) {

                            PrintShopCreator.Base.Common.openWindow('contactWindow', '/admin/contact/export?mode=csv&sid='+ sid, 800, 700);

                        }
                    }
            }
        },{
                text: translation._('Export XLS')
                ,
                iconCls:'icon-export'
                ,
                listeners:{
                    click:{
                        scope:this,
                        buffer:200,
                        fn:function(btn) {

                            PrintShopCreator.Base.Common.openWindow('contactWindow', '/admin/contact/export?sid='+ sid, 800, 700);

                        }
                    }
            }
            }]
        }); // eo apply

    this.bbar = new Ext.PagingToolbar({
        store:this.store
        ,
        pageSize:10
        ,
        displayInfo: true
        ,
        plugins: [filters,new Ext.ux.ProgressBarPager()]
    });

    // call parent
    PrintShopCreator.Admin.Contact.Grid.superclass.initComponent.apply(this, arguments);
} // eo function initComponent
// {{{
,
onRender:function() {
    // call parent
    PrintShopCreator.Admin.Contact.Grid.superclass.onRender.apply(this, arguments);

    // load store
    this.store.load({
        params:{
            start:0,
            limit:10
        }
    });

} // eo function onRender
// }}}

,
onRowAction:function(grid, record, action, row, col) {
    switch(action) {
		
        case 'icon-go-tab':
            var grid1 = Ext.getCmp('PrintShopCreatorAdminAccountGrid');
            grid1.store.reload({
                params:{
                    start:0,
                    limit:10,
                    contact_id: record.id
                    }
                });
        var grid3 = Ext.getCmp('PrintShopCreatorAdminOrderGrid');
        grid3.store.reload({
            params:{
                start:0,
                limit:10,
                contact_id: record.id
                }
            });
				
    break;
			
			

case 'icon-edit-record':
    PrintShopCreator.Base.Common.openWindow('contactWindow', '/admin/contact/edit?sid='+ sid + '&uid=' + record.id, 800, 700);
    break;
}
} // eo onRowAction
	
,
showError:function(msg, title) {
    Ext.Msg.show({
        title:title || 'Error'
        ,
        msg:Ext.util.Format.ellipsis(msg, 2000)
        ,
        icon:Ext.Msg.ERROR
        ,
        buttons:Ext.Msg.OK
        ,
        minWidth:1200 > String(msg).length ? 360 : 600
    });
} // eo function showError

,
deleteRecord:function(record) {
    Ext.Msg.show({
        title: translation._('msg_delete')
        ,
        msg: translation._('msg_delete_confirm_beforetext') + ':<br><b>' + record.get('self_firstname') + ' ' + record.get('self_lastname') + '</b><br>' + translation._('msg_delete_confirm_aftertext')
        ,
        icon:Ext.Msg.QUESTION
        ,
        buttons:Ext.Msg.YESNO
        ,
        scope:this
        ,
        fn:function(response) {
            if('yes' == response) {
                try {
                    var req = new XMLHttpRequest;
					
                    req.open('POST', '/admin/contact/edit?config=5' + '&uid=' + record.id, false);
                    req.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
                    req.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
                    req.send('&delete=1');
                    if (req.status == 200 || req.status == 304 || req.status == 0 || req.status == null) {
                        this.store.load({
                            params:{
                                start:0,
                                limit:10
                            }
                        });
                    return Ext.util.JSON.decode(req.responseText);
                }
            } catch (e) {
                return '';
            }
        }else{
            return;
        }
    //				console.info('Deleting record');
    }
    });
} // eo function deleteRecord

}); // eo extend

// register xtype
Ext.reg('PrintShopCreatorAdminContactGrid', PrintShopCreator.Admin.Contact.Grid);
Ext.QuickTips.init();
	
var contact = new Ext.FormPanel({
		               
    renderTo: 'contactindex',
    closable: false,
    resizable: false,
    layout: 'fit',
    border: false,
    items:[{
        xtype:'PrintShopCreatorAdminContactGrid',
        id:'PrintShopCreatorAdminContactGrid'
    }]
});
contact.show();
