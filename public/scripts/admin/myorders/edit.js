Ext.onReady(function() {


    Ext.QuickTips.init();

    var translation = new Locale.Gettext();
    translation.textdomain('Contact');

    var url = '/admin/myorders/edit?config=3&uid=' + uid;

    var products = new Ext.grid.GridPanel({
        autoWidth: true,
autoHeight: true,
        store: new Ext.data.Store({
            reader:new Ext.data.JsonReader({
                     id:'id'
                    ,totalProperty:'totalCount'
                    ,root:'rows'
                    ,fields:[
                         {name:'id', type:'int'}
                        ,{name:'articlename', type:'string'}
                        ,{name:'count', type:'string'}
                        ,{name:'priceone', type:'string'}
                        ,{name:'priceall', type:'string'}
                        ,{name:'options', type:'string'}
                    ]
                })
                ,proxy:new Ext.data.HttpProxy({url:url})
                ,baseParams:{cmd:'getData', objName:'product'}
                ,sortInfo:{field:'id', direction:'ASC'}
                ,remoteSort:true
        }),
        cm: new Ext.grid.ColumnModel([
            {id:'article', header: translation._('myorders_edit_js_Productname'), width: 20, sortable: true, dataIndex: 'articlename'},
            {header: translation._('myorders_edit_js_Count'), width: 10, sortable: true, dataIndex: 'count'},
            {header: translation._('myorders_edit_js_Price one'), width: 10, sortable: true, dataIndex: 'priceone'},
            {header: translation._('myorders_edit_js_Price all'), width: 10, sortable: true, dataIndex: 'priceall'},
            {header: translation._('myorders_edit_js_Options'), width: 60, sortable: true, dataIndex: 'options'}
        ]),
        viewConfig: {
            forceFit:true
        },
        collapsible: true,
        animCollapse: false,
        iconCls: 'icon-grid'
    });
    
    products.store.load();

    var tabPanelOrder = {
            title: translation._('myorders_edit_js_Overview'),
            layout:'form',
            layoutOnTabChange:true,
            defaults: {
                border: true,
                frame: true            
            },
            items: {
                xtype:'metaform'
                ,url:'/admin/myorders/edit?config=4&uid=' + uid
            }
    }


    var tabPanelContact = {
            title: translation._('myorders_edit_js_Contact'),
            layout:'form',
            layoutOnTabChange:true,
            defaults: {
                border: true,
                frame: true            
            },
            items: {
                xtype:'metaform'
	            ,url:'/admin/myorders/edit?config=5&uid=' + uid
            }
    }
    
    var tabPanelProducts = {
            title: translation._('myorders_edit_js_Products'),
            layout:'form',
            layoutOnTabChange:true,
            defaults: {
                border: true,
                frame: true            
            },
            items: products
    }
            
  
    
     var win = new Ext.Window({
         id:'metaform-win'
        ,layout:'fit'
        ,width: 800
        ,scope:this
        ,height: 600
        ,title: translation._('myorders_edit_js_Edit Order')
        ,items: new Ext.TabPanel({
                plain:true,
                activeTab: 0,
                id: 'editMainTabPanel',
                layoutOnTabChange:true,  
                items:[
                    tabPanelOrder,
                    tabPanelContact,
                    tabPanelProducts
                ]
            })
        });

    win.show();
});
