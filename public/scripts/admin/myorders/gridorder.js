Ext.ns('PrintShopCreator.Admin.MyOrder');

var translation = new Locale.Gettext();
translation.textdomain('MyOrder');

function renderStatus(data, cell, record, rowIndex, columnIndex, store){
         var value=record.get('status')
         //cell.css = "UnStyleCss"; //défini dans style.css
         cell.css = "status" + value; //Color's set here
         return data; //data = text
}

PrintShopCreator.Admin.MyOrder.Grid = Ext.extend(Ext.grid.GridPanel, {
     
    border:false
    ,layout: 'fit'
    ,stateful:true
    ,url:'/admin/myorders/all?config=1'
    ,objName:'contact'
    ,idName:'id'
    ,frame: false
    ,autoHeight: true
    ,autoWidth: true
    ,id: 'myordergrid'
    ,initComponent:function() {
        
        // create row actions
        this.rowActions = new Ext.ux.grid.RowActions({
             actions:[{
                 iconCls:'icon-edit-record'
                ,qtip: translation._('myorders_gridorder_js_Edit')
            },{
                 iconCls:'icon-upload-record'
                ,qtip: translation._('myorders_gridorder_js_Uploadcenter')
            }]
            ,widthIntercept:Ext.isSafari ? 4 : 2
            ,id:'actions'
        });
        this.rowActions.on('action', this.onRowAction, this);

        Ext.apply(this, {
            // {{{
            store:new Ext.data.Store({
                reader:new Ext.data.JsonReader({
                     id:'id'
                    ,totalProperty:'totalCount'
                    ,root:'rows'
                    ,fields:[
                         {name:'id', type:'int'}
                        ,{name:'alias', type:'string'}
                        ,{name:'created', type:'date', dateFormat: 'Y-m-d H:i:s'}
                        ,{name:'account', type:'string'}
                        ,{name:'contact', type:'string'}
                        ,{name:'status', type:'string'}
                     
                    ]
                })
                ,proxy:new Ext.data.HttpProxy({url:this.url})
                ,baseParams:{cmd:'getData', objName:this.objName}
                ,sortInfo:{field:'created', direction:'DESC'}
                ,remoteSort:true
            })
            // }}}
            // {{{
            ,columns:[{
                 header: translation._('myorders_gridorder_js_Alias')
                ,id:'alias'
                ,dataIndex:'alias'
                ,width:160
                ,sortable:true
                
            },{
                 header: translation._('myorders_gridorder_js_Date')
                ,id:'created'
                ,dataIndex:'created'
                ,width:160
                ,sortable:true
                ,renderer: Ext.util.Format.dateRenderer('d.m.Y')
            },{
                 header: translation._('myorders_gridorder_js_Contact')
                ,id:'contact'
                ,dataIndex:'contact'
                ,width:100
                ,sortable:true
                
            },{
                 header: translation._('myorders_gridorder_js_Account')
                ,id:'account'
                ,dataIndex:'account'
                ,width:100
                ,sortable:true
                
            },{
                 header: translation._('myorders_gridorder_js_Status')
                ,id:'status'
                ,dataIndex:'status'
                ,width:100
                ,sortable:true
                ,renderer: renderStatus
            }, this.rowActions]
            // }}}
            ,plugins:[this.rowActions]
            ,viewConfig:{forceFit:true}
           
        }); // eo apply

        this.bbar = new Ext.PagingToolbar({
            store:this.store
            ,displayInfo: true
            ,plugins: [new Ext.ux.ProgressBarPager()]
            ,pageSize:10
        });

        // call parent
        PrintShopCreator.Admin.MyOrder.Grid.superclass.initComponent.apply(this, arguments);
    } // eo function initComponent
    // {{{
    ,onRender:function() {
        // call parent
        PrintShopCreator.Admin.MyOrder.Grid.superclass.onRender.apply(this, arguments);

        // load store
        this.store.load({params:{start:0,limit:10}});

    } // eo function onRender
    // }}}

    ,onRowAction:function(grid, record, action, row, col) {
        switch(action) {
        
            case 'icon-upload-record':
               PrintShopCreator.Base.Common.openWindow('uploadWindow', '/admin/upload/all?uid=' + record.id, 800, 600);
            break;
            
            case 'icon-minus':
                this.deleteRecord(record);
            break;

            case 'icon-edit-record':
                PrintShopCreator.Base.Common.openWindow('contactWindow', '/admin/myorders/edit?uid=' + record.id, 800, 600);
            break;
        }
    } // eo onRowAction
    
    ,showError:function(msg, title) {
        Ext.Msg.show({
             title:title || 'Error'
            ,msg:Ext.util.Format.ellipsis(msg, 2000)
            ,icon:Ext.Msg.ERROR
            ,buttons:Ext.Msg.OK
            ,minWidth:1200 > String(msg).length ? 360 : 600
        });
    } // eo function showError

    ,deleteRecord:function(record) {
        Ext.Msg.show({
             title: translation._('msg_delete')
            ,msg: translation._('msg_delete_confirm_beforetext') + ':<br><b>' + record.get('alias') + '</b><br>' + translation._('msg_delete_confirm_aftertext')
            ,icon:Ext.Msg.QUESTION
            ,buttons:Ext.Msg.YESNO
            ,scope:this
            ,fn:function(response) {
                if('yes' == response) {
                     try {
                        var req = new XMLHttpRequest;
                    
                        req.open('POST', '/admin/order/edit?config=5' + '&uid=' + record.id, false);
                        req.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
                        req.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
                        req.send('&delete=1');
                        if (req.status == 200 || req.status == 304 || req.status == 0 || req.status == null) {
                          return Ext.util.JSON.decode(req.responseText);
                        }
                      } catch (e) {
                        return '';
                      }
                }else{
                    return;
                }
//              console.info('Deleting record');
            }
        });
    } // eo function deleteRecord

}); // eo extend

// register xtype
Ext.reg('PrintShopCreatorAdminMyOrderGrid', PrintShopCreator.Admin.MyOrder.Grid);
Ext.QuickTips.init();
    
    var MyOrder = new Ext.FormPanel({
                       
        renderTo: 'orderindex',
        closable: false,
        resizable: false,
        layout: 'fit',
        border: false,
        items:[{xtype:'PrintShopCreatorAdminMyOrderGrid', id:'PrintShopCreatorAdminMyOrderGrid'}]
});
MyOrder.show();
