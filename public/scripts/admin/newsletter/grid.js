// vim: sw=4:ts=4:nu:nospell:fdc=4
/**
 * Ext.ux.grid.RecordForm Plugin Example Application
 *
 * @author    Ing. Jozef Sakáloš
 * @copyright (c) 2008, by Ing. Jozef Sakáloš
 * @date      31. March 2008
 * @version   $Id: recordform.js 187 2008-04-16 00:03:34Z jozo $

 * @license recordform.js is licensed under the terms of
 * the Open Source LGPL 3.0 license.  Commercial use is permitted to the extent
 * that the code/component(s) do NOT become part of another Open Source or Commercially
 * licensed development library or toolkit without explicit permission.
 * 
 * License details: http://www.gnu.org/licenses/lgpl.html
 */

/*global Ext, Web, Example */

Ext.ns('PrintShopCreator');

var translation = new Locale.Gettext();
translation.textdomain('App');

function renderStatus(data, cell, record, rowIndex, columnIndex, store){
         var value=record.get('status');
         //cell.css = "UnStyleCss"; //défini dans style.css
         cell.css = "status" + value; //Color's set here
         return data; //data = text
}
PrintShopCreator.NewsletterGrid = Ext.extend(Ext.grid.GridPanel, {
	 
	border:false
	,layout: 'fit'
	,stateful:true
	,url:'/admin/newsletter/fetchnewsletter/format/json?sid=' + sid
	,objName:'Newsletter'
	,idName:'id'
	,frame: false
	,autoHeight: true
	,autoWidth: true
,id: 'newslettergrid'
	,initComponent:function() {
		

		// create row actions
		this.rowActions = new Ext.ux.grid.RowActions({
			 actions:[{
				 iconCls:'icon-arrow-up'
				,qtip: translation._('Newsletter_gridNewsletter_js_Test')
			},{
				 iconCls:'icon-arrow-divide'
				,qtip: translation._('Newsletter_gridNewsletter_js_Send')
			},{
				 iconCls:'icon-minus'
				,qtip: translation._('Newsletter_gridNewsletter_js_Delete Page')
			},{
				 iconCls:'icon-edit-record'
				,qtip: translation._('Newsletter_gridNewsletter_js_Edit Page')
			}]
			,widthIntercept:Ext.isSafari ? 4 : 2
			,id:'actions'
		});
		this.rowActions.on('action', this.onRowAction, this);

		Ext.apply(this, {
			// {{{
			store:new Ext.data.Store({
				reader:new Ext.data.JsonReader({
					 id:'id'
					,totalProperty:'totalCount'
					,root:'rows'
					,fields:[
						 {name:'id', type:'int'}
						,{name:'title', type:'string'}
                                                ,{name:'subject', type:'string'}
                                                ,{name:'uuid', type:'string'}
					]
				})
				,proxy:new Ext.data.HttpProxy({url:this.url})
				,baseParams:{cmd:'getData', objName:this.objName}
				,sortInfo:{field:'title', direction:'ASC'}
				,remoteSort:true
			})
			// }}}
			// {{{
			,columns:[{
				 header: translation._('Newsletter_gridNewsletter_js_Title')
				,id:'title'
				,dataIndex:'title'
				,width:160
				,sortable:true
				,editor:new Ext.form.TextField({
					allowBlank:false
				})
			},{
				 header: translation._('Newsletter_gridNewsletter_js_Subject')
				,id:'subject'
				,dataIndex:'subject'
				,width:160
				,sortable:true
				,editor:new Ext.form.TextField({
					allowBlank:false
				})
			}, this.rowActions]
			// }}}
			,plugins:[this.rowActions]
			,viewConfig:{forceFit:true}			
			,tbar:[{
			//,plugins:[new Ext.ux.grid.Search({
			//	iconCls:'icon-zoom'
			//	,readonlyIndexes:['note']
			//	,disableIndexes:['pctChange']
			//}), this.rowActions]
			//,viewConfig:{forceFit:true}
			//,tbar:[{
				 text: translation._('Newsletter_gridNewsletter_js_addNewsletter')
				,iconCls:'icon-form-add'
				,listeners:{
					click:{scope:this, buffer:200, fn:function(btn) {
						PrintShopCreator.Base.Common.openWindow('NewsletterWindow', '/admin/newsletter/addnewsletter?sid='+ sid, 800, 600);
					}}
				}
			}]
		}); // eo apply

		this.bbar = new Ext.PagingToolbar({
			 store:this.store
			,displayInfo: true
			,plugins: [new Ext.ux.ProgressBarPager()]
			,pageSize:20
		});

		// call parent
		PrintShopCreator.NewsletterGrid.superclass.initComponent.apply(this, arguments);
	} // eo function initComponent
	// {{{
	,onRender:function() {
		// call parent
		PrintShopCreator.NewsletterGrid.superclass.onRender.apply(this, arguments);

		// load store
		this.store.load({params:{start:0,limit:20}});

	} // eo function onRender
	// }}}

	,addRecord:function() {
		var store = this.store;
		if(store.recordType) {
			var rec = new store.recordType({newRecord:true});
			
			return rec;
		}
		return false;
	} // eo function addRecord

	,onRowAction:function(grid, record, action, row, col) {
		switch(action) {
			case 'icon-arrow-up':
				this.testSend(record);
			break;
                        case 'icon-arrow-divide':
				this.send(record);
			break;
                        case 'icon-minus':
				this.deleteRecord(record);
			break;
                        case 'icon-edit-record':
				
				PrintShopCreator.Base.Common.openWindow('articleWindow', '/admin/newsletter/addnewsletter?sid='+ sid + '&uid=' + record.data.id, 800, 600);
			break;
		}
	} // eo onRowAction

	,showError:function(msg, title) {
		Ext.Msg.show({
			 title:title || 'Error'
			,msg:Ext.util.Format.ellipsis(msg, 2000)
			,icon:Ext.Msg.ERROR
			,buttons:Ext.Msg.OK
			,minWidth:1200 > String(msg).length ? 360 : 600
		});
	} // eo function showError

	,deleteRecord:function(record) {
		Ext.Msg.show({
			 title: translation._('msg_delete')
			,msg: translation._('msg_delete_confirm_beforetext') + ':<br><b>' + record.get('title') + '</b><br>' + translation._('msg_delete_confirm_aftertext')
			,icon:Ext.Msg.QUESTION
			,buttons:Ext.Msg.YESNO
			,scope:this
			,fn:function(response) {
				if('yes' == response) {
					 try {
					    var req = new XMLHttpRequest;

					    req.open('POST', '/admin/newsletter/deletemotivtheme/format/json?uid=' + record.id, false);
					    req.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
					    req.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
					    req.send('&delete=1');
					    if (req.status == 200 || req.status == 304 || req.status == 0 || req.status == null) {
                          this.store.load({params:{start:0,limit:20}});
					      return Ext.util.JSON.decode(req.responseText);
					    }
					  } catch (e) {
					    return '';
					  }
				}else{
				    return;
				}
//				console.info('Deleting record');
			}
		});
	} // eo function deleteRecord

        ,testSend:function(record) {
		Ext.Msg.show({
			 title: translation._('msg_newsletter_send_test_title')
			,msg: translation._('msg_newsletter_send_beforetext') + ':<br><b>' + record.get('title') + '</b><br>' + translation._('msg_newsletter_send_aftertext')
			,icon:Ext.Msg.QUESTION
			,buttons:Ext.Msg.YESNO
			,scope:this
			,fn:function(response) {
				if('yes' == response) {
					 try {
					    var req = new XMLHttpRequest;

					    req.open('POST', '/admin/newsletter/sendtest/format/json?uid=' + record.id, false);
					    req.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
					    req.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
					    req.send('&delete=1');
					    if (req.status == 200 || req.status == 304 || req.status == 0 || req.status == null) {
                                                this.store.load({params:{start:0,limit:20}});
                                                return Ext.util.JSON.decode(req.responseText);
					    }
					  } catch (e) {
					    return '';
					  }
				}else{
				    return;
				}
//				console.info('Deleting record');
			}
		});
	} // eo function deleteRecord

        ,send:function(record) {
		Ext.Msg.show({
			 title: translation._('msg_newsletter_send_title')
			,msg: translation._('msg_newsletter_send_beforetext') + ':<br><b>' + record.get('title') + '</b><br>' + translation._('msg_newsletter_send_aftertext')
			,icon:Ext.Msg.QUESTION
			,buttons:Ext.Msg.YESNO
			,scope:this
			,fn:function(response) {
				if('yes' == response) {
					 try {
					    var req = new XMLHttpRequest;

					    req.open('POST', '/admin/newsletter/send/format/json?uid=' + record.id, false);
					    req.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
					    req.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
					    req.send('&delete=1');
					    if (req.status == 200 || req.status == 304 || req.status == 0 || req.status == null) {
                                                this.store.load({params:{start:0,limit:20}});
                                                return Ext.util.JSON.decode(req.responseText);
					    }
					  } catch (e) {
					    return '';
					  }
				}else{
				    return;
				}
//				console.info('Deleting record');
			}
		});
	} // eo function deleteRecord

}); // eo extend

// register xtype
Ext.reg('Newslettergrid', PrintShopCreator.NewsletterGrid);

Ext.QuickTips.init();
	
	var order = new Ext.FormPanel({
		               
        renderTo: 'newsletterindex',
        closable: false,
        resizable: false,
        layout: 'fit',
        border: false,
        items:[{xtype:'Newslettergrid', id:'Newslettergrid'}]
});
order.show();
