Ext.onReady(function() {


    Ext.QuickTips.init();

    var translation = new Locale.Gettext();
    translation.textdomain('App');

    var win = new Ext.Window({
         id:'metaform-win'
        ,layout:'fit'
        ,width: 800
        ,scope:this
        ,height: 600
        ,title: translation._('theme_edit_js_Newsletter')
        ,items:{
             xtype:'metaform'
            ,url:'/admin/newsletter/loadsavenewsletter/format/json?sid='+ sid + '&uid=' + uid
            ,buttons:
            [{
                 text: translation._('news_edit_js_Save')
                ,iconCls: 'action_applyChanges'
                ,listeners:{
                   click:{scope:this, buffer:200, fn:function(btn) {
	                   Ext.getCmp('metaform-win').items.get(0).getForm().submit({waitMsg: translation._('news_edit_js_Saving')
                               ,url: '/admin/newsletter/loadsavenewsletter/format/json?sid='+ sid + '&uid=' + uid + '&save=1'
                                ,success: function(form, action){
                                      if(action.result.isNew) {
                                            window.location = '/admin/newsletter/addnewsletter?sid='+ sid + '&uid=' + action.result.uid;
                                      }
                                }});
                }}}
            },{
                  text: translation._('news_edit_js_Save and Close')
                ,iconCls: 'action_applyChangesClose'
                ,listeners:{
                   click:{scope:this, buffer:200, fn:function(btn) {
	                   Ext.getCmp('metaform-win').items.get(0).getForm().submit({waitMsg: translation._('news_edit_js_Saving')
	                   , url: '/admin/newsletter/loadsavenewsletter/format/json?sid='+ sid + '&uid=' + uid + '&save=1'
	                   ,success: function(form, action){
				                  window.close();
				                }
	                   
	                   });
                }}}
            },{
                text: translation._('news_edit_js_Close'),
                listeners:{
                   click:{scope:this, buffer:200, fn:function(btn) {
                        window.close();
                }}},
                iconCls: 'action_cancel'
            }]
        }
    });

    win.show();
});