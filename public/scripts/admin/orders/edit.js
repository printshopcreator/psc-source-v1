Ext.onReady(function() {


    Ext.QuickTips.init();

    var translation = new Locale.Gettext();
    translation.textdomain('Contact');

    var url = '/admin/orders/edit?config=3&sid='+ sid + '&uid=' + uid;

    var products = new Ext.grid.GridPanel({
        autoWidth: true,
        id: 'products_grid',
        height: 700,
        sm: new Ext.grid.RowSelectionModel({
	                singleSelect: true,
	                listeners: {
	                    rowselect: function(sm, row, rec) {
                            rec.data.options = rec.data.options.replace('|','\n');
	                        Ext.getCmp("company-form").getForm().loadRecord(rec);
	                    }
	                }
	            }),
        store: new Ext.data.Store({
            reader:new Ext.data.JsonReader({
                     id:'id'
                    ,totalProperty:'totalCount'
                    ,root:'rows'
                    ,fields:[
                         {name:'id', type:'int'}
                        ,{name:'articlename', type:'string'}
                        ,{name:'special', type:'boolean'}
                        ,{name:'count', type:'string'}
                        ,{name:'status', type:'int'}
                        ,{name:'status_title', type:'string'}
                        ,{name:'priceone', type:'string'}
                        ,{name:'priceall', type:'string'}
                        ,{name:'options', type:'string'}
                        ,{name:'special', type:'boolean'}
                        ,{name:'pdf', type:'string'}
                    ]
                })
                ,proxy:new Ext.data.HttpProxy({url:url})
                ,baseParams:{cmd:'getData', objName:'product'}
                ,sortInfo:{field:'id', direction:'ASC'}
                ,remoteSort:true
        }),
        cm: new Ext.grid.ColumnModel([
            {id:'article', header: translation._('orders_edit_js_Productame'), width: 20, sortable: true, dataIndex: 'articlename'},
            {header: translation._('orders_edit_js_Special'), width: 10, sortable: true, dataIndex: 'special',renderer: PrintShopCreator.Base.Common.renderBool},
            {header: translation._('orders_edit_js_Count'), width: 10, sortable: true, dataIndex: 'count'},
            {header: translation._('orders_edit_js_Price one'), width: 10, sortable: true, dataIndex: 'priceone'},
            {header: translation._('orders_edit_js_Price all'), width: 10, sortable: true, dataIndex: 'priceall'},
            {header: translation._('Status'), width: 10, sortable: true, dataIndex: 'status_title'},
            {header: translation._('Pdf'), width: 10, sortable: true, dataIndex: 'pdf'}
        ]),
        viewConfig: {
            forceFit:true
        },
        collapsible: true,
        animCollapse: false,
        iconCls: 'icon-grid'
    });
    
    products.store.load();

    var tabPanelOrder = {
            title: translation._('orders_edit_js_Overview'),
            id: 'overview',
            layout:'form',
            height: 700,
            width: 400,
            layoutOnTabChange:true,
            defaults: {
                border: true,
                frame: true            
            },
            items: {
                xtype:'metaform'
                ,height: 700
                ,url:'/admin/orders/edit?config=4&sid='+ sid + '&uid=' + uid
                
            }
            ,buttons: [{
                         text: translation._('orders_edit_js_Save')
                        ,iconCls: 'action_applyChanges'
                        ,listeners:{
                           click:{scope:this, buffer:200, fn:function(btn) {
                               Ext.getCmp('overview').items.get(0).getForm().submit({waitMsg: translation._('orders_edit_js_Saving'), url: '/admin/orders/edit?config=5&sid='+ sid + '&uid=' + uid + '&save=1'});
                        }}}
                    },{
                          text: translation._('article_edit_js_Save and Close')
                        ,iconCls: 'action_applyChangesClose'
                        ,listeners:{
                           click:{scope:this, buffer:200, fn:function(btn) {
                               Ext.getCmp('overview').items.get(0).getForm().submit(
                                       {waitMsg: translation._('article_edit_js_Saving')
                                       ,url: '/admin/orders/edit?config=5&sid='+ sid + '&uid=' + uid + '&save=1'
                                       ,success: function(form, action){
                                                              window.close();
                                                    }
                                   });
                        }}}
                    },{
                        text: translation._('article_edit_js_Close'),
                        listeners:{
                           click:{scope:this, buffer:200, fn:function(btn) {
                                window.close();
                        }}},
                        iconCls: 'action_cancel'
                    }]
    }


    var tabPanelContact = {
            title: translation._('orders_edit_js_Contact'),
            layout:'form',
            id: 'contact',
            height: 700,
            layoutOnTabChange:true,
            defaults: {
                border: true,
                frame: true            
            },
            
            items: {
                xtype:'metaform'
                ,height: 700
	            ,url:'/admin/orders/edit?config=5&sid='+ sid + '&uid=' + uid
	            
            }
            ,buttons:
                    [{
                         text: translation._('orders_edit_js_Save')
                        ,iconCls: 'action_applyChanges'
                        ,listeners:{
                           click:{scope:this, buffer:200, fn:function(btn) {
                               Ext.getCmp('contact').items.get(0).getForm().submit({waitMsg: translation._('orders_edit_js_Saving'), url: '/admin/orders/edit?config=5&sid='+ sid + '&uid=' + uid + '&save=1'});
                        }}}
                    }]
    }
    
    var tabPanelProducts = new Ext.FormPanel({
            title: translation._('orders_edit_js_Products'),
            layout:'column',
            id: 'company-form',

            height: 700,
            layoutOnTabChange:true,
            defaults: {
                border: true,
                frame: true            
            },
            items: [{
                columnWidth: 0.6,
                layout: 'fit',
                items: products
            },{
                columnWidth: 0.4,
                xtype: 'fieldset',
                labelWidth: 90,
                
                title: translation._('orders_edit_js_product_details'),
                defaults: {width: 140},	// Default config options for child items
                defaultType: 'textfield',
                autoHeight: true,
                bodyStyle: Ext.isIE ? 'padding:0 0 5px 15px;' : 'padding:10px 15px;',
                border: false,
                style: {
                    "margin-left": "10px", // when you add custom margin in IE 6...
                    "margin-right": Ext.isIE6 ? (Ext.isStrict ? "-10px" : "-13px") : "0"  // you have to adjust for it somewhere else
                },
                items: [{
                    fieldLabel: translation._('Status'),
                    name: 'status_title',
                    store: new Ext.data.JsonStore({
                    url: '/service/?type=get&mode=system&service=status_pos',
                    root: 'items',
                    fields: ['name', 'label']
                    }),
                    mode: 'locale',
                    triggerAction: 'all',
                    selectOnFocus:true,
                    valueField:'name',
                    displayField:'label',
                    xtype: 'combo',
                    listeners:{
                        'select': function() {
                            var record = Ext.getCmp("products_grid").getSelectionModel().getSelected().id;
                            var newstatus = this.getValue();
                            Ext.Ajax.request({
                               url: '/admin/orders/changeposstatus/format/json',
                               params: { uuid: record, newstatus: newstatus }
                            });
                         }
                    }
                },{
                    fieldLabel: translation._('orders_edit_js_product_options'),
                    name: 'options',
                    xtype: 'textarea',
                    width: 175,
                    height: 150
                }]
            }]
    })
            
  
    
     var win = new Ext.Window({
         id:'metaform-win'
        ,layout:'fit'
        ,width: 900
        ,scope:this
        
        ,height: 700
        ,title: translation._('orders_edit_js_Edit Order')
        ,items: new Ext.TabPanel({
                plain:true,
                activeTab: 0,
                id: 'editMainTabPanel',
                layoutOnTabChange:true,  
                items:[
                    tabPanelOrder,
                    tabPanelContact,
                    tabPanelProducts
                ]
            })
          ,tbar: [
                            {
                                text: translation._('orders_edit_js_Offer'),
                                tooltip: translation._('orders_edit_js_Print Offer'),
                                iconCls:'print',
                                handler: function() {
                                    PrintShopCreator.Base.Common.openWindow('printWindow', '/service/?type=print&mode=offer&uid=' + uid, 200, 100);
                                }
                            }, '-',
                            {
                                text: translation._('orders_edit_js_Order'),
                                tooltip: translation._('orders_edit_js_Print Order'),
                                handler: function() {
                                    PrintShopCreator.Base.Common.openWindow('printWindow', '/service/?type=print&mode=order&uid=' + uid, 200, 100);
                                },
                                iconCls:'print'
                            }, '-',
                            {
                                text: translation._('orders_edit_js_Invoice'),
                                tooltip: translation._('orders_edit_js_Print Invoice'),
                                handler: function() {
                                    PrintShopCreator.Base.Common.openWindow('printWindow', '/service/?type=print&mode=invoice&uid=' + uid, 200, 100);
                                },
                                iconCls:'print'
                            }, '-',
                            {
                                text: translation._('orders_edit_js_delivery ticket'),
                                tooltip: translation._('orders_edit_js_Print delivery ticket'),
                                handler: function() {
                                    PrintShopCreator.Base.Common.openWindow('printWindow', '/service/?type=print&mode=delivery&uid=' + uid, 200, 100);
                                },
                                iconCls:'print'
                            }, '-',
                            {
                                text: translation._('orders_edit_js_Jobticket'),
                                tooltip: translation._('orders_edit_js_Print Jobticket'),
                                handler: function() {
                                    PrintShopCreator.Base.Common.openWindow('printWindow', '/service/?type=print&mode=jobtiket&uid=' + uid, 200, 100);
                                },
                                iconCls:'print'
                            }, '-',
                            {
                                text: translation._('Label'),
                                tooltip: translation._('Label'),
                                handler: function() {
                                    PrintShopCreator.Base.Common.openWindow('labelWindow', '/service/?type=print&mode=label&uid=' + uid, 200, 100);
                                },
                                iconCls:'print'
                            }, '-',
                             {
                                 text: translation._('Storno'),
                                 tooltip: translation._('Storno'),
                                 handler: function() {
                                     PrintShopCreator.Base.Common.openWindow('labelWindow', '/service/?type=print&mode=storno&uid=' + uid, 200, 100);
                                 },
                                 iconCls:'print'
                             }, '-',
                            {
                                text: translation._('Package'),
                                tooltip: translation._('Package'),
                                handler: function() {
                                    PrintShopCreator.Base.Common.openWindow('labelWindow', '/service/index/getzip?uid=' + uid, 200, 100);
                                },
                                iconCls:'print'
                            }
                            
                        ]
        });

    win.show();
});
