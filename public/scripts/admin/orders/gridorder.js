Ext.ns('PrintShopCreator.Admin.Order');

var translation = new Locale.Gettext();
translation.textdomain('Order');

function renderStatus(data, cell, record, rowIndex, columnIndex, store){
         var value=record.get('status')
         //cell.css = "UnStyleCss"; //défini dans style.css
         cell.css = "status" + value; //Color's set here
         return data; //data = text
}

PrintShopCreator.Admin.Order.Grid = Ext.extend(Ext.grid.GridPanel, {
     
    border:false
    ,layout: 'fit'
    ,stateful:true
    ,url:'/admin/account/all?config=1&sid=' + sid + '&market=' + market
    ,objName:'contact'
    ,idName:'id'
    ,frame: false
	,stripeRows: true
    ,autoHeight: true
    ,autoWidth: true
    ,columnsText: 'Spalten'
    ,id: 'contactgrid'
    ,initComponent:function() {

       
        // create row actions
        this.rowActions = new Ext.ux.grid.RowActions({
             actions:[{
                 iconCls:'icon-go-tab'
                ,qtip: translation._('orders_gridorder_js_Filter')
            },{
                 iconCls:'icon-edit-record'
                ,qtip: translation._('orders_gridorder_js_Edit')
            },{
                 iconCls:'icon-upload-record'
                 ,qtip: translation._('orders_gridorder_js_Uploadcenter')
             },{
                 iconCls:'icon-download-package'
                ,qtip: translation._('orders_gridorder_js_Download_Package')
            }]
            ,id:'actions'
        });
        this.rowActions.on('action', this.onRowAction, this);

        var filters = new Ext.ux.grid.GridFilters({
          filters:[
            {type: 'string',  dataIndex: 'alias'},
            {type: 'string',  dataIndex: 'c/self_firstname'},
            {type: 'string',  dataIndex: 'c/self_lastname'},
            {type: 'date',  dataIndex: 'created'},
            {
                type: 'list',
                dataIndex: 'status',
                store : new Ext.data.Store({
                    proxy:  new Ext.data.HttpProxy({ url: '/service/?type=get&mode=system&service=status', method: 'POST' }),
                    reader: new Ext.data.JsonReader({ totalProperty: 'count', root: 'items' },
                    [
                        { name: 'id', type: 'string', mapping: 'name' },
                        { name: 'label', type: 'string', mapping: 'label' }
                    ])
                    ,labelField: 'name'
                })
                ,phpMode: true
                ,labelField: 'label'
            },
            {
                type: 'list',
                dataIndex: 'shop_id',
                store : new Ext.data.Store({
                    proxy:  new Ext.data.HttpProxy({ url: '/service/?type=get&mode=system&service=shops', method: 'POST' }),
                    reader: new Ext.data.JsonReader({ totalProperty: 'count', root: 'items' },
                    [
                        { name: 'id', type: 'string', mapping: 'name' },
                        { name: 'label', type: 'string', mapping: 'label' }
                    ])
                    ,labelField: 'name'
                })
                ,phpMode: true
                ,labelField: 'label'
            },
            {dataIndex:'enable', type:'boolean', defaultValue: true, value: true}

        ]});

        Ext.apply(this, {
            // {{{
            store:new Ext.data.Store({
                reader:new Ext.data.JsonReader({
                     id:'id'
                    ,totalProperty:'totalCount'
                    ,root:'rows'
                    ,fields:[
                         {name:'id', type:'int'}
                        ,{name:'alias', type:'string'}
                        ,{name:'created', type:'date', dateFormat: 'Y-m-d H:i:s'}
                        ,{name:'account', type:'string'}
                        ,{name:'c/self_firstname', type:'string'}
                        ,{name:'c/self_lastname', type:'string'}
                        ,{name:'c/self_department', type:'string'}
                        ,{name:'status', type:'string'}
                        ,{name:'basketfield2', type:'string'}
                        ,{name:'shop_id', type:'string'}
                        ,{name:'enable', type:'boolean'}
                     
                    ]
                })
                ,proxy:new Ext.data.HttpProxy({url:this.url})
                ,baseParams:{cmd:'getData', objName:this.objName}
                ,sortInfo:{field:'created', direction:'DESC'}
                ,remoteSort:true
            })



            // }}}
            // {{{
            ,columns:[{
                 header: translation._('orders_gridorder_js_Alias')
                ,id:'alias'
                ,dataIndex:'alias'
                ,width:140
                ,sortable:true
                
            },{
                 header: translation._('orders_gridorder_js_Date')
                ,id:'created'
                ,dataIndex:'created'
                ,width:140
                ,sortable:true
                ,renderer: Ext.util.Format.dateRenderer('d.m.Y')
            },{
                 header: translation._('Vorname')
                ,id:'self_firstname'
                ,dataIndex:'c/self_firstname'
                ,width:90
                ,sortable:true
                
            },{
                 header: translation._('Nachname')
                ,id:'self_lastname'
                ,dataIndex:'c/self_lastname'
                ,width:90
                ,sortable:true
                
            },{
                 header: translation._('Firma')
                ,id:'self_department'
                ,dataIndex:'c/self_department'
                ,width:90
                ,sortable:true
                
            },{
                 header: translation._('orders_gridorder_js_Account')
                ,id:'account'
                ,dataIndex:'account'
                ,width:90
                ,sortable:false
                
            },{
                 header: translation._('orders_gridorder_js_Shop')
                ,id:'shop_id'
                ,dataIndex:'shop_id'
                ,width:90
                ,sortable:true
            },{
                 header: translation._('orders_gridorder_js_Status')
                ,id:'status'
                ,dataIndex:'status'
                ,width:90
                ,sortable:true
                ,renderer: renderStatus
            },{
				 header: translation._('article_index_js_Enable')
				,id:'enable'
				,dataIndex:'enable'
				,width:40
				,sortable:true
				,renderer: PrintShopCreator.Base.Common.renderBool
			},{
				 header: translation._('Ref')
					,id:'basketfield2'
					,dataIndex:'basketfield2'
					,width:130
					,sortable:true
				}, this.rowActions]
            // }}}
            ,plugins:[this.rowActions,filters]
            ,viewConfig:{forceFit:true}
           
        }); // eo apply

        this.bbar = new Ext.PagingToolbar({
            store:this.store
            ,pageSize:20
            ,displayInfo: true
            ,plugins: [filters,new Ext.ux.ProgressBarPager()]
        });

        // call parent
        PrintShopCreator.Admin.Order.Grid.superclass.initComponent.apply(this, arguments);
    } // eo function initComponent
    // {{{
    ,onRender:function() {
        // call parent
        PrintShopCreator.Admin.Order.Grid.superclass.onRender.apply(this, arguments);

        // load store
        this.store.load({params:{start:0,limit:20}});

    } // eo function onRender
    // }}}

    ,onRowAction:function(grid, record, action, row, col) {
        switch(action) {
        
            case 'icon-go-tab':
                var grid1 = Ext.getCmp('PrintShopCreatorAdminContactGrid');
                grid1.store.reload({params:{start:0,limit:10, contact_id: record.id}});
                var grid3 = Ext.getCmp('PrintShopCreatorAdminAccountGrid');
                grid3.store.reload({params:{start:0,limit:10, contact_id: record.id}});
                
            break;
            
            case 'icon-minus':
                this.deleteRecord(record);
            break;
            case 'icon-upload-record':
               PrintShopCreator.Base.Common.openWindow('uploadWindow', '/admin/upload/all?sid='+ sid + '&uid=' + record.id, 960, 680);
            break;
            case 'icon-edit-record':
                PrintShopCreator.Base.Common.openWindow('contactWindow', '/admin/orders/edit?sid='+ sid + '&uid=' + record.id, 960, 680);
            break;
            case 'icon-download-package':
                PrintShopCreator.Base.Common.openWindow('contactWindow', '/service/index/getzip?uid=' + record.id, 960, 680);
            break;
        }
    } // eo onRowAction
    
    ,showError:function(msg, title) {
        Ext.Msg.show({
             title:title || 'Error'
            ,msg:Ext.util.Format.ellipsis(msg, 2000)
            ,icon:Ext.Msg.ERROR
            ,buttons:Ext.Msg.OK
            ,minWidth:1200 > String(msg).length ? 360 : 600
        });
    } // eo function showError

    ,deleteRecord:function(record) {
        Ext.Msg.show({
			      title: translation._('msg_delete')
			      ,msg: translation._('msg_delete_confirm_beforetext') + ':<br><b>' + record.get('alias') + '</b><br>' + translation._('msg_delete_confirm_aftertext')
            ,icon:Ext.Msg.QUESTION
            ,buttons:Ext.Msg.YESNO
            ,scope:this
            ,fn:function(response) {
                if('yes' == response) {
                     try {
                        var req = new XMLHttpRequest;
                    
                        req.open('POST', '/admin/orders/edit?config=5' + '&uid=' + record.id, false);
                        req.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
                        req.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
                        req.send('&delete=1');
                        if (req.status == 200 || req.status == 304 || req.status == 0 || req.status == null) {
                            this.store.load({params:{start:0,limit:10}});
                          return Ext.util.JSON.decode(req.responseText);
                        }
                      } catch (e) {
                        return '';
                      }
                }else{
                    return;
                }
//              console.info('Deleting record');
            }
        });
    } // eo function deleteRecord

}); // eo extend

// register xtype
Ext.reg('PrintShopCreatorAdminOrderGrid', PrintShopCreator.Admin.Order.Grid);
Ext.QuickTips.init();
    
    var order = new Ext.FormPanel({
                       
        renderTo: 'orderindex',
        closable: false,
        resizable: false,
        layout: 'fit',
        border: false,
        items:[{xtype:'PrintShopCreatorAdminOrderGrid', id:'PrintShopCreatorAdminOrderGrid'}]
});
order.show();
