Ext.ns('PrintShopCreator.Admin.ThemeOrder');

var translation = new Locale.Gettext();
translation.textdomain('Order');

function renderStatus(data, cell, record, rowIndex, columnIndex, store){
         var value=record.get('status')
         //cell.css = "UnStyleCss"; //défini dans style.css
         cell.css = "status" + value; //Color's set here
         return data; //data = text
}

PrintShopCreator.Admin.ThemeOrder.Grid = Ext.extend(Ext.grid.GridPanel, {
     
    border:false
    ,layout: 'fit'
    ,stateful:true
    ,url:'/admin/account/all?config=1&sid=' + sid + '&market=' + market
    ,objName:'contact'
    ,idName:'id'
    ,frame: false
    ,autoHeight: true
	,stripeRows: true
    ,autoWidth: true
    ,columnsText: 'Spalten'
    ,id: 'contactgrid'
    ,initComponent:function() {

       
        // create row actions
        this.rowActions = new Ext.ux.grid.RowActions({
             actions:[]
            ,widthIntercept:Ext.isSafari ? 4 : 2
            ,id:'actions'
        });
        this.rowActions.on('action', this.onRowAction, this);

        var filters = new Ext.ux.grid.GridFilters({
          filters:[
            {type: 'string',  dataIndex: 'd/alias'},
            {type: 'string',  dataIndex: 'c/self_firstname'},
            {type: 'date',  dataIndex: 'm/createdd'},
            {
                type: 'list',
                dataIndex: 'shop_id',
                store : new Ext.data.Store({
                    proxy:  new Ext.data.HttpProxy({ url: '/service/?type=get&mode=system&service=shops', method: 'POST' }),
                    reader: new Ext.data.JsonReader({ totalProperty: 'count', root: 'items' },
                    [
                        { name: 'id', type: 'string', mapping: 'name' },
                        { name: 'label', type: 'string', mapping: 'label' }
                    ])
                    ,labelField: 'name'
                })
                ,phpMode: true
                ,labelField: 'label'
            }

        ]});

        Ext.apply(this, {
            // {{{
            store:new Ext.data.Store({
                reader:new Ext.data.JsonReader({
                     id:'id'
                    ,totalProperty:'totalCount'
                    ,root:'rows'
                    ,fields:[
                         {name:'id', type:'int'}
                        ,{name:'d/alias', type:'string'}
                        ,{name:'m/createdd', type:'date', dateFormat: 'Y-m-d H:i:s'}
                        ,{name:'d/account', type:'string'}
                        ,{name:'c/self_firstname', type:'string'}
                        ,{name:'m/shop_id', type:'string'}
                        ,{name:'m/resale_price', type:'string'}
                     
                    ]
                })
                ,proxy:new Ext.data.HttpProxy({url:this.url})
                ,baseParams:{cmd:'getData', objName:this.objName}
                ,sortInfo:{field:'m.createdd', direction:'DESC'}
                ,remoteSort:true
            })



            // }}}
            // {{{
            ,columns:[{
                 header: translation._('orders_gridorder_js_Alias')
                ,id:'alias'
                ,dataIndex:'d/alias'
                ,width:160
                ,sortable:true
                
            },{
                 header: translation._('orders_gridorder_js_Date')
                ,id:'created'
                ,dataIndex:'m/createdd'
                ,width:160
                ,sortable:true
                ,renderer: Ext.util.Format.dateRenderer('d.m.Y')
            },{
                 header: translation._('orders_gridorder_js_Contact')
                ,id:'contact'
                ,dataIndex:'c/self_firstname'
                ,width:100
                ,sortable:true
                
            },{
                 header: translation._('orders_gridorder_js_Account')
                ,id:'account'
                ,dataIndex:'d/account'
                ,width:100
                ,sortable:true
                
            },{
                 header: translation._('orders_gridorder_js_Shop')
                ,id:'shop_id'
                ,dataIndex:'m/shop_id'
                ,width:100
                ,sortable:true
            },{
                 header: translation._('orders_gridorder_js_ResalePrice')
                ,id:'shop_id'
                ,dataIndex:'m/resale_price'
                ,width:100
                ,sortable:true
                ,renderer: Ext.util.Format.euMoney
            }]
            // }}}
            ,plugins:[filters]
            ,viewConfig:{forceFit:true}
           
        }); // eo apply

        this.bbar = new Ext.PagingToolbar({
            store:this.store
            ,pageSize:20
            ,displayInfo: true
            ,plugins: [filters,new Ext.ux.ProgressBarPager()]
        });

        // call parent
        PrintShopCreator.Admin.ThemeOrder.Grid.superclass.initComponent.apply(this, arguments);
    } // eo function initComponent
    // {{{
    ,onRender:function() {
        // call parent
        PrintShopCreator.Admin.ThemeOrder.Grid.superclass.onRender.apply(this, arguments);

        // load store
        this.store.load({params:{start:0,limit:20}});

    } // eo function onRender
    // }}}

    ,onRowAction:function(grid, record, action, row, col) {
       
    } // eo onRowAction
    
    ,showError:function(msg, title) {
        
    } // eo function showError

    

}); // eo extend

// register xtype
Ext.reg('PrintShopCreatorAdminThemeOrderGrid', PrintShopCreator.Admin.ThemeOrder.Grid);
Ext.QuickTips.init();
    
    var ordertheme = new Ext.FormPanel({
                       
        renderTo: 'orderindex',
        closable: false,
        resizable: false,
        layout: 'fit',
        border: false,
        items:[{xtype:'PrintShopCreatorAdminThemeOrderGrid', id:'PrintShopCreatorAdminThemeOrderGrid'}]
});
ordertheme.show();
