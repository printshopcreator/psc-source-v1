Ext.onReady(function() {


    Ext.QuickTips.init();

    var translation = new Locale.Gettext();
    translation.textdomain('App');

    var win = new Ext.Window({
         id:'metaform-win'
        ,layout:'fit'
        ,width: 800
        ,scope:this
        ,height: 600
        ,title: translation._('papier_edit_js_Papier')
        ,items:{
             xtype:'metaform'
            ,url:'/admin/papierdb/loadsavepapier/format/json?uid=' + uid
            ,buttons:
            [{
                 text: translation._('news_edit_js_Save')
                ,iconCls: 'action_applyChanges'
                ,listeners:{
                   click:{scope:this, buffer:200, fn:function(btn) {
	                   Ext.getCmp('metaform-win').items.get(0).getForm().submit({waitMsg: translation._('news_edit_js_Saving')
                               ,url: '/admin/papierdb/loadsavepapier/format/json?uid=' + uid + '&save=1'
                                ,success: function(form, action){
                                      if(action.result.isNew) {
                                            window.location = '/admin/papierdb/addpapier?uid=' + action.result.uid;
                                      }
                                }});
                }}}
            },{
                  text: translation._('news_edit_js_Save and Close')
                ,iconCls: 'action_applyChangesClose'
                ,listeners:{
                   click:{scope:this, buffer:200, fn:function(btn) {
	                   Ext.getCmp('metaform-win').items.get(0).getForm().submit({waitMsg: translation._('news_edit_js_Saving')
	                   , url: '/admin/papierdb/loadsavepapier/format/json?uid=' + uid + '&save=1'
	                   ,success: function(form, action){
				                  window.close();
				                }
	                   
	                   });
                }}}
            },{
                text: translation._('news_edit_js_Close'),
                listeners:{
                   click:{scope:this, buffer:200, fn:function(btn) {
                        window.close();
                }}},
                iconCls: 'action_cancel'
            }]
        }
    });

    win.show();
});