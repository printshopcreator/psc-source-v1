
Ext.ns('PrintShopCreator');

var translation = new Locale.Gettext();
translation.textdomain('App');

function renderStatus(data, cell, record, rowIndex, columnIndex, store){
         var value=record.get('status');
         //cell.css = "UnStyleCss"; //défini dans style.css
         cell.css = "status" + value; //Color's set here
         return data; //data = text
}
PrintShopCreator.PapierdbGrid = Ext.extend(Ext.grid.GridPanel, {
	 
	border:false
	,layout: 'fit'
	,stateful:true
	,url:'/admin/papierdb/fetchpapers/format/json'
	,objName:'papierdb'
	,idName:'id'
	,frame: false
	,autoHeight: true
	,autoWidth: true
,id: 'papierdbgrid'
,title: translation._('PapierDB_gridPapierDB_js_PapierDB')
	,initComponent:function() {
		

		// create row actions
		this.rowActions = new Ext.ux.grid.RowActions({
			 actions:[{
				 iconCls:'icon-minus'
				,qtip: translation._('PapierDB_gridProductThemes_js_Delete Page')
			},{
				 iconCls:'icon-edit-record'
				,qtip: translation._('PapierDB_gridProductThemes_js_Edit Page')
			}]
			,widthIntercept:Ext.isSafari ? 4 : 2
			,id:'actions'
		});
		this.rowActions.on('action', this.onRowAction, this);

		var filters = new Ext.ux.grid.GridFilters({
			  filters:[
				{type: 'string',  dataIndex: 'art_nr'},
				{type: 'string',  dataIndex: 'description_1'},
				{type: 'string',  dataIndex: 'description_2'}
			]});
		
		Ext.apply(this, {
			// {{{
			store:new Ext.data.Store({
				reader:new Ext.data.JsonReader({
					 id:'id'
					,totalProperty:'totalCount'
					,root:'rows'
					,fields:[
						 {name:'uuid', type:'string'}
						,{name:'art_nr', type:'string'}
						,{name:'description_1', type:'string'}
						,{name:'description_2', type:'string'}
						,{name:'grammatur', type:'string'}
						,{name:'preis', type:'string'}
					]
				})
				,proxy:new Ext.data.HttpProxy({url:this.url})
				,baseParams:{cmd:'getData', objName:this.objName}
				,sortInfo:{field:'art_nr', direction:'ASC'}
				,remoteSort:true
			})
			// }}}
			// {{{
			,columns:[{
						 header: translation._('papier_all_js_artnr')
							,id:'art_nr'
							,dataIndex:'art_nr'
							,width:160
							,sortable:true
						},{
				 header: translation._('papier_all_js_description_1')
				,id:'description_1'
				,dataIndex:'description_1'
				,width:160
				,sortable:true
			},{
				 header: translation._('papier_all_js_description_2')
					,id:'description_2'
					,dataIndex:'description_2'
					,width:160
					,sortable:true
				},{
					 header: translation._('papier_all_js_grammatur')
						,id:'grammatur'
						,dataIndex:'grammatur'
						,width:160
						,sortable:true
					},{
				 header: translation._('papier_all_js_preis')
					,id:'preis'
					,dataIndex:'preis'
					,width:100
					
				}, this.rowActions]
			// }}}
			,plugins:[this.rowActions, filters]
			,viewConfig:{forceFit:true}			
			,tbar:[{
			//,plugins:[new Ext.ux.grid.Search({
			//	iconCls:'icon-zoom'
			//	,readonlyIndexes:['note']
			//	,disableIndexes:['pctChange']
			//}), this.rowActions]
			//,viewConfig:{forceFit:true}
			//,tbar:[{
				 text: translation._('PapierDB_grid_js_add')
				,iconCls:'icon-form-add'
				,listeners:{
					click:{scope:this, buffer:200, fn:function(btn) {
						PrintShopCreator.Base.Common.openWindow('papierWindow', '/admin/papierdb/addpapier', 800, 600);
					}}
				}
			}]
		}); // eo apply

		this.bbar = new Ext.PagingToolbar({
			 store:this.store
			,displayInfo: true
			,plugins: [filters, new Ext.ux.ProgressBarPager()]
			,pageSize:20
		});

		// call parent
		PrintShopCreator.PapierdbGrid.superclass.initComponent.apply(this, arguments);
	} // eo function initComponent
	// {{{
	,onRender:function() {
		// call parent
		PrintShopCreator.PapierdbGrid.superclass.onRender.apply(this, arguments);

		// load store
		this.store.load({params:{start:0,limit:20}});

	} // eo function onRender
	// }}}

	,addRecord:function() {
		var store = this.store;
		if(store.recordType) {
			var rec = new store.recordType({newRecord:true});
			
			return rec;
		}
		return false;
	} // eo function addRecord

	,onRowAction:function(grid, record, action, row, col) {
		switch(action) {
			case 'icon-minus':
				this.deleteRecord(record);
			break;

			case 'icon-edit-record':
				
				PrintShopCreator.Base.Common.openWindow('papierWindow', '/admin/papierdb/addpapier?uid=' + record.get('uuid'), 800, 600);
			break;
		}
	} // eo onRowAction

	,showError:function(msg, title) {
		Ext.Msg.show({
			 title:title || 'Error'
			,msg:Ext.util.Format.ellipsis(msg, 2000)
			,icon:Ext.Msg.ERROR
			,buttons:Ext.Msg.OK
			,minWidth:1200 > String(msg).length ? 360 : 600
		});
	} // eo function showError

	,deleteRecord:function(record) {
		Ext.Msg.show({
			 title: translation._('msg_delete')
			,msg: translation._('msg_delete_confirm_beforetext') + ':<br><b>' + record.get('description_1') + '</b><br>' + translation._('msg_delete_confirm_aftertext')
			,icon:Ext.Msg.QUESTION
			,buttons:Ext.Msg.YESNO
			,scope:this
			,fn:function(response) {
				if('yes' == response) {
					 try {
					    var req = new XMLHttpRequest;

					    req.open('POST', '/admin/papierdb/deletepapier/format/json?uid=' + record.get('uuid'), false);
					    req.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
					    req.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
					    req.send('&delete=1');
					    if (req.status == 200 || req.status == 304 || req.status == 0 || req.status == null) {
                          this.store.load({params:{start:0,limit:20}});
					      return Ext.util.JSON.decode(req.responseText);
					    }
					  } catch (e) {
					    return '';
					  }
				}else{
				    return;
				}
//				console.info('Deleting record');
			}
		});
	} // eo function deleteRecord

}); // eo extend

// register xtype
Ext.reg('PapierdbGrid', PrintShopCreator.PapierdbGrid);

Ext.QuickTips.init();
	
	var PapierdbGrid = new Ext.FormPanel({
		               
        renderTo: 'papierindex',
        closable: false,
        resizable: false,
        layout: 'fit',
        border: false,
        items:[{xtype:'PapierdbGrid', id:'PapierdbGrid'}]
});
PapierdbGrid.show();
