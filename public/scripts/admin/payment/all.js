Ext.ns('PrintShopCreator.Admin.Payment');


var translation = new Locale.Gettext();
translation.textdomain('App');


PrintShopCreator.Admin.Payment.LIST = Ext.extend(Ext.grid.GridPanel, {
	 
	border:false
	,layout: 'fit'
	,stateful:true
	,url:'/admin/payment/all?config=1&sid=' + sid
	,objName:'payment'
	,idName:'id'
	,frame: false
	,autoHeight: true
	,autoWidth: true
	,id: 'paymentgrid'
	,initComponent:function() {
		
		
		// create row actions
		this.rowActions = new Ext.ux.grid.RowActions({
			 actions:[{
				 iconCls:'icon-minus'
				,qtip: translation._('payment_all_js_Delete')
			},{
				 iconCls:'icon-edit-record'
				,qtip: translation._('payment_all_js_Edit')
			}]
		});
		this.rowActions.on('action', this.onRowAction, this);

		Ext.apply(this, {
			// {{{
			store:new Ext.data.Store({
				reader:new Ext.data.JsonReader({
					 id:'id'
					,totalProperty:'totalCount'
					,root:'rows'
					,fields:[
						 {name:'id', type:'int'}
						,{name:'title', type:'string'}
						,{name:'wert', type:'string'}
						,{name:'pos', type:'string'}
					]
				})
				,proxy:new Ext.data.HttpProxy({url:this.url})
				,baseParams:{cmd:'getData', objName:this.objName}
				,sortInfo:{field:'title', direction:'ASC'}
				,remoteSort:true
			})
			// }}}
			// {{{
			,columns:[{
				 header: 'Id'
						,id:'id'
						,dataIndex:'id'
						,width:20
						,sortable:true

					},{
				 header: translation._('payment_all_js_title')
				,id:'title'
				,dataIndex:'title'
				,width:160
				,sortable:true
				,editor:new Ext.form.TextField({
					allowBlank:false
				})
			},{
				 header: translation._('payment_all_js_wert')
				,id:'wert'
				,dataIndex:'wert'
				,width:160
				,sortable:true
                ,renderer: Ext.util.Format.euMoney
			},{
                 header: translation._('Pos')
                ,id:'pos'
                ,dataIndex:'pos'
                ,width:160
                ,sortable:true
                ,editor:new Ext.form.TextField({
                    allowBlank:false
                })
            }, this.rowActions]
			// }}}
			,plugins:[this.rowActions]
			,viewConfig:{forceFit:true}
			,tbar:[{
				 text: translation._('payment_all_js_Add Payment')
				,iconCls:'icon-form-add'
				,handler: function(e) {
                		PrintShopCreator.Base.Common.openWindow('paymentWindow', '/admin/payment/edit?sid='+ sid, 800, 600);
                }
			}]
		}); // eo apply

		this.bbar = new Ext.PagingToolbar({
			 store:this.store
			,displayInfo: true
			,plugins: [new Ext.ux.ProgressBarPager()]
			,pageSize:10
		});

		// call parent
		PrintShopCreator.Admin.Payment.LIST.superclass.initComponent.apply(this, arguments);
	} // eo function initComponent
	// {{{
	,onRender:function() {
		// call parent
		PrintShopCreator.Admin.Payment.LIST.superclass.onRender.apply(this, arguments);

		// load store
		this.store.load({params:{start:0,limit:10}});

	} // eo function onRender
	// }}}

	

	,onRowAction:function(grid, record, action, row, col) {
		switch(action) {
			
			case 'icon-minus':
				this.deleteRecord(record);
			break;

			case 'icon-edit-record':
			
			    PrintShopCreator.Base.Common.openWindow('paymentWindow', '/admin/payment/edit?sid='+ sid + '&uid=' + record.id, 800, 600);
				
			break;
		}
	} // eo onRowAction

	

	
	,showError:function(msg, title) {
		Ext.Msg.show({
			 title:title || 'Error'
			,msg:Ext.util.Format.ellipsis(msg, 2000)
			,icon:Ext.Msg.ERROR
			,buttons:Ext.Msg.OK
			,minWidth:1200 > String(msg).length ? 360 : 600
		});
	} // eo function showError

	,deleteRecord:function(record) {
		Ext.Msg.show({
			 title: translation._('msg_delete')
			,msg: translation._('msg_delete_confirm_beforetext') + ':<br><b>' + record.get('title') + '</b><br>' + translation._('msg_delete_confirm_aftertext')
			,icon:Ext.Msg.QUESTION
			,buttons:Ext.Msg.YESNO
			,scope:this
			,fn:function(response) {
				if('yes' == response) {
					 try {
					    var req = new XMLHttpRequest;
					
					    req.open('POST', '/admin/payment/edit?uid=' + record.id, false);
					    req.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
					    req.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
					    req.send('&delete=1');
					    if (req.status == 200 || req.status == 304 || req.status == 0 || req.status == null) {
					      this.store.load({params:{start:0,limit:10}});
                          return Ext.util.JSON.decode(req.responseText);
					    }
					  } catch (e) {
					    return '';
					  }
				}else{
				    return;
				}
//				console.info('Deleting record');
			}
		});
	} // eo function deleteRecord

}); // eo extend

// register xtype
Ext.reg('PrintShopCreatorAdminPaymentLIST', PrintShopCreator.Admin.Payment.LIST);
Ext.QuickTips.init();
	
	var paymentList = new Ext.FormPanel({
		               
        renderTo: 'paymentlist',
        closable: false,
        resizable: false,
        layout: 'fit',
        border: false,
        items:[{xtype:'PrintShopCreatorAdminPaymentLIST', id:'PrintShopCreatorAdminPaymentLIST'}]
});
paymentList.show();
