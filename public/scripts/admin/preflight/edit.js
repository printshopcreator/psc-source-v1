Ext.QuickTips.init();

Ext.onReady(function() {

    var translation = new Locale.Gettext();
    translation.textdomain('App');

    var win = new Ext.Window({
         id:'metaform-win'
        ,layout:'fit'
        ,width: 800
        ,scope:this
        ,height: 600
        ,title: translation._('Preflight Vorlage')
        ,items:{
             xtype:'metaform'
            ,url:'/admin/preflight/loadsavepreflight/format/json?config=3&uid=' + uid
            ,buttons:
            [{
                 text: translation._('cms_edit_js_Save')
                ,iconCls: 'action_applyChanges'
                ,listeners:{
                   click:{scope:this, buffer:200, fn:function(btn) {
    
	                   Ext.getCmp('metaform-win').items.get(0).getForm().submit({waitMsg: translation._('cms_edit_js_Saving')
                               ,url: '/admin/preflight/loadsavepreflight/format/json?config=3&uid=' + uid + '&save=1'
                               ,success: function(form, action){
                                        opener.Ext.getCmp('preflightgrid').store.reload();
                                      if(action.result.isNew) {
                                        window.location = '/admin/preflight/addpreflight?uid=' + action.result.uid;
                                      }
                                }});
                            
                }}}
            },{
                  text: translation._('cms_edit_js_Save and Close')
                ,iconCls: 'action_applyChangesClose'
                ,listeners:{
                   click:{scope:this, buffer:200, fn:function(btn) {
	                   Ext.getCmp('metaform-win').items.get(0).getForm().submit({waitMsg: translation._('cms_edit_js_Saving')
	                   , url: '/admin/preflight/loadsavepreflight/format/json?config=3&uid=' + uid + '&save=1'
	                   ,success: function(form, action){
                               opener.Ext.getCmp('preflightgrid').store.reload();
				                  window.close();  
				                }
	                   
	                   });
                }}}
            },{
                text: translation._('cms_edit_js_Close'),
                listeners:{
                   click:{scope:this, buffer:200, fn:function(btn) {
                        window.close();
                }}},
                iconCls: 'action_cancel'
            }]
        }
    });

    win.show();
});
