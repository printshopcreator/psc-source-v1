Ext.ns('PrintShopCreator');

var translation = new Locale.Gettext();
translation.textdomain('App');

PrintShopCreator.PreflightGrid = Ext
		.extend(
				Ext.grid.GridPanel,
				{

					border : false,
					layout : 'fit',
					stateful : true,
					url : '/admin/preflight/fetchpreflight/format/json',
					objName : 'booking',
					idName : 'id',
					frame : true,
					autoHeight : true,
					autoWidth : true,
					id : 'preflightgrid',
					title : translation._('preflight_grid_js_title'),
					initComponent : function() {

						// create row actions
						this.rowActions = new Ext.ux.grid.RowActions({
							actions : [ {
								iconCls : 'icon-edit-record',
								qtip : translation
										._('preflight_grid_js_Edit_Preflight')
							},{
								 iconCls:'icon-copy'
								,qtip: translation._('article_index_js_Copy_Preflight')
							},{
								 iconCls:'icon-minus'
										,qtip: translation._('cms_gridcms_js_Delete Page')
									}],
							widthIntercept : Ext.isSafari ? 4 : 2,
							id : 'actions'
						});
						this.rowActions.on('action', this.onRowAction, this);

						Ext
								.apply(
										this,
										{
											// {{{
											store : new Ext.data.Store(
													{
														reader : new Ext.data.JsonReader(
																{
																	id : 'id',
																	totalProperty : 'totalCount',
																	root : 'rows',
																	fields : [
																			{
																				name : 'id',
																				type : 'int'
																			},
																			{
																				name : 'nr',
																				type : 'string'
																			} ]
																}),
														proxy : new Ext.data.HttpProxy(
																{
																	url : this.url
																}),
														baseParams : {
															cmd : 'getData',
															objName : this.objName
														},
														sortInfo : {
															field : 'id',
															direction : 'DESC'
														},
														remoteSort : true
													})
											// }}}
											// {{{
											,
											columns : [
													{
														header : translation
																._('preflight_gridshops_js_Name'),
														id : 'nr',
														dataIndex : 'nr',
														width : 160,
														sortable : true,
														editor : new Ext.form.TextField(
																{
																	allowBlank : false
																})
													}, this.rowActions ]
											// }}}
											,
											plugins : [ this.rowActions ],
											viewConfig : {
												forceFit : true
											},
											tbar : [ {
												text : translation
														._('Preflightcheckregel hinzufügen'),
												iconCls : 'icon-form-add',
												listeners : {
													click : {
														scope : this,
														buffer : 200,
														fn : function(btn) {
															PrintShopCreator.Base.Common
																	.openWindow(
																			'preflightWindow',
																			'/admin/preflight/addpreflight',
																			800,
																			600);
														}
													}
												}
											} ]
										}); // eo apply

						this.bbar = new Ext.PagingToolbar({
							store : this.store,
							displayInfo : true,
							pageSize : 20
						});

						// call parent
						PrintShopCreator.PreflightGrid.superclass.initComponent
								.apply(this, arguments);
					} // eo function initComponent
					// {{{
					,
					onRender : function() {
						// call parent
						PrintShopCreator.PreflightGrid.superclass.onRender
								.apply(this, arguments);
						this.view.getRowClass = function(record, index) {
							if (record.data.booking_typ == 1) {
								return 'status2';
							}
							if (record.data.booking_typ == 2) {
								return 'status1';
							}
							if (record.data.booking_typ == 3) {
								return 'status3';
							}
							if (record.data.booking_typ == 4) {
								return 'status4';
							}
						};
						// load store
						this.store.load({
							params : {
								start : 0,
								limit : 20
							}
						});

					} // eo function onRender
					// }}}

					,
					addRecord : function() {
						var store = this.store;
						if (store.recordType) {
							var rec = new store.recordType({
								newRecord : true
							});

							return rec;
						}
						return false;
					} // eo function addRecord

					,
					onRowAction : function(grid, record, action, row, col) {
						switch (action) {
							case 'icon-copy':
								this.copyRecord(record);
							break;
							case 'icon-minus':
								this.deleteRecord(record);
							break;
							case 'icon-edit-record':
	
								PrintShopCreator.Base.Common.openWindow(
										'bookingWindow',
										'/admin/preflight/addpreflight?uid='
												+ record.id, 800, 600);
							break;
						}
					} // eo onRowAction

					,
					showError : function(msg, title) {
						Ext.Msg.show({
							title : title || 'Error',
							msg : Ext.util.Format.ellipsis(msg, 2000),
							icon : Ext.Msg.ERROR,
							buttons : Ext.Msg.OK,
							minWidth : 1200 > String(msg).length ? 360 : 600
						});
					} // eo function showError
					,deleteRecord:function(record) {
						Ext.Msg.show({
							 title: translation._('msg_delete')
							,msg: translation._('msg_delete_confirm_beforetext') + ':<br><b>' + record.get('nr') + '</b><br>' + translation._('msg_delete_confirm_aftertext')
							,icon:Ext.Msg.QUESTION
							,buttons:Ext.Msg.YESNO
							,scope:this
							,fn:function(response) {
								if('yes' == response) {
									 try {
									    var req = new XMLHttpRequest;

									    req.open('POST', '/admin/preflight/deletepreflight/format/json?uid=' + record.id, false);
									    req.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
									    req.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
									    req.send('&delete=1');
									    if (req.status == 200 || req.status == 304 || req.status == 0 || req.status == null) {
				                          this.store.load({params:{start:0,limit:20}});
									      return Ext.util.JSON.decode(req.responseText);
									    }
									  } catch (e) {
									    return '';
									  }
								}else{
								    return;
								}
//								console.info('Deleting record');
							}
						});
					} // eo function deleteRecord
					,
					copyRecord : function(record) {
						Ext.Msg.show({
							 title: translation._('msg_copy')
							,msg: translation._('msg_copy_confirm_beforetext') + ':<br><b>' + record.get('nr') + '</b><br>' + translation._('msg_copy_confirm_aftertext')
							,icon:Ext.Msg.QUESTION
							,buttons:Ext.Msg.YESNO
							,scope:this
							,fn:function(response) {
								if('yes' == response) {
									 try {
									    var req = new XMLHttpRequest;

									    req.open('POST', '/admin/preflight/copypreflight/format/json?uid=' + record.id, false);
									    req.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
									    req.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
									    req.send('&copy=1');
									    if (req.status == 200 || req.status == 304 || req.status == 0 || req.status == null) {
				                          this.store.load({params:{start:0,limit:20}});
									      return Ext.util.JSON.decode(req.responseText);
									    }
									  } catch (e) {
									    return '';
									  }
								}else{
								    return;
								}
//								console.info('Deleting record');
							}
						});
					} // eo function deleteRecord

				}); // eo extend

// register xtype
Ext.reg('preflightgrid', PrintShopCreator.PreflightGrid);

Ext.QuickTips.init();

var preflight = new Ext.FormPanel({

	renderTo : 'preflightindex',
	closable : false,
	resizable : false,
	layout : 'fit',
	border : false,
	items : [ {
		xtype : 'preflightgrid',
		id : 'preflightgrid'
	} ]
});
preflight.show();
