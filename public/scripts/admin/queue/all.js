Ext.ns('PrintShopCreator.Admin.Queues');

PrintShopCreator.Admin.Queues.LIST = Ext.extend(Ext.grid.GridPanel, {
	 
	border:false
	,layout: 'fit'
	,stateful:true
	,url:'/admin/queue/all?config=1&sid=' + sid
	,objName:'queue'
	,idName:'id'
	,frame: true
	,autoHeight: true
	,autoWidth: true
	,id: 'queuesgrid'
	,title: translation._('queue_all_js_Queues')
	,initComponent:function() {
		
		
		// create row actions
		this.rowActions = new Ext.ux.grid.RowActions({
			 actions:[{
				 iconCls:'icon-minus'
				,qtip: translation._('queue_all_js_Delete')
			},{
                 iconCls:'icon-copy'
                 ,qtip: translation._('Queue kopieren')
             },{
				 iconCls:'icon-edit-record'
				,qtip: translation._('queue_all_js_Edit')
			}]
			,widthIntercept:Ext.isSafari ? 4 : 2
			,id:'actions'
		});
		this.rowActions.on('action', this.onRowAction, this);

		Ext.apply(this, {
			// {{{
			store:new Ext.data.Store({
				reader:new Ext.data.JsonReader({
					 id:'id'
					,totalProperty:'totalCount'
					,root:'rows'
					,fields:[
						 {name:'id', type:'int'}
						,{name:'name', type:'string'}
						,{name:'typ', type:'string'}
						,{name:'typName', type:'string'}
						,{name:'enable', type:'boolean'}
					]
				})
				,proxy:new Ext.data.HttpProxy({url:this.url})
				,baseParams:{cmd:'getData', objName:this.objName}
				,sortInfo:{field:'name', direction:'ASC'}
				,remoteSort:true
			})
			// }}}
			// {{{
			,columns:[{
				 header: translation._('queue_all_js_Name')
				,id:'name'
				,dataIndex:'name'
				,width:160
				,sortable:true
				,editor:new Ext.form.TextField({
					allowBlank:false
				})
			},{
				 header: translation._('queue_all_js_Typ')
				,id:'typ'
				,dataIndex:'typName'
				,width:100
				,sortable:true
				,editor:new Ext.form.TextField({
					allowBlank:false
				})
			}, this.rowActions]
			// }}}
			,plugins:[this.rowActions]
			,viewConfig:{forceFit:true}
			,tbar:[{
				xtype: 'combo'
				
				,emptyText: translation._('queue_all_js_Select Queuetyp')
       			,store: new Ext.data.JsonStore({
						    url: '/admin/queue/all?config=2',
						    root: 'rows',
						    fields: ['name', 'id']
				})
                ,mode: 'remote'
                ,triggerAction: 'all'
        		,selectOnFocus:true
        		,valueField:'id'
           		,displayField:'name'
           		,listeners:{
					select:{scope:this, buffer:200, fn:function(btn) {
					
						PrintShopCreator.Base.Common.openWindow('queueWindow', '/admin/queue/edit?sid='+ sid + '&type=' + btn.value, 800, 600);
					}}
				}
	            
			}]
		}); // eo apply

		this.bbar = new Ext.PagingToolbar({
			 store:this.store
			,displayInfo: true
			,plugins: [new Ext.ux.ProgressBarPager()]
			,pageSize:10
		});

		// call parent
		PrintShopCreator.Admin.Queues.LIST.superclass.initComponent.apply(this, arguments);
	} // eo function initComponent
	// {{{
	,onRender:function() {
		// call parent
		PrintShopCreator.Admin.Queues.LIST.superclass.onRender.apply(this, arguments);

		// load store
		this.store.load({params:{start:0,limit:20}});

	} // eo function onRender
	// }}}

	

	,onRowAction:function(grid, record, action, row, col) {
		switch(action) {
			
			case 'icon-minus':
				this.deleteRecord(record);
			break;
            case 'icon-copy':
                this.copyRecord(record);
            break;
			case 'icon-edit-record':
			
			    PrintShopCreator.Base.Common.openWindow('articleWindow', '/admin/queue/edit?sid='+ sid + '&uid=' + record.id + '&type=' + record.data.typ, 800, 600);
				
			break;
		}
	} // eo onRowAction

	

	
	,showError:function(msg, title) {
		Ext.Msg.show({
			 title:title || 'Error'
			,msg:Ext.util.Format.ellipsis(msg, 2000)
			,icon:Ext.Msg.ERROR
			,buttons:Ext.Msg.OK
			,minWidth:1200 > String(msg).length ? 360 : 600
		});
	} // eo function showError

	,deleteRecord:function(record) {
		Ext.Msg.show({
			 title: translation._('msg_delete')
			,msg: translation._('msg_delete_confirm_beforetext') + ':<br><b>' + record.get('name') + '</b><br>' + translation._('msg_delete_confirm_aftertext')
			,icon:Ext.Msg.QUESTION
			,buttons:Ext.Msg.YESNO
			,scope:this
			,fn:function(response) {
				if('yes' == response) {
					 try {
					    var req = new XMLHttpRequest;

					    req.open('POST', '/admin/queue/edit?uid=' + record.id, false);
					    req.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
					    req.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
					    req.send('&delete=1');
					    if (req.status == 200 || req.status == 304 || req.status == 0 || req.status == null) {
                          this.store.load({params:{start:0,limit:20}});
					      return Ext.util.JSON.decode(req.responseText);
					    }
					  } catch (e) {
					    return '';
					  }
				}else{
				    return;
				}
//				console.info('Deleting record');
			}
		});
	} // eo function deleteRecord
    ,copyRecord:function(record) {

        combo = new Ext.form.ComboBox({
            fieldLabel: 'Shop',
            hiddenName:'shopsel',
            store: new Ext.data.JsonStore({
                url: '/service/?type=get&mode=system&service=shops',
                root: 'items',
                fields: ['name', 'label']
            }),
            valueField:'name',
            displayField:'label',
            mode: 'remote',
            triggerAction: 'all',
            forceSelection: true,
            width:190,
            emptyText:translation._('Select a Shop')
        });

        var tabs = new Ext.FormPanel({
            labelWidth: 120,
            width: 600,
            frame: true,
            id: 'copyshopform',
            items: [{
                xtype: 'fieldset',
                title: translation._('Mode'),
                items: [{
                    xtype: 'radiogroup',
                    fieldLabel: translation._('Mode'),
                    height: 90,
                    id: 'modes',
                    columns: 1,
                    items: [
                        { boxLabel: translation._('ThisShop'), name: 'radBreakType', inputValue: 1, checked: true  },
                        { boxLabel: translation._('OtherShop'), name: 'radBreakType', inputValue: 2}
                    ]
                }]
            },{
                xtype: 'fieldset',
                border: true,
                title: translation._('Settings for Other Shop'),
                items: [combo]
            }]
        });

        var win = new Ext.Window({
            title: translation._('Copy Queue'),
            closable: true,
            id: 'copyshop',
            modal: true,
            closeAction: 'hide',
            width: 500,
            height: 350,
            plain: true,
            layout: 'fit',
            border: false,
            items: tabs,
            buttons: [{
                text: translation._('Execute'),
                iconCls: 'action_applyChanges',
                id: 'insert-btn',
                scope: this,
                handler: function() {

                    var radio = Ext.getCmp('modes');

                    if(radio.items.items[0].getGroupValue() == 1) {
                        try {
                            var req = new XMLHttpRequest;

                            req.open('POST', '/admin/queue/edit?uuid=' + record.data.id, false);
                            req.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
                            req.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
                            req.send('&copy=1');
                            if (req.status == 200 || req.status == 304 || req.status == 0 || req.status == null) {
                                this.store.load({params:{start:0,limit:20}});
                                win.close();
                                return Ext.util.JSON.decode(req.responseText);
                            }
                        } catch (e) {
                            win.close();
                        }

                    }
                    if(radio.items.items[0].getGroupValue() == 2) {
                        try {
                            var req = new XMLHttpRequest;

                            req.open('POST', '/admin/queue/edit?uuid=' + record.data.id, false);
                            req.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
                            req.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
                            req.send('&copymove=1&target=' + combo.getValue());
                            if (req.status == 200 || req.status == 304 || req.status == 0 || req.status == null) {
                                this.store.load({params:{start:0,limit:20}});
                                win.close();
                            }
                            win.hide();
                        } catch (e) {
                            win.close();
                        }
                    }
                }
            }, {
                text: translation._('Close'),
                iconCls: 'action_cancel',
                handler: function() {
                    win.close();
                }
            }]
        });
        win.show();
    } // eo function deleteRecord
}); // eo extend

// register xtype
Ext.reg('PrintShopCreatorAdminQueuesLIST', PrintShopCreator.Admin.Queues.LIST);
Ext.QuickTips.init();
	
	var queuesList = new Ext.FormPanel({
		               
        renderTo: 'queueslist',
        closable: false,
        resizable: false,
        layout: 'fit',
        border: false,
        items:[{xtype:'PrintShopCreatorAdminQueuesLIST', id:'PrintShopCreatorAdminQueuesLIST'}]
});
queuesList.show();
