Ext.onReady(function() {


    Ext.QuickTips.init();

    var translation = new Locale.Gettext();
    translation.textdomain('App');

    var win = new Ext.Window({
         id:'metaform-win'
        ,layout:'fit'
        ,width: 800
        ,scope:this
        ,height: 600
        ,title: translation._('Edit Queue')
        ,items:{
             xtype:'metaform'
            ,url:'/admin/queue/edit?config=3&sid='+ sid + '&type=' + type + '&uid=' + uid
            ,buttons:
            [{
                 text: translation._('Save')
                ,iconCls: 'action_applyChanges'
                ,listeners:{
                   click:{scope:this, buffer:200, fn:function(btn) {
	                   Ext.getCmp('metaform-win').items.get(0).getForm().submit({waitMsg: translation._('Saving')
                               ,url: '/admin/queue/edit?config=3&sid='+ sid + '&type=' + type + '&uid=' + uid + '&save=1'
                                ,success: function(form, action){
                                      if(action.result.isNew) {
                                            window.location = '/admin/queue/edit?sid='+ sid + '&type=' + type + '&uid=' + action.result.uid;
                                      }
                                }});
                }}}
            },{
                  text: translation._('Save and Close')
                ,iconCls: 'action_applyChangesClose'
                ,listeners:{
                   click:{scope:this, buffer:200, fn:function(btn) {
	                   Ext.getCmp('metaform-win').items.get(0).getForm().submit({waitMsg: translation._('Saving')
	                   , url: '/admin/queue/edit?config=3&sid='+ sid + '&type=' + type + '&uid=' + uid + '&save=1'
	                   ,success: function(form, action){
				                  window.close();  
				                }
	                   
	                   });
                }}}
            },{
                text: translation._('Close'),
                listeners:{
                   click:{scope:this, buffer:200, fn:function(btn) {
                        window.close();
                }}},
                iconCls: 'action_cancel'
            }]
        }
    });

    win.show();
});