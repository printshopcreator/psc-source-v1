/**
 * Tine 2.0
 *
 * @package     ExampleApplication
 * @license     http://www.gnu.org/licenses/agpl.html AGPL Version 3
 * @author      Cornelius Weiss <c.weiss@metaways.de>
 * @copyright   Copyright (c) 2007-2008 Metaways Infosystems GmbH (http://www.metaways.de)
 * @version     $Id: Models.js 11798 2009-12-02 14:49:06Z p.schuele@metaways.de $
 *
 */

Ext.ns('PrintShopCreator.Admin.Queues', 'PrintShopCreator.Admin.Queues.Model');

/**
 * @type {Array}
 * ExampleRecord model fields
 */
PrintShopCreator.Admin.Queues.Model.QueueArray = Tine.Tinebase.Model.genericFields.concat([
    { name: 'id' },
    { name: 'name' },
    // TODO add more record fields here
    // tine 2.0 notes + tags
    { name: 'notes'},
    { name: 'tags' }
]);

/**
 * @type {Tine.Tinebase.ExampleRecord}
 * record definition
 */
PrintShopCreator.Admin.Queues.Model.Queue = Tine.Tinebase.data.Record.create(PrintShopCreator.Admin.Queues.Model.QueueArray, {
    appName: 'Queue',
    modelName: 'Queue',
    idProperty: 'id',
    titleProperty: 'title',
    // ngettext('ExampleRecord', 'ExampleRecords', n);
    recordName: 'Queue',
    recordsName: 'Queues',
    containerProperty: 'container_id',
    // ngettext('record list', 'record lists', n);
    containerName: 'record list',
    containersName: 'record lists',
    getTitle: function() {
        return this.get('number') ? (this.get('number') + ' ' + this.get('title')) : false;
    }
});

PrintShopCreator.Admin.Queues.Model.Queue.getDefaultData = function() {
    return {
    	/*
        is_open: 1,
        is_billable: true
        */
    };
};

//Tine.ExampleApplication.FilterPanel = Tine.widgets.grid.PersistentFilterPicker

/**
 * default ExampleRecord backend
 */
PrintShopCreator.Admin.Queues.recordBackend = new Tine.Tinebase.data.RecordProxy({
    appName: 'Queue',
    modelName: 'Queue',
    recordClass: PrintShopCreator.Admin.Queues.Model.Queue
});
