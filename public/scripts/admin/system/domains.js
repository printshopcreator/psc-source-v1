Ext.onReady(function() {


    Ext.QuickTips.init();

    var translation = new Locale.Gettext();
    translation.textdomain('App');

    var url = '/admin/system/domains?config=1&sid='+ sid + '&uid=' + uid;

    var deleteRecord = function(record) {
		Ext.Msg.show({
			 title: translation._('msg_delete')
			,msg: translation._('msg_delete_confirm_beforetext') + ':<br><b>' + record.get('name') + '</b><br>' + translation._('msg_delete_confirm_aftertext')
			,icon:Ext.Msg.QUESTION
			,buttons:Ext.Msg.YESNO
			,scope:this
			,fn:function(response) {
				if('yes' == response) {
					 try {
					    var req = new XMLHttpRequest;

					    req.open('POST', '/admin/system/domains?uid=' + record.id, false);
					    req.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
					    req.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
					    req.send('&delete=1');
					    if (req.status == 200 || req.status == 304 || req.status == 0 || req.status == null) {
                          products.store.load({params:{start:0,limit:20}});
					      return Ext.util.JSON.decode(req.responseText);
					    }
					  } catch (e) {
					    return '';
					  }
				}else{
				    return;
				}
//				console.info('Deleting record');
			}
		});
	} // eo function deleteRecord

    var onRowAction = function(grid, record, action, row, col) {
		switch(action) {
			case 'icon-minus':
				deleteRecord(record);
			break;
		}
	} // eo onRowAction

    var rowActions = new Ext.ux.grid.RowActions({
             actions:[{
                 iconCls:'icon-minus'
                ,qtip: translation._('domains_griddomain_js_Delete')
            }]
            ,widthIntercept:Ext.isSafari ? 4 : 2
            ,id:'actions'
        });
    rowActions.on('action', onRowAction, this);

    var myRecordObj = Ext.data.Record.create([
            {name: 'name', mapping: 'ecgsname', sortDir: 'ASC', sortType: 'asUCString'},
            {name: 'id'}
    ]);

    function addRecord() {
            var r = new myRecordObj({
                name: 'Neuer Name',
                newRecord:'yes',
                id: 0
            });
            products.stopEditing();
            products.store.insert(0, r);
            products.startEditing(0, 0);
    };

    function handleEdit(editEvent) {
			updateDB(editEvent);
	}

    function updateDB(oGrid_Event) {
        var isNewRecord = oGrid_Event.record.data.newRecord;
        Ext.Ajax.request(
            {
                waitMsg: 'Speichern ...',
                url: '/admin/system/domains',
                params: {
                    task: "updategs",
                    key: 'id',
                    uid: oGrid_Event.record.get('id'),
                    sid: uid,
                    dataId: oGrid_Event.record.data.id,
                    newRecord: isNewRecord,
                    field: oGrid_Event.field,
                    value: oGrid_Event.value,
                    originalValue: oGrid_Event.record.modified
                },
                failure:function(response,options){
                    Ext.MessageBox.alert('Warning','Oops...');
                },
                success:function(response,options){
                    if(isNewRecord == 'yes'){
                        var responseData = Ext.util.JSON.decode(response.responseText);
                        var newID = responseData.newID;
                        oGrid_Event.record.set('newRecord','no');
                        oGrid_Event.record.set('id',newID);
                        products.store.commitChanges();
                    } else {
                        products.store.commitChanges();
                    }

                    products.store.reload();
                }
             }
        );
    };
    
    var products = new Ext.grid.EditorGridPanel({
        autoWidth: true,
        height: 700,
        store: new Ext.data.Store({
            reader:new Ext.data.JsonReader({
                     id:'id'
                    ,totalProperty:'totalCount'
                    ,root:'rows'
                    ,fields:[
                         {name:'id', type:'int'}
                        ,{name:'name', type:'string'}
                    ]
                })
                ,proxy:new Ext.data.HttpProxy({url:url})
                ,baseParams:{cmd:'getData', objName:'product'}
                ,sortInfo:{field:'id', direction:'ASC'}
                ,remoteSort:true
        }),
        cm: new Ext.grid.ColumnModel([
            {id:'name', header: translation._('domains_edit_js_Name'), width: 20, sortable: true, dataIndex: 'name',
            editor: new Ext.form.TextField({
                        allowBlank: false
                    })
            },
            rowActions
        ]),
        plugins:[rowActions],
        viewConfig: {
            forceFit:true
        },
        collapsible: true,
        animCollapse: false,
        iconCls: 'icon-grid',
        tbar: [
                {
                    text: 'Hinzuf&uuml;gen',
                    tooltip: 'Hinzuf&uuml;gen',
                    iconCls:'icon-plus',
                    handler: addRecord
                }
         ]
    });

    products.store.load();

    products.addListener('afteredit', handleEdit);

    var win = new Ext.Window({
         id:'metaform-win'
        ,layout:'fit'
        ,width: 800
        ,scope:this
        ,height: 600
        ,title: translation._('domains_edit_js_DOMAINS')
        ,items:products
    });

    win.show();
});