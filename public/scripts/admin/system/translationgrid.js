
Ext.ns('PrintShopCreator');

var translation = new Locale.Gettext();
translation.textdomain('App');

function renderStatus(data, cell, record, rowIndex, columnIndex, store){
         var value=record.get('status');
         //cell.css = "UnStyleCss"; //défini dans style.css
         cell.css = "status" + value; //Color's set here
         return data; //data = text
}

var editor = new Ext.ux.grid.RowEditor();

PrintShopCreator.TranslationGrid = Ext.extend(Ext.grid.EditorGridPanel, {
	 
	border:false
	,layout: 'fit'
	,stateful:true
	,url:'/admin/system/fetchtranslation/format/json'
	,objName:'ShopThemes'
	,idName:'id'
	,frame: false
	,autoHeight: true
	,autoWidth: true
,id: 'ShopThemesgrid'
,title: translation._('Translations')
        
	,initComponent:function() {
	
	
		var filters = new Ext.ux.grid.GridFilters({
        filters:[
          {type: 'string',  dataIndex: 'id'},
          {type: 'string',  dataIndex: 'de'}
          

          ]});

		Ext.apply(this, {
			// {{{
			store:new Ext.data.Store({
				reader:new Ext.data.JsonReader({
					 id:'id'
					,totalProperty:'totalCount'
					,root:'rows'
					,fields:[
						 {name:'id', type:'string'}
						,{name:'en', type:'string'}
						,{name:'de', type:'string'}
					]
				})
				,proxy:new Ext.data.HttpProxy({
					api: {
			        read    : this.url,
			        create  : this.url + '?mode=create',
			        update  : this.url + '?mode=update',
			        destroy : this.url
			    }})
				,writer: new Ext.data.JsonWriter({
				    encode: true,
				    writeAllFields: true
				})
				,baseParams:{cmd:'getData', objName:this.objName}
//				,sortInfo:{field:'id', direction:'ASC'}
				,remoteSort:true
				,autoSave: false
			})
			// }}}
			// {{{
			,columns:[{
				 header: 'Id'
						,id:'id'
						,dataIndex:'id'
						,width:100
						,sortable:true
						,editor: new Ext.form.TextField({
		                    allowBlank: false
		                })

					},{
				 header: translation._('en')
					,id:'en'
					,dataIndex:'en'
					,width:100
					,sortable:true
					,editor: new Ext.form.TextField({
	                    allowBlank: false
	                })

				},{
					 header: translation._('de')
						,id:'de'
						,dataIndex:'de'
						,width:100
						,sortable:true
						,editor: new Ext.form.TextField({
		                    allowBlank: false
		                })

						
					}]
			// }}}
			,viewConfig:{forceFit:true}
			,plugins: [ editor, filters]

		}); // eo apply

		editor.on({
			  scope: this,
			  afteredit: function(roweditor, changes, record, rowIndex) {
			    //your save logic here - might look something like this:
			    this.store.save();
			  }
			});
		//this.tbar = this.buildTopToolbar();

		this.bbar = new Ext.PagingToolbar({
			 store:this.store
			,displayInfo: true
			,plugins: [filters, new Ext.ux.ProgressBarPager()]
			,pageSize:30
		});
		
		// call parent
		PrintShopCreator.TranslationGrid.superclass.initComponent.apply(this, arguments);
	} // eo function initComponent
	// {{{
	,onRender:function() {
		// call parent
		PrintShopCreator.TranslationGrid.superclass.onRender.apply(this, arguments);

		// load store
		this.store.load({params:{start:0,limit:30}});

	} // eo function onRender
	// }}}
	,onAdd : function(btn, ev) {
        var u = new this.store.recordType({
            id : '',
            en: '',
            de : ''
        });
        this.stopEditing();
        this.store.insert(0, u);
        this.startEditing(0, 1);
    }

	,buildTopToolbar : function() {
        return [{
            text: 'Add',
            iconCls: 'silk-add',
            handler: this.onAdd,
            scope: this
        }, '-', {
            text: 'Delete',
            iconCls: 'silk-delete',
            handler: this.onDelete,
            scope: this
        }, '-'];
    }

	,showError:function(msg, title) {
		Ext.Msg.show({
			 title:title || 'Error'
			,msg:Ext.util.Format.ellipsis(msg, 2000)
			,icon:Ext.Msg.ERROR
			,buttons:Ext.Msg.OK
			,minWidth:1200 > String(msg).length ? 360 : 600
		});
	} // eo function showError

	

}); // eo extend

// register xtype
Ext.reg('Translationgrid', PrintShopCreator.TranslationGrid);

Ext.QuickTips.init();
	
	var translation = new Ext.FormPanel({
		               
        renderTo: 'translationindex',
        closable: false,
        resizable: false,
        layout: 'fit',
        border: false,
        items:[{xtype:'Translationgrid', id:'Translationgrid'}]
});
translation.show();
