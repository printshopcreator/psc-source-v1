myRecordObj = Ext.data.Record.create([
        {name: 'created', type: 'date', dateFormat: 'Y-m-d H:i:s'},
        {name: 'name'},
        {name: 'count'},
        {name: 'file'},
        {name: 'size'},
        {name: 'uploadfinish', type: 'bool'},
        {name: 'id'},
        {name: 'download'},
        {name: 'uid'}
    ]);
reader = new Ext.data.JsonReader({
    root: 'rows',
    totalProperty: 'totalCount', 
    id: 'uid' 
},
    myRecordObj 
);
var store = new Ext.data.GroupingStore({
        proxy: new Ext.data.HttpProxy({
            url: '/admin/upload/all?config=1&uid=' + uid,
            method: 'POST'
        }),
        reader: reader,

        sortInfo:{field: 'name', direction: "ASC"},
        groupField:'name'
});

function onItemToggleFinish(item, pressed){
            Ext.Ajax.request(
                { 
                    waitMsg: 'Speichern ...',
                    url: '/admin/upload/all?config=2&uid=' + uid,
                    params: {
                        posid: item.groupId
                        
                    },
                    failure:function(response,options){
                        Ext.MessageBox.alert('Warning','Oops...');
                    },                                 
                    success:function(response,options){
                            
                            store.load({params: {start:0, limit: 50}});  
                    }                                     
                 }
            );
            
    }



Ext.onReady(function() {


    Ext.QuickTips.init();

    var translation = new Locale.Gettext();
    translation.textdomain('App');

	var form = new Ext.form.FormPanel({
	        baseCls: 'x-plain',
	        labelWidth: 55,
	        fileUpload:true,
	        region: "north",
	        height: 120,
	        url: '/service/upload/uploadcenter?uid=' + uid,
	        defaultType: 'textfield',
	        buttons: [new Ext.Button({
	                        text: translation._('upload_all_js_Upload now'),
	                        scope: this,
	                        handler: function(btn, event){
	                           form.getForm().submit({                                   
	                                waitMsg: translation._('upload_all_js_please wait .....'),
                                        success: function() {
                                            store.load({params: {
                                                    start: 0,
                                                    limit: 50
                                            }});
                                        }
	                           });
	                          
	                        }
	                    })],
	
	        items: [{
	            fieldLabel: translation._('upload_all_js_Position'),
	            name: 'id',
	            //anchor:'100%',  // anchor width by percentage
	            xtype: 'combo',
	            store: new Ext.data.JsonStore({
	                   url: '/admin/upload/all?config=3&uid=' + uid,
	                   root: 'items',
	                   fields: ['label', 'value']
	            }),
	            valueField: 'value',
	            displayField: 'label',
	            hiddenName: 'id',
	            mode: 'remote',
	            width: 300,
	            triggerAction: 'all',
	            emptyText: translation._('upload_all_js_select pos')
	        },{
	            fieldLabel: translation._('upload_all_js_File'),
	            name: 'file',
	            xtype: 'textfield',
	            inputType: 'file'
	            //anchor:'100%'  // anchor width by percentage
	        }]
	    });
	    
    var tb = new Ext.Toolbar({
        items:['-', {
            text: translation._('upload_all_js_Finish'),
            enableToggle: true,
            iconCls: 'btn-finish',
            toggleHandler: onItemToggleFinish,
            pressed: false
        }],
        scope: this
    });
    
    var rowactions = new Ext.ux.grid.RowActions({
             actions:[{
                 iconCls:'icon-minus'
                ,qtip: translation._('upload_all_js_Delete')
            }]
            ,widthIntercept:Ext.isSafari ? 4 : 2
            ,id:'actions'
        });
    
    var grid = new Ext.grid.EditorGridPanel({
        
        
        rowActions: rowactions,
        
        onRowAction:function(grid, record, action, row, col) {
	       switch(action) {
	       
	           case 'icon-minus':
	               Ext.Ajax.request(
		                { 
		                    waitMsg: 'Löschen ...',
		                    url: '/service/upload/deletefile?sid='+ sid + '&uid=' + record.data.id,
		                    params: {
		                        del: record.data.uid
		                        
		                    },
		                    failure:function(response,options){
		                        Ext.MessageBox.alert('Warning','Oops...');
		                    },                                 
		                    success:function(response,options){
		                            
		                            store.load({params: {start:0, limit: 50}});  
		                    }                                     
		                 }
		            );
	           break;
	           
	       }
        },        
        clicksToEdit:1,
        selModel: new Ext.grid.RowSelectionModel({singleSelect:false}),
        store: store,
        columns: [
            {header: translation._('upload_all_js_id'), hidden: true, width: 15, sortable: true, dataIndex: 'id'},
            {header: translation._('upload_all_js_Created'), width: 20, hidden:true, sortable: true, dataIndex: 'created', renderer: Ext.util.Format.dateRenderer('d.m.Y')},
            {header: translation._('upload_all_js_Name'), width: 20, hidden:true, sortable: true, dataIndex: 'name'},
            {header: translation._('upload_all_js_Count'), width: 10, hidden:true, sortable: true, dataIndex: 'count'},
            {header: translation._('upload_all_js_file'), width: 20, sortable: true, dataIndex: 'file'},
            
            {header: translation._('upload_all_js_size'), width: 20, sortable: true, dataIndex: 'size'},
            {header: translation._('Download'), width: 20, sortable: true, dataIndex: 'download'
            ,renderer: function(val) { return '<a target="_blank" href="'+val+'">'+ translation._('upload_all_js_download')+'</a>'; }},
            rowactions
            
            
        ],
        plugins:[rowactions],
        view: new Ext.ux.grid.TbarGroupingView({
            forceFit:true,
            getRowClass: function(r){
                var d = r.data;
                if(d.uploadfinish){
                    return 'greenrow';
                }
                if(!d.uploadfinish){
                    return 'redrow';
                }
                return '';
            },
            groupTextTpl: '{text} ({[values.rs.length]} {[values.rs.length > 1 ? "Items" : "Item"]})'
            //tbar: tb
        }),
        bbar: new Ext.PagingToolbar({
            store:this.store
            ,displayInfo:true
            ,pageSize:10
        }),
        clicksToEdit:2,
        region: "center",
        height: 400,
        collapsible: true,
        animCollapse: false,
        title: translation._('upload_all_js_items'),
        iconCls: 'icon-grid'
        
    });
    grid.rowActions.on('action', grid.onRowAction, this);
    grid.store.load({params: {
            start: 0,
            limit: 50
    }});



    var win = new Ext.Window({
         id:'metaform-win'
        ,title: translation._('upload_all_js_Uploadcenter')
        ,layout:'form'
        ,width: 800
        ,scope:this
        ,height: 600
        ,items: [form,grid]
    });

    win.show();
});
