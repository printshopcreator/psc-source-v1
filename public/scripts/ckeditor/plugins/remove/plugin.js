﻿/**
 * @license Copyright (c) 2003-2013, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.html or http://ckeditor.com/license
 */

/**
 * @fileOverview Save plugin.
 */

(function() {

	var pluginName = 'remove';

	// Register a plugin named "save".
	CKEDITOR.plugins.add( pluginName, {
		icons: 'remove', // %REMOVE_LINE_CORE%
		init: function( editor ) {

			editor.addCommand( pluginName, {
                exec: function( editor ) {
                    $(saveLayouterCreateData.elements).each(function(elkey, el) {
                        if(el.id == editor.name) {
                            $(".form-element-" + editor.name).remove();
                            saveLayouterCreateData.elements.splice(elkey, 1);
                            saveLayouterCreateData.count--;
                            return;
                        }
                    });
                    saveData();
                    render('div#steplayouter div.smallpreview','smallpreview');
                }
            } );

			editor.ui.addButton( 'Remove', {
				label: "Entfernen",
				command: pluginName,
				toolbar: 'document,11'
			});
		}
	});
})();
