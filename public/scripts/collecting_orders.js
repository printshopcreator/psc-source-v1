Function.prototype.Timer = function (interval, calls, onend) {
    var count = 0;
    var payloadFunction = this;
    var startTime = new Date();
    var callbackFunction = function () {
        return payloadFunction(startTime, count);
    };
    var endFunction = function () {
        if (onend) {
            onend(startTime, count, calls);
        }
    };
    var timerFunction =  function () {
        count++;
        if (count < calls && callbackFunction() != false) {
            window.setTimeout(timerFunction, interval);
        } else {
            endFunction();
        }
    };
    timerFunction();
};

function leadingzero (number) {
    return (number < 10) ? '0' + number : number;
}
function countdown (seconds, target) {
    var element = $('#generate_orders');
    var calculateAndShow = function () {
        if (seconds > 0) {
            seconds--;
        } else {
            return false;
        }
    };
    var completed = function () {
        element.html("<strong>Redirect ...<\/strong>");
        document.location.href="/basket"
    };
    calculateAndShow.Timer(1000, Infinity, completed);
}


$(function(){
    $('a[rel="#collecting_orders"]').unbind();
    $('a[rel="#collecting_orders"]').click(function(){
        $('#upload_mode').val('templateprint');
        $('#myUpload').modal('toggle');
        $('#myCollectingOrders').modal('toggle');
        $(document).off('focusin.modal');
        loadContacts();

        $('a[rel="#addContact"]').unbind();
        $('a[rel="#addContact"]').click(function(){
            $('#addContact').modal('toggle');
            $(document).off('focusin.modal');
            return false;

        });

        return false;

    });



    $('#saveContact').click(function() {
        $(document).off('focusin.modal');
        $.post(
            '/user/contactadd/format/json',
            $("#addContact form").serialize(),
            function(data){
                if(data.success) {

                    $('#addContact').modal('toggle');
                    loadContacts();
                }else{
                    $.each(data.messages, function(index, item) {
                        $.each(item, function(indexx, items) {
                            $('#addContact form #' + index + '-' + indexx).css('border', '1px solid red');
                        })
                    });
                }
            });

        return false;
    });

    $('#generate_orders').click(function() {
        $('.modal-footer').html("!!!BITTE WARTEN ------------ VORSCHAU WIRD GENERIERT !!!");

        var countGen = 0;

        $.each($("#collecting_data_form input[name='contact_uuid[]']:checked"), function() {
            countGen++;
            $('#collecting_orders_data').val($(this).val());
            $.ajax({
                type: "POST",
                url: $("#buyform").attr("action"),
                data: $("#buyform").serialize(), // serializes the form's elements.
                success: function (data) {
                    $.ajax({
                        type: "POST",
                        url: "/tp.php",
                        data: {
                            genUrl: data.genUrl
                        }, // serializes the form's elements.
                        success: function (data) {
                            countGen--;
                            if (countGen == 0) {
                                document.location.href = "/basket";
                                $('#myCollectingOrders').modal('toggle');
                            }
                        }

                    });
                }

            });
        });
        return false;
    });


});


function loadContacts() {
    $.ajax({
        type: "POST",
        url: '/article/getcollectingcontacts/format/json',
        data: {},
        success: function(data) {
            var markup = '<tr><td><input type="checkbox" name="contact_uuid[]" value="<%= uuid %>"/></td>'
                       + '<td><p><small class="mail"><%= self_email %></small></p></td><td><p><small class="firma"><%= self_department %></small></p></td>'
                       + '<td><p><small class="firstname"><%= self_firstname %></small></p></td><td><p><small class="lastname"><%= self_lastname %></small></p></td>'
                       + '<td><p><small class="street"><%= self_street %> <%= self_house_number %></small></p></td><td><p><small class="city"><%= self_zip %> <%= self_city %></small></p></td>'
                       + '<td><input type="file" name="logo1" class="file_upload_templateprint" style="display:none"/><a data-uuid="<%= uuid %>" rel="#templateprint-upload-picture" class="templateprint-upload-picture btn btn-small pull-right">Bild tauschen</a></td>'
                       + '<td><a rel="#overlaytemp" class="btn btn-small pull-right" href="' + previewEditUrl + '&w2puserid=<%= uuid %>">Vorschau Edit</a></td></tr>';


            $('#collecting_data tbody').empty();

            $.each(data.result, function(index, item) {

                $('#collecting_data tbody').append(_.template(markup, item));

            });

            bindTemplatebrindButtons();

            var fuzzyOptions = {
                searchClass: "fuzzy_search",
                location: 0,
                distance: 100,
                threshold: 0.4,
                multiSearch: true
            };

            var options = {
                valueNames: [ 'firstname', 'lastname', 'firma', 'mail', 'street', 'city' ],
                plugins: [ ListFuzzySearch() ]
            };

            var userList = new List('myCollectingOrders', options);

            return false;
        }
    })


}