// isArray is in SVN only

Ext.ns('PrintShopCreator');

var translation = new Locale.Gettext();
translation.textdomain('App');


if(!Ext.isArray) {
    Ext.isArray = function(v) {
        return v && 'function' == typeof v.pop;
    }
}

/**
 *
 * @class Ext.ux.MetaForm
 * @extends Ext.FormPanel
 */
Ext.ux.MetaForm = Ext.extend(Ext.FormPanel, {
    // configurables
     autoInit:true
    ,frame:true
    ,loadingText:'Loading...'
    ,savingText:'Saving...'
    ,buttonMinWidth:90
    ,columnCount:1
    ,layout: 'column'
    /**
     * createButtons {Array} array of buttons to create
     *         valid values are ['meta', 'load', defaults', 'reset', 'save', 'ok', 'cancel']
     */
    /**
     * ignoreFields: {Array} of field names to ignore when received in metaData
     */

    // {{{
    ,initComponent:function() {

        // create one item to avoid error
        Ext.apply(this, {
            items:this.items || {}
        }); // eo apply

        // get buttons if we have button creation routines
        if('function' == typeof this.getButton) {
            this.buttons = this.getButtons();
        }

        // call parent
        Ext.ux.MetaForm.superclass.initComponent.apply(this, arguments);
        
        this.addEvents('cancel', 'ok');

        // install event handlers on basic form
        this.form.on({
             beforeaction:{scope:this, fn:this.beforeAction}
             
            ,actioncomplete:{scope:this, fn:function(form, action) {
                // (re) configure the form if we have (new) metaData
                if('load' === action.type && action.result.metaData) {
                    this.onMetaChange(this, action.result.metaData);
                    this.beforeAction(this, action);
                }
                // update bound data on successful submit
                else if('submit' == action.type) {
                    this.updateBoundData();
                }
                
            }}
        });
        this.form.trackResetOnLoad = true;
        

    } // eo function initComponent
    // }}}
    // {{{
    /**
     * private, changes order of execution in Ext.form.Action.Load::success
     * to allow reading of data in this server request (otherwise data would
     * be loaded to the form before onMetaChange is run from actioncomplete event
     */
    ,beforeAction:function(form, action) {
        action.success = function(response) {
            var result = this.processResponse(response);
            if(result === true || !result.success || !result.data){
                this.failureType = Ext.form.Action.LOAD_FAILURE;
                this.form.afterAction(this, false);
                return;
            }
            // original
//            this.form.clearInvalid();
//            this.form.setValues(result.data);
//            this.form.afterAction(this, true);

            this.form.afterAction(this, true);
            this.form.clearInvalid();
			this.form.setValues(action.result.metaData.data);
			           
        };
        
    } // eo function beforeAction
    // }}}
    // {{{
    /**
     * @param {Object} data A reference to on external data object. The idea is that form can display/change an external object
     */
    ,bind:function(data) {
        this.data = data;
        this.form.setValues(this.data);
    } // eo function bind
    // }}}
    // {{{
    /**
     * override this if you want a special buttons config
     */
    ,getButtons:function() {
        var buttons = [];
        if(Ext.isArray(this.createButtons)) {
            Ext.each(this.createButtons, function(name) {
                var button;
                switch(name) {
                    case 'meta':
                        button = this.getButton(name, {
                            handler:this.load.createDelegate(this, [{params:{meta:true}}])
                        });
                    break;

                    case 'load':
                        button = this.getButton(name, {
                             scope:this
                            ,handler:this.load
                        });
                    break;

                    case 'defaults':
                        button = this.getButton(name, {
                             scope:this
                            ,handler:this.setDefaultValues
                        });
                    break;

                    case 'reset':
                        button = this.getButton(name, {
                             scope:this
                            ,handler:this.reset
                        });
                    break;

                    case 'save':
                    case 'submit':
                        button = this.getButton(name, {
                            handler:this.submit.createDelegate(this, [{params:{cmd:'setPref'}}])
                        });
                    break;

                    case 'ok':
                        button = this.getButton(name, {
                             scope:this
                            ,handler:this.onOk
                        });
                    break;

                    case 'cancel':
                        button = this.getButton(name, {
                             scope:this
                            ,handler:this.onCancel
                        });
                    break;
                }
                if(button) {
                    Ext.apply(button, {
                        minWidth:this.buttonMinWidth
                    });
                    buttons.push(button);
                }
            }, this);
        };
        return buttons;
    } // eo function getButtons
    // }}}
    // {{{
    ,getOptions:function(o) {
        var options = {
             url:this.url
            ,method:this.method || 'post'
        };
        Ext.apply(options, o);
        options.params = Ext.apply(this.baseParams || {}, o.params);
        return options;
    } // eo function getOptions
    // }}}
    // {{{
    /**
     * @return {Object} object with name/value pairs using fields.getValue() methods
     */
    ,getValues:function() {
        var values = {};
        this.form.items.each(function(f) {
            values[f.name] = f.getValue();
        });
        return values;
    } // eo function getValues
    // }}}
    // {{{
    ,load:function(o) {
        var options = this.getOptions(o);
        if(this.loadingText) {
            options.waitMsg = this.loadingText;
        }
        this.form.load(options);
    } // eo function load
    // }}}
    // {{{
    /**
     * cancel button handler - fires cancel event only
     */
    ,onCancel:function() {
        this.fireEvent('cancel', this);
    } // eo function onCancel
    // }}}
    // {{{
    /**
     * Override this if you need a custom functionality
     *
     * @param {Ext.FormPanel} this
     * @param {Object} meta Metadata
     * @return void
     */
    ,onMetaChange:function(form, meta) {
        this.removeAll();

        // declare varables
        var columns, colIndex, tabIndex, ignore = {};

        this.add(new Ext.Toolbar({
            id: 'toolbar',
            renderTo: 'toolbar'
        }));

        // add column layout
        this.add(new Ext.TabPanel({
		    activeTab: 0,
		    frame: true,
                    border: true,
		    width: meta.formConfig.defaults.width,
		    layoutOnTabChange: true,
		    autoHeight: true
		    
		}));
        
        tabIndex = 0;

        if(Ext.isArray(this.ignoreFields)) {
            Ext.each(this.ignoreFields, function(f) {
                ignore[f] = true;
            });
        }
        this.items.get(0).render();
        if(typeof meta.buttons != 'undefined') {
            Ext.each(meta.buttons, function(item) {
                this.items.get(0).addButton({
                    text: item.title,
                    handler: function(e) {
                        window.open(item.href);
                    }
                });
                this.items.get(0).addSeparator();
            }, this);
        }
        // loop through metadata colums or fields
        // format follows grid column model structure
        Ext.each(meta.columns || meta.fields, function(item) {
            if(true === ignore[item.name]) {
                return;
            }
            
            this.items.get(1).add(new Ext.Panel({
            	title:	translation._(item.general.title),
            	layout:'form',
                padding: 10,
                labelWidth: 150,
            	defaultType: 'textfield'
            	,autoHeight: true
            }));
            
            colIndex = 0;
            
            columns = this.items.get(1).items.get(tabIndex);
            
            

            Ext.each(item.fields, function(field) {
             	
            	
		                     	
		        if(field.editor.regex){
                //alert(item.editor.regex);
                	field.editor.regex= new RegExp(field.editor.regex);
	            }
	            var config = Ext.apply({}, field.editor, {
	                 name:field.name || field.dataIndex
	                ,fieldLabel:field.fieldLabel || field.header
                        ,id: field.id || ''
	                ,width: field.editor.width ? parseInt(field.editor.width) : 580
	                ,heigth: field.editor.height ? field.editor.height : 22
	                ,xtype:field.editor && field.editor.xtype ? field.editor.xtype : 'textfield'
	                ,scope: this
	            });
	            
	            if('uploadfield' != config.xtype) {
	               Ext.apply(config, {
                       value: meta.data[field.name]  
                    });
	            }
                    
	            // handle regexps
	            if(config.editor && config.editor.regex) {
	                config.editor.regex = new RegExp(field.editor.regex);
	            }
	
	            // to avoid checkbox misalignment
	            if('checkbox' === config.xtype) {
	                Ext.apply(config, {
	                      boxLabel:' '
	                     ,checked:meta.data[field.name] 
                         ,value: 1
	                });
	            }
	            
	            if('htmleditorimage' === config.xtype) {
	                Ext.apply(config, {
	                	  xtype: 'htmleditorimage',
	                      plugins: [new Ext.ux.HTMLEditorImage(config.service),new Ext.ux.MediaButton(config.service)]
	                });
	            }
                
                    if('tinymce' === config.xtype) {
                        Ext.apply(config, {
                              xtype: 'tinymce',
                              width: 600,
                            height: 400,
                            tinymceSettings: {
                                theme : "advanced",
                                plugins: "safari,pagebreak,style,layer,table,advhr,advimage,advlink,emotions,iespell,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,noneditable,visualchars,nonbreaking,xhtmlxtras,template",
                                theme_advanced_buttons1 : "bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,|,styleselect,formatselect,fontselect,fontsizeselect",
                                theme_advanced_buttons2 : "cut,copy,paste,pastetext,pasteword,|,search,replace,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,link,unlink,anchor,image,cleanup,help,code,|,insertdate,inserttime,preview,|,forecolor,backcolor",
                                theme_advanced_buttons3 : "tablecontrols,|,hr,removeformat,visualaid,|,sub,sup,|,charmap,emotions,iespell,media,advhr,|,print,|,ltr,rtl,|",
                                theme_advanced_buttons4 : "insertlayer,moveforward,movebackward,absolute,|,styleprops,|,cite,abbr,acronym,del,ins,attribs,|,visualchars,nonbreaking,template,pagebreak",
                                theme_advanced_toolbar_location : "top",
                                theme_advanced_toolbar_align : "left",
                                theme_advanced_statusbar_location : "bottom",
                                theme_advanced_resizing : false,
                                extended_valid_elements : "a[name|href|target|title|onclick],img[class|src|border=0|alt|title|hspace|vspace|width|height|align|onmouseover|onmouseout|name],hr[class|width|size|noshade],font[face|size|color|style],span[class|align|style]",
                                template_external_list_url : "example_template_list.js"
                            },
                            id: 'text1',
                            name: 'text1'
                        });
                    }
                
	            if('checkbox' === config.xtype || 'xcheckbox' == config.xtype) {
	                Ext.apply(config, {
	                	 id: field.name,
                                 checked: meta.data[field.name]
                                 
	                });
	            }
	            if(typeof sid == 'undefined') sid = "";
	            if('combo' === config.xtype) {
	                Ext.apply(config, {
	                    store: new Ext.data.JsonStore({
						    url: '/service/?type=get&mode=system&service=' + config.service + '&sid=' + sid,
						    root: 'items',
						    fields: ['name', 'label']
						}),
	                    mode: 'remote',
	                    triggerAction: 'all',
	            		selectOnFocus:true,
	            		valueField:'name',
	               		displayField:'label',
	               		hiddenName: field.name
	                   
	                });
	            }
	            if('multiselect' === config.xtype) {
	                
	                var storee = new Ext.data.JsonStore({
						    url: '/service/?type=get&mode=system&service=' + config.service + '&sid=' + sid,
						    root: 'items',
						    fields: ['name', 'label']
					});
					
	                Ext.apply(config, {
	                    dataFields:["name", "label"],
	                    id: field.name,
	                    store : storee,
	            		valueField:'name',
	               		displayField:'label'
	                   
	                });
	                storee.load();
					
	            }
	            if(meta.formConfig.msgTarget) {
	                config.msgTarget = meta.formConfig.msgTarget;
	            }
                
                if(typeof config.disabled != 'undefined' && config.disabled == 'false') {
                    config.disabled = false;
                }
	
	            // add to columns on ltr principle
	            columns.add(config);
	            colIndex = colIndex == this.columnCount ? 0 : colIndex;
	            
            }, this);
            
			tabIndex = tabIndex+1;
			
        }, this);

        this.doLayout();
        Ext.each(meta.columns || meta.fields, function(item) {
	        Ext.each(item.fields, function(field) {
	                
	                
	                                
	                if(field.editor.regex){
	                
	                    field.editor.regex = new RegExp(field.editor.regex);
	                }
	                if('multiselect' === field.editor.xtype) {
	                    
			            Ext.getCmp(field.name).firstValues = meta.data[field.name];
			            
			            
			            //Ext.getCmp(field.name).updateImage(meta.data[field.name]);
	                }

                        if('xcheckbox' === field.editor.xtype) {
	                    //fieldCh = Ext.getCmp(field.name);
                            //fieldCh.value;
                            //if()
			    //fieldCh.checked = meta.data[field.name];
			    //Ext.getCmp(field.name).updateImage(meta.data[field.name]);
	                }
	                
	                if('uploadfield' === field.editor.xtype) {
	                    //Ext.getCmp(field.name).setValue(meta.data[field.name]);
	                    if(meta.data[field.name] != null && meta.data[field.name] != '' && meta.data[field.name].length == 40) {
	                    	Ext.getCmp(field.name).imageSrc = '/service/index?type=upload&mode=get&id=' + meta.data[field.name] + '&width=' + Ext.getCmp(field.name).width + '&height=' + (Ext.getCmp(field.name).height-2) + '&ratiomode=0';
			            	Ext.getCmp(field.name).setValue(Ext.getCmp(field.name).imageSrc, meta.data[field.name]);
			            	Ext.getCmp(field.name).updateImage(meta.data[field.name]);
			            }
			            
			            //
	                }

                        

	         }, this);
         }, this);
         
         
        
    } // eo function onMetaChange
    // }}}
    // {{{
    ,onOk:function() {
        tinyMCE.triggerSave();
        this.updateBoundData();        
        this.fireEvent('ok', this);
        
    }
    // }}}
    // {{{
    ,onRender:function() {
        // call parent
        Ext.ux.MetaForm.superclass.onRender.apply(this, arguments);

        this.form.waitMsgTarget = this.el;

        if(true === this.autoInit) {
            this.load({params:{meta:true}});
        }
        else if ('object' == typeof this.autoInit) {
            this.load(this.autoInit);
        }
    } // eo function onRender
    // }}}
    // {{{
    
    /**
     * private, removes all items from both formpanel and basic form
     */
    ,removeAll:function() {
        // remove border from header
        var hd = this.body.up('div.x-panel-bwrap').prev();
        if(hd) {
            hd.applyStyles({border:'none'});
        }
        // remove form panel items
        this.items.each(this.remove, this);

        // remove basic form items
        this.form.items.clear();
    } // eo function removeAllItems
    // }}}
    // {{{
    ,reset:function() {
        this.form.reset();
    } // eo function reset
    // }}}
    // {{{
    ,setDefaultValues:function() {
        this.form.items.each(function(item) {
            item.setValue(item.defaultValue);
        });
    } // eo function setDefaultValues
    // }}}
    // {{{
    ,submit:function(o) {
        tinyMCE.triggerSave();
        var options = this.getOptions(o);
        if(this.savingText) {
            options.waitMsg = this.savingText;
        }
        
        this.form.submit(options);
    } // eo function submit
    // }}}
    // {{{
    ,updateBoundData:function() {
        if(this.data) {
            Ext.apply(this.data, this.getValues());
        }
    } // eo function updateBoundData
    // }}}

});

// register xtype
Ext.reg('metaform', Ext.ux.MetaForm);

// eof  