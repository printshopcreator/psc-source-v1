var layouterDynamicSize = false;
var layouterDynamicWidth = false;
var layouterDynamicHeight = false;

function templatePrintRedirect(uuid, layouterid, load) {
    if(load && load == 3) {
        backwin();
        loadContacts();
        return;
    }

    if(load) {
        var load ="?load=1"
    }else{
        var load = "";
    }

    if(layouterid) {
        document.location = "/article/show/uuid/" + uuid + '/' + layouterid + load;
    }else{
        document.location = "/article/show/uuid/" + uuid + load;
    }
    
}

function templatePrintClose() {
    
    backwin();

}

function bindTemplatebrindButtons() {
    $("a[rel='#overlaytemp']").unbind();
    $("a[rel='#overlaytemp']").click(function() {

        $('#upload_mode').val('templateprint');
        var href = $(this).attr("href");

        var width = $(window).width()/100*90;
        var height = $(window).height()/100*90;

        $("#frame").attr('src', href);

        $('body').data('fullscreen', 2);

        $('#layouter_frame').css({width: width + 'px', height: height + 'px', top: ($(window).height()/2-(height/2)), left: ($(window).width()/2-(width/2))}).fadeIn();
        $('#overlay_frame').css({width: $(window).width(), height: $(window).height()}).show();
        scroll(0,0);
        return false;
    });

    $("a[rel='#overlaystep']").unbind();
    $("a[rel='#overlaystep']").click(function() {

        var href = $(this).attr("href");
        if(layouterDynamicSize) {
            href = href + '&dynamicSizeWidth=' + layouterDynamicWidth + '&dynamicSizeHeight=' + layouterDynamicHeight + '&dynamicSize=1';
        }

        document.location.href=href;

        return false;
    });

    $("a[rel='#overlaybanner']").unbind();
    $("a[rel='#overlaybanner']").click(function() {

        var href = $(this).attr("href");

        document.location.href=href;

        return false;
    });

    var templateprint_contact_uuid = "";

    $('.file_upload_templateprint').on('change', function(event) {

        var files = event.target.files;

        event.stopPropagation(); // Stop stuff happening
        event.preventDefault(); // Totally stop stuff happening

        // START A LOADING SPINNER HERE
        $('.modal-footer span').html("!!!BITTE WARTEN BILD WIRD HOCHGELADEN !!!");

        // Create a formdata object and add the files
        var data = new FormData();
        $.each(files, function(key, value)
        {
            data.append("logo1", value);
        });

        data.append("uuid", templateprint_contact_uuid);

        $.ajax({
            url: '/service/template/storeUpload',
            type: 'POST',
            data: data,
            cache: false,
            dataType: 'json',
            processData: false, // Don't process the files
            contentType: false, // Set content type to false as jQuery will tell the server its a query string request
            success: function (data, textStatus, jqXHR) {
                $('.modal-footer span').html("UPLOAD ABGESCHLOSSEN");
                if (typeof data.error === 'undefined') {
                    // Success so call function to process the form
                   $(".templateprint-upload-picture[data-uuid=" + templateprint_contact_uuid + "]").addClass("btn-success");
                }
                else {
                    // Handle errors here
                    console.log('ERRORS: ' + data.error);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                // Handle errors here
                console.log('ERRORS: ' + textStatus);
                // STOP LOADING SPINNER
            }
        });


    });

    $("a[rel='#templateprint-upload-picture']").unbind();
    $("a[rel='#templateprint-upload-picture']").click(function() {

        templateprint_contact_uuid = $(this).attr("data-uuid");
        var element = $(this).prev();
        element.click();
        return false;
    });
}
