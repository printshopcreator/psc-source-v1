Ext.BLANK_IMAGE_URL = "/scripts/extjs/resources/images/default/s.gif";
Ext.QuickTips.init();

Date.patterns = {
    ISO8601Long:"Y-m-d H:i:s",
    ISO8601Short:"Y-m-d",
    ISO8601Time:"H:i:s",
    ShortDate: "n/j/Y",
    LongDate: "l, F d, Y",
    FullDateTime: "l, F d, Y g:i:s A",
    MonthDay: "F d",
    ShortTime: "g:i A",
    LongTime: "g:i:s A",
    SortableDateTime: "Y-m-d\\TH:i:s",
    UniversalSortableDateTime: "Y-m-d H:i:sO",
    YearMonth: "F, Y"
};

Locale.prototype.TranslationLists={
    Date:{
        full:"L, j. F Y",
        "long":"d. F Y",
        medium:"d.m.Y",
        "short":"d.m.y"
    },
    Time:{
        full:"H:i:s v",
        "long":"H:i:s T",
        medium:"H:i:s",
        "short":"H:i"
    },
    DateTime:{
        Ed:"E d",
        H:"H",
        HHmm:"H:i",
        HHmmss:"H:i:s",
        MMMEd:"E M d",
        MMMMd:"d. F",
        MMMMdd:"d. F",
        MMd:"d/m",
        MMdd:"d.m.",
        Md:"d.n.",
        hhmm:"g:i A",
        hhmmss:"g:i:s A",
        mmss:"i:s",
        yyMM:"m.y",
        yyMMM:"M y",
        yyMMdd:"d.m.y",
        yyQ:"Q y",
        yyQQQQ:"QQQQ y",
        yyyy:"Y",
        yyyyMMMM:"F Y"
    },
    Month:{
        "1":"Januar",
        "2":"Februar",
        "3":"März",
        "4":"April",
        "5":"Mai",
        "6":"Juni",
        "7":"Juli",
        "8":"August",
        "9":"September",
        "10":"Oktober",
        "11":"November",
        "12":"Dezember"
    },
    Day:{
        sun:"Sonntag",
        mon:"Montag",
        tue:"Dienstag",
        wed:"Mittwoch",
        thu:"Donnerstag",
        fri:"Freitag",
        sat:"Samstag"
    },
    Symbols:{
        decimal:",",
        group:".",
        list:";",
        percent:"%",
        zero:"0",
        pattern:"#",
        plus:"+",
        minus:"-",
        exponent:"E",
        mille:"‰",
        infinity:"∞",
        nan:"NaN"
    },
    Question:{
        yes:"ja:j",
        no:"nein:n"
    },
    Language:{
        aa:"Afar",
        ab:"Abchasisch",
        ace:"Aceh-Sprache",
        ach:"Acholi-Sprache",
        ada:"Adangme",
        ady:"Adygai",
        ae:"Avestisch",
        af:"Afrikaans",
        afa:"Afro-Asiatische Sprachen  (Andere)",
        afh:"Afrihili",
        ain:"Ainu-Sprache",
        ak:"Akan",
        akk:"Akkadisch",
        ale:"Aleutisch",
        alg:"Algonkin-Sprachen",
        alt:"Süd-Altaisch",
        am:"Amharisch",
        an:"Aragonesisch",
        ang:"Altenglisch",
        anp:"Angika",
        apa:"Apachen-Sprache",
        ar:"Arabisch",
        arc:"Aramäisch",
        arn:"Araukanisch",
        arp:"Arapaho-Sprache",
        art:"Kunstsprachen (Andere)",
        arw:"Arawak-Sprachen",
        as:"Assamesisch",
        ast:"Asturianisch",
        ath:"Athapaskische Sprachen",
        aus:"Australische Sprachen",
        av:"Awarisch",
        awa:"Awadhi",
        ay:"Aymara",
        az:"Aserbaidschanisch",
        ba:"Baschkirisch",
        bad:"Banda-Sprache",
        bai:"Bamileke-Sprache",
        bal:"Belutschisch",
        ban:"Balinesisch",
        bas:"Basaa-Sprache",
        bat:"Baltische Sprachen (Andere)",
        be:"Weißrussisch",
        bej:"Bedauye",
        bem:"Bemba-Sprache",
        ber:"Berbersprachen (Andere)",
        bg:"Bulgarisch",
        bh:"Biharisch",
        bho:"Bhodschpuri",
        bi:"Bislama",
        bik:"Bikol-Sprache",
        bin:"Bini-Sprache",
        bla:"Blackfoot-Sprache",
        bm:"Bambara-Sprache",
        bn:"Bengalisch",
        bnt:"Bantusprachen (Andere)",
        bo:"Tibetisch",
        br:"Bretonisch",
        bra:"Braj-Bhakha",
        bs:"Bosnisch",
        btk:"Batak",
        bua:"Burjatisch",
        bug:"Buginesisch",
        byn:"Blin",
        ca:"Katalanisch",
        cad:"Caddo",
        cai:"Indianersprachen, Zentralamerika (Andere)",
        car:"Karibische Sprachen",
        cau:"Kaukasische Sprachen (Andere)",
        cch:"Atsam",
        ce:"Tschetschenisch",
        ceb:"Cebuano",
        cel:"Keltische Sprachen (Andere)",
        ch:"Chamorro-Sprache",
        chb:"Chibcha-Sprachen",
        chg:"Tschagataisch",
        chk:"Trukesisch",
        chm:"Tscheremissisch",
        chn:"Chinook",
        cho:"Choctaw",
        chp:"Chipewyan",
        chr:"Cherokee",
        chy:"Cheyenne",
        cmc:"Cham-Sprachen",
        co:"Korsisch",
        cop:"Koptisch",
        cpe:"Kreolisch-Englisch (Andere)",
        cpf:"Kreolisch-Französisch (Andere)",
        cpp:"Kreolisch-Portugiesisch (Andere)",
        cr:"Cree",
        crh:"Krimtatarisch",
        crp:"Kreolische Sprachen",
        cs:"Tschechisch",
        csb:"Kaschubisch",
        cu:"Kirchenslawisch",
        cus:"Kuschitische Sprachen (Andere)",
        cv:"Tschuwaschisch",
        cy:"Kymrisch",
        da:"Dänisch",
        dak:"Dakota-Sprache",
        dar:"Darginisch",
        day:"Dajak",
        de:"Deutsch",
        del:"Delaware-Sprache",
        den:"Slave (Athapaskische Sprachen)",
        dgr:"Dogrib",
        din:"Dinka-Sprache",
        doi:"Dogri",
        dra:"Drawidische Sprachen (Andere)",
        dsb:"Niedersorbisch",
        dua:"Duala",
        dum:"Mittelniederländisch",
        dv:"Maledivisch",
        dyu:"Dyula-Sprache",
        dz:"Bhutanisch",
        ee:"Ewe-Sprache",
        efi:"Efik",
        egy:"Ägyptisch",
        eka:"Ekajuk",
        el:"Griechisch",
        elx:"Elamisch",
        en:"Englisch",
        en_US:"Englisch (USA)",
        enm:"Mittelenglisch",
        eo:"Esperanto",
        es:"Spanisch",
        et:"Estnisch",
        eu:"Baskisch",
        ewo:"Ewondo",
        fa:"Persisch",
        fan:"Pangwe-Sprache",
        fat:"Fanti-Sprache",
        ff:"Ful",
        fi:"Finnisch",
        fil:"Filipino",
        fiu:"Finnougrische Sprachen (Andere)",
        fj:"Fidschianisch",
        fo:"Färöisch",
        fon:"Fon-Sprache",
        fr:"Französisch",
        frm:"Mittelfranzösisch",
        fro:"Altfranzösisch",
        frr:"Nordfriesisch",
        frs:"Ostfriesisch",
        fur:"Friulisch",
        fy:"Friesisch",
        ga:"Gälisch",
        gaa:"Ga-Sprache",
        gay:"Gayo",
        gba:"Gbaya-Sprache",
        gd:"Schottisches Gälisch",
        gem:"Germanische Sprachen (Andere)",
        gez:"Geez",
        gil:"Gilbertesisch",
        gl:"Galizisch",
        gmh:"Mittelhochdeutsch",
        gn:"Guarani",
        goh:"Althochdeutsch",
        gon:"Gondi-Sprache",
        gor:"Mongondou",
        got:"Gotisch",
        grb:"Grebo-Sprache",
        grc:"Altgriechisch",
        gsw:"Schweizerdeutsch",
        gu:"Gujarati",
        gv:"Manx",
        gwi:"Kutchin-Sprache",
        ha:"Hausa",
        hai:"Haida-Sprache",
        haw:"Hawaiianisch",
        he:"Hebräisch",
        hi:"Hindi",
        hil:"Hiligaynon-Sprache",
        him:"Himachali",
        hit:"Hethitisch",
        hmn:"Miao-Sprachen",
        ho:"Hiri-Motu",
        hr:"Kroatisch",
        hsb:"Obersorbisch",
        ht:"Kreolisch",
        hu:"Ungarisch",
        hup:"Hupa",
        hy:"Armenisch",
        hz:"Herero-Sprache",
        ia:"Interlingua",
        iba:"Iban",
        id:"Indonesisch",
        ie:"Interlingue",
        ig:"Igbo-Sprache",
        ii:"Sichuan Yi",
        ijo:"Ijo-Sprache",
        ik:"Inupiak",
        ilo:"Ilokano-Sprache",
        inc:"Indoarische Sprachen (Andere)",
        ine:"Indogermanische Sprachen (Andere)",
        inh:"Ingush",
        io:"Ido-Sprache",
        ira:"Iranische Sprachen (Andere)",
        iro:"Irokesische Sprachen",
        is:"Isländisch",
        it:"Italienisch",
        iu:"Inukitut",
        ja:"Japanisch",
        jbo:"Lojban",
        jpr:"Jüdisch-Persisch",
        jrb:"Jüdisch-Arabisch",
        jv:"Javanisch",
        ka:"Georgisch",
        kaa:"Karakalpakisch",
        kab:"Kabylisch",
        kac:"Kachin-Sprache",
        kaj:"Jju",
        kam:"Kamba",
        kar:"Karenisch",
        kaw:"Kawi",
        kbd:"Kabardinisch",
        kcg:"Tyap",
        kfo:"Koro",
        kg:"Kongo",
        kha:"Khasi-Sprache",
        khi:"Khoisan-Sprachen (Andere)",
        kho:"Sakisch",
        ki:"Kikuyu-Sprache",
        kj:"Kwanyama",
        kk:"Kasachisch",
        kl:"Grönländisch",
        km:"Kambodschanisch",
        kmb:"Kimbundu-Sprache",
        kn:"Kannada",
        ko:"Koreanisch",
        kok:"Konkani",
        kos:"Kosraeanisch",
        kpe:"Kpelle-Sprache",
        kr:"Kanuri-Sprache",
        krc:"Karatschaiisch-Balkarisch",
        krl:"Karelisch",
        kro:"Kru-Sprachen",
        kru:"Oraon-Sprache",
        ks:"Kaschmirisch",
        ku:"Kurdisch",
        kum:"Kumükisch",
        kut:"Kutenai-Sprache",
        kv:"Komi-Sprache",
        kw:"Kornisch",
        ky:"Kirgisisch",
        la:"Latein",
        lad:"Judenspanisch",
        lah:"Lahnda",
        lam:"Lamba-Sprache",
        lb:"Luxemburgisch",
        lez:"Lesgisch",
        lg:"Ganda-Sprache",
        li:"Limburgisch",
        ln:"Lingala",
        lo:"Laotisch",
        lol:"Mongo",
        loz:"Rotse-Sprache",
        lt:"Litauisch",
        lu:"Luba",
        lua:"Luba-Lulua",
        lui:"Luiseno-Sprache",
        lun:"Lunda-Sprache",
        luo:"Luo-Sprache",
        lus:"Lushai-Sprache",
        lv:"Lettisch",
        mad:"Maduresisch",
        mag:"Khotta",
        mai:"Maithili",
        mak:"Makassarisch",
        man:"Manding-Sprache",
        map:"Austronesische Sprachen",
        mas:"Massai-Sprache",
        mdf:"Moksha",
        mdr:"Mandaresisch",
        men:"Mende-Sprache",
        mg:"Madagassisch",
        mga:"Mittelirisch",
        mh:"Marschallesisch",
        mi:"Maori",
        mic:"Micmac-Sprache",
        min:"Minangkabau-Sprache",
        mis:"Verschiedene Sprachen",
        mk:"Mazedonisch",
        mkh:"Mon-Khmer-Sprachen (Andere)",
        ml:"Malayalam",
        mn:"Mongolisch",
        mnc:"Mandschurisch",
        mni:"Meithei-Sprache",
        mno:"Manobo-Sprache",
        mo:"Moldauisch",
        moh:"Mohawk-Sprache",
        mos:"Mossi-Sprache",
        mr:"Marathi",
        ms:"Malaiisch",
        mt:"Maltesisch",
        mul:"Polyglott",
        mun:"Munda-Sprachen",
        mus:"Muskogee-Sprachen",
        mwl:"Mirandesisch",
        mwr:"Marwari",
        my:"Birmanisch",
        myn:"Maya-Sprachen",
        myv:"Erzya",
        na:"Nauruisch",
        nah:"Nahuatl",
        nai:"Indianersprachen, Nordamerika (Andere)",
        nap:"Neapolitanisch",
        nb:"Norwegisch Bokmål",
        nd:"Ndebele-Sprache (Nord)",
        nds:"Niederdeutsch",
        ne:"Nepalesisch",
        "new":"Newari",
        ng:"Ndonga",
        nia:"Nias-Sprache",
        nic:"Nigerkordofanische Sprachen (Andere)",
        niu:"Niue-Sprache",
        nl:"Niederländisch",
        nl_BE:"Flämisch",
        nn:"Norwegisch Nynorsk",
        no:"Norwegisch",
        nog:"Nogai",
        non:"Altnordisch",
        nqo:"N’Ko",
        nr:"Ndebele-Sprache (Süd)",
        nso:"Sotho-Sprache (Nord)",
        nub:"Nubische Sprachen",
        nv:"Navajo-Sprache",
        nwc:"Alt-Newari",
        ny:"Chewa-Sprache",
        nym:"Nyamwezi-Sprache",
        nyn:"Nyankole",
        nyo:"Nyoro",
        nzi:"Nzima",
        oc:"Okzitanisch",
        oj:"Ojibwa-Sprache",
        om:"Oromo",
        or:"Orija",
        os:"Ossetisch",
        osa:"Osage-Sprache",
        ota:"Osmanisch",
        oto:"Otomangue-Sprachen",
        pa:"Pandschabisch",
        paa:"Papuasprachen (Andere)",
        pag:"Pangasinan-Sprache",
        pal:"Mittelpersisch",
        pam:"Pampanggan-Sprache",
        pap:"Papiamento",
        pau:"Palau",
        peo:"Altpersisch",
        phi:"Philippinen-Austronesisch (Andere)",
        phn:"Phönikisch",
        pi:"Pali",
        pl:"Polnisch",
        pon:"Ponapeanisch",
        pra:"Prakrit",
        pro:"Altprovenzalisch",
        ps:"Paschtu",
        pt:"Portugiesisch",
        qu:"Quechua",
        raj:"Rajasthani",
        rap:"Osterinsel-Sprache",
        rar:"Rarotonganisch",
        rm:"Rätoromanisch",
        rn:"Rundi-Sprache",
        ro:"Rumänisch",
        roa:"Romanische Sprachen (Andere)",
        rom:"Zigeunersprache",
        root:"Root",
        ru:"Russisch",
        rup:"Aromunisch",
        rw:"Ruandisch",
        sa:"Sanskrit",
        sad:"Sandawe-Sprache",
        sah:"Jakutisch",
        sai:"Indianersprachen, Südamerika (Andere)",
        sal:"Salish-Sprache",
        sam:"Samaritanisch",
        sas:"Sasak",
        sat:"Santali",
        sc:"Sardisch",
        scn:"Sizilianisch",
        sco:"Schottisch",
        sd:"Sindhi",
        se:"Nord-Samisch",
        sel:"Selkupisch",
        sem:"Semitische Sprachen (Andere)",
        sg:"Sango",
        sga:"Altirisch",
        sgn:"Gebärdensprache",
        sh:"Serbo-Kroatisch",
        shn:"Schan-Sprache",
        si:"Singhalesisch",
        sid:"Sidamo",
        sio:"Sioux-Sprachen",
        sit:"Sinotibetische Sprachen (Andere)",
        sk:"Slowakisch",
        sl:"Slowenisch",
        sla:"Slawische Sprachen (Andere)",
        sm:"Samoanisch",
        sma:"Süd-Samisch",
        smi:"Lappisch",
        smj:"Lule-Lappisch",
        smn:"Inari-Lappisch",
        sms:"Skolt-Lappisch",
        sn:"Shona",
        snk:"Soninke-Sprache",
        so:"Somali",
        sog:"Sogdisch",
        son:"Songhai-Sprache",
        sq:"Albanisch",
        sr:"Serbisch",
        srn:"Srananisch",
        srr:"Serer-Sprache",
        ss:"Swazi",
        ssa:"Nilosaharanische Sprachen (Andere)",
        st:"Süd-Sotho-Sprache",
        su:"Sundanesisch",
        suk:"Sukuma-Sprache",
        sus:"Susu",
        sux:"Sumerisch",
        sv:"Schwedisch",
        sw:"Suaheli",
        syr:"Syrisch",
        ta:"Tamilisch",
        tai:"Thaisprachen (Andere)",
        te:"Telugu",
        tem:"Temne",
        ter:"Tereno-Sprache",
        tet:"Tetum-Sprache",
        tg:"Tadschikisch",
        th:"Thai",
        ti:"Tigrinja",
        tig:"Tigre",
        tiv:"Tiv-Sprache",
        tk:"Turkmenisch",
        tkl:"Tokelauanisch",
        tl:"Tagalog",
        tlh:"Klingonisch",
        tli:"Tlingit-Sprache",
        tmh:"Tamaseq",
        tn:"Tswana-Sprache",
        to:"Tongaisch",
        tog:"Tonga (Nyasa)",
        tpi:"Neumelanesisch",
        tr:"Türkisch",
        ts:"Tsonga",
        tsi:"Tsimshian-Sprache",
        tt:"Tatarisch",
        tum:"Tumbuka-Sprache",
        tup:"Tupi-Sprachen",
        tut:"Altaische Sprachen (Andere)",
        tvl:"Elliceanisch",
        tw:"Twi",
        ty:"Tahitisch",
        tyv:"Tuwinisch",
        udm:"Udmurtisch",
        ug:"Uigurisch",
        uga:"Ugaritisch",
        uk:"Ukrainisch",
        umb:"Mbundu-Sprache",
        und:"Sprache nicht ermittelt",
        ur:"Urdu",
        uz:"Usbekisch",
        vai:"Vai-Sprache",
        ve:"Venda-Sprache",
        vi:"Vietnamesisch",
        vo:"Volapük",
        vot:"Wotisch",
        wa:"Wallonisch",
        wak:"Wakashanisch",
        wal:"Walamo-Sprache",
        war:"Waray",
        was:"Washo-Sprache",
        wen:"Sorbisch",
        wo:"Wolof",
        xal:"Kalmückisch",
        xh:"Xhosa",
        yao:"Yao-Sprache",
        yap:"Yapesisch",
        yi:"Jiddisch",
        yo:"Yoruba",
        ypk:"Yupik-Sprache",
        za:"Zhuang",
        zap:"Zapotekisch",
        zen:"Zenaga",
        zh:"Chinesisch",
        zh_Hans:"Chinesisch (vereinfacht)",
        zh_Hant:"Chinesisch (traditionell)",
        znd:"Zande-Sprache",
        zu:"Zulu",
        zun:"Zuni-Sprache",
        zxx:"Keine Sprachinhalte",
        zza:"Zaza"
    },
    Territory:{
        "001":"Welt",
        "002":"Afrika",
        "003":"Nordamerika",
        "005":"Südamerika",
        "009":"Ozeanien",
        "011":"Westafrika",
        "013":"Mittelamerika",
        "014":"Ostafrika",
        "015":"Nordafrika",
        "017":"Zentralafrika",
        "018":"Südliches Afrika",
        "019":"Nord-, Mittel- und Südamerika",
        "021":"Nördliches Amerika",
        "029":"Karibik",
        "030":"Ostasien",
        "034":"Südasien",
        "035":"Südostasien",
        "039":"Südeuropa",
        "053":"Australien und Neuseeland",
        "054":"Melanesien",
        "057":"Mikronesisches Inselgebiet",
        "061":"Polynesien",
        "062":"Süd-Zentralasien",
        "142":"Asien",
        "143":"Zentralasien",
        "145":"Westasien",
        "150":"Europa",
        "151":"Osteuropa",
        "154":"Nordeuropa",
        "155":"Westeuropa",
        "172":"Gemeinschaft Unabhängiger Staaten",
        "419":"Lateinamerika und Karibik",
        "830":"Kanalinseln",
        AD:"Andorra",
        AE:"Vereinigte Arabische Emirate",
        AF:"Afghanistan",
        AG:"Antigua und Barbuda",
        AI:"Anguilla",
        AL:"Albanien",
        AM:"Armenien",
        AN:"Niederländische Antillen",
        AO:"Angola",
        AQ:"Antarktis",
        AR:"Argentinien",
        AS:"Amerikanisch-Samoa",
        AT:"Österreich",
        AU:"Australien",
        AW:"Aruba",
        AX:"Alandinseln",
        AZ:"Aserbaidschan",
        BA:"Bosnien und Herzegowina",
        BB:"Barbados",
        BD:"Bangladesch",
        BE:"Belgien",
        BF:"Burkina Faso",
        BG:"Bulgarien",
        BH:"Bahrain",
        BI:"Burundi",
        BJ:"Benin",
        BM:"Bermuda",
        BN:"Brunei Darussalam",
        BO:"Bolivien",
        BR:"Brasilien",
        BS:"Bahamas",
        BT:"Bhutan",
        BV:"Bouvetinsel",
        BW:"Botsuana",
        BY:"Belarus",
        BZ:"Belize",
        CA:"Kanada",
        CC:"Kokosinseln (Keeling)",
        CD:"Demokratische Republik Kongo",
        CF:"Zentralafrikanische Republik",
        CG:"Kongo",
        CH:"Schweiz",
        CI:"Côte d’Ivoire",
        CK:"Cookinseln",
        CL:"Chile",
        CM:"Kamerun",
        CN:"China",
        CO:"Kolumbien",
        CR:"Costa Rica",
        CS:"Serbien und Montenegro",
        CU:"Kuba",
        CV:"Kap Verde",
        CX:"Weihnachtsinsel",
        CY:"Zypern",
        CZ:"Tschechische Republik",
        DE:"Deutschland",
        DJ:"Dschibuti",
        DK:"Dänemark",
        DM:"Dominica",
        DO:"Dominikanische Republik",
        DZ:"Algerien",
        EC:"Ecuador",
        EE:"Estland",
        EG:"Ägypten",
        EH:"Westsahara",
        ER:"Eritrea",
        ES:"Spanien",
        ET:"Äthiopien",
        FI:"Finnland",
        FJ:"Fidschi",
        FK:"Falklandinseln",
        FM:"Mikronesien",
        FO:"Färöer",
        FR:"Frankreich",
        GA:"Gabun",
        GB:"Vereinigtes Königreich",
        GD:"Grenada",
        GE:"Georgien",
        GF:"Französisch-Guayana",
        GG:"Guernsey",
        GH:"Ghana",
        GI:"Gibraltar",
        GL:"Grönland",
        GM:"Gambia",
        GN:"Guinea",
        GP:"Guadeloupe",
        GQ:"Äquatorialguinea",
        GR:"Griechenland",
        GS:"Südgeorgien und die Südlichen Sandwichinseln",
        GT:"Guatemala",
        GU:"Guam",
        GW:"Guinea-Bissau",
        GY:"Guyana",
        HK:"Sonderverwaltungszone Hongkong",
        HM:"Heard- und McDonald-Inseln",
        HN:"Honduras",
        HR:"Kroatien",
        HT:"Haiti",
        HU:"Ungarn",
        ID:"Indonesien",
        IE:"Irland",
        IL:"Israel",
        IM:"Isle of Man",
        IN:"Indien",
        IO:"Britisches Territorium im Indischen Ozean",
        IQ:"Irak",
        IR:"Iran",
        IS:"Island",
        IT:"Italien",
        JE:"Jersey",
        JM:"Jamaika",
        JO:"Jordanien",
        JP:"Japan",
        KE:"Kenia",
        KG:"Kirgisistan",
        KH:"Kambodscha",
        KI:"Kiribati",
        KM:"Komoren",
        KN:"St. Kitts und Nevis",
        KP:"Demokratische Volksrepublik Korea",
        KR:"Republik Korea",
        KW:"Kuwait",
        KY:"Kaimaninseln",
        KZ:"Kasachstan",
        LA:"Laos",
        LB:"Libanon",
        LC:"St. Lucia",
        LI:"Liechtenstein",
        LK:"Sri Lanka",
        LR:"Liberia",
        LS:"Lesotho",
        LT:"Litauen",
        LU:"Luxemburg",
        LV:"Lettland",
        LY:"Libyen",
        MA:"Marokko",
        MC:"Monaco",
        MD:"Republik Moldau",
        ME:"Montenegro",
        MG:"Madagaskar",
        MH:"Marshallinseln",
        MK:"Mazedonien",
        ML:"Mali",
        MM:"Myanmar",
        MN:"Mongolei",
        MO:"Sonderverwaltungszone Macao",
        MP:"Nördliche Marianen",
        MQ:"Martinique",
        MR:"Mauretanien",
        MS:"Montserrat",
        MT:"Malta",
        MU:"Mauritius",
        MV:"Malediven",
        MW:"Malawi",
        MX:"Mexiko",
        MY:"Malaysia",
        MZ:"Mosambik",
        NA:"Namibia",
        NC:"Neukaledonien",
        NE:"Niger",
        NF:"Norfolkinsel",
        NG:"Nigeria",
        NI:"Nicaragua",
        NL:"Niederlande",
        NO:"Norwegen",
        NP:"Nepal",
        NR:"Nauru",
        NU:"Niue",
        NZ:"Neuseeland",
        OM:"Oman",
        PA:"Panama",
        PE:"Peru",
        PF:"Französisch-Polynesien",
        PG:"Papua-Neuguinea",
        PH:"Philippinen",
        PK:"Pakistan",
        PL:"Polen",
        PM:"St. Pierre und Miquelon",
        PN:"Pitcairn",
        PR:"Puerto Rico",
        PS:"Palästinensische Gebiete",
        PT:"Portugal",
        PW:"Palau",
        PY:"Paraguay",
        QA:"Katar",
        QO:"Äußeres Ozeanien",
        QU:"Europäische Union",
        RE:"Réunion",
        RO:"Rumänien",
        RS:"Serbien",
        RU:"Russische Föderation",
        RW:"Ruanda",
        SA:"Saudi-Arabien",
        SB:"Salomonen",
        SC:"Seychellen",
        SD:"Sudan",
        SE:"Schweden",
        SG:"Singapur",
        SH:"St. Helena",
        SI:"Slowenien",
        SJ:"Svalbard und Jan Mayen",
        SK:"Slowakei",
        SL:"Sierra Leone",
        SM:"San Marino",
        SN:"Senegal",
        SO:"Somalia",
        SR:"Suriname",
        ST:"São Tomé und Príncipe",
        SV:"El Salvador",
        SY:"Syrien",
        SZ:"Swasiland",
        TC:"Turks- und Caicosinseln",
        TD:"Tschad",
        TF:"Französische Süd- und Antarktisgebiete",
        TG:"Togo",
        TH:"Thailand",
        TJ:"Tadschikistan",
        TK:"Tokelau",
        TL:"Osttimor",
        TM:"Turkmenistan",
        TN:"Tunesien",
        TO:"Tonga",
        TR:"Türkei",
        TT:"Trinidad und Tobago",
        TV:"Tuvalu",
        TW:"Taiwan",
        TZ:"Tansania",
        UA:"Ukraine",
        UG:"Uganda",
        UM:"Amerikanisch-Ozeanien",
        US:"Vereinigte Staaten",
        UY:"Uruguay",
        UZ:"Usbekistan",
        VA:"Vatikanstadt",
        VC:"St. Vincent und die Grenadinen",
        VE:"Venezuela",
        VG:"Britische Jungferninseln",
        VI:"Amerikanische Jungferninseln",
        VN:"Vietnam",
        VU:"Vanuatu",
        WF:"Wallis und Futuna",
        WS:"Samoa",
        YE:"Jemen",
        YT:"Mayotte",
        ZA:"Südafrika",
        ZM:"Sambia",
        ZW:"Simbabwe",
        ZZ:"Unbekannte oder ungültige Region"
    },
    CityToTimezone:{
        "Etc/Unknown":"Unbekannt",
        "Europe/Tirane":"Tirana",
        "Asia/Yerevan":"Erivan",
        "America/Curacao":"Curaçao",
        "Antarctica/South_Pole":"Südpol",
        "Antarctica/Vostok":"Wostok",
        "Antarctica/DumontDUrville":"Dumont D'Urville",
        "Europe/Vienna":"Wien",
        "Europe/Brussels":"Brüssel",
        "Africa/Ouagadougou":"Wagadugu",
        "Atlantic/Bermuda":"Bermudas",
        "America/St_Johns":"St. John's",
        "Europe/Zurich":"Zürich",
        "Pacific/Easter":"Osterinsel",
        "America/Havana":"Havanna",
        "Atlantic/Cape_Verde":"Kap Verde",
        "Indian/Christmas":"Weihnachts-Inseln",
        "Asia/Nicosia":"Nikosia",
        "Africa/Djibouti":"Dschibuti",
        "Europe/Copenhagen":"Kopenhagen",
        "Africa/Algiers":"Algier",
        "Africa/Cairo":"Kairo",
        "Africa/El_Aaiun":"El Aaiún",
        "Atlantic/Canary":"Kanaren",
        "Africa/Addis_Ababa":"Addis Abeba",
        "Pacific/Fiji":"Fidschi",
        "Atlantic/Faeroe":"Färöer",
        "Asia/Tbilisi":"Tiflis",
        "Africa/Accra":"Akkra",
        "Europe/Athens":"Athen",
        "Atlantic/South_Georgia":"Süd-Georgien",
        "Asia/Hong_Kong":"Hongkong",
        "Asia/Baghdad":"Bagdad",
        "Asia/Tehran":"Teheran",
        "Europe/Rome":"Rom",
        "America/Jamaica":"Jamaika",
        "Asia/Tokyo":"Tokio",
        "Asia/Bishkek":"Bischkek",
        "Indian/Comoro":"Komoren",
        "America/St_Kitts":"St. Kitts",
        "Asia/Pyongyang":"Pjöngjang",
        "America/Cayman":"Kaimaninseln",
        "Asia/Aqtobe":"Aktobe",
        "America/St_Lucia":"St. Lucia",
        "Europe/Vilnius":"Wilna",
        "Europe/Luxembourg":"Luxemburg",
        "Africa/Tripoli":"Tripolis",
        "Europe/Chisinau":"Kischinau",
        "Asia/Macau":"Macao",
        "Indian/Maldives":"Malediven",
        "America/Mexico_City":"Mexiko-Stadt",
        "Africa/Niamey":"Niger",
        "Asia/Muscat":"Muskat",
        "Europe/Warsaw":"Warschau",
        "Atlantic/Azores":"Azoren",
        "Europe/Lisbon":"Lissabon",
        "America/Asuncion":"Asunción",
        "Asia/Qatar":"Katar",
        "Indian/Reunion":"Réunion",
        "Europe/Bucharest":"Bukarest",
        "Europe/Moscow":"Moskau",
        "Asia/Yekaterinburg":"Jekaterinburg",
        "Asia/Novosibirsk":"Nowosibirsk",
        "Asia/Krasnoyarsk":"Krasnojarsk",
        "Asia/Yakutsk":"Jakutsk",
        "Asia/Vladivostok":"Wladiwostok",
        "Asia/Sakhalin":"Sachalin",
        "Asia/Kamchatka":"Kamtschatka",
        "Asia/Riyadh":"Riad",
        "Africa/Khartoum":"Khartum",
        "Asia/Singapore":"Singapur",
        "Atlantic/St_Helena":"St. Helena",
        "Africa/Mogadishu":"Mogadischu",
        "Africa/Sao_Tome":"São Tomé",
        "America/El_Salvador":"Salvador",
        "Asia/Damascus":"Damaskus",
        "Asia/Dushanbe":"Duschanbe",
        "America/Port_of_Spain":"Port-of-Spain",
        "Asia/Taipei":"Taipeh",
        "Africa/Dar_es_Salaam":"Daressalam",
        "Europe/Uzhgorod":"Uschgorod",
        "Europe/Kiev":"Kiew",
        "Europe/Zaporozhye":"Saporischja",
        "Asia/Tashkent":"Taschkent",
        "America/St_Vincent":"St. Vincent",
        "America/St_Thomas":"St. Thomas"
    }
};

Ext.LinkButton = Ext.extend(Ext.Button, {
    template: new Ext.Template(
        '<table cellspacing="0" class="x-btn {3}"><tbody class="{4}">',
        '<tr><td class="x-btn-tl"><i>&#160;</i></td><td class="x-btn-tc"></td><td class="x-btn-tr"><i>&#160;</i></td></tr>',
        '<tr><td class="x-btn-ml"><i>&#160;</i></td><td class="x-btn-mc"><em class="{5}" unselectable="on"><a href="{6}" style="display:block" target="{7}" class="x-btn-text {2}">{0}</a></em></td><td class="x-btn-mr"><i>&#160;</i></td></tr>',
        '<tr><td class="x-btn-bl"><i>&#160;</i></td><td class="x-btn-bc"></td><td class="x-btn-br"><i>&#160;</i></td></tr>',
        '</tbody></table>').compile(),

    buttonSelector : 'a:first',

    baseParams: {},

    params: {},

    getTemplateArgs: function() {
        return Ext.Button.prototype.getTemplateArgs.apply(this).concat([this.getHref(), this.target]);
    },

    onClick : function(e){
        if(e.button != 0){
            return;
        }
        if(!this.disabled){
            if (this.fireEvent("click", this, e) == false) {
                e.stopEvent();
            } else {
                if(this.handler){
                    this.handler.call(this.scope || this, this, e);
                }
            }
        }
    },

    getHref: function() {
        var result = this.href;
        alert(result);
        var p = Ext.urlEncode(Ext.apply(Ext.apply({}, this.baseParams), this.params));
        if (p.length) {
            result += ((this.href.indexOf('?') == -1) ? '?' : '&') + p;
        }
        return result;
    },

    setParams: function(p) {
        this.params = p;
        this.el.child(this.buttonSelector, true).href = this.getHref();
    }
});
Ext.reg('linkbutton', Ext.LinkButton);


/**
 * @copyright (c) 2010, by Otavio Augusto R. Fernandes
 * @date      22. February 2010
 * @version   $Id: FieldGroup.js 10 2010-03-10 11:47:15Z oaugusts $
 * @required  3.2.x
 */

Ext.ns('Ext.ux.form');

Ext.ux.form.FieldGroup = Ext.extend(Ext.Container,{
    /**
     * @property defaultMargins
     * @type String
     * The margins to apply by default to each field in the field group
     */
   defaultMargins : '0 8 0 0',

    /*!
     * Wrap items
     */
   initComponent : function(){       
       var config = {
           layout: 'hbox',
           autoHeight: true
       }

       //apply required configs
       Ext.apply(this, config);

       //wrap each item
       this.items = this.wrapItems(this.items);

       Ext.ux.form.FieldGroup.superclass.initComponent.apply(this, arguments);
   },

   /**
    * @private
    * Wrap each item in a container with form layout
    * @param {Array}  items
    * @return {Array} wraped items
    */
   wrapItems : function(items){
       var wraps = [], wrap, item;

       //wrap each item its container
       for (i = 0, j = items.length; i < j; i++){
           item = items[i];

           Ext.apply(item,{anchor: '100%'});

           //create the wrap for current item
           wrap = {
               xtype: 'container',               
               labelWidth: item.labelWidth,
               flex: item.flex,
               width: item.width,
               layout: 'form',
               items:[
                 item
               ]
           }

           //apply default margins to each item except the last
           if (i < j -1)
              Ext.applyIf(wrap, {margins: this.defaultMargins});

           wraps.push(wrap);
       }
       
       return wraps;
    }
});

Ext.reg('fieldgroup',Ext.ux.form.FieldGroup);



Ext.ns('Ext.ux.form');
Ext.ux.form.XCheckbox = Ext.extend(Ext.form.Checkbox, {
     submitOffValue:'0'
    ,submitOnValue:'1'

    ,onRender:function() {

        this.inputValue = this.submitOnValue;

        // call parent
        Ext.ux.form.XCheckbox.superclass.onRender.apply(this, arguments);

        // create hidden field that is submitted if checkbox is not checked
        this.hiddenField = this.wrap.insertFirst({tag:'input', type:'hidden'});

        // support tooltip
        if(this.tooltip) {
            this.imageEl.set({qtip:this.tooltip});
        }

        // update value of hidden field
        this.updateHidden();

    } // eo function onRender

    /**
     * Calls parent and updates hiddenField
     * @private
     */
    ,setValue:function(v) {
        v = this.convertValue(v);
        this.updateHidden(v);
        Ext.ux.form.XCheckbox.superclass.setValue.apply(this, arguments);
    } // eo function setValue

    ,setValueNotOther:function(v) {
        v = this.convertValue(v);
        this.updateHidden(v);
        Ext.ux.form.XCheckbox.superclass.setValue.apply(this, arguments);
    }
    /**
     * Updates hiddenField
     * @private
     */
    ,updateHidden:function(v) {
        v = this.convertValue(v);
        if(this.hiddenField) {
            this.hiddenField.dom.value = v ? this.submitOnValue : this.submitOffValue;
            this.hiddenField.dom.name = v ? '' : this.el.dom.name;
        }
    } // eo function updateHidden

    /**
     * Converts value to boolean
     * @private
     */
    ,convertValue:function(v) {
        return ((v === true || v === 'true' || v == '1' ||  v == 1 || v === this.submitOnValue || String(v).toLowerCase() === 'on') && v != '0');
    } // eo function convertValue

}); // eo extend

// register xtype
Ext.reg('xcheckbox', Ext.ux.form.XCheckbox);

// eof


Ext.namespace('PrintShopCreator.Base');

PrintShopCreator.Base.Common = function(){


	/**
	 * Open browsers native popup
	 * @param {string} _windowName
	 * @param {string} _url
	 * @param {int} _width
	 * @param {int} _height
	 */
	var _openWindow = function(_windowName, _url, _width, _height){
		var w,h,x,y,leftPos,topPos,popup;
		
		if (document.all) {
			w = document.body.clientWidth;
			h = document.body.clientHeight;
			x = window.screenTop;
			y = window.screenLeft;
		}
		else 
			if (window.innerWidth) {
				w = window.innerWidth;
				h = window.innerHeight;
				x = window.screenX;
				y = window.screenY;
			}
		leftPos = ((w - _width) / 2) + y;
		topPos = ((h - _height) / 2) + x;
		
		popup = window.open(_url, _windowName, 'width=' + _width + ',height=' + _height + ',top=' + topPos + ',left=' + leftPos +
		',directories=no,toolbar=no,location=no,menubar=no,scrollbars=no,status=no,resizable=yes,dependent=no');
		popup.blur();
                popup.focus();
                
		return popup;
	};

    var _openWindowWithToolbar = function(_windowName, _url, _width, _height){
		var w,h,x,y,leftPos,topPos,popup;

		if (document.all) {
			w = document.body.clientWidth;
			h = document.body.clientHeight;
			x = window.screenTop;
			y = window.screenLeft;
		}
		else
			if (window.innerWidth) {
				w = window.innerWidth;
				h = window.innerHeight;
				x = window.screenX;
				y = window.screenY;
			}
		leftPos = ((w - _width) / 2) + y;
		topPos = ((h - _height) / 2) + x;

		popup = window.open(_url, _windowName, 'width=' + _width + ',height=' + _height + ',top=' + topPos + ',left=' + leftPos +
		',directories=no,toolbar=yes,location=no,menubar=yes,scrollbars=yes,status=yes,resizable=yes,dependent=no');
                popup.blur();
                popup.focus();
                
		return popup;
	};


     var _renderBool = function(val) {
        var checkedImg = '/scripts/extjs/resources/images/default/dd/drop-yes.gif';
        var uncheckedImg = '/scripts/extjs/resources/images/default/dd/drop-no.gif';
        var cb = ''
            + '<div style="text-align:center;height:13px;overflow:visible">'
            + '<img style="vertical-align:-3px" src="'
            + (val == 'true' || val == 1 || val == '1' ? checkedImg : uncheckedImg)
            + '"'
            + ' />'
            + '</div>'
        ;
        return cb;
    }

    
	return {
		openWindow:       _openWindow,
        openWindowWithToolbar:       _openWindowWithToolbar,
        renderBool:       _renderBool,
        dateRenderer: function(date){
            return Ext.util.Format.date(date, Locale.getTranslationData('Date', 'medium'));
        }
	};
}();

/**
*
* Extension to get a EU money formatter.
*
* @author: Thomas Stachl
* @email: thomas@stachl.me
*
*/
Ext.util.Format.euMoney = function (v) {
   v = (Math.round((v-0)*100))/100;
   v = (v == Math.floor(v)) ? v + ".00" : ((v*10 == Math.floor(v*10)) ? v + "0" : v);
   v = String(v);
   var ps = v.split('.');
   var whole = ps[0];
   var sub = ps[1] ? ','+ ps[1] : ',00';
   var r = /(\d+)(\d{3})/;
   while (r.test(whole)) {
       whole = whole.replace(r, '$1' + '.' + '$2');
   }
   v = whole + sub;
   if(v.charAt(0) == '-'){
       return '-' + v.substr(1) + ' &euro;';
   }
   return v + " &euro;";
};

