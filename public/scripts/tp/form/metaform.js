// isArray is in SVN only

Ext.ns('PrintShopCreator');

var translation = new Locale.Gettext();
translation.textdomain('App');


if(!Ext.isArray) {
    Ext.isArray = function(v) {
        return v && 'function' == typeof v.pop;
    }
}

/**
 *
 * @class Ext.ux.MetaForm
 * @extends Ext.FormPanel
 */
Ext.ux.MetaForm = Ext.extend(Ext.FormPanel, {
    // configurables
    autoInit:true
    ,
    frame:true
    ,
    loadingText:'Loading...'
    ,
    savingText:'Saving...'
    ,
    buttonMinWidth:90
    ,
    columnCount:1
    ,
    layout: 'column'

    ,
    stores: 0
    /**
     * createButtons {Array} array of buttons to create
     *         valid values are ['meta', 'load', defaults', 'reset', 'save', 'ok', 'cancel']
     */
    /**
     * ignoreFields: {Array} of field names to ignore when received in metaData
     */

    // {{{
    ,
    initComponent:function() {

        // create one item to avoid error
        Ext.apply(this, {
            items:this.items || {}
        }); // eo apply

        // get buttons if we have button creation routines
        if('function' == typeof this.getButton) {
            this.buttons = this.getButtons();
        }

        // call parent
        Ext.ux.MetaForm.superclass.initComponent.apply(this, arguments);
        
        this.addEvents('cancel', 'ok');

        // install event handlers on basic form
        this.form.on({
            beforeaction:{
                scope:this,
                fn:this.beforeAction
                }
             
            ,
            actioncomplete:{
                scope:this,
                fn:function(form, action) {
                    // (re) configure the form if we have (new) metaData
                    if('load' === action.type && action.result.metaData) {
                        this.onMetaChange(this, action.result.metaData);
                        this.beforeAction(this, action);
                    }
                    // update bound data on successful submit
                    else if('submit' == action.type) {
                        this.updateBoundData();
                    }
                
                }
            }
        });
    this.form.trackResetOnLoad = true;
        

} // eo function initComponent
// }}}
// {{{
/**
     * private, changes order of execution in Ext.form.Action.Load::success
     * to allow reading of data in this server request (otherwise data would
     * be loaded to the form before onMetaChange is run from actioncomplete event
     */
,
beforeAction:function(form, action) {
    action.success = function(response) {
        var result = this.processResponse(response);
        if(result === true || !result.success || !result.data){
            this.failureType = Ext.form.Action.LOAD_FAILURE;
            this.form.afterAction(this, false);
            return;
        }
        // original
        //            this.form.clearInvalid();
        //            this.form.setValues(result.data);
        //            this.form.afterAction(this, true);

        this.form.afterAction(this, true);
        this.form.clearInvalid();
    //this.form.setValues(action.result.metaData.data);
			           
    };
        
} // eo function beforeAction
// }}}
// {{{
/**
     * @param {Object} data A reference to on external data object. The idea is that form can display/change an external object
     */
,
bind:function(data) {
    this.data = data;
    this.form.setValues(this.data);
} // eo function bind
// }}}
// {{{
/**
     * override this if you want a special buttons config
     */
,
getButtons:function() {
    var buttons = [];
    if(Ext.isArray(this.createButtons)) {
        Ext.each(this.createButtons, function(name) {
            var button;
            switch(name) {
                case 'meta':
                    button = this.getButton(name, {
                        handler:this.load.createDelegate(this, [{
                            params:{
                                meta:true
                            }
                        }])
                    });
                break;

            case 'load':
                button = this.getButton(name, {
                    scope:this
                    ,
                    handler:this.load
                });
                break;

            case 'defaults':
                button = this.getButton(name, {
                    scope:this
                    ,
                    handler:this.setDefaultValues
                });
                break;

            case 'reset':
                button = this.getButton(name, {
                    scope:this
                    ,
                    handler:this.reset
                });
                break;

            case 'save':
            case 'submit':
                button = this.getButton(name, {
                    handler:this.submit.createDelegate(this, [{
                        params:{
                            cmd:'setPref'
                        }
                    }])
                });
            break;

        case 'ok':
            button = this.getButton(name, {
                scope:this
                ,
                handler:this.onOk
            });
            break;

        case 'cancel':
            button = this.getButton(name, {
                scope:this
                ,
                handler:this.onCancel
            });
            break;
        }
        if(button) {
            Ext.apply(button, {
                minWidth:this.buttonMinWidth
            });
            buttons.push(button);
        }
    }, this);
};
return buttons;
} // eo function getButtons
// }}}
// {{{
,
getOptions:function(o) {
    var options = {
        url:this.url
        ,
        method:this.method || 'post'
    };
    Ext.apply(options, o);
    options.params = Ext.apply(this.baseParams || {}, o.params);
    return options;
} // eo function getOptions
// }}}
// {{{
/**
     * @return {Object} object with name/value pairs using fields.getValue() methods
     */
,
getValues:function() {
    var values = {};
    this.form.items.each(function(f) {
        values[f.name] = f.getValue();
    });
    return values;
} // eo function getValues
// }}}
// {{{
,
load:function(o) {
    var options = this.getOptions(o);
    if(this.loadingText) {
        options.waitMsg = this.loadingText;
    }
    this.form.load(options);
} // eo function load
// }}}
// {{{
/**
     * cancel button handler - fires cancel event only
     */
,
onCancel:function() {
    this.fireEvent('cancel', this);
} // eo function onCancel
// }}}
// {{{
/**
     * Override this if you need a custom functionality
     *
     * @param {Ext.FormPanel} this
     * @param {Object} meta Metadata
     * @return void
     */
,
onMetaChange:function(form, meta) {
    this.removeAll();

    // declare varables
    var columns, colIndex, tabIndex, ignore = {};

    this.add(new Ext.Toolbar());

    // add column layout
    this.add(new Ext.TabPanel({
        activeTab: 0,
        frame: false,
        border: true,
        width: meta.formConfig.defaults.width,
        layoutOnTabChange: true,
        autoHeight: true
		    
    }));
        
    tabIndex = 0;

    if(Ext.isArray(this.ignoreFields)) {
        Ext.each(this.ignoreFields, function(f) {
            ignore[f] = true;
        });
    }
    //this.items.get(0).render();
    if(typeof meta.buttons != 'undefined') {
        Ext.each(meta.buttons, function(item) {
            if(item.href != "" && item.href != "clear_layouter" && item.href != "clear_templateprint") {
                this.items.get(0).addButton({
                    text: item.title,
                    handler: function(e) {
                        window.open(item.href);
                    }
                });
            };
            if(item.href=="clear_layouter") {
                this.items.get(0).addButton({
                    text: item.title,
                    handler: function(e) {
                        Ext.Msg.show({
                            title: translation._('msg_clear_layouter')
                            ,
                            msg: translation._('msg_clear_layouter_confirm_beforetext')
                            ,
                            icon:Ext.Msg.QUESTION
                            ,
                            buttons:Ext.Msg.YESNO
                            ,
                            scope:this
                            ,
                            fn:function(response) {
                                if('yes' == response) {
                                    try {
                                        var req = new XMLHttpRequest;
	
                                        req.open('POST', '/admin/article/clearlayouter/format/json?uuid=' + uid, false);
                                        req.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
                                        req.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
                                        req.send();
                                        if (req.status == 200 || req.status == 304 || req.status == 0 || req.status == null) {
                                            this.store.load({
                                                params:{
                                                    start:0,
                                                    limit:20
                                                }
                                            });
                                        return Ext.util.JSON.decode(req.responseText);
                                    }
                                } catch (e) {
                                    return '';
                                }
                            }else{
                                return;
                            }
                        }
                        });
                }
                });
        }
            if(item.href=="clear_templateprint") {
                this.items.get(0).addButton({
                    text: item.title,
                    handler: function(e) {
                        Ext.Msg.show({
                            title: translation._('Templateprint')
                            ,
                            msg: translation._('Template l&ouml;schen?')
                            ,
                            icon:Ext.Msg.QUESTION
                            ,
                            buttons:Ext.Msg.YESNO
                            ,
                            scope:this
                            ,
                            fn:function(response) {
                                if('yes' == response) {
                                    try {
                                        var req = new XMLHttpRequest;

                                        req.open('POST', '/admin/article/cleartemplateprint/format/json?uuid=' + uid, false);
                                        req.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
                                        req.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
                                        req.send();
                                        if (req.status == 200 || req.status == 304 || req.status == 0 || req.status == null) {
                                            this.store.load({
                                                params:{
                                                    start:0,
                                                    limit:20
                                                }
                                            });
                                            return Ext.util.JSON.decode(req.responseText);
                                        }
                                    } catch (e) {
                                        return '';
                                    }
                                }else{
                                    return;
                                }
                            }
                        });
                    }
                });
            }
        this.items.get(0).addSeparator();
        }, this);
}
// loop through metadata colums or fields
// format follows grid column model structure
Ext.each(meta.columns || meta.fields, function(item) {
    if(true === ignore[item.name]) {
        return;
    }
            
    this.items.get(1).add(new Ext.Panel({
        title:	translation._(item.general.title),
        layout:'form',
        labelAlign: item.general.labelAlign ? item.general.labelAlign : 'left',
        padding: 2,
        labelWidth: 200,
        defaults: {
            width: 200
        },
        defaultType: 'textfield'
        ,
        autoHeight: true
    }));
            
    colIndex = 0;
            
    columns = this.items.get(1).items.get(tabIndex);

    Ext.each(item.fields, function(field) {
             	
            	
		                     	
        if(field.editor.regex){
            //alert(item.editor.regex);
            field.editor.regex= new RegExp(field.editor.regex);
        }
        var config = Ext.apply({}, field.editor, {
            name:field.name || field.dataIndex
            ,
            fieldLabel:field.fieldLabel || field.header
            ,
            help: field.help ? field.help : ''
            ,
            width: field.editor.width ? parseInt(field.editor.width) : 550
            ,
            heigth: field.editor.height ? field.editor.height : 22
            ,
            xtype:field.editor && field.editor.xtype ? field.editor.xtype : 'textfield'
            ,
            scope: this
        });
	            
        if('uploadfield' != config.xtype && 'combo' != config.xtype) {
            Ext.apply(config, {
                value: meta.data[field.name]
                ,
                id: field.id || ''
            });
        }
                    
        // handle regexps
        if(config.editor && config.editor.regex) {
            config.editor.regex = new RegExp(field.editor.regex);
        }
	
        // to avoid checkbox misalignment
        if('checkbox' === config.xtype) {
            Ext.apply(config, {
                boxLabel:' '
                ,
                checked:meta.data[field.name]
                ,
                value: 1
            });
        }
	            
	            
        if('compositefield' === config.xtype || 'fieldgroup' === config.xtype) {
            var items = [];
	            	
            if('fieldgroup' === config.xtype) {
                Ext.apply(config, {
                    labelWidth: config.labelWidth ? config.labelWidth : 80
                });
            }
	            	
            Ext.each(field.items, function(fld) {
	            		
                var conf = Ext.apply({}, fld.editor, {
                    name:fld.name || fld.dataIndex
                    ,
                    fieldLabel:fld.fieldLabel || fld.header
                    ,
                    help: fld.help ? fld.help : ''
                    ,
                    heigth: fld.editor.height ? fld.editor.height : 25
                    ,
                    xtype:fld.editor && fld.editor.xtype ? fld.editor.xtype : 'textfield'
                    ,
                    scope: this
                    ,
                    id: fld.id || ''

                });
	            		
                if(fld.editor.width) {
                    Ext.apply(conf, {

                        width: parseInt(fld.editor.width)
    	                   
                    });
                }else{
                    Ext.apply(conf, {

                        flex: 1
    	                   
                    });
                }

                if('uploadfield' != conf.xtype && 'combo' != conf.xtype) {
                    Ext.apply(conf, {
                        value: meta.data[fld.name]
                        ,
                        id: fld.id || ''
                    });
                }
	            		
                if(typeof sid == 'undefined') sid = "";
                if('combo' === conf.xtype) {

                    Ext.apply(conf, {
                        store: new Ext.data.JsonStore({
                            url: '/service/?type=get&mode=system&service=' + conf.service + '&sid=' + sid,
                            root: 'items',
                            fields: ['name', 'label']
                        }),
                        mode: 'locale',
                        triggerAction: 'all',
                        selectOnFocus:true,
                        valueField:'name',
                        displayField:'label',
                        hiddenName: fld.nameid,
                        hiddenValue: meta.data[fld.nameid],
                        value: meta.data[fld.name]
	    	                   
                    });

	                            
	    	                

                    Ext.apply(conf, {

                        value: meta.data[fld.name]
	    	                   
                    });
	                            
	                           
                }
	    	            
                if('checkbox' === conf.xtype || 'xcheckbox' == conf.xtype) {
                    if(meta.data[fld.name] == '0') {
                        Ext.apply(conf, {
                            id: fld.name,
                            checked: false
	    	                                 
                        });
                    }else{
                        Ext.apply(conf, {
                            id: fld.name,
                            checked: meta.data[fld.name]
	    	                                 
                        });
                    }
                }
	    	            
	            		
                items.push(Ext.ComponentMgr.create(conf));
	            		
            });
	            	
            Ext.apply(config, {
                items: items
            });
        }
	            
        if('htmleditorimage' === config.xtype) {
            Ext.apply(config, {
                xtype: 'htmleditorimage',
                plugins: [new Ext.ux.HTMLEditorImage(config.service),new Ext.ux.MediaButton(config.service)]
            });
        }

        if('htmleditor' === config.xtype) {
            Ext.apply(config, {
                enableFont: false,
                plugins: [new Ext.ux.form.HtmlEditor.HeadingMenu(), new Ext.ux.form.HtmlEditor.HR(),new Ext.ux.form.HtmlEditor.Table(),new Ext.ux.form.HtmlEditor.Image()]
            });
        }
                
        if('tinymce' === config.xtype) {
            Ext.apply(config, {
                xtype: 'tinymce',
                width: 600,
                height: 400,
                tinymceSettings: {
                    theme : "advanced",
                    plugins: "safari,pagebreak,style,layer,table,advhr,advimage,advlink,emotions,iespell,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,noneditable,visualchars,nonbreaking,xhtmlxtras,template",
                    theme_advanced_buttons1 : "bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,|,styleselect,formatselect,fontselect,fontsizeselect",
                    theme_advanced_buttons2 : "cut,copy,paste,pastetext,pasteword,|,search,replace,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,link,unlink,anchor,image,cleanup,help,code,|,insertdate,inserttime,preview,|,forecolor,backcolor",
                    theme_advanced_buttons3 : "tablecontrols,|,hr,removeformat,visualaid,|,sub,sup,|,charmap,emotions,iespell,media,advhr,|,print,|,ltr,rtl,|",
                    theme_advanced_buttons4 : "insertlayer,moveforward,movebackward,absolute,|,styleprops,|,cite,abbr,acronym,del,ins,attribs,|,visualchars,nonbreaking,template,pagebreak",
                    theme_advanced_toolbar_location : "top",
                    theme_advanced_toolbar_align : "left",
                    theme_advanced_statusbar_location : "bottom",
                    theme_advanced_resizing : false,
                    extended_valid_elements : "a[name|href|target|title|onclick],img[class|src|border=0|alt|title|hspace|vspace|width|height|align|onmouseover|onmouseout|name],hr[class|width|size|noshade],font[face|size|color|style],span[class|align|style]",
                    template_external_list_url : "example_template_list.js"
                },
                id: 'text1',
                name: 'text1'
            });
        }

        if('contactgrid' === config.xtype) {
            config = new PrintShopCreator.Admin.Contact.GridPanel({
                anchor: '100% 98%'
            })
        }

        if('articlelanggrid' === config.xtype) {
            config = new PrintShopCreator.Admin.Article.LangGrid({
                anchor: '100% 98%'
            });

        }

        if('articlegrouplanggrid' === config.xtype) {
            config = new PrintShopCreator.Admin.Articlegroup.LangGrid({
                anchor: '100% 98%'
            });

        }

        if('liefergrid' === config.xtype) {
            config = new PrintShopCreator.Admin.Contact.GridLieferPanel({
                anchor: '100% 98%'
            })
        }
                
        if('checkbox' === config.xtype || 'xcheckbox' == config.xtype) {
            if(meta.data[field.name] == '0') {
                Ext.apply(config, {
                    id: field.name,
                    checked: false
	                                 
                });
            }else{
                Ext.apply(config, {
                    id: field.name,
                    checked: meta.data[field.name]
	                                 
                });
            }
        }
        if(typeof sid == 'undefined') sid = "";
        if('combo' === config.xtype) {

            Ext.apply(config, {
                store: new Ext.data.JsonStore({
                    url: '/service/?type=get&mode=system&service=' + config.service + '&sid=' + sid,
                    root: 'items',
                    fields: ['name', 'label']
                }),
                mode: 'locale',
                triggerAction: 'all',
                selectOnFocus:true,
                valueField:'name',
                displayField:'label',
                hiddenName: field.nameid,
                hiddenValue: meta.data[field.nameid],
                value: meta.data[field.name]
	                   
            });

                        

            Ext.apply(config, {

                value: meta.data[field.name]
	                   
            });
                        
                       
        }
        if('superboxselect' === config.xtype) {
            if(config.addvar) {
                var storee = new Ext.data.JsonStore({
                    url: '/service/?type=get&mode=system&data=' + eval(config.addvar) + '&service=' + config.service + '&sid=' + sid,
                    root: 'items',
                    fields: ['name', 'label']

                });
            }else{
                var storee = new Ext.data.JsonStore({
                    url: '/service/?type=get&mode=system&service=' + config.service + '&sid=' + sid,
                    root: 'items',
                    fields: ['name', 'label']

                });
            }
            storee.on('beforeload', function(e) {
                this.stores = this.stores+1;
                Ext.MessageBox.wait(this.loadingText);
            },this);
            storee.on('load', function(e) {
                this.stores = this.stores - 1;
                if(this.stores == 0) {
                    Ext.MessageBox.updateProgress(1);
                    Ext.MessageBox.hide();
                }
            },this);
            storee.load();
            Ext.apply(config, {
                dataFields:["name", "label"],
                id: field.name,
                store : storee,
                valueField:'name',
                displayField:'label',
                displayFieldTpl: '{label}',
                resizable: true,
                mode: 'local'
	                   
            });
	                
					
        }

        if('formlink' == config.xtype) {
            Ext.apply(config, {
                link: field.value
            });
        }

        if(meta.formConfig.msgTarget) {
            config.msgTarget = meta.formConfig.msgTarget;
        }
                
        if(typeof config.disabled != 'undefined' && config.disabled == 'false') {
            config.disabled = false;
        }
	
        // add to columns on ltr principle
        columns.add(config);
        colIndex = colIndex == this.columnCount ? 0 : colIndex;
	            
    }, this);
            
    tabIndex = tabIndex+1;
			
}, this);

this.doLayout();
Ext.each(meta.columns || meta.fields, function(item) {
    Ext.each(item.fields, function(field) {
	                
	                
	                                
        if(field.editor.regex){
	                
            field.editor.regex = new RegExp(field.editor.regex);
        }
        if('htmleditor' === field.editor.xtype) {
            if(typeof Ext.getCmp(field.name) != 'undefined') Ext.getCmp(field.name).setValue(meta.data[field.name]);
        }
        if('combo' == field.editor.xtype && field.editor.reload) {
                            
                     
        }
                        
        if('uploadfield' === field.editor.xtype) {
            //Ext.getCmp(field.name).setValue(meta.data[field.name]);
            if(meta.data[field.name] != null && meta.data[field.name] != '' && (meta.data[field.name].length == 40 || meta.data[field.name].length == 36)) {
                Ext.getCmp(field.name).imageSrc = '/service/index?type=upload&mode=get&id=' + meta.data[field.name] + '&width=' + Ext.getCmp(field.name).width + '&height=' + (Ext.getCmp(field.name).height-2) + '&ratiomode=0';
                if(typeof Ext.getCmp(field.name) != 'undefined') Ext.getCmp(field.name).setValue(Ext.getCmp(field.name).imageSrc, meta.data[field.name]);
                if(Ext.getCmp(field.name).rendered) {
                    Ext.getCmp(field.name).updateImage(meta.data[field.name]);
                }
            }
        }
        if('compositefield' === field.editor.xtype) {
            Ext.each(field.items, function(fielde) {
                if(meta.data[fielde.name] != null && meta.data[fielde.name] != '' && meta.data[fielde.name].length == 40) {
                    Ext.getCmp(fielde.name).imageSrc = '/service/index?type=upload&mode=get&id=' + meta.data[fielde.name] + '&width=' + Ext.getCmp(fielde.name).width + '&height=' + (Ext.getCmp(fielde.name).height-2) + '&ratiomode=0';
                    if(typeof Ext.getCmp(fielde.name) != 'undefined') Ext.getCmp(fielde.name).setValue(Ext.getCmp(fielde.name).imageSrc, meta.data[fielde.name]);
                    if(Ext.getCmp(fielde.name).rendered) {
                        Ext.getCmp(fielde.name).updateImage(meta.data[fielde.name]);
                    }
                }
            }, this)
        }
    }, this);
}, this);
        
} // eo function onMetaChange
// }}}
// {{{
,
onOk:function() {
    this.updateBoundData();
    this.fireEvent('ok', this);
        
}
// }}}
// {{{
,
onRender:function() {
    // call parent
    Ext.ux.MetaForm.superclass.onRender.apply(this, arguments);

    this.form.waitMsgTarget = this.el;

    if(true === this.autoInit) {
        this.load({
            params:{
                meta:true
            }
        });
}
else if ('object' == typeof this.autoInit) {
    this.load(this.autoInit);
}
} // eo function onRender
// }}}
// {{{
    
/**
     * private, removes all items from both formpanel and basic form
     */
,
removeAll:function() {
    // remove border from header
    var hd = this.body.up('div.x-panel-bwrap').prev();
    if(hd) {
        hd.applyStyles({
            border:'none'
        });
    }
    // remove form panel items
    this.items.each(this.remove, this);

    // remove basic form items
    this.form.items.clear();
} // eo function removeAllItems
// }}}
// {{{
,
reset:function() {
    this.form.reset();
} // eo function reset
// }}}
// {{{
,
setDefaultValues:function() {
    this.form.items.each(function(item) {
        item.setValue(item.defaultValue);
    });
} // eo function setDefaultValues
// }}}
// {{{
,
submit:function(o) {
    var options = this.getOptions(o);
    if(this.savingText) {
        options.waitMsg = this.savingText;
    }
        
    this.form.submit(options);
} // eo function submit
// }}}
// {{{
,
updateBoundData:function() {
    if(this.data) {
        Ext.apply(this.data, this.getValues());
    }
} // eo function updateBoundData
// }}}

});

// register xtype
Ext.reg('metaform', Ext.ux.MetaForm);

// eof  