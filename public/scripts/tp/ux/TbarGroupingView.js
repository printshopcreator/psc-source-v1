/**
 * @author fvielle
 */
Ext.namespace('Ext.ux','Ext.ux.grid');

Ext.ux.grid.TbarGroupingView = function(config){
	
	Ext.ux.grid.TbarGroupingView.superclass.constructor.call(this, config);
	
	 // shortcuts
    if(this.tbar){
		if(typeof this.tbar == 'object'){
			this.groupToolbar = this.tbar;
		}
		delete this.tbar;
    };
    
    //private
	this.getCssPading = function(cssSelector){
		var cssRule, cssPadding, padding = {};
		cssRule = Ext.util.CSS.getRule(cssSelector);
		
		if (typeof cssRule == 'object'){
			cssPadding = cssRule.style ? cssRule.style.padding.replace(/\D+/g, ' ').split(' ') : [];
			cssPadding.splice(cssPadding.length-1, 1);
			
		} else {
			cssPadding = [];
		}
		
		switch (cssPadding.length){
			case 1:
				padding.top = parseInt(cssPadding[0]);
				padding.left = parseInt(cssPadding[0]);
				padding.bottom = parseInt(cssPadding[0]);
				padding.right = parseInt(cssPadding[0]);
				break;
			case 2 :
				padding.top = parseInt(cssPadding[0]);
				padding.left = parseInt(cssPadding[1]);
				padding.bottom = parseInt(cssPadding[0]);
				padding.right = parseInt(cssPadding[1]);
				break;
			case 3 :
				padding.top = parseInt(cssPadding[0]);
				padding.left = parseInt(cssPadding[1]);
				padding.bottom = parseInt(cssPadding[2]);
				padding.right = parseInt(cssPadding[1]);
				break;
			case 4 :
				padding.top = parseInt(cssPadding[0]);
				padding.left = parseInt(cssPadding[1]);
				padding.bottom = parseInt(cssPadding[2]);
				padding.right = parseInt(cssPadding[3]);
				break;
			default:
				padding = {
					top: 0,
					left: 0,
					bottom: 0,
					right: 0
				};
		}
		return padding;
	}

	//private
	this.computeWidth = function(el){
		var pw, c, cw, w;
		var p = el.parent();
		var selector = Ext.isIE ? '.x-grid-group-hd DIV' : '.x-grid-group-hd div';
		var pPadding = this.getCssPading(selector);
		
		pw = p.getWidth()-(pPadding.left+pPadding.right);
		
		cw = 0;
		c = p.dom.childNodes;
		for (var i=0; i < c.length ;i++){
			cw += Ext.get(c[i]).getWidth();
		}
		
		w = pw - cw; 		
		return w;
	}
	
	/**
	 *  retrieve the toolbar object for a defined group id
	 */
	this.getGroupToolbar = function(gId){
		
		var tbarList = this.groupToolbarList ? this.groupToolbarList : this.view && this.view.groupToolbarList ? this.view.groupToolbarList : false ;
		var groupId = gId ? gId.replace(/ext-gen[0-9]+-gp-/, '') : this.groupId ? this.groupId : false;
		tbar = tbarList && groupId ? tbarList[groupId] : [];
		
		return tbar;
		
	}

	/**
	 *  retrieve the the grid's records for a defined group id
	 */
	this.getGroupRecords = function(gId){
		var records, groupId = gId ? gId : this.groupId ? this.groupId : false;
		if(groupId) {
			
			records = this.grid.store.queryBy(function(r) {
				return r._groupId.match(groupId);
			});
			records = records ? records.items : [];
		}
		return records;
	}
	
    /**
	 * private 
	 * process the toolbar
	 */
	this.addGroupToolbar = function(){
		if (this.groupToolbar){
			this.mainBody.hide(); //we hide 'cause it's not quit optimised and can cause some display artefact during layout

			if(this.groupToolbar.initialConfig){ // is it an toolbar items config array or a toolbar
                this.groupToolbar = this.groupToolbar.initialConfig; //we build the toolbar with the given items config
            } else if (!this.groupToolbar.items) {
				this.groupToolbar.items = this.groupToolbar;
			}
			
			this.groupToolbarList = []; // an array for retrieve the toolbar belonging (english?) to a group
			
			var group = this.getGroups();
			for (var i=0; i < group.length ;i++){
				
				var groupId = group[i].id  ? group[i].id.replace(/ext-gen[0-9]+-gp-/, '') : false; //retrieving the group id
				var el = group[i].id  ? Ext.get(group[i].id+'-tb') : false; //retrieving the the toolbar container element for this group toolbar
				
				if (el){ // found?
					
					//add toolbar items properties for them to get the group an toolbar they belong to
					//var items = this.groupToolbar.initialConfig.buttons ? this.groupToolbar.initialConfig.buttons : this.groupToolbar.initialConfig.items ? this.groupToolbar.initialConfig.items : [];
					Ext.each(this.groupToolbar.items, function(item, i) {						
						item.groupId = groupId;
						item.getGroupToolbar = this.getGroupToolbar;
						item.view =  this;
						return item;
					},this);

					// we compute the availlable width for the container element
					var w = this.computeWidth(el);
					el.setSize(w);

					this.groupToolbarList[groupId] = new Ext.Toolbar(this.groupToolbar); // building the toolbar
					
					/* 
					 *  add toolbar properties for use with the getGroupRecords extended function
					 */
					this.groupToolbarList[groupId].view =  this;
					this.groupToolbarList[groupId].grid =  this.grid;
					this.groupToolbarList[groupId].id =  group[i].id+'-tb';
					this.groupToolbarList[groupId].groupId =  groupId;
					this.groupToolbarList[groupId].getGroupRecords = this.getGroupRecords;
					
            		this.groupToolbarList[groupId].render(el); // and rendering it
				}
			}
			
			this.mainBody.show(); // we can show our grid body now
		}
	}
	
    /*
     * adding {groupId}-tb div to contain groups toolbar
     */
	this.initTemplates = Ext.ux.grid.TbarGroupingView.superclass.initTemplates.createInterceptor(function(){
		this.startGroup = new Ext.XTemplate(
			'<div id="{groupId}" class="x-grid-group {cls}">',
			'<div id="{groupId}-hd" class="x-grid-group-hd" style="{style}">',
			'<div><div class="ux-grow-text">', this.groupTextTpl ,'</div><div id="{groupId}-tb" class="ux-grow-tb"></div></div>',
			'</div>',
			'<div id="{groupId}-bd" class="x-grid-group-body">'
		);
	});
	
	// we don't want clicked toolbar's items to colapse group
	this.interceptMouse = Ext.ux.grid.TbarGroupingView.superclass.interceptMouse.createInterceptor(function(e){
		if (e.getTarget('.ux-grow-tb') || e.getTarget('.x-form-field')) {
			return false;
		}
	});
	this.refresh = Ext.ux.grid.TbarGroupingView.superclass.refresh.createSequence(this.addGroupToolbar);
		
}

Ext.extend(Ext.ux.grid.TbarGroupingView, Ext.grid.GroupingView);