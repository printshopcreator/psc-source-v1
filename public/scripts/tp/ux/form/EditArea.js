Ext.namespace('Ext.ux.form');

Ext.ux.form.EditArea = Ext.extend(Ext.form.TextArea, {
    initComponent: function() {
        this.eaid = this.id;
        Ext.ux.form.EditArea.superclass.initComponent.apply(this, arguments);
        this.on('resize', function(ta, width, height) {
            var el = Ext.get('frame_editarea-' + this.eaid);
            if (el) {
                el.setSize(width,height);
                    //var size = dialog.getSize();
                    //el.setSize(size.width - dialog.getFrameWidth(), size.height - dialog.getFrameHeight());
            }

        });
        this.on('beforehide', function() {
            editAreaLoader.deleteInstance(this.eaid);
        });
        this.on('beforeshow', function() {
            editAreaLoader.init({
                id: this.eaid,
                start_highlight: this.initialConfig.start_highlight || true,
                language: 'de',
                syntax: this.initialConfig.syntax,
                allow_toggle: this.initialConfig.allow_toggle || true,
                allow_resize: this.initialConfig.allow_resize || false,
                replace_tab_by_spaces: this.initialConfig.replace_tab_by_spaces || 4,
                toolbar: this.initialConfig.toolbar || "search, go_to_line, |, undo, redo, |, select_font,|, change_smooth_selection, highlight, reset_highlight, |, help",
                is_editable: this.initialConfig.is_editable || true
            });
        });
    },
    onRender: function() {
        Ext.ux.form.EditArea.superclass.onRender.apply(this, arguments);
        editAreaLoader.init({
            id: this.eaid,
            start_highlight: this.initialConfig.start_highlight || true,
            language: 'de',
            syntax: this.initialConfig.syntax,
            allow_toggle: this.initialConfig.allow_toggle || true,
            allow_resize: this.initialConfig.allow_resize || false,
            replace_tab_by_spaces: this.initialConfig.replace_tab_by_spaces || 4,
            toolbar: this.initialConfig.toolbar || "search, go_to_line, |, undo, redo, |, select_font,|, change_smooth_selection, highlight, reset_highlight, |, help",
            is_editable: this.initialConfig.is_editable || true
        });
    },
    getValue: function() {
        var v = editAreaLoader.getValue(this.eaid);
        // this should set the textarea's value and fire events
        //Ext.ux.form.EditArea.superclass.setValue.apply(this, [v]);
        return v;
    },
    setValue: function(v) {
        Ext.ux.form.EditArea.superclass.setValue.apply(this, [v]);
        editAreaLoader.setValue(this.eaid, v);
    }
    //validate: function() {
    //    this.getValue();
    //    Ext.ux.form.EditArea.superclass.validate.apply(this, arguments);
    //}
});

Ext.reg('ux-editarea', Ext.ux.form.EditArea);