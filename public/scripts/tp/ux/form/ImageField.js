/*
 * Tine 2.0
 * 
 * @license     http://www.gnu.org/licenses/agpl.html AGPL Version 3
 * @author      Cornelius Weiss <c.weiss@metaways.de>
 * @copyright   Copyright (c) 2007-2008 Metaways Infosystems GmbH (http://www.metaways.de)
 * @version     $Id: ImageField.js 2627 2008-05-26 13:49:30Z nelius_weiss $
 *
 */
 
Ext.namespace('Ext.ux.form');

/**
 * @class Ext.ux.form.ImageField
 * 
 * <p>A field which displayes a image of the given url and optionally supplies upload
 * button with the feature to display the newly uploaded image on the fly</p>
 * <p>Example usage:</p>
 * <pre><code>
 var formField = new Ext.ux.form.ImageField({
     name: 'jpegimage',
     width: 90,
     height: 90
 });
 * </code></pre>
 */
Ext.ux.form.ImageField = Ext.extend(Ext.form.Field, {
    /**
     * @cfg {String}
     */
    defaultImage: '/images/admin/empty_photo.jpg',

    isFormField: true,

    defaultAutoCreate : {tag:'input', type:'hidden'},
    
    initComponent: function() {
        Ext.ux.form.ImageField.superclass.initComponent.call(this);
        this.imageSrc = this.defaultImage;
    },
    onRender: function(ct, position) {
        Ext.ux.form.ImageField.superclass.onRender.call(this, ct, position);
        
        // the container for the browe button
        this.buttonCt = Ext.DomHelper.insertFirst(ct, '<div>&nbsp;</div>', true);
        this.buttonCt.setSize(this.width, 70);
        this.buttonCt.on('contextmenu', this.onContextMenu, this);
        
        // the image container        
        this.imageCt = Ext.DomHelper.insertFirst(this.buttonCt, this.getImgTpl().apply(this), true);
        
        this.bb = new Ext.ux.form.BrowseButton({
            buttonCt: this.buttonCt,
            renderTo: this.buttonCt,
            scope: this,
            handler: this.onFileSelect,
            //debug: true
            id: this.id + '_bb'
        });

        this.positionEl = this.bb;
    },

    getPositionEl: function() {
      return this.buttonCt;
    },
    getValue: function() {
        var value = Ext.ux.form.ImageField.superclass.getValue.call(this);
        return value;
    },
    setValue: function(value, imgid) {
        Ext.ux.form.ImageField.superclass.setValue.call(this, value);
        this.imageSrc = value ? value : this.defaultImage;
        //this.updateImage(imgid);
    },

    onFileSelect: function(bb) {
        var input = bb.detachInputFile();
        var uploader = new Ext.ux.file.Uploader({
            input: input
        });
        if(! uploader.isImage()) {
            Ext.MessageBox.alert('Not An Image', 'Plase select an image file (gif/png/jpeg)').setIcon(Ext.MessageBox.ERROR);
            return;
        }
        
        this.buttonCt.mask('Loading', 'x-mask-loading');
        uploader.upload();
        uploader.on('uploadcomplete', function(uploader, record){
            //var method = Ext.util.Format.htmlEncode('');
            this.imageSrc = '/service/index?type=upload&mode=get&id=' + record.get('tempFile').id + '&width=' + this.width + '&height=' + (this.height-2) + '&ratiomode=0';
            //this.setValue(this.imageSrc, record.get('tempFile').id);
            
            this.updateImage(record.get('tempFile').id, false);
            
        }, this);
    },
    /**
     * executed on image contextmenu
     * @private
     */
    onContextMenu: function(e, input) {
        e.preventDefault();
        
        var ct = Ext.DomHelper.append(this.buttonCt, '<div>&nbsp;</div>', true);
        
        var upload = new Ext.menu.Item({
            text: 'Change Image',
            iconCls: 'action_uploadImage'
        });
        upload.on('render', function(){
            var ct = upload.getEl();
            var bb = new Ext.ux.form.BrowseButton({
                buttonCt: ct,
                renderTo: ct,
                scope: this,
                handler: function(bb) {
                    this.ctxMenu.hide();
                    this.onFileSelect(bb);
                }
                //debug: true
            });
        }, this);
        
        this.ctxMenu = new Ext.menu.Menu({
            items: [
            upload,
            {
                text: 'Edit Image',
                iconCls: 'action_cropImage',
                scope: this,
                disabled: true,//this.imageSrc == this.defaultImage,
                handler: function() {
                    var cropper = new Ext.ux.form.ImageCropper({
                        image: this.getValue()
                    });
                    cropper.show();
                }
            
            },{
                text: 'Delete Image',
                iconCls: 'action_delete',
                disabled: this.imageSrc == this.defaultImage,
                scope: this,
                handler: function() {
                    this.setValue('');
                    this.updateImage();
                }
                
            }]
        });
        this.ctxMenu.showAt(e.getXY());
    },
    getImgTpl: function() {
        if (!this.imgTpl) {
            this.imgTpl = new Ext.XTemplate('<img ',
                'src="{imageSrc}" ',
                'width="{width}" ',
                'height="{height -2}" ',
                'style="border: 1px solid #B5B8C8;" ',
                ' >'
            ).compile();
        }
        return this.imgTpl;
    },
    updateImage: function(imgid,first) {
        var ct = this.imageCt.up('div');
        //this.imageCt.remove();
        var img = Ext.DomHelper.insertAfter(this.imageCt, this.getImgTpl().apply(this), true);
        // replace image after load
        if(first==true) {
	        img.on('load', function(){
	            this.imageCt.remove();
	            this.imageCt = img;
	            this.buttonCt.unmask();
	            //this.setValue(imgid);
	        }, this);
        }else{
        
        	img.on('load', function(){
	            this.imageCt.remove();
	            this.imageCt = img;
	            this.buttonCt.unmask();
	            this.setValue(imgid);
	        }, this);
        }
        
        //img.on('', function() {
        //}, this);
    }
});
Ext.reg('uploadfield', Ext.ux.form.ImageField);


Ext.form.SliderField = Ext.extend(Ext.Slider, {
    isFormField: true,
    onRender: function(){
        Ext.form.SliderField.superclass.onRender.apply(this, arguments);
        this.nrField = this.el.createChild({
            tag: 'input', type: 'hidden', name: this.name , value: this.value, style: 'position: relative; float:right; height:15px; left: 15px; margin-top:-19px; font-size:10px;width:10px;'
        });
    },
    setValue: function(v) {
        if(this.maxValue && v > this.maxValue) v = this.maxValue;
        if(this.minValue && v < this.minValue) v = this.minValue;

        Ext.form.SliderField.superclass.setValue.apply(this, arguments);
        this.nrField.dom.value = v;
    },
    markInvalid: Ext.emptyFn,
    clearInvalid: Ext.emptyFn,
    validate: function(){this.nrField.dom.disabled=false;return true;}
});

Ext.reg('sliderfield', Ext.form.SliderField);
