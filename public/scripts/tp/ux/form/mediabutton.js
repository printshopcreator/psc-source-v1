var translation = new Locale.Gettext();
translation.textdomain('mediabutton');

Ext.ux.MediaButton = function(url) {

  // PRIVATE

  // pointer to Ext.ux.HTMLEditor
  var editor;

  // pointer to Ext.Window
  var win;

  // pointer to Ext.FormPanel
  var tabs;

  // pointer to Ext.ux.ImageBrowser
  var imageBrowser;

  // other private variables
  var constrained = false;
  var originalWidth = 0;
  var originalHeight = 0;

  var url = url;
  // return the selected image (if an image is selected)
  var getSelectedImage = function() {

    if (Ext.isIE) {

      // ie specific code
      return function() {
        var selection = editor.doc.selection;
        if (selection.type == "Control") {
          var element = selection.createRange()(0);
          if (element.nodeName.toLowerCase() == 'img') {
            return element;
          }
        }
        return null;
      }

    } else {

      // firefox specific code
      return function() {
        var selection = editor.win.getSelection();
        if (selection.focusOffset == selection.anchorOffset + 1) {
          var element = selection.focusNode.childNodes[selection.focusOffset - 1];
          if (element.nodeName.toLowerCase() == 'img') {
            return element;
          }
        }
        return null;
      }
    }
  }();
  var createDiv = function() {
    var element = document.createElement("div");
    element.id='mediaspace';

    return element;
  }
  // create new image node from image details
  var createMedia = function() {
    var element = document.createElement("script");
    element.type='text/javascript';
    element.innerHTML = '\nvar s1 = new SWFObject(\'/scripts/player/player-viral.swf\',\'ply\',\'' + tabs.form.findField('width').getValue() + '\',\'' + tabs.form.findField('height').getValue() + '\',\'9\',\'#ffffff\');\ns1.addParam(\'allowfullscreen\',\'true\');\ns1.addParam(\'allowscriptaccess\',\'always\');\ns1.addParam(\'wmode\',\'opaque\');\ns1.addParam(\'flashvars\',\'file=' + tabs.form.findField('yturl').getValue() + '\');\ns1.write(\'mediaspace\');\n';
    
    return element;
  }

  // insert the image into the editor (browser-specific)
  var insertMediaByBrowser = function() {

    if (Ext.isIE) {

      // ie-specific code
      return function() {

        // get selected text/range
        var selection = editor.doc.selection;
        var range = selection.createRange();

        // insert the image over the selected text/range
        range.pasteHTML(createMedia().outerHTML);
      };
      
    } else {
      
      // firefox-specific code
      return function() {

        // get selected text/range
        var selection = editor.win.getSelection();
        
        // delete selected text/range
        if (!selection.isCollapsed) {
          selection.deleteFromDocument();
        }
        
        // insert the image
        selection.getRangeAt(0).insertNode(createMedia());

        selection.getRangeAt(0).insertNode(createDiv());
      };
    }
  }();

  // insert the image into the editor
  var insertMedia = function() {
    
    // focus on the editor to regain selection
    editor.win.focus();

    // insert the image
    insertMediaByBrowser();

    // perform required toolbar operations
    editor.updateToolbar();
    editor.deferFocus();
  };

  // enable insert button when image source has been entered
  var sourceChanged = function() {
    var disabled = (tabs.form.findField('src').getValue() == "");
    Ext.getCmp('insert-btn').setDisabled(disabled);
  };

  // open/show the image details window
  var openImageWindow = function() {

    if (!win) {

      // create Ext.FormPanel if not previously created
      tabs = new Ext.FormPanel({
        labelWidth: 70,
        width: 350,
        modal: true,
        items: {
          xtype: 'tabpanel',
          border: false,
          activeTab: 0,
          bodyStyle: 'padding:5px',
          defaults: {autoHeight: true}, 
          items: [{
            xtype: 'fieldset',
            border: true,
            title: translation._('General'),
            autoHeight: true,
            defaults: {width: 270},
            items: [{
              xtype: 'textfield',
              fieldLabel:  translation._('Youtube URL'),
              name: 'yturl'
            },{
              layout: "column",
              autoWidth: true,
              border: false,
              defaults: {layout: 'form', border: false},
              hideLabel: true,
              items: [{
                items: [{
                  xtype: "numberfield",
                  fieldLabel:  translation._('Dimensions'),
                  name: 'width',
                  width: 50,
                  allowDecimals: false,
                  allowNegative: false
                }]
              }, {
                items: [{
                  xtype: "statictextfield",
                  submitValue: false,
                  hideLabel: true,
                  value: 'x'
                }]
              }, {
                items: [{
                  xtype: "numberfield",
                  hideLabel: true,
                  name: 'height',
                  width: 50,
                  allowDecimals: false,
                  allowNegative: false
                }]
              }, {
                items: [{
                  xtype: "statictextfield",
                  hideLabel: true,
                  value: '',
                  width: 15
                }]
              }]
            }]
          }]
        }
      });

      // create Ext.Window if not previously created
      win = new Ext.Window({
        title: translation._('Insert/Edit Media'),
        closable: true,
    	modal: true,
        closeAction: 'hide',
        width: 400,
        height: 350,
        plain: true,
        layout: 'fit',
        border: false,
        items: tabs,
        buttons: [{
          text: translation._('Save'),
          iconCls: 'action_applyChanges',
          id: 'insert-btn',
          disabled: false,
          handler: function() {
            win.hide();
            insertMedia();
          }
        }, {
          text: translation._('Close'),
          iconCls: 'action_cancel',
          handler: function() {
            win.hide();
          }
        }],
    		listeners: {
    			'show': function() {
    			  tabs.form.reset();
            var element = getSelectedImage();
            if (element) {

              // still working on this!!!
              // need to fix image source as it is changed
              // from a relative url to an absolute url
              tabs.form.findField('src').setValue(element.src);
              tabs.form.findField('alt').setValue(element.alt);
              tabs.form.findField('width').setValue(element.style.width);
              tabs.form.findField('height').setValue(element.style.height);
              tabs.form.findField('constrain').setValue(true);
            }
    			}
    		}
      });
    }

    // show the window
 	win.show.defer(200, win);

  }

  // PUBLIC

  return {

    // Ext.ux.HTMLEditorImage.init
    // called upon instantiation
    init: function(htmlEditor) {
      editor = htmlEditor;

      // append the insert image icon to the toolbar
      editor.tb.insertToolsAfter('createlink', {
        itemId: 'media',
        cls: 'x-btn-icon x-edit-media',
        handler: openImageWindow,
        scope: this,
        clickEvent: 'mousedown',
        tooltip: {
          title: 'Mediaplayer',
          text: 'Insert/MediaPlayer.',
          cls: 'x-html-editor-tip'
        }
      });
    }
  }
}
