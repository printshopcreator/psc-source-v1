function makeRequest(url) {
	var httpRequest;

	if (window.XMLHttpRequest) {
		httpRequest = new XMLHttpRequest();
		if (httpRequest.overrideMimeType) {
			httpRequest.overrideMimeType('text/xml');
		}
	} else if (window.ActiveXObject) {
		try {
			httpRequest = new ActiveXObject("Msxml2.XMLHTTP");
		} catch (e) {
			try {
				httpRequest = new ActiveXObject("Microsoft.XMLHTTP");
			} catch (e) {
			}
		}
	}

	if (!httpRequest) {
		alert('Giving up :( Cannot create an XMLHTTP instance');
		return false;
	}

	httpRequest.onreadystatechange = function() {
		evalProgress(httpRequest);
	};
	httpRequest.open('GET', url, true);
	httpRequest.send('');

}

function observeProgress() {
	setTimeout("getProgress()", 1500);
}

function getProgress() {
	makeRequest('/service/upload/fileinfo?progress_key=' + document
			.getElementById('progress_key').value);
}

function evalProgress(httpRequest) {
	try {
		if (httpRequest.readyState == '4') {
			if (httpRequest.status == '200') {
				eval('var data = ' + httpRequest.responseText);

				if (data.error) {
					error(data);
				} else {

					if (data.finished) {
						data.message = 'Upload erfolgreich';
						finish(data);
					} else {
						update(data);
						setTimeout("getProgress()", 2000);
					}
				}
			} else {
				alert('There was a problem with the request.');
			}
		}
	} catch (e) {
		alert('Caught Exception: ' + e.description);
	}
}

function error(data) {
	fr = document.getElementById('uploadtarget');
	fr.src = 'about:blank';
	document.getElementById('pg-text-2').innerHTML = data.message;
	document.getElementById('file').value = '';
}

function update(data) {
	document.getElementById('pg-percent').style.width = data.percent + '%';

	document.getElementById('pg-text-2').innerHTML = data.text;
}

function finish(data) {
	$("#uploads").load('/service/upload/getfilesproduct');
	document.getElementById('pg-percent').style.width = '100%';

	document.getElementById('pg-text-2').innerHTML = data.message;

	document.getElementById('file').value = '';
}

function deleteUpload(uuid) {
	 $.ajax({
		   type: "GET",
		   url: "/service/upload/deleteupload",
		   data: 'uuid=' + uuid,
		   success: function(msg){
		 	eval('msg = ' + msg);
		     finish(msg);
		   }
		 });
}

// Center

function makeRequestCenter(url, id) {
	var httpRequest;

	if (window.XMLHttpRequest) {
		httpRequest = new XMLHttpRequest();
		if (httpRequest.overrideMimeType) {
			httpRequest.overrideMimeType('text/xml');
		}
	} else if (window.ActiveXObject) {
		try {
			httpRequest = new ActiveXObject("Msxml2.XMLHTTP");
		} catch (e) {
			try {
				httpRequest = new ActiveXObject("Microsoft.XMLHTTP");
			} catch (e) {
			}
		}
	}

	if (!httpRequest) {
		alert('Giving up :( Cannot create an XMLHTTP instance');
		return false;
	}

	httpRequest.onreadystatechange = function() {
		evalProgressCenter(httpRequest, id);
	};
	httpRequest.open('GET', url, true);
	httpRequest.send('');

}

function observeProgressCenter(id) {
	setTimeout("getProgressCenter(" + id + ")", 1500);
}

function getProgressCenter(id) {
	makeRequestCenter('/service/upload/fileinfo?progress_key=' + document
			.getElementById('progress_key').value+ '&' +new Date() , id);
}

function evalProgressCenter(httpRequest, id) {
	try {
		if (httpRequest.readyState == 4) {
			if (httpRequest.status == 200) {
				eval('var data = ' + httpRequest.responseText);

				if (data.error) {
					errorCenter(data, id);
				} else {

					if (data.finished) {
						data.message = 'Upload erfolgreich';
						finishCenter(id, data);
					} else {
						updateCenter(data, id);
						setTimeout("getProgressCenter(" + id + ")", 2000);
					}
				}
			} else {
				alert('There was a problem with the request.');
			}
		}
	} catch (e) {
		alert('Caught Exception: ' + httpRequest.responseText);
	}
}

function errorCenter(data, id) {
	fr = document.getElementById('uploadtarget' + id);
	fr.src = 'about:blank';
	document.getElementById('pg-text-2-' + id).innerHTML = data.message;

}

function updateCenter(data, id) {
	document.getElementById('pg-percent-' + id).style.width = data.percent + '%';

	document.getElementById('pg-text-2-' + id).innerHTML = data.text;
}

function finishCenter(id, msg) {
	$('#uploads-' + id).load('/service/upload/getfiles?id=' + id);

}

function deleteUploadCenter(uuid, id) {
	$.ajax({
		   type: "GET",
		   url: "/service/upload/deletecenter",
		   data: 'uuid=' + uuid,
		   success: function(msg){
				eval('msg = ' + msg);
		     finishCenter(id , msg);
		   }
		 });
}