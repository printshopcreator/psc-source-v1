function makeRequest(url)
                                {
                                    var httpRequest;

                                    if (window.XMLHttpRequest) {
                                        httpRequest = new XMLHttpRequest();
                                        if (httpRequest.overrideMimeType) {
                                            httpRequest.overrideMimeType('text/xml');
                                        }
                                    } else if (window.ActiveXObject) {
                                        try {
                                            httpRequest = new ActiveXObject("Msxml2.XMLHTTP");
                                        } catch (e) {
                                            try {
                                                httpRequest = new ActiveXObject("Microsoft.XMLHTTP");
                                            } catch (e) {}
                                        }
                                    }

                                    if (!httpRequest) {
                                        alert('Giving up :( Cannot create an XMLHTTP instance');
                                        return false;
                                    }

                                    httpRequest.onreadystatechange = function() { evalProgress(httpRequest); };
                                    httpRequest.open('GET', url, true);
                                    httpRequest.send('');

                                }

                                function observeProgress()
                                {
                                    setTimeout("getProgress()", 1500);
                                }

                                function getProgress()
                                {
                                    makeRequest('/service/upload/fileinfo?progress_key=' + document.getElementById('progress_key').value);
                                }

                                function evalProgress(httpRequest)
                                {
                                    try {
                                        if (httpRequest.readyState == '4') {
                                            if (httpRequest.status == '200') {
                                                eval('var data = ' + httpRequest.responseText);

                                                if (data.error) {
                                                    error(data);
                                                }else{

                                                    if (data.finished) {
                                                        finish();
                                                        fr = document.getElementById('uploadtarget');
                                                       	eval('var data = ' + fr.contentWindow.document.body.innerHTML);
                                                       	if (data.error) {
                                                            document.getElementById('pg-text-2').innerHTML = data.message;
                                                        }
                                                    }else {
                                                        update(data);
                                                        setTimeout("getProgress()", 2000);
                                                    }
                                                }
                                            } else {
                                                alert('There was a problem with the request.');
                                            }
                                        }
                                    } catch(e) {
                                        alert('Caught Exception: ' + e.description);
                                    }
                                }

                                function error(data) {
                                    fr = document.getElementById('uploadtarget');
                                    fr.src = 'about:blank';
                                    document.getElementById('pg-text-2').innerHTML = data.message;
                                    document.getElementById('file').value='';
                                }

                                function update(data)
                                {
                                    document.getElementById('pg-percent').style.width = data.percent + '%';

                                    document.getElementById('pg-text-2').innerHTML = data.text;
                                }

                                function finish()
                                {
                                    $("#uploads").load('/service/upload/getfilesproduct');
                                    document.getElementById('pg-percent').style.width = '100%';

                                    document.getElementById('pg-text-2').innerHTML = 'Fertig';

                                    document.getElementById('file').value='';

                                }

                                function deleteUpload(uuid)
                                {
                                    $.ajax({
                                        type: "GET",
                                        url: "/service/upload/deleteupload",
                                        data: 'uuid=' + uuid,
                                        success: function(msg){
                                            eval('msg = ' + msg);
                                            finish(msg);
                                        }
                                    });

                                    $("#uploads").load('/service/upload/getfilesproduct');
                                }