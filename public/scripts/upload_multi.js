$(function(){
    'use strict';

    $('a[rel="#upload_multi"]').unbind();
    $('a[rel="#upload_multi"]').click(function(){

        $('#upload_mode').val('templateprint');
        $('#myUpload').modal('hide');
        $('#myUploadMulti').modal('toggle');

        disableUploadButtons = false;
        getuploads("all");
        return false;

    });

    var uploadtemplate = tmpl("template-wizard");

    $('#upload_all').html(uploadtemplate({files:[{id: 'all', sessionid: sessionid}]}));

    $('#upload_all').fileupload({
        autoUpload: true,
        done: function (e, data) {
            $('.template-upload').remove();
            finishPreflight(data.result.motive[0].id, data.result.motive[0].uuid);

        }
    });

    $('#myUploadMulti .close').unbind();
    $('#myUploadMulti .close').click(function(){

        $('#myUpload').modal('hide');
        $('#myUploadMulti').modal('toggle');

    });

})