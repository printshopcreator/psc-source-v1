
/**
 * @class		Ext.ux.FileBrowserPanel
 * @extends		Ext.Panel
 * @namespace	Ext.ux
 *
 * FileBrowserPanel
 *
 * @author		Rocco Bruyn
 * @version		0.1
 *
 * @licence		WTFPL (http://sam.zoy.org/wtfpl/COPYING)
 */

// Create namespace
Ext.namespace('Ext.ux');

/**
 * Create new Extension
 *
 * @constructor
 * @param	config	{Object}	An object literal config object
 */
Ext.ux.FileBrowserPanel = Ext.extend(Ext.Panel, {
	/**
	 * @cfg {String} The URL that is used to process data (required)
	 */
	dataUrl: 'filebrowserpanel.php',
	/**
	 * @cfg {String} Default image to use in thumnail view
	 */
	defaultThumbnailImage: '/scripts/vendor/filebrowserpanel/media/icons/48/document_blank.png',
	/**
	 * @cfg	{Function} Function that is used to show a file upload window/dialog
	 * Needs to be implemented at construction
	 */
	fileUploadCallback: Ext.emptyFn,
	
	/**
	 * Called by Ext when instantiating
	 * 
	 * @private
	 * @param config {Object} Configuration object
	 */
	initComponent: function() {
		
		// create the tree that displays folders
		this.tree = new Ext.tree.TreePanel({
			cls: 'ux-filebrowser-tree',
                        id: 'ux-filebrowser-tree',
			title: this.il8n.treePanelHeaderText,
			border: false,
			useArrows: true,
			enableDrop: true,
			dropConfig: {
				ddGroup: 'fileMoveDD',
				appendOnly: true,
				onNodeDrop: this.onTreeNodeDrop.createDelegate(this)
			},
			loader: new Ext.tree.TreeLoader({
				url: this.dataUrl,
				listeners: {
					beforeload: {
						fn: this.onTreeLoaderBeforeLoad,
						scope: this
					}
				}
			}),
			root: new Ext.tree.AsyncTreeNode({
				text: 'Root',
				expanded: true,
                                children: [{
                                    text: 'Images',
                                    leaf: false
                                }]

			}),
			selModel: new Ext.tree.DefaultSelectionModel({
				listeners: {
					selectionchange: {
						fn: this.onTreeSelectionChange,
						scope: this
					}
				}
			}),
			tools: [],
			listeners: {
				contextmenu: {
					fn: this.onTreeContextMenu,
					scope: this
				},
				render: {
					fn: this.onTreeRender,
					scope: this
				}
			}
		});
		
		// create an editor for the tree for modifying folder names
		this.treeEditor = new Ext.tree.TreeEditor(this.tree, {
			allowBlank: false,
			grow: true,
			growMin: 90,
			growMax: 240
		},{
			completeOnEnter: true,
			cancelOnEsc: true,
			ignoreNoChange: true,
			selectOnFocus: true,
			listeners: {
				beforestartedit: {
					fn: this.onTreeEditorBeforeStartEdit,
					scope: this
				},
				beforecomplete: {
					fn: this.onTreeEditorBeforeComplete,
					scope: this
				}
			}
		});
		
		// create a grid that displays files
		this.grid = new Ext.grid.EditorGridPanel({
			cls: 'ux-filebrowser-grid',
                        id: 'filebrowser-grid',
			//title: this.il8n.gridPanelHeaderText,
			border: false,
			stripeRows: true,
			enableDragDrop: true,
			ddGroup: 'fileMoveDD',
			colModel: new Ext.grid.ColumnModel([
				{
					header: this.il8n.gridColumnNameHeaderText,
					id: 'name',
					dataIndex: 'name',
					sortable: true,
					editor:	new Ext.form.TextField()
				},{
					header: this.il8n.gridColumnSizeHeaderText,
					dataIndex: 'size',
					sortable: true,
					renderer: Ext.util.Format.bytesToSi
				},{
					header: this.il8n.gridColumnTypeHeaderText,
					dataIndex: 'type',
					sortable: true
				},{
					header: this.il8n.gridColumnDateModifiedText,
					dataIndex: 'date_modified',
					sortable: true,
					renderer: Ext.util.Format.dateRenderer(this.il8n.displayDateFormat)
				}
			]),
			store: new Ext.data.JsonStore({
				url: this.dataUrl,
				root: 'data',
				fields: [
					{name: 'name', type: 'string'},
					{name: 'size', type: 'int'},
					{name: 'type', type: 'string'},
					{name: 'date_modified', type: 'date', dateFormat: 'timestamp'},
					{name: 'row_class', type: 'string', defaultValue: 'ux-filebrowser-icon-unknown-file'},
					{name: 'thumbnail', type: 'string', defaultValue: this.defaultThumbnailImage}
				],
				listeners: {
					beforeload: {
						fn: this.onGridStoreBeforeLoad,
						scope: this
					}
				}
			}),
			selModel: new Ext.grid.RowSelectionModel({
				listeners: {
					selectionchange: {
						fn: this.onGridSelectionChange,
						scope: this
					}
				}
			}),
			viewConfig: {
				forceFit: true,
				emptyText: this.il8n.noFilesText,
				getRowClass: function(record, rowIndex, rowParams, store) {
					return 'ux-filebrowser-iconrow ' + record.get('row_class');
				}
			},
			listeners: {
				render: {
					fn: this.onGridRender
				},
				rowcontextmenu: {
				   fn: this.onGridContextMenu,
				   scope: this
				},
				afteredit: {
					fn: this.onGridEditorAfterEdit,
					scope: this
				}
			}
		});
		
		// create a panel that will display files as thumbnails
		this.thumbs = new Ext.Panel({
			cls: 'ux-filebrowser-thumbnails',
			border: false,
			layout: 'fit',
			items: new Ext.DataView({
				store: this.grid.getStore(),
				tpl: new Ext.XTemplate(
					'<tpl for=".">',
						'<div class="ux-filebrowser-thumb-wrap" id="ux-filebrowser-thumb-{#}">',
							'<div class="ux-filebrowser-thumb"><img src="{thumbnail}" title="{name}"></div>',
							'<span class="x-editable">{name:ellipsis(18)}</span>',
						'</div>',
					'</tpl>',
					'<div class="x-clear"></div>'),
				style: {
					overflow: 'auto'
				},
				multiSelect: true,
				overClass: 'x-view-over',
				itemSelector: '.ux-filebrowser-thumb-wrap',
				emptyText: '<div class="x-grid-empty">' + this.il8n.noFilesText + '</div>',
				plugins: [
					new Ext.ux.FileBrowserPanel.LabelEditor({
						dataIndex: 'name',
						listeners: {
							complete: {
								fn: this.onThumbEditorComplete,
								scope: this
							}
						}
					})
				],
				listeners: {
					selectionchange: {
						fn: this.onThumbsSelectionChange,
						scope: this
					},
					render: {
						fn: this.onThumbRender,
						scope: this
					},
					contextmenu: {
						fn: this.onThumbsContextMenu,
						scope: this
					}
				}
			})
		});
		
		// config
		var config = Ext.apply(this.initialConfig, {
			layout: 'border',
			border: false,
			items:[{
				region: 'west',
				width: 200,
				autoScroll: true,
				split: true,
				collapsible: true,
				collapseMode: 'mini',
				items: this.tree
			},{
				region: 'center',
				layout: 'card',
				activeItem: 0,
				tbar: new Ext.Toolbar({
					items: [{
						xtype: 'button',
						text: this.il8n.renameText,
						cmd: 'rename',
						iconCls: 'ux-filebrowser-icon-rename',
						canToggle: true,
						disabled: true,
						handler: this.onGridToolbarClick.createDelegate(this)
					},{
						xtype: 'button',
						text: this.il8n.deleteText,
						cmd: 'delete',
						iconCls: 'ux-filebrowser-icon-delete',
						canToggle: true,
						disabled: true,
						handler: this.onGridToolbarClick.createDelegate(this)
					}, '-', {
						xtype: 'button',
						text: this.il8n.uploadText,
                        cmd: 'upload',
						iconCls: 'ux-filebrowser-icon-upload',
						canToggle: true,
						disabled: false,
						handler: this.onGridToolbarClick.createDelegate(this)
					},{
						xtype: 'button',
						text: this.il8n.downloadText,
						cmd: 'download',
						iconCls: 'ux-filebrowser-icon-download',
						canToggle: true,
						disabled: true,
						handler: this.onGridToolbarClick.createDelegate(this)
					}, '-', {
						xtype: 'button',
						text: this.il8n.viewsText,
						iconCls: 'ux-filebrowser-icon-view',
						menu: [{
							text: this.il8n.detailsText,
							cmd: 'switchView',
							iconCls: 'ux-filebrowser-icon-details',
							cardIndex: 0,
							viewMode: 'details',
							handler: this.onGridToolbarClick.createDelegate(this)
						},{
							text: this.il8n.thumbnailsText,
							cmd: 'switchView',
							iconCls: 'ux-filebrowser-icon-thumbnails',
							cardIndex: 1,
							viewMode: 'thumbnails',
							handler: this.onGridToolbarClick.createDelegate(this)
						}]
					}]
				}),
				items: [
					this.grid,
					this.thumbs
				]
			}]
		});

		// appy the config
		Ext.apply(this, config);
		
		// Call parent (required)
		Ext.ux.FileBrowserPanel.superclass.initComponent.apply(this, arguments);
		
		// flag indicating which 'viewMode' is selected
		// can be 'details' or 'thumbnails'
		this.viewMode = 'details';
		
		// install events
		this.addEvents(
			/**
			 * @event beforecreatefolder
			 * Fires before a new folder is created on the server,
			 * return false to cancel the event
			 *
			 * @param	this	{Ext.ux.FileBrowserPanel}
			 * @param	node	{Ext.tree.TreeNode}			The node representing the folder to be created
			 */
			'beforecreatefolder',
			
			/**
			 * @event beforerenamefolder
			 * Fires before folder will be renamed on the server,
			 * return false to cancel the event
			 *
			 * @param	this	{Ext.ux.FileBrowserPanel}
			 * @param	node	{Ext.tree.TreeNode}			The node representing the folder that will be renamed
			 * @param	newName	{String}					The new folder name
			 * @param	oldName	{String}					The old folder name
			 */
			'beforerenamefolder',
			
			/**
			 * @event beforedeletefolder
			 * Fires before folder will be deleted on the server,
			 * return false to cancel the event
			 *
			 * @param	this	{Ext.ux.FileBrowserPanel}
			 * @param	node	{Ext.tree.TreeNode}			The node representing the folder that will be deleted
			 */
			'beforedeletefolder',
			
			/**
			 * @event beforerenamefile
			 * Fires before file will be renamed on the server,
			 * return false to cancel the event
			 *
			 * @param	this	{Ext.ux.FileBrowserPanel}
			 * @param	record	{Ext.data.Record}			The record representing the file that will be renamed
			 * @param	newName	{String}					The new file name
			 * @param	oldName	{String}					The old file name
			 * */
			'beforerenamefile',
			
			/**
			 * @event beforedeletefile
			 * Fires before file will be deleted from the server,
			 * return false to cancel the event
			 *
			 * @param	this	{Ext.ux.FileBrowserPanel}
			 * @param	record	{Ext.data.Record}			The record representing the file that will be deleted
			 */
			'beforedeletefile',
			
			/**
			 * @event beforedownloadfile
			 * Fires before file will be downloaded from the server,
			 * return false to cancel the event
			 *
			 * @param	this	{Ext.ux.FileBrowserPanel}
			 * @param	record	{Ext.data.Record}			The record representing the file that will be downloaded
			 */
			'beforedownloadfile',
			
			/**
			 * @event beforemovefile
			 * Fires before one or more files will be moved to another folder on the server,
			 * return false to cancel the event
			 *
			 * @param	this				{Ext.ux.FileBrowserPanel}
			 * @param	files				{Array}						An array containing Ext.data.Record objects representing the file(s) to move
			 * @param	sourceFolder		{String}					Path of the source folder
			 * @param	destinationFolder	{String}					Path of the destination folder	
			 */
			'beforemovefile',
			
			/**
			 * @event createfolder
			 * Fires when folder was successfully created
			 *
			 * @param	this	{Ext.ux.FileBrowserPanel}
			 * @param	opts	{Object}					The options that were used for the original request
			 * @param	o		{Object}					Decoded response body from the server
			 */
			'createfolder',
			
			/**
			 * @event renamefolder
			 * Fires when folder was successfully renamed
			 *
			 * @param	this	{Ext.ux.FileBrowserPanel}
			 * @param	opts	{Object}					The options that were used for the original request
			 * @param	o		{Object}					Decoded response body from the server
			 */
			'renamefolder',
			
			/**
			 * @event deletefolder
			 * Fires when folder was successfully deleted
			 *
			 * @param	this	{Ext.ux.FileBrowserPanel}
			 * @param	opts	{Object}					The options that were used for the original request
			 * @param	o		{Object}					Decoded response body from the server
			 */
			'deletefolder',
			
			/**
			 * @event renamefile
			 * Fires when file was successfully renamed
			 *
			 * @param	this	{Ext.ux.FileBrowserPanel}
			 * @param	opts	{Object}					The options that were used for the original request
			 * @param	o		{Object}					Decoded response body from the server
			 */
			'renamefile',
			
			/**
			 * @event deletefile
			 * Fires when file(s) was/were successfully deleted
			 *
			 * @param	this	{Ext.ux.FileBrowserPanel}
			 * @param	opts	{Object}					The options that were used for the original request
			 * @param	o		{Object}					Decoded response body from the server
			 */
			'deletefile',
			
			/**
			 * @event movefile
			 * Fires when file(s) was/were successfully moved
			 *
			 * @param	this	{Ext.ux.FileBrowserPanel}
			 * @param	opts	{Object}					The options that were used for the original request
			 * @param	o		{Object}					Decoded response body from the server
			 */
			'movefile',
			
			/**
			 * @event createfolderfailed
			 * Fires when creation of folder failed
			 *
			 * @param	this	{Ext.ux.FileBrowserPanel}
			 * @param	opts	{Object}					The options that were used for the original request
			 * @param	o		{Object}					Decoded response body from the server
			 */
			'createfolderfailed',
			
			/**
			 * @event renamefolderfailed
			 * Fires when renaming folder failed
			 *
			 * @param	this	{Ext.ux.FileBrowserPanel}
			 * @param	opts	{Object}					The options that were used for the original request
			 * @param	o		{Object}					Decoded response body from the server
			 */
			'renamefolderfailed',
			
			/**
			 * @event deletefolderfailed
			 * Fires when deleting folder failed
			 *
			 * @param	this	{Ext.ux.FileBrowserPanel}
			 * @param	opts	{Object}					The options that were used for the original request
			 * @param	o		{Object}					Decoded response body from the server
			 */
			'deletefolderfailed',
			
			/**
			 * @event renamefilefailed
			 * Fires when renaming file failed
			 *
			 * @param	this	{Ext.ux.FileBrowserPanel}
			 * @param	opts	{Object}					The options that were used for the original request
			 * @param	o		{Object}					Decoded response body from the server
			 */
			'renamefilefailed',
			
			/**
			 * @event deletefilefailed
			 * Fires when deleting file(s) failed
			 *
			 * @param	this	{Ext.ux.FileBrowserPanel}
			 * @param	opts	{Object}					The options that were used for the original request
			 * @param	o		{Object}					Decoded response body from the server
			 */
			'deletefilefailed',
			
			/**
			 * @event movefilefailed
			 * Fires when moving file(s) failed
			 *
			 * @param	this	{Ext.ux.FileBrowserPanel}
			 * @param	opts	{Object}					The options that were used for the original request
			 * @param	o		{Object}					Decoded response body from the server
			 */
			'movefilefailed'
		);
		
	}, // eo function initComponent
	
	/**
	 * Event handler for when 'plus' tool in the tree header is clicked
	 * Invokes creation of new folder
	 *
	 * @private
	 * @param	evt		{Ext.EventObject}	The click event
	 * @param	toolEl	{Ext.Element}		The tool element
	 * @param	panel	{Ext.Panel}			The host panel
	 * @param	tc		{Ext.Panel}			The tool config object
	 * @returns			{Void}
	 */
	onPlusToolClick: function(evt, toolEl, panel, tc) {
		var node = this.tree.getSelectionModel().getSelectedNode();
		this.invokeCreateFolder(node);
	}, // eo function onPlusToolClick
	
	/**
	 * Event handler for when 'minus' tool in the tree header is clicked
	 * Invokes deletion of selected folder
	 *
	 * @private
	 * @param	evt		{Ext.EventObject}	The click event
	 * @param	toolEl	{Ext.Element}		The tool element
	 * @param	panel	{Ext.Panel}			The host panel
	 * @param	tc		{Ext.Panel}			The tool config object
	 * @returns			{Void}
	 */
	onMinusToolClick: function(evt, toolEl, panel, tc) {
		// get the selected folder from the tree and delete it
		var node = this.tree.getSelectionModel().getSelectedNode();
		this.deleteFolder(node);
	}, // oe onMinusToolClick
	
	/**
	 * Event handler for when 'gear' tool in the tree header is clicked
	 * Invokes renaming of selected folder
	 *
	 * @private
	 * @param	evt		{Ext.EventObject}	The click event
	 * @param	toolEl	{Ext.Element}		The tool element
	 * @param	panel	{Ext.Panel}			The host panel
	 * @param	tc		{Ext.Panel}			The tool config object
	 * @returns			{Void}
	 */
	onGearToolClick: function(evt, toolEl, panel, tc) {
		var node = this.tree.getSelectionModel().getSelectedNode();
		this.treeEditor.triggerEdit(node);
	}, // eo function onGearToolClick
	
	/**
	 * Event handler for when tree node is right-clicked
	 * Shows context menu
	 *
	 * @private
	 * @param	node	{Ext.tree.TreeNode}	Tree node that was right-clicked
	 * @param	evt		{Ext.EventObject}	Event object
	 * @returns			{Void}
	 */
	onTreeContextMenu: function(node, evt) {
		evt.stopEvent();
		node.select();
		
		var contextMenu = this.getTreeContextMenu();
		contextMenu.find('text', this.il8n.renameText)[0].setDisabled(node.isRoot);
		//contextMenu.find('text', this.il8n.deleteText)[0].setDisabled(node.isRoot);
		contextMenu.showAt(evt.getXY());
	}, // eo function onTreeContextMenu
	
	/**
	 * Event handlers for when grid row is right-clicked
	 * Shows context menu
	 *
	 * @private
	 * @param	grid		{Ext.grid.GridPanel}	Grid panel that was right-clicked
	 * @param	rowIndex	{integer}				Index of the selected row
	 * @param	evt			{Ext.EventObject}		Event object
	 * @returns				{Void}
	 */
	onGridContextMenu: function(grid, rowIndex, evt) {
		evt.stopEvent();
		grid.getSelectionModel().selectRow(rowIndex);
		
		var contextMenu = this.getGridContextMenu();
		contextMenu.rowIndex = rowIndex;
		contextMenu.showAt(evt.getXY());
	}, // eo function onGridContextMenu
	
	/**
	 * @private
	 * @param	dataView	{Ext.DataView}		The containing data view
	 * @param	index		{Number}			Index of the node that was right clicked
	 * @param	node		{HTMLElement}		The node that was right clicked
	 * @param	evt			{Ext.EventObject}	Event object
	 * @returns				{Void}
	 */
	onThumbsContextMenu: function(dataView, index, node, evt) {
		evt.preventDefault();
		dataView.select(node);
		
		var contextMenu = this.getGridContextMenu();
		contextMenu.dataView = dataView;
		contextMenu.node = node;
		contextMenu.showAt(evt.getXY());
	}, // eo function onThumbsContextMenu
	
	/**
	 * Event handler for when tree-specific contextmenu item is clicked
	 * Delegates actions for menu items to other methods
	 *
	 * @private
	 * @param menu	{Ext.menu.Menu}		The context menu
	 * @param menu	{Ext.menu.Item}		The menu item that was clicked
	 * @param menu	{Ext.EventObject}	Event object
	 * @returns		{Void}
	 */
	onTreeContextMenuClick: function(menu, menuItem, evt) {
		var node = this.tree.getSelectionModel().getSelectedNode();
		switch(menuItem.cmd) {
			case 'new':
				this.invokeCreateFolder(node);
			break;
			case 'rename':
				this.treeEditor.triggerEdit(node);
			break;
			
			case 'delete':
				this.deleteFolder(node);
			break;
		}
		
	}, // eo function onTreeContextMenuClick
	
	/**
	 * Event handler for when grid-specific contentmenu item is clicked
	 * Delegates actions for menu items to other methods
	 *
	 * @private
	 * @param menu	{Ext.menu.Menu}		The context menu
	 * @param menu	{Ext.menu.Item}		The menu item that was clicked
	 * @param menu	{Ext.EventObject}	Event object
	 * @returns		{Void}
	 */
	onGridContextMenuClick: function(menu, menuItem, evt) {
		
		switch(menuItem.cmd) {
			case 'rename':
				if(this.viewMode == 'details') {
					var colIndex = this.grid.getColumnModel().getIndexById('name');
					this.grid.startEditing(menu.rowIndex, colIndex);
				} else if(this.viewMode == 'thumbnails') {
					// get LabelEditor from DataView (first plugin)
					// and the record that is represented by the right-clicked node
					var labelEditor = menu.dataView.plugins[0];
					var record = menu.dataView.getRecord(menu.node);
					
					// get the <span> that contains the text
					var el = Ext.DomQuery.selectNode(labelEditor.labelSelector, menu.node);
					
					// invoke editing
					labelEditor.startEdit(el, record.get('name'));
					labelEditor.activeRecord = record;
				}
			break;
			
			case 'delete':
				if(this.viewMode == 'details') {
					var records = this.grid.getSelectionModel().getSelections();
				} else if(this.viewMode == 'thumbnails') {
					var records = menu.dataView.getSelectedRecords();
				}
				this.deleteFile(records);
			break;
			
			case 'download':
				if(this.viewMode == 'details') {
					var record = this.grid.getSelectionModel().getSelected();
				} else if(this.viewMode == 'thumbnails') {
					var record = menu.dataView.getRecord(menu.node);
				}
				this.downloadFile(record);
			break;
			
			default:
			break;
		}
	}, // eo function onGridContextMenuClick
	
	/**
	 * Event handler for all button that are clicked in the grid toolbar
	 *
	 * @private
	 * @param	button	{Ext.Button}		The button that was clicked
	 * @param	evt		{Exr.EventObject}	The click event
	 * @returns			{Void}
	 */
	onGridToolbarClick: function(button, evt) {
		
		switch(button.cmd) {
			case 'rename':
				if(this.viewMode == 'details') {
					var rowIndex = this.grid.getSelectionModel().last;
					var colIndex = this.grid.getColumnModel().getIndexById('name');
					this.grid.startEditing(rowIndex, colIndex);
				} else if(this.viewMode == 'thumbnails') {
					// get the DataView from the thumbs panel (first item)
					// then get the LabelEditor from the DataView (first plugin)
					var dataView = this.thumbs.get(0);
					var labelEditor = dataView.plugins[0];
					
					// get the first selected node which is an HTMLElement
					// and the record that is represents
					var node = dataView.getSelectedNodes()[0];
					var record = dataView.getRecord(node);
					
					// get the <span> that contains the text
					var el = Ext.DomQuery.selectNode(labelEditor.labelSelector, node);
					
					// make sure only one node is selected
					dataView.select(node);
					
					// invoke editing
					labelEditor.startEdit(el, record.get('name'));
					labelEditor.activeRecord = record;
				}
			break;
			
			case 'delete':
				if(this.viewMode == 'details') {
					var records = this.grid.getSelectionModel().getSelections();
				} else if(this.viewMode == 'thumbnails') {
					var dataView = this.thumbs.get(0);
					var records = dataView.getSelectedRecords();
				}
				this.deleteFile(records);
			break;
			
			case 'download':
				if(this.viewMode == 'details') {
					var record = this.grid.getSelectionModel().getSelected();
				} else if(this.viewMode == 'thumbnails') {
					var dataView = this.thumbs.get(0);
					var record = dataView.getSelectedRecords()[0];
				}
				this.downloadFile(record);
			break;
			
			case 'switchView':
				this.grid.getSelectionModel().clearSelections();
				this.thumbs.get(0).clearSelections();
				
				this.find('region', 'center')[0].getLayout().setActiveItem(button.cardIndex);
				this.viewMode = button.viewMode;
			break;
                        case 'upload':
                            var node = this.tree.getSelectionModel().getSelectedNode();
                            var dialog = new Ext.ux.UploadDialog.Dialog({
                              url: '/service/upload/backendupload?id=' + sid,
                              reset_on_hide: false,
                              allow_close_on_upload: true,
                              upload_autostart: true,
                              modal: true
                            });
                            dialog.on({
                                uploadcomplete: function(updialog) {
                                    Ext.getCmp('filebrowser-grid').store.reload();
                                }
                            });
                            dialog.setBaseParams({path:node.getPath('text')});
                            dialog.show();
                            
			break;
			default:
			break;
		}
		
	}, // eo function onGridToolbarClick
	
	/**
	 * Event handler for when editit of treenode is initiated, but before the value changes
	 * Checks if node is not the root node, since it's not allowed to change that
	 *
	 * @param	editor	{Ext.tree.TreeEditor}	The tree editor
	 * @param	boundEl	{Ext.Element}			Underlying element
	 * @param	value	{mixed}					Field value
	 * @returns			{Boolean}				Proceed?
	 */
	onTreeEditorBeforeStartEdit: function(editor, boundEl, value) {
		return (editor.editNode.isRoot) ? false : true;
	}, // eo function onTreeEditorBeforeStartEdit
	
	/**
	 * Event handler for when node has been edited but change is not yet been
	 * reflected in the underlying field
	 * Return false to undo editing
	 *
	 * @private
	 * @param	editor		{Ext.tree.TreeEditor}	The tree editor where the node was edited
	 * @param	newValue	{String}				New value of the node
	 * @param	oldValue	{String}				Old value of the node
	 * @returns				{Boolean}				Proceed?
	 */
	onTreeEditorBeforeComplete: function(editor, newValue, oldValue) {
		// set the new folder name in the node beiing renamed
		editor.editNode.setText(newValue);
		// check if the node beiing edited is a new or existing folder
		if(editor.editNode.attributes.isNew) {
			this.createFolder(editor.editNode);
			this.treeEditor.ignoreNoChange = true;
		} else {
			this.renameFolder(editor.editNode, newValue, oldValue);
		}
		
		return true;
	}, // eo function onTreeEditorBeforeComplete
	
	/**
	 * Event handler for when filename is changed
	 * Checks if new filename doesn't already exist
	 *
	 * @private
	 * @param	event	{Ext.EventObject}	Edit event object
	 * @returns			{Boolean}			Success
	 */
	onGridEditorAfterEdit: function(evt) {
		// check if the extension was changed, we don't allow this
		var extensionChanged = this.checkExtensionChanged(evt.value, evt.originalValue);
		if(extensionChanged) {
			this.alertExtensionChanged(evt.originalValue);
			evt.record.reject();
			return false;
		}
		
		this.renameFile(evt.record, evt.value, evt.originalValue);
		return true;
	}, // eo function onGridEditorAfterEdit
	
	/**
	 * Event handler for when thumbnail has been edited
	 * Return false to undo editing
	 * 
	 * @private
	 * @param	editor		{Ext.Editor}	The LabelEditor
	 * @param	newValue	{String}		New value of the file
	 * @param	oldValue	{String}		Old value of the file
	 * @returns				{Boolean}		Proceed
	 */
	onThumbEditorComplete: function(editor, newValue, oldValue) {
		// check if the extension of the file was not changed, we dont allow this
		var extensionChanged = this.checkExtensionChanged(newValue, oldValue);
		if(extensionChanged) {
			this.alertExtensionChanged(oldValue);
			return false;
		}
		
		// set the new filename in the record beiing modified
		editor.activeRecord.set(editor.dataIndex, newValue);
		this.renameFile(editor.activeRecord, newValue, oldValue);
		return true;
	}, // eo function onThumbEditorComplete
	
	/**
	 * Event handler for when the selection changes in the tree
	 * Causes files to be loaded in the grid for selected node
	 * Toggles displaying of 'minus' and 'gear' tools depending on selection
	 *
	 * @private
	 * @param	sm		{Ext.tree.DefaultSelectionModel}	Selection model
	 * @param	node	{Ext.tree.TreeNode}					The selected tree node
	 * @returns			{Void}
	 */
	onTreeSelectionChange: function(sm, node) {
		this.grid.getStore().load();
		//this.tree.getTool('minus').setDisplayed((!node.isRoot));
		//this.tree.getTool('gear').setDisplayed((!node.isRoot));
	}, // eo function onTreeSelectionChange
	
	/**
	 * Event handler for when selection in the grid changes
	 * En- or disables buttons in the toolbar depending on selection in the grid
	 *
	 * @private
	 * @param	sm	{Ext.grid.RowSelectionModel}	The selection model
	 * @returns		{Void}
	 */
	onGridSelectionChange: function(sm) {
		if(sm.hasSelection()) {
			this.enableGridToolbarButtons();
		} else {
			//this.disableGridToolbarButtons();
		}
	}, // eo function onGridSelectionChange
	
	/**
	 * Event handler for when the selection of the thumbnails changes
	 * En- or disables buttons in the toolbar depending on the selections
	 *
	 * @private
	 * @param	dataView	{Ext.DataView}	The dataview that contains the thumbnails
	 * @param	selections	{Array}			The selected nodes
	 * @returns				{Void}
	 */
	onThumbsSelectionChange: function(dataView, selections) {
		if(selections.length > 0) {
			this.enableGridToolbarButtons();
		} else {
			//this.disableGridToolbarButtons();
		}
	}, // eo function onThumbsSelectionChange
	
	/**
	 * Event handler for when the tree is about to load new data
	 * Appends folder path of the selected node to the request
	 *
	 * @private
	 * @param	treeLoader	{Ext.tree.TreeLoader}	The Treeloader
	 * @param	node		{Ext.tree.TreeNode}		The selected node
	 * @param	callback	{Object}				Callback function specified in the 'load' call from the treeloader
	 * @returns				{Void}
	 */
	onTreeLoaderBeforeLoad: function(treeLoader, node, callback) {
		treeLoader.baseParams.mode = 'get-folders';
		treeLoader.baseParams.path = node.getPath('text');
	}, // eo function onTreeLoaderBeforeLoad
	
	/**
	 * Event handler for when the grid is about to load new data
	 * Appends the folder path of the selected node to the request
	 *
	 * @private
	 * @param	store	{Ext.data.JsonStore}	The store object
	 * @param	opts	{Object}				Loading options
	 * @returns			{Void}
	 */
	onGridStoreBeforeLoad: function(store, opts) {
		store.baseParams.mode = 'get-files';
		store.baseParams.path = this.tree.getSelectionModel().getSelectedNode().getPath('text');
	}, // eo function onGridStoreBeforeLoad
	
	/**
	 * Event handler for when the tree is renderer
	 * Selects the root node, causing the files in the root to be loaded
	 *
	 * @private
	 * @param	tree	{Ext.tree.TreePanel}	The tree panel
	 * @returns	void
	 */
	onTreeRender: function(tree) {
		tree.getSelectionModel().select(tree.root);
	}, // eo function onTreeRender
	
	/**
	 * Event handler when the grid is rendered
	 * Creates a new tooltip that shows on the grid rows
	 *
	 * @private
	 * @param	grid	{Ext.grid.GridPanel}	The grid panel
	 * @returns			{Void}
	 */
	onGridRender: function(grid) {
		this.tip = new Ext.ToolTip({
			view: this.getView(),
			target: this.getView().mainBody,
			delegate: '.x-grid3-row',
			renderTo: Ext.getBody(),
			listeners: {
				beforeshow: function(tip) {
					var text = Ext.DomQuery.selectValue('td:first div', tip.triggerElement);
					tip.body.update(text);
				}
			}
		});
	}, // eo function onGridRender
	
	/**
	 * Event handler for when the thumbnail panel is rendered
	 * Configures dragzone for dataview
	 *
	 * @private
	 * @param	dataView	{Ext.DataView}	The DataView from the panel
	 * @returns				{Void}
	 */
	onThumbRender: function(dataView) {
		// configure new DragZone object
		dataView.dragZone = new Ext.dd.DragZone(dataView.getEl(), {
			/**
			 * DragDrop group this zone belongs to
			 */
			ddGroup: 'fileMoveDD',
			
			/**
			 * Implementation of getDragData
			 * Returns an object literal containing information about what is being dragged
			 *
			 * @param	evt	{Ext.EventObject}	MouseDown event
			 * @returns		{Object}			Object with drag data
			 */
			getDragData: function(evt) {
				// get the event target, that be the item that is beiing dragged
				var sourceEl = evt.getTarget(dataView.itemSelector);				
				if(sourceEl) {
					// if nothing is selected, select the node beiing dragged
					// else/if multiple nodes are selected and the node beiing dragged is outside the selection,
					// select the node beiing dragged
					if(dataView.getSelectedNodes().length === 0) {
						dataView.select(sourceEl);
					} else if(!evt.ctrlKey && !evt.shiftKey &&
							  dataView.getSelectedNodes().length > 0 &&	
							  dataView.getSelectedNodes().indexOf(sourceEl) == -1) {
						dataView.select(sourceEl);
					}
					
					// clone the node to use as dragProxy
					dragProxyHtml = sourceEl.cloneNode(true);
					dragProxyHtml.id = Ext.id();
					
					// compose and return dragData
					return {
						ddel: dragProxyHtml,
						sourceEl: sourceEl,
						repairXY: Ext.fly(sourceEl).getXY(),
						sourceStore: dataView.store,
						draggedRecord: dataView.getRecord(sourceEl),
						selections: dataView.getSelectedRecords()
					};
				}
				
				return null;
			},
			
			/**
			 * Gets the XY position used to restore the drag in case of invalid drop
			 *
			 * @returns	{Array}	XY location
			 */
			getRepairXY: function() {
				return this.dragData.repairXY;
			}
		});
	}, // onThumbRender
	
	/**
	 * Event handler for when a file from the grid is dropped on a folder
	 * in the tree
	 * 
	 * @private
	 * @param	nodeData	{Object}			Custom data associated with the drop node
	 * @param	source		{Ext.dd.DragSource}	The source that was dragged over this dragzone
	 * @param	evt			{Ext.EventObject}	The Event object
	 * @param	data		{Object}			Object containing arbitrairy data supplied by drag source
	 * @returns				{Boolean}
	 */
	onTreeNodeDrop: function(nodeData, source, evt, data) {
		// get the source- and destination folder
		var from = this.tree.getSelectionModel().getSelectedNode().getPath('text');
		var to = nodeData.node.getPath('text');
		
		// move the file
		this.moveFile(data.selections, from, to);
		return true;
	}, // eo function onTreeNodeDrop
	
	/**
	 * Gets and lazy creates context menu for folder tree
	 *
	 * @private
	 * @returns {Ext.menu.Menu} Context menu
	 */
	getTreeContextMenu: function() {
		
		if(!this.treeContextMenu) {
			this.treeContextMenu = new Ext.menu.Menu({
				items: [{
					text: this.il8n.newText,
					cmd: 'new',
					iconCls: 'ux-filebrowser-icon-newfolder'
				},{
					text: this.il8n.renameText,
					cmd: 'rename',
					iconCls: 'ux-filebrowser-icon-renamefolder'
				}],
				listeners: {
					click: {
						fn: this.onTreeContextMenuClick,
						scope: this
					}
				}
			});
		}
		
		return this.treeContextMenu;
	}, // eo function getTreeContextMenu
	
	/**
	 * Gets and lazy creates context menu for file grid
	 *
	 * @private
	 * @returns {Ext.menu.Menu} Context menu
	 */
	getGridContextMenu: function() {
		
		if(!this.gridContextMenu) {
			this.gridContextMenu = new Ext.menu.Menu({
				items: [{
					text: this.il8n.renameText,
					cmd: 'rename',
					iconCls: 'ux-filebrowser-icon-renamefile'
				},{
					text: this.il8n.deleteText,
					cmd: 'delete',
					iconCls: 'ux-filebrowser-icon-deletefile'
				},{
					text: this.il8n.downloadText,
					cmd: 'download',
					iconCls: 'ux-filebrowser-icon-downloadfile'
				}],
				listeners: {
					click: {
						fn: this.onGridContextMenuClick,
						scope: this
					}
				}
			});
		}
		
		return this.gridContextMenu;
	}, // eo function getGridContextMenu
	
	/**
	 * Invokes the creation of a new folder
	 * 
	 * @private
	 * @param	node	{Ext.tree.TreeNode}	The node under which the new folder needs to be created
	 * @returns			{Void}
	 */
	invokeCreateFolder: function(node) {
		// expand the selected node, append a new childnode to it and immediately start editing it
		node.expand(false, true, function(node) {
			var newNode = node.appendChild(new Ext.tree.TreeNode({
				text: this.il8n.newFolderText,
				cls: 'x-tree-node-collapsed',
				isNew: true		// flag indicating to create a new folder rather than renaming an existing one
			}));
			this.treeEditor.ignoreNoChange = false;
			this.treeEditor.triggerEdit(newNode);
		}, this);
	}, // eo function invokeCreateFolder

	/**
	 * Checks if the extension of a renamed file was not changed
	 *
	 * @private
	 * @param	newName	{String}	New filename
	 * @param	oldName	{String}	Old filename
	 * @returns			{Boolean}	If extension was changed
	 */
	checkExtensionChanged: function(newName, oldName) {
		//var matchNew = Ext.form.VTypes.filenameVal.exec(newName);
		//var matchOld = Ext.form.VTypes.filenameVal.exec(oldName);
		return false;
	}, // eo function checkExtensionChanged
	
	/**
	 * Alert user that filename cannot be changed because
	 * the extension may not change
	 *
	 * @private
	 * @param	fileName	{String}	The original filename
	 * @param	callback	{Function}	Optional callback function to execute after the alert
	 * @returns				{Void}
	 */
	alertExtensionChanged: function(fileName, callback) {
		Ext.Msg.show({
			title: this.il8n.extensionChangeTitleText,
			msg: String.format(this.il8n.extensionChangeMsgText, fileName),
			buttons: Ext.Msg.OK,
			icon: Ext.Msg.ERROR,
			closable: false,
			fn: callback || Ext.EmptyFn
		});
	}, // eo function alertExtensionChanged

	/**
	 * Disables buttons in the toolbar that are marked 'toggleable'
	 *
	 * @private
	 * @returns {Void}
	 */
	disableGridToolbarButtons: function() {
		var buttons = this.find('region', 'center')[0].getTopToolbar().find('canToggle', true);
		Ext.each(buttons, function(button) {
			//button.disable();
		});
	}, // eo function disableGridToolbarButtons
	
	/**
	 * Enables buttons in the toolbar that are marked 'toggleable'
	 *
	 * @private
	 * @returns {Void}
	 */
	enableGridToolbarButtons: function() {
		var buttons = this.find('region', 'center')[0].getTopToolbar().find('canToggle', true);
		Ext.each(buttons, function(button) {
			button.enable();
		});
	}, // eo function enableGridToolbarButtons
	
	/**
	 * Callback that handles all actions performed on the server (rename, move etc.)
	 * Called when Ajax request finishes, regardless if this was a success or not
	 *
	 * @private
	 * @param	opts		{Object}	The options that were used for the original request
	 * @param	success		{Boolean}	If the request succeded
	 * @param	response	{Object}	The XMLHttpRequest object containing the response data
	 * @returns				{Void}
	 */
	actionCallback: function(opts, success, response) {
		var o = {};
		
		// check if request was successful
		if(true !== success) {
			Ext.Msg.show({
				title: this.il8n.actionRequestFailureTitleText,
				msg: this.il8n.actionRequestFailureMsgText,
				buttons: Ext.Msg.OK,
				icon: Ext.Msg.ERROR,
				closable: false
			});
			return;
		}
		
		// decode response in try..catch, response might be mangled/incorrect
		// show error message in case of failure
		try {
			o = Ext.decode(response.responseText);
		} catch(e) {
			Ext.Msg.show({
				title: this.il8n.actionResponseFailureTitleText,
				msg: this.il8n.actionResponseFailureMsgText,
				buttons: Ext.Msg.OK,
				icon: Ext.Msg.ERROR,
				closable: false
			});
		}
		
		// check if server reports all went well
		// handle success/failure accordingly
		if(true === o.success) {
			switch(opts.params.mode) {
				
				case 'create-folder':
					// fire createfolder event
					if(true !== this.eventsSuspended) {
						this.fireEvent('createfolder', this, opts, o);
					}
					
					// remove flag
					delete opts.node.attributes.isNew;
				break;
				
				case 'rename-folder':
					// fire renamefolder
					if(true !== this.eventsSuspended) {
						this.fireEvent('renamefolder', this, opts, o);
					}
				break;
				
				case 'delete-folder':					
					// fire deletefolder event
					if(true !== this.eventsSuspended) {
						this.fireEvent('deletefolder', this, opts, o);
					}
					
					// remove node
					opts.node.parentNode.select();
					opts.node.remove();
				break;
				
				case 'rename-file':
					// fire renamefile event
					if(true !== this.eventsSuspended) {
						this.fireEvent('renamefile', this, opts, o);
					}
					
					// commit the change in the record
					opts.record.commit();
				break;
				
				case 'delete-file':
					// fire deletefile event
					if(true !== this.eventsSuspended) {
						this.fireEvent('deletefile', this, opts, o);
					}
					
					// delete record(s) from the grid
					var store = this.grid.getStore();
					Ext.each(o.data.successful, function(item, index, allItems) {
						var record = store.getById(item.recordId)
						store.remove(record);
					});
				break;
				
				case 'move-file':
					// fire movefile event
					if(true !== this.eventsSuspended) {
						this.fireEvent('movefile', this, opts, o);
					}
					
					// delete record(s) from the grid
					var store = this.grid.getStore();
					Ext.each(o.data.successful, function(item, index, allItems) {
						var record = store.getById(item.recordId)
						store.remove(record);
					});
				break;
				
				default:
				break;
			}
		} else {
			switch(opts.params.mode) {
				
				case 'create-folder':
					// fire createfolderfailed event
					if(true !== this.eventsSuspended) {
						this.fireEvent('createfolderfailed', this, opts, o);
					}
					
					// remove the node
					opts.node.remove();
				break;
				
				case 'rename-folder':
					// fire renamefolderfailed event
					if(true !== this.eventsSuspended) {
						this.fireEvent('renamefolderfailed', this, opts, o);
					}
					
					// reset name
					opts.node.setText(opts.oldName);
				break;
				
				case 'delete-folder':
					// fire deletefolderfailed event
					if(true !== this.eventsSuspended) {
						this.fireEvent('deletefolderfailed', this, opts, o);
					}
				break;
				
				case 'rename-file':
					// fire renamefilefailed event
					if(true !== this.eventsSuspended) {
						this.fireEvent('renamefilefailed', this, opts, o);
					}
					
					// reject the change in the record
					opts.record.reject();
				break;
				
				case 'delete-file':
					// fire deletefilefailed event
					if(true !== this.eventsSuspended) {
						this.fireEvent('deletefilefailed', this, opts, o);
					}
					
					// delete successfully moved record(s) from the grid
					var store = this.grid.getStore();
					Ext.each(o.data.successful, function(item, index, allItems) {
						var record = store.getById(item.recordId)
						store.remove(record);
					});
				break;
				
				case 'move-file':
					// fire movefilefailed event
					if(true !== this.eventsSuspended) {
						this.fireEvent('movefilefailed', this, opts, o);
					}
					
					// delete successfully moved record(s) from the grid
					var store = this.grid.getStore();
					Ext.each(o.data.successful, function(item, index, allItems) {
						var record = store.getById(item.recordId)
						store.remove(record);
					});
					
					// prompt for overwrite
					if(o.data.existing.length > 0) {
						Ext.Msg.show({
							title: this.il8n.confirmOverwriteTitleText,
							msg: this.il8n.confirmOverwriteMsgText,
							buttons: Ext.Msg.YESNO,
							icon: Ext.Msg.QUESTION,
							closable: false,
							scope: this,
							fn: function(buttonId, text, cfg) {
								if(buttonId == 'yes') {
									// create array with remaining files
									var files = [];
									var store = this.grid.getStore();
									Ext.each(o.data.existing, function(item, index, allItems){
										files.push(store.getById(item.recordId));
									});
									
									// call again, but with overwrite option
									this.moveFile(files, opts.params.sourcePath, opts.params.destinationPath, true);
								}
							}
						});
					}
				break;
				
				default:
				break;
			}
		}
		
	}, // eo function actionCallback
	
	/**
	 * Create a new folder on the server
	 *
	 * @private
	 * @param	node	{Ext.tree.TreeNode}	The node representing the folder to create
	 * @returns			{Void}
	 */
	createFolder: function(node) {
		// fire beforecreatefolder event
		if(true !== this.eventsSuspended &&
		   false === this.fireEvent('beforecreatefolder', this, node)) {
			return;
		}
		
		// send request to server
		Ext.Ajax.request({
			url: this.dataUrl,
			node: node,
			callback: this.actionCallback,
			scope: this,
			params: {
				mode: 'create-folder',
				path: node.getPath('text')
			}
		});
	}, // eo function createFolder
	
	/**
	 * Renames a given folder on the server
	 *
	 * @private
	 * @param	node	{Ext.tree.TreeNode}	The treenode that needs to be renamed
	 * @param	newName	{String}			The old foldername
	 * @param	oldName	{String}			The new foldername
	 * @returns			{Void}
	 */
	renameFolder: function(node, newName, oldName) {
		// fire beforerenamefolder event
		if(true !== this.eventsSuspended &&
		   false === this.fireEvent('beforerenamefolder', this, node, newName, oldName)) {
			return;
		}
		
		// send request to server
		Ext.Ajax.request({
			url: this.dataUrl,
			node: node,
			newName: newName,
			oldName: oldName,
			callback: this.actionCallback,
			scope: this,
			params: {
				mode: 'rename-folder',
				path: node.parentNode.getPath('text'),
				newName: newName,
				oldName: oldName
			}
		});
		
	}, // eo function renameFolder
	
	/**
	 * Deletes folder from the server
	 *
	 * @private
	 * @param	node	{Ext.tree.TreeNode}	The treenode that needs to be deleted
	 * @returns			{Void}
	 */
	deleteFolder: function(node) {
		// fire beforedeletefolder event
		if(true !== this.eventsSuspended &&
		   false === this.fireEvent('beforedeletefolder', this, node)) {
			return;
		}
		
		// confirm removal
		Ext.Msg.show({
			title: this.il8n.confirmDeleteFolderTitleText,
			msg: String.format(this.il8n.confirmDeleteFolderMsgText, node.text),
			buttons: Ext.Msg.YESNO,
			icon: Ext.Msg.QUESTION,
			closable: false,
			scope: this,
			fn: function(buttonId) {
				if(buttonId == 'yes') {
					// send request to server
					Ext.Ajax.request({
						url: this.dataUrl,
						node: node,
						callback: this.actionCallback,
						scope: this,
						params: {
							mode: 'delete-folder',
							path: node.getPath('text')
						}
					});
				}
			}
		});
		
	}, // eo function deleteFolder
	
	/**
	 * Renames file on the server
	 *
	 * @private
	 * @param	record	{Ext.data.Record}	Record representing the file that is beiing renamed
	 * @param	newName	{String}			New filename
	 * @param	oldName	{String}			Old filname
	 * @returns			{Void}
	 */
	renameFile: function(record, newName, oldName) {
		// fire beforerenamefile event
		if(true !== this.eventsSuspended &&
		   false === this.fireEvent('beforerenamefile', this, record, newName, oldName)) {
			return;
		}
		
		// send request to server
		Ext.Ajax.request({
			url: this.dataUrl,
			record: record,
			newName: newName,
			oldName: oldName,
			callback: this.actionCallback,
			scope: this,
			params: {
				mode: 'rename-file',
				path: this.tree.getSelectionModel().getSelectedNode().getPath('text'),
				newName: newName,
				oldName: oldName
			}
		});
		
	}, // eo function renameFile
	
	/**
	 * Deletes file from the server
	 *
	 * @private
	 * @param	files	{Array}	Array of Ext.data.Record objects representing the files that need to be deleted
	 * @returns			{Void}
	 */
	deleteFile: function(files) {
		// fire beforedeletefile event
		if(true !== this.eventsSuspended &&
		   false === this.fireEvent('beforedeletefile', this, files)) {
			return;
		}
		
		// set request parameters
		var params = {
			mode: 'delete-file'
		};
		
		// loop over files array and add request parameters like:
		// files[rec-id] : /Root/path/to/filename.ext
		var folder = this.tree.getSelectionModel().getSelectedNode().getPath('text') + '/';
		Ext.each(files, function(item, index, allItems){
			params['files[' + item.id + ']'] = folder + item.get('name');
		});
		// prepare confirmation texts depending on amount of files
		var dialogTitle = this.il8n.confirmDeleteSingleFileTitleText;
		var dialogMsg = String.format(this.il8n.confirmDeleteSingleFileMsgText, files[0].get('name'));
		if(files.length > 1) {
			dialogTitle = this.il8n.confirmDeleteMultipleFileTitleText;
			dialogMsg = String.format(this.il8n.confirmDeleteMultipleFileMsgText, files.length)
		}
		
		// confirm removal
		Ext.Msg.show({
			title: dialogTitle,
			msg: dialogMsg,
			buttons: Ext.Msg.YESNO,
			icon: Ext.Msg.QUESTION,
			closable: false,
			scope: this,
			fn: function(buttonId) {
				if(buttonId == 'yes') {
					// send request to server
					Ext.Ajax.request({
						url: this.dataUrl,
						callback: this.actionCallback,
						scope: this,
						params: params
					});
				}
			}
		});
	}, // eo function deleteFile

	/**
	 * Download a file from the server
	 * Shamelessly stolen from Saki's FileTreePanel (Saki FTW! :p) 
	 * But it does exactly what i need it to do, and it does it very well..
	 * @see http://filetree.extjs.eu/ 
	 * 
	 * @private
	 * @param	record	{Ext.data.Record}	Record representing the file that needs to be downloaded
	 * @returns	{Void}
	 */
	downloadFile: function(record) {
		// fire beforedownloadfile event
		if(true !== this.eventsSuspended &&
		   false === this.fireEvent('beforedownloadfile', this, record)) {
			return;
		}

                PrintShopCreator.Base.Common.openWindow('cmsWindow', '/admin/file/all?mode=download-file&path=' + this.getSelectedFilePath() + '&id='+ sid, 800, 600);

	},

	/**
	 * Move a file on the server to another folder
	 *
	 * @private
	 * @param	files				{Array}		Array of Ext.data.Record objects representing the files to move
	 * @param	sourceFolder		{String}	Source folder
	 * @param	destinationFolder	{String}	Destination folder
	 * @param	overwrite			{Boolean}	If files should be overwritten in destination, defaults to false
	 * @returns						{Void}
	 */
	moveFile: function(files, sourceFolder, destinationFolder, overwrite) {
		// fire beforemovefile event
		if(true !== this.eventsSuspended &&
		   false === this.fireEvent('beforemovefile', this, files, sourceFolder, destinationFolder)) {
			return;
		}
		
		// set request parameters
		var params = {
			mode: 'move-file',
			sourcePath: sourceFolder,
			destinationPath: destinationFolder,
			overwrite: (true === overwrite) ? true : false
		};
		
		// loop over files array and add request parameters like:
		// files[rec-id] : filename.ext
		Ext.each(files, function(item, index, allItems) {
			params['files[' + item.id + ']'] = item.get('name');
		});
		
		// send request to server
		Ext.Ajax.request({
			url: this.dataUrl,
			scope: this,
			callback: this.actionCallback,
			scope: this,
			params: params
		});
	}, // eo function moveFile
	
	/**
	 * Refreshes the grid with data from the server
	 *
	 * @returns	{Void}
	 */
	refreshGrid: function() {
		this.grid.getStore().load();
	}, // eo function refreshGrid

	/**
	 * Gets the full path of the current selected file
	 *
	 * @returns {String} The full path of the current selected file
	 */
	getSelectedFilePath: function() {
		// get selected node from the tree
		var node = this.tree.getSelectionModel().getSelectedNode();
		
		// get the selected node from the grid or thumbnails, depending on viewMode
		if(this.viewMode == 'details') {
			var record = this.grid.getSelectionModel().getSelected();
		} else if(this.viewMode == 'thumbnails') {
			var dataView = this.thumbs.get(0);
			var record = dataView.getSelectedRecords()[0];
		}
		
		// check if they are both present
		if(node === null || record === undefined) {
			return null;
		}
		
		// construct full path and return it
		var path = node.getPath('text') + '/' + record.get('name');
		return path;
	} // eo function getSelectedFilePath
	
}); // eo extend

// register xtype
Ext.reg('ux-filebrowserpanel', Ext.ux.FileBrowserPanel);

/**
 * LabelEditor
 * Used for editing the labels of the thumbnails
 * Code from ExtJS example website
 *
 * @class Ext.ux.FileBrowserPanel.LabelEditor
 * @extends Ext.Editor
 */
Ext.ux.FileBrowserPanel.LabelEditor = Ext.extend(Ext.Editor, {
	alignment: 'tl-tl',
	hideEl: false,
	cls: 'x-small-editor',
	shim: false,
	ignoreNoChange: true,
	completeOnEnter: true,
	cancelOnEsc: true,
	labelSelector: 'span.x-editable',
	
	constructor: function(cfg, field) {
		Ext.ux.FileBrowserPanel.LabelEditor.superclass.constructor.call(this,
			field || new Ext.form.TextField({
				allowBlank: false,
				growMin: 90,
				growMax: 240,
				grow: true,
				selectOnFocus: true
			}), cfg
		);
	},
	
	init: function(view) {
		this.view = view;
		view.on('render', this.initEditor, this);
	},
	
	initEditor: function() {
		this.view.on({
			scope: this,
			containerclick: this.doBlur,
			click: this.doBlur
		});
		this.view.getEl().on('mousedown', this.onMouseDown, this, {delegate: this.labelSelector});
	},
	
	onMouseDown: function(evt, target) {
		if(!evt.ctrlKey && !evt.shiftKey){
			var item = this.view.findItemFromChild(target);
			evt.stopEvent();
			var record = this.view.store.getAt(this.view.indexOf(item));
			this.startEdit(target, record.data[this.dataIndex]);
			this.activeRecord = record;
		} else {
			evt.preventDefault();
		}
	},
	
	doBlur: function() {
		if(this.editing) {
			this.field.blur();
		}
	}
}); // eo extend


/**
 * Strings for internationalization
 */
Ext.ux.FileBrowserPanel.prototype.il8n = {
	displayDateFormat: 'd/m/Y H:i',
	newText: translation._('filemanager_index_js_New'),
	renameText: translation._('filemanager_index_js_Rename'),
	deleteText: translation._('filemanager_index_js_Delete'),
	uploadText: translation._('filemanager_index_js_Upload'),
	downloadText: translation._('filemanager_index_js_Download'),
	viewsText: translation._('filemanager_index_js_Views'),
	detailsText: translation._('filemanager_index_js_Details'),
	thumbnailsText: translation._('filemanager_index_js_Thumbnails'),
	newFolderText: translation._('filemanager_index_js_New_Folder'),
	noFilesText: translation._('filemanager_index_js_No_files_to_display'),
	treePanelHeaderText: translation._('filemanager_index_js_Folders'),
	gridPanelHeaderText: translation._('filemanager_index_js_Files'),
	gridColumnNameHeaderText: translation._('filemanager_index_js_Name'),
	gridColumnSizeHeaderText: translation._('filemanager_index_js_Size'),
	gridColumnTypeHeaderText: translation._('filemanager_index_js_Type'),
	gridColumnDateModifiedText: translation._('filemanager_index_js_Date_Modified'),
	extensionChangeTitleText: translation._('filemanager_index_js_Error_changing_extension'),
	extensionChangeMsgText: translation._('filemanager_index_js_Cannot_rename'),
	confirmDeleteFolderTitleText: translation._('filemanager_index_js_Confirm_folder_delete'),
	confirmDeleteFolderMsgText: translation._("filemanager_index_js_Are_remove_folder_contents"),
	confirmDeleteSingleFileTitleText: translation._('filemanager_index_js_Confirm_file_delete'),
	confirmDeleteSingleFileMsgText: translation._("filemanager_index_js_Are_delete_file"),
	confirmDeleteMultipleFileTitleText: translation._('filemanager_index_js_Confirm_multiple_file_delete'),
	confirmDeleteMultipleFileMsgText: translation._('filemanager_index_js_Are you sure you want to delete these {0} files?'),
	confirmOverwriteTitleText: translation._('filemanager_index_js_Confirm file replace'),
	confirmOverwriteMsgText: translation._('filemanager_index_js_One or more files with the same name already exist in the destination folder. Do you wish to overwrite these?'),
	actionRequestFailureTitleText: translation._('filemanager_index_js_Oh dear..'),
	actionRequestFailureMsgText: translation._('filemanager_index_js_It seems like your colleague spilled coffee on your keyboard. We can\'t send your request until you hang it out to dry'),
	actionResponseFailureTitleText: translation._('filemanager_index_js_PANIC!!'),
	actionResponseFailureMsgText: translation._('filemanager_index_js_Pink elephants are stampeding through the server! Run for the hills!')
};

/**
 * Additional Format function(s) to use
 */
Ext.apply(Ext.util.Format, {
	/**
	 * Format filesize to human readable format
	 * Also deals with filesizes in units larger then MegaBytes
	 * 
	 * @param	size	{number}	Filesize in bytes
	 * @returns			{String}	Formatted filesize
	 */
	bytesToSi: function(size) {
		if(typeof size == 'number' && size > 0) {
			var s = ['b', 'Kb', 'Mb', 'Gb', 'Tb', 'Pb', 'Eb', 'Zb', 'Yb'];
			var e = Math.floor(Math.log(size) / Math.log(1024));
			var r = size / Math.pow(1024, e);
			if(Math.round(r.toFixed(2)) != r.toFixed(2)) {
				r = r.toFixed(2);
			}
			return r + ' ' + s[e];
		} else {
			return '0 b';
		}
	}
	
});// eo apply

/**
 * Additional VType(s)to use
 */
Ext.apply(Ext.form.VTypes, {
	/**
	 * Validation type for filenames
	 * allows only alphanumeric, underscore, hypen and dot
	 * Checks for extension between 2 and 4 karakters
	 */
	filenameVal: /[a-z0-9_\-\.]+\.([a-z0-9]{2,4})$/i,
	filenameMask: /[a-z0-9_\-\.]/i,
	filenameText: 'Filename is invalid or contains illegal characters',
	filename: function(val, field) {
		return Ext.form.VTypes.filenameVal.test(val);
	}
	
}); // eo apply

// eof