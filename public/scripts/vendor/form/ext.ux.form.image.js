/**
 * @author Shea Frederick - http://www.vinylfox.com
 * @class Ext.ux.form.HtmlEditor.Table
 * @extends Ext.util.Observable
 * <p>A plugin that creates a button on the HtmlEditor for making simple tables.</p>
 */
Ext.ux.form.HtmlEditor.Image = Ext.extend(Ext.util.Observable, {
        // private
        cmd: 'image',

        win: null,

        tabs: null,
    /**
     *
     *
     * @cfg {Array} tableBorderOptions
     * A nested array of value/display options to present to the user for table border style. Defaults to a simple list of 5 varrying border types.
     */
        tableBorderOptions: [['none','None'],['1px solid #000','Sold Thin'],['2px solid #000','Solid Thick'],['1px dashed #000','Dashed'],['1px dotted #000','Dotted']],
        // private
    init: function(cmp){
        this.cmp = cmp;
        this.cmp.on('render', this.onRender, this);
    },
        // private
    onRender: function() {
        var tabs;

        var getSelectedImage = function() {

            if (Ext.isIE) {

              // ie specific code
              return function() {
                var selection = cmp.doc.selection;
                if (selection.type == "Control") {
                  var element = selection.createRange()(0);
                  if (element.nodeName.toLowerCase() == 'img') {
                    return element;
                  }
                }
                return null;
              }

            } else {

              // firefox specific code
              return function() {
                var selection = cmp.win.getSelection();
                if (selection.focusOffset == selection.anchorOffset + 1) {
                  var element = selection.focusNode.childNodes[selection.focusOffset - 1];
                  if (element.nodeName.toLowerCase() == 'img') {
                    return element;
                  }
                }
                return null;
              }
            }
          }();

        var insertImageByBrowser = function() {

            if (Ext.isIE) {

              // ie-specific code
              return function() {

                // get selected text/range
                var selection = cmp.doc.selection;
                var range = selection.createRange();

                // insert the image over the selected text/range
                range.pasteHTML(createImage().outerHTML);
              };

            } else {

              // firefox-specific code
              return function() {

                // get selected text/range
                var selection = cmp.win.getSelection();

                // delete selected text/range
                if (!selection.isCollapsed) {
                  selection.deleteFromDocument();
                }

                if(Ext.isChrome) {
                    cmp.insertAtCursor(createImage().outerHTML);
                }else{
                    selection.getRangeAt(0).insertNode(createImage());
                }
              };
            }
          }();
         // insert the image into the editor
        var insertImage = function() {

            // focus on the editor to regain selection
            cmp.focus();

            // insert the image
            insertImageByBrowser();

            // perform required toolbar operations
            //cmp.updateToolbar();
            cmp.deferFocus();
        };
              // set image details to data passed from image browser
      var setImageDetails = function(data) {
        tabs.form.findField('src').setValue(data.url);
        tabs.form.findField('alt').setValue(data.name);
        tabs.form.findField('width').setValue(data.width);
        tabs.form.findField('height').setValue(data.height);
        tabs.form.findField('constrain').setValue(true);
        sourceChanged();
      };

      // create new image node from image details
      var createImage = function() {
        var element = document.createElement("img");
        element.src = tabs.form.findField('src').getValue();
        element.alt = tabs.form.findField('alt').getValue();
        element.style.width = tabs.form.findField('width').getValue() + "px";
        element.style.height = tabs.form.findField('height').getValue() + "px";

        return element;
      }

        // if constraining size ratio then adjust height if width changed
      var widthChanged = function() {
        if (constrained) {
          tabs.form.findField('height').setValue(
            Math.round(tabs.form.findField('width').getValue()
            / originalWidth * originalHeight)
          );
        }
      };

      // if constraining size ratio then adjust width if height changed
      var heightChanged = function() {
        if (constrained) {
          tabs.form.findField('width').setValue(
            Math.round(tabs.form.findField('height').getValue()
            / originalHeight * originalWidth)
          );
        }
      };

      // record original image size when constrain is checked
      var constrain = function(checkbox, checked) {
        constrained = checked;
        if (constrained) {
          originalWidth = tabs.form.findField('width').getValue();
          originalHeight = tabs.form.findField('height').getValue();
          if (!originalWidth || !originalHeight) {
            checkbox.setValue(false);
          }
        }
      };

      // enable insert button when image source has been entered
      var sourceChanged = function() {
        var disabled = (tabs.form.findField('src').getValue() == "");
        Ext.getCmp('insert-btn').setDisabled(disabled);
      };
        var imageBrowser;
        
        tabs = new Ext.FormPanel({
            labelWidth: 120,
            width: 600,
            modal: true,
            items: {
              xtype: 'tabpanel',
              border: false,
              activeTab: 0,
              bodyStyle: 'padding:5px',
              defaults: {autoHeight: true},
              items: [{
                xtype: 'fieldset',
                border: true,
                title: translation._('General'),
                autoHeight: true,
                defaults: {width: 270},
                items: [{
                  xtype: 'trigger',
                    fieldLabel:  translation._('Source'),
                    triggerClass: 'x-form-search-trigger',
                  name: 'src',
                  allowBlank: false,
                            listeners: {
                                    //'change': {fn: sourceChanged, scope: this}
                            },
                    onTriggerClick: function() {
                        if (!imageBrowser) {
                            imageBrowser = new Ext.ux.ImageBrowser({
                                    width: 700,
                                    height: 500,

                                    listURL: '/service/?type=upload&mode=list&id=' + sid,

                        // these are also example php scripts
                                    uploadURL: '/service/upload/uploadcms?sid=' + sid,
                                    deleteURL: '/service/upload/deletecms?sid=' + sid,

                        // set the callback from the image browser
                                    callback: setImageDetails
                            });
                    }
                    imageBrowser.show();
                    }
                }, {
                  xtype: 'textfield',
                  fieldLabel:  translation._('Description'),
                  name: 'alt'
                }, {
                  xtype: 'textfield',
                  fieldLabel:  translation._('Title'),
                  name: 'title'
                }, {
                  layout: "column",
                  autoWidth: true,
                  border: false,
                  defaults: {layout: 'form', border: false},
                  hideLabel: true,
                  items: [{
                    items: [{
                      xtype: "numberfield",
                      fieldLabel:  translation._('Dimensions'),
                      name: 'width',
                      width: 50,
                      allowDecimals: false,
                      allowNegative: false,
                            listeners: {
                                    'change': {fn: widthChanged, scope: this}
                            }
                    }]
                  }, {
                    items: [{
                      xtype: "statictextfield",
                      submitValue: false,
                      hideLabel: true,
                      value: 'x'
                    }]
                  }, {
                    items: [{
                      xtype: "numberfield",
                      hideLabel: true,
                      name: 'height',
                      width: 50,
                      allowDecimals: false,
                      allowNegative: false,
                            listeners: {
                                    'change': {fn: heightChanged, scope: this}
                            }
                    }]
                  }, {
                    items: [{
                      xtype: "statictextfield",
                      hideLabel: true,
                      value: '',
                      width: 15
                    }]
                  }, {
                    items: [{
                      xtype: "checkbox",
                      hideLabel: true,
                      boxLabel:  translation._("Constrain Proportions"),
                      name: 'constrain',
                      checked: false,
                            listeners: {
                                    'check': {fn: constrain, scope: this}
                            }
                    }]
                  }]
                }]
              }]
            }
          });

        win = new Ext.Window({
            title: translation._('Insert/Edit Image'),
            closable: true,
            modal: true,
            closeAction: 'hide',
            width: 600,
            height: 350,
            plain: true,
            layout: 'fit',
            border: false,
            items: tabs,
            buttons: [{
              text: translation._('Save'),
              iconCls: 'action_applyChanges',
              id: 'insert-btn',
              disabled: true,
              handler: function() {
                win.hide();
                insertImage();
              }
            }, {
              text: translation._('Close'),
              iconCls: 'action_cancel',
              handler: function() {
                win.hide();
              }
            }],
            listeners: {
                'show': function() {
                    tabs.form.reset();
                    var element = getSelectedImage();
                    if (element) {

                      // still working on this!!!
                      // need to fix image source as it is changed
                      // from a relative url to an absolute url
                      tabs.form.findField('src').setValue(element.src);
                      tabs.form.findField('alt').setValue(element.alt);
                      tabs.form.findField('width').setValue(element.style.width);
                      tabs.form.findField('height').setValue(element.style.height);
                      tabs.form.findField('constrain').setValue(true);
                    }
                }
            }
          });

        var cmp = this.cmp;
        var btn = this.cmp.getToolbar().addButton({
          iconCls: 'x-edit-image',
          handler: function() {
                    // create Ext.Window if not previously created
              win.show();
          },
          scope: this,
          tooltip: 'Insert Image'
        });
    }
});
