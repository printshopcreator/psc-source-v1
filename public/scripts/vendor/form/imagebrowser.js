// Ext.ux.ImageBrowser
// an image browser for the Ext.ux.HtmlEditorImage plugin

var translation = new Locale.Gettext();
translation.textdomain('imageeditor');

Ext.ux.ImageBrowser = function(config) {

  // PRIVATE

  // cache data by image name for easy lookup
  var lookup = {};
  
  // currently selected image data
  var data;

  // format loaded image data
	var formatData = function(data) {
    data.label = (data.name.length > 15)
      ? data.name.substr(0, 12) + '...' : data.name;
  	data.title = "Name: " + data.name +
  	  "<br>Dimensions: " + data.width + " x " + data.height +
  	  "<br>Size: " + ((data.size < 1024) ? data.size + " bytes"
  	    : (Math.round(((data.size * 10) / 1024)) / 10) + " KB");
  	if (data.width > data.height) {
  	  if (data.width < 80) {
    	  data.thumbwidth = data.width;
    	  data.thumbheight = data.height;
  	  } else {
    	  data.thumbwidth = 80;
    	  data.thumbheight = 80 / data.width * data.height;
    	}
  	} else {
  	  if (data.height < 80) {
    	  data.thumbwidth = data.width;
    	  data.thumbheight = data.height;
  	  } else {
    	  data.thumbwidth = 80 / data.height * data.width;
    	  data.thumbheight = 80;
    	}
  	}
	  data.thumbleft = (Math.round((80 - data.thumbwidth) / 2)) + "px";
	  data.thumbtop = (Math.round((80 - data.thumbheight) / 2)) + "px";
	  data.thumbwidth = Math.round(data.thumbwidth) + "px";
	  data.thumbheight = Math.round(data.thumbheight) + "px";
  	lookup[data.name] = data;
  	return data;
  };

  // create the image upload form
  var form = Ext.getBody().createChild({
    tag: 'form',
    cls: 'x-hidden'
  });

  // called if image was uploaded successfully
  var uploadSuccess = function(response, options) {
    response = Ext.util.JSON.decode(response.responseText);
		if (response.success == 'true') {
		  	this.reset();
		} else {
			this.reset();
		}
  };

  // called if image was not uploaded successfully
  var uploadFailure = function(response, options) {
		Ext.MessageBox.alert("Upload Failed", response.responseText);
  };

  // upload a new image file
  var uploadFile = function(record) {
    
    var inputFileEl = record.detachInputFile();
    inputFileEl.appendTo(form);
    Ext.Ajax.request({
       method: 'post',
       url: this.uploadURL,
       isUpload: true,
       form: form,
       success: uploadSuccess,
       failure: uploadFailure,
       scope: this
    });
  };

  // delete an image file
  var deleteImage = function(doDelete) {

     //indicatorOn();
     if (doDelete === "yes") {
       Ext.Ajax.request({
         method: 'post',
         url: this.deleteURL,
         params: "image=" + data.name,
         success: function(response) {
           //indicatorOff();
     		  this.reset();
     		 // uploadSuccess();
         },
         scope: this
       });
     }
  };

  // confirm if ok to delete image
  var confirmDelete = function() {
    Ext.MessageBox.confirm("Bild l&ouml;schen?",
      "Sind Sie sicher " + data.name + " zu l&ouml;schen?", deleteImage, this);
  };
  
  // create template for image thumbnails
	var thumbTemplate = new Ext.XTemplate(
		'<tpl for=".">',
			'<div class="thumb-wrap" id="{name}">',
				'<div class="thumb"><img src="{url}" ext:qtip="{title}" style="top:{thumbtop}; left:{thumbleft}; width:{thumbwidth}; height:{thumbheight};"></div>',
				'<span>{label}</span>',
		  '</div>',
		'</tpl>'
	);
	thumbTemplate.compile();

  // create json store for loading image data
	var store = new Ext.data.JsonStore({
    url: config.listURL,
    root: 'images',
    fields: [
      'name',
      {name: 'width', type: 'float'},
      {name: 'height', type: 'float'},
      {name: 'size', type: 'float'},
      {name: 'label'},
      'url'
    ],
		listeners: {
			//'beforeload': {fn: indicatorOn, scope: this},
      //'load': {fn: indicatorOff, scope: this},
      //'loadexception': {fn: indicatorOff, scope: this}
		}
	});
	store.load();
  
  // called when image selection is changed
	var selectionChanged = function() {
    var selNode = view.getSelectedNodes();
		if (selNode && selNode.length > 0) {
			selNode = selNode[0];
			Ext.getCmp('select-btn').enable();
			Ext.getCmp('delete-btn').enable();
      data = lookup[selNode.id];
		} else {
	    Ext.getCmp('select-btn').disable();
			Ext.getCmp('delete-btn').disable();
		}
	};
	
  // perform callback to parent function
	var doCallback = function() {
		this.hide(this.animateTarget, function() {
      if (this.callback) {
				this.callback(data);
			}
		});
  };

  // image load exception
	var onLoadException = function(v,o) {
    view.getEl().update('<div style="padding:10px;">Error loading images.</div>');
	};
	
  // create Ext.DataView to display thumbnails
  var view = new Ext.DataView({
		tpl: thumbTemplate,
		singleSelect: true,
		overClass: 'x-view-over',
		itemSelector: 'div.thumb-wrap',
		emptyText : '<div style="padding:10px;">No images match the specified filter</div>',
		store: store,
		listeners: {
			'selectionchange': {fn: selectionChanged, scope: this, buffer: 100},
			'dblclick': {fn: doCallback, scope: this},
			'loadexception': {fn: onLoadException, scope: this},
			'beforeselect': {fn: function(view) {
        return view.store.getRange().length > 0;
	    }}
		},
		prepareData: formatData.createDelegate(this)
	});

  // create filter to easily search images
	var filterView = function() {
		var filter = Ext.getCmp('filter');
		view.store.filter('name', filter.getValue());
	};

  // apply additional config values
  Ext.applyIf(config, {
  	title: translation._('Browse Images'),
  	layout: 'fit',
		minWidth: 514,
		minHeight: 323,
		modal: true,
		closeAction: 'hide',
		border: false,
		items: [{
			id: 'img-browser-view',
			autoScroll: true,
			items: view,
      tbar: ['Filter:', ' ',
      {
      	xtype: 'textfield',
      	id: 'filter',
      	selectOnFocus: true,
      	width: 100,
      	listeners: {
      		'render': {fn: function() {
  		    	Ext.getCmp('filter').getEl().on('keyup', function() {
  		    		filterView();
          	}, this, {buffer:500});
      		}, scope: this}
        }
      }, ' ', '-', {
        xtype: 'browsebuttonupload',
        id: 'add',
        iconCls: 'add-image',
        text: translation._('Upload'),
        handler: uploadFile,
        inputFileName: 'file',
        scope: this
      }, {
        id:'delete-btn',
        iconCls:'delete-image',
        text:translation._('Delete'),
        handler: confirmDelete,
        scope: this
      }, ' ']
		}],
		buttons: [{
			id: 'select-btn',
			text: translation._('Select'),
			iconCls: 'action_applyChanges',
			handler: doCallback,
			scope: this
		}, {
			text: translation._('Close'),
          	iconCls: 'action_cancel',
			handler: function() {
			  this.hide();
			},
			scope: this
		}],
		keys: {
			key: 27, // Esc key
			handler: function() {
			  this.hide();
			},
			scope: this
		}
	});
  
  // call Ext.Window constructor passing config
	Ext.ux.ImageBrowser.superclass.constructor.call(this, config);

  // refresh the image list
	this.reset = function() {
		view.getEl().dom.parentNode.scrollTop = 0;
		store.reload();
		Ext.getCmp('filter').reset();
	};
}

// Ext.ux.ImageBrowser
// extension of Ext.Window
Ext.extend(Ext.ux.ImageBrowser, Ext.Window, {

 
});
