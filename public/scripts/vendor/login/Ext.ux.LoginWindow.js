Ext.namespace('Ext.ux');
Ext.ux.LoginWindow = function(config) {
  Ext.apply(this, config); 
  var css = "#login-logo .x-plain-body {background:#f9f9f9 url('" + this.winBanner + "') no-repeat;}" + "#login-form  {background: " + this.formBgcolor + " none;}" + ".ux-auth-header-icon {background: url('" + this.basePath + "/locked.gif') 0 4px no-repeat !important;}" + ".ux-auth-form {padding:10px;}"+ ".ux-auth-login {background-image: url('" + this.basePath + "/key.gif') !important}"
            + ".ux-auth-close {background-image: url('" + this.basePath + "/close.gif') !important}";
    
  Ext.util.CSS.createStyleSheet(css, this._cssId); 
  this.addEvents({
    'show': true,
    'reset': true,
    'submit': true,
    'submitpass': true
  });
  Ext.ux.LoginWindow.superclass.constructor.call(this, config);
  this._logoPanel = new Ext.Panel({
    baseCls: 'x-plain',
    id: 'login-logo',
    region: 'center'
  }); 
  this.usernameId = Ext.id();
  this.passwordId = Ext.id();
  this.emailId = Ext.id();
  this.emailFieldsetId = Ext.id();
  this.languageId = Ext.id();
  this._loginButtonId = Ext.id();
  this._resetButtonId = Ext.id();
  this._passwordButtonId = Ext.id();
  this._WinPasswordButtonId = Ext.id(); 
  this._formPanel = new Ext.form.FormPanel({
    region: 'south',
    border: false,
    bodyStyle: "padding: 5px;",
    baseCls: 'x-plain',
    id: 'login-form',
    waitMsgTarget: true,    
    labelWidth: 80,
    defaults: {
      width: 300
    },
    baseParams: {
      task: 'login'
    },
    listeners: {
      'actioncomplete': {
        fn: this.onSuccess,
        scope: this
      },
      'actionfailed': {
        fn: this.onFailure,
        scope: this
      }
    },
    height: 110,
    items: [{
      xtype: 'textfield',
      id: this.usernameId,
      name: this.usernameField,
      fieldLabel: this.usernameLabel,
      allowBlank: false
    },
    {
      xtype: 'textfield',
      inputType: 'password',
      id: this.passwordId,
      name: this.passwordField,
      fieldLabel: this.passwordLabel,
      validateOnBlur: false,
      allowBlank: false
    },
    {
      xtype: 'iconcombo',
      id: this.languageId,
      hiddenName: this.languageField,
      fieldLabel: this.languageLabel,
      store: new Ext.data.SimpleStore({
        fields: ['languageCode', 'languageName', 'countryFlag'],
        data: [['de_DE', 'Deutsch', 'ux-flag-de']]
      }),
      valueField: 'languageCode',
      value: this.defaultLanguage,
      displayField: 'languageName',
      iconClsField: 'countryFlag',
      triggerAction: 'all',
      editable: false,
      mode: 'local'
    }]
  });
  Ext.getCmp(this.languageId).on('select',
  function() {
    this.defaultLanguage = Ext.getCmp(this.languageId).getValue(); //var lang = this.defaultLanguage; 	
    this.setlanguage();
  },
  this); 
  //Painel do formul�rio de recupera��o de senha
  this._formPasswordPanel = new Ext.form.FormPanel({
    bodyStyle: "padding: 5px;",
    id: 'password-form',
    waitMsgTarget: true,
    labelWidth: 90,
    autoHeight: true,
    buttonAlign: 'center',
    baseParams: {
      task: 'forgotPassword'
    },
    items: [{
      layout: 'form',
      border: false,
      items: [{
        xtype: 'fieldset',
        title: this.emailFieldset,
        id: this.emailFieldsetId,
        autoHeight: true,
        items: [{
          xtype: 'textfield',
          vtype: this.emailVtype,
          id: this.emailId,
          name: this.emailField,
          fieldLabel: this.emailLabel,
          vtype: this.emailVtype,
          validateOnBlur: false,
          anchor: '98%',
          allowBlank: false
        }]
      }]
    }],
    buttons: [{
      text: this.passwordButton,
      id: this._WinPasswordButtonId,
      width: 100,
      handler	: this.Passwordsubmit,
      scope: this
    }]
  }); 
  var buttons = [{
    id: this._loginButtonId,
    text: this.loginButton,
    handler: this.submit,
    scale: 'medium',
    scope: this
  }];
  var keys = [{
    key: [10, 13],
    handler: this.submit,
    scope: this
  }]; 
  if (typeof this.passwordButton == 'string') {
    buttons.push({
      id: this._passwordButtonId,
      text: this.passwordButton,      
      handler: this.password,
      scale: 'medium',
      scope: this
    });
  } 
  if (typeof this.resetButton == 'string') {
    buttons.push({
      id: this._resetButtonId,
      text: this.resetButton,      
      handler: this.reset,
      scale: 'medium',
      scope: this
    });
    keys.push({
      key: [27],
      handler: this.reset,
      scope: this
    });
  } 
  this._window = new Ext.Window({
    width: 429,
    height: 280,
    closable: false,
    resizable: false,
    draggable: true,
    modal: this.modal,
    iconCls: 'ux-auth-header-icon',
    title: this.title,
    layout: 'border',
    bodyStyle: 'padding:5px;',
    buttons: buttons,
    buttonAlign: 'center',
    keys: keys,
    plain: false,
    items: [this._logoPanel, this._formPanel]
  }); 
  this._windowPassword = new Ext.Window({
    width: 350,
    height: 160,
    closable: true,
    resizable: false,
    draggable: true,
    modal: this.modal,
    iconCls: 'ux-auth-header-icon',
    title: this.Passwordtitle,
    bodyStyle: 'padding:5px;',
    keys: keys,
    closeAction: 'hide',
    items: [this._formPasswordPanel]
  }); 
  this._window.on('show',
  function() {
    this.setlanguage();
    Ext.getCmp(this.usernameId).focus(false, true);    
    this.fireEvent('show', this);
  },
  this);
}; 
Ext.extend(Ext.ux.LoginWindow, Ext.util.Observable, {
  /**
     * T�tulo da janela principal
     *
     * @type {String}
     */
  title: '',
  /**
     * T�tulo da janela de recupera��o de senha
     *
     * @type {String}
     */
  Passwordtitle: '',  
    /**
     * T�tulo do fieldset da janela de recupera��o de senha
     *
     * @type {String}
     */
  emailFieldset: '',  
  /**
     * Mensagem de espera ao enviar os dados
     *
     * @type {String}
     */
  waitMessage: '',
  /**
     * Texto do bot�o de login
     *
     * @type {String}
     */
  //loginButton : Ext.lang.us.login,
  loginButton: '',
  /**
     * Texto do bot�o de recupera��o de senha
     *
     * @type {String}
     */
  passwordButton: '',
  /**
     * Texto do bot�o limpar
     *
     * @type {String}
     */
  resetButton: '',
  /**
     * T�tulo do campo usu�rio
     *
     * @type {String}
     */
  usernameLabel: '',
  /**
     * Nome do campo usu�rio
     *
     * @type {String}
     */
  usernameField: 'username',
  /**
     * Valida��o do campo usu�rio
     *
     * @type {String}
     */
  usernameVtype: 'alphanum',
  /**
     * T�tulo do campo email
     *
     * @type {String}
     */
  emailLabel: '',
  /**
     * Nome do campo email
     *
     * @type {String}
     */
  emailField: 'email',
  /**
     * Valida��o do campo email
     *
     * @type {String}
     */
  emailVtype: 'email',
  /**
     * T�tulo do campo senha
     *
     * @type {String}
     */
  passwordLabel: '',
  /**
     * Nome do campo senha
     *
     * @type {String}
     */
  passwordField: 'password',
  /**
     * Valida��o do campo senha
     *
     * @type {String}
     */
  passwordVtype: 'alphanum',
  /**
     * Nome do combo idioma
     *
     * @type {String}
     */
  languageField: 'lang',
  /**
     * T�tulo do iconcombobox idioma
     *
     * @type {String}
     */
  languageLabel: '',
  /**
     * Url de requisi��o de login
     *
     * @type {String}
     */
  url: '',
  /**
     * Url de requisi��o de recupera��o de senha
     *
     * @type {String}
     */
  emailUrl: '',  
  /**
     * Url de destino caso login seja efetivado
     *
     * @type {String}
     */
  locationUrl: '',
  /**
     * Diret�rio das imagens
     *
     * @type {String}
     */
  basePath: 'img',
  /**
     * Logotipo do sistema (Banner)
     *
     * @type {String}
     */
  winBanner: '',
  /**
     * Cor de fundo do formul�rio
     *
     * @type {String}
     */
  formBgcolor: '',
  /**
     * M�todo de envio do formul�rio
     *
     * @type {String}
     */
  method: 'post',

  resetsuccesstitle: 'Erfolgreich',
  resetsuccessmsg: 'Sie bekommen eine Mail mit dem Freischaltcode',

  reseterrortitle: 'Fehler',
  reseterrormsg: 'EMailadresse nicht gefunden',
  /**
     * Abrir janela modal
     *
     * @type {Bool}
     */
  modal: false,
  /**
     * Identificador do CSS
     *
     * @type {String}
     */
  _cssId: 'ux-LoginWindow-css',
  /**
     * Painel topo (Logotipo do sistema)
     *
     * @type {Ext.Panel}
     */
  _logoPanel: null,
  /**
     * Painel do formul�rio
     *
     * @type {Ext.form.FormPanel}
     */
  _formPanel: null,
  /**
     * Objeto da janela principal
     *
     * @type {Ext.Window}
     */
  _window: null,
  /**
     * Objeto da janela de recupera��o de senha
     *
     * @type {Ext.Window}
     */
  _windowPassword: null,
  /**
     * Exibe a  janela principal
     *
     * @param {Ext.Element} el
     */
  show: function(el) {
    this._window.show(el);
  },
  /**
     * Exibe a  janela de recupera��o de senha
     *
     * @param {Ext.Element} el
     */
  password: function(el) {
    this._windowPassword.show(el);
  },
  /**
     * Limpa o formul�rio
     */
  reset: function() {
    if (this.fireEvent('reset', this)) {
      this._formPanel.getForm().reset();
    }
  },  
  /**
     * Idioma padr�o do formul�rio
     */
  defaultLanguage: 'de_DE',
  /**
     * Seleciona o idioma
     */
  setlanguage: function() {
    Ext.override(Ext.form.Field, {
      setFieldLabel: function(text) {
        if (this.rendered) {
          this.el.up('.x-form-item', 10, true).child('.x-form-item-label').update(text);
        } else {
          this.fieldLabel = text;
        }
      }
    });
    if (this.defaultLanguage == 'pt_PT') {
      this._window.setTitle('Autentica��o');
      this._windowPassword.setTitle('Recuperar senha');
      Ext.getCmp(this._loginButtonId).setText('Entrar');
      Ext.getCmp(this._passwordButtonId).setText('Recuperar senha');
      Ext.getCmp(this._resetButtonId).setText('Limpar');
      Ext.getCmp(this._WinPasswordButtonId).setText('Recuperar senha');
      Ext.getCmp(this.emailId).setFieldLabel('Email');
      Ext.getCmp(this.emailFieldsetId).setTitle('Digite seu email'); 
      Ext.getCmp(this.usernameId).setFieldLabel('Usu�rio:');
      Ext.getCmp(this.passwordId).setFieldLabel('Senha:');
      Ext.getCmp(this.languageId).setFieldLabel('Idioma:');      
      this.waitMessage = 'Enviando dados...';  
    } else if (this.defaultLanguage == 'es_ES') {
      this._window.setTitle('Autenticaci�n');
      this._windowPassword.setTitle('Recuperar contrase�a');
      Ext.getCmp(this._loginButtonId).setText('Inicio');
      Ext.getCmp(this._passwordButtonId).setText('Recuperar contrase�a');
      Ext.getCmp(this._resetButtonId).setText('Limpiar');
      Ext.getCmp(this._WinPasswordButtonId).setText('Recuperar contrase�a');
      Ext.getCmp(this.emailId).setFieldLabel('Email');
      Ext.getCmp(this.emailFieldsetId).setTitle('Ingrese su e-mail');
      Ext.getCmp(this.usernameId).setFieldLabel('Usuario:');
      Ext.getCmp(this.passwordId).setFieldLabel('Contrase�a:');
      Ext.getCmp(this.languageId).setFieldLabel('Idioma:');
      this.waitMessage = 'Env�o de datos...'; 
      } else if (this.defaultLanguage == 'fr_FR') {
      this._window.setTitle('Authentification');
      this._windowPassword.setTitle('R�cup�rer le mot de passe');
      Ext.getCmp(this._loginButtonId).setText('Entrer');
      Ext.getCmp(this._passwordButtonId).setText('R�cup�rer le mot de passe');
      Ext.getCmp(this._resetButtonId).setText('Propre');
      Ext.getCmp(this._WinPasswordButtonId).setText('R�cup�rer le mot de passe');
      Ext.getCmp(this.emailId).setFieldLabel('E-mail');
      Ext.getCmp(this.emailFieldsetId).setTitle('Entrez votre email');
      Ext.getCmp(this.usernameId).setFieldLabel('Utilisateur:');
      Ext.getCmp(this.passwordId).setFieldLabel('Mot de passe:');
      Ext.getCmp(this.languageId).setFieldLabel('Langue:');
      this.waitMessage = 'Envoi de donn�es...'; 
    } else if (this.defaultLanguage == 'it_IT') {
    	this._window.setTitle('Autenticazione');
      this._windowPassword.setTitle('Recuperare la password');
      Ext.getCmp(this._loginButtonId).setText('Inserisci');
      Ext.getCmp(this._passwordButtonId).setText('Recuperare la password');
      Ext.getCmp(this._resetButtonId).setText('Cancella');
      Ext.getCmp(this._WinPasswordButtonId).setText('Recuperare la password');
      Ext.getCmp(this.emailId).setFieldLabel('E-mail');
      Ext.getCmp(this.emailFieldsetId).setTitle('Inserisci la tua e-mail');
      Ext.getCmp(this.usernameId).setFieldLabel('Utente:');
      Ext.getCmp(this.passwordId).setFieldLabel('Password:');
      Ext.getCmp(this.languageId).setFieldLabel('Lingua:');   
      this.waitMessage = 'Invio di dati...';  	
    } else if (this.defaultLanguage == 'en_US') {
      this._window.setTitle('Authentication');
      this._windowPassword.setTitle('Recover password');
      Ext.getCmp(this._loginButtonId).setText('Login');
      Ext.getCmp(this._passwordButtonId).setText('Recover password');
      Ext.getCmp(this._resetButtonId).setText('Clear');
      Ext.getCmp(this._WinPasswordButtonId).setText('Recover password');
      Ext.getCmp(this.emailId).setFieldLabel('Email');
      Ext.getCmp(this.emailFieldsetId).setTitle('Enter your email');
      Ext.getCmp(this.usernameId).setFieldLabel('Username:');
      Ext.getCmp(this.passwordId).setFieldLabel('Password:');
      Ext.getCmp(this.languageId).setFieldLabel('Language:');
      this.waitMessage = 'Sending data...';     	
    } else if (this.defaultLanguage == 'de_DE') {
      this._window.setTitle('Anmeldung');
      this._windowPassword.setTitle('Password wiederherstellen');
      Ext.getCmp(this._loginButtonId).setText('Login');
      Ext.getCmp(this._passwordButtonId).setText('Passwort wiederherstellen');
      Ext.getCmp(this._resetButtonId).setText('Reset');
      Ext.getCmp(this._WinPasswordButtonId).setText('Senden');
      Ext.getCmp(this.emailId).setFieldLabel('Email');
      Ext.getCmp(this.emailFieldsetId).setTitle('Bitte EMail eingeben');
      Ext.getCmp(this.usernameId).setFieldLabel('Benutzername:');
      Ext.getCmp(this.passwordId).setFieldLabel('Passwort:');
      Ext.getCmp(this.languageId).setFieldLabel('Sprache:');
      this.waitMessage = 'Sende Daten...';
    }
  },
  
  /**
     * Envia a requisi��o de login
     */
    submit : function () {
        var form = this._formPanel.getForm();

        if (form.isValid())
        {
            Ext.getCmp(this._loginButtonId).disable();
            if(Ext.getCmp(this._cancelButtonId)) {
                Ext.getCmp(this._cancelButtonId).disable();
            }
            if (this.fireEvent('submit', this, form.getValues()))
            {
                form.submit ({
                    url     : this.url,
                    method  : this.method,
                    waitMsg : this.waitMessage,
                    success : this.onSuccess,
                    failure : this.onFailure,
                    scope   : this
                });
            }
        }
    },
    
    /**
     * Envia a requisi��o de recupera��o de senha 
     */
    Passwordsubmit : function () {
        var form = this._formPasswordPanel.getForm();

        if (form.isValid())
        {
            Ext.getCmp(this._WinPasswordButtonId).disable();
            if (this.fireEvent('submitpass', this, form.getValues()))
            {
                form.submit ({
                    url     : this.emailUrl,
                    method  : this.method,
                    waitMsg : this.waitMessage,
                    success : this.onEmailSuccess,
                    failure : this.onEmailFailure,
                    scope   : this
                });
            }
        }
    },
    
  /**
     * Se receber sucesso
     *
     * @param {Ext.form.BasicForm} form
     * @param {Ext.form.Action} action
     */
  onSuccess: function(form, action) {
    if (action && action.result) {
      window.location = this.locationUrl;
    }
  },
  /**
     * Se receber falha
     *
     * @param {Ext.form.BasicForm} form
     * @param {Ext.form.Action} action
     */
  onFailure: function(form, action) { // enable buttons
	 Ext.MessageBox.show({
          title: 'Fehler',
          msg: action.result.errors.reason[0],
          buttons: Ext.MessageBox.OK,
          animEl: 'mb9',
          icon: Ext.MessageBox.ERROR
      });
    Ext.getCmp(this._loginButtonId).enable();
    if (Ext.getCmp(this._resetButtonId)) {
      Ext.getCmp(this._resetButtonId).enable();
    }
  },  
  /**
     * Se receber sucesso
     *
     * @param {Ext.form.BasicForm} form
     * @param {Ext.form.Action} action
     */
  onEmailSuccess: function(form, action) {
    if (action && action.result) {
      Ext.MessageBox.show({
							title: this.resetsuccesstitle,
							msg: this.resetsuccessmsg,
							buttons: Ext.MessageBox.OK,
							icon: Ext.MessageBox.INFO
						});
    }
  },
  /**
     * Se receber falha
     *
     * @param {Ext.form.BasicForm} form
     * @param {Ext.form.Action} action
     */
  onEmailFailure: function(form, action) { 
  	// Ativa os bot�es
    Ext.getCmp(this._WinPasswordButtonId).enable();
    Ext.MessageBox.show({
							title: this.reseterrortitle,
							msg: this.reseterrormsg,
							buttons: Ext.MessageBox.OK,
							icon: Ext.MessageBox.INFO
						});
  }
});
