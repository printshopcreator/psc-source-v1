$(function () {

    $('#saveEditAdresse').click(function () {
        $.post(
            '/user/addressupdate/' + $('#editaddressuuid').val() + '/format/json',
            $("#updateaddress").serialize(),
            function (data) {
                if (data.success) {
                    loadAddressesDelivery();
                    loadAddressesInvoice();
                    loadAddressesSender();
                }
            }
        )
        $('#editAdresse').modal('hide');
        return false;
    });


    $('#saveAdresse').click(function () {
        $.post(
            '/user/addressadd/format/json',
            $("#register").serialize(),
            function (data) {
                if (data.success) {
                    loadAddressesDelivery();
                    loadAddressesInvoice();
                    loadAddressesSender();

                    $('#addAdress').modal('hide');

                } else {
                    $.each(data.messages, function (index, item) {
                        $('#register #' + index).css('border', '1px solid red');
                    });
                }
            });

        return false;
    });

    editadress();

    loadAddressesDelivery();
    loadAddressesInvoice();
    loadAddressesSender();

    $('#sameDeliveryAsInvoice').click(function() {
        $('#delivery').val($('#invoice').val());
        $('#deliveryaddresstext').html($('#invoiceaddresstext').html());
    });

    $('#sameSenderAsShop').click(function() {
        $('#sender').val(0);
        $('#senderaddresstext').html($('#senderaddresstext_hidden').html());
    });
    
    $('#myDelivery .search').change(function() {
        loadAddressesDelivery();
    });

    $('#myInvoice .search').change(function() {
        loadAddressesInvoice();
    });

    $('#mySender .search').change(function() {
        loadAddressesSender();
    });
});

function loadAddressesDelivery() {

    $.ajax({
        type: "POST",
        url: '/user/addressget/format/json',
        data: {
            'term': $('#myDelivery .search').val(),
            'mode': '2'
        },
        success: function(data) {

            var markup = '<tr id="address-<%= uuid %>">'
                        + '<td class="kundenNr"><%= kundenNr %></td>'
                        + '<td class="company"><%= company %></td>'
                        + '<td class="company2"><%= company2 %></td>'
                        + '<td class="names"><%= anredeText %> <%= firstname %> <%= lastname %></td>'
                        + '<td class="address"><%= street %> <%= house_number %> <%= zip %> <%= city %></td>'
                        + '<td class="country"><%= country %></td>'
                        + '<td align="right">'
                        + '<div class="btn-group">'
                        + '<input type="hidden" id="text" name="text" value="<% if(company != "" && company != null) {  %><%= company %>&lt;br/&gt;<% } %><% if(company2 != "" && company2 != null) { %><%= company2 %>&lt;br/&gt;<% } %><%= anredeText %> <%= firstname %> <%= lastname %>&lt;br/&gt;<%= street %> <%= house_number %>&lt;br/&gt;<%= zip %> <%= city %>"/>'
                        + '<input type="hidden" name="id" value="<%= uuid %>"/>'
                        + '<a class="editAddress btn btn-info btn-xs"  data-dismiss="modal" data-toggle="modal" data-target="#editAdresse" href=""><span class="glyphicon glyphicon-pencil"></span></a>'
                        + '<a class="useasdelivery btn btn-success btn-xs" href="" title="verwenden"><span class="glyphicon glyphicon-ok"></span></a>'
                        + '</div>'
                        + '</td>'
                        + '</tr>';

            $('#myDelivery tbody').empty();

            $.each(data.data, function(index, item) {

                $('#myDelivery tbody').append(_.template(markup, item));

            });

            $('.useasdelivery').unbind();
            $('.useasdelivery').click(function () {
                $('#delivery').val($(this).prev().prev().val());
                $('#deliveryaddresstext').html($(this).prev().prev().prev().val());
                $('.modal').modal('hide');
                return false;
            });
            
            editadress();
        }
    });
}

function loadAddressesInvoice() {

    $.ajax({
        type: "POST",
        url: '/user/addressget/format/json',
        data: {
            'term': $('#myInvoice .search').val(),
            'mode': '1'
        },
        success: function(data) {

            var markup = '<tr id="address-<%= uuid %>">'
                + '<td class="kundenNr"><%= kundenNr %></td>'
                + '<td class="company"><%= company %></td>'
                + '<td class="company2"><%= company2 %></td>'
                + '<td class="names"><%= anredeText %> <%= firstname %> <%= lastname %></td>'
                + '<td class="address"><%= street %> <%= house_number %> <%= zip %> <%= city %></td>'
                + '<td class="country"><%= country %></td>'
                + '<td align="right">'
                + '<div class="btn-group">'
                + '<input type="hidden" id="text" name="text" value="<% if(company != "" && company != null) {  %><%= company %>&lt;br/&gt;<% } %><% if(company2 != "" && company2 != null) { %><%= company2 %>&lt;br/&gt;<% } %><%= anredeText %> <%= firstname %> <%= lastname %>&lt;br/&gt;<%= street %> <%= house_number %>&lt;br/&gt;<%= zip %> <%= city %>"/>'
                + '<input type="hidden" name="id" value="<%= uuid %>"/>'
                + '<a class="editAddress btn btn-info btn-xs"  data-dismiss="modal" data-toggle="modal" data-target="#editAdresse" href=""><span class="glyphicon glyphicon-pencil"></span></a>'
                + '<a class="useasinvoice btn btn-success btn-xs" href="" title="verwenden"><span class="glyphicon glyphicon-ok"></span></a>'
                + '</div>'
                + '</td>'
                + '</tr>';

            $('#myInvoice tbody').empty();

            $.each(data.data, function(index, item) {

                $('#myInvoice tbody').append(_.template(markup, item));

            });

            $('.useasinvoice').unbind();
            $('.useasinvoice').click(function () {
                $('#invoice').val($(this).prev().prev().val());
                $('#invoiceaddresstext').html($(this).prev().prev().prev().val());
                $('.modal').modal('hide');
                return false;
            });

            editadress();
        }
    });
}

function loadAddressesSender() {
    $.ajax({
        type: "POST",
        url: '/user/addressget/format/json',
        data: {
            'term': $('#mySender .search').val(),
            'mode': '3'
        },
        success: function(data) {

            var markup = '<tr id="address-<%= uuid %>">'
                + '<td class="kundenNr"><%= kundenNr %></td>'
                + '<td class="company"><%= company %></td>'
                + '<td class="company2"><%= company2 %></td>'
                + '<td class="names"><%= anredeText %> <%= firstname %> <%= lastname %></td>'
                + '<td class="address"><%= street %> <%= house_number %> <%= zip %> <%= city %></td>'
                + '<td class="country"><%= country %></td>'
                + '<td align="right">'
                + '<div class="btn-group">'
                + '<input type="hidden" id="text" name="text" value="<% if(company != "" && company != null) {  %><%= company %>&lt;br/&gt;<% } %><% if(company2 != "" && company2 != null) { %><%= company2 %>&lt;br/&gt;<% } %><%= anredeText %> <%= firstname %> <%= lastname %>&lt;br/&gt;<%= street %> <%= house_number %>&lt;br/&gt;<%= zip %> <%= city %>"/>'
                + '<input type="hidden" name="id" value="<%= uuid %>"/>'
                + '<a class="editAddress btn btn-info btn-xs"  data-dismiss="modal" data-toggle="modal" data-target="#editAdresse" href=""><span class="glyphicon glyphicon-pencil"></span></a>'
                + '<a class="useassender btn btn-success btn-xs" href="" title="verwenden"><span class="glyphicon glyphicon-ok"></span></a>'
                + '</div>'
                + '</td>'
                + '</tr>';

            $('#mySender tbody').empty();

            $.each(data.data, function(index, item) {

                $('#mySender tbody').append(_.template(markup, item));

            });

            $('.useassender').unbind();
            $('.useassender').click(function () {
                $('#sender').val($(this).prev().prev().val());
                $('#senderaddresstext').html($(this).prev().prev().prev().val());
                $('.modal').modal('hide');
                return false;
            });

            editadress();
        }
    });
}


function editadress() {
    $('.editAddress').unbind();
    $('.editAddress').click(function () {

        $.getJSON('/user/addressload/' + $(this).prev().val() + '/format/json', function (data) {
            var items = [];

            $('#editaddressuuid').val(data.address.uuid);
            $('#updateaddress #firstname').val(data.address.firstname);
            $('#updateaddress #lastname').val(data.address.lastname);
            $('#updateaddress #zip').val(data.address.zip);
            $('#updateaddress #city').val(data.address.city);
            $('#updateaddress #fax').val(data.address.fax);
            $('#updateaddress #street').val(data.address.street);
            $('#updateaddress #company2').val(data.address.company2);
            $('#updateaddress #kostenstellung').val(data.address.kostenstellung);
            $('#updateaddress #house_number').val(data.address.house_number);
            $('#updateaddress #company').val(data.address.company);
            $('#updateaddress #phone').val(data.address.phone);
            $('#updateaddress #email').val(data.address.email);
            $('#updateaddress #mobil_phone').val(data.address.mobil_phone);
            $('#updateaddress #update_country').val(data.address.country);
            $('#updateaddress #update_anrede').val(data.address.anrede);
            $('#updateaddress #ustid').val(data.address.ustid);
            $('#updateaddress #homepage').val(data.address.homepage);

        });
        $(document).off('focusin.modal');
        $('#editAdresse').modal('show');

        return false;
    });

    $('.addAdress').unbind();
    $('.addAdress').click(function () {
        $(document).off('focusin.modal');
        $('#addAdresse').modal('show');

        return false;
    });
}
