var calccount = 1;



$(function(){

    $("#CALCFORM select:not(select[id='selectprodukt'], input[name='selectprodukt']), #CALCFORM input[type='radio']:not(input[name='selectprodukt']), #CALCFORM input[type='checkbox'], #CALCFORM input[type='text']").attr('onChange', null);

    $("#CALCFORM select:not(select[id='selectprodukt'], input[name='selectprodukt']), #CALCFORM input[type='radio']:not(input[name='selectprodukt']), #CALCFORM input[type='checkbox'], #CALCFORM input[type='text']").change(function() {
        $(this).closest("form").submit();
        return false;
    });
    $("#CALCFORM #kalk_artikel").unbind();
    $("#CALCFORM #kalk_artikel").change(function() {
        $("#CALCFORM").unbind();
        $(this).closest("form").submit();
    });


    $("#buyform").submit(function(event) {
        if($('#buyform #load').val() == 1) {
            return true;
        }

        if($('#collecting_data_form').length > 0 && $('#collecting_data_form').serialize().length > 0) {
            return;
        }

        if($('#upload_mode').val() == 'none' || $('#upload_mode').val() == '') {

            if($('#myUpload table tr').length == 1 && $('#myUpload table tr td a[rel="#myUploadCenter"]').length == 1) {
                return;
            }

            //       if(b2bshop) {
            if( typeof(b2bshop) != "undefined" && b2bshop) {
                if($('#myUpload table tr').length == 1) {
                    $('#myUpload table tr a.btn').click();
                }else{
                    $('#myUpload').modal();
                }
                return false;
            }else{


                /* das ist mal sehr geraten und gehacked - karsten euting -29.11.2014 */
                $('#myUpload').modal();
                return false;

                if($('#loaded').length == 0 ) {
                    $('.flashmessages').append('<div class="alert alert-block alert-error fade in">' +
                        '<button type="button" class="close" data-dismiss="alert">×</button>' +
                        '<p>Bitte w&auml;hlen Sie wie Sie Ihre Daten &uuml;bertragen wollen!</p>' +
                        '<p><button class="btn btn-primary">Problem beheben</button></p></div>');

                    $('.flashmessages button').unbind();
                    $('.flashmessages button').click(function() {
                        if($('#myUpload table tr').length == 1) {
                            $('#myUpload table tr a.btn').click();
                        }else{
                            $('#myUpload').modal();
                        }
                    });
                    return false;
                }// if($('#loaded').length == 0 )
                else{
                }
            }

        }


    });

    $('#CALCFORM #send').click(function() {
        $(this).closest("form").data("lastButtonClicked", this.id)
    });

    $('#CALCFORM #special').click(function() { /** setzt das (interne) property 'lastButtonClicked' auf 'spezial' */
    $(this).closest("form").data("lastButtonClicked", this.id);
    });

    $("#CALCFORM").submit(function(event){
        if($(this).data("lastButtonClicked") != "" && $(this).data("lastButtonClicked") == 'layouter') {
            return true;
        }

        var special = "?XDEBUG_PROFILE";

        $('#CALCFORM').showLoading();

        if($(this).data("lastButtonClicked") != "" && $(this).data("lastButtonClicked") == 'special') {
            special = '?special=1';

        }

        if(($(this).data("lastButtonClicked") != "" && $(this).data("lastButtonClicked") == 'layouter') || ($("input[name='upload_mode']:checked").length > 0 && $("#CALCFORM").data("upload_mode") != $("input[name='upload_mode']:checked").val()) || $(this).data("lastButtonClicked") != "" && $(this).data("lastButtonClicked") == 'send_anfrage') {

            return true;
        }



        $.post(
            '/article/calc/' + $('#ajax_calc_id').val() + '/format/json' + special,
            $("#CALCFORM").serialize(),
            function(data){
                var lastelement = null;
                var elements = [];


                $('#price_netto').html(data.netto);
                $('#price_steuer').html(data.steuer);
                $('#price_brutto').html(data.brutto);

                if(data.ownValues['display_versand']) {
                    $('#price_versand').html(data.ownValues['display_versand'].toFixed(2).replace(".",",") + ' €');
                }else{
                    $('#price_versand').html('0,00 €');
                }

                if(data.ownValues['layouterDynamicWidth']) {
                    layouterDynamicWidth = data.ownValues['layouterDynamicWidth'].toFixed(0);
                    layouterDynamicHeight = data.ownValues['layouterDynamicHeight'].toFixed(0);
                    layouterDynamicSize = true;
                }

                if(data.precalc) {
                    $.each(data.precalc, function(index, item) {
                        $('#precalc_sum_' + index).html(item.netto);
                        $('#precalc_mwert_' + index).html(item.mwert);
                        $('#precalc_summe_' + index).html(item.summe);
                    });
                }

                if(data.debug) {
                    var debug = "";
                    $.each(data.debug, function(index, item) {
                        debug = debug + "<p>" + item + "</p>";
                    });
                    $('.debug').html(debug);
                }

                if(data.weight > 0) {
                    $('#weight_display').show();
                    $('#weight_display span').html(data.weight);
                }

                var error = false;

                $.each(data.form, function(index, item) {

                    if(item.type == 'checkbox') {

                        if($("input[name='" + item.name + "[]']").length > 0) {

                            $("#" + item.name + "-element").html("");

                            $.each(item.options, function(index, option) {
                                $("#" + item.name + "-element").append('<input type="checkbox" value="' + option.key + '" name="' + item.name + '[]" id="' + item.name + '-' + option.key + '">' + option.value + '<br/>');
                                if(item.onchange) {
                                    $('#' + item.name + '-' + option.key).attr('onChange', item.onchange);
                                }else{
                                    $('#' + item.name + '-' + option.key).change(function() {
                                        $(this).closest("form").submit();
                                    });
                                }
                                elements.push(item.name + '-' + option.key);
                                elements.push(item.name);
                            });
                            $.each(item.value, function(index, option) {
                                $("#" + item.name + "-" + option).attr('checked', 'checked');
                            });

                        }else{

                            var nextEl = $('#' + lastelement + '-element');
                            if(nextEl.length == 0) {
                                var nextEl = $('#' + lastelement);
                            }

                            $('<dt id="' + item.name + '-label"><label for="' + item.name + '">' + item.label + '</label></dt><dd id="' + item.name + '-element"></dd>').insertAfter(nextEl);
                            $.each(item.options, function(index, option) {
                                $('#' + item.name + '-element').append(
                                    $('<input type="checkbox" value="' + option.key + '" name="' + item.name + '[]" id="' + item.name + '-' + option.key + '"/>' + option.value + '<br/>'));
                                if(item.onchange) {
                                    $('#' + item.name + '-' + option.key).attr('onChange', item.onchange);
                                }else{
                                    $('#' + item.name + '-' + option.key).change(function() {
                                        $(this).closest("form").submit();
                                    });
                                }
                                elements.push(item.name + '-' + option.key);
                                elements.push(item.name);
                            });
                            $.each(item.value, function(index, option) {
                                $("#" + item.name + "-" + option).attr('checked', 'checked');
                            });

                        }

                    }

                    if(item.type == 'radio') {

                        if($("input[name='" + item.name + "']").length > 0) {

                            $("#" + item.name + "-element").html("");
                            if(item.name == 'upload_mode') {
                                $.each(item.options, function(index, option) {
                                    $("#" + item.name + "-element").append('<input type="radio" value="' + index + '" name="' + item.name + '" id="' + item.name + '-' + index + '">' + option + '<br/>');
                                    if(item.onchange) {
                                        $('#' + item.name + '-' + index).attr('onChange', item.onchange);
                                    }
                                    elements.push(item.name + '-' + index);
                                });
                            }else{
                                $.each(item.options, function(index, option) {
                                    $("#" + item.name + "-element").append('<input type="radio" value="' + option.key + '" name="' + item.name + '" id="' + item.name + '-' + option.key + '">' + option.value + '<br/>');
                                    if(item.onchange) {
                                        $('#' + item.name + '-' + option.key).attr('onChange', item.onchange);
                                    }
                                    elements.push(item.name + '-' + option.key);
                                    elements.push(item.name);
                                });
                            }
                            $("#" + item.name + "-" + item.value).attr('checked', 'checked');

                        }else{

                            var nextEl = $('#' + lastelement + '-element');
                            if(nextEl.length == 0) {
                                var nextEl = $('#' + lastelement);
                            }

                            $('<dt id="' + item.name + '-label"><label for="' + item.name + '">' + item.label + '</label></dt><dd id="' + item.name + '-element"></dd>').insertAfter(nextEl);
                            $.each(item.options, function(index, option) {
                                $('#' + item.name + '-element').append(
                                    $('<input type="radio" value="' + option.key + '" name="' + item.name + '" id="' + item.name + '-' + option.key + '"/>' + option.value + '<br/>'));
                                if(item.onchange) {
                                    $('#' + item.name + '-' + option.key).attr('onChange', item.onchange);
                                }
                                elements.push(item.name + '-' + option.key);
                                elements.push(item.name);
                            });
                            $("#" + item.name + "-" + item.value).attr('checked', 'checked');

                        }

                    }

                    if(item.type == 'text') {

                        if($("#" + item.name).length > 0) {
                            $("#" + item.name).parent().html('<input type="hidden" name="' + item.name + '" id="' + item.name + '" data-isnote="1" />' + item.value);
                        }else{
                            if(!nextEl) {
                                var nextEl = $('#' + lastelement + '-element').parent().parent();
                                if(nextEl.length == 0) {
                                    if($('#' + lastelement).attr('type') == "hidden") {
                                        var nextEl = $('#' + lastelement);
                                    }else{
                                        var nextEl = $('#' + lastelement).parent().parent();
                                    }
                                    if(nextEl.length == 0) {
                                        var nextEl = $("input[name='" + lastelement + "[]']").first().parent().parent().parent();
                                    }

                                }
                            }
                            $('<div class="form-group"><label for="' + item.name + '" class="form-label optional">' + item.label + '</label><div class="form-controls">' + '<input type="hidden" name="' + item.name + '" id="' + item.name + '" data-isnote="1" />' +  item.value + '</div></div>').insertAfter(nextEl);

                        }

                    }

                    if(item.type != 'checkbox' && item.type != 'radio' && item.type != 'text') {
                        if($('#' + item.name).length > 0) {

                            var itemDom = $('#' + item.name);


                            if(item.type == 'select') {

                                if(itemDom.is('select')) {
                                    $('#' + item.name + ' option').remove();
                                    $.each(item.options, function(index, option) {
                                        $('#' + item.name).append(
                                            $('<option></option>').val(option.key).html(option.value));
                                    });
                                    $("#" + item.name + " option[value='" + item.value + "']").attr('selected', 'selected');
                                }else{

                                    itemDom.replaceWith('<select id="' + item.name + '" name="' + item.name + '" class="form-control"></select>');

                                    $.each(item.options, function(index, option) {
                                        $('#' + item.name).append(
                                            $('<option></option>').val(option.key).html(option.value));
                                    });
                                    $("#" + item.name + " option[value='" + item.value + "']").attr('selected', 'selected');
                                }
                            }

                            if(item.type == 'input') {

                                if(itemDom.is('input[type="text"]')) {
                                    itemDom.val(item.value);
                                }else{
                                    // karsten euting - 17.11.2014 -  itemDom.replaceWith('<input type="text" id="' + item.name + '" name="' + item.name + '" value="' + item.value + '"/>');
                                    itemDom.replaceWith('<input class="form-control" type="text" id="' + item.name + '" name="' + item.name + '" value="' + item.value + '"/>');
                                }
                                if(item.error) {
                                    itemDom.parent().parent().addClass("error");
                                    if(itemDom.parent().find('#error_' + item.name).length == 0) {
                                        if(item.value > item.max) {
                                            $('<div class="help-inline" id="error_' + item.name + '">Maximal: ' + item.max + '</div>').insertAfter(itemDom);
                                        }
                                        if(item.value < item.min) {
                                            $('<div class="help-inline" id="error_' + item.name + '">Minimal: ' + item.min + '</div>').insertAfter(itemDom);
                                        }
                                    }
                                    error = true;

                                }else{
                                    itemDom.parent().parent().removeClass("error");
                                    $('#error_' + item.name).remove();
                                }
                            }

                            if(item.type == 'hidden') {

                                if(itemDom.is('input[type="hidden"]')) {
                                    itemDom.val(item.value);
                                }else{
                                    itemDom.replaceWith('<input type="hidden" id="' + item.name + '" name="' + item.name + '" value="' + item.value + '"/>');

                                }
                            }

                            if(item.type == 'textarea') {

                                if(itemDom.is('textarea')) {
                                    itemDom.val(item.value);
                                }else{
                                    itemDom.replaceWith('<textarea id="' + item.name + '" cols="' + item.cols + '" rows="' + item.rows + '" class="form-control" name="' + item.name + '">' + item.value + '</textarea>');

                                }
                            }

                            if(item.type == 'submit') {

                                if(itemDom.is('input[type="submit"]')) {
                                    itemDom.val(item.label);
                                }else{
                                    itemDom.replaceWith('<input type="submit" id="' + item.name + '" name="' + item.name + '" value="' + item.label + '"/>');
                                }

                                if(item.name == 'send_anfrage') {
                                    $('#CALCFORM #send_anfrage').addClass('btn'); /** ke 17.11.2014 */
                                    $('#CALCFORM #send_anfrage').val('  OK  '); /** ke 26.11.2014 */
                                    $('#CALCFORM #send_anfrage').css('width','10em'); /** ke 26.11.2014 */


                                    $('#CALCFORM #send_anfrage').click(function() {
                                        $(this).closest("form").data("lastButtonClicked", this.id)
                                    });
                                }
                            }


                        }else{

                            var nextEl = $('#' + lastelement + '-element').parent().parent();
                            if(nextEl.length == 0) {
                                if($('#' + lastelement).attr('type') == "hidden") {
                                    var nextEl = $('#' + lastelement);
                                }else{
                                    var nextEl = $('#' + lastelement).parent().parent();
                                }
                                if(nextEl.length == 0) {
                                    var nextEl = $("input[name='" + lastelement + "[]']").first().parent().parent().parent();
                                }

                            }
                            if($(nextEl).data("isnote") === 1) {
                                nextEl = $(nextEl).parent().parent();
                            }

                            var helpLink = "";
                            if(item.helplink) {
                                helpLink = ' helplink="' + item.helplink + "#" + item.ankerlink + '"';
                            }

                            if(item.type == 'select') {
                                $('<div class="form-group"><label for="' + item.name + '" class="control-label optional">' + item.label + '</label><div class="form-controls"><select class="form-control" id="' + item.name + '" name="' + item.name + '"'+helpLink+'></select></div></div>').insertAfter(nextEl);
                                $.each(item.options, function(index, option) {
                                    $('#' + item.name).append(
                                        $('<option></option>').val(option.key).html(option.value));
                                });
                                $("#" + item.name + " option[value='" + item.value + "']").attr('selected', 'selected');
                            }
                            if(item.type == 'hidden') {
                                $('<input type="hidden" id="' + item.name + '" name="' + item.name + '" value="' + item.value + '"/>').insertAfter(nextEl);
                            }
                            if(item.type == 'input') {
                                $('<div class="form-group"><label for="' + item.name + '" class="control-label optional">' + item.label + '</label><div class="form-controls"><input class="form-control" type="text" id="' + item.name + '" name="' + item.name + '" value="' + item.value + '"'+helpLink+'/></div></div>').insertAfter(nextEl);
                                if(item.error) {
                                    $('#' + item.name).parent().parent().addClass("error");
                                    if($('#' + item.name).parent().find('#error_' + item.name).length == 0) {
                                        if(item.value > item.max) {
                                            $('<div class="help-inline" id="error_' + item.name + '">Maximal: ' + item.max + '</div>').insertAfter($('#' + item.name));
                                        }
                                        if(item.value < item.min) {
                                            $('<div class="help-inline" id="error_' + item.name + '">Minimal: ' + item.min + '</div>').insertAfter($('#' + item.name));
                                        }
                                    }
                                    error = true;
                                }
                            }
                            if(item.type == 'textarea') {
                                $('<div class="form-group"><label for="' + item.name + '" class="control-label optional">' + item.label + '</label><div class="form-controls"><textarea cols="' + item.cols + '" rows="' + item.rows + '" class="form-control" id="' + item.name + '" name="' + item.name + '">' + item.value + '</textarea></div></div>').insertAfter(nextEl);
                            }
                            if(item.type == 'submit') {
                                $('<div class="form-group"><label for="' + item.name + '" class="control-label optional">' + item.label + '</label><div class="form-controls"><input type="submit" id="' + item.name + '" class="btn-success btn-large" name="' + item.name + '" value="' + item.label + '"/></div></div>').insertAfter(nextEl);
                                if(item.name == 'send_anfrage') {
                                    $('#CALCFORM #send_anfrage').click(function() {
                                        $(this).closest("form").data("lastButtonClicked", this.id)
                                    });
                                }
                            }
                        }
                    }
                    lastelement = item.name;
                    elements.push(item.name);

                });

                $('#CALCFORM select, #CALCFORM input, #CALCFORM textarea').each(function() {


                    if($.inArray($(this).attr('id'), elements) == -1) {
                        if($(this).attr('type') == "hidden") {
                            if($(this).data("isnote") === 1) {
                                $('#' + $(this).attr('id')).parent().parent().remove();
                            }else {
                                $('#' + $(this).attr('id')).remove();
                            }
                        }else{
                            $('#' + $(this).attr('id')).parent().parent().remove();
                        }
                    }
                    if($.inArray($(this).attr('name').replace("[]", ""), elements) == -1) {
                        if($(this).attr('type') == "hidden") {
                            $('#' + $(this).attr('name').replace("[]", "")).remove();
                        }else{
                            $('#' + $(this).attr('name').replace("[]", "")).parent().parent().remove();
                        }
                    }
                });

                $("#CALCFORM select:not(select[id='selectprodukt'], input[name='selectprodukt']), #CALCFORM input[type='radio']:not(input[name='selectprodukt']), #CALCFORM input[type='checkbox'], #CALCFORM input[type='text'], #CALCFORM textarea").unbind();
                $("#CALCFORM select:not(select[id='selectprodukt'], input[name='selectprodukt']), #CALCFORM input[type='radio']:not(input[name='selectprodukt']), #CALCFORM input[type='checkbox'], #CALCFORM input[type='text'], #CALCFORM textarea").change(function() {
                    $(this).closest("form").submit();
                    return false;
                });


                if(data.brutto.indexOf("0,00") == 0 && calccount < 2) {
                    $('#CALCFORM').hideLoading();
                    $('#CALCFORM').submit();
                    calccount++;
                }else{
                    $('#CALCFORM').hideLoading();
                    calccount = 1;
                }
                if(data.brutto.indexOf("0,00") == 0) {
                    $('#in_basket').addClass('disabled');
                }else{
                    $('#in_basket').removeClass('disabled');
                }

                if(error) {
                    $('#in_basket').addClass('disabled');
                    $('#price_netto, #price_steuer, #price_brutto').html("0,00 €");
                }else{
                    $('#in_basket').removeClass('disabled');
                }

                showInfoButtons();
            },
            "json"
        );

        return false;
    });

    showInfoButtons();

    $('#CALCFORM').submit();
})

function showInfoButtons() {
    $('input[helplink], select[helplink]').each(function() {
        $(this).parent().find(".btn-info").remove();
        $(this).after('<a class="btn btn-info" href="' + $(this).attr('helplink') + '" target="_blank" style="margin-left: 10px"><i class="fa fa-question-circle"></i></a>');
    });

    $('.helpbox').click(function() {

        if( $("#help-modal").length == 0) {
            $('body').append('<div class="modal hide" id="help-modal" style="width: 800px; margin-left: -420px;"><div class="modal-header"><button type="button" class="close" data-dismiss="modal">×</button><h3>Infos</h3></div><div class="modal-body" style="overflow: auto; height: 400px;"></div></div>');


        }

        $('#help-modal .modal-body').load($(this).attr('href') + '?help=1&width=700&height=450');
        $('#help-modal').modal();
        return false;
    });

    $("#CALCFORM select:not(select[id='selectprodukt'], input[name='selectprodukt']), #CALCFORM input[type='radio']:not(input[name='selectprodukt']), #CALCFORM input[type='checkbox'], #CALCFORM input[type='text']").attr('onChange', null);

    $("#CALCFORM select:not(select[id='selectprodukt'], input[name='selectprodukt']), #CALCFORM input[type='radio']:not(input[name='selectprodukt']), #CALCFORM input[type='checkbox'], #CALCFORM input[type='text']").change(function() {
        $(this).closest("form").submit();
        return false;
    });
    $("#CALCFORM #kalk_artikel").unbind();
    $("#CALCFORM #kalk_artikel").change(function() {
        $("#CALCFORM").unbind();
        $(this).closest("form").submit();
    });
}
