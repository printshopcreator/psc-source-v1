class Saxoprint {

    constructor(productId, saxoprintProductId, auflage, mwert) {
        this.productId = productId;
        this.saxoprintProductId = saxoprintProductId;
        this.auflage = auflage;
        this.mwert = mwert;
    }

    init() {
        $('.loading').show();
        this.getProduct();
    }

    getProduct() {
        var self = this;
        $.post("/apps/plugin/custom/psc/saxoprint/getProduct",
            {
                saxoprintProductId: this.saxoprintProductId,
                versand: "standard",
                config: "all",
                mwert: this.mwert
            },
            function(data,status) {
                self.buildUi(data.options, data.default_configuration);
            })
    }

    getAuflagen() {
        $('.loading').show();
        var self = this;

        if($('#auflage option').length > 0) {
            self.calcPrice();
            return;
        }
        $.post("/apps/plugin/custom/psc/saxoprint/getPrices",
            {
                saxoprintProductId: this.saxoprintProductId,
                auflage: this.auflage,
                config: $('#calc_saxoprint').serialize(),
                mwert: this.mwert
            },
            function(data,status) {
                $('#auflage').empty();
                var option = '';
                $.each(data, function(index, item) {
                    option += '<option name="' + index + '">' + index + '</option>'
                })
                $('#auflage').html(option);

                self.calcPrice();
            })
    }

    buildUi(productOptions, defaultConfig) {
        var form = "";
        var options = "";
        var selected = "";

        var auflagen = this.auflage.split(",");

        form += '<div class="form-group form-group-sm">\n' +
            '    <label for="auflage" class="col-sm-4 control-label">Auflage</label>\n' +
            '    <div class="col-sm-8">\n' +
            '      <select class="form-control" id="auflage">';


       /* $.each(auflagen, function(index, item) {
            form += '<option value="' + item + '">' + item + '</option>';
        });*/


        form += '</select>' +
        '    </div>\n' +
        '  </div>';

        $.each(productOptions, function(index, item) {
            options = "";
            $.each(item.values, function(i, value) {
                selected = "";
                if(parseInt(defaultConfig[item.id]) == value.id) {
                    selected = 'selected="selected"';
                }

                options += '<option value="' + value.id + '" ' + selected + '>' + value.name + '</option>';
            });

            form += '<div class="form-group form-group-sm">\n' +
                '    <label for="' + item.id + '" class="col-sm-4 control-label">' + item.name + '</label>\n' +
                '    <div class="col-sm-8">\n' +
                '      <select class="form-control" name="' + item.id + '">' + options + '</select>' +
                '    </div>\n' +
                '  </div>';

        })

        form += '<div class="form-group form-group-sm">\n' +
            '    <label for="versand" class="col-sm-4 control-label">Versand</label>\n' +
            '    <div class="col-sm-8">\n' +
            '      <select class="form-control" name="versand" id="versand">' +
            '           <option value="1055">Standard</option>' +
            '           <option value="1056">Standard+</option>' +
            '           <option value="1057">Express</option>' +
            '           <option value="1058">Express+</option>' +
            '      </select>' +
            '    </div>\n' +
            '  </div>';

        $('#selextender').append(form);
        this.calc();
    }

    bindUi() {
        var self = this;
        $('#calc_saxoprint select, #auflage').unbind();
        $('#calc_saxoprint select').change(function() {
            self.calc();
        });
        $('#auflage').change(function() {
            self.calcPrice();
        });
    }

    calc() {
        $('.loading').show();
        var self = this;
        $.post("/apps/plugin/custom/psc/saxoprint/getAvailableOptions",
            {
                saxoprintProductId: this.saxoprintProductId,
                auflage: $('#auflage').val(),
                versand: "standard",
                config: $('#calc_saxoprint').serialize(),
                mwert: this.mwert
            },
            function(data,status) {
                $.each(data.valid_configuration, function(index, item) {
                    $("#calc_saxoprint select[name='" + index + "'] option").prop('disabled', true);

                    $.each(item, function(i, v) {
                        $("#calc_saxoprint select[name='" + index + "'] option[value='" + v + "']").prop('disabled', false);
                    })
                })

                $.each(data.valid_setup, function(index, item) {
                    $("#calc_saxoprint select[name='" + index + "']").val(item);
                })
                self.getAuflagen();
            })
    }

    storePrice(price) {
        $('.loading').show();

        var options = {'auflage': $('#auflage').val()};
        var infos = [];

        $('#calc_saxoprint select').each(function() {
            infos.push(
                {
                    name: this.name,
                    value: $(this).find('option:selected').val(),
                    text: $(this).find('option:selected').text(),
                }
            );

            options[this.name] = $(this).find('option:selected').text();
        })

        $.post("/apps/plugin/custom/psc/saxoprint/storePrice",
            {
                saxoprintProductId: this.saxoprintProductId,
                productId: this.productId,
                netto: price,
                steuer: price/100*this.mwert,
                brutto: price + (price/100*this.mwert),
                options: JSON.stringify(options),
                infos: JSON.stringify(infos)
            },
            function(data,status) {
                $('.loading').hide();
            })
    }

    calcPrice() {
        var self = this;
        $.post("/apps/plugin/custom/psc/saxoprint/getPricesV3",
            {
                saxoprintProductId: self.saxoprintProductId,
                productId: self.productId,
                auflage: $('#auflage').val(),
                config: $('#calc_saxoprint').serialize(),
                mwert: self.mwert
            },
            function(data,status) {

                $('#saxoprisprice_netto').html(new Intl.NumberFormat('de-DE', { style: 'currency', currency: 'EUR' }).format(data['Price']));
                $('#saxoprice_steuer').html(new Intl.NumberFormat('de-DE', { style: 'currency', currency: 'EUR' }).format(data['Price']/100*self.mwert));
                $('#saxoprice_brutto').html(new Intl.NumberFormat('de-DE', { style: 'currency', currency: 'EUR' }).format(data['Price']+(data['Price']/100*self.mwert)));

                self.bindUi();
                self.storePrice(data['Price']);
            })



    }
}