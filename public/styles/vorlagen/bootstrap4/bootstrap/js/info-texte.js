/**
* Allgemeine Beschreibungen
*
* Aufbau:
* Erst der Erläuterungstext dann ein "|" gefolgt vom Titel der Box
* einfache und doppelte anf.zeichen korrekt?
* datei-format UTF-8?
* alle zeilen mit ";" abgeschlossen
*/

var infotext = new Array ();
    /*infotext['auflage'] = "Tragen Sie hier Ihre Wunschmenge ein.<br><strong>Test Text...</strong>|Auflage";
    infotext['kurz'] = "Breite in cm ein.|Breite";
    infotext['lang'] = "Höhe in cm ein.|Höhe";
    infotext['keilrahmen'] = "Auf wunsch mit Keilrahmen.|Keilrahmen";
    infotext['produktion'] = "Standard Produktionszeit in Werktage.|Produktionszeit";
    infotext['kalk_artikel'] = "Plot-Typ auswählen.|Typ";
    infotext['info'] = "Hier können Sie uns eine Nachricht zu diesen Produkt eintragen.|Zusatzangaben";
    infotext['papier'] = "Hier die neue Beschreibung.|Papier";
    infotext['material'] = "Hier die neue Beschreibung.|Material";
    infotext['datenformat'] = "Hier die neue Beschreibung.|Datenformat";
    infotext['swswseiten'] = "Anzahl Seiten";*/

    
/**
* Spezielle Beschreibungen überschreiben die allgemeinen
* Es muss aber eine allg. Beschreibung vorhanden sein, um überschrieben zu werden.
* Es reicht nicht, sie allein hier zu setzen!
*
* Wichtig ist die korrekte Benennung der Variablen:
* var infotext_ArtikelID_000 -> wobei "000" durch die entsprechende ID des Artikels zu ersetzen ist.
*
* Bitte auf korrekte initialisierung achten!
* var infotext_ArtikelID_000 = new Array();
*
* var infotext_ArtikelID_288 = new Array();
*    infotext_ArtikelID_288['auflage'] = "Bei Broschüren ist alles <strong>anders</strong>|SPEZIALEINTRAG für Broschüre A4 ";
*/