function showstep1(uuid) {
    $('#myMotiv').modal('hide');
    $('#myArticlegroups').modal('show');
    $.ajax({
        type:"POST",
        url:'/article/get' + motive_modal_mode + 'articlegroups/format/json',
        data:{},
        success:function (data) {

            if (data.success) {
                $('#myArticlegroups #product-bar-carousel').replaceWith('<ul id="product-bar-carousel" class="thumbnails"></ul>');
                $.each(data.result, function (i, res) {
                    $('#myArticlegroups #product-bar-carousel').append('<li class="span2 span_custom"><a href="' + res.url + '" rel="' + res.id + '">' + res.image_html + '<h5 rel="' + res.subgroups + '" style="text-align:center">' + res.title + '</h5></a></li>');
                });

                $('#myArticlegroups #product-bar-carousel a').click(function () {
                    var group = $(this).find('h5').attr('rel');
                    var id = $(this).attr('rel');
                    if (group == 'true') {
                        showstep2(uuid, id);
                    } else {
                        showstep3(uuid, 1, id);
                    }
                    return false;
                });


                return;
            }

        }
    });

    $('#myArticlegroups #back').click(function () {
        $('#myArticlegroups').modal('hide');
        $('#myMotiv').modal('show');
    });
}

function showstepnew1() {
    $('#myNewArticlegroups').modal('show');
    $.ajax({
        type:"POST",
        url:'/article/get' + motive_modal_mode + 'articlegroups/format/json',
        data:{},
        success:function (data) {

            if (data.success) {
                $('#myNewArticlegroups #product-bar-carousel').replaceWith('<ul id="product-bar-carousel" class="thumbnails"></ul>');
                $.each(data.result, function (i, res) {
                    $('#myNewArticlegroups #product-bar-carousel').append('<li class="span2 span_custom"><a href="' + res.url + '" rel="' + res.id + '">' + res.image_html + '<h5 rel="' + res.subgroups + '" style="text-align:center">' + res.title + '</h5></a></li>');
                });

                $('#myNewArticlegroups #product-bar-carousel a').click(function () {
                    var group = $(this).find('h5').attr('rel');
                    var id = $(this).attr('rel');
                    if (group == 'true') {
                        showstepnew2(id);
                    } else {
                        showstepnew3(1, id);
                    }
                    return false;
                });


                return;
            }

        }
    });
}

function showstep2(uuid, group_id) {
    $('#myArticlegroups').modal('hide');
    $('#myArticlegroups2').modal('show');

    $.ajax({
        type:"POST",
        url:'/article/get' + motive_modal_mode + 'articlegroups/format/json',
        data:{'id':group_id},
        success:function (data) {

            if (data.success) {
                $('#myArticlegroups2 #product-bar-carousel').replaceWith('<ul id="product-bar-carousel" class="thumbnails"></ul>');
                $.each(data.result, function (i, res) {
                    $('#myArticlegroups2 #product-bar-carousel').append('<li class="span2 span_custom"><a href="' + res.url + '" rel="' + res.id + '">' + res.image_html + '<h5 style="text-align:center">' + res.title + '</h5></a></li>');
                });

                $('#myArticlegroups2 #product-bar-carousel a').click(function () {
                    var group = $(this).find('.product-bar-img-holder').attr('rel');
                    var id = $(this).attr('rel');
                    showstep3(uuid, 2, id, group_id);
                    return false;
                });

            }


        }
    });

    $('#myArticlegroups2 #back').click(function () {
        $('#myArticlegroups2').modal('hide');
        showstep1(uuid);
    });
}

function showstepnew2(group_id) {
    $('#myNewArticlegroups').modal('hide');
    $('#myNewArticlegroups2').modal('show');

    $.ajax({
        type:"POST",
        url:'/article/get' + motive_modal_mode + 'articlegroups/format/json',
        data:{'id':group_id},
        success:function (data) {

            if (data.success) {
                $('#myNewArticlegroups2 #product-bar-carousel').replaceWith('<ul id="product-bar-carousel" class="thumbnails"></ul>');
                $.each(data.result, function (i, res) {
                    $('#myNewArticlegroups2 #product-bar-carousel').append('<li class="span2 span_custom"><a href="' + res.url + '" rel="' + res.id + '">' + res.image_html + '<h5 style="text-align:center">' + res.title + '</h5></a></li>');
                });

                $('#myNewArticlegroups2 #product-bar-carousel a').click(function () {
                    var group = $(this).find('.product-bar-img-holder').attr('rel');
                    var id = $(this).attr('rel');
                    showstepnew3(2, id, group_id);
                    return false;
                });

            }


        }
    });

    $('#myNewArticlegroups2 #back').click(function () {
        $('#myNewArticlegroups2').modal('hide');
        showstepnew1(uuid);
    });
}

function showstep3(uuid, step, id, group_id) {
    $('#myArticlegroups').modal('hide');
    $('#myArticlegroups2').modal('hide');
    $('#myProducts').modal('show');
    var url = '';
    $.ajax({
        type:"POST",
        url:'/article/get' + motive_modal_mode + 'article/format/json',
        data:{'id':id},
        success:function (data) {

            if (data.success) {
                $('#myProducts #product-bar-carousel').replaceWith('<ul id="product-bar-carousel" class="thumbnails"></ul>');
                $.each(data.result, function (i, res) {
                    $('#myProducts #product-bar-carousel').append('<li class="span2 span_custom"><a href="' + res.url + '" rel="' + res.uuid + '" value="' + res.title + '">' + res.image_html + '<h5 style="text-align:center">' + res.title + '</h5></a></li>');
                });

                $('#myProducts #product-bar-carousel a').unbind();
                $('#myProducts #product-bar-carousel a').click(function () {
                    $('#myProducts #product-bar-carousel a h5').removeClass('active');
                    $(this).find('h5').addClass('active');
                    $('#myProducts .motivoverlay-headline-schritt').html('Ihre Auswahl: ' + $(this).attr('value'));
                    var uu = $(this).attr('rel');
                    $('#myProducts #designerbutton-gestalten').attr('href', '/layouter/show/' + uu + '//1?copy=3&autofill=1');
                    $.ajax({
                        type:"POST",
                        url:'/motiv/addautofillmotiv/format/json?motiv_uuid=' + uuid + '&article_uuid=' + uu,
                        data:{},
                        success:function (data) {
                            $('#myProducts #designerbutton-gestalten').removeClass('disabled');
                            return false;
                        }
                    });
                    return false;

                });

            }
        }});


    if (step == 1) {
        $('#myProducts #back').click(function () {
            $('#myProducts').modal('hide');
            showstep1(uuid);
        });
    }
    if (step == 2) {
        $('#myProducts #back').click(function () {
            $('#myProducts').modal('hide');
            showstep2(uuid, group_id);
        });
    }
}

function showstepnew3(step, id, group_id) {
    $('#myNewArticlegroups').modal('hide');
    $('#myNewArticlegroups2').modal('hide');
    $('#myNewProducts').modal('show');
    var url = '';
    $.ajax({
        type:"POST",
        url:'/article/get' + motive_modal_mode + 'article/format/json',
        data:{'id':id},
        success:function (data) {

            if (data.success) {
                $('#myNewProducts #product-bar-carousel').replaceWith('<ul id="product-bar-carousel" class="thumbnails"></ul>');
                $.each(data.result, function (i, res) {
                    $('#myNewProducts #product-bar-carousel').append('<li class="span2 span_custom"><a href="' + res.url + '" rel="' + res.uuid + '" value="' + res.title + '">' + res.image_html + '<h5 style="text-align:center">' + res.title + '</h5></a></li>');
                });

                $('#myNewProducts #product-bar-carousel a').unbind();
                $('#myNewProducts #product-bar-carousel a').click(function () {
                    $('#myNewProducts #product-bar-carousel a h5').removeClass('active');
                    $(this).find('h5').addClass('active');
                    $('#myNewProducts .motivoverlay-headline-schritt').html('Ihre Auswahl: ' + $(this).attr('value'));
                    var uu = $(this).attr('rel');
                    $('#myNewProducts #designerbutton-gestalten').attr('href', '/layouter/show/' + uu + '//1?copy=3&autofill=1');

                    $('#myNewProducts #designerbutton-gestalten').removeClass('disabled');

                    return false;

                });

            }
        }});


    if (step == 1) {
        $('#myNewProducts #back').click(function () {
            $('#myNewProducts').modal('hide');
            showstepnew1();
        });
    }
    if (step == 2) {
        $('#myNewProducts #back').click(function () {
            $('#myNewProducts').modal('hide');
            showstepnew2(group_id);
        });
    }
}

var upload_open_from = false;

$(function () {

    $('.tellafriend-open').click(function () {
        $('#tellafriend').modal();
    });


    $('.new_product').click(function () {
        showstepnew1();
        return false;
    });

    $('ul.dropdown-menu [data-toggle=dropdown]').on('click', function(event) {
        event.preventDefault();
        event.stopPropagation();
        $(this).parent().siblings().removeClass('open');
        $(this).parent().toggleClass('open');
    });

    $('.showmotivwizard').click(function () {

        return false;
	    
        uuid = $(this).attr('rel');
        var motiv_title = '';

        $('#myMotiv').modal('show');

        $.ajax({
            type:"POST",
            url:'/motiv/getmotiv/format/json',
            data:{'id':uuid, 'mode':motive_modal_bigimage},
            success:function (data) {

                if (data.success) {
                    $('.overlay-motiv-left-image').html(data.image_html);
                    $('.motivoverlay-headline').html(data.title);
                    motiv_title = data.title;
                    $('.motivoverlay-text-author').html('by ' + data.author);
                    $('.motivoverlay-text-info').html(data.info);
                    $('.motivoverlay-preis-preis').html(data.price_html);

                    if (data.myfav) {
                        $('#motiv-merken').html('Motiv nicht mehr merken');

                        $('#motiv-merken').unbind();
                        $('#motiv-merken').click(function () {

                            $.ajax({
                                type:"POST",
                                url:'/motiv/delfav/' + uuid,
                                data:{},
                                success:function (data) {
                                    $('.favcount').html(parseInt($('.favcount').html()) - 1);
                                    $('#myMotiv').modal('hide');
                                    document.location.reload();
                                }
                            });
                            //$.achtung({className: "achtungSuccess", message: "Das Motiv wurde aus Ihrer Merklisten entfernt!", timeout: 0});


                        });


                    } else {
                        $('#motiv-merken').html('Motiv merken');
                        $('#motiv-merken').unbind();
                        $('#motiv-merken').click(function () {

                            $.ajax({
                                type:"POST",
                                url:'/motiv/addfav/' + uuid,
                                data:{},
                                success:function (data) {
                                    $('.favcount').html(parseInt($('.favcount').html()) + 1);
                                }
                            });
                            //$.achtung({className: "achtungSuccess", message: "Das Motiv wurde Ihrer Merklistenzugefügt! Aus Ihrer Merkliste können Sie später im Online-Designer Ihre Motive wählen. Eine Übersicht der Merkliste finden Sie oben rechts.", timeout: 0});
                            $('#myMotiv').modal('hide');


                        });
                    }
                    if (data.mymotiv) {
                        $('a.mymotiv-settings').html('Eigenschaften bearbeiten');
                        $('a.mymotiv-settings').show();
                        $('a.mymotiv-settings').click(function () {
                            window.open('/admin/motiv/edit?sid=' + data.shop_id + '&uid=' + uuid, 'motivWindow', 'width=800,height=600,top=10,left=10,directories=no,toolbar=no,location=no,menubar=no,scrollbars=no,status=no,resizable=yes,dependent=no');
                            return false;
                        });
                        $('a.mymotiv-delete').html('Motiv Löschen');
                        $('a.mymotiv-delete').show();
                        $('a.mymotiv-delete').click(function () {
                            document.location.href = "/motiv/delete/" + uuid;
                            return false;
                        });
                    } else {
                        $('a.mymotiv-settings, a.mymotiv-delete').html('');
                        $('a.mymotiv-settings, a.mymotiv-delete').hide();
                        $('a.mymotiv-settings, a.mymotiv-delete').unbind();
                    }

                    return;
                }

            }
        });

    });

    $('a[rel="#myUploadMode"]').unbind();
    $('a[rel="#myUploadMode"]').click(function () {
        upload_open_from = 1;
        if ($('#myUpload table tr').length == 1) {
            var id = $('#myUpload table tr a.btn').attr('id');
            if(id == 'tp_fullscreen') {
                document.location.href = $('#myUpload table tr a.btn').attr('href');
            }else{
                $('#myUpload table tr a.btn').click();
            }
        } else {
            $('#myUpload').modal();
        }
        return false;
    });

    $('a[rel="#steplayoutermail"]').unbind();
    $('a[rel="#mail"]').click(function () {
        $('#upload_mode').val('mail');
        $('#CALCFORM').submit();
        $('#myUpload').modal('hide');
        $('#myUploadMail').modal();

    });

    $('a[rel="#provided"]').unbind();
    $('a[rel="#provided"]').click(function () {
        $('#upload_mode').val('provided');
        $('#CALCFORM').submit();
        $('#myUpload').modal('hide');
    });
    
    $('a[rel="#myUploadCenter"]').unbind();
    $('a[rel="#myUploadCenter"]').click(function () {
        $('#upload_mode').val('center');
        $('#CALCFORM').submit();
        $('#myUpload').modal('hide');
        $('#buyform').submit();
    });

    $('a[rel="#post"]').unbind();
    $('a[rel="#post"]').click(function () {
        $('#upload_mode').val('post');
        $('#CALCFORM').submit();
        $('#myUpload').modal('hide');
        $('#myUploadPost').modal();
    });

    $('a[rel="#fromLatestOrder"]').unbind();
    $('a[rel="#fromLatestOrder"]').click(function () {
        $('#upload_mode').val('fromLatestOrder');
        $.getJSON('/article/copylatestuploadfromordertosession/format/json?id=' + article_id, function(item_image) {
            $('#CALCFORM').submit();
        });

        $('#myUpload').modal('hide');
    });

    $('a[rel="overlaydesigner"]').unbind();
    $('a[rel="overlaydesigner"]').click(function () {

        $.ajax({
            type:"POST",
            url:'/article/getnewdesignarticles/format/json',
            data:{},
            success:function (data) {

                if (data.products > 1) {
                    showstep1(uuid);
                } else {
                    $.ajax({
                        type:"POST",
                        url:'/motiv/addautofillmotiv/format/json?motiv_uuid=' + uuid + '&article_uuid=' + data.product,
                        data:{},
                        success:function (datas) {

                            var href = '/layouter/show/' + data.product + '//1?copy=3&autofill=1';

                            var width = $(window).width() / 100 * 90;
                            var height = $(window).height() / 100 * 90;

                            $("#frame").attr('src', href);

                            $('body').data('fullscreen', 2);

                            $('#layouter_frame').css({width:width + 'px', height:height + 'px', top:($(window).height() / 2 - (height / 2)), left:($(window).width() / 2 - (width / 2))}).fadeIn();
                            $('#overlay_frame').css({width:$(window).width(), height:$(window).height()}).show();
                            scroll(0, 0);
                            return false;
                        }
                    })

                }

            }
        });

        return false;
    });


    $('#myshop_selector').change(function () {
        document.location.href = "/myshop/switchshop/" + $(this).val();
    });

    $('.myshop #products, .myshop #productgroups, .myshop #theme, .myshop #market_theme, .myshop #articlegroup').multiSelect();

    $("a[rel='upload_form']").click(function () {
        $('#myUpload').modal('hide');
        $('#myUploadWizard').modal();
        showUpload();
    });

    $('.closeMyUploadWizard').click(function () {

        $('#upload_mode').val('none');
        $('#myUploadWizard').modal('hide');
    });

    $("a[rel='#overlay']").click(function () {

        $('#upload_mode').val('online');
        var href = $(this).attr("href");

        var width = $(window).width() / 100 * 90;
        var height = $(window).height() / 100 * 90;

        $("#frame").attr('src', href);

        $('body').data('fullscreen', 2);

        $('#layouter_frame').css({width:width + 'px', height:height + 'px', top:($(window).height() / 2 - (height / 2)), left:($(window).width() / 2 - (width / 2))}).fadeIn();
        $('#overlay_frame').css({width:$(window).width(), height:$(window).height()}).show();
        scroll(0, 0);
        return false;
    });

    bindTemplatebrindButtons();

    if($('.pop_over').length) {
        $('.pop_over').popover();
    }


    $('.send-tell').click(function () {
        var error = false;
        if ($('#yourname').val() == "") {
            $('#yourname').parent().addClass('error');
            error = true;
        } else {
            $('#yourname').parent().removeClass('error');
        }

        if ($('#youremail').val() == "") {
            $('#youremail').parent().addClass('error');
            error = true;
        } else {
            $('#youremail').parent().removeClass('error');
        }

        if ($('#friendname').val() == "") {
            $('#friendname').parent().addClass('error');
            error = true;
        } else {
            $('#friendname').parent().removeClass('error');
        }

        if ($('#friendemail').val() == "") {
            $('#friendemail').parent().addClass('error');
            error = true;
        } else {
            $('#friendemail').parent().removeClass('error');
        }

        if ($('#yourmessage').val() == "") {
            $('#yourmessage').parent().addClass('error');
            error = true;
        } else {
            $('#yourmessage').parent().removeClass('error');
        }

        if ($('#cp-input').val() == "") {
            $('#cp-input').parent().parent().addClass('error');
            error = true;
        } else {
            $('#cp-input').parent().parent().removeClass('error');
        }
        if (!error) {

            var data = $("#tellafriendForm").serialize();
            data = data + "&url=" + document.location.href + "&article_id=" + $("#ajax_calc_id").val();

            $.ajax({
                type:"POST",
                url:"/index/sendtellafriend/format/json",
                data:data,
                success:function (content) {
                    eval('content = ' + content + ';');
                    if (content.success) {
                        $("#captcha").html(content.captcha);
                        $("#tellafriend").modal('toggle');
                        $("#captcha").removeClass('errors');
                        //$.achtung({className: "achtungSuccess", message: "Mail gesendet", timeout: 0});
                    } else {
                        $("#captcha").html(content.captcha);
                        $("#captcha").addClass('errors');
                    }
                }
            });
        }
        return false;
    });
    $(document).off('focusin.modal');
});



function backwin() {
    $('#site-wrapper').animate({opacity:1});
    $('#layouter_frame').fadeOut();
    $("#frame").attr('src', "");
    $("body").data('fullscreen', 3);
    $('#overlay_frame').hide();

}

function toggleScreen(fullscreen) {
    if (fullscreen == 'true') {

        var frame = $("#frame")[0].contentDocument;
        $("#my_flash", frame).width($(window).width());
        $("#my_flash", frame).height($(window).height());

        $('#layouter_frame').css({width:$(window).width(), height:$(window).height(), top:0, left:0});

        $('body').data('fullscreen', 1);

    }
    if (fullscreen == 'false') {

        var width = $(window).width() / 100 * 90;
        var height = $(window).height() / 100 * 90;

        $('#layouter_frame').css({width:width, height:height, top:($(window).height() / 2 - (height / 2)), left:($(window).width() / 2 - (width / 2))});

        var frame = $("#frame")[0].contentDocument;
        $("#my_flash", frame).width(width);
        $("#my_flash", frame).height(height);

        $('body').data('fullscreen', 2);
    }
    $(window).resize();
}

$(window).resize(function () {

    if ($('body').data('fullscreen') == 1) {
        var frame = $("#frame")[0].contentDocument;
        $("#my_flash", frame).width($(window).width());
        $("#my_flash", frame).height($(window).height());

        $('#layouter_frame').css({width:$(window).width(), height:$(window).height()});
        $('#overlay_frame').css({width:$(window).width(), height:$(window).height()});
    }
    if ($('body').data('fullscreen') == 2) {

        var width = $(window).width() / 100 * 90;
        var height = $(window).height() / 100 * 90;

        var frame = $("#frame")[0].contentDocument;
        $("#my_flash", frame).width(width);
        $("#my_flash", frame).height(height);

        $('#layouter_frame').css({width:width, height:height, top:($(window).height() / 2 - (height / 2)), left:($(window).width() / 2 - (width / 2))});
        $('#overlay_frame').css({width:$(window).width(), height:$(window).height()});
    }
    if ($('body').data('fullscreen') == 3) {

        var width = 880;
        var height = 650;

        var frame = $("#frame")[0].contentDocument;
        $("#my_flash", frame).width(width);
        $("#my_flash", frame).height(height);

        $('#layouter_frame').css({width:width, height:height, top:($(window).height() / 2 - (height / 2)), left:($(window).width() / 2 - (width / 2))});
        $('#overlay_frame').css({width:$(window).width(), height:$(window).height()});
    }
});