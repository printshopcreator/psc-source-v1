Function.prototype.Timer = function (interval, calls, onend) {
    var count = 0;
    var payloadFunction = this;
    var startTime = new Date();
    var callbackFunction = function () {
        return payloadFunction(startTime, count);
    };
    var endFunction = function () {
        if (onend) {
            onend(startTime, count, calls);
        }
    };
    var timerFunction =  function () {
        count++;
        if (count < calls && callbackFunction() != false) {
            window.setTimeout(timerFunction, interval);
        } else {
            endFunction();
        }
    };
    timerFunction();
};

function leadingzero (number) {
    return (number < 10) ? '0' + number : number;
}
function countdown (seconds, target) {
    var element = $('#generate_orders');
    var calculateAndShow = function () {
        if (seconds > 0) {
            seconds--;
        } else {
            return false;
        }
    };
    var completed = function () {
        element.html("<strong>Redirect ...<\/strong>");
        document.location.href="/basket"
    };
    calculateAndShow.Timer(1000, Infinity, completed);
}

var selectedContacts = [];

function generate(contacts) {

    if(contacts.length == 0) {
        return false;
    }

    var value = _.first(contacts);

    $('#collecting_orders_data').val(value);
    $.ajax({
        type: "POST",
        url: $("#buyform").attr("action"),
        data: $("#buyform").serialize(), // serializes the form's elements.
        success: function (data) {
            $.ajax({
                type: "POST",
                url: "/tp.php",
                data: {
                    genUrl: data.genUrl
                }, // serializes the form's elements.
                success: function (data) {
                    contacts = _.without(contacts, value);

                    $('.modal-footer').html("!!!BITTE WARTEN ES WERDEN NOCH " + contacts.length + " GENERIERT !!!");
                    if (contacts.length == 0) {
                        document.location.href = "/basket";
                        $('#myCollectingOrders').modal('toggle');
                    }else{
                        generate(contacts);
                    }
                }

            });
        }

    });
}


$(function(){
    $('a[rel="#collecting_orders"]').unbind();
    $('a[rel="#collecting_orders"]').click(function(){
        $('#upload_mode').val('templateprint');
        $('#myUpload').modal('hide');
        $('#myCollectingOrders').modal('toggle');
        $(document).off('focusin.modal');
        loadContacts();

        return false;

    });

    $('a[rel="#addContact"]').unbind();
    $('a[rel="#addContact"]').click(function(){
        $.post(
            '/user/contactaddempty/format/json',
            {
                account_id:$('#addContactAccount').val()
            },
            function(data) {
                $('#addContactAccount').prop("selectedIndex",0);
                if (data.success) {
                    loadContacts(data.contact_uuid);
                }
            }
        );
        return false;
    });

    $.post(
        '/user/accountgetall/format/json',
        {},
        function(data) {
            if (data.success) {
                accountOwn = data.myAccount;
                $.each(data.accounts, function(key, item) {
                    if(accountFilter == 1) {
                        $('#addContactAccount').append(
                            $('<option></option>').val(item.id).html(item.label + " " + item.appendix)
                        );
                    }
                    if(accountFilter == 2 && item.id == data.myAccount) {
                        $('#addContactAccount').append(
                            $('<option></option>').val(item.id).html(item.label + " " + item.appendix)
                        );
                    }
                    if(accountFilter == 3 && item.id == accountId) {
                        $('#addContactAccount').append(
                            $('<option></option>').val(item.id).html(item.label + " " + item.appendix)
                        );
                    }
                });
            }
        }
    );


    $('#saveContact').click(function() {
        $(document).off('focusin.modal');
        $.post(
            '/user/contactadd/format/json',
            $("#addContact form").serialize(),
            function(data){
                if(data.success) {

                    $('#addContact').modal('toggle');
                    loadContacts();
                }else{
                    $.each(data.messages, function(index, item) {
                        $.each(item, function(indexx, items) {
                            $('#addContact form #' + index + '-' + indexx).css('border', '1px solid red');
                        })
                    });
                }
            });

        return false;
    });

    $('#generate_orders').click(function() {

        var contactsToGen = [];

        $('.modal-footer').html("!!!BITTE WARTEN ES WERDEN NOCH " + $("#collecting_data_form input[name='contact_uuid[]']").length + " GENERIERT !!!");
        $.each($("#collecting_data_form input[name='contact_uuid[]']"), function() {
            contactsToGen.push($(this).val());
        });

        generate(contactsToGen);

        return false;
    });



    $('#myCollectingOrders .searchBtn').unbind();
    $('#myCollectingOrders .searchBtn').click(function() {
        $('#myCollectingOrders #myTab').find('li a').first().click();
        loadContacts();

    });

});

function addContact(uuid) {
    selectedContacts.push(uuid);

    loadContacts();
    loadSelectedContacts();

    return false;
}

function removeContact(uuid) {
    selectedContacts = _.without(selectedContacts, _.findWhere(selectedContacts, uuid));

    loadContacts();
    loadSelectedContacts();

    return false;
}


function loadSelectedContacts() {
    $('.selectContactsCount').html(selectedContacts.length);

    $.ajax({
        type: "POST",
        url: '/article/getcollectingcontacts/format/json',
        data: {
            'selectMode': 1,
            'selectedContacts': selectedContacts,
        },
        success: function(data) {
            var templateTop = '<tr><td class="collectingCellTop">'
                + '<input type="hidden" name="contact_uuid[]" value="<%= uuid %>"/><button type="button" onclick="removeContact(\'<%= uuid %>\')" class="btn btn-danger" style="background-color: #d9534f;border-color: #d43f3a;">-</button>'
                + '</td>'
                + '<td class="collectingCellTop"><p><small class="id">(<%= id %>)</small> <small class="firstname"><%= self_firstname %></small> <small class="lastname"><%= self_lastname %></small></p></td>'
                + '<td class="collectingCellTop"><p><small class="firma"><%= self_department %></small></p></td>'
                + '<td class="collectingCellTop"><p><small class="street"><%= self_street %> <%= self_house_number %></small> <small class="city"><%= self_zip %> <%= self_city %></small></p></td>';

            if(collectingOrdersChangePicture) {
                templateTop += '<td class="collectingCellTop"><input type="file" name="logo1" class="file_upload_templateprint" style="display:none"/><a data-uuid="<%= uuid %>" rel="#templateprint-upload-picture" class="templateprint-upload-picture btn btn-small btn-success"><i class="fa fa-picture-o"></i></a></td>';
            }else{
                templateTop += '<td class="collectingCellTop"></td>';
            }

            if(collectingOrdersCopy) {
                templateTop += '<td class="collectingCellTop"><button data-uuid="<%= uuid %>" class="collectionOrderCopy btn btn-small btn-success"><i class="fa fa-files-o"></i></button></td>'
            }else{
                templateTop += '<td class="collectingCellTop"></td></tr>';
            }

            templateBottom = '<tr><td class="collectingCellBottom"></td><td class="collectingCellBottom"><p><small class="mail"><%= self_email %></small></p></td>'
                + '<td class="collectingCellBottom"><p><small class="firma"><%= accountCompany %></small></p></td>'
                + '<td class="collectingCellBottom"><p><small class="street"><%= accountStreet %> <%= accountHouseNumber %></small> <small class="city"><%= accountZip %> <%= accountCity %></small></p></td>';

            if(collectingOrdersInviteContact) {
                templateBottom += '<td class="collectingCellBottom"><button data-email="<%= self_email %>" data-uuid="<%= uuid %>" class="collectionOrderInvite btn btn-small btn-success"><i class="fa fa-paper-plane"></i></button></td>';
            }else{
                templateBottom += '<td class="collectingCellBottom"></td>';
            }

            templateBottom += '<td class="collectingCellBottom"><a rel="#overlaytemp" class="btn btn-small btn-success" href="' + previewEditUrl + '&w2puserid=<%= uuid %>"><i class="fa fa-pencil"></i></a></td></tr>';

            templateTop = _.template(templateTop);
            templateBottom = _.template(templateBottom);

            $('#collecting_data_selected tbody').empty();

            $.each(data.result, function(index, item) {
                $('#collecting_data_selected tbody').append(templateTop(item));
                $('#collecting_data_selected tbody').append(templateBottom(item));
            });

            bindTemplatebrindButtons();

            bindButtons();

            return false;
        }
    })
}

function loadContacts(uuid) {

    var search = $('#myCollectingOrders .search').val();
    var accountSearch = accountId;
    if(uuid) {
        search = uuid;
        accountSearch = '';
    }
    if(accountFilter == 1) {
        accountSearch = '';
    }
    if(accountFilter == 2) {
        accountSearch = accountOwn;
    }
    $.ajax({
        type: "POST",
        url: '/article/getcollectingcontacts/format/json',
        data: {
            'search': search,
            'account_id': accountSearch
        },
        success: function(data) {

            var templateTop = '<tr><td class="collectingCellTop">'

                +  '<% if ($.inArray(uuid, selectedContacts) === -1) { %><button type="button" class="btn btn-success" onclick="addContact(\'<%= uuid %>\')">+</button><% }else{ %><button type="button" onclick="removeContact(\'<%= uuid %>\')" class="btn btn-danger" style="background-color: #d9534f;border-color: #d43f3a;">-</button><% } %>'
                + '</td>'
                + '<td class="collectingCellTop"><p><small class="id">(<%= id %>)</small> <small class="firstname"><%= self_firstname %></small> <small class="lastname"><%= self_lastname %></small><br /><small class="mail"><%= self_email %></small></p></td>'
                + '<td class="collectingCellTop" style="border-bottom: 1px dashed;"><p><small class="firma"><%= self_department %></small></p>'
                + '<p><small class="street"><%= self_street %> <%= self_house_number %></small><br /><small class="city"><%= self_zip %> <%= self_city %></small></p></td>';
            if(collectingOrdersChangePicture) {
                templateTop += '<td class="collectingCellTop"><input type="file" name="logo1" class="file_upload_templateprint" style="display:none"/><a data-uuid="<%= uuid %>" title="Bild tauschen/hochladen" rel="#templateprint-upload-picture" class="templateprint-upload-picture btn btn-small btn-success"><i class="fa fa-picture-o"></i></a></td>';
            }else{
                templateTop += '<td class="collectingCellTop"></td>';
            }

            
                templateTop += '<td class="collectingCellTop">'; if(collectingOrdersCopy) { templateTop += '<button data-uuid="<%= uuid %>" title="Kopie erstellen" class="collectionOrderCopy btn btn-small btn-success" style="margin-bottom: 10px;"><i class="fa fa-files-o"></i></button><br />'; }else{ templateTop += ''; } if(collectingOrdersInviteContact) { templateTop += '<button title="Einladungsemail senden" data-email="<%= self_email %>" data-uuid="<%= uuid %>" class="collectionOrderInvite btn btn-small btn-success" style="margin-bottom: 10px;"><i class="fa fa-paper-plane"></i></button><br />'; }else{ templateTop += ''; } templateTop += '<a rel="#overlaytemp" title="Bearbeiten" class="btn btn-small btn-success" href="' + previewEditUrl + '&w2puserid=<%= uuid %>"><i class="fa fa-pencil"></i></a></td>'


            templateBottom = '<tr><td class="collectingCellBottom"></td><td class="collectingCellBottom"><p></p></td>'
                + '<td class="collectingCellBottom"><p><small class="firma"><%= accountCompany %></small></p>'
                + '<p><small class="street"><%= accountStreet %> <%= accountHouseNumber %></small><br /><small class="city"><%= accountZip %> <%= accountCity %></small></p></td>';

            templateBottom += '<td class="collectingCellBottom"></td></tr>';

            templateTop = _.template(templateTop);
            templateBottom = _.template(templateBottom);

            $('#collecting_data tbody').empty();

            $.each(data.result, function(index, item) {
                $('#collecting_data tbody').append(templateTop(item));
                $('#collecting_data tbody').append(templateBottom(item));
            });

            bindTemplatebrindButtons();

            bindButtons();

            return false;
        }
    })
}

function bindButtons() {

    $('.collectionOrderInvite').unbind();
    $('.collectionOrderInvite').click(function () {

        $('#inviteContact #inputEmail').val($(this).attr('data-email'));
        $('#inviteContact #inputUuid').val($(this).attr('data-uuid'));

        $('#inviteContact').modal('show');

    });

    $('#inviteContactButton').unbind();
    $('#inviteContactButton').click(function () {
        $.post(
            '/user/contactinvite/format/json',
            {
                uuid: $('#inviteContact #inputUuid').val(),
                email: $('#inviteContact #inputEmail').val()
            },
            function (data) {
                $('#inviteContact').modal('hide');
                alert("Einladung gesendet");
                loadContacts();
                loadSelectedContacts();
            }
        );
    });

    $('.collectionOrderCopy').unbind();
    $('.collectionOrderCopy').click(function () {

        var uuid = $(this).attr('data-uuid');

        $.post(
            '/user/contactcopy/format/json',
            {
                uuid: uuid
            },
            function (data) {
                loadContacts(data.contact_uuid);
            }
        );

    });

}