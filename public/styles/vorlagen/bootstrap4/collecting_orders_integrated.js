Function.prototype.Timer = function (interval, calls, onend) {
    var count = 0;
    var payloadFunction = this;
    var startTime = new Date();
    var callbackFunction = function () {
        return payloadFunction(startTime, count);
    };
    var endFunction = function () {
        if (onend) {
            onend(startTime, count, calls);
        }
    };
    var timerFunction =  function () {
        count++;
        if (count < calls && callbackFunction() != false) {
            window.setTimeout(timerFunction, interval);
        } else {
            endFunction();
        }
    };
    timerFunction();
};

function leadingzero (number) {
    return (number < 10) ? '0' + number : number;
}
function countdown (seconds, target) {
    var element = $('#generate_orders');
    var calculateAndShow = function () {
        if (seconds > 0) {
            seconds--;
        } else {
            return false;
        }
    };
    var completed = function () {
        element.html("<strong>Redirect ...<\/strong>");
        document.location.href="/basket"
    };
    calculateAndShow.Timer(1000, Infinity, completed);
}

$(function() {

    loadContacts();

    $('#myCollectingOrders .searchBtn').unbind();
    $('#myCollectingOrders .searchBtn').click(function() {
        loadContacts();
    });

    $('a[rel="#addContact"]').unbind();
    $('a[rel="#addContact"]').click(function(){
        $.post(
            '/user/contactaddempty/format/json',
            {
                account_id:$('#addContactAccount').val()
            },
            function(data) {
                $('#addContactAccount').prop("selectedIndex",0);
                if (data.success) {
                    loadContacts(data.contact_uuid);
                }
            }
        );
        return false;
    });

    $.post(
        '/user/accountgetall/format/json',
        {},
        function(data) {
            if (data.success) {
                accountOwn = data.myAccount;
                $.each(data.accounts, function(key, item) {
                    if(accountFilter == 1) {
                        $('#addContactAccount').append(
                            $('<option></option>').val(item.id).html(item.label + " " + item.appendix)
                        );
                    }
                    if((accountFilter == 2 || accountFilter == 5) && item.id == data.myAccount) {
                        $('#addContactAccount').append(
                            $('<option></option>').val(item.id).html(item.label + " " + item.appendix)
                        );
                    }
                    if((accountFilter == 3 || accountFilter == 4) && item.id == accountId) {
                        $('#addContactAccount').append(
                            $('<option></option>').val(item.id).html(item.label + " " + item.appendix)
                        );
                    }
                });
            }
        }
    );

});

function bindButtons() {
    $('button.collectionCalcGenerate').unbind();
    $('button.collectionCalcGenerate').click(function(){
        generate(this);
        return false;
    });
}

function generate(elm) {

    elm = $(elm);
    var elmAuflage = elm.parent().find('.auflage');
    var auflage = elmAuflage.val();

    if(auflage > 0) {

        $('#auflage').val(auflage);

        var uuid = elm.data('uuid');
        $.post(
            '/article/calc/' + $('#ajax_calc_id').val() + '/format/json',
            $("#CALCFORM").serialize(),
        function(data){

            $('#collecting_orders_data').val(uuid);
            $.ajax({
                type: "POST",
                url: $("#buyform").attr("action"),
                data: $("#buyform").serialize(), // serializes the form's elements.
                success: function (data) {
                    $.ajax({
                        type: "POST",
                        url: "/tp.php",
                        data: {
                            genUrl: data.genUrl
                        }, // serializes the form's elements.
                        success: function (data) {
                            $('.flashmessages').append('<div class="alert alert-block alert-success fade in">' +
                                '<button type="button" class="close" data-dismiss="alert">×</button>' +
                                '<p>Im Warenkorb</p></div>');
                        }

                    });
                }

            });
        });
    }
}

function loadContacts(uuid) {

    $('.messageCollection').html('<span class="alert alert-block alert-success">Lade ...</span>');

    var search = $('#myCollectingOrders .search').val();


    var accountSearch = accountId;
    var accountSub = false;
    if(uuid) {
        search = uuid;
        accountSearch = '';
    }
    if(accountFilter == 1) {
        accountSearch = '';
    }
    if(accountFilter == 2) {
        accountSearch = accountOwn;
    }
    if(accountFilter == 4) {
        accountSub = true;
    }
    if(accountFilter == 5) {
        accountSearch = accountOwn;
        accountSub = true;
    }
    $.ajax({
        type: "POST",
        url: '/article/getcollectingcontacts/format/json',
        data: {
            'search': search,
            'account_id': accountSearch,
            'account_sub': accountSub
        },
        success: function(data) {

            var templateTop = '<tr class="top"><td class="collectingCellTop">'

                + '</td>'
                + '<td class="collectingCellTop"><p><small class="id">(<%= id %>)</small> <small class="firstname"><%= self_firstname %></small> <small class="lastname"><%= self_lastname %></small></p></td>'
                + '<td class="collectingCellTop"><p><small class="firma"><%= self_department %></small></p></td>'
                + '<td class="collectingCellTop"><p><small class="street"><%= self_street %> <%= self_house_number %></small> <small class="city"><%= self_zip %> <%= self_city %></small></p></td>';
            if(collectingOrdersChangePicture) {
                templateTop += '<td class="collectingCellTop"><input type="file" name="logo1" class="file_upload_templateprint" style="display:none"/><a data-uuid="<%= uuid %>" title="Bild tauschen/hochladen" rel="#templateprint-upload-picture" class="templateprint-upload-picture btn btn-small btn-success"><i class="fa fa-picture-o"></i></a></td>';
            }else{
                templateTop += '<td class="collectingCellTop"></td>';
            }

            if(collectingOrdersCopy) {
                templateTop += '<td class="collectingCellTop"><button data-uuid="<%= uuid %>" title="Kopie erstellen" class="collectionOrderCopy btn btn-small btn-success"><i class="fa fa-files-o"></i></button></td>'
            }else{
                templateTop += '<td class="collectingCellTop"></td></tr>';
            }

            templateBottom = '<tr class="bottom"><td class="collectingCellBottom"></td><td class="collectingCellBottom"><p><small class="mail"><%= self_email %></small></p></td>'
                + '<td class="collectingCellBottom"><p><small class="firma"><%= accountCompany %></small></p></td>'
                + '<td class="collectingCellBottom"><p><small class="street"><%= accountStreet %> <%= accountHouseNumber %></small> <small class="city"><%= accountZip %> <%= accountCity %></small></p></td>';

            if(collectingOrdersInviteContact) {
                templateBottom += '<td class="collectingCellBottom"><button title="Einladungsemail senden" data-email="<%= self_email %>" data-uuid="<%= uuid %>" class="collectionOrderInvite btn btn-small btn-success"><i class="fa fa-paper-plane"></i></button></td>';
            }else{
                templateBottom += '<td class="collectingCellBottom"><form class="form-inline"><div class="form-group"><input type="text" class="form-control auflage" value="0"/></div><button class="btn btn-default collectionCalcGenerate" data-uuid="<%= uuid %>" type="button"><i class="fa fa-shopping-cart"></i></button></form></td>';
            }

            templateBottom += '<td class="collectingCellBottom"><a rel="#overlaytemp" title="Bearbeiten" class="btn btn-small btn-success" href="' + previewEditUrl + '&w2puserid=<%= uuid %>"><i class="fa fa-pencil"></i></a></td></tr>';

            templateTop = _.template(templateTop);
            templateBottom = _.template(templateBottom);

            $('#collecting_data tbody').empty();

            $.each(data.result, function(index, item) {
                $('#collecting_data tbody').append(templateTop(item));
                $('#collecting_data tbody').append(templateBottom(item));
            });

            bindTemplatebrindButtons();

            bindButtons();
            $('.messageCollection').html('');
            return false;
        }
    })
}


