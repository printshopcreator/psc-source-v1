$(function () {


    // Initialize the jQuery File Upload widget:
    $('.uploads').fileupload({
        singleFileUploads: true,
        autoUpload: true,
        limitMultiFileUploads: 1,
        done: function (e, data) {
            $('.template-upload').remove();
            finishPreflightCenter(data);

        },
        always: function(e,data) {
            if(data._response.result.error) {
                alert(data._response.result.message);
            }
        }
    });

    // Enable iframe cross-domain access via redirect option:
    $('.uploads').fileupload(
        'option',
        'redirect',
        window.location.href.replace(
            /\/[^\/]*$/,
            '/cors/result.html?%s'
        )
    );
    // Load existing files:
    $('.uploads').each(function () {
        var that = this;
        $.getJSON('/service/upload/getfilesfromorderspos?id=' + $(this).find('#pos_id').val() + '&type=' + $(this).find('#type').val() + '&format=json', function (result) {
            if (result.files && result.files) {
                $(that).fileupload('option', 'done')
                    .call(that, null, {result: result.files});
            }
        });
    });

    bindDeleteButtons();
});


function finishPreflightCenter(data) {
    $(data.result).each(function (key, row) {

        var uploadtemplate = tmpl("template-download");

        $('#uploads-' + row.id + '_' + row.type + ' .files').append(uploadtemplate({files:[row]}));

        displayPreflightCenter(row.id, row.key, row.type);
        bindDeleteButtons();
    });
}

function displayPreflightCenter(id, uuid, type) {
    $.ajax({
        type: "POST",
        url: '/preflight/checkpreflightcenter/format/json?uuid=' + uuid + '&type=' + id + '&type=' + type,
        data: $("#CALCFORM").serialize(),
        success: function(data) {
            if(data.success) {
                $('.preflight_' + uuid).html('<div class="alert alert-success">Upload erfolgreich</div>');
                return;
            }else{
                var mess = '';
                $.each(data.errors, function(i, error) {
                    mess = mess + '<div class="alert alert-error">' + error.message + "</div>";
                });
                mess = mess + "";
                $('.preflight_' + uuid).html(mess);

                $.ajax({
                    type: "GET",
                    url: '/service/upload/deletecenter?uuid=' + uuid,
                    success: function(data) {

                    }
                });
            }
            bindDeleteButtons();
        }
    });
}

function bindDeleteButtons() {
    $('.uploads .delete > button').unbind();
    $('.uploads .delete > button').click(function() {
        $.ajax({
            type: "GET",
            url: $(this).attr("data-url"),
            success: function(data) {

            }
        });
    })
}
