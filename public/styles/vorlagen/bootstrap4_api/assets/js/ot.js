/*
 * jQuery optionTree Plugin
 * version: 1.0.1
 * @requires jQuery v1.2 or later
 *
 * Dual licensed under the MIT and GPL licenses:
 *   http://www.opensource.org/licenses/mit-license.php
 *   http://www.gnu.org/licenses/gpl.html
 *
 * @version $Id: jquery.optionTree.js 3 2009-03-03 00:06:13Z kkotowicz $
 * @author  Krzysztof Kotowicz <kkotowicz at gmail dot com>
 */

/**
 * Converts passed JSON option tree into dynamically created <select> elements allowing you to
 * choose nested options.
 *
 * @param String tree options tree
 * @param array options additional options (optional)
 */
(function($){
$.fn.optionTree = function(tree, options) {

    options = $.extend({
        choose: 'Choose...',
        preselect: {},
        select_class: '',
        leaf_class: 'final',
        mode: 'themes',
        empty_value: '',
        container: '',
        select: '',
        field: '',
        shop: ''
    }, options || {});

    var cleanName = function (name) {
        return name.replace(/_*$/, '');
    };

    var removeNested = function (name) {
        $("[name^='"+ name + "']").remove();
    };
    
    var removeNestedId = function (name) {
        $("[id^='"+ name + "']").remove();
    };

    var setValue = function(name, value) {
        if(value != "" && value != 0) 
            $("input[name='" + cleanName(name) + "']").val(value).change();
    }

    return this.each(function() {
        
        var orgname = $(this).attr('name');
        var name = $(this).attr('name') + "_";
        
        if(options.clear) {
            removeNestedId(options.field + '__');
            $('#' + cleanName(options.field) + '_name').html('');
            $('#' + cleanName(options.field) + 'add').show();
            $('#' + cleanName(options.field) + 'edit').hide();
            $('#' + cleanName(options.field) + 'delete').hide();
            setValue(options.field, '');
            return;
        }
        
        if(this.value == 'new') {
            $('#dialog-form').data('mode', options.mode);
            if($(this).prev().val()) {
                $('#dialog-form').data('parent', $(this).prev().val());
            }else{
                $('#dialog-form').data('parent', 0);
            }
            $('#dialog-form').data('field', cleanName(options.field));
            $('#dialog-form').data('select', options.select);
            $('#dialog-form').modal();
            return;
        }else{
            setValue(options.field, this.value);
        }
        
        var orgfield = options.field;
        options.field = options.field + "_";

        // remove all dynamic options of lower levels
        removeNested(name);
        
        removeNestedId(options.field + '_');
        
        value_id = tree;
        
        $.ajax({url: "/resale/" + options.mode + "/format/json",
                dataType: 'json',
                data: {id: tree, shop: options.shop, depth: $(this).parent().find('.navigator').length},
                context: this,
                success: function(tree) {
            
             // many options exists for current nesting level
    
                // create select element with all the options
                // and bind onchange event to recursively call this function
                    if($(this).find(':selected').text() != "") {
                        $('#' + orgfield + 'name').html(' '+$(this).find(':selected').text());
                        $('#' + cleanName(options.field) + 'add').hide();
                        $('#' + cleanName(options.field) + 'edit').show();
                        $('#' + cleanName(options.field) + 'delete').show();
                    }
                    
                    
                    if(!$('#' + orgfield + '_name').length) {
                        $('#' + orgfield + 'name').after('<span id="' + orgfield + '_name"></span>');
                    }
                    
                    
                if(tree.rows.length > 0) {
                
                var $select = $('<select class="navigator">').attr('name',name).attr('size', 10)
                .change(function() {
                    if (this.options[this.selectedIndex].value != '') {
                            $(this).optionTree(this.value, options);
                    } else {
                           removeNested(name + '_');
                    }
                });

                if ($(this).is('input'))
                    $select.insertBefore(this);
                else
                    $select.insertAfter(this);
    
                if (options.select_class)
                    $select.addClass(options.select_class);
    
                $.each(tree.rows, function(k, v) {
                    var o = $("<option>").html(v.title)
                        .attr('value', v.id);
                    var clean = cleanName(name);
                        if (options.leaf_class && typeof v != 'object') // this option is a leaf node
                            o.addClass(options.leaf_class);
    
                        o.appendTo($select);
                        if (options.preselect && options.preselect[clean] && options.preselect[clean] == v) {
                            o.get(0).selected = true;
                            $select.change();
                        }
                });
                };
            if (!tree.sub) { // single option is selected by the user (function called via onchange event())
                setValue(options.field, value_id);
                setValue('addid', value_id);
                var $ok = $('<img src="/images/ok.jpg" width="100">').attr('name',name);
                
                if(tree.rows.length == 0) {
                    //$ok.insertAfter(this);
                }
                if(tree.rows.length > 0) {  
                    //$ok.insertAfter($select);
                }
                
                
                //$('#' + orgfield + 'name').html(' '+ $(this.context).find(':selected').text());
               
                
            }
        }});
    });

}
})(jQuery);
