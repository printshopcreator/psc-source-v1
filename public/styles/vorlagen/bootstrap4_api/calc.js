var calccount = 1;

var calcHasError = false;

$(function(){
    $("#in_basket").click(function(event) {
        
        if($('#upload_mode').val() == 'none' && $('#myUpload .modal-body > div').length > 0 ) {
            $('#myUpload').modal();
            return;
        }
        var formElements = $('#CALCFORM').serializeArray();
        var formArray = {};
        $.each(formElements, function( index, value ) {
            var name = value.name;
            var val = value.value;
            formArray[name] = val;
        });

        if(productLoaded === 1) {
            $.ajax({
                url: "/apps/api/basket/legacy/update",
                contentType: "application/json",
                dataType: 'json',
                method: 'post',
                data: JSON.stringify({productUUId: productUUId, values: formArray, count: 1, uploadMode: $('#upload_mode').val() }),
                success: function(result){
                    document.location="/basket";
                }
            });
            return false;
        }else{
            $.ajax({
                url: "/apps/api/basket/legacy/add",
                contentType: "application/json",
                dataType: 'json',
                method: 'post',
                data: JSON.stringify({productUUId: productUUId, values: formArray, count: 1, uploadMode: $('#upload_mode').val() }),
                success: function(result){
                    document.location="/basket";
                }
            });
            return false;
        }
    });
    if(productLoaded === 1) {
        loadCalc(productUUId, true);
    }else{
        loadCalc(productUUId);
    }
})

function loadCalc(productUUId, firstLoad = false) {
    var formArray = {};

    calcHasError = false;

    if(firstLoad) {
        formArray = productValues;
    }

    var formElements = $('#CALCFORM').serializeArray();
    $.each(formElements, function( index, value ) {
        var name = value.name;
        var val = value.value;
        formArray[name] = val;
    });
    
    $('#CALCFORM .invalid-feedback').remove();

    $('#CALCFORM input').each(function() {
        var $elm = $(this);
        var elementHasError = false;
        if($elm.data('minValue') && $elm.data('minValue') > formArray[$elm.attr('id')]) {
            $elm.parent().after($('<div class="invalid-feedback d-block">Wert muss größer sein als: ' + $elm.data('minValue') + '</div>')); 
            calcHasError = true;
            elementHasError = true;
        }
        if($elm.data('maxValue') && $elm.data('maxValue') < formArray[$elm.attr('id')]) {
            $elm.parent().after($('<div class="invalid-feedback d-block">Wert muss kleiner sein als: ' + $elm.data('maxValue') + '</div>')); 
            calcHasError = true;
            elementHasError = true;
        }
        if(elementHasError) {
            $elm.addClass("is-invalid");
        }else{
            $elm.removeClass("is-invalid");
        }
    });

    if(calcHasError) {
        return;
    }

    $.ajax({
          url: "/apps/api/plugin/system/psc/xmlcalc/price",
          contentType: "application/json",
          dataType: 'json',
          method: 'post',
          data: JSON.stringify({product: productUUId, test: false, values : formArray}),
          success: function(result){

              buildForm(result.elements, false);

              $.each(result.displayGroups, function(index, group) {
                buildcollapseHeader(group);
                buildForm(result.elements, group.id);
                buildcollapseFooter();
              });

              if(calcHasError) {
                $('#netto').html("ERR");
                $('#mwert').html("ERR");
                $('#brutto').html("ERR");
                $("#in_basket").prop("disabled",true);
                
                // PRECALC AUSBLENDEN?
                //loadPreCalc(productUUId, false, result.preCalc);
              }else{
                $('#netto').html(new Intl.NumberFormat('de-DE', { style: 'currency', currency: productLanguage }).format(result.netto/100));
                $('#mwert').html(new Intl.NumberFormat('de-DE', { style: 'currency', currency: productLanguage }).format(result.steuer/100));
                $('#brutto').html(new Intl.NumberFormat('de-DE', { style: 'currency', currency: productLanguage }).format(result.brutto/100));
                $("#in_basket").prop("disabled",false);
                
                loadPreCalc(productUUId, false, result.preCalc);
              }
              bindCalcButtons();
              productXml = result.xmlProduct;
        },
        error: function(err){
        }


  });

}

function bindCalcButtons() {
    $('.toogle-group').unbind();
    $('.toogle-group').on('click', function() {
        $('#' + $(this).data('target')).toggleClass("hide");
    });
    $('#CALCFORM input, #CALCFORM select').unbind();
    $('#CALCFORM input, #CALCFORM select').change(function() {
          loadCalc(productUUId);
    });
}

function buildcollapseHeader(group) {
    var $button = $('<button type="button">').data("target", "group-" + group.id).addClass('btn btn-primary w-100 toogle-group').text(group.name);
    var $div = $('<div>').addClass("hide").attr("id", "group-" + group.id);

    $('#CALCFORM').append($button);
    $('#CALCFORM').append($div);
}

function buildcollapseFooter(group) {
}

function loadPreCalc(productUUId, testMode, preCalc) {
    
    $('#preCalc').html('');
    if(preCalc.length === 0) {
        $('#preCalcContainer').hide();
    }else{
        $('#preCalcContainer').show();
    }
    $.each(preCalc, function(indexGroup, group) {
        var htmlGroup = $('<div class="col-4"><h5>' + group.name + '</h5><div class="row" id="group_' + indexGroup + '"></div></div>');
        $('#preCalc').append(htmlGroup);
        $.each(group.variants, function(indexVariant, variant) {
            var htmlVariant = $('<div class="col-6">' + variant.name + '</div><div style="text-align:right;" class="col-6" id="variant_' + indexGroup + '_' + indexVariant + '"></div>');
            $('#group_' + indexGroup).append(htmlVariant);
            
            var formElements = $('#CALCFORM').serializeArray();
            var formArray = {};
            $.each(formElements, function( index, value ) {
                var name = value.name;
                var val = value.value;
                formArray[name] = val;
            });
            $.each(variant.values, function(indexValue,value) {
                formArray[value.key] = value.value;
            });
            $.ajax({
                url: "/apps/api/plugin/system/psc/xmlcalc/price",
                contentType: "application/json",
                dataType: 'json',
                method: 'post',
                data: JSON.stringify({product: productUUId, test: testMode, values : formArray}),
                success: function(result){       
                    $('#variant_' + indexGroup + '_' + indexVariant).html(new Intl.NumberFormat('de-DE', { style: 'currency', currency: productLanguage }).format(result.netto/100));
                }
            })
        });
    });
}

function buildForm(elements, display_group = "") {
    var previousId = null;

	$.each(elements, function(index, element) {
        if(element.displayGroup != display_group)  {
            return;
        }

        if(element.valid === false) {
            removeElement(element);
            return;
        }
        var $label = $('<label>').addClass('form-label').text(element.name);
        $container = $('<div class="form-group" id="container_' + element.id + '"></div>');     
        if(element.htmlType == "hidden") {
                var $obj = $('<input>', {
                type: 'hidden',
              value: element.rawValue,
              id: element.id,
              name: element.id
            });
            $element = $('<div>').addClass("col-8");
            $obj.appendTo($element);

            addOrReplaceFormElement(previousId, element.id, $container, $element, null, element.valid);
            return;
        }
        if(element.htmlType == "select") {

            var $obj = $('<select>', {
              id: element.id,
              title: element.help,
              name: element.id
            });
           $.each(element.options, function(ind, opt) {
          
                if(opt.valid) {
                    $obj.append(new Option(opt.name, opt.id, opt.selected, opt.selected));
              }
           });
           $obj.addClass('form-control');
        }
        if(element.htmlType == "text") {
            var $obj = $('<input>', {
                type: 'text',
              value: element.defaultValue,
              id: element.id,
              name: element.id,
              title: element.help,
              disabled: true
            });
            $obj.addClass("form-control");
        }
        if(element.htmlType == "input") {
            var $obj = $('<input>', {
                type: 'text',
              value: element.rawValue,
              id: element.id,
              title: element.help,
              name: element.id
            });
            if(element.minValue != null) {
                $obj.data('minValue', element.minValue);
            }
            if(element.maxValue != null) {
                $obj.data('maxValue', element.maxValue);
            }
            $obj.addClass("form-control");
         }
        
        if(element.htmlType == "checkbox") {
            var $obj = $('<checkbox>', {
                value: element.rawValue,
              id: element.id,
              title: element.help,
              name: element.id
            });
        }
        
        if(element.htmlType == "textarea") {
            var $obj = $('<textarea>', {
              value: element.rawValue,
              id: element.id,
              title: element.help,
              name: element.id
            });
        }
        
        $element = $('<div>').addClass("form-controls");
        $inputGroup = $('<div class="input-group input-group-sm">');
        $inputGroup.appendTo($element);
        $obj.appendTo($inputGroup);

        addOrReplaceFormElement(previousId, element.id, $container, $element, $label, element.valid, display_group);
        if(element.helpLink) {
            $obj.after('<div class="input-group-append"><a class="btn btn-outline-primary btn-circle" onclick="modalopen(\'' + element.helpLink + '\');return false;" target="_blank" href="' + element.helpLink + '">?</a></div>');
        }
        if(element.validationErrors.length > 0) {
            if(calcHasError === false) {
                calcHasError = true;
            }
            $obj.addClass("is-invalid");
            $.each(element.validationErrors, function(errorIndex, error) {
                if(error.type === 'input::validation::max') {
                   $obj.parent().after($('<div class="invalid-feedback d-block">Wert muss kleiner sein als: ' + error.maxValue + '</div>')); 
                }
                if(error.type === 'input::validation::min') {
                   $obj.parent().after($('<div class="invalid-feedback d-block">Wert muss größer sein als: ' + error.minValue + '</div>')); 
                }
            });
        }else{
            $obj.removeClass("is-invalid");
        }
        previousId = element.id;
  })
}

function removeElement(element) {
    $('#container_' + element.id).remove();        
}

function addOrReplaceFormElement(previousId, id, $container, $element, $label, valid, display_group) {
    if($('#container_' + id).length > 0) {
        if(!valid) {
            $('#container_' + id).remove();
        }else{
            $('#container_' + id).html('');
            if($label !== null) {
                $label.appendTo($('#container_' + id));
            }
            $element.appendTo($('#container_' + id));
        }
    }else{
        if(previousId !== null && $('#container_' + previousId).length > 0) {
            $container.insertAfter($('#container_' +  previousId));
        }else{
            if(display_group != "") {
                $('#group-' + display_group).append($container);
            }else{
		        $('#CALCFORM').append($container);
            }
        }
        if($label !== null) {
            $label.appendTo($container);
        }
        $element.appendTo($container);
    }
}
function modalopen(href) {
    $('.help-modal').load(href,function(){
        $('#myModal').modal({show:true});});
}