var markup = '<tr>'
    + '<td><%= nr %></td>'
    + '<td><input type="text" class="form-control input-sm" name="item[<%= nr %>][company]" value="<%= company %>"/></td>'
    + '<td><input type="text" class="form-control input-sm" name="item[<%= nr %>][firstname]" value="<%= firstname %>"/></td>'
    + '<td><input type="text" class="form-control input-sm" name="item[<%= nr %>][lastname]" value="<%= lastname %>"/></td>'
    + '<td><input type="text" class="form-control input-sm" name="item[<%= nr %>][street]" value="<%= street %>"/></td>'
    + '<td><input type="text" class="form-control input-sm" name="item[<%= nr %>][house_number]" size="3" value="<%= house_number %>"/></td>'
    + '<td><input type="text" class="form-control input-sm" name="item[<%= nr %>][zip]" size="5" value="<%= zip %>"/></td>'
    + '<td><input type="text" class="form-control input-sm" name="item[<%= nr %>][city]" value="<%= city %>"/></td>'
    + '<td><input type="text" class="form-control input-sm" name="item[<%= nr %>][country]" size="2" value="<%= country %>"/></td>'
    + '<td><input type="text" class="form-control input-sm amount" name="item[<%= nr %>][amount]" size="5" value="<%= amount %>"/></td>'
    + '<td><button class="deliveryRemove btn btn-danger input-sm" data-value="<%= nr %>">-</button></td>'
    + '</tr>';

var wrongSumAmount = 'Ihre Aufteilung stimmt nicht mit der Menge überein. <%= sum %> != <%= amount %>';


var items = [];

var sumAmount = 0;

var itemDelivery = {
    company: 'Firma',
    nr: 1,
    street: 'Straße',
    house_number: '12',
    city: 'Ort',
    zip: '12345',
    country: 'DE',
    firstname: 'Vorname',
    lastname: 'Nachname',
    amount: 1000,
}

$(function(){
    
    $('.openMultiDelivery').unbind();
    $('.openMultiDelivery').click(function(){

        $('#myDeliveryData').modal();
        loadDeliveryData();
        return false;
    });
    


    $('a[rel="#addDeliveryContact"]').unbind();
    $('a[rel="#addDeliveryContact"]').click(function(){

        itemDelivery.nr = items.length + 1;

        $('#delivery_data > tbody:last-child')
            .append(_.template(markup, itemDelivery));

        items.push(itemDelivery);
        bindDeliveryButton();
        checkSumAmount();
    });


    $('a[rel="#saveDeliveryData"]').unbind();
    $('a[rel="#saveDeliveryData"]').click(function(){

        $.post(
            '/basket/savedeliverydata/format/json',
            $('#delivery_data_form').serialize(),
            function(data){
                $('#myDeliveryData').modal('hide');
                $('#deliveryaddresstext').html('Lieferaufteilung');
            }
        );
    });

});


function bindDeliveryButton() {
    $('.deliveryRemove').unbind();
    $('.deliveryRemove').click(function(){

        $(this).parent().parent().remove();
        $.post(
            '/basket/savedeliverydata/format/json',
            $('#delivery_data_form').serialize(),
            function(data){
                loadDeliveryData();
            }
        );

        return false;
    });

    $('.amount').unbind();
    $('.amount').change(function() {
        checkSumAmount();
    });
}

function checkSumAmount() {
    
        var sum = 0;

        $.each($('.amount'), function() {
            sum = sum + parseInt($(this).val());
        });

        if(sum != sumAmount) {
            $('.wrongSumAmount').html(_.template(wrongSumAmount, {sum: sum, amount: sumAmount}));
            $('.wrongSumAmount').show();
            $('a[rel="#saveDeliveryData"]').attr('disabled',true);
        }else{
            $('.wrongSumAmount').hide();
            $('a[rel="#saveDeliveryData"]').attr('disabled',false);
        }

}

function loadDeliveryData() {
    $.ajax({
        type: "POST",
        url: '/basket/getdeliverydata/format/json',
        data: {},
        success: function(data) {

            items = [];
            
            $('#delivery_data > tbody').empty();

            sumAmount = parseInt(data.sumAmount);

            $.each(data.rows, function(index, item) {

                item.nr = items.length + 1

                $('#delivery_data > tbody:last-child').append(_.template(markup, item));

                items.push(item);

            });
            bindDeliveryButton();
            checkSumAmount();
            return false;
        }
    })


}