<?php

define('PITCH_APIKEY', 'key_be09431368f511a97ba5da05dfbb0b94');
define('PITCH_SECRETKEY', 'secr_FAvZA1^4m&cBblB5rM24au6!DKk$yb');

function generateSignature () {
    $timestamp = time();
    $signature = md5(PITCH_APIKEY . PITCH_SECRETKEY . $timestamp);
    return array ('timestamp'=>$timestamp, 'apiKey'=>PITCH_APIKEY, 'signature'=>$signature);
}

$ch = curl_init();
curl_setopt($ch, CURLOPT_URL, "https://api.pitchprint.io/runtime/fetch-project");
curl_setopt($ch, CURLOPT_POST, true);

$opts = generateSignature();
$opts['projectId'] = 'e5fa0e03687d460dbd6d5fa614d848f3';

curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($opts));
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

$output = curl_exec($ch);
$http_status = curl_getinfo($ch, CURLINFO_HTTP_CODE);
$content_type = curl_getinfo($ch, CURLINFO_CONTENT_TYPE);
$curlerr = curl_error($ch);
curl_close($ch);

header('Content-Type: application/json');

echo $output;

?>
<html>
    <title>PitchPrint Sample SDK</title>
    <head>
        <script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
        <script src="https://pitchprint.io/rsc/js/client.js"></script>
    </head>
    <body>
		<div id="pp_loader_div"><img src="https://pitchprint.io/rsc/images/loaders/spinner_new.svg" ></div>
		<button id="launch_btn" >Launch Designer</button>
		<div id="pp_preview_div"></div>
	</body>
	<script>
		(function() {
			//Get handles to the UI elements we'll be using
			var _launchButton = document.getElementById('launch_btn'),
				_previewDiv = document.getElementById('pp_preview_div'),
				_loaderDiv = document.getElementById('pp_loader_div');
				
			//Disable the Launch button until PitchPrint is ready for use
			_launchButton.setAttribute('disabled', 'disabled');
				
				
			/*Initialize PitchPrint.
				Kindly read more here on the options.. https://docs.pitchprint.com
			*/
			var ppclient = new PitchPrintClient({
				apiKey: 'key_be09431368f511a97ba5da05dfbb0b94',		//Kinldy provide your own APIKey
				designId: '4aeb405b8514bfe66ffcbd5ca21dbd5b',	//Change this to your designId
                langCode: "de",
                mode: "edit",
                projectId: "e5fa0e03687d460dbd6d5fa614d848f3",
				userData: "Email-adgigant@gmail.com",
				custom: true
			});
				
			//Function to run once the app is validated (ready to be used)
			var appValidated = () => {
				_launchButton.removeAttribute('disabled');				//Enable the Launch button
				_launchButton.onclick = () => ppclient.showApp();		//Attach event listener to the button when clicked to show the app
				_loaderDiv.style.display = 'none';						//Hide the loader
			};
			
			//Function to run once the user has saved their project
			var projectSaved = (_val) => {
				let _data = _val.data;	
                console.log(_data);								//You can console.log the _data varaible to see all that's passed down
                console.log(_data.projectId);								//You can console.log the _data varaible to see all that's passed down
				if (_data && _data.previews && _data.previews.length) {
					_previewDiv.innerHTML = _data.previews.reduce((_str, _prev) => `${_str}<img src="${_prev}">`, '');		//Show the preview images
				}
			};
			
			//Attach events to the app. You can see a list of all the events here: https://docs.pitchprint.com
			ppclient.on('app-validated', appValidated);
			ppclient.on('project-saved', projectSaved);
		})();
	</script>
</html>