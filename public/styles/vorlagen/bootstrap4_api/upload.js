var curstep = 1;

var step_max = 1;

var calccount = 1;

var isBack = false;

var disableUploadButtons = true;

var upload_success = false;

function showUpload() {
    $('#wizard').html('<ul></ul>');
    $('#upload_mode').val('article');
    $.getJSON('/preflight/getuploads/format/json?article=' + productUrl + '&which=' + productXml + '&' +  $("#CALCFORM").serialize(), function(data) {

        var html = "";

        disableUploadButtons = false;

        var uploadtemplate = tmpl("template-wizard");

        $.each(data.uploads, function(i, upload) {


            $('#wizard ul').append('<li><a id="link_step-' + (i+1) + '" class="' + upload.id + '" href="#step-' + (i+1) + '" style="text-align: left;background-color: #F8F8F8; border-color: #ccc; color: #CCCCCC; width: 300px; height: 80px;"><h3>' + upload.name + '</h3><p>' + upload.description + '</p></a></li>');

            //$('#wizard').append('<div id="step-' + (i+1) + '" rel="' + upload.id + '"><div class="upload_border"><h2>Uploadassistent</h2><div id="upload_' + upload.id + '" style="width: 780px; height: 110px;"></div></div><div class="upload_border"><h2>Ergebnisse</h2><div class="plupload"><div class="plupload_content"><div class="plupload_filelist_header"><div class="plupload_file_name">Dateiname</div><div class="plupload_file_action preflight">Löschen</div><div class="plupload_file_status  preflight"><span>Status</span></div><div class="plupload_file_size">Größe</div><div class="plupload_clearer">&nbsp;</div></div><ul class="plupload_filelist" id="files_' + upload.id + '"></ul></div></div></div></div>');

		// karsten euting - 12.11.2014 
		// $('#wizard').append('<div id="step-' + (i+1) + '" rel="' + upload.id + '"><h2>Uploadassistent</h2><div id="upload_' + upload.id + '" style="width: 780px; height: 110px;"></div></div>');
		$('#wizard').append('<div id="step-' + (i+1) + '" rel="' + upload.id + '"><div id="upload_' + upload.id + '" class="uploadassistent" ></div></div>');

            $('#upload_' + upload.id).html(uploadtemplate({files:[{id: upload.id, sessionid: sessionid}]}));

            $('#step-' + (i+1)).data('upload_id', upload.id);


            html +='<div id="finish_upload_' + upload.id + '" style="float:left; width: 370px; margin: 10px;"></div>';

            step_max = (i+1);
        });

        $('#wizard ul').append('<li style="display:none"><a id="link_step-' + (step_max+1) + '" href="#step-' + (step_max+1) + '"></a></li>');

        $('#wizard').append('<div id="step-' + (step_max+1) + '"><div id="finish_uploads">' + html + '</div></div>');

        var selectedStep = 0;
        if(upload_success) {
            selectedStep = step_max;
        }

        $('#wizard').smartWizard({
            labelNext:'Weiter', // label for Next button
            labelPrevious:'Zurück', // label for Previous button
            labelFinish:'Fertigstellen',  // label for Finish button
            // Events
            enableFinishButton: true,
            selected: selectedStep,
            enableAllSteps: true,
            onLeaveStep: null, // triggers when leaving a step
            onShowStep: function(step) {
                var step_num= step.attr('rel');
                curstep = step_num;
                var upload_id = $('#step-' + step_num).data('upload_id');

                $('#link_step-' + curstep).css({
                    'border-color': '#EA8511',
                    'background-color': '#EA8511',
                    'color': '#ffffff'
                });

                if(step_num == (step_max+1)) {
                    for(var i=1; i<step_num; i++) {
                        $('#link_step-' + i).css('border-color', '#22467C');
                        $('#link_step-' + i).css('background-color', '#22467C');
                        $('#link_step-' + i).css('color', '#ffffff');
                    }
                    $('#wizard .buttonNext').attr('class', 'buttonNext buttonDisabled');
                    $('.buttonFinish').show();
                    getfinishuploads();
                }else{
                    $('.buttonFinish').hide();
                }
                $('#upload_' + upload_id).fileupload({
                    singleFileUploads: true,
                    autoUpload: true,
                    limitMultiFileUploads: 1,
                    done: function (e, data) {
                        $('.template-upload').remove();
                        finishPreflight(data.result.motive[0].id, data.result.motive[0].uuid);

                    }
                })


                if(step_num != (step_max+1)) {
                    getuploads(upload_id);
                    $('#wizard .buttonNext').attr('class', 'buttonNext');
                }

                //$('.plupload_add, .plupload_start').html('');

            },  // triggers when showing a step
            onFinish: function() {
                $('#wizard').html('<ul></ul>');
                $('#upload-box').css('top', '-600px');
                $('.modal').modal('hide');
                loadCalc(productUUId);
                upload_success = true;
                if(upload_open_from == 2) {
                    $('#buyform').submit();
                }

            },
            onLeaveStep: function(step, back) {
                var step_num= step.attr('rel');
                var upload_id = $('#step-' + step_num).data('upload_id');


                if( $('#files_' + upload_id).children().length == 0 && step_num != (step_max+1) && !back) {
                    $('#wizard').smartWizard('showMessage','Bitte laden Sie eine Datei hoch');
                    return false;
                }
                return true;
            }

        });
        var height = $(window).height()/100*70;

        $('#upload-box').css({top: (height/2)});
        $('.close_upload').unbind();
        $('.close_upload').click(function() {
            $('#wizard').html('<ul></ul>');
            $('#upload-box').css('top', '-600px');
            $('#upload_mode').val('');
            $.unblockUI();

        });
    });

}

function getfinishuploads() {

    $.getJSON('/preflight/getfiles/format/json', function(data) {
        $.each(data.data, function(index, item) {
            $('#finish_upload_' + item.what).html('');
            $.getJSON('/preflight/getfile/format/json?no_image=1&uuid=' + item.key, function(itemm) {
                $('#finish_upload_' + item.what).append('<div id="finish_file_' +  item.key + '"><div style="text-align: center; min-height:200px;" id="finish_file_preview_' +  item.key + '"></div><a href="javascript:deleteUploadPreflightFinish(\'' + item.what + '\',\'' +  item.key + '\');">Datei Löschen</a><div id="finish_preflight_' +  item.key + '"><ul><li>System wartet auf Daten...</li></ul></div></div>');
                $.ajax({
                    type: "POST",
                    url: '/preflight/checkpreflight/format/json?uuid=' + item.key + '&type=' + item.what + '&ajax_calc_id=' + productUrl,
                    data: $("#CALCFORM").serialize(),
                    success: function(data) {
                        if(data.success) {
                            $('#wizard ul li a.' + item.what).css('border-color', '#8CC63F');
                            $('#wizard ul li a.' + item.what).css('background-color', '#8CC63F');
                            $('#wizard ul li a.' + item.what).css('color', '#ffffff');
                            $('#wizard ul li a.' + item.what + ' span small').html('Ihre Daten wurden erfolgreich übermittelt.');
                            $('#finish_preflight_' + item.key).html('<div class="alert alert-success">Preflight Check erfolgreich</div>');

                        }else{
                            var mess = '<div class="alert alert-error">';
                            $.each(data.errors, function(i, error) {
                                mess = mess + error.message + "<br/>";
                            });
                            mess = mess + "</div>";
                            $('#finish_preflight_' + item.key).html(mess);

                            $('#wizard ul li a.' + item.what).css('border-color', '#e20000');
                            $('#wizard ul li a.' + item.what).css('background-color', '#e20000');
                            $('#wizard ul li a.' + item.what).css('color', '#ffffff');
                            $('#wizard ul li a.' + item.what + ' span small').html('Bitte überprüfen Sie Ihre Daten.');
                        }

                        $('#finish_file_preview_' +  item.key).html('Lade Vorschau bitte warten...');

                        $.getJSON('/preflight/getfile/format/json?uuid=' + item.key, function(item_image) {
                            $('#finish_file_preview_' +  item.key).html(item_image.img);


                        });
                    }
                });
            });
        });
    });

}

function deleteUploadPreflightFinish(upload_id, uuid)
{

    $.ajax({
        type: "GET",
        url: "/service/upload/deleteupload",
        data: 'uuid=' + uuid,
        success: function(msg){
            for(var i=1; i<(step_max+1); i++) {
                if($('#step-' + i +'[rel="' + upload_id + '"]').children().length == 0) {
                    $('#link_step-' + i).css({
                        'border-color': '#8CC63F',
                        'background-color': '#8CC63F',
                        'color':'#ffffff'
                    });
                }else{
                    $('#link_step-' + i).css({
                        'border-color': '#EA8511',
                        'background-color': '#EA8511',
                        'color': '#ffffff'
                    });
                }
            }
            var step = $('div[rel="' + upload_id + '"]').attr('id');
            $('#upload_' + upload_id).fileupload('enable');
            $("#link_" + step).click();


        }
    });
}

function getuploads(id) {
    $('#files_' + id).html('');
    $.getJSON('/preflight/getfiles/format/json', function(data) {
        $.each(data.data, function(index, item) {
            if(id == item.what) {
                finishPreflight(id, item.key);
                $('#upload_' + (id) + '_browse').hide();
            }
        });
    });

}

function finishPreflight(id, uuid)
{
    $.getJSON('/preflight/getfile/format/json?no_image=1&uuid=' + uuid, function(item) {
        var uploadtemplate = tmpl("template-download");

        $('#files_' + id).append(uploadtemplate({files:[{key: item.key, error: false, name: item.name, size: 0, delete_url:'deleteUploadPreflight(\'' + id + '\',\'' + uuid + '\');' }]}));

        checkPreflightSingle(id, uuid);
    });
}



function checkPreflightSingle(id, uuid, di)
{
    $.ajax({
        type: "POST",
        url: '/preflight/checkpreflight/format/json?uuid=' + uuid + '&type=' + id + '&ajax_calc_id=' + productUrl,
        data: $("#CALCFORM").serialize(),
        success: function(data) {
            if(data.success) {
                if(disableUploadButtons) {
                    $('#link_step-' + curstep).css('border-color', '#8CC63F');
                    $('#link_step-' + curstep).css('background-color', '#8CC63F');
                    $('#link_step-' + curstep).css('color', '#ffffff');
                }
                $('#upload-' + uuid + ' .preflight').html('<span><i class="icon-ok"></i> Preflightcheck OK</span>');
                $('#wizard .buttonNext').attr('style', 'display:block');

            }else{
                var mess = '';
                $.each(data.errors, function(i, error) {
                    mess += error.message + " ";
                });
                mess = mess + "";
                $('#upload-' + uuid + ' .preflight').html('<span><i class="icon-warning-sign"></i> Preflightcheck Fehler<br/>' + mess + '</span>');
                $('#wizard .buttonNext').attr('style', 'display:none');
            }
            if(disableUploadButtons) {
                $('#upload_' + id).fileupload('disable');
                $('.msgBox').hide();
                $('#upload-' + uuid + ' .btn.btn-danger').prop('disabled', '');
            }
        }
    });
}

function updateUpload()
{
    $.getJSON('/service/upload/getfilesproductjson', function(data) {
        $('#uploads').html('');
        if(data.length > 0) {
            $('#uploadFrame').slideDown();
        }
        $.each(data, function(index, item) {
            $('#uploads').append(item.img + '<a href="/uploads/' + item.shop + '/article/' + item.value + '" target="_blank"></a> <a href="javascript:deleteUpload(\'' + item.key + '\');">Datei Löschen</a><br/>');
        });
    });
}

function deleteUploadPreflight(upload_id, uuid)
{
    $.ajax({
        type: "GET",
        url: "/service/upload/deleteupload",
        data: 'uuid=' + uuid,
        success: function(msg){
            /*$('#upload_' + upload_id).pluploadQueue({
             runtimes : 'gears,html5,flash,silverlight,browserplus',
             browse_button : 'pick files',
             max_file_size : '250mb',
             url : '/preflight/upload/format/json',
             flash_swf_url : '/scripts/plupload/js/plupload.flash.swf',
             silverlight_xap_url : '/scripts/plupload/js/plupload.silverlight.xap',
             multipart_params: {
             'id': upload_id,
             'ARTID': sessionid
             },
             init : {
             FileUploaded: function(up, file, info) {
             info = $.parseJSON(info.response);
             finishPreflight(upload_id, info.id);

             },
             FilesAdded: function(up, files) {
             $('#upload_' + upload_id + '_browse').hide();
             },
             FilesRemoved: function(up, files) {
             $('#upload_' + upload_id + '_browse').show();
             }

             },
             filters : [
             {
             title : "Image files",
             extensions : "jpg,gif,png,pdf,jpeg,tiff,tif"
             }
             ]
             });*/
            $('#upload-' + uuid).remove();
            $('#upload_' + upload_id).fileupload('enable');

            //$('.plupload_add, .plupload_start').html('');
        }
    });
    return false;
}

function deleteUpload(uuid)
{
    $.ajax({
        type: "GET",
        url: "/service/upload/deleteupload",
        data: 'uuid=' + uuid,
        success: function(msg){
            eval('msg = ' + msg);
            finish(msg);
        }
    });

    updateUpload();
}


function updateUploadCenter(id, di, pos)
{
    $.getJSON('/preflight/getfilescenter/format/json?id=' + pos + '&type=' + id, function(data) {
        $.each(data.data, function(index, item) {
            finishCenter(id, item.key, di);
        });
    });


}

function deleteUploadCenter(id, uuid, di) {
    $.ajax({
        type: "GET",
        url: "/service/upload/deletecenter",
        data: 'uuid=' + uuid,
        success: function(msg){
            $('#file_' + uuid).remove();
            $('#' + di + '_browse').show("slow");
        }
    });
}